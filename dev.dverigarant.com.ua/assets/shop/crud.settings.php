<?php
/* Shop settings */
if(!defined('IN_MANAGER_MODE') || IN_MANAGER_MODE != 'true') die("No access");
define("ROOT", dirname(__FILE__).'/settings/');

$allowed_modules = array_diff(scandir(ROOT), array('.', '..', 'index.tpl'));
$default_module  = reset($allowed_modules);
$modules = array();

foreach ($allowed_modules as $module) {
    $file = ROOT.$module."/index.php";

    $fp = fopen( $file, 'r' );
    $file_data = fread( $fp, 8192 );
    fclose( $fp );

    if (preg_match_all( '/^[ \t\/*#@]*(Name|Icon|Version):(.*)$/mi', $file_data, $match )) {
        $modules[$module] = array_combine($match[1], $match[2]);
    }
}

$languages      = explode(",", $modx->config['lang_list']);
$bootstrap      = isset($_GET['b']) && in_array($_GET['b'], $allowed_modules) ? $_GET['b'] : $default_module;
$controller     = isset($_GET['c']) ? $_GET['c'] : "";
$res            = [];
$res['title']   = "Дополнительные настройки";
$res['version'] = "v 2.2";
$res['url']     = $url = MODX_MANAGER_URL."index.php?a=".$_GET['a']."&b=";
$search         = true;
$modx->config['lang'] = $modx->config['lang_default'];

include dirname(__FILE__)."/class.shop.php";
$shop = new Shop($modx);

include_once ROOT.$bootstrap."/index.php";

if (isset($tpl)) {
    ob_start();
    include_once ROOT.$bootstrap.'/tpl/'.$tpl;
    $res['content'] = ob_get_contents();
    ob_end_clean();
}

$url = $res['url'];

include_once(includeFileProcessor("includes/header.inc.php", $manager_theme));
include ROOT."index.tpl";
include_once(includeFileProcessor("includes/footer.inc.php", $manager_theme));
die;