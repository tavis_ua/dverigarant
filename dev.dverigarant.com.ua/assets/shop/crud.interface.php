<?php
if(!defined('IN_MANAGER_MODE') || IN_MANAGER_MODE != 'true') die("No access");
define("ROOT", dirname(__FILE__));
define("TPLS", dirname(__FILE__)."/tpl/interface/");
$languages            = explode(",", $modx->config['lang_list']);
$bootstrap            = isset($_GET['b']) ? $_GET['b'] : "dashboard";
$controller           = isset($_GET['c']) ? $_GET['c'] : "";
$cat_templ_id         = $modx->config['shop_tpl_category']; // id шаблона категорий
$res                  = [];
$res['title']         = "Управление магазином";
$res['version']       = "v 2.2";
$res['url']           = $url = "/inspiration/index.php?a=".$_GET['a']."&b=";
$search               = "";
$modx->config['lang'] = $modx->config['lang_default'];
$messages             = [
    "item_added"       => $_lang["shop_item_added"],
    "item_updated"     => $_lang["shop_item_updated"],
    "payments_updated" => $_lang["shop_payments_updated"],
    "order_updated"    => $_lang["shop_order"]." № ".str_pad($_GET['id'], 6, 0, STR_PAD_LEFT)." ".$_lang["shop_updated"],
    "import_error_xml" => $_lang["shop_import_error_xml"],
    "import_error_csv" => $_lang["shop_import_error_csv"],
    "import_ok"        => $_lang["shop_import_ok"],
    "export_ok"        => $_lang["shop_export_ok"],
    "recall_updated"   => $_lang["shop_recall_updated"],
    "recall_deleted"   => $_lang["shop_recall_deleted"],
    "filter_deleted"   => $_lang["shop_filter_deleted"]
];
$status_names = [
    "ru" => ["Удалён","Новый","Отправлен","Доставлен"],
    "ua" => ["Видалено","Новий","Відправлено","Доставлено"],
    "en" => ["Deleted","New","Sent","Delivered"]
];

include ROOT."/class.shop.php";
$shop = new Shop($modx);

switch ($bootstrap) {
    default :
        include_once ROOT.'/interface/'.$bootstrap.'/index.php';
        $tpl = "../../interface/".$bootstrap."/tpl/" . $tpl;
        break;

	case 'variations':
		include ROOT.'/inc/variations.php';
		break;
    case "deleteAditionalEquipment":{
        if(isset($_POST['id'])){
            print $shop->deleteAdditionalEquipment($_POST['id']);
            exit();
        }
    }break;
    case "updateAditionalEquipment":{
        if(isset($_POST['id'])){
            print $shop->updateAdditionalEquipment($_POST);
            exit();
        }
    }break;
    case "addAditionalEquipment":{
        print $shop->addAdditionalEquipment($_POST);
        exit();
    }break;
    case "deleteEquipment":{
        if(isset($_POST['id'])){
            print $shop->deleteEquipment($_POST['id']);
            exit();
        }
    }break;
    case "updateEquipment":{
        if(isset($_POST['id'])){
            print $shop->updateEquipment($_POST);
            exit();
        }
    }break;
    case "addEquipment":{
        print $shop->addEquipment($_POST);
        exit();
    }break;
    case "showEquipmentList":{
        print $shop->showEquipmentList($_POST);
        exit();
    }break;
    case "addEquipmentValueItem":{
        print $shop->addEquipmentValueItem($_POST);
        exit();
    }break;
    case "updateValueEquipment":{
        print $shop->updateEquipmentValueItem($_POST);
        exit();
    }break;
    case "products":
        switch ($controller) {
            case "add":
                $tiny_mce     = $shop->tinyMCE("introtext, additional, description, content", "300px");
                $categories   = $shop->getCategories($cat_templ_id, $modx->config['lang']);
                $recommenders = $shop->getRecommender();
                $product['product_categories'] =  [];
                $product['product_recommender'] = [];
                $tpl          = "product.tpl";
                break;
            case "edit":
                $tiny_mce        = $shop->tinyMCE("introtext, additional, description, content, seo_content", "300px");
                $categories      = $shop->getCategories($cat_templ_id, $modx->config['lang']);
                $recommenders    = $shop->getRecommender($_GET['i']);
                $options         = $shop->getProductOptions($_GET['i']);
                $product['option_id'] = count($options) == 1 ? $options[0]['id']: false;
                $pfvs            = count($options) == 1 ? $shop->getProductFilters($product['option_id']): false;
                $addEqup         = $shop->getAdditionalEquipment($_GET['i']);
                $equipment       = $shop->getEquipment($_GET['i']);
                $equipmentValues = $shop->getEquipmentValues($_GET['i']);
                $filters         = $shop->getProductFilterList($_GET['i']);
                $characteristics = $shop->getProductCharacteristicList($_GET['i']);
                $product         = $shop->getProduct($_GET['i']);
                foreach ($languages as $value) {
                    $product[$value] = $shop->getProductString($_GET['i'], $value);
                }
                $tpl = "product.tpl";
                break;
            case "save":
                $_POST['product']['product_id'] = $shop->saveProduct($_POST['product']);
                foreach ($languages as $value) {
                    $shop->saveProductString($_POST['product'], $value);
                }
                $modx->clearCache();
                die(header("Location: ".$url."products&c=edit&i=".$_POST['product']['product_id']));
            case "update":
                $shop->updateProduct($_POST['product']);
                if (is_array($_POST['options']) && count($_POST['options']) > 0) {
                    $shop->updateProductOptions($_POST['options']);
                }
                foreach ($languages as $value) {
                    $shop->saveProductString($_POST['product'], $value);
                }
                $modx->clearCache();
                die(header("Location: ".$url."products"));
            case "copy":
                $productId = $shop->copyProduct($_GET['i']);
                $modx->clearCache();
                die(header("Location: ".$url."products&c=edit&i=".$productId));
            case "delete" :
                $shop->deleteProduct($_GET['i']);
                $modx->clearCache();
                die(header("Location: ".$url."products"));
            default:
                $search = [];
                $res['url'] .= "products";
                if (isset($_GET['srch']) && trim($_GET['srch']) != '') {
                    $search['srch'] = trim($_GET['srch']);
                    $res['url'] .= "&srch={$search['srch']}";
                }
                if (isset($_GET['filter_status']) && trim($_GET['filter_status']) != '') {
                    $search['filter_status'] = (int)$_GET['filter_status'];
                    $res['url'] .= "&filter_status={$search['filter_status']}";
                }
                if (isset($_GET['filter_publish']) && trim($_GET['filter_publish']) != '') {
                    $search['filter_publish'] = (int)$_GET['filter_publish'];
                    $res['url'] .= "&filter_publish={$search['filter_publish']}";
                }
                if (isset($_GET['filter_availability']) && trim($_GET['filter_availability']) != '') {
                    $search['filter_availability'] = (int)$_GET['filter_availability'];
                    $res['url'] .= "&filter_availability={$search['filter_availability']}";
                }

                $modx->setPlaceholder('url', $res['url']."&p=");
                $products = $shop->getProductList($search);
                $pagin    = $shop->getPaginate();
                $tpl = "products.tpl";
                break;
        }
        break;
    case "promocodes":
        if ($_GET['code'] != "") die(json_encode($modx->db->getRow($modx->db->query("SELECT * FROM `modx_a_codes` WHERE code_id = ".$_GET['code']))));
        if (isset($_GET['activate']) && (int)$_GET['activate'] > 0) {
            $status = isset($_GET['status']) ? (int)$_GET['status'] : 0;
            $modx->db->query("UPDATE `modx_a_codes` SET code_status = {$status} WHERE code_id = ".(int)$_GET['activate']);
            die(header("Location: ".$url."promocodes"));
        }
        if (count($_POST) > 0) {
            if (!empty($_POST['code']['id'])) {
                $modx->db->query("
                    UPDATE `modx_a_codes` SET 
                        code_expire   = '".$_POST['code']['expire']."',
                        code          = '".$modx->db->escape($_POST['code']['code'])."',
                        code_freq     = '".$_POST['code']['freq']."',
                        code_mod      = '".(int)$_POST['code']['mod']."'
                    WHERE code_id = '".$_POST['code']['id']."'
                ");
            } else {
                $modx->db->query("
                    INSERT INTO `modx_a_codes` SET 
                        code_date     = '".date("Y-m-d H:i:s")."',
                        code_expire   = '".$_POST['code']['expire']."',
                        code          = '".$modx->db->escape($_POST['code']['code'])."',
                        code_freq     = '".$_POST['code']['freq']."',
                        code_mod      = '".(int)$_POST['code']['mod']."'
                    ON DUPLICATE KEY UPDATE code_expire = '".$_POST['code']['expire']."'
                ");
            }
        }
        $codes = $modx->db->query("
            SELECT *, 
                (SELECT count(*) from `modx_a_order` WHERE order_code = code) AS 'used'
            FROM `modx_a_codes` 
            ".(isset($_GET['s']) ? " WHERE code like '%".$modx->db->escape($_GET['s'])."%'" : "")." 
            ORDER BY code_date DESC
        ");
        $search = true;
        $modx->setPlaceholder('url', $url."promocodes&p=");
        $pagin = $shop->getPaginate();
        $tpl   = "promocodes.tpl";
        break;

    case "images":
        $imagePath = "assets/products/".$_REQUEST['folder'];
        $folder = MODX_BASE_PATH.$imagePath;
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0777);
        }
        switch ($controller) {
            case "set_cover":
                $error = [];
                foreach ($_FILES['uploader']['tmp_name'] as $key => $value) {
                    list($width, $height) = getimagesize($value);
                    if ($width <= $modx->config['maxImageWidth']) {
                        if ($height <= $modx->config['maxImageHeight']) {
                            $type = explode("/", $_FILES['uploader']['type'][$key]);
                            if ($type[0] == "image") {
                                $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
                                $filename = $folder."/". $name;
                                move_uploaded_file($value, $filename);
                                chmod($filename, 0644);
                                $modx->db->query("DELETE FROM `modx_a_product_images` WHERE `image_use` = 'cover' AND `image_product_id` = ".(int)$_REQUEST['folder']);
                                $modx->db->query("INSERT INTO `modx_a_product_images` SET `image_use` = 'cover', `image_file` = '".$name."', `image_product_id` = ".(int)$_REQUEST['folder']);
                            }
                        } else {
                            $error[$_FILES['uploader']['name'][$key]] = $_lang['image_error_height'].$modx->config['maxImageHeight']."px.";
                        }
                    } else {
                        $error[$_FILES['uploader']['name'][$key]] = $_lang['image_error_width'].$modx->config['maxImageWidth']."px.";
                    }
                }
                die(json_encode($error));
            case "get_cover" :
                $images = $shop->getProductImages($_REQUEST['folder'], 'cover');
                while ($img = $modx->db->getRow($images)) {
                    $rimg = $modx->runSnippet('R', array('img' => "/".$imagePath."/".$img['image_file'], "folder" => "backendProducts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100'));
                    require TPLS."image_cover.tpl";
                }
                die;
            case "set_second":
                foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
                    $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
                    $filename = $folder."/". $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                    $modx->db->query("DELETE FROM `modx_a_product_images` WHERE image_use = 'second' AND image_product_id = ".(int)$_REQUEST['folder']);
                    $modx->db->query("INSERT INTO `modx_a_product_images` SET image_use = 'second', image_file = '".$name."', image_product_id = ".(int)$_REQUEST['folder']);
                }
                echo $name;
                die;
            case "get_second" :
                $images = $shop->getProductImages($_REQUEST['folder'], 'second');
                while ($img = $modx->db->getRow($images)) {
                    $rimg = $modx->runSnippet('R', array('img' => "/".$imagePath."/".$img['image_file'], "folder" => "backendProducts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100'));
                    require TPLS."image_cover.tpl";
                }
                die;
            case "set_slider":
                foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
                    $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
                    $filename = $folder."/". $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                    $modx->db->query("INSERT INTO `modx_a_product_images` SET image_use = 'slider', image_file = '".$name."', image_product_id = ".(int)$_REQUEST['folder']);
                }
                echo $name;
                die;
            case "get_slider" :
                $images = $shop->getProductImages($_REQUEST['folder'], 'slider');
                while ($img = $modx->db->getRow($images)) {
                    $rimg = $modx->runSnippet('R', array('img' => "/".$imagePath."/".$img['image_file'], "folder" => "backendProducts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100'));
                    require TPLS."images.tpl";
                }
                die;
	        case "set_product_tags":
		        foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
			        $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
			        $filename = $folder."/". $name;
			        move_uploaded_file($value, $filename);
			        chmod($filename, 0644);
			        $modx->db->query("INSERT INTO `modx_a_product_images` SET image_use = 'product_tag', image_file = '".$name."', image_product_id = ".(int)$_REQUEST['folder']);
		        }
		        echo $name;
		        die;
	        case "get_product_tags" :
		        $images = $shop->getProductImages($_REQUEST['folder'], 'product_tag');
		        while ($img = $modx->db->getRow($images)) {
			        $rimg = $modx->runSnippet('R', array('img' => "/".$imagePath."/".$img['image_file'], "folder" => "backendProducts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100'));
			        require TPLS."images.tpl";
		        }
		        die;
            case "delete_image" :
                $folder = (int)$_REQUEST['folder'];
                unlink($folder."/".$_REQUEST['delete']);
                $modx->db->query("DELETE FROM `modx_a_product_images` WHERE image_file = '".$modx->db->escape($_REQUEST['delete'])."' AND image_product_id = '".(int)$folder."'");
                die;
            case "edit_image" :
                $img_id = $modx->db->makeArray($modx->db->query("
                    SELECT image_id 
                    FROM `modx_a_product_images` 
                    WHERE image_product_id = ".(int)$_GET['product_id']." AND image_file = '".$modx->db->escape($_GET['file'])."' 
                    LIMIT 1
                "));
                if (count($img_id) == 0) die;
                $image_id = $img_id[0]['image_id'];
                $fields = $modx->db->query("
                    SELECT * 
                    FROM  `modx_a_product_image_fields` 
                    WHERE `image_image_id` = $image_id
                ");
                while ($field = $modx->db->getRow($fields)) {
                    $image[$field['image_lang']] = $field;
                }
                include TPLS."tabs.tpl";
                die;
            case "set_image_fields" :
                if (is_array($_POST['image'])) {
                    foreach ($_POST['image'] as $id => $lagArray) {
                        if (0 < (int)$id) {
                            foreach ($lagArray as $langId => $value) {
                                $modx->db->query("
                                    INSERT INTO `modx_a_product_image_fields` SET 
                                        image_image_id    = '".$id."',
                                        image_lang        = '".$modx->db->escape($langId)."',
                                        image_alt         = '".$modx->db->escape($value['image_alt'])."',
                                        image_title       = '".$modx->db->escape($value['image_title'])."',
                                        image_description = '".$modx->db->escape($value['image_description'])."'
                                    ON DUPLICATE KEY UPDATE 
                                        image_alt         = '".$modx->db->escape($value['image_alt'])."',
                                        image_title       = '".$modx->db->escape($value['image_title'])."',
                                        image_description = '".$modx->db->escape($value['image_description'])."'
                                ");
                            }
                        }
                    }
                }
                die('Ok');
            case "set_sort" :
                if (count($_POST) > 0) {
                    $sort = 1;
                    foreach ($_POST['image'] as $key => $value) {
                        $modx->db->query("UPDATE `modx_a_product_images` SET image_position = ".$sort." WHERE image_file = '".$modx->db->escape($key)."'");
                        $sort++;
                    }
                }
                die;
        }
        break;

}
if (isset($tpl)) {
    ob_start();
    include TPLS.$tpl;
    $res['content'] = ob_get_contents();
    ob_end_clean();
}
include_once(includeFileProcessor("includes/header.inc.php",$manager_theme));
include TPLS."index.tpl";
include_once(includeFileProcessor("includes/footer.inc.php", $manager_theme));
die;