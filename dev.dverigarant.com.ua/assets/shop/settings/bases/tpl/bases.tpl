<form id="editForm" action="<?=$url?>bases" method="post">
    <h2 class="muted">Основные настройки</h2>
    <table class="table table-condensed table-striped table-bordered table-hover">
        <?php if ($isNews): ?>
            <tr>
                <th class="vertical-align">Корень блога:<br><small class="text-muted">[(news_catalog_root)]</small></th>
                <td>
                    <input type="number" name="base[news_catalog_root]" value="<?=$modx->config['news_catalog_root']?>" class="form-control"/>
                </td>
            </tr>
            <tr>
                <th class="vertical-align" width="160px">Страница новости:<br><small class="text-muted">[(news_page_item)]</small></th>
                <td>
                    <input type="number" name="base[news_page_item]" value="<?=$modx->config['news_page_item']?>" class="form-control"/>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <th class="vertical-align">Корень акций:<br><small class="text-muted">[(discounts_catalog_root)]</small></th>
            <td>
                <input type="number" name="base[discounts_catalog_root]" value="<?=$modx->config['discounts_catalog_root']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align" width="160px">Страница акции:<br><small class="text-muted">[(discounts_page_item)]</small></th>
            <td>
                <input type="number" name="base[discounts_page_item]" value="<?=$modx->config['discounts_page_item']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Товар, URL от корня:<br><small class="text-muted">[(shop_root_item)]</small></th>
            <td>
                <input type="hidden" name="base[shop_root_item]" value="0"/>
                <input type="checkbox" name="base[shop_root_item]" value="1" <?=$modx->config['shop_root_item'] == 1 ? 'checked' : '' ?> class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align" width="160px">Страница товара:<br><small class="text-muted">[(shop_page_item)]</small></th>
            <td>
                <input type="number" name="base[shop_page_item]" value="<?=$modx->config['shop_page_item']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Шаблон категории:<br><small class="text-muted">[(shop_tpl_category)]</small></th>
            <td class="vertical-align">
                <input type="number" name="base[shop_tpl_category]" value="<?=$modx->config['shop_tpl_category']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Корень каталога:<br><small class="text-muted">[(shop_catalog_root)]</small></th>
            <td>
                <input type="number" name="base[shop_catalog_root]" value="<?=$modx->config['shop_catalog_root']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Спасибо за заказ:<br><small class="text-muted">[(shop_thanks_order)]</small></th>
            <td>
                <input type="number" name="base[shop_thanks_order]" value="<?=$modx->config['shop_thanks_order']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Преимущества покупки:<br><small class="text-muted">[(advantages)]</small></th>
            <td>
                <input type="number" name="base[advantages]" value="<?=$modx->config['advantages']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Остались вопросы:<br><small class="text-muted">[(questions)]</small></th>
            <td>
                <input type="number" name="base[questions]" value="<?=$modx->config['questions']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Страница Усех:<br><small class="text-muted">[(success_send)]</small></th>
            <td>
                <input type="number" name="base[success_send]" value="<?=$modx->config['success_send']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Оформить заказ:<br><small class="text-muted">[(checkout)]</small></th>
            <td>
                <input type="number" name="base[checkout]" value="<?=$modx->config['checkout']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Страница поиска:<br><small class="text-muted">[(search)]</small></th>
            <td>
                <input type="number" name="base[search]" value="<?=$modx->config['search']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Исключить страницы из хлебных крошек:<br><small class="text-muted">[(bread_ignoreIds)]</small></th>
            <td>
                <input type="text" name="base[bread_ignoreIds]" value="<?=$modx->config['bread_ignoreIds']?>" class="form-control"/>
            </td>
        </tr>
        <!--<tr>
            <th class="vertical-align"> заказ:<br><small class="text-muted">[(qqq)]</small></th>
            <td>
                <input type="number" name="base[qqq]" value="<?=$modx->config['qqq']?>" class="form-control"/>
            </td>
        </tr>-->

<!--        <tr>
            <th class="vertical-align">Авто регистрация при покупке:<br><small class="text-muted">[(auto_create_account)]</small></th>
            <td>
                <input type="hidden" name="base[auto_create_account]" value="0"/>
                <input type="checkbox" name="base[auto_create_account]" value="1" <?=$modx->config['auto_create_account'] == 1 ? 'checked' : '' ?> class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Личный кабинет:<br><small class="text-muted">[(webuser_account)]</small></th>
            <td>
                <input type="number" name="base[webuser_account]" value="<?=$modx->config['webuser_account']?>" class="form-control"/>
            </td>
        </tr>
        <tr>
            <th class="vertical-align">Страница Авторизации:<br><small class="text-muted">[(webuser_login)]</small></th>
            <td>
                <input type="number" name="base[webuser_login]" value="<?=$modx->config['webuser_login']?>" class="form-control"/>
            </td>
        </tr> -->
    </table>
    <div id="actions">
        <ul class="actionButtons">
            <li id="Button1" class="transition">
                <a href="#" class="primary" onclick="documentDirty=false;document.getElementById('editForm').submit();"><i class="fa fa-save"></i>&emsp;Сохранить</a>
            </li>
        </ul>
    </div>
</form>