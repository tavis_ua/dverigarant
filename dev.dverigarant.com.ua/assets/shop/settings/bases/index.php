<?php
/**
 * Name: Общие
 * Icon: compass
 * Version: 1
 */

$isNews = (bool)$modx->db->getValue($modx->db->query("SHOW TABLES LIKE '".str_replace('`', '', end(explode('.', $modx->getFullTableName('a_news'))))."'"));
if (is_array($_POST['base']) && count($_POST['base']) > 0) {
    foreach($_POST['base'] as $key => $value) {
        $modx->db->query("REPLACE INTO `modx_system_settings` (setting_value, setting_name) VALUES ('".$modx->db->escape($value)."', '".$key."')");
    }
    $modx->clearCache('full');
    die(header("Location: index.php?a=250"));
}
$tpl = "bases.tpl";