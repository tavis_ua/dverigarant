<h2 class="muted">Шаблоны писем</h2>
<table class="table table-condensed table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align:center;">Название</th>
            <th width="100px" style="text-align:center;">Идентификатор</th>
            <th width="110px" style="text-align:center;">Настройка</th>
            <th width="<?=count($languages)*42;?>px" style="text-align:center;">Предпросмотр</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($templates as $template): ?>
        <tr id="<?=$template['mail_name']?>">
            <td class="vertical-align"><b><?=$template['mail_title']?></b></td>
            <td class="vertical-align"><b><?=$template['mail_name']?></b></td>
            <td><a href="<?=$url?>templates&c=edit&i=<?=$template['mail_name']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a></td>
            <td>
                <?php foreach ($languages as $lang): ?>
                    <a href="<?=$url?>templates&c=preview&i=<?=$template['mail_name']?>&lang=<?=$lang;?>" class="btn btn-primary" target="_blank"><?=strtoupper($lang);?></a>
                <?php endforeach; ?>
            </td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>templates&c=add"><i class="fa fa-plus-square"></i>&emsp;Создать шаблон</a>
        </li>
        <li id="Button4" class="transition">
            <a href="<?=$url?>templates&c=edit&i=meta"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&emsp;Редактировать Мета</a>
        </li>
    </ul>
</div>