<?php if (!isset($template[$modx->config['lang']]['mail_name']) || $template[$modx->config['lang']]['mail_name'] == ''): ?>
    <form id="editForm" action="<?=$url?>templates&c=save" method="post">
        <h2 class="muted">Добавление <small>шаблона письма</small></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>templates&c=update" method="post">
        <h2 class="muted">Редактирование <small>(<?=$template[$modx->config['lang']]['mail_title'];?>)</small></h2>
<?php endif; ?>
        <table class="table table-condesed">
            <tr>
                <th width="200px">Название шаблона</th>
                <td><input type="text" maxlength="32" name="template[mail_title]" <?=($template[$modx->config['lang']]['mail_name'] == "meta" ? "readonly" : "")?> value="<?=$template[$modx->config['lang']]['mail_title']?>" class="form-control"></td>
            </tr>
            <tr>
                <th>Идентификатор шаблона</th>
                <td><input type="text" maxlength="16" name="template[mail_name]" <?=($template[$modx->config['lang']]['mail_name'] == "meta" ? "readonly" : "")?> value="<?=$template[$modx->config['lang']]['mail_name']?>" class="form-control"></td>
            </tr>
        </table>
        <?php foreach ($languages as $lang): ?>
            <br />
            <h4><b>Версия для языка: <kbd><?=$lang?></kbd></b></h4>
            <input type="text" name="template[mail_subject][<?=$lang?>]" placeholder="Тема письма" class="form-control" value="<?=$template[$lang]['mail_subject']?>">
            <br />
            <textarea name="template[mail_template][<?=$lang?>]" id="<?=$lang?>_content" class="form-control" rows="10"><?=($template[$lang]['mail_template'] == "" ? "Содержание шаблона" : $template[$lang]['mail_template'] )?></textarea>
        <?php endforeach; ?>
        <?php if ($_GET['tpl'] == "meta"): ?>
            <p class="text-danger">"В META-шаблоне обязательно нужно предопределить плейсхолдер <code>{body}</code> который будет заменяться на содержимое встраиваемого подшаблона"</p>
        <?php endif; ?>
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;document.getElementById('editForm').submit();"><i class="fa fa-save"></i>&emsp;Сохранить</a>
                </li>
            </ul>
        </div>
    </form>
    <?=$code_mirror;?>