<?php
/**
 * Name: Шаблоны писем
 * Icon: envelope
 * Version: 1
 */

switch ($controller) {
    case "add" :
        $code_mirror = $shop->CodeMirror("template[mail_template]", "300px");
        $tpl         = "template.tpl";
        break;
    case "edit" :
        $code_mirror = $shop->CodeMirror("template[mail_template]", "300px");
        $template    = $shop->getMailTemplate($_GET['i']);
        $tpl         = "template.tpl";
        break;
    case "preview" : // Предпросмотр письма
        $lang  = $_GET['lang'] ? $_GET['lang'] : $modx->config['lang'];
        $currency = explode("|", $modx->config['shop_curr_code']);
        $badge = explode("|", $modx->config['shop_curr_badge']);
        $modx->config['currbadge'] = $badge[array_search($_SESSION['currency'], $currency)];
        $meta  = $shop->getMailTemplate("meta");
        $tpl   = $shop->getMailTemplate($_GET['i']);
        $out  = str_replace('{body}', $tpl[$lang]['mail_template'], $meta[$lang]['mail_template']);
        $out  = str_replace('[!', '[[', $out);
        $out  = str_replace('!]', ']]', $out);
        if (strpos($out, '{items}') !== false || strpos($out, '{product}') !== false) {
            $modx->config['lang'] = $lang;
            $items = $modx->runSnippet('Shop', ['get' => 'getCatalog', 'limit' => '5', 'tpl' => 'mail_order_item']);
            $out  = str_replace('{items}', $items, $out);
            $out  = str_replace('{product}', $items, $out);
        }
        $out  = $modx->parseDocumentSource($modx->rewriteUrls($out));
        die($out);
        break;
    case "save":
    case "update":
        $_POST['template'] = $shop->setMailTemplate($_POST['template']);
        $modx->clearCache();
        die(header("Location: ".$url."templates"));
    default:
        $templates = $modx->db->makeArray(
            $modx->db->query("
                        SELECT DISTINCT 
                            `mail_name`,
                            `mail_title`
                        FROM `modx_a_mail_templates` 
                        WHERE `mail_name` != 'meta'
                        ORDER BY `mail_title` ASC
                    "));
        $tpl = "templates.tpl";
        break;
}