<script type="text/javascript">
    document.title = "<?=$res['title']?> <?=$res['version']?>";
</script>
<h1 class="pagetitle">
    <span class="pagetitle-icon"><i class="fa fa-cogs"></i></span>
    <span class="pagetitle-text"><?=$_lang['shop_settings']?></span>
</h1>
<div class="sectionBody">
    <div class="dynamic-tab-pane-control tab-pane">
        <div class="tab-row">
            <?php foreach ($modules as $module => $value): ?>
                <h2 class="tab<?=($bootstrap==$module?' selected':'');?>">
                    <a href="<?=$url?><?php echo $module; ?>">
                        <span><i class="fa fa-<?php echo trim($value['Icon']);?>"></i>&nbsp;<?php echo trim($value['Name']); ?></span>
                    </a>
                </h2>
            <?php endforeach; ?>
            <!-- <h2 class="tab<?=($bootstrap=='payments'?' selected':'');?>"><a href="<?=$url?>payments"><span><i class="fa fa-credit-card"></i> Системы оплат</span></a></h2> -->
        </div>
        <div class="tab-page" id="tabContents">
            <?=$res['content']?>
        </div>
    </div>
</div>