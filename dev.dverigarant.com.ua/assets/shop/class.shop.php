<?php
    if (!class_exists('Shop')) {
        /**
         * Class Shop
         */
        class Shop
        {
            private $modx = null;
            private $db   = null;
            
            /**
             * Shop constructor.
             *
             * @param object $modx
             */
            public function __construct($modx)
            {
                $this->modx = $modx;
                $this->db   = $modx->db;
            }

	        // Установить кеш
	        public function set_cache($file, $data)
	        {
		        $filename = MODX_BASE_PATH."assets/cache/shop/" . $file . '.php';

		        if (file_exists($filename)) {
			        unlink(MODX_BASE_PATH . "assets/cache/shop/" . $file . '.php');
		        }

		        $fp = fopen( $filename, 'wb+' );
		        fwrite( $fp, serialize( $data ) );
		        fclose( $fp );

		        @chmod( $filename, 0666 );
	        }

	        // Прочитать кеш
	        public function get_cache($file)
	        {
		        $filename = MODX_BASE_PATH."assets/cache/shop/" . $file . '.php';

		        if (!file_exists(MODX_BASE_PATH."assets/cache/shop")) {
			        mkdir(MODX_BASE_PATH."assets/cache/shop");
			        chmod(MODX_BASE_PATH."assets/cache/shop", 0777);
		        }

		        if( ! @filesize( $filename ) ) {
			        return false;
		        }

		        return unserialize( file_get_contents( $filename ) );

	        }

            //Натуральная сортировка массива по полю
	        public function natSortByField($arr = [], $field = 'id') {
		        if(is_array($arr) && count($arr) > 0) {
			        usort($arr, function ($a, $b) use ($field) {
				        if ($a[$field] == $b[$field]) {
					        return 0;
				        }
				        return ($a[$field] < $b[$field]) ? -1 : 1;
			        });
			        return $arr;
		        }
            }


            public function getDeliveryFields() {
                $delivery_fields = $this->db->makeArray($this->db->query("
                    SELECT 
                        *,
                        (SELECT GROUP_CONCAT(CONCAT(`value`, '|', `id`) SEPARATOR '__') FROM `modx_a_delivery_field_values` WHERE `field_id` = `df`.`id` AND `lang` = '{$this->modx->config['lang_default']}') AS 'field_values'
                    FROM `modx_a_delivery_fields` `df`
                    WHERE `df`.`active` = 1
                "));

                if(is_array($delivery_fields) && count($delivery_fields) > 0) {
                    foreach ($delivery_fields as $delivery_field) {
                        if(!empty(trim($delivery_field['field_values']))) {
                            $dfvs = explode('__', $delivery_field['field_values']);
                            if(is_array($dfvs) && count($dfvs) > 0) {
                                foreach ($dfvs as $dfv) {
                                    $dfv_val = '';
                                    $dfv_id = 0;
                                    $dfv_val = explode('|', $dfv)[0];
                                    $dfv_id = explode('|', $dfv)[1];
                                    $delivery_field['values'][$dfv_id] = $dfv_val;
                                }
                            }
                        }
                        $d_fields[$delivery_field['delivery_id']][] = $delivery_field;
                    }
                }
                return $d_fields;
            }

            public function getNPFields($api_key, $lang, $only_cities = 0) {
            	if(!empty(trim($api_key))) {
            		$delivery_fields = '';
		            $new_post = [];
		            include_once MODX_BASE_PATH.'assets/shop/NovaPoshtaApi2.class.php';
		            $np = new NovaPoshtaApi2($api_key, $lang);
		            // Список городов
		            $cities = $np->getCities(0);
		            if($cities['success'] == true) {
			            foreach ($cities['data'] as $key => $value) {
				            $value['name'] = isset($value['Description'.ucfirst(strtolower($lang))]) ? $value['Description'.ucfirst(strtolower($lang))] : $value['Description'];
				            $cities['items'] .= $this->modx->parseChunk('tpl_new_post_item', $value);
				            if ($key == 0) {
					            $city = $value['Ref'];
					            $new_post['first_name'] = $value['city_name'];
				            }
			            }
			            $cities['name'] = 'checkout[delivery][city]';
			            $cities['field_id'] = 'novaPoshta-city';
			            $delivery_fields .= $this->modx->parseChunk('tpl_new_post_outer', $cities);
		            }
		            // Список отделений первого города
		            $departaments = $np->getWarehouses($city);
		            if($departaments['success'] == true) {
			            foreach ($departaments['data'] as $key => $value) {
				            $value['name'] = isset($value['Description'.ucfirst(strtolower($lang))]) ? $value['Description'.ucfirst(strtolower($lang))] : $value['Description'];
				            $departaments['items'] .= $this->modx->parseChunk('tpl_new_post_item', $value);
				            if ($key == 0) {
					            $departaments['first_name'] = $value['name'];
				            }
			            }
		            }
		            $departaments['name']       = 'checkout[delivery][departament]';
		            $departaments['field_id']   = 'novaPoshta-departament';
		            $delivery_fields .= $this->modx->parseChunk('tpl_new_post_outer', $departaments);
		            return $delivery_fields;
	            } else {
            		return false;
	            }
            }

	        /**
	         * reportUsers
	         * Получить все записи из БД для оповещения пользователей по ID товара
	         *
	         * @param int $product_id ID товара
	         * @return array
	         */
	        public function reportUsers($option){
		        $need_report_users = $this->db->makeArray($this->db->query("SELECT * FROM `modx_a_report_admission` WHERE `option_id` = " . $option['id']));
		        $product = $this->getProduct($option['product']);
		        $option['product_name'] = $product['product_name'];
		        $option['product_url'] = $product['product_url'];
		        $o_f_values = $this->getOptionFilterValues($option['id']);
		        $mail_data['filter_values'] = '';
		        if ($o_f_values) {
			        foreach ($o_f_values as &$o_f_value) {
				        $mail_data['filter_values'] .= '<strong>'. $o_f_value['filter_name'] . ':<strong></strong><strong>'. $o_f_value['filter_value'] . ':<strong></strong><br>';
			        }
		        }
		        $product['product_cover'] = (isset($product['product_cover']) && !empty(trim($product['product_cover']))) ? "assets/products/" .  $product['product_id'] . "/" . $product['product_cover'] : '/assets/snippets/phpthumb/noimage.png';

		        $mail_data['product'] = $this->modx->parseDocumentSource($this->modx->parseChunk("mail_report_admission", $product));
		        $mail_data['product_url'] = $this->modx->config['site_url'] . $this->modx->config['lang'] . '/' . $product['product_url'];
		        $mail_data['product_name'] = $product['product_name'];

		        if(is_array($need_report_users) && count($need_report_users) > 0) {
			        foreach ($need_report_users as $user) {
				        if (!empty(trim($user["user_email"]))) {
					        //Отправка письма на почту о поступлении товара
					        $this->sendMail($user["user_email"], "admission", $mail_data);
				        }
			        }
			        $this->clearReportUsers($option['id']);
		        }
		        return true;
	        }

	        /**
	         * clearReportUsers
	         * Очистить таблицу оповещения пользователей с указанным товаром (ID опции)
	         *
	         * @param int $oid ID опции
	         */
	        public function clearReportUsers($oid){
		        $this->db->query("DELETE FROM `modx_a_report_admission` WHERE `option_id` = " . $oid);
	        }

	        /**
	         * getOptionFilterValues
	         * Получить значения фильтров по ID опции
	         *
	         * @param int $oid ID опции
	         * @return array
	         */
	        public function getOptionFilterValues($oid){
		        $ofvalues_query = "
	                SELECT
	                    `f`.`filter_id` AS 'filter_id',
	                    `f`.`filter_url` AS 'filter_url',
	                    `fs`.`filter_name` AS 'filter_name',
	                    `fv`.`value_id` AS 'filter_value_id',
	                    `fv`.`filter_value_{$this->modx->config['lang']}` AS 'filter_value',
	                    `fv`.`image` AS 'filter_value_image',
	                    `fv`.`filter_url` AS 'filter_value_url',
	                    `pfv`.`product_id` AS 'p_id'
	                FROM `modx_a_filter_values` `fv`
	                INNER JOIN `modx_a_filters` `f` ON `f`.`filter_id` = `fv`.`filter_id` AND `f`.`filter_product_active` = 1
	                INNER JOIN `modx_a_filter_strings` `fs` ON `fs`.`filter_id` = `f`.`filter_id` AND `fs`.`string_locate` = '{$this->modx->config['lang']}'
	                INNER JOIN `modx_a_product_filter_values` `pfv` ON `pfv`.`value_id` = `fv`.`value_id`
	                WHERE `pfv`.`product_id` = '{$oid}'
	            ";
		        $ofvalues = $this->db->makeArray($this->db->query($ofvalues_query));
		        if (is_array($ofvalues) && count($ofvalues) > 0) {
		        	return $ofvalues;
		        }
		        return null;
	        }
            
            /**
             * dd
             * Дебаг переменной с прерыванием выполнения кода
             * помещает в себя любое количество переменных
             *
             * @param mixed $var Переменная или список переменных
             * через запятую для отображения их содержимого
             */
            public static function dd(...$var)
            {
//                if ($_COOKIE['debug'] == 123) {
                    echo "<pre>";
                    foreach ($var as &$v) {
                        var_dump($v);
                    }
                    die;
//                }
            }
            
            /**
             * console_log
             * Запись переменных в файл без прерывания выполнения кода
             * помещает в себя любое количество переменных
             *
             * @param mixed $var Переменная или список переменных
             * через запятую для отображения их содержимого
             */
            public static function console_log() {
                if (!func_num_args()) {
                    return; # Аргументы не переданы
                }

                $folder = MODX_BASE_PATH . 'assets/cache/log/';

                if (!file_exists($folder)) {
                    mkdir($folder);
                    chmod($folder, 0777);
                }

                $log_name = $folder . date("Y-m-d") . '.log'; // Раскидываем по дате
                $f = fopen($log_name, "a");
                fwrite($f, '['.date("Y-m-d H:i:s").'] ');
                foreach (func_get_args() as $arg) {
                    if (is_bool($arg)) {
                        $s = $arg ? 'TRUE' : 'FALSE';
                    } elseif (is_array($arg) || is_object($arg)) {
                        $s = print_r($arg, true);
                    } else {
                        $s = $arg;
                    }
                    fwrite($f, $s.' '); # вывод аргументов разделяется пробелом
                }
                fwrite($f, "\n");
                fclose($f);
            }

            /**
             * is_json
             * Проверка json строки на валидность с возвратом массива значений
             *
             * @param string $json Входные данные
             * @param bool $return Возвращать массив данных
             * @return bool|mixed
             */
            public function is_json($json, $return = true)
            {
                $data = json_decode($json, true);
                return (json_last_error() == JSON_ERROR_NONE) ? ($return ? $data : TRUE) : FALSE;
            }

            /**
             * renderMenu
             * Отрисовка дерева меню (рекурсивно)
             *
             * @param array $data Многомерный массив (дерево меню)
             * @param string $tpl Чанк пункта меню
             * @param string $tplSubMenu Чанк пункта меню содержащий вложенные элементы
             * @param string $tplActive Чанк активного пункта меню
             * @param string $tplSubMenuActive Чанк активного пункта меню содержащий вложенные элементы
             */
            public function renderMenu($data, $tpl, $tplSubMenu, $tplActive, $tplSubMenuActive){
                $string = '';
                $i = 1;
                foreach($data as $item){
                    if ($item['published'] == 0) {
                        continue;
                    }
                    $item['i'] = $i;
                    $item['link'] = $this->modx->makeUrl((int)$item['id'], '', '', 'null');

                    $item['menutitle'] = $item['menutitle'] != '' ? $item['menutitle'] : $item['pagetitle'];
                    if(is_array($item['childs']) && count($item['childs']) > 0){
                        if ($item['dept'] = 2 && count($item['childs']) > 0) {
                            $item['have_three'] == 1;
                        }
                        //Рекурсия
                        $item['submenu'] = $this->renderMenu($item['childs'], $tpl, $tplSubMenu, $tplActive, $tplSubMenuActive);
                        if($this->modx->documentIdentifier == $item['id']) {
                            $string .= $this->modx->parseChunk($tplSubMenuActive, $item);
                        } else {
                            $string .= $this->modx->parseChunk($tplSubMenu, $item);
                        }
                    } else {
                        if($this->modx->documentIdentifier == $item['id']) {
                            $string .= $this->modx->parseChunk($tplActive, $item);
                        } else {
                            $string .= $this->modx->parseChunk($tpl, $item);
                        }
                    }
                    $i++;
                }
                return $string;
            }
            
            /**
             * googleTranslate
             * Получить перевод строки с помощью Google translate
             *
             * @param string $text Строка для перевода
             * @param string $source язык оригинала
             * @param string $target требуемый язык
             * @return string Результаты перевода
             */
            public function googleTranslate($text, $source = 'ru', $target = 'uk')
            {
                if ($source == 'ua') {$source = 'uk';}
                if ($target == 'ua') {$target = 'uk';}

                if ($source == $target) {
                    return $text;
                }
                // Google translate URL
                $url = 'https://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=uk-RU&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e';
                $fields_string = 'sl=' . urlencode($source) . '&tl=' . urlencode($target) . '&q=' . urlencode($text) ;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 3);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                //curl_setopt($ch, CURLOPT_HEADER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                curl_setopt($ch, CURLOPT_USERAGENT, 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1');

                $result = curl_exec($ch);
                $result = json_decode($result, TRUE);

                $out = '';
                if (isset($result['sentences'])) {
                    foreach ($result['sentences'] as $s) {
                        $out .= isset($s['trans']) ? $s['trans'] : '';
                    }
                } else {
                    $out = 'No result';
                }

                return $out;
            }

            /**
             * getOrderList
             * Получить список всех заказов из БД
             *
             * @param integer $limit Количество записей для выборки
             * @param string $search Дополнительное условие для фильтрации
             * @return mixed
             */
            public function getOrderList($search = '')
            {
                $limit  = 30;
                $page   = isset($_REQUEST['p']) ? ((int)($_REQUEST['p'])-1) * $limit : 0;
                $search = $this->db->escape(trim($_GET['search']));
                if ($search != "") {
                    $search = "WHERE `o`.`order_code` = '".$search."'";
                }
                $query  = "
                SELECT SQL_CALC_FOUND_ROWS
                    `o`.`order_id`,
                    `o`.`order_created`,
                    `o`.`order_client`,
                    `o`.`order_cost`,
                    `o`.`order_delivery_cost`,
                    `o`.`order_currency`,
                    `o`.`order_status`,
                    `o`.`order_status_pay`,
                    `o`.`order_currency`,
                    `o`.`order_code`,
                    `o`.`order_buy_click`
                FROM `modx_a_order` o
                {$search}
                ORDER BY `o`.`order_id` DESC
                LIMIT ".$page.", ".$limit;
                $res = $this->db->query($query);
                $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
                $this->modx->setPlaceholder("max_items", $pages['cnt']);
                $this->modx->setPlaceholder("limit_items", $limit);
                return $this->db->makeArray($res);
            }
            
            /**
             * getOrder
             * Получить основную информацию о заказе по его ID
             *
             * @param integer $pid ID заказа
             * @return array or null
             */
            public function getOrder($pid)
            {
                $res = null;
                if ($pid = (int)$pid) {
                    $res = $this->db->getRow($this->db->query("
                    SELECT *,
                      (SELECT code_mod FROM `modx_a_codes` WHERE code = o.order_code) AS 'order_discount',
                      (SELECT `pagetitle_{$this->modx->config['lang_default']}` FROM `modx_site_content` WHERE `id` = o.order_payment) AS 'order_pay'
                    FROM `modx_a_order` o
                    WHERE o.order_id = ".$pid
                    ));
                }
                return $res;
            }
            
            /**
             * getOrderProducts
             * Получить информацию о товарах заказа по его ID
             *
             * @param integer $pid ID заказа
             * @return array or null
             */
            public function getOrderProducts($pid)
            {
                $res = null;
                if ($pid = (int)$pid) {
                    $res = $this->db->makeArray($this->db->query("
                    SELECT *,
                        (SELECT order_currency FROM `modx_a_order` WHERE order_id = op.order_id) AS 'order_currency',
                        (SELECT product_name FROM `modx_a_product_strings` WHERE product_id = op.product_id AND string_locate = '".$this->modx->config['lang']."') AS 'product_name'
                    FROM `modx_a_order_products` op
                    WHERE op.order_id = ".$pid
                    ), 'option_id');
                    foreach ($res as $key=>$product){
                        //Дополнительная комплектация
                        $sql = "select id, name, price from modx_a_products_parameters where ID in ({$res[$key]['addEqp']})";
                        $resEqp = $this->db->makeArray($this->db->query($sql));
                        $res[$key]['addEqp'] = [];
                        foreach ($resEqp as $eqp){
                            $res[$key]['addEqp'][$eqp['id']] = ['name'=>$eqp['name'], 'price'=>$eqp['price']];
                        }
                        //Комплектация
                        $sql = "select modx_a_products_equipment_values.id, modx_a_products_equipment.name param_name, modx_a_products_equipment_values.name value, modx_a_products_equipment_values.price 
                              from modx_a_products_equipment_values
                              INNER JOIN modx_a_products_equipment on modx_a_products_equipment_values.eqp_id = modx_a_products_equipment.id
                            where modx_a_products_equipment_values.ID in ({$res[$key]['Eqp']})";
                        $resEqp = $this->db->makeArray($this->db->query($sql));
                        $res[$key]['Eqp'] = [];
                        foreach ($resEqp as $eqp){
                            $res[$key]['Eqp'][$eqp['id']] = ['name'=>$eqp['param_name'], 'value'=>$eqp['value'], 'price'=>$eqp['price']];
                        }
                    }
                }
                return $res;
            }
            
            /**
             * getOrderDeliveries
             * Получить список доставок товара
             *
             */
            public function getOrderDeliveries()
            {
                $res = [];
                $query = $this->db->makeArray($this->db->query("
                SELECT delivery_id, delivery_title_ru FROM `modx_a_delivery`
            "));
                if (is_array($query)) {
                    foreach ($query as $q) {
                        $res[$q['delivery_id']] = $q['delivery_title_ru'];
                    }
                }
                return $res;
            }
            
            /**
             * getSelfCities
             * Получить список городов самовывоза
             *
             */
            public function getSelfCities()
            {
                $res = [];
                $query = $this->db->makeArray($this->db->query("
                SELECT setting_id, setting_name_ru
                FROM `modx_a_delivery_settings`
                WHERE setting_key_all = 'city' AND setting_delivery = 1
                ORDER BY setting_name_ru ASC
            "));
                if (is_array($query)) {
                    foreach ($query as $q) {
                        $res[$q['setting_id']] = $q['setting_name_ru'];
                    }
                }
                return $res;
            }
            
            /**
             * getProductList
             * Получить список всех товаров из БД
             *
             * @param integer $limit Количество записей для выборки
             * @return mixed
             */
            public function getProductList($search = [])
            {
                $where = [];
                if (isset($search['srch'])) {
                    $where[] = "(`p`.`product_id` = '".(int)$search['srch']."' OR `p`.`product_id` IN (SELECT `product_id` FROM `modx_a_product_strings` WHERE `string_locate` = '{$this->modx->config['lang_default']}' AND `product_name` LIKE '%".$this->db->escape($search['srch'])."%'))";
                }
                if (isset($search['filter_status'])) {
                    $where[] = "`p`.`product_status` = '".(int)$search['filter_status']."'";
                }
                if (isset($search['filter_publish'])) {
                    $where[] = "`p`.`product_active` = '".(int)$search['filter_publish']."'";
                }
                if (isset($search['filter_availability'])) {
                    $where[] = "`p`.`product_availability` = '".(int)$search['filter_availability']."'";
                }

                if (count($where)) {
                    $where = 'WHERE '.implode(' AND ', $where);
                } else {
                    $where = '';
                }

                $limit = 30;
                $page  = isset($_REQUEST['p']) ? ((int)$_REQUEST['p'] - 1) * $limit : 0;
                $query = "
                SELECT SQL_CALC_FOUND_ROWS
                    `p`.*,
                    (SELECT `product_name` FROM `modx_a_product_strings` WHERE `string_locate` = '".$this->modx->config['lang']."' AND `product_id` = `p`.`product_id`) AS 'product_name',
                    (SELECT `price` FROM `modx_a_product_options` WHERE `product` = `p`.`product_id` AND `default_option` = '1') AS 'price',
                    (SELECT `image_file` FROM `modx_a_product_images` WHERE `image_use` = 'cover' AND `image_product_id` = `p`.`product_id` LIMIT 1) AS 'product_cover'
                FROM `modx_a_products` p
                {$where}
                ORDER BY `p`.`product_updated` DESC
                LIMIT ".$page.", ".$limit;
                $res = $this->db->query($query);
                $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
                $this->modx->setPlaceholder("max_items", $pages['cnt']);
                $this->modx->setPlaceholder("limit_items", $limit);
                return $this->db->makeArray($res);
            }
            
            /**
             * getProduct
             * Получить основную информацию о продукте по его ID
             *
             * @param integer $pid ID продукта
             * @return array or null
             */
            public function getProduct($pid, $lang = null)
            {
            	$lang = !$lang ? $this->modx->config['lang_default']: $lang;
                $res = null;
                if ($pid = (int)$pid) {
                    $res = $this->db->getRow($this->db->query("
                    SELECT
                        p.*,
                        (SELECT GROUP_CONCAT(modx_id SEPARATOR '|') FROM modx_a_product_categories WHERE product_id = p.product_id) AS 'product_categories',
                        (SELECT `product_name` FROM `modx_a_product_strings` WHERE `product_id` = `p`.`product_id` AND `string_locate` = '{$lang}') AS 'product_name',
                        (SELECT `image_file` FROM `modx_a_product_images` WHERE `image_use` = 'cover' AND `image_product_id` = `p`.`product_id` LIMIT 1) AS 'product_cover'
                    FROM `modx_a_products` p
                    WHERE p.product_id = ".$pid
                    ));
                    $res['product_categories']  = explode('|', $res['product_categories']);
                    $res['product_recommender'] = explode(',', $res['product_recommender']);
                    $res['product_look_like'] = explode(',', $res['product_look_like']);
                }
                return $res;
            }
            
            /**
             * getProductString
             * Получить переводы для продукта
             *
             * @param integer $product_id ID продукта
             * @param string $lang Язык
             * @return array
             */
            public function getProductString($product_id, $lang = 'ru')
            {
                return $this->db->getRow($this->db->query("SELECT * FROM `modx_a_product_strings` WHERE product_id = ".$product_id." AND string_locate = '".$lang."'"));
            }
            
            /**
             * saveProduct
             * Сохранить новый товар
             *
             * @param array $product продукт
             * @return integer
             */
            public function saveProduct($product)
            {
                $alias = $product['product_url'] != '' ? $this->validateUrl($product['product_url']) : $this->setUrl($product[$this->modx->config['lang_default']]['product_name'], "modx_a_products", "product_url");
                $alias = str_replace('/', '-', $alias);
                $product['product_price']       = number_format(floatval($product['product_price']), 2, '.', '');
                $product['product_price_old']   = number_format(floatval($product['product_price_old']), 2, '.', '');
                $product['product_weight']      = number_format(floatval($product['product_weight']), 2, '.', '');
                $product['product_recommender'] = is_array($product['product_recommender']) ? implode(",", $product['product_recommender']) : "";
                $product['product_look_like'] = is_array($product['product_look_like']) ? implode(",", $product['product_look_like']) : "";
                $this->db->query("
                    INSERT INTO `modx_a_products` SET
                        product_active              = '".(int)$product['product_active']."',
                        product_availability        = '".(int)$product['product_availability']."',
                        product_type                = '".(int)$product['product_type']."',
                        product_main_slider_new     = '".(int)$product['product_main_slider_new']."',
                        product_main_slider_discont = '".(int)$product['product_main_slider_discont']."',
                        product_main_slider_popular = '".(int)$product['product_main_slider_popular']."',
                        product_status              = '".(int)$product['product_status']."',
                        product_rating              = '".(int)$product['product_rating']."',
                        product_category            = '".(int)$product['product_category']."',
                        product_url                 = '".$alias."',
                        product_code                = '".$this->db->escape($product['product_code'])."',
                        product_discont             = '".$this->db->escape($product['product_discont'])."',
                        product_price               = '".(float)$product['product_price']."',
                        product_price_old           = '".(float)$product['product_price_old']."',
                        product_weight              = '".$product['product_weight']."',
                        product_recommender         = '".$product['product_recommender']."',
                        product_look_like           = '".$product['product_look_like']."'
                ");
                $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                $this->setProductCategories($result['last_id'], $product['product_categories']);
                $this->setProductFilters($result['last_id'], $product['product_filters']);

	            $post = $_POST['options'];
	            $post['default_option'] = 1;
	            $post['product_code'] = $product['product_code'];
	            $post['price'] = isset($post['price']) && $post['price'] != '' ? $post['price'] : 0;
	            $post['price_old'] = isset($post['price_old']) && $post['price_old'] != '' ? $post['price_old'] : 0;
	            $post['ru_name'] = isset($product['ru']['product_name']) && $this->db->escape($product['ru']['product_name']) != '' ? $this->db->escape($product['ru']['product_name']) : '';
	            $post['ua_name'] = isset($product['ua']['product_name']) && $this->db->escape($product['ua']['product_name']) != '' ? $this->db->escape($product['ua']['product_name']) : '';

	            unset($post['id']);
	            $id = $this->db->insert(['active' => 1, 'product' => $result['last_id']], 'modx_a_product_options');
	            $this->db->update($post, 'modx_a_product_options', "`id` = '" . $id . "'");
                return $result['last_id'];
            }
            
            /**
             * updateProduct
             * Обновить существующий товар
             *
             * @param array $product продукт
             * @return mixed
             */
            public function updateProduct($product)
            {
                $alias = $product['product_url'] != '' ? $this->validateUrl($product['product_url']) : $this->setUrl($product[$this->modx->config['lang_default']]['product_name'], "modx_a_products", "product_url");
                $alias = str_replace('/', '-', $alias);
                $product['product_price']       = number_format(floatval($product['product_price']), 2, '.', '');
                $product['product_price_old']   = number_format(floatval($product['product_price_old']), 2, '.', '');
                $product['product_weight']      = number_format(floatval($product['product_weight']), 2, '.', '');
                $product['product_recommender'] = is_array($product['product_recommender']) ? implode(",", $product['product_recommender']) : "";
                $product['product_look_like'] = is_array($product['product_look_like']) ? implode(",", $product['product_look_like']) : "";
                $this->setProductCategories($product['product_id'], $product['product_categories']);
                //$this->setProductFilters($product['product_id'], $product['product_filters']);

	            if (is_array($_POST['options']) && count($_POST['options']) > 0) {
		            $post = $_POST['options'];
		            if(!$post['id']) {
                        $id = $this->db->insert(['active' => 1, 'product' => $product['product_id']], 'modx_a_product_options');
                    }

		            $post['product'] = $product['product_id'];
//		            $report_users = $this->reportUsers($post);

                    $this->setProductFilters($post['id'], $product['product_filters']);
		            unset($post['id']);
		            $this->db->update($post, 'modx_a_product_options', "`id` = '" . $id . "'");
	            }

                return $this->db->query("
                    UPDATE `modx_a_products` SET
                        product_active              = '".(int)$product['product_active']."',
                        product_availability        = '".(int)$product['product_availability']."',
                        product_type                = '".(int)$product['product_type']."',
                        product_main_slider_new     = '".(int)$product['product_main_slider_new']."',
                        product_main_slider_discont = '".(int)$product['product_main_slider_discont']."',
                        product_main_slider_popular = '".(int)$product['product_main_slider_popular']."',
                        product_status              = '".(int)$product['product_status']."',
                        product_rating              = '".(int)$product['product_rating']."',
                        product_category            = '".(int)$product['product_category']."',
                        product_url                 = '".$alias."',
                        product_code                = '".$this->db->escape($product['product_code'])."',
                        product_discont             = '".$this->db->escape($product['product_discont'])."',
                        product_price               = '".(float)$product['product_price']."',
                        product_price_old           = '".(float)$product['product_price_old']."',
                        product_weight              = '".$product['product_weight']."',
                        product_recommender         = '".$product['product_recommender']."',
                        product_look_like           = '".$product['product_look_like']."',
                        product_updated             = NOW()
                    WHERE product_id = '".(int)$product['product_id']."'
                ");
            }

            public function updateProductOptions($option)
            {
                $option['price']       = number_format(floatval($option['price']), 2, '.', '');
                $option['price_old']   = number_format(floatval($option['price_old']), 2, '.', '');

                return $this->db->query("
                    UPDATE `modx_a_product_options` SET
                        `price`               = '".(float)$option['price']."',
                        `price_old`           = '".(float)$option['price_old']."',
                        `updated_at`             = NOW()
                    WHERE `id` = '".(int)$option['id']."'
                ");
            }
            
            /**
             * saveProductString
             * Обновить переводы товара
             *
             * @param array $product продукт
             * @param string $lang язык для переводов
             * @return mixed
             */
            public function saveProductString($product, $lang = 'ru')
            {
                return $this->db->query("
                    INSERT INTO `modx_a_product_strings` SET
                        string_locate           = '".$lang."',
                        product_id              = '".(int)$product['product_id']."',
                        product_name            = '".$this->db->escape($product[$lang]['product_name'])."',
                        product_introtext       = '".$this->db->escape($product[$lang]['product_introtext'])."',
                        product_additional      = '".$this->db->escape($product[$lang]['product_additional'])."',
                        product_description     = '".$this->db->escape($product[$lang]['product_description'])."',
                        product_content         = '".$this->db->escape($product[$lang]['product_content'])."',
                        product_seo_title       = '".$this->db->escape($product[$lang]['product_seo_title'])."',
                        product_seo_description = '".$this->db->escape($product[$lang]['product_seo_description'])."',
                        product_seo_content     = '".$this->db->escape($product[$lang]['product_seo_content'])."',
                        product_seo_keywords    = '".$this->db->escape($product[$lang]['product_seo_keywords'])."',
                        product_seo_canonical   = '".$this->db->escape($product[$lang]['product_seo_canonical'])."',
                        product_seo_robots      = '".$this->db->escape($product[$lang]['product_seo_robots'])."'
                    ON DUPLICATE KEY UPDATE
                        product_name            = '".$this->db->escape($product[$lang]['product_name'])."',
                        product_introtext       = '".$this->db->escape($product[$lang]['product_introtext'])."',
                        product_additional      = '".$this->db->escape($product[$lang]['product_additional'])."',
                        product_description     = '".$this->db->escape($product[$lang]['product_description'])."',
                        product_content         = '".$this->db->escape($product[$lang]['product_content'])."',
                        product_seo_title       = '".$this->db->escape($product[$lang]['product_seo_title'])."',
                        product_seo_description = '".$this->db->escape($product[$lang]['product_seo_description'])."',
                        product_seo_content     = '".$this->db->escape($product[$lang]['product_seo_content'])."',
                        product_seo_keywords    = '".$this->db->escape($product[$lang]['product_seo_keywords'])."',
                        product_seo_canonical   = '".$this->db->escape($product[$lang]['product_seo_canonical'])."',
                        product_seo_robots      = '".$this->db->escape($product[$lang]['product_seo_robots'])."'
                ");
            }
            
            /**
             * copyProduct
             * Скопировать товар
             *
             * @param $pid ID продукта
             * @return mixed
             */
            public function copyProduct($pid)
            {
                $last_id = $pid;
                if ((int)$pid > 0) {
                    // Копируем товар
                    $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_products` WHERE product_id = {$pid}");
                    $this->db->query("ALTER TABLE `products_".$pid."` MODIFY product_id INT NULL");
                    $this->db->query("UPDATE `products_".$pid."` SET product_id = NULL, product_active = 0, product_url = CONCAT_WS('-', product_url, 'duplicate')");
                    $this->db->query("INSERT INTO `modx_a_products` SELECT * FROM `products_".$pid."`");
                    $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                    $this->db->query("DROP TABLE `products_".$pid."`");
                    $last_id = $result['last_id'];
                    // Копируем переводы
                    $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_product_strings` WHERE product_id = {$pid}");
                    $this->db->query("ALTER TABLE `products_".$pid."` MODIFY string_id INT NULL");
                    $this->db->query("UPDATE `products_".$pid."` SET string_id = NULL, product_id = {$last_id}, product_name = CONCAT_WS(' ', 'Duplicate of', product_name)");
                    $this->db->query("INSERT INTO `modx_a_product_strings` SELECT * FROM `products_".$pid."`");
                    $this->db->query("DROP TABLE `products_".$pid."`");
                    // Копируем категории
                    $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_product_categories` WHERE product_id = {$pid}");
                    $this->db->query("UPDATE `products_".$pid."` SET product_id = {$last_id}");
                    $this->db->query("INSERT INTO `modx_a_product_categories` SELECT * FROM `products_".$pid."`");
                    $this->db->query("DROP TABLE `products_".$pid."`");
                    // Копируем опции
                    $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_product_options` WHERE product = {$pid}");
                    $this->db->query("ALTER TABLE `products_".$pid."` MODIFY id INT NULL");
                    $this->db->query("UPDATE `products_".$pid."` SET id = NULL, product = {$last_id}");
                    $this->db->query("INSERT INTO `modx_a_product_options` SELECT * FROM `products_".$pid."`");
//                    TODO:как то нужно скопировать фильтра вариацый, потом
//                    $optid = $this->db->getColumn('id', $this->db->query("SELECT `id` FROM `modx_a_product_options` WHERE product = {$pid}"));
//                    $last_optid = $this->db->getColumn('id', $this->db->query("SELECT `id` FROM `modx_a_product_options` WHERE product = {$last_id}"));
                    $this->db->query("DROP TABLE `products_".$pid."`");
                    // Копируем фильтры
                    $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_product_filter_values` WHERE product_id = {$pid}");
                    $this->db->query("UPDATE `products_".$pid."` SET product_id = {$last_id}");
                    $this->db->query("INSERT INTO `modx_a_product_filter_values` SELECT * FROM `products_".$pid."`");
                    $this->db->query("DROP TABLE `products_".$pid."`");
                    // Копируем фильтры опцый
//                    foreach ($last_optid as $v) {
//                        $this->db->query("CREATE TEMPORARY TABLE `products_".$pid."` AS SELECT * FROM `modx_a_product_filter_values` WHERE product_id = {$pid}");
//                        $this->db->query("UPDATE `products_".$pid."` SET product_id = {$last_id}");
//                        $this->db->query("INSERT INTO `modx_a_product_filter_values` SELECT * FROM `products_".$pid."`");
//                        $this->db->query("DROP TABLE `products_".$pid."`");
//                    }
                    // Копируем изображения
                    $images = $this->db->makeArray($this->db->query("SELECT * FROM `modx_a_product_images` WHERE image_product_id = {$pid}"));
                    if (is_array($images) && count($images) > 0) {
                        foreach ($images as $image) {
                            $imageFields = $this->db->makeArray($this->db->query("SELECT * FROM `modx_a_product_image_fields` WHERE image_image_id = {$image['image_id']}"));
                            unset($image["image_id"]);
                            $image["image_product_id"] = $last_id;
                            $imageQueries = [];
                            foreach ($image as $k => $v) {
                                $imageQueries[] = $k." = '".$v."'";
                            }
                            if (count($imageQueries) > 0) {
                                $imageQuery = "INSERT INTO `modx_a_product_images` SET ".implode(", ", $imageQueries);
                                $this->db->query($imageQuery);
                                $imgLastId = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                                $imgLastId = $imgLastId['last_id'];
                            }
                            if (is_array($imageFields) && count($imageFields) > 0) {
                                foreach ($imageFields as $imageField) {
                                    unset($imageField["image_field_id"]);
                                    $imageField["image_image_id"] = $imgLastId;
                                    $imageFieldsQueries = [];
                                    foreach ($imageField as $ik => $iv) {
                                        $imageFieldsQueries[] = $ik." = '".$iv."'";
                                    }
                                    if (count($imageFieldsQueries) > 0) {
                                        $imageFieldsQuery = "INSERT INTO `modx_a_product_image_fields` SET ".implode(", ", $imageFieldsQueries);
                                        $this->db->query($imageFieldsQuery);
                                    }
                                }
                            }
                        }
                    }
                    // Копируем файлы
                    $this->copyDirRecursive(MODX_BASE_PATH."assets/products/".$pid, MODX_BASE_PATH."assets/products/".$last_id);
                }
                return $last_id;
            }
            
            /**
             * deleteProduct
             * Удалить товар
             *
             * @param array $product продукт
             */
            public function deleteProduct($product_id)
            {
                $this->db->query("DELETE FROM `modx_a_products` WHERE product_id = '".(int)$product_id."' LIMIT 1");
                $this->db->query("DELETE FROM `modx_a_product_strings` WHERE product_id = '".(int)$product_id."'");
                $this->db->query("DELETE FROM `modx_a_product_images` WHERE image_product_id = '".(int)$product_id."'");
                $this->db->query("DELETE FROM `modx_a_product_options` WHERE product = '".(int)$product_id."'");
                $this->removeDirRecursive(MODX_BASE_PATH."/assets/products/".(int)$product_id."/");
                $this->removeDirRecursive(MODX_BASE_PATH."/assets/cache/images/backendProducts/".(int)$product_id."/");
            }

            /*getProductByOptionsID
             * Получаю ИД основного товара согласно опции
             * @param int
             * @return int*/
            public function getProductIdFromOption($option_id){
                $sql = "select product from modx_a_product_options where id = ".$option_id;
                $res = $this->db->makeArray($this->db->query($sql));
                return $res[0]['product'];
            }


	        /**
	         * getProductOptions
	         * Получить список опций товара
	         *
	         * @param integer $product_id ID товара
	         * @return array
	         */
	        public function getProductOptions($product_id)
	        {
		        $res = [];
		        if ((int)$product_id > 0) {
			        $res = $this->db->makeArray($this->db->query("
                        SELECT * FROM `modx_a_product_options` WHERE `product` = '{$product_id}' ORDER BY `position`
                    "));
		        }
		        return $res;
	        }

            /**
             * @param integer $product_id
             * @param string $use
             * @return mixed
             */
            public function getProductImages($product_id, $use)
            {
                return $this->db->query("SELECT * FROM `modx_a_product_images` WHERE image_product_id = '".$product_id."' AND image_use = '".$use."' ORDER BY image_position ASC");
            }
            
            /**
             * setProductCategories
             * Сохранение категорий к которым привязан товар
             *
             * @param integer $product_id ID товара
             * @param array $categories Массив ID категорий
             * @return bool
             */
            private function setProductCategories($product_id, $categories)
            {
                $this->db->query("DELETE FROM `modx_a_product_categories` WHERE product_id = '".(int)$product_id."'");
                $categories = array_filter($categories, function($element) {return !empty($element) && trim($element) != '';});
                if (is_array($categories) && count($categories) > 0) {
                    $categories = array_unique($categories);
                    $queries = [];
                    foreach ($categories as $category) {
                        $queries[] = '('.$product_id.','.$category.')';
                    }
                    $query = "INSERT INTO `modx_a_product_categories` (product_id, modx_id) VALUES ".implode(',', $queries).";";
                    $this->db->query($query);
                }
                return true;
            }

            /**
             * getProductFilterList
             * Получить список всех фильтров доступных для категории товара
             *
             * @param integer $product_id ID товара
             * @return array
             */
            public function getProductFilterList($product_id)
            {
                $res = [];
                if ((int)$product_id > 0) {
                    $query = $this->db->makeArray($this->db->query("
                        SELECT
                            fv.*,
                            f.`filter_id`,
                            f.`filter_type`,
                            fv.`filter_value_{$this->modx->config['lang_default']}`,
                            (SELECT `filter_name` FROM `modx_a_filter_strings` WHERE `filter_id` = f.`filter_id` AND `string_locate` = '{$this->modx->config['lang_default']}') AS 'filter_name'
                        FROM `modx_a_filters` f
                        LEFT JOIN `modx_a_filter_values` fv
                        ON f.`filter_id` = fv.`filter_id`
                        WHERE f.`filter_id` IN (
                            SELECT DISTINCT `filter_id`
                            FROM `modx_a_filter_categories`
                            WHERE `modx_id` IN (
                                SELECT `modx_id`
                                FROM `modx_a_product_categories`
                                WHERE `product_id` = {$product_id}
                            )
                        ) AND `filter_feature` = '1'
                    "));
                    if (is_array($query)) {
                        foreach ($query as $q) {
                            $res[$q['filter_id']]['filter_type'] = $q['filter_type'];
                            $res[$q['filter_id']]['filter_name'] = $q['filter_name'];
                            if ($q['filter_type'] == 2) {
                                $n = $q;
                                unset($n['value_id']);
                                unset($n['filter_id']);
                                unset($n['filter_url']);
                                unset($n['filter_value_num']);
                                unset($n['filter_type']);
                                unset($n['filter_name']);
                                $res[$q['filter_id']]['filter_values'][$q['value_id']] = $n;
                            } else {
                                $res[$q['filter_id']]['filter_values'][$q['value_id']] = $q['filter_value_'.$this->modx->config['lang_default']];
                            }
                        }
                    }
                }
                return $res;
            }

            /**
             * getProductCharacteristicList
             * Получить список всех характеристик доступных для категории товара
             *
             * @param integer $product_id ID товара
             * @return array
             */
            public function getProductCharacteristicList($product_id)
            {
                $res = [];
                if ((int)$product_id > 0) {
                    $query = $this->db->makeArray($this->db->query("
                        SELECT
                            fv.*,
                            f.`filter_id`,
                            f.`filter_type`,
                            fv.`filter_value_{$this->modx->config['lang_default']}`,
                            (SELECT `filter_name` FROM `modx_a_filter_strings` WHERE `filter_id` = f.`filter_id` AND `string_locate` = '{$this->modx->config['lang_default']}') AS 'filter_name'
                        FROM `modx_a_filters` f
                        LEFT JOIN `modx_a_filter_values` fv
                        ON f.`filter_id` = fv.`filter_id`
                        WHERE f.`filter_id` IN (
                            SELECT DISTINCT `filter_id`
                            FROM `modx_a_filter_categories`
                            WHERE `modx_id` IN (
                                SELECT `modx_id`
                                FROM `modx_a_product_categories`
                                WHERE `product_id` = {$product_id}
                            )
                        ) AND `filter_feature` = '0'
                    "));
                    if (is_array($query)) {
                        foreach ($query as $q) {
                            $res[$q['filter_id']]['filter_type'] = $q['filter_type'];
                            $res[$q['filter_id']]['filter_name'] = $q['filter_name'];
                            if ($q['filter_type'] == 2) {
                                $n = $q;
                                unset($n['value_id']);
                                unset($n['filter_id']);
                                unset($n['filter_url']);
                                unset($n['filter_value_num']);
                                unset($n['filter_type']);
                                unset($n['filter_name']);
                                $res[$q['filter_id']]['filter_values'][$q['value_id']] = $n;
                            } else {
                                $res[$q['filter_id']]['filter_values'][$q['value_id']] = $q['filter_value_'.$this->modx->config['lang_default']];
                            }
                        }
                    }
                }
                return $res;
            }

            /* getValuesEquipment
             * Получаю список комплектации
             * @param integer @product_id ID товара
             * @return array
             * */
            public function getEquipment($product_id){
                $res = [];
                if(!empty($product_id)){
                    $sql = "select `modx_a_products_equipment`.`id`,
                          `modx_a_products_equipment`.`name`,
                          `modx_a_products_equipment`.`sort`,
                          `modx_a_products_equipment`.`price`
                        from modx_a_products_equipment
                        WHERE `modx_a_products_equipment`.`active` = 1
                              and `modx_a_products_equipment`.`product_id` = '{$product_id}'
                        order by `modx_a_products_equipment`.`sort`, `name`";
                    $res = $this->db->makeArray($this->db->query($sql));
                }
                return $res;
            }

            /* getValuesEquipment
             * Получаю список комплектации
             * @param integer @product_id ID товара
             * @return array
             * */
            public function getValuesEquipment($product_id){
                $out = [];
                if(!empty($product_id)){
                    $sql = "select modx_a_products_equipment_values.eqp_id,
                      `modx_a_products_equipment`.`name`, modx_a_products_equipment_values.name val_name, 
                      `modx_a_products_equipment_values`.`id` val_id,
                      `modx_a_products_equipment_values`.`price` val_price
                    from modx_a_products_equipment
                      INNER JOIN modx_a_products_equipment_values
                        on modx_a_products_equipment_values.eqp_id = `modx_a_products_equipment`.`id`
                    WHERE `modx_a_products_equipment`.`active` = 1
                    and `modx_a_products_equipment`.`product_id` = '{$product_id}'";
                    $res = $this->db->makeArray($this->db->query($sql));
                    foreach ($res as $item){
                        $out[$item['eqp_id']][] = [
                            'param_name' => $item['name'],
                            'val_name'   => $item['val_name'],
                            'val_id'     => $item['val_id'],
                            'val_price'  => $item['val_price'],
                            ];
                    }
                }
                return $out;
            }

            /* getEquipmentValues
             * Получаю список дополнительной комплектации
             * @param integer @product_id ID товара
             * @return array
             * */
            public function getEquipmentValues($product_id){
                $res = [];
                if(!empty($product_id)){
                    $sql = "select `modx_a_products_equipment_values`.`id`,
                        `modx_a_products_equipment_values`.`name`,
                        `modx_a_products_equipment_values`.`sort`,
                        `modx_a_products_equipment_values`.`price`,                        
                        `modx_a_products_equipment_values`.`eqp_id`                        
                        from modx_a_products_equipment_values 
                        INNER JOIN modx_a_products_equipment on modx_a_products_equipment.id = modx_a_products_equipment_values.eqp_id
                    WHERE `modx_a_products_equipment`.`active` = 1 
                      and `modx_a_products_equipment`.`product_id` = '{$product_id}' 
                      order by `modx_a_products_equipment`.`sort`, `name`";
                    $res = $this->db->makeArray($this->db->query($sql));
                }
                return $res;
            }

            /* getAdditionalEquipment
             * Получаю список дополнительной комплектации
             * @param integer @product_id ID товара
             * @return array
             * */
            public function getAdditionalEquipment($product_id){
                $res = [];
                if(!empty($product_id)){
                    $sql = "select `id`, `name`, `sort`, `price`, `disable`, `default`, `active` from modx_a_products_parameters WHERE active = 1 and product_id = '{$product_id}' order by sort, name";
                    $res = $this->db->makeArray($this->db->query($sql));
                }
                return $res;
            }

            /* getAdditionalEquipment
             * Получаю список дополнительной комплектации
             * @param integer @product_id ID товара
             * @return array
             * */
            public function addEquipmentItem($param){
                $res = false;
                if(!empty($product_id)){
                    $sql = "insert into modx_a_products_equipment_values(eqp_id,name,value,price,active,dtCreate)
                        VALUES ({$param['']})";
                    $res = $this->db->makeArray($this->db->query($sql));
                }
                return $res;
            }

            /* addEquipmentValueItem
             * Добавить значение комплектации комплектации
             * @param array параметры значения для ИД комплектации
             * @return bit
             * */
            public function addEquipmentValueItem($param){
                $res = false;
                if(!empty($param['eqp_id'])){
                    $sql = "insert into modx_a_products_equipment_values(eqp_id,name,sort,price,active,dtCreate)
                        VALUES ({$param['eqp_id']}, '{$param['name']}', {$param['sort']}, {$param['price']}, 1, now())";
                    $res = $this->db->makeArray($this->db->query($sql));
                }
                return $res;
            }
            /* updateEquipmentValueItem
             * Добавить значение комплектации комплектации
             * @param array параметры значения для ИД комплектации
             * @return bit
             * */
            public function updateEquipmentValueItem($param){
                $res = false;
                if(!empty($param['id'])){
                    $sql = "update modx_a_products_equipment_values set name = '{$param['edit_name']}',sort = {$param['edit_sort']},price = {$param['edit_price']},tms = now() where id = {$param['id']}";
                    $res = $this->db->query($sql);
                }
                return $res;
            }

            /*deleteAdditionalEquipment
             * удаление записи дополнительной комплектации*/
            public function deleteAdditionalEquipment($eqpID){
                if(!empty($eqpID)) {
                    $sql = "update modx_a_products_parameters set tms=now(), active = 0 where id = " . $eqpID;
                    return $this->db->query($sql);
                }
                return false;
            }

            /*deleteEquipment
             * удаление записи комплектации*/
            public function deleteEquipment($eqpID){
                if(!empty($eqpID)) {
                    $sql = "update modx_a_products_equipment set tms=now(), active = 0 where id = " . $eqpID;
                    return $this->db->query($sql);
                }
                return false;
            }

            /*addAdditionalEquipment
             * Добавление записи дополнительной комплектации
             * @param array
             * @return bit
             * */
            public function addAdditionalEquipment($param){
                if(!empty($param)) {
                    $sql = "insert into modx_a_products_parameters (`product_id`, `name`, `sort`, `price`, `disable`, `default`, `active`, `dtCreate`) 
                      VALUES ('{$param['product_id']}','{$param['name']}','{$param['sort']}','{$param['price']}',".($param['disable'] == 'on' ? 1 : 0).",".($param['default'] == 'on' ? 1 : 0).",1,now())";
                    return $this->db->query($sql);
                }
                return false;
            }

            /*showEquipmentList
             *@param int ИД комплектации
             *@result string json значений комплектации*/
            public function showEquipmentList($equip_id){
                $out = [];
                if(!empty($equip_id)){
                    $sql = "select `id`,`name`,`value`, `price` from modx_a_products_equipment_values where eqp_id = $equip_id and active = 1";
                    $values = $this->db->makeArray($this->db->query($sql));
                    foreach ($values as $key=>$value){
                        $out[$value['id']] = [
                            'name' =>   $value['name'],
                            'value'=>   $value['value'],
                            'price'=>   $value['price'],
                            ];
                    }
                }
                return json_encode($out);
            }



            /*addAdditionalEquipment
             * Добавление записи дополнительной комплектации
             * @param array
             * @return bit
             * */
            public function addEquipment($param){
                if(!empty($param)) {
                    $sql = "insert into modx_a_products_equipment (`product_id`, `name`, `sort`, `price`, `active`, `dtCreate`) 
                      VALUES ('{$param['product_id']}','{$param['name']}','{$param['sort']}','{$param['price']}',1,now())";
                    $this->db->query($sql);
                    return $this->db->conn->insert_id;

                }
                return false;
            }

            /*updateAdditionalEquipment
             * обновление записи дополнительной комплектации *
             * @param array
             * @return bit
             */
            public function updateAdditionalEquipment($param){
                if(!empty($param)) {
                    $sql = "update modx_a_products_parameters set tms=now(),`name`='{$param['edit_name']}', 
                            `sort` = '{$param['edit_sort']}', 
                            `price` = '{$param['edit_price']}',
                            `disable` = ".($param['edit_disable'] == 'on'?1:0).",
                            `default` = ".($param['edit_default'] == 'on'?1:0)."
                             where id = " . $param['id'];
                    return $this->db->query($sql);
                }
                return false;
            }

            /*updateAdditionalEquipment
             * обновление записи дополнительной комплектации *
             * @param array
             * @return bit
             */
            public function updateEquipment($param){
                if(!empty($param)) {
                    $sql = "update modx_a_products_equipment set tms=now(),`name`='{$param['edit_name']}', 
                            `sort` = '{$param['edit_sort']}', 
                            `price` = '{$param['edit_price']}'
                             where id = " . $param['id'];
                    $result = $this->db->query($sql);
                    $result = $result;
                }
                return false;
            }

            /**
             * getProductFilters
             * Получить список фильтров товара
             *
             * @param integer $product_id ID товара
             * @return array
             */
            public function getProductFilters($product_id)
            {
                $res = [];
                if ((int)$product_id > 0) {
                    $query = $this->db->makeArray($this->db->query("
                        SELECT `filter_id`, GROUP_CONCAT(`value_id` SEPARATOR ',') AS 'value_id' FROM `modx_a_product_filter_values` WHERE `product_id` = '{$product_id}' GROUP BY `filter_id`
                    "));
                    if (is_array($query)) {
                        foreach ($query as &$q) {
                            $v = explode(',', $q['value_id']);
                            if (count($v) > 1) {
                                $res[$q['filter_id']] = $v;
                            } else {
                                $res[$q['filter_id']] = $q['value_id'];
                            }
                        }
                    }
                }
                return $res;
            }

            /**
             * setProductFilters
             * Сохранение значений фильтров товара
             *
             * @param integer $product_id ID товара
             * @param array $values Массив ID значений
             * @return bool
             */
            public function setProductFilters($product_id, $values)
            {
                $this->db->query("DELETE FROM `modx_a_product_filter_values` WHERE `product_id` = '".(int)$product_id."'");
                if (is_array($values)) {
                    $queries = [];
                    foreach ($values as $key => $value) {
                        if ($value != '') {
                            if (is_array($value)) {
                                foreach ($value as $k => $v) {
                                    if ($k == 0) {
                                        if (is_array($v) && count(array_diff($v, [""])) > 0) {
                                            $value_id = $this->searchFilterValueId($key, $v);
                                            if ($value_id) {
                                                $queries[] = '('.$product_id.','.$key.','.$value_id.')';
                                            }
                                        }
                                    }
                                    if ((int)$v > 0 && !is_array($v)) {
                                        $queries[] = '('.$product_id.','.$key.','.$v.')';
                                    }

                                }
                            } else {
                                $queries[] = '('.$product_id.','.$key.','.$value.')';
                            }
                        }
                    }
                    if (count($queries) > 0) {
                        $query = "INSERT INTO `modx_a_product_filter_values` (`product_id`, `filter_id`, `value_id`) VALUES ".implode(',', $queries).";";
                        $this->db->query($query);
                    }
                }
                return true;
            }

            /**
             * searchFilterValueId
             * Поиск ИД значения фильтра по дефолтному языку
             *
             * @param integer $filter_id ID родительского фильтра
             * @param array $values Массив значений
             * @return int|string
             */
            private function searchFilterValueId($filter_id, $values)
            {
                $search = isset($values['filter_value_num']) ? "CAST(`filter_value_num` AS DECIMAL(7,2)) = CAST('".number_format(floatval($values['filter_value_num']), 2, '.', '')."' AS DECIMAL(7,2))" : "filter_value_{$this->modx->config['lang_default']} = '".$values['filter_value_'.$this->modx->config['lang_default']]."'";
                $fv = $this->db->getRow($this->db->query("SELECT `value_id`, `filter_url` FROM `modx_a_filter_values` WHERE {$search} LIMIT 1"));
                $fields = $this->db->makeArray($this->db->query("DESCRIBE `modx_a_filter_values`"));
                $fields = array_column($fields, 'Field');
                $queries = [];
                $que = [];
                $id  = (int)$fv['value_id'] > 0 ? (int)$fv['value_id'] : "NULL";
                if (isset($values['filter_value_num'])) {
                    $url = isset($fv['filter_url']) && !empty($fv['filter_url']) ? $fv['filter_url'] : $this->setUrl(str_replace([',', '.'], '-', $values['filter_value_num']), "modx_a_filter_values", "filter_url");
                } else {
                    $url = isset($fv['filter_url']) && !empty($fv['filter_url']) ? $fv['filter_url'] : $this->setUrl($values['filter_value_'.$this->modx->config['lang_default']], "modx_a_filter_values", "filter_url");
                }
                $num = (float)$values['filter_value_num'] > 0 ? number_format(floatval($values['filter_value_num']), 2, '.', '') : "NULL";
                foreach ($fields as $field) {
                    if ($field == 'value_id') $que[] = $id;
                    elseif ($field == 'filter_id') $que[] = $filter_id;
                    elseif ($field == 'filter_url') $que[] = "'".$url."'";
                    elseif ($field == 'filter_value_num') $que[] = $num;
                    else $que[] = "'".$values[$field]."'";
                }
                if (count(array_diff($que, ["''", "NULL"])) > 3 || (float)$num > 0) {
                    $queries[] = '('.implode(', ', $que).')';
                }
                if (count($queries) > 0) {
                    if ((int)$id > 0) {
                        $query = "REPLACE INTO `modx_a_filter_values` (".implode(', ', $fields).") VALUES ".implode(',', $queries).";";
                        $this->db->query($query);
                    } else {
                        $query = "INSERT INTO `modx_a_filter_values` (".implode(', ', $fields).") VALUES ".implode(',', $queries).";";
                        $this->db->query($query);
                        $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                        $id = $result['last_id'];
                    }
                }
                return $id;
            }
            
            /**
             * getCategories
             * Получить список всех категорий каталога
             *
             * @param $cat_templ_id
             * @param $lang
             * @param null $isfolder
             * @return null
             */
            public function getCategories($cat_templ_id, $lang, $isfolder = null)
            {
                $res = null;
                $wherefolder = '';
                if (!$isfolder) $wherefolder = "AND isfolder = 0";
                $query = "
                SELECT
                  c.id,
                  c.pagetitle_$lang AS 'pagetitle',
                  (SELECT pagetitle_$lang FROM `modx_site_content` WHERE id = c.parent) AS 'parenttitle',
                  (SELECT pagetitle_$lang FROM `modx_site_content` WHERE id = (SELECT parent FROM `modx_site_content` WHERE id = c.parent)) AS 'parentuptitle'
                FROM `modx_site_content` c
                WHERE c.template = $cat_templ_id AND c.parent <> 0 $wherefolder
                ORDER BY parenttitle ASC, pagetitle ASC
                ";
                $result = $this->db->makeArray($this->db->query($query));
                foreach ($result as $value) {
                    $res[$value['id']]['title'] = $value['pagetitle'].' ('.$value['id'].')';
                    if ($value['parenttitle']) $res[$value['id']]['title'] = $value['parenttitle'].' -> '.$res[$value['id']]['title'];
                    if ($value['parentuptitle']) $res[$value['id']]['title'] = $value['parentuptitle'].' -> '.$res[$value['id']]['title'];
                }
                return $res;
            }
            
            /**
             * getRecommender
             * Получить список товаров для рекомендаций
             *
             * @return null
             */
            public function getRecommender($cid = 0)
            {
                $res = null;
                $query = "
                    SELECT
                        p.product_id,
                        (SELECT `product_name` FROM `modx_a_product_strings` WHERE `string_locate` = '".$this->modx->config['lang']."' AND `product_id` = p.`product_id`) AS 'product_name'
                    FROM `modx_a_products` p
                    WHERE p.`product_id` != '{$cid}'
                    ORDER BY `product_name`
                ";
                $result = $this->db->makeArray($this->db->query($query));
                foreach ($result as $value) {
                    $res[$value['product_id']] = $value['product_name'].' ('.$value['product_id'].')';
                }
                return $res;
            }

            /**
             * getFilterList
             * Получить список всех фильтров из БД
             *
             * @param integer $limit Количество записей для выборки
             * @return mixed
             */
            public function getFilterList()
            {
                $limit = 30;
                $page  = isset($_REQUEST['p']) ? ((int)$_REQUEST['p'] - 1) * $limit : 0;
                $query = "
                    SELECT SQL_CALC_FOUND_ROWS
                        f.filter_id,
                        f.filter_feature,
                        f.filter_type,
                        f.filter_url,
                        (SELECT filter_name FROM `modx_a_filter_strings` WHERE string_locate = '".$this->modx->config['lang']."' AND filter_id = f.filter_id) AS 'filter_name'
                    FROM `modx_a_filters` f
                    ORDER BY f.filter_updated DESC
                    LIMIT ".$page.", ".$limit;
                $res = $this->db->query($query);
                $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
                $this->modx->setPlaceholder("max_items", $pages['cnt']);
                $this->modx->setPlaceholder("limit_items", $limit);
                return $this->db->makeArray($res);
            }
            
            /**
             * getFilter
             * Получить основную информацию о фильтре по его ID
             *
             * @param integer $fid ID фильтра
             * @return array or null
             */
            public function getFilter($fid)
            {
                $res = null;
                if ($fid = (int)$fid) {
                    $res = $this->db->getRow($this->db->query("
                    SELECT
                        f.*,
                        (SELECT GROUP_CONCAT(`modx_id` SEPARATOR '|') FROM `modx_a_filter_categories` WHERE `filter_id` = f.`filter_id`) AS 'filter_categories'
                    FROM `modx_a_filters` f
                    WHERE f.`filter_id` = ".$fid
                    ));
                    $res['filter_categories'] = explode('|', $res['filter_categories']);
                }
                return $res;
            }

            /**
             * saveFilter
             * Сохранение нового фильтра
             *
             * @param $filter
             * @return mixed
             */
            public function saveFilter($filter)
            {
                $alias = $filter['filter_url'] != '' ? $this->validateUrl($filter['filter_url']) : $this->setUrl($filter[$this->modx->config['lang_default']]['filter_name'], "modx_a_filters", "filter_url");
                $alias = str_replace('/', '-', $alias);
                $this->db->query("
                    INSERT INTO `modx_a_filters` SET
                        `filter_url` 			= '".$alias."',
                        `filter_feature`        = '".(int)$filter['filter_feature']."',
                        `filter_hide`           = '".(int)$filter['filter_hide']."',
                        `filter_type`           = '".(int)$filter['filter_type']."',
                        `filter_product_active` = '".(int)$filter['filter_product_active']."',
                        `filter_seo_active`     = '".(int)$filter['filter_seo_active']."'
                ");
                $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                $this->setFilterCategories($result['last_id'], $filter['filter_categories']);
                $this->setFilterValues($result['last_id'], $filter['values']);
                return $result['last_id'];
            }

            /**
             * updateFilter
             * Обновить существующий фильтр
             *
             * @param array $filter продукт
             * @return mixed
             */
            public function updateFilter($filter)
            {
                $alias = $filter['filter_url'] != '' ? $this->validateUrl($filter['filter_url']) : $this->setUrl($filter[$this->modx->config['lang_default']]['filter_name'], "modx_a_filters", "filter_url");
                $alias = str_replace('/', '-', $alias);
                $this->setFilterCategories($filter['filter_id'], $filter['filter_categories']);
                $this->setFilterValues($filter['filter_id'], $filter['values']);
                return $this->db->query("
                    UPDATE `modx_a_filters` SET
                        `filter_url` 			= '".$alias."',
                        `filter_feature`        = '".(int)$filter['filter_feature']."',
                        `filter_hide`           = '".(int)$filter['filter_hide']."',
                        `filter_type`           = '".(int)$filter['filter_type']."',
                        `filter_product_active` = '".(int)$filter['filter_product_active']."',
                        `filter_seo_active`     = '".(int)$filter['filter_seo_active']."',
                        `filter_updated`        = NOW()
                    WHERE `filter_id` = '".(int)$filter['filter_id']."'
                ");
            }
            
            /**
             * deleteFilter
             * Удалить фильтр
             *
             * @param integer $filter_id Фильтр
             */
            public function deleteFilter($filter_id)
            {
                $this->db->query("DELETE FROM `modx_a_filters` WHERE filter_id = '".(int)$filter_id."' LIMIT 1");
                $this->db->query("DELETE FROM `modx_a_filter_strings` WHERE filter_id = '".(int)$filter_id."'");
            }

            /**
             * saveFilterString
             * Обновить переводы товара
             *
             * @param array $filter массив текстов
             * @param string $lang язык
             * @return mixed
             */
            public function saveFilterString($filter, $lang = 'ru')
            {
                return $this->db->query("
                    INSERT INTO `modx_a_filter_strings` SET
                        `string_locate`      = '".$lang."',
                        `filter_id`          = '".(int)$filter['filter_id']."',
                        `filter_name`        = '".$this->db->escape($filter[$lang]['filter_name'])."',
                        `filter_description` = '".$this->db->escape($filter[$lang]['filter_description'])."'
                    ON DUPLICATE KEY UPDATE
                        `filter_name`        = '".$this->db->escape($filter[$lang]['filter_name'])."',
                        `filter_description` = '".$this->db->escape($filter[$lang]['filter_description'])."'
                ");
            }
            
            /**
             * getFilterString
             * Получить переводы для товара
             *
             * @param integer $filter_id ID фильтра
             * @param string $lang Язык
             * @return array
             */
            public function getFilterString($filter_id, $lang = 'ru')
            {
                return $this->db->getRow($this->db->query("SELECT * FROM `modx_a_filter_strings` WHERE filter_id = ".$filter_id." AND string_locate = '".$lang."'"));
            }
            
            /**
             * setFilterCategories
             * Сохранение категорий к которым привязан фильтр
             *
             * @param integer $filter_id ID товара
             * @param array $categories Массив ID категорий
             * @return bool
             */
            private function setFilterCategories($filter_id, $categories)
            {
                $this->db->query("DELETE FROM `modx_a_filter_categories` WHERE filter_id = '".(int)$filter_id."'");
                if (is_array($categories)) {
                    $categories = array_unique($categories);
                    $queries = [];
                    foreach ($categories as $category) {
                        $queries[] = '('.$filter_id.','.$category.')';
                    }
                    $query = "INSERT INTO `modx_a_filter_categories` (filter_id, modx_id) VALUES ".implode(',', $queries).";";
                    $this->db->query($query);
                }
                return true;
            }

            /**
             * setFilterValues
             * Сохранение списка значений фильтра
             *
             * @param integer $filter_id ID фильтра
             * @param array $values Массив значений
             * @return bool
             */
            private function setFilterValues($filter_id, $values)
            {
                $lang_list_arr = explode(',', $this->modx->config['lang_list']);
                $lang_list_cnt = (is_array($lang_list_arr)) ? count($lang_list_arr) : 1;
                $this->db->query("DELETE FROM `modx_a_filter_values` WHERE `filter_id` = '".(int)$filter_id."'");
                if (is_array($values)) {
                    $fields = $this->db->makeArray($this->db->query("DESCRIBE `modx_a_filter_values`"));
                    $fields = array_column($fields, 'Field');
                    $queries= [];
                    foreach ($values['value_id'] as $key => $val) {
                        $que = [];
                        $val = (int)$val > 0 ? (int)$val : "NULL";
                        if (trim($values['filter_value_num'][$key])) {
                            $url = isset($values['filter_url'][$key]) && !empty($values['filter_url'][$key]) ? $this->validateUrl($values['filter_url'][$key]) : $this->setUrl(str_replace([',', '.'], '-', $values['filter_value_num'][$key]), "modx_a_filter_values", "filter_url");
                        } else {
                            $url = isset($values['filter_url'][$key]) && !empty($values['filter_url'][$key]) ? $this->validateUrl($values['filter_url'][$key]) : $this->setUrl($values['filter_value_'.$this->modx->config['lang_default']][$key], "modx_a_filter_values", "filter_url");
                        }
                        $num = (float)$values['filter_value_num'][$key] > 0 ? number_format(floatval($values['filter_value_num'][$key]), 2, '.', '') : "NULL";
                        foreach ($fields as $field) {
                            if ($field == 'value_id') $que[] = $val;
                            elseif ($field == 'filter_id') $que[] = $filter_id;
                            elseif ($field == 'filter_url') $que[] = "'".$url."'";
                            elseif ($field == 'filter_value_num') $que[] = $num;
                            else $que[] = "'".$this->db->escape($values[$field][$key])."'";
                        }

                        if (count(array_diff($que, ["''", "NULL"])) > $lang_list_cnt || (float)$num > 0) {
                            $queries[] = '('.implode(', ', $que).')';
                        }
                    }
                    if (count($queries) > 0) {
                        $query = "INSERT INTO `modx_a_filter_values` (`".implode('`,`', $fields)."`) VALUES ".implode(',', $queries).";";
                        $this->db->query($query);
                    }
                }
                return true;
            }
            
            /**
             * getFilterValues
             * Получение списка значений фильтра
             *
             * @param integer $fid ID фильтра
             * @return array
             */
            public function getFilterValues($fid)
            {
                $res = null;
                if ($fid = (int)$fid) {
                    $query = "
                    SELECT *
                    FROM `modx_a_filter_values`
                    WHERE filter_id = {$fid}
                    ORDER BY filter_value_{$this->modx->config['lang_default']} ASC
                    ";
                    $res = $this->db->makeArray($this->db->query($query));
                }
                return $res;
            }
                        
            /**
             * getFaqsList
             * Получить список всех вопросов
             *
             * @param integer $limit Количество записей для выборки
             * @return mixed
             */
            public function getFaqsList()
            {
                $limit = 30;
                $page  = isset($_REQUEST['p']) ? ((int)$_REQUEST['p'] - 1) * $limit : 0;
                $query = "
                SELECT SQL_CALC_FOUND_ROWS
                    f.faq_id,
                    f.faq_active,
                    f.faq_created,
                    f.faq_name,
                    f.faq_email,
                    f.faq_phone,
                    (SELECT faq_question  FROM `modx_a_faq_strings` WHERE string_locate = '".$this->modx->config['lang']."' AND faq_id = f.faq_id) AS 'faq_question',
                    (SELECT faq_question  FROM `modx_a_faq_strings` WHERE faq_id = f.faq_id AND faq_question <> '' LIMIT 1) AS 'faq_question_rand',
                    (CASE WHEN f.faq_category > 0
                        THEN (SELECT pagetitle_".$this->modx->config['lang']." FROM `modx_site_content` WHERE id = f.faq_category)
                        ELSE ''
                    END) AS 'faq_category'
                FROM `modx_a_faqs` f
                ORDER BY f.faq_updated DESC
                LIMIT ".$page.", ".$limit;
                $res = $this->db->query($query);
                $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
                $this->modx->setPlaceholder("max_items", $pages['cnt']);
                $this->modx->setPlaceholder("limit_items", $limit);
                return $this->db->makeArray($res);
            }
            
            /**
             * getFaq
             * Получить основную информацию о вопросе по его ID
             *
             * @param integer $fid ID вопроса
             * @return array or null
             */
            public function getFaq($fid)
            {
                $res = null;
                if ($fid = (int)$fid) {
                    $res = $this->db->getRow($this->db->query("
                    SELECT
                      f.*
                    FROM `modx_a_faqs` f
                    WHERE f.faq_id = ".$fid
                    ));
                }
                return $res;
            }
            
            /**
             * getFaqString
             * Получить переводы для вопроса
             *
             * @param integer $faq_id ID вопроса
             * @param string $lang Язык
             * @return array
             */
            public function getFaqString($faq_id, $lang = 'ru')
            {
                return $this->db->getRow($this->db->query("SELECT * FROM `modx_a_faq_strings` WHERE faq_id = ".$faq_id." AND string_locate = '".$lang."'"));
            }
            
            /**
             * saveFaq
             * Обновить вопрос
             *
             * @param $faq
             * @return mixed
             */
            public function saveFaq($faq)
            {
                $this->db->query("
                INSERT INTO `modx_a_faqs` SET
                    faq_active   = '".(int)$faq['faq_active']."',
                    faq_category = '".(int)$faq['faq_category']."',
                    faq_name     = '".$this->db->escape($faq['faq_name'])."',
                    faq_email    = '".$this->db->escape($faq['faq_email'])."',
                    faq_phone    = '".$this->db->escape($faq['faq_phone'])."'
            ");
                $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                return $result['last_id'];
            }
            
            /**
             * updateFaq
             * Обновить вопрос
             *
             * @param $faq
             * @return mixed
             */
            public function updateFaq($faq)
            {
                return $this->db->query("
                UPDATE `modx_a_faqs` SET
                    faq_active   = '".(int)$faq['faq_active']."',
                    faq_category = '".(int)$faq['faq_category']."',
                    faq_name     = '".$this->db->escape($faq['faq_name'])."',
                    faq_email    = '".$this->db->escape($faq['faq_email'])."',
                    faq_phone    = '".$this->db->escape($faq['faq_phone'])."'
                WHERE faq_id = '".(int)$faq['faq_id']."'
            ");
            }
            
            /**
             * saveFaqString
             * Сохранить переводы вопроса
             *
             * @param $faq
             * @param string $lang
             * @return mixed
             */
            public function saveFaqString($faq, $lang = 'ru')
            {
                return $this->db->query("
                INSERT INTO `modx_a_faq_strings` SET
                    string_locate = '".$lang."',
                    faq_id        = '".(int)$faq['faq_id']."',
                    faq_question  = '".$this->db->escape($faq[$lang]['faq_question'])."',
                    faq_answer    = '".$this->db->escape($faq[$lang]['faq_answer'])."'
                ON DUPLICATE KEY UPDATE
                    faq_question  = '".$this->db->escape($faq[$lang]['faq_question'])."',
                    faq_answer    = '".$this->db->escape($faq[$lang]['faq_answer'])."'
            ");
            }
            
            /**
             * deleteFilter
             * Удалить вопрос
             *
             * @param integer $faq_id ID вопроса
             */
            public function deleteFaq($faq_id)
            {
                $this->db->query("DELETE FROM `modx_a_faqs` WHERE faq_id = '".(int)$faq_id."' LIMIT 1");
                $this->db->query("DELETE FROM `modx_a_faq_strings` WHERE faq_id = '".(int)$faq_id."'");
            }

            /**
             * getMetersList
             * Получить список всех вопросов
             *
             * @param integer $limit Количество записей для выборки
             * @return mixed
             */
            public function getMetersList()
            {
                $limit = 30;
                $page  = isset($_REQUEST['p']) ? ((int)$_REQUEST['p'] - 1) * $limit : 0;
                $query = "
                SELECT SQL_CALC_FOUND_ROWS
                    f.meter_id,
                    f.meter_active,
                    f.meter_created,
                    f.meter_name,
                    f.meter_email,
                    f.meter_phone,
                    (SELECT meter_question  FROM `modx_a_meter_strings` WHERE string_locate = '".$this->modx->config['lang']."' AND meter_id = f.meter_id) AS 'meter_question',
                    (SELECT meter_question  FROM `modx_a_meter_strings` WHERE meter_id = f.meter_id AND meter_question <> '' LIMIT 1) AS 'meter_question_rand',
                    (CASE WHEN f.meter_category > 0
                        THEN (SELECT pagetitle_".$this->modx->config['lang']." FROM `modx_site_content` WHERE id = f.meter_category)
                        ELSE ''
                    END) AS 'meter_category'
                FROM `modx_a_meters` f
                ORDER BY f.meter_updated DESC
                LIMIT ".$page.", ".$limit;
                $res = $this->db->query($query);
                $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
                $this->modx->setPlaceholder("max_items", $pages['cnt']);
                $this->modx->setPlaceholder("limit_items", $limit);
                return $this->db->makeArray($res);
            }

            /**
             * getMeter
             * Получить основную информацию о вопросе по его ID
             *
             * @param integer $fid ID вопроса
             * @return array or null
             */
            public function getMeter($fid)
            {
                $res = null;
                if ($fid = (int)$fid) {
                    $res = $this->db->getRow($this->db->query("
                    SELECT
                      f.*
                    FROM `modx_a_meters` f
                    WHERE f.meter_id = ".$fid
                    ));
                }
                return $res;
            }

            /**
             * getMeterString
             * Получить переводы для вопроса
             *
             * @param integer $meter_id ID вопроса
             * @param string $lang Язык
             * @return array
             */
            public function getMeterString($meter_id, $lang = 'ru')
            {
                return $this->db->getRow($this->db->query("SELECT * FROM `modx_a_meter_strings` WHERE meter_id = ".$meter_id." AND string_locate = '".$lang."'"));
            }

            /**
             * saveMeter
             * Обновить вопрос
             *
             * @param $meter
             * @return mixed
             */
            public function saveMeter($meter)
            {
                $this->db->query("
                INSERT INTO `modx_a_meters` SET
                    meter_active   = '".(int)$meter['meter_active']."',
                    meter_category = '".(int)$meter['meter_category']."',
                    meter_name     = '".$this->db->escape($meter['meter_name'])."',
                    meter_email    = '".$this->db->escape($meter['meter_email'])."',
                    meter_phone    = '".$this->db->escape($meter['meter_phone'])."'
            ");
                $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
                return $result['last_id'];
            }

            /**
             * updateMeter
             * Обновить вопрос
             *
             * @param $meter
             * @return mixed
             */
            public function updateMeter($meter)
            {
                return $this->db->query("
                UPDATE `modx_a_meters` SET
                    meter_active   = '".(int)$meter['meter_active']."',
                    meter_category = '".(int)$meter['meter_category']."',
                    meter_name     = '".$this->db->escape($meter['meter_name'])."',
                    meter_email    = '".$this->db->escape($meter['meter_email'])."',
                    meter_phone    = '".$this->db->escape($meter['meter_phone'])."'
                WHERE meter_id = '".(int)$meter['meter_id']."'
            ");
            }

            /**
             * saveMeterString
             * Сохранить переводы вопроса
             *
             * @param $meter
             * @param string $lang
             * @return mixed
             */
            public function saveMeterString($meter, $lang = 'ru')
            {
                return $this->db->query("
                INSERT INTO `modx_a_meter_strings` SET
                    string_locate = '".$lang."',
                    meter_id        = '".(int)$meter['meter_id']."',
                    meter_question  = '".$this->db->escape($meter[$lang]['meter_question'])."',
                    meter_answer    = '".$this->db->escape($meter[$lang]['meter_answer'])."'
                ON DUPLICATE KEY UPDATE
                    meter_question  = '".$this->db->escape($meter[$lang]['meter_question'])."',
                    meter_answer    = '".$this->db->escape($meter[$lang]['meter_answer'])."'
            ");
            }

            /**
             * deleteFilter
             * Удалить вопрос
             *
             * @param integer $meter_id ID вопроса
             */
            public function deleteMeter($meter_id)
            {
                $this->db->query("DELETE FROM `modx_a_meters` WHERE meter_id = '".(int)$meter_id."' LIMIT 1");
                $this->db->query("DELETE FROM `modx_a_meter_strings` WHERE meter_id = '".(int)$meter_id."'");
            }
            /**
             * getUserLogin
             * Проверка авторизации пользователя
             *
             * @return array
             */
            public function getUserLogin($documentObject)
            {
                foreach ($documentObject as $key => $value) {
                    if (preg_match("/user_/i", $key)) {
                        unset($documentObject[$key]);
                    }
                }
                unset($documentObject['loggedIn']);
                unset($documentObject['username']);
                unset($documentObject['usertype']);
                if ($user = $this->modx->userLoggedIn()) {
                    unset($user['id']);
                    $user['user_loggedIn'] = (int)$user['loggedIn'];
                    if (count($_SESSION['webuser'])) {
                        foreach ($_SESSION['webuser'] as $key => $value) {
                            $user['user_'.$key] = $value;
                        }
                    }
                } else {
                    $user['user_loggedIn']    = 0;
                    $user['user_internalKey'] = 0;
                }
                
                return array_merge($documentObject, $user);
            }
                        
            /**
             * getMailTemplate
             *
             * @param string $id Имя шаблона
             * @return mixed
             */
            public function getPaginate($tpl_enable = 'pagination_backend_enable', $tpl_disable = 'pagination_backend_disable', $classJS = ' js_change_page'){
                if($this->modx->getPlaceholder("max_items") != '' && $this->modx->getPlaceholder("limit_items") != '') {
                    $pages['all'] = ceil($this->modx->getPlaceholder("max_items") / $this->modx->getPlaceholder("limit_items"));
                }
                $prev           = $prev != '' ? $prev : '&laquo;';
                $next           = $next != '' ? $next : '&raquo;';
                $classPrev      = ($classPrev != '' ? $classPrev : 'prev').$classJS;
                $classNext      = ($classNext != '' ? $classNext : 'next').$classJS;
                $classPage      = ($classPage != '' ? $classPage : 'page').$classJS;
                $classCurrent   = $classCurrent != '' ? $classCurrent : 'active';
                $classSeparator = $classSeparator != '' ? $classSeparator : 'separator';
                $separator      = $separator != '' ? $separator : '...';
                $url            = $this->modx->getPlaceholder("url") != '' ? $this->modx->getPlaceholder("url") : '';
                if($pages['all'] > 1){
                    $limit_pagination = 5;
                    if(isset($_GET['p'])) {
                        $curent = isset($_REQUEST['p']) ? $_REQUEST['p'] : 1;
                    } else {
                        $curent = ($this->modx->getPlaceholder("page")) ? $this->modx->getPlaceholder("page") : 1;
                    }
                    $start  = $curent - $limit_pagination;
                    $end    = $curent + $limit_pagination;
                    $start  = $start < 1 ? 1 : $start;
                    $end    = $end > $pages['all'] ? $pages['all'] : $end;
                    if($curent > 1) {
                        $res .= $this->modx->parseChunk($tpl_enable , array('url' => $url."1", 'class' => $classPrev, 'title' => $prev, 'rel' => "prev"));
                    }
                    
                    if($curent > $limit_pagination+1) {
                        $res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.'1', 'class' => $classPage, 'title' => '1', 'rel' => "prev")).$this->modx->parseChunk($tpl_disable, array('class' => $classSeparator, 'title' => $separator));
                    }
                    for($i=$start; $i<$end+1; $i++) {
                        if($i==($curent)) {
                            $res .= $this->modx->parseChunk($tpl_disable, array('class' => $classCurrent, 'title' => $i, 'rel' => ""));
                        } else {
                            $res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.$i, 'class' => $classPage, 'title' => $i,  "rel" => ($i < $curent) ? "prev" : "next"));
                        }
                    }
                    if($curent < ($pages['all'] - $limit_pagination)) {
                        $res .= $this->modx->parseChunk($tpl_disable, array('class' => $classSeparator, 'title' => $separator)).$this->modx->parseChunk($tpl_enable , array('url' => $url.$pages['all'], 'class' => $classPage, 'title' => $pages['all'], 'rel' => "next"));
                    }
                    if($curent != $pages['all']) {
                        $res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.$pages['all'], 'class' => $classNext, 'title' => $next, 'rel' => "next"));
                    }
                }
                return $res;
            }
            
            /**
             * getMailTemplate
             *
             * @param string $id Имя шаблона
             * @return mixed
             */
            public function getMailTemplate($id)
            {
                $arr = $this->db->makeArray($this->db->query("SELECT * FROM `modx_a_mail_templates` WHERE mail_name = '{$id}'"));
                foreach ($arr as $value) {
                    $res[$value['mail_lang']] = $value;
                }
                return $res;
            }
            
            /**
             * setMailTemplate
             *
             * @param string $template шаблон
             * @return mixed
             */
            public function setMailTemplate($template)
            {
                $langs = explode(",", $this->modx->config['lang_list']);
                foreach ($langs as $lang) {
                    $this->db->query("
                    INSERT INTO `modx_a_mail_templates` SET
                        mail_name     = '".$this->db->escape($template['mail_name'])."',
                        mail_lang     = '{$lang}',
                        mail_title    = '".$this->db->escape($template['mail_title'])."',
                        mail_subject  = '".$this->db->escape($template['mail_subject'][$lang])."',
                        mail_template = '".$this->db->escape($template['mail_template'][$lang])."'
                    ON DUPLICATE KEY UPDATE
                        mail_title    = '".$this->db->escape($template['mail_title'])."',
                        mail_subject  = '".$this->db->escape($template['mail_subject'][$lang])."',
                        mail_template = '".$this->db->escape($template['mail_template'][$lang])."'
                ");
                }
                return true;
            }
            
            /**
             * getPrice
             * Получить цену в пересчете на выбранную валюту по курсу из настроек
             *
             * @param $amount
             * @param string $to
             * @return float
             */
            public function getPrice($amount, $to = "uah", $delimiter = "&nbsp;")
            {
                $codes = explode("|", $this->modx->config['shop_curr_code']);
                $rates = explode("|", $this->modx->config['shop_curr_rate']);
                foreach ($codes as $key => $value) {
                    if ($value != '') {
                        $curr[$value] = $rates[$key];
                    }
                }
                if ($this->modx->config['shop_curr_default_code'] == "uah") {
                    $res = round($amount / $curr[$to], 2);
                } else {
                    $res = round($amount * $curr[$to], 2);
                }
                return number_format($res, 0, '.', $delimiter);
            }
            
            /**
             * getPriceToDefault
             * Получить цену в пересчете из выбранной валюты по курсу из настроек
             *
             * @param $amount
             * @param string $from
             * @return float
             */
            public function getPriceToDefault($amount, $from = "uah")
            {
                $codes = explode("|", $this->modx->config['shop_curr_code']);
                $rates = explode("|", $this->modx->config['shop_curr_rate']);
                foreach ($codes as $key => $value) {
                    if ($value != '') {
                        $curr[$value] = $rates[$key];
                    }
                }
                if ($this->modx->config['shop_curr_default_code'] == "uah") {
                    $res = round($amount * $curr[$from], 2);
                } else {
                    $res = round($amount / $curr[$from], 2);
                }
                return $res;
            }

            /**
             * formatMoney
             * Форматирует цену в целое число или с копейками
             *
             * @param $number
             * @param int $cents копейки: 0=никогда, 1=если необходимо, 2=всегда
             * @return string
             */
            public function formatMoney($number, $cents = 1)
            {
                if (is_numeric($number)) {
                    if (!$number) {
                        $money = ($cents == 2 ? '0.00' : '0');
                    } else {
                        if (floor($number) == $number) {
                            $money = number_format($number, ($cents == 2 ? 2 : 0));
                        } else {
                            $money = number_format(round($number, 2), ($cents == 0 ? 0 : 2));
                        }
                    }
                    return $money;
                }
            }
            
            /**
             * tinyMCE
             * генерирует код необходимый для подключения tinyMCE
             *
             * @param string $ids Список id полей через запятую
             * @param string $height Высота окна
             * @return string
             */
            public function tinyMCE($ids, $height = '500px')
            {
                $elements = [];
                $langs    = explode(",", $this->modx->config['lang_list']);
                $ids      = explode(",", $ids);
                foreach ($langs as $lang) {
                    foreach ($ids as $id) {
                        $elements[] = trim($lang)."_".trim($id);
                    }
                }
                $editor = "TinyMCE4";
                return implode("", $this->modx->invokeEvent('OnRichTextEditorInit', array(
                    'editor' => $editor,
                    'elements' => $elements,
                    'height' => $height
                )));
            }
            
            /**
             * CodeMirror
             * генерирует код необходимый для подключения CodeMirror
             *
             * @param string $ids Список name полей через запятую
             * @param string $height Высота окна
             * @return string
             */
            public function CodeMirror($ids, $height = '500px')
            {
                $elements = [];
                $langs    = explode(",", $this->modx->config['lang_list']);
                $ids      = explode(",", $ids);
                foreach ($langs as $lang) {
                    foreach ($ids as $id) {
                        $elements[] = trim($id)."[".trim($lang)."]";
                    }
                }
                $editor = "Codemirror";
                return implode("", $this->modx->invokeEvent('OnRichTextEditorInit', array(
                    'editor' => $editor,
                    'elements' => $elements,
                    'height' => $height
                )));
            }

            public function go404() {
                header("HTTP/1.0 404 Not Found");
                header("HTTP/1.1 404 Not Found");
                header("Status: 404 Not Found");
                $this->modx->sendForward($this->modx->config['error_page']);
                die;
            }

            /**
             * sendMail
             *
             * @param string $to
             * @param string $template
             * @param string $data
             * @param bool $lang
             * @return mixed
             * @throws phpmailerException
             */
            public function sendMail($to, $template, $data, $lang = false)
            {
                // Определение языка
                if (!$lang) {
                    $lang = $this->modx->config['lang'];
                }
                if (!$lang) {
                    $lang = $this->modx->config['lang_default'];
                }
                // Подготовка шаблона
                $meta = $this->getMailTemplate("meta");
                $tpl  = $this->getMailTemplate($template);
                if (is_array($data)) {
                    foreach ($data as $key => $value) {
                        $parse["{".$key."}"] = $value;
                    }
                }
                $body = strtr($tpl[$lang]['mail_template'], $parse);
                $body = str_replace('{body}', $body, $meta[$lang]['mail_template']);
                $body = str_replace('[!', '[[', $body);
                $body = str_replace('!]', ']]', $body);
                $body = $this->modx->parseDocumentSource($this->modx->rewriteUrls($body));

                // Отправка письма
                if (!class_exists('PHPMailer')) {
                    require MODX_BASE_PATH.MGR_DIR."/includes/controls/phpmailer/class.phpmailer.php";
                }
                $Password = $this->modx->config['smtppw'];
                $Password = substr($Password,0,-7);
                $Password = str_replace('%','=',$Password);
                $Password = base64_decode($Password);
                $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->SMTPDebug  = 0;
                $mail->SMTPAuth   = true;
                //$mail->SMTPSecure = "ssl"; // для gmail открыть
                $mail->CharSet    = 'UTF-8';
                $mail->Host       = $this->modx->config['smtp_host'];
                $mail->Port       = $this->modx->config['smtp_port'];
                $mail->Username   = $this->modx->config['smtp_username'];
                $mail->Password   = $Password;
                $mail->Subject    = $this->modx->parseDocumentSource($this->modx->rewriteUrls($tpl[$lang]['mail_subject']));
                $mail->SetFrom($this->modx->config['smtp_username'], $this->modx->config['site_name']);
                $mail->AddAddress($to);
                $mail->MsgHTML($body);
                return $mail->Send();
            }
            
            /**
             * setUrl
             * Генератор уникального URL в таблице
             *
             * @param string $name Строка для генерации
             * @param string $table Таблица для поиска
             * @param string $field Поле для поиска
             * @return string
             */
            public function setUrl($name, $table, $field)
            {
                include_once MODX_BASE_PATH . "assets/plugins/transalias/transalias.class.php";
                $transalias = new TransAlias;
                $remove_periods = 'No';
                $table_name = 'russian';
                $transalias->loadTable($table_name, $remove_periods);
                $char_restrict = 'alphanumeric';
                $word_separator = 'dash';
                $alias = $transalias->stripAlias($name,$char_restrict,$word_separator);
                $alias = strip_tags($alias);
                $alias = strtolower($alias);
                $alias = preg_replace('/[^\.A-Za-z0-9 _-]/', '', $alias); // strip non-alphanumeric characters
                $alias = str_replace('_', '', $alias);
                $alias = str_replace( ['.', ','], '-', $alias);
                $alias = preg_replace('/\s+/', '-', $alias); // convert white-space to dash
                $alias = preg_replace('/-+/', '-', $alias);  // convert multiple dashes to one
                $alias = trim($alias, '-'); // trim excess
                $x = 0;
                do {
                    $newAlias = $alias.($x == 0 ? '' : $x);
                    $x++;
                } while (
                    $this->db->getRow($this->db->query("
                        SELECT `$field` AS 'alias' FROM `$table` WHERE `$field` = '{$newAlias}'
                        UNION ALL
                        SELECT `alias` FROM {$this->modx->getFullTableName('site_content')} WHERE `alias` = '{$newAlias}'
                    "))
                );
                return  $newAlias;
            }

            /**
             * validateUrl
             * Валидация URL на допустимые символы
             *
             * @param string $alias Строка для проверки
             * @return string
             */
            public function validateUrl($alias)
            {
                include_once MODX_BASE_PATH . "assets/plugins/transalias/transalias.class.php";
                $transalias = new TransAlias;
                $remove_periods = 'No';
                $table_name = 'russian';
                $transalias->loadTable($table_name, $remove_periods);
                $char_restrict = 'alphanumeric';
                $word_separator = 'dash';
                $alias = $transalias->stripAlias($alias,$char_restrict,$word_separator);
                $alias = strip_tags($alias);
                $alias = strtolower($alias);
                $alias = preg_replace('/[^\.A-Za-z0-9 _-]/', '', $alias); // strip non-alphanumeric characters
                $alias = str_replace('_', '', $alias);
                $alias = str_replace( ['.', ','], '-', $alias);
                $alias = preg_replace('/\s+/', '-', $alias); // convert white-space to dash
                $alias = preg_replace('/-+/', '-', $alias);  // convert multiple dashes to one
                $alias = trim($alias, '-'); // trim excess
                return  $alias;
            }

            /**
             * setFileName
             * Генератор уникального имени файла в папке
             *
             * @param string $name Строка для генерации
             * @param string $path Поле для поиска
             * @return string
             */
            public function setFileName($name, $path)
            {
                include_once MODX_BASE_PATH . "assets/plugins/transalias/transalias.class.php";
                $transalias = new TransAlias;
                $remove_periods = 'No';
                $table_name = 'russian';
                $transalias->loadTable($table_name, $remove_periods);
                $char_restrict = 'alphanumeric';
                $word_separator = 'dash';
                $name = $transalias->stripAlias($name,$char_restrict,$word_separator);
                $name = strip_tags($name);
                $name = strtolower($name);
                $name = preg_replace('/[^\.A-Za-z0-9 _-]/', '', $name); // strip non-alphanumeric characters
                $name = preg_replace('/\s+/', '-', $name); // convert white-space to dash
                $name = preg_replace('/-+/', '-', $name);  // convert multiple dashes to one
                $name = trim($name, '-'); // trim excess
                $name = str_replace('-', '_', $name);
                $names = explode('.', $name);
                $ext = array_pop($names);
                $name = implode($names);
                $x = 0;
                do {
                    $newName = $name.($x == 0 ? '' : $x).'.'.$ext;
                    $x++;
                } while (file_exists($path.$newName));
                return  $newName;
            }
            
            /**
             * copyDirRecursive
             * Рекурсивное копирование папки
             *
             * @param $src исходная папка (без слеша на конце)
             * @param $dst папка назначения (без слеша на конце)
             */
            public function copyDirRecursive($src, $dst)
            {
                $dir = opendir($src);
                if (!is_dir($dir)) {
                    return false;
                }
                if (!file_exists($dst)) {
                    mkdir($dst);
                    chmod($dst, 0777);
                }
                while (false !== ($file = readdir($dir))) {
                    if ($file != '.' && $file != '..') {
                        if (is_dir($src.'/'.$file)) {
                            $this->copyDirRecursive($src.'/'.$file, $dst.'/'.$file);
                        } else {
                            copy($src.'/'.$file, $dst.'/'.$file);
                        }
                    }
                }
                closedir($dir);
            }
            
            /**
             * removeDirRecursive
             * Рекурсивное удаление папки
             *
             * @param $dir
             * @param $keep_file 
             */
            public function removeDirRecursive($dir, $keep_file = 0)
            {
                if ($objs = glob($dir."/*")) {
                    foreach($objs as $obj) {
                        if (is_dir($obj)) {
                            if ($keep_file == 1) {
                                $this->clearCacheRecursive($obj);
                            }
                            $this->removeDirRecursive($obj, $keep_file);
                        } else {
                            if ($keep_file == 0) {
                                unlink($obj);
                            }
                        }
                    }
                } else {
                    rmdir($dir);
                }
            }

            /**
             * clearCasheRecursive
             * Рекурсивная очистка кеша
             *
             * @param $dir Массив директорий
             * @return mixed
             */
            public function clearCacheRecursive(...$dir)
            {
                $objs = [];
                $exclude = ['index.html'];
                $ress = [];

                foreach ($dir as &$d) {
                    $objs = array_merge($objs, glob(MODX_BASE_PATH.$this->modx->getCacheFolder().$d.'/*'));
                }

                if (is_array($objs)) {
                    foreach ($objs as &$obj) {
                        if (is_dir($obj)) {
                            $this->removeDirRecursive($obj, 1);
                        } else {
                            if(!in_array(basename($obj), $exclude)) {
                                $mime = reset(explode('/', mime_content_type($obj)));
                                if ($mime == 'image') {
                                    if (time() - filectime($obj) > 2628003) { // 1 месяц
                                        $ress[reset(explode('/', mime_content_type($obj)))][] = '';
                                        unlink($obj);
                                    }
                                } else {
                                    $ress[reset(explode('/', mime_content_type($obj)))][] = '';
                                    unlink($obj);
                                }
                            }
                        }
                    }
                }

                if (count($ress)) {
                    $text = '';
                    foreach ($ress as $key => $res) {
                        switch ($key) {
                            case 'image':
                                $text .= "<p><b>".count($res)."</b> изображений очищено.</p>";
                                break;
                            case 'text':
                                $text .= "<p><b>".count($res)."</b> сниппетов очищено.</p>";
                                break;
                        }
                    }
                    return $text;
                }

                return "<p>Файлов в кастомном кеше не обнаружено.</p>";
            }
            
            /**
             * sendSMS
             * Рассылка смс LetSads
             *
             * @param $phone    моб.номер в формате 38 0xx xxx xx xx
             * @param $message  текст сообщения
             * @return mixed
             */
            public function sendSMS($phone, $message)
            {
                $sUrl = 'http://letsads.com/api';
                $phone = preg_replace('/\D+/i', '', $phone);
                $login = preg_replace('/\D+/i', '', $this->modx->config['shop_sms_login']);
                
                $sXML = '<?xml version="1.0" encoding="UTF-8"?>
                        <request>
                            <auth>
                                <login>'.$login.'</login>
                                <password>'.$this->modx->config['shop_sms_pass'].'</password>
                            </auth>
                            <message>
                                    <from>'.$this->modx->config['shop_sms_sender'].'</from>
                                    <text>'.$message.'</text>
                                    <recipient>'.$phone.'</recipient>
                            </message>
                        </request>';
                
                $rCurl = curl_init($sUrl);
                curl_setopt($rCurl, CURLOPT_HEADER, 0);
                curl_setopt($rCurl, CURLOPT_POSTFIELDS, $sXML);
                curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($rCurl, CURLOPT_POST, 1);
                $sAnswer = curl_exec($rCurl);
                curl_close($rCurl);
                
                if (stripos($sAnswer, 'error') !== false) {
                    return false;
                }
                preg_match_all("|<sms_id>(.*)</sms_id>|U",
                    $sAnswer, $sms_id, PREG_PATTERN_ORDER);
                
                return $sms_id;
            }
            
            /**
             * genPass
             * Генерирует случайную строку заданной длины и букв ланинского алфавита и цифр
             *
             * @param int $length  длина пароля
             * @param bool $chars  использовать буквы
             * @return null|string
             */
            public function genPass($length = 8, $chars = true)
            {
                if ($chars) {
                    $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP!#@-_|";
                } else {
                    $chars = "1234567890";
                }
                $size = StrLen($chars) - 1;
                $pass = null;
                while ($length--) {
                    $pass .= $chars[rand(0, $size)];
                }

                return $pass;
            }

            /**
             * isValidMd5
             * Проверяет является ли строка валидным хешем md5
             *
             * @param string $md5
             * @return int
             */
            public function isValidMd5($md5 ='')
            {
                return preg_match('/^[a-f0-9]{32}$/', $md5);
            }

            /**
             * mb_ucfirst
             * Преобразует первый символ строки в верхний регистр
             *
             * @param string $str строка для преобразования
             * @param string $encoding кодировка
             * @param bool $lower_str_end true - окончание слова в нижний регистр, false -> не менять
             * @return string
             */
            public function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = true)
            {
                $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
                $str_end = "";
                if ($lower_str_end) {
                    $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
                } else {
                    $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
                }
                $str = $first_letter . $str_end;
                return $str;
            }
        }
    }