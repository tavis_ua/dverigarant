<?php

$type = 'variations';

switch ($controller) {
    case 'edit':
        $options = $modx->db->getRow($modx->db->select("*", "modx_a_product_options", "id=".(int)$_GET['vid']));
	    $characteristics = $shop->getProductCharacteristicList($_GET['pid']);
	    $filters         = $shop->getProductFilterList($_GET['pid']);
	    $pfvs            = $shop->getProductFilters($_GET['vid']);
    case 'add':
        $tiny_mce = $shop->tinyMCE("content, description", "300px");
        $tpl   = "/" . $type . "/edit.tpl";
        break;

    case 'save':
        $post = $_POST['options'];
        $pid = (int)$post['pid'];
        $post['product'] = (int)$pid;

        if((int)$post['default_option'] > 0) {
	        $modx->db->update(['default_option' => 0], 'modx_a_product_options', "`product` = '" . $pid . "'");
        }
	    $id = (int)$post['vid'];

        if ((int)$post['vid'] > 0) {

        } else {
            $id = $modx->db->insert(['active' => 1, 'product' => $pid], 'modx_a_product_options');
        }

	    $option_filters = $post['option_filters'];
        unset($post['pid']);
        unset($post['vid']);
        unset($post['option_filters']);

        foreach ($post as $row => $value) {
            $post[$row] = $modx->db->escape($value);
        }
        $post['active'] = (int)$post['active'] > 0 ? 1: 0;
        $post['availability'] = (int)$post['availability'] > 0 ? 1: 0;
        $post['price'] = isset($post['price']) && $post['price'] != '' ? $post['price'] : 0;
        $post['price_old'] = isset($post['price_old']) && $post['price_old'] != '' ? $post['price_old'] : 0;
        $modx->db->update($post, 'modx_a_product_options', "`id` = '" . $id . "'");
	    $shop->setProductFilters($id, $option_filters);

        header("Location: " . $url . "products&c=edit&i=" . $pid);
        break;

    case 'delete':
        if ((int)$_GET['vid'] > 0) {
            $id = (int)$_GET['vid'];
            $modx->db->delete('modx_a_product_options', "`id` = '{$id}'");
        }

        $pid = (int)$_GET['pid'];
        header("Location: " . $url . "products&c=edit&i=" . $pid);
        break;
}
