<?php
/**
 * Переводы текстовых элементов модуля
 */
return [
    "ru" => [
        'region_title'                  => 'Настройка регионов',
        'review_addition'               => 'Добавление <small>отзыва</small>',
        'review_editing'                => 'Редактирование',
        'review_edit'                   => 'Редактировать',
        'review_save'                   => 'Сохранить',
        'review_copy'                   => 'Скопировать',
        'review_created'                => 'Создано',
        'review_create'                 => 'Создать',
        'review_delete'                 => 'Удалить',
        'review_act'                    => 'Действие',
        'review_main'                   => 'Основное',
        'review_images'                 => 'Изображения',
        'review_visibility'             => 'Видимость',
        'review_yes'                    => 'Да',
        'review_not'                    => 'Нет',
        'review_home'                   => 'На главной',
        'review_item_id'                => 'ID товара',
        'review_email_user'             => 'Email пользователя',
        'review_rating'                 => 'Рейтинг',
        'review_name_user'              => 'Имя пользователя',
        'review_text'                   => 'Текст отзыва',
        'review_loading_images'         => 'Загрузка изображений',
        'review_no_found'               => 'Отзывов не найдено',
        'review_user'                   => 'Пользователь',
        'review_product'                => 'Товар',
        'review_unpublished'            => 'Не опубликован',
        'review_published'              => 'Опубликован',
    ],

    "uk" => [
        'region_title'                  => 'Настройка регионов',
    ],

    "en" => [
        'region_title'                  => 'Настройка регионов',
    ],
];