<h2 class="muted">Отзывы</h2>
<?php if (count($reviews) == 0 || !is_array($reviews)):?>
<div class="row"><h1 class="screen"><strong><?=$_lang['review_no_found']?></strong></h1></div>
<?php else: ?>
<table class="table table-condensed table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="70px" style="text-align:center;">ID</th>
            <th style="text-align:center;"><?=$_lang['review_user']?></th>
            <th style="text-align:center;"><?=$_lang['review_product']?></th>
            <th style="text-align:center;"><?=$_lang['review_rating']?></th>
            <th width="110px" style="text-align:center;"><?=$_lang['review_visibility']?></th>
            <th width="100px" style="text-align:center;"><?=$_lang['review_home']?></th>
            <th width="150px" style="text-align:center;"><?=$_lang['review_created']?></th>
            <th width="250px" style="text-align:center;"><?=$_lang['review_act']?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($reviews as $review): ?>
    <tr id="product-<?=$review['review_id']?>">
        <td class="vertical-align"><?=$review['review_id']?></td>
        <td>&nbsp;
            <img src="<?=$modx->runSnippet('R', ['img' => $review['photo'], 'folder' => 'backendReviews/'.$review['review_webuser'], 'opt' => 'w=50&h=50&far=1'])?>" alt="<?php if ($review['photo']) echo $modx->runSnippet('R', ['img' => $review['photo'], 'folder' => 'backendReviews/'.$review['review_webuser'], 'opt' => 'w=400&h=400&far=1'])?>" class="img-thumbnail" />&emsp;
            <b><?php echo htmlspecialchars($review['review_user_name']);?> <?=$review['review_user_email']?></b>
        </td>
        <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$review['review_product']?>&nbsp;-&nbsp;<?=$review['review_product_name']?></b></td>
        <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$review['review_rating']?></b></td>
        <td class="vertical-align" style="text-align:center;">
            <b>
                <?php if($review['review_active'] == 0):?><span class="label label-danger"><?=$_lang['review_unpublished']?></span><?php endif;?>
                <?php if($review['review_active'] == 1):?><span class="label label-success"><?=$_lang['review_published']?></span><?php endif;?>
            </b>
        </td>
        <td class="vertical-align" style="text-align:center;">
            <b>
                <?php if($review['review_home'] == 0):?><span class="label label-danger"><?=$_lang['review_not']?></span><?php endif;?>
                <?php if($review['review_home'] == 1):?><span class="label label-success"><?=$_lang['review_yes']?></span><?php endif;?>
            </b>
        </td>
        <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$modx->runSnippet("Shop", ["get" => "getDate", "from" => "Y-m-d H:i:s", "to" => "H:i:s d.m.Y", "date" => $review['review_created']])?></b></td>
        <td class="vertical-align" style="text-align:center;">
            <a href="<?=$url?>reviews&c=edit&i=<?=$review['review_id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;<?=$_lang['review_edit']?></a>
            <a href="<?=$url?>reviews&c=delete&i=<?=$review['review_id']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;<?=$_lang['review_delete']?></a>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>reviews&c=add"><i class="fa fa-plus-square"></i>&emsp;<?=$_lang['review_create']?></a>
        </li>
    </ul>
</div>
<img src="/inspiration/media/style/MODxRE2/images/misc/ajax-loader.gif" id="img-preview" style="display: none;" class="img-thumbnail">
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("table img").on("mouseenter", function(){
            var alt = jQuery(this).attr("alt");
            if (alt.length > 0) {
                jQuery("#img-preview").attr("src", alt).show();
            }
        });
        jQuery("table img").on("mouseleave", function(){
            jQuery("#img-preview").hide();
        });
    });
</script>
