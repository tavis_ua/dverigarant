<?php if (!isset($review['review_id']) || (int)$review['review_id'] == 0): ?>
    <form id="editForm" action="<?=$url?>reviews&c=save" method="post">
        <h2 class="muted"><?=$_lang['review_addition']?></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>reviews&c=update" method="post">
        <h2 class="muted"><?=$_lang['review_editing']?> <small>(<?php echo htmlspecialchars($review[$modx->config['lang']]['review_user_name']);?> - <?=$review['review_product']?>)</small></h2>
<?php endif; ?>
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li class="active"><a href="#base" role="tab" data-toggle="tab"><b><?=$_lang['review_main']?></b></a></li>
            <?php foreach ($languages as $k => $lng): ?>
            <li role="presentation"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><?=$lng?></b></a></li>
            <?php endforeach; ?>
            <?php if ((int)$review['review_id'] > 0): ?>
            <li role="presentation" id="mediaTab"><a href="#media" role="tab" data-toggle="tab"><b><?=$_lang['review_images']?></b></a></li>
            <?php endif; ?>
        </ul>
        <br />
        <div class="container-fluid">
            <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="base">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" width="160px"><?=$_lang['review_visibility']?>:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="review[review_active]" <?=($review['review_active'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="<?=$_lang['review_yes']?>" data-off-text="<?=$_lang['review_not']?>" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"><?=$_lang['review_home']?>:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="review[review_home]" <?=($review['review_home'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="<?=$_lang['review_yes']?>" data-off-text="<?=$_lang['review_not']?>" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"><?=$_lang['review_item_id']?>:</th>
                        <td>
                            <input type="number" name="review[review_product]" value="<?=$review['review_product']?>" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"><?=$_lang['review_email_user']?>:</th>
                        <td>
                            <input type="text" name="review[review_user_email]" value="<?=$review['review_user_email']?>" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"><?=$_lang['review_rating']?>:</th>
                        <td>
                            <!-- <input type="text" name="review[review_rating]" value="<?=$review['review_rating']?>" class="form-control" /> -->
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="1" <?=($review['review_rating'] == 0 ? "checked" : "")?> type="radio" class="form-control" />0
                            </label>
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="1" <?=($review['review_rating'] == 1 ? "checked" : "")?> type="radio" class="form-control" />1
                            </label>
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="2" <?=($review['review_rating'] == 2 ? "checked" : "")?> type="radio" class="form-control" />2
                            </label>
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="3" <?=($review['review_rating'] == 3 ? "checked" : "")?> type="radio" class="form-control" />3
                            </label>
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="4" <?=($review['review_rating'] == 4 ? "checked" : "")?> type="radio" class="form-control" />4
                            </label>
                            <label class="radio-inline">
                                <input name="review[review_rating]" value="5" <?=($review['review_rating'] == 5 ? "checked" : "")?> type="radio" class="form-control" />5
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <?php foreach ($languages as $k => $lng): ?>
            <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" width="160px"><?=$_lang['review_name_user']?>:</th>
                        <td>
                            <input type="text" name="review[<?=$lng?>][review_user_name]" value="<?php echo htmlspecialchars($review[$lng]['review_user_name']);?>" class="form-control" />
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"><?=$_lang['review_text']?>:</th>
                        <td>
                            <textarea id="<?=$lng?>_content" name="review[<?=$lng?>][review_content]" class="form-control" rows="2"><?=$review[$lng]['review_content']?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <?php endforeach; ?>
            <div role="tabpanel" class="tab-pane" id="media">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" style="text-align:center;" width="160px">
                            <a href="#" id="slider" class="btn btn-primary"><i class="fa fa-camera-retro"></i><?=$_lang['review_images']?></a>
                        </th>
                        <td>
                            <ul class="slider"><?=$_lang['review_loading_images']?></ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
        <input type="hidden" name="review[review_id]" value="<?=$review['review_id']?>" />
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;document.getElementById('editForm').submit();"><i class="fa fa-save"></i><?=$_lang['review_save']?></a>
                </li>
                <?php if ((int)$review['review_id'] > 0): ?>
                <li id="Button3" class="transition">
                    <a href="<?=$url?>reviews&c=copy&i=<?=$review['review_id']?>"><i class="fa fa-files-o"></i><?=$_lang['review_copy']?></a>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </form>
    <?php if ((int)$review['review_id'] > 0): ?>
        <script src="/assets/site/ajaxupload.js"></script>
        <script type="text/javascript">
            jQuery(document).on("click", "#mediaTab", function() {
                getCover();
                jQuery(".slider").sortable();
            });

            jQuery(document).on("click", ".js_delete", function() {
                var t = jQuery(this).prevAll("img");
                jQuery.ajax({url:"<?=$url?>reviews",type:"GET",data:"c=delete_image&folder=<?=$review['review_id']?>&delete="+t.attr("alt"),success:function(ajax){
                t.parents("li").fadeOut().remove();
                }});
            });

            function getCover() {
                jQuery.ajax({url:"<?=$url?>reviews",type:"GET",data:"c=get_cover&folder=<?=$review['review_id']?>",success:function(ajax){
                    jQuery(".slider").html(ajax);
                }});
            }

            jQuery(document).ready(function(){
                new AjaxUpload(jQuery("#slider"), {
                    action: "<?=$url?>reviews&c=set_cover",
                    multiple: true,
                    name: "uploader[]",
                    data: {
                        "size"  : 2048576,
                        "folder": "<?=$review['review_id']?>"
                    },
                    onSubmit: function(file, ext){
                        if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                            alert('<?=$_lang["shop_file_format"]?>');
                            return false;
                        }
                    },
                    onComplete: function(file, response){
                        getCover();
                    }
                });
            });
        </script>
    <?php endif; ?>
    <?=$tiny_mce?>