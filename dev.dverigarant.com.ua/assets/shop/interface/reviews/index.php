<?php

include dirname(__FILE__)."/class.php";
$class = new Reviews($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

$imagePathCache = MODX_BASE_PATH."assets/cache/images/";

switch ($controller) {
    case "add":
        $tiny_mce = $shop->tinyMCE("content", "300px");
        $tpl = "review.tpl";
        break;

    case "edit":
        $tiny_mce = $shop->tinyMCE("content", "300px");
        $review   = $class->getReview($_GET['i']);
        foreach ($languages as $value) {
            $review[$value] = $class->getReviewString($_GET['i'], $value);
        }
        $tpl = "review.tpl";
        break;

    case "save":
        $_POST['review']['review_id'] = $class->saveReview($_POST['review']);
        foreach ($languages as $value) {
            $class->saveReviewString($_POST['review'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."reviews&c=edit&i=".$_POST['review']['review_id']));

    case "update":
        $class->updateReview($_POST['review']);
        foreach ($languages as $value) {
            $class->saveReviewString($_POST['review'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."reviews"));

    case "copy":
        $reviewId = $class->copyReview($_GET['i']);
        $modx->clearCache();
        die(header("Location: ".$url."reviews&c=edit&i=".$reviewId));

    case "delete" :
        $class->deleteReview($_GET['i']);
        $modx->clearCache();
        die(header("Location: ".$url."reviews"));

    case "set_cover":
        $imagePath = MODX_BASE_PATH."assets/images/reviews/";

        if (!file_exists($imagePath)){
            mkdir($imagePath);
            chmod($imagePath, 0777);
        }

        $folder = $imagePath.$_REQUEST['folder'];

        if (!file_exists($folder)){
            mkdir($folder);
            chmod($folder, 0777);
        }

        foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
            $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
            $filename = $folder."/". $name;
            move_uploaded_file($value, $filename);
            chmod($filename, 0644);
            $modx->db->query("INSERT INTO `modx_a_review_images` SET `file` = '{$name}', `review_id` = '".(int)$_REQUEST['folder']."';");
        }
        die($name);

    case "get_cover":
        $imagePath = "assets/images/reviews/".$_REQUEST['folder'];

        $images = $modx->db->makeArray($modx->db->query("SELECT * FROM `modx_a_review_images` WHERE `review_id` = '".(int)$_REQUEST['folder']."' ORDER BY `position` ASC;"));
        foreach ($images as $img) {
            $img['image_file'] = $img['file'];
            $rimg = $modx->runSnippet('R', array('img' => $imagePath."/".$img['file'], "folder" => "backendReviews/".$_REQUEST['folder'], 'opt' => 'w=150&h=150&far=1'));
            require TPLS."../../interface/".$bootstrap."/tpl/image_cover.tpl";
        }
        die;

    case "delete_image":
        $imagePath = MODX_BASE_PATH."assets/images/reviews/";
        $folder = $imagePath.$_REQUEST['folder'];
        $file = (int)$_REQUEST['delete'];
        $modx->db->query("DELETE FROM `modx_a_review_images` WHERE `file` = '".$modx->db->escape($_REQUEST['delete'])."' AND `review_id` = '".(int)$_REQUEST['folder']."';");
        unlink($folder."/".$_REQUEST['delete']);
        $class->removeDirRecursive($imagePathCache."backendReviews/".(int)$_REQUEST['folder']."/");
        unlink($imagePathCache."backendReviews/".(int)$_REQUEST['folder']);
        die;

    default:
        $reviews = $class->getReviewList();
        $modx->setPlaceholder('url', $url."reviews&p=");
        $pagin = $shop->getPaginate();
        $tpl = "reviews.tpl";
        break;
}