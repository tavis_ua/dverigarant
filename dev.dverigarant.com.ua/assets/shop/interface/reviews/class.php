<?php

class Reviews extends Shop
{
    private $modx = null;
    private $db   = null;

    public function __construct($modx)
    {
        parent::__construct($modx);
        $this->modx = $modx;
        $this->db   = $modx->db;
    }

    /**
     * getReviewList
     * Получить список всех отзывов
     *
     * @param integer $limit Количество записей для выборки
     * @return mixed
     */
    public function getReviewList()
    {
        $limit = 30;
        $page  = isset($_REQUEST['p']) ? ((int)$_REQUEST['p'] - 1) * $limit : 0;
        $query = "
            SELECT SQL_CALC_FOUND_ROWS
                r.*, wu.`photo`, 
                (SELECT `review_user_name` FROM `modx_a_review_strings` WHERE `string_locate` = '".$this->modx->config['lang']."' AND `review_id` = r.`review_id`) AS 'review_user_name',
                (CASE WHEN r.`review_product` > 0
                    THEN (SELECT `product_name` FROM `modx_a_product_strings` WHERE `string_locate` = '".$this->modx->config['lang']."' AND `product_id` = r.`review_product`)
                    ELSE ''
                END) AS 'review_product_name'
            FROM `modx_a_reviews` r
            LEFT JOIN `modx_web_user_attributes` wu ON wu.`internalKey` = r.`review_webuser`
            ORDER BY r.`review_updated` DESC
            LIMIT ".$page.", ".$limit;
        $res = $this->db->query($query);
        $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
        $this->modx->setPlaceholder("max_items", $pages['cnt']);
        $this->modx->setPlaceholder("limit_items", $limit);
        return $this->db->makeArray($res);
    }

    /**
     * getReview
     * Получить основную информацию о отзыве по его ID
     *
     * @param integer $rid ID отзыва
     * @return array or null
     */
    public function getReview($rid)
    {
        $res = null;
        if ($rid = (int)$rid) {
            $res = $this->db->getRow($this->db->query("SELECT * FROM `modx_a_reviews` WHERE `review_id` = '{$rid}';"));
        }
        return $res;
    }

    /**
     * getReviewString
     * Получить переводы для отзыва
     *
     * @param integer $review_id ID отзыва
     * @param string $lang Язык
     * @return array
     */
    public function getReviewString($review_id, $lang = 'ru')
    {
        return $this->db->getRow($this->db->query("SELECT * FROM `modx_a_review_strings` WHERE review_id = ".$review_id." AND string_locate = '".$lang."'"));
    }

    public function saveReview($review)
    {
        $this->db->query("
            INSERT INTO `modx_a_reviews` SET
                `review_active`     = '".(int)$review['review_active']."',
                `review_home`       = '".(int)$review['review_home']."',
                `review_product`    = '".(int)$review['review_product']."',
                `review_rating`     = '".(int)$review['review_rating']."',
                `review_user_email` = '".$this->db->escape($review['review_user_email'])."'
        ");
        $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
        return $result['last_id'];
    }

    /**
     * updateReview
     * Обновить отзыв
     *
     * @param $review
     * @return mixed
     */
    public function updateReview($review)
    {
        return $this->db->query("
            UPDATE `modx_a_reviews` SET
                `review_active`     = '".(int)$review['review_active']."',
                `review_home`       = '".(int)$review['review_home']."',
                `review_product`    = '".(int)$review['review_product']."',
                `review_rating`     = '".(int)$review['review_rating']."',
                `review_user_email` = '".$this->db->escape($review['review_user_email'])."'
            WHERE review_id = '".(int)$review['review_id']."'
        ");
    }

    /**
     * saveReviewString
     * Сохранить переводы отзыва
     *
     * @param $review
     * @param string $lang
     * @return mixed
     */
    public function saveReviewString($review, $lang = 'ru')
    {
        return $this->db->query("
            INSERT INTO `modx_a_review_strings` SET
                `string_locate`    = '".$lang."',
                `review_id`        = '".(int)$review['review_id']."',
                `review_user_name` = '".$this->db->escape($review[$lang]['review_user_name'])."',
                `review_content`   = '".$this->db->escape($review[$lang]['review_content'])."'
            ON DUPLICATE KEY UPDATE
                `review_user_name` = '".$this->db->escape($review[$lang]['review_user_name'])."',
                `review_content`   = '".$this->db->escape($review[$lang]['review_content'])."'
        ");
    }

    /**
     * copyReview
     * Скопировать отзыв
     *
     * @param $rid
     * @return mixed
     */
    public function copyReview($rid)
    {
        $last_id = $rid;
        if ((int)$rid > 0) {
            $this->db->query("CREATE TEMPORARY TABLE `reviews_".$rid."` AS SELECT * FROM `modx_a_reviews` WHERE review_id = {$rid}");
            $this->db->query("ALTER TABLE `reviews_".$rid."` MODIFY review_id INT NULL");
            $this->db->query("UPDATE `reviews_".$rid."` SET review_id = NULL, review_active = 0");
            $this->db->query("INSERT INTO `modx_a_reviews` SELECT * FROM `reviews_".$rid."`");
            $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
            $this->db->query("DROP TABLE `reviews_".$rid."`");
            $last_id = $result['last_id'];
            $this->db->query("CREATE TEMPORARY TABLE `reviews_".$rid."` AS SELECT * FROM `modx_a_review_strings` WHERE review_id = {$rid}");
            $this->db->query("ALTER TABLE `reviews_".$rid."` MODIFY string_id INT NULL");
            $this->db->query("UPDATE `reviews_".$rid."` SET string_id = NULL, review_id = {$last_id}");
            $this->db->query("INSERT INTO `modx_a_review_strings` SELECT * FROM `reviews_".$rid."`");
            $this->db->query("DROP TABLE `reviews_".$rid."`");
            $this->copyDirRecursive(MODX_BASE_PATH."assets/images/reviews/".$rid, MODX_BASE_PATH."assets/images/reviews/".$last_id);
        }
        return $last_id;
    }

    public function deleteReview($review_id)
    {
        $this->db->query("DELETE FROM `modx_a_reviews` WHERE review_id = '".(int)$review_id."' LIMIT 1");
        $this->db->query("DELETE FROM `modx_a_review_strings` WHERE review_id = '".(int)$review_id."'");
        $this->removeDirRecursive(MODX_BASE_PATH."/assets/images/reviews/".(int)$review_id."/");
        $this->removeDirRecursive(MODX_BASE_PATH."/assets/cache/images/backendReviews/".(int)$review_id."/");
    }
}