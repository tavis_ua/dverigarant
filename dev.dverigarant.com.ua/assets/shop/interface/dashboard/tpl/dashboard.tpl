<div class="container-fluid">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&ensp;<b>Заказы</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tr>
                            <th width="150px">Всего</th>
                            <td><?=$dashboard['order_count']?></td>
                        </tr>
                        <tr>
                            <th>Новых</th>
                            <td><?=$dashboard['order_new']?></td>
                        </tr>
                        <tr>
                            <th>Не оплаченых</th>
                            <td><?=$dashboard['order_nopaid']?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-cubes" aria-hidden="true"></i>&ensp;<b>Товары</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tr>
                            <th width="150px">Всего</th>
                            <td><?=$dashboard['product_count']?></td>
                        </tr>
                        <tr>
                            <th>Опубликованых</th>
                            <td><?=$dashboard['product_visible']?></td>
                        </tr>
                        <tr>
                            <th>Не опубликованых</th>
                            <td><?=$dashboard['product_novisible']?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php if ($isNews): ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading"><i class="fa fa-rss" aria-hidden="true"></i>&ensp;<b>Новости</b></div>
                    <div class="panel-body">
                        <table class="table table-condensed table-striped table-bordered table-hover">
                            <tr>
                                <th width="150px">Всего</th>
                                <td><?=$news['news_count']?></td>
                            </tr>
                            <tr>
                                <th>Опубликованых</th>
                                <td><?=$news['news_pub']?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>