<?php

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

$dashboard = $modx->db->getRow($modx->db->query("
    SELECT
        (SELECT COUNT(`order_id`) FROM `modx_a_order`) AS 'order_count',
        (SELECT COUNT(`order_id`) FROM `modx_a_order` WHERE `order_status` = '1') AS 'order_new',
        (SELECT COUNT(`order_id`) FROM `modx_a_order` WHERE `order_status_pay` = '0') AS 'order_nopaid',
        (SELECT COUNT(`product_id`) FROM `modx_a_products`) AS 'product_count',
        (SELECT COUNT(`product_id`) FROM `modx_a_products` WHERE `product_active` = '1') AS 'product_visible',
        (SELECT COUNT(`product_id`) FROM `modx_a_products` WHERE `product_active` = '0') AS 'product_novisible'
"));

$isNews = (bool)$modx->db->getValue($modx->db->query("SHOW TABLES LIKE '".str_replace('`', '', end(explode('.', $modx->getFullTableName('a_news'))))."'"));
$news = [];
if ($isNews) {
    $news = $modx->db->getRow($modx->db->query("
        SELECT
            (SELECT COUNT(`id`) FROM {$modx->getFullTableName('a_news')}) AS 'news_count',
            (SELECT COUNT(`id`) FROM {$modx->getFullTableName('a_news')} WHERE `published` = '1') AS 'news_pub'
    "));
}
$tpl = "dashboard.tpl";