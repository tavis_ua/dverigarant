<?php if (!isset($filter['filter_id']) || (int)$filter['filter_id'] == 0): ?>
<form id="editForm" action="<?=$url?>filters&c=save" method="post">
    <h2 class="muted">&emsp;Добавление <small>фильтра</small></h2>
    <?php else: ?>
    <form id="editForm" action="<?=$url?>filters&c=update" method="post">
        <h2 class="muted">&emsp;Редактирование <small>(<?=$filter[$modx->config['lang']]['filter_name']?>)</small></h2>
        <?php endif; ?>
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li class="active"><a href="#base" role="tab" data-toggle="tab"><b>Основное</b></a></li>
            <?php foreach ($languages as $k => $lng): ?>
            <li role="presentation"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><?=$lng?></b></a></li>
            <?php endforeach; ?>
            <li><a href="#values" role="tab" data-toggle="tab"><b>Значения</b></a></li>
        </ul>
        <br />
        <div class="container-fluid">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="base">
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" width="160px">Тип фильтра:</th>
                            <td>
                                <div class="bootstrap-switch">
                                    <input type="checkbox" name="filter[filter_feature]" <?=($filter['filter_feature'] ? "checked" : "")?> value="1" data-on-color="primary" data-off-color="info" data-label-text="&nbsp;" data-on-text="Фильтр" data-off-text="Характеристика" class="bsw">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align" width="160px">Выводить в каталоге:</th>
                            <td>
                                <div class="bootstrap-switch">
                                    <input type="checkbox" name="filter[filter_hide]" <?=($filter['filter_hide'] ? "checked" : "")?> value="1" data-on-color="warning" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Включен" data-off-text="Выключен" class="bsw">
                                </div>
                            </td>
                        </tr>
                        <!--<tr>
                            <th class="vertical-align">Тип выбора:</th>
                            <td>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="1"<?php if($filter['filter_type'] == 1 || (int)$filter['filter_type'] == 0) echo ' checked';?>>
                                        <b>Числовой</b> - Значением фильтра может быть только число <i>(тип number)</i>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="2"<?php if($filter['filter_type'] == 2) echo ' checked';?>>
                                        <b>Текстовый</b> - Значением фильтра может быть короткий текст <i>(тип text)</i>
                                    </label>
                                </div>-->
                                <!--<div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="3"<?php if($filter['filter_type'] == 3) echo ' checked';?>>
                                        <b>ИЛИ</b> - Выбор одной из опций <i>(тип radio)</i>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="4"<?php if($filter['filter_type'] == 4) echo ' checked';?>>
                                        <b>И</b> - Выбор нескольких опций <i>(тип checkbox)</i>
                                    </label>
                                </div>-->
                                <!--<div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="5"<?php if($filter['filter_type'] == 5) echo ' checked';?>>
                                        <b>Моно выбор</b> - Выбор одного из предлагаемых опций <i>(тип select)</i>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="filter[filter_type]" value="6"<?php if($filter['filter_type'] == 6) echo ' checked';?>>
                                        <b>Мульти выбор</b> - Выбор множества из предлагаемых опций <i>(тип multiselect)</i>
                                    </label>
                                </div>
                            </td>
                        </tr>-->
                        <tr>
                            <th class="vertical-align">Привязать к категориям:</th>
                            <td>
                                <select class="form-control chosen-select" data-placeholder="Выберите категорию" name="filter[filter_categories][]" multiple>
                                    <option value=""></option>
                                    <?php foreach ($categories as $id => $title): ?>
                                        <option value="<?=$id;?>" <?php if(in_array($id, $filter['filter_categories'])) echo 'selected'; ?>><?=$title['title'];?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Ссылка:</th>
                            <td>
                                <input type="text" name="filter[filter_url]" value="<?=$filter['filter_url']?>" class="form-control" />
                                &emsp;<small class="text-info">Оставьте пустым для автоматического заполнения</small>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php foreach ($languages as $k => $lng): ?>
                <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" width="160px">Название:</th>
                            <td>
                                <input type="text" name="filter[<?=$lng?>][filter_name]" value="<?=$filter[$lng]['filter_name']?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Описание:</th>
                            <td>
                                <textarea id="<?=$lng?>_description" name="filter[<?=$lng?>][filter_description]" class="form-control" rows="2"><?=$filter[$lng]['filter_description']?></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php endforeach; ?>
                <div role="tabpanel" class="tab-pane" id="values">
                    <div class="panel panel-default" style="display: none;">
                        <div class="panel-heading">&ensp;<span class="js_remove pull-right" title="Удалить"><i class="fa fa-times"></i></span>&emsp;<span class="js_add pull-right" title="Добавить"><i class="fa fa-plus"></i></span></div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <?php foreach ($languages as $k => $lng): ?>
                                <tr>
                                    <th width="160px" class="vertical-align">Значение <span class="text-uppercase"><?=$lng?></span></th>
                                    <td><input type="text" name="filter[values][filter_value_<?=$lng?>][]" value="" class="form-control" /></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th class="vertical-align">Значение URL</th>
                                    <td>
                                        <input type="text" name="filter[values][filter_url][]" value="" class="form-control" />
                                        <input type="hidden" name="filter[values][value_id][]" value="" />
                                    </td>
                                </tr>
                                <!--<tr>
                                    <th class="vertical-align">Иконка <small style="color:red;">(36x36px)</small></th>
                                    <td>
                                        <a href="#" id="add--" class="btn btn-primary js_iva"><i class="fa fa-wpexplorer"></i>&emsp;Добавить</a>
                                        <input type="hidden" name="filter[values][image][]" value="" class="form-control" />
                                    </td>
                                </tr>-->
                            </table>
                        </div>
                    </div>
                    <?php if (!isset($values) || count($values) == 0): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">&ensp;<span class="js_remove pull-right" title="Удалить"><i class="fa fa-times"></i></span>&emsp;<span class="js_add pull-right" title="Добавить"><i class="fa fa-plus"></i></span></div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <?php foreach ($languages as $k => $lng): ?>
                                <tr>
                                    <th width="160px" class="vertical-align">Значение <span class="text-uppercase"><?=$lng?></span></th>
                                    <td><input type="text" name="filter[values][filter_value_<?=$lng?>][]" value="" class="form-control" /></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th class="vertical-align">Значение URL</th>
                                    <td>
                                        <input type="text" name="filter[values][filter_url][]" value="" class="form-control" />
                                        <input type="hidden" name="filter[values][value_id][]" value="" />
                                    </td>
                                </tr>
                                <!--<tr>
                                    <th class="vertical-align">Иконка <small style="color:red;">(36x36px)</small></th>
                                    <td>
                                        <a href="#" id="add-0" class="btn btn-primary js_iva"><i class="fa fa-wpexplorer"></i>&emsp;Добавить</a>
                                        <input type="hidden" name="filter[values][image][]" value="" class="form-control" />
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php else: ?>
                    <?php foreach ($values as $value): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">&ensp;<span class="js_remove pull-right" title="Удалить"><i class="fa fa-times"></i></span>&emsp;<span class="js_add pull-right" title="Добавить"><i class="fa fa-plus"></i></span></div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <?php foreach ($languages as $k => $lng): ?>
                                <tr>
                                    <th width="160px" class="vertical-align">Значение <span class="text-uppercase"><?=$lng?></span></th>
                                    <td><input type="text" name="filter[values][filter_value_<?=$lng?>][]" value="<?=$value['filter_value_'.$lng]?>" class="form-control" /></td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th class="vertical-align">Значение URL</th>
                                    <td>
                                        <input type="text" name="filter[values][filter_url][]" value="<?=$value['filter_url']?>" class="form-control" />
                                        <input type="hidden" name="filter[values][value_id][]" value="<?=$value['value_id']?>" />
                                    </td>
                                </tr>
                                <!--<tr>
                                    <th class="vertical-align">Иконка <small style="color:red;">(36x36px)</small></th>
                                    <td>
                                        <?php if($value['image']):?>
                                            <img src="<?=$modx->runSnippet('R', ['img' => '/assets/products/characteristic/'.$value['image'], 'folder' => 'backendCharacter', 'opt' => 'w=50&h=30&far=1'])?>" alt="/assets/products/characteristic/<?=$value['image']?>" class="img-thumbnail"/>&emsp;
                                            <a href="#" id="add-<?=$value['value_id']?>" class="btn btn-primary js_iva"><i class="fa fa-recycle"></i>&emsp;Изменить</a>
                                            <a href="#" id="del-<?=$value['value_id']?>" class="btn btn-danger js_ivd"><i class="fa fa-times"></i>&emsp;Удалить</a>
                                        <?php else:?>
                                            <a href="#" id="add-<?=$value['value_id']?>" class="btn btn-primary js_iva"><i class="fa fa-wpexplorer"></i>&emsp;Добавить</a>
                                        <?php endif;?>
                                            <input type="hidden" name="filter[values][image][]" value="<?=$value['image']?>" class="form-control" />
                                    </td>
                                </tr>-->
                            </table>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <input type="hidden" name="filter[filter_id]" value="<?=$filter['filter_id']?>" />
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;document.getElementById('editForm').submit();"><i class="fa fa-save"></i>&emsp;Сохранить</a>
                </li>
            </ul>
        </div>
    </form>
    <img src="/inspiration/media/style/MODxRE2/images/misc/ajax-loader.gif" id="img-preview" style="display: none;" class="img-thumbnail">
    <script src="/assets/site/ajaxupload.js"></script>
    <script type="text/javascript">
        var filter_type = jQuery("[name=\"filter[filter_type]\"]:checked").val();
        if (filter_type == 1 || filter_type == 2) {
            jQuery("[href=\"#values\"]").parent().attr("style", "display:none;");
        }
        jQuery(document).on("click", "[name=\"filter[filter_type]\"]", function() {
            console.log(jQuery(this).val());
            if (jQuery(this).val() == 1 || jQuery(this).val() == 2) {
                jQuery("[href=\"#values\"]").parent().attr("style", "display:none;");
            } else {
                jQuery("[href=\"#values\"]").parent().removeAttr('style');
            }
        });
        jQuery(document).on("click", ".js_add", function() {
            jQuery('#values .panel:first').clone().insertAfter('#values .panel:first');
            jQuery('#values .panel:nth-child(2)').find('#add--').attr('id', 'add-'+jQuery('.js_iva').length * 100000);
            jQuery('#values .panel:nth-child(2)').removeAttr('style');
        });
        jQuery(document).on("click", ".js_remove", function() {
            jQuery(this).parent().parent().remove();
            if (jQuery('.js_remove').length < 2) {
                jQuery('#values .panel:first').clone().insertAfter('#values .panel:first');
                jQuery('#values .panel:nth-child(2)').find('#add--').attr('id', 'add-'+jQuery('.js_iva').length * 100000);
                jQuery('#values .panel:nth-child(2)').removeAttr('style');
            }
        });
        jQuery(".js_iva").each(function() {
            var did = jQuery(this).attr('id');
            initUpload(did);
        });
        jQuery(document).on("mouseenter", ".js_iva", function() {
            var did = jQuery(this).attr('id');
            initUpload(did);
            return false;
        });
        jQuery(document).on("click", ".js_ivd", function() {
            var did = jQuery(this).attr('id');
            jQuery("#"+did).parent().find('[name="filter[values][image][]"]').val('');
            jQuery("#"+did).parent().find('img').remove();
            return false;
        });
        jQuery(document).on("mouseenter", "table img", function() {
            var alt = jQuery(this).attr("alt");
            if (alt.length > 0) {
                jQuery("#img-preview").attr("src", alt).show();
            }
        });
        jQuery(document).on("mouseleave", "table img", function() {
            jQuery("#img-preview").hide();
        });
        function initUpload(did) {
            var upload = new AjaxUpload(jQuery("#"+did), {
                action: "<?=$url?>filters&c=add_img",
                multiple: false,
                name: "uploader[]",
                data: {"size":2048576},
                onSubmit: function(file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|svg|JPG|PNG|JPEG|SVG)$/.test(ext))) {
                        alert('Не разрешенный формат файла');
                        return false;
                    }
                },
                onComplete: function(file, response) {
                    getImage(did, response);
                    jQuery("#"+did).parent().find('[name="filter[values][image][]"]').val(response);
                }
            });
        }
        function getImage(did, img) {
            jQuery.ajax({
                url:"<?=$url?>filters&c=get_img&img="+img,
                type:"GET",
                success:function(ajax) {
                    jQuery("#"+did).parent().find('img').remove();
                    jQuery("#"+did).before('<img src="'+ajax+'" alt="/assets/products/characteristic/'+img+'" class="img-thumbnail"/>');
                }
            });
        }
    </script>