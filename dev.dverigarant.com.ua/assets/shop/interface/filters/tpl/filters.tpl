<div class="container-fluid">
<h2 class="muted">Доступные фильтры и характеристики магазина</h2>
<?php if (count($filters) == 0 || !is_array($filters)):?>
<div class="row"><h1 class="screen"><strong>Фильтров не найдено</strong></h1></div>
<?php else: ?>
<table class="table table-condensed table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align:center;">Название</th>
            <th width="110px" style="text-align:center;">Тип</th>
            <th width="110px" style="text-align:center;">В каталоге</th>
            <!--<th width="120px" style="text-align:center;">Выбор</th>-->
            <th width="250px" style="text-align:center;">Действие</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($filters as $filter): ?>
            <tr>
                <td class="vertical-align">&nbsp;<b><?=$filter['filter_name']?></b></td>
                <td class="vertical-align">&nbsp;
                    <b>
                        <?php if($filter['filter_feature'] == 0):?><span class="label label-primary">Характаристика</span><?php endif;?>
                        <?php if($filter['filter_feature'] == 1):?><span class="label label-info">Фильтр</span><?php endif;?>
                    </b>
                </td>
                <td class="vertical-align">&nbsp;
                    <b>
                        <?php if($filter['filter_hide'] == 0):?><span class="label label-danger">Выключен</span><?php endif;?>
                        <?php if($filter['filter_hide'] == 1):?><span class="label label-warning">Включен</span><?php endif;?>
                    </b>
                </td>
                <!--<td class="vertical-align">&nbsp;
                    <b>
                        <?php if($filter['filter_type'] == 1):?><span class="label label-default">Числовой</span><?php endif;?>
                        <?php if($filter['filter_type'] == 2):?><span class="label label-default">Текстовый</span><?php endif;?>
                        <?php if($filter['filter_type'] == 3):?><span class="label label-default">ИЛИ</span><?php endif;?>
                        <?php if($filter['filter_type'] == 4):?><span class="label label-default">И</span><?php endif;?>
                        <?php if($filter['filter_type'] == 5):?><span class="label label-default">Моно выбор</span><?php endif;?>
                        <?php if($filter['filter_type'] == 6):?><span class="label label-default">Мульти выбор</span><?php endif;?>
                    </b>
                </td>-->
                <td class="vertical-align" style="text-align:center;">
                    <a href="<?=$url?>filters&c=edit&i=<?=$filter['filter_id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a>
                    <a href="<?=$url?>filters&c=delete&i=<?=$filter['filter_id']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>filters&c=add"><i class="fa fa-plus-square"></i>&emsp;Создать</a>
        </li>
    </ul>
</div>
</div>