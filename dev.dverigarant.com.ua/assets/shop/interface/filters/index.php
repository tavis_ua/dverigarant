<?php

include dirname(__FILE__)."/class.php";
$class = new Filters($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

$folder = MODX_BASE_PATH."assets/products/characteristic/";

switch ($controller) {
    case "add":
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang'], 1);
        $filter['filter_feature'] = 1;
        $tpl = "filter.tpl";
        break;
    case "edit":
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang'], 1);
        $values     = $shop->getFilterValues($_GET['i']);
        $filter     = $shop->getFilter($_GET['i']);
        foreach ($languages as $value) {
            $filter[$value] = $shop->getFilterString($_GET['i'], $value);
        }
        $tpl = "filter.tpl";
        break;
    case "save":
        $_POST['filter']['filter_id'] = $shop->saveFilter($_POST['filter']);
        foreach ($languages as $value) {
            $shop->saveFilterString($_POST['filter'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."filters&c=edit&i=".$_POST['filter']['filter_id']));
    case "update":
        $shop->updateFilter($_POST['filter']);
        foreach ($languages as $value) {
            $shop->saveFilterString($_POST['filter'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."filters"));
    case "delete":
        $shop->deleteFilter($_GET['i']);
        $modx->clearCache();
        die(header("Location: ".$url."filters"));
    case "set_sort":
        if (count($_POST) > 0) {
            $filters = array_keys($_POST['filter']);
            if (is_array($filters)) {
                foreach ($filters as $position => $filter) {
                    $modx->db->query("UPDATE `modx_a_filters` SET `position` = '{$position}' WHERE `filter_id` = ".(int)$filter);
                }
            }
        }
        die;
    case "add_img":
        foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
            $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));

            if (!file_exists($folder)) {
                mkdir($folder);
                chmod($folder, 0777);
            }

            $filename = $folder . $name;
            move_uploaded_file($value, $filename);
            chmod($filename, 0644);
        }
        die($name);
    case "get_img":
        die($modx->runSnippet('R', ['img' => '/assets/products/characteristic/'.$_GET['img'], 'folder' => 'backendCharacter', 'opt' => 'w=30&h=30&zc=1']));
    default:
        $filters = $shop->getFilterList();
        $modx->setPlaceholder('url', $url."filters&p=");
        $pagin = $shop->getPaginate();
        $tpl = "filters.tpl";
        break;
}