<?php

include dirname(__FILE__)."/class.php";
$class = new Faq($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

switch ($controller) {
    case "add":
        $tiny_mce   = $shop->tinyMCE("content", "300px");
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang']);
        $tpl        = "faq.tpl";
        break;
    case "edit":
        $tiny_mce   = $shop->tinyMCE("content", "300px");
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang']);
        $faq        = $shop->getFaq($_GET['i']);
        foreach ($languages as $value) {
            $faq[$value] = $shop->getFaqString($_GET['i'], $value);
        }
        $tpl = "faq.tpl";
        break;
    case "save":
        $_POST['faq']['faq_id'] = $shop->saveFaq($_POST['faq']);
        foreach ($languages as $value) {
            $shop->saveFaqString($_POST['faq'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."faqs&c=edit&i=".$_POST['faq']['faq_id']));
    case "update":
        $shop->updateFaq($_POST['faq']);
        foreach ($languages as $value) {
            $shop->saveFaqString($_POST['faq'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."faqs"));
    case "delete" :
        $shop->deleteFaq($_GET['i']);
        $modx->clearCache();
        die(header("Location: ".$url."faqs"));
    default:
        $faqs  = $shop->getFaqsList();
        $modx->setPlaceholder('url', $url."faqs&p=");
        $pagin = $shop->getPaginate();
        $tpl   = "faqs.tpl";
        break;
}