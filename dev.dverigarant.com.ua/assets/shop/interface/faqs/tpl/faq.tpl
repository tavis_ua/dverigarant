<?php if (!isset($faq['faq_id']) || (int)$faq['faq_id'] == 0): ?>
<form id="editForm" action="<?=$url?>faqs&c=save" method="post">
    <h2 class="muted">Добавление <small>отзыва</small></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>faqs&c=update" method="post">
        <h2 class="muted">Редактирование <small>(<?=$faq['faq_id']?>)</small></h2>
<?php endif; ?>
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li class="active"><a href="#base" role="tab" data-toggle="tab"><b>Основное</b></a></li>
            <?php foreach ($languages as $k => $lng): ?>
                <li role="presentation"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><?=$lng?></b></a></li>
            <?php endforeach; ?>
        </ul>
        <br />
        <div class="container-fluid">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="base">
                    <table class="table table-bordered table-condensed">
                        <tr>
                              <th class="vertical-align" width="160px">Статус выполнения:</th>
                              <td>
                                  <div class="bootstrap-switch">
                                      <input type="checkbox" name="faq[faq_active]" <?=($faq['faq_active'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Выполнено" data-off-text="Не выполнено" class="bsw">
                                  </div>
                              </td>
                          </tr>
                        <!--  <tr>
                            <th class="vertical-align">Категория:</th>
                            <td>
                                <select class="form-control chosen-select" data-placeholder="Выберите категорию" name="faq[faq_category]">
                                    <?php foreach ($categories as $id => $title): ?>
                                        <option value="<?=$id;?>" <?php if($id == $faq['faq_category']) echo 'selected'; ?>><?=$title['title'];?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <th class="vertical-align">Имя задавшего:</th>
                            <td>
                                <input type="text" name="faq[faq_name]" value="<?=$faq['faq_name']?>" class="form-control" />
                            </td>
                        </tr>
                       <!-- <tr>
                            <th class="vertical-align">Email задавшего:</th>
                            <td>
                                <input type="text" name="faq[faq_email]" value="<?=$faq['faq_email']?>" class="form-control" />
                            </td>
                        </tr> -->
                        <tr>
                            <th class="vertical-align">Телефон задавшего:</th>
                            <td>
                                <input type="text" name="faq[faq_phone]" value="<?=$faq['faq_phone']?>" class="form-control" />
                            </td>
                        </tr>
                    </table>
                </div>
                <?php foreach ($languages as $k => $lng): ?>
                    <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th class="vertical-align" width="160px">Вопрос:</th>
                                <td>
                                    <div class="">
                                        <input type="text" name="faq[<?=$lng?>][faq_question]" value="<?=$faq[$lng]['faq_question']?>" class="form-control" data-validate="textMaxLenght:255" />
                                        <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> Вопрос"</b> не должно превышать 255 символов</div>
                                    </div>
                                </td>
                            </tr>
                          <!--  <tr>
                                <th class="vertical-align">Ответ:</th>
                                <td>
                                    <textarea id="<?=$lng?>_content" name="faq[<?=$lng?>][faq_answer]" class="form-control" rows="2"><?=$faq[$lng]['faq_answer']?></textarea>
                                </td>
                            </tr>-->
                        </table>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <input type="hidden" name="faq[faq_id]" value="<?=$faq['faq_id']?>" />
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;saveForm('#editForm');"><i class="fa fa-save"></i>&emsp;Сохранить</a>
                </li>
            </ul>
        </div>
    </form>
    <?=$tiny_mce?>