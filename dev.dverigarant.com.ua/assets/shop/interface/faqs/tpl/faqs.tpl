<h2 class="muted">Вопрос - ответ</h2>
<?php if (count($faqs) == 0 || !is_array($faqs)):?>
    <div class="row"><h1 class="screen"><strong>Вопросов не найдено</strong></h1></div>
<?php else: ?>
    <table class="table table-condensed table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th width="70px" style="text-align:center;">ID</th>
                <th style="text-align:center;">Вопрос</th>
                <!--<th style="text-align:center;">Категория</th>-->
                <th width="110px" style="text-align:center;">Статус выполнения:</th>
                <th width="100px" style="text-align:center;">Создано</th>
                <th width="250px" style="text-align:center;">Действие</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($faqs as $faq): ?>
                <tr id="faq-<?=$faq['faq_id']?>">
                    <td class="vertical-align"><?=$faq['faq_id']?></td>
                    <td>&nbsp;
                        <b><?=(!empty($faq['faq_question'])?$faq['faq_question']:$faq['faq_question_rand'])?></b>
                    </td>
                    <!--<td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$faq['faq_category']?></b></td>-->
                    <td class="vertical-align" style="text-align:center;">
                        <b>
                            <?php if($faq['faq_active'] == 0):?><span class="btn btn-danger">Не выполнено</span><?php endif;?>
                            <?php if($faq['faq_active'] == 1):?><span class="btn btn-success">Выполнено</span><?php endif;?>
                        </b>
                    </td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$faq['faq_created']?></b></td>
                    <td class="vertical-align" style="text-align:center;">
                        <a href="<?=$url?>faqs&c=edit&i=<?=$faq['faq_id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a>
                        <a href="<?=$url?>faqs&c=delete&i=<?=$faq['faq_id']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                    </td>
                </tr>
                <?php endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>faqs&c=add"><i class="fa fa-plus-square"></i>&emsp;Создать</a>
        </li>
    </ul>
</div>