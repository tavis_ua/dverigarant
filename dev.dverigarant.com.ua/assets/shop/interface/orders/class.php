<?php

class Orders extends Shop
{
    private $modx = null;
    private $db   = null;

    public function __construct($modx)
    {
        parent::__construct($modx);
        $this->modx = $modx;
        $this->db   = $modx->db;
    }
}