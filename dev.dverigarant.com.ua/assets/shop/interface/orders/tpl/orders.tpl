<h2 class="muted">Заказы магазина</h2>
<?php if (count($orders) == 0 || !is_array($orders)):?>
    <div class="row"><h1 class="screen"><strong>Заказов не найдено</strong></h1></div>
<?php else: ?>
    <table class="table table-condensed table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th width="70px" style="text-align:center;">ID</th>
                <th width="170px" style="text-align:center;">Дата</th>
                <th style="text-align:center;">Клиент</th>
                <th width="120px" style="text-align:center;">Стоимость</th>
                <th width="100px" style="text-align:center;">Статус</th>
                <th width="100px" style="text-align:center;">Оплата</th>
                <!--<th width="100px" style="text-align:center;">Промокод</th>-->
                <th width="100px" style="text-align:center;">Действие</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($orders as $order): ?>
                <?php $client = json_decode($order['order_client'], true);?>
                <tr id="product-<?=$product['product_id']?>" class="<?php if($order['order_buy_click']){echo 'buy_click';}?>">
                    <td class="vertical-align"><?=str_pad($order['order_id'], 6, 0, STR_PAD_LEFT)?></td>
                    <td class="vertical-align">&nbsp;<?=$modx->runSnippet("Shop", ["get" => "getDate", "from" => "Y-m-d H:i:s", "to" => "d. m. Y H:i:s", "date" => $order['order_created']])?></td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<?php echo htmlspecialchars($client['name']);?> (<?php echo htmlspecialchars($client['phone']);?> <?php echo htmlspecialchars($client['email']);?>)</td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<?=number_format($order['order_cost'], 2, '.', ' ')?> <?=mb_strtoupper($order['order_currency'])?></td>
                    <td class="vertical-align" style="text-align:center;">
                        <b>
                            <?php if($order['order_status'] == 0):?><span class="label label-default">Удалён</span><?php endif;?>
                            <?php if($order['order_status'] == 1):?><span class="label label-danger">Новый</span><?php endif;?>
                            <?php if($order['order_status'] == 2):?><span class="label label-info">Отправлен</span><?php endif;?>
                            <?php if($order['order_status'] == 3):?><span class="label label-success">Доставлен</span><?php endif;?>
                        </b>
                    </td>
                    <td class="vertical-align" style="text-align:center;">
                        <b>
                            <?php if($order['order_status_pay'] == 0):?><span class="label label-danger">Не оплачен</span><?php endif;?>
                            <?php if($order['order_status_pay'] == 1):?><span class="label label-success">Оплачен</span><?php endif;?>
                        </b>
                    </td>
                    <!--<td class="vertical-align" style="text-align:center;">&nbsp;<?=$order['order_code']?></td>-->
                   <td class="vertical-align" style="text-align:center;">
                       <a href="<?=$url?>orders&c=edit&i=<?=$order['order_id']?>" class="btn btn-primary"><i class="fa fa-eye"></i>&emsp;Детали</a>
                   </td>
               </tr>
           <?php endforeach;?>
       </tbody>
   </table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
