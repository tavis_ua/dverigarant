<div class="message-panel panel" id="addEqp">
    <table class="table table-condensed table-striped table-bordered table-hover">
        <thead>
            <tr><th colspan="2" id="addEqpLabel">Дополнительная комплектация<button type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button></th></tr>
            <tr><th>Наименование</th><th>Цена</th></tr>
        </thead>
        <tbody id="addEqpBody">

        </tbody>
    </table>
</div>
<form id="editForm" action="<?=$url?>orders&c=update" method="post">
    <h2 class="muted">&emsp;Детали заказа № <?=str_pad($order['order_id'], 6, 0, STR_PAD_LEFT)?><?php if($order['order_buy_click']){echo ' (покупка в 1 клик)';}?></h2>
    <br />
    <div class="container-fluid">
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-success">
                <div class="panel-heading"><i class="fa fa-user-circle" aria-hidden="true"></i>&ensp;<b>Клиент</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tr>
                            <th width="150px">Имя</th>
                            <td><?php echo htmlspecialchars($client['name']);?></td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td><?=$client['phone']?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>
                                <?=$client['email']?>
                                <input type="hidden" name="client[email]" value="<?=$client['email']?>" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-inbox" aria-hidden="true"></i>&ensp;<b>Заказ</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tr>
                            <th width="150px">Дата создания</th>
                            <td><?=$order['order_created']?></td>
                        </tr>
                        <tr>
                            <th>Статус</th>
                            <td>
                                <select name="order[order_status]" class="form-control" data-active="<?=$order['order_status']?>">
                                    <option value="0" <?php if(0 == $order['order_status']) echo 'selected'; ?>>Удалён</option>
                                    <option value="1" <?php if(1 == $order['order_status']) echo 'selected'; ?>>Новый</option>
                                    <option value="2" <?php if(2 == $order['order_status']) echo 'selected'; ?>>Отправлен</option>
                                    <option value="3" <?php if(3 == $order['order_status']) echo 'selected'; ?>>Доставлен</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Оплата</th>
                            <td>
                                <select name="order[order_status_pay]" class="form-control" data-active="<?=$order['order_status_pay']?>">
                                    <option value="0" <?php if(0 == $order['order_status_pay']) echo 'selected'; ?>>Не оплачен</option>
                                    <option value="1" <?php if(1 == $order['order_status_pay']) echo 'selected'; ?>>Оплачен</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Коментарий клиента</th>
                            <td><textarea name="order[order_comment]" cols="30" rows="3" class="form-control" readonly><?=$comment?></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-warning">
                <div class="panel-heading"><i class="fa fa-truck" aria-hidden="true"></i>&ensp;<b>Доп. услуги</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <tr>
                            <th width="150px">Доставка</th>
                            <td>
                                <?php if ($order['order_delivery'] == 1) : ?>
                                <div class="panel-body bg-success">Да</div>
                                <?php else : ?>
                                <div class="panel-body bg-secondary">Нет</div>
                                <?php endif ; ?>
                            </td>
                        </tr>
                        <tr>
                            <th width="150px">Установка</th>
                            <td>
                                <?php if ($order['order_installation'] == 1) : ?>
                                <div class="panel-body bg-success">Да</div>
                                <?php else : ?>
                                <div class="panel-body bg-secondary">Нет</div>
                                <?php endif ; ?>
                            </td>
                        </tr>
                    </table>
                    <!--<table id="delivery-1" class="table table-condensed table-striped table-bordered table-hover" <?php if(1 != $delivery['method']) echo 'style="display:none;"'; ?>>
                        <tr>
                            <th width="150px">Адрес самовывоза</th>
                            <td>
                                <input type="hidden" name="delivery[1][method]" value="1" />
                                <select name="delivery[1][self_city]" class="form-control">
                                    <?php foreach($cities as $key => $value): ?>
                                        <option value="<?=$key?>" <?php if($key == $delivery['self_city']) echo 'selected'; ?>><?=$value?></option>
                                    <?php endforeach;?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table id="delivery-2" class="table table-condensed table-striped table-bordered table-hover" <?php if(2 != $delivery['method']) echo 'style="display:none;"'; ?>>
                        <tr>
                            <th width="150px">Имя</th>
                            <td>
                                <input type="hidden" name="delivery[2][method]" value="2" />
                                <input type="text" name="delivery[2][name]" class="form-control" value="<?=$delivery['name']?>" />
                            </td>
                        </tr>
                        <tr>
                            <th>Телефон</th>
                            <td><input type="text" name="delivery[2][phone]" class="form-control" value="<?=$delivery['phone']?>" /></td>
                        </tr>
                        <tr>
                            <th>Город</th>
                            <td>
                                <input type="text" name="delivery[2][city]" class="form-control" value="<?=$delivery['city']?>" />
                            </td>
                        </tr>
                        <tr>
                            <th>Улица</th>
                            <td><input type="text" name="delivery[2][street]" class="form-control" value="<?=$delivery['street']?>" /></td>
                        </tr>
                        <tr>
                            <th>Дом</th>
                            <td><input type="text" name="delivery[2][house]" class="form-control" value="<?=$delivery['house']?>" /></td>
                        </tr>
                        <tr>
                            <th>Квартира</th>
                            <td><input type="text" name="delivery[2][apartament]" class="form-control" value="<?=$delivery['apartament']?>" /></td>
                        </tr>
                        <!--<tr>
                            <th>Коментарий</th>
                            <td><input type="text" name="delivery[2][comment]" class="form-control" value="<?=$delivery['comment']?>" /></td>
                        </tr>-->
                    <!--</table>
                    <table id="delivery-3" class="table table-condensed table-striped table-bordered table-hover" <?php if(3 != $delivery['method']) echo 'style="display:none;"';?>>
                        <tr>
                            <th>Город</th>
                            <td>
                                <input type="hidden" name="delivery[3][method]" value="3" />
                                <input type="text" name="delivery[3][city]" class="form-control" value="<?=$delivery['city']?>" />
                            </td>
                        </tr>
                        <tr>
                            <th width="150px">Отделение новой почты</th>
                            <td><input type="text" name="delivery[3][departament]" class="form-control" value="<?=$delivery['departament']?>" /></td>
                        </tr>
                    </table>-->
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-cubes" aria-hidden="true"></i>&ensp;<b>Товары</b></div>
                <div class="panel-body">
                    <table class="table table-condensed table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Цена</th>
                                <th>Кол-во</th>
                                <th>Стоимость</th>
                                <th>Доп.компл.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($products as $product): ?>
                                <tr>
                                    <td> <div style="word-wrap: break-word; overflow-wrap: break-word; max-width: 425px;"><?=$product['product_name']?></div></td>
                                    <td><?=$product['product_price']?> <?=mb_strtoupper($product['order_currency'])?></td>
                                    <td><?=$product['product_count']?></td>
                                    <td><?=($product['product_price'] * $product['product_count'])?> <?=mb_strtoupper($product['order_currency'])?></td>
                                    <td><button type="button" class="btn btn-primary" onclick="showAddEqp('<?=$product['addEqp']?>')"><i class="fa fa-eye"></i> Детали</button></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                        <tfoot>
                        <?php if ($order['order_delivery'] == 1) : ?>
                        <tr>
                            <th class="info">Стоимость доставки:</th>
                            <td class="info"><?=$order['order_delivery_cost']?> <?=mb_strtoupper($product['order_currency'])?></td>
                        </tr>
                        <?php endif ; ?>
                        <?php if ($order['order_installation'] == 1) : ?>
                        <tr>
                            <th class="info">Стоимость установки:</th>
                            <td class="info"><?=$order['order_installation_cost']?> <?=mb_strtoupper($product['order_currency'])?></td>
                        </tr>
                        <?php endif ; ?>
                        <tr>
                            <th class="info">Общая сумма заказа:</th>
                            <td class="info">
                                <b><?=($order['order_cost'])?> <?=mb_strtoupper($product['order_currency'])?></b></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <?php if (!empty(trim($order['order_code']))) : ?>
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-qrcode" aria-hidden="true"></i>&nbsp;<b>Промокод</b></div>
                    <div class="panel-body">
                        <table class="table table-condensed table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Скидка</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?=$order['order_code']?></td>
                                    <td><?=$order['order_discount']?> %</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <div class="panel panel-danger">
                <div class="panel-heading"><i class="fa fa-bell" aria-hidden="true"></i>&ensp;<b>Уведомления</b></div>
                <div class="panel-body">
                    <input type="checkbox" name="order[notify][email]" value="1" />&emsp;<b>Уведомить клиента о смене статуса заказа по email</b>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="order[order_id]" value="<?=$order['order_id']?>" />
<input type="hidden" name="order[order_lang]" value="<?=$order['order_lang']?>" />
<div id="actions">
    <ul class="actionButtons">
        <li id="Button1" class="transition">
            <a href="#" class="primary" onclick="documentDirty=false;document.getElementById('editForm').submit();"><i class="fa fa-save"></i>&emsp;Обновить</a>
        </li>
        <li id="Button3" class="transition">
            <a href="<?=$url?>orders"><i class="fa fa-level-up"></i>&emsp;Назад</a>
        </li>
    </ul>
</div>
</form>
<script type="text/javascript">
    function showAddEqp(addEqp) {
        jQuery.ajax({
            url:'index.php?a=2&b=orders&c=getAddEqp&addEpq='+addEqp,
            success:function (result) {
                jQuery('#addEqpBody').html(result);
                jQuery('#addEqp').show();
            }
        });
    }
    jQuery(document).on("click", "button.close", function() {
        jQuery('#addEqp').css('display', 'none');
    });
    jQuery(document).on("change", "[name=\"delivery[method]\"]", function() {
        var idx = jQuery("[name=\"delivery[method]\"] option:selected").val();
        jQuery("#delivery-1").attr("style", "display:none;");
        jQuery("#delivery-2").attr("style", "display:none;");
        jQuery("#delivery-3").attr("style", "display:none;");
        jQuery("#delivery-"+idx).removeAttr("style");
    });
</script>