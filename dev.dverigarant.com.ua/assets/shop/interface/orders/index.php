<?php

include dirname(__FILE__)."/class.php";
$class = new Orders($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

switch ($controller) {
    case "edit":
        $order      = $shop->getOrder($_GET['i']);
        $deliveries = $shop->getOrderDeliveries();
        $cities     = $shop->getSelfCities();
        $products   = $shop->getOrderProducts($_GET['i']);
        $client     = json_decode($order['order_client'], true);
        $comments    = json_decode($order['order_comment'], true);
        foreach ($comments as $key => $item) {
            $comment .= "{$products[$key]['product_name']}: {$item};" . PHP_EOL;
        }
        $delivery   = json_decode($order['order_delivery'], true);
        if (is_numeric($delivery['city']) && (int)$delivery['city'] > 0) {
            $delivery['city'] = $cities[$delivery['city']];
        }
        $tpl = "order.tpl";
        break;
    case "update":
        $modx->db->query("
            UPDATE `modx_a_order` SET
                order_status     = ".(int)$_POST['order']['order_status'].",
                order_status_pay = ".(int)$_POST['order']['order_status_pay'].",
                #order_comment    = '".$modx->db->escape($_POST['order']['order_comment'])."',
                #order_delivery	 = '".$modx->db->escape(json_encode($_POST['delivery'][$_POST['delivery']['method']]))."'
            WHERE order_id = ".(int)$_POST['order']['order_id']."
        ");
        if ((int)$_POST['order']['notify']['email'] && !empty($_POST['client']['email'])) {
            $shop->sendMail(
                $_POST['client']['email'],
                "notyf_stat_email",
                [
                    "order_id" => str_pad((int)$_POST['order']['order_id'], 6, 0, STR_PAD_LEFT),
                    "order_status_name" => $status_names[$_POST['order']['order_lang']][(int)$_POST['order']['order_status']]
                ],
                $_POST['order']['order_lang']
            );
        }
        die(header("Location: ".$url."orders"));
    default:
        $modx->setPlaceholder('url', $url."orders&p=");
        if (isset($_GET['search']) && trim($_GET['search']) != '') {
            $search = $modx->db->escape(trim($_GET['search']));
            $modx->setPlaceholder('url', $url."orders&search=".$search."&p=");
        }
        $orders = $shop->getOrderList($search);
        $pagin  = $shop->getPaginate();
        $tpl = "orders.tpl";
        break;
}