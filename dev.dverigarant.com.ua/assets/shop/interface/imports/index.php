<?php

include dirname(__FILE__)."/class.php";
$class = new Imports($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

// Подключение библиотек работы с Excel
require MODX_BASE_PATH."assets/lib/vendor/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

switch ($controller) {
    case "export": // Экспорт товаров
        $spreadsheet = new Spreadsheet();
        // Установить настройки документа
        $spreadsheet->getProperties()->setCreator('Seiger')
            ->setLastModifiedBy('Seiger')
            ->setTitle('Office 2007 XLSX Document')
            ->setSubject('Office 2007 XLSX Document')
            ->setDescription('Exporting the list of products for the products list')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Export file');
        // Настройки форматирования
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('U')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('V')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('W')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('X')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('Y')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('Z')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('AB')->setWidth(30);
        // Добавляем заголовки
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ИД')
            ->setCellValue('B1', 'Видимость')
            ->setCellValue('C1', 'Наличие')
            ->setCellValue('D1', 'Статус')
            ->setCellValue('E1', 'Ссылка')
            ->setCellValue('F1', 'Артикул')
            ->setCellValue('G1', 'Цена')
            ->setCellValue('H1', 'Цена старая')
            ->setCellValue('I1', 'Вес')
            ->setCellValue('G1', 'Категория (Для хлебных крошек)')
            ->setCellValue('K1', 'Категории')
            ->setCellValue('L1', 'Рекомендуемые товары')
            ->setCellValue('M1', 'Название UA')
            ->setCellValue('N1', 'Аннотация UA')
            ->setCellValue('O1', 'Полное описание UA')
            ->setCellValue('P1', 'Дополнительная информация UA')
            ->setCellValue('Q1', 'Видео UA')
            ->setCellValue('R1', 'Название RU')
            ->setCellValue('S1', 'Аннотация RU')
            ->setCellValue('T1', 'Полное описание RU')
            ->setCellValue('U1', 'Дополнительная информация RU')
            ->setCellValue('V1', 'Видео RU')
            ->setCellValue('W1', 'Название EN')
            ->setCellValue('X1', 'Аннотация EN')
            ->setCellValue('Y1', 'Полное описание EN');
        $spreadsheet->getActiveSheet()->setCellValue('Z1', 'Дополнительная информация EN');
        $spreadsheet->getActiveSheet()->setCellValue('AA1', 'Видео EN');
        $spreadsheet->getActiveSheet()->setCellValue('AB1', 'Фильтр Бренд');
        // Выбрать Товары из БД
        $productsArr = $modx->db->makeArray($modx->db->query("
					SELECT *, 
					    (SELECT GROUP_CONCAT(modx_id) FROM `modx_a_product_categories` WHERE product_id = p.product_id) AS 'product_categories',
					    (SELECT value_id FROM `modx_a_product_filter_values` WHERE filter_id = 1 AND product_id = p.product_id) AS 'filter_brand'
					FROM `modx_a_products` p ORDER BY p.product_id
				"));
        $stringsArr = $modx->db->makeArray($modx->db->query("
					SELECT * FROM `modx_a_product_strings` ORDER BY product_id
				"));
        $strings = [];
        foreach ($stringsArr as $str) {
            $strings[$str['product_id']][$str['string_locate']] = $str;
        }
        // Обработка списка
        if (is_array($productsArr)) {
            $i = 1;
            foreach ($productsArr as $product) {
                ++$i;
                $spreadsheet->getActiveSheet()->setCellValue('A' .$i, $product['product_id']);
                $spreadsheet->getActiveSheet()->setCellValue('B' .$i, $product['product_active']);
                $spreadsheet->getActiveSheet()->setCellValue('C' .$i, $product['product_availability']);
                $spreadsheet->getActiveSheet()->setCellValue('D' .$i, $product['product_status']);
                $spreadsheet->getActiveSheet()->setCellValue('E' .$i, $product['product_url']);
                $spreadsheet->getActiveSheet()->setCellValue('F' .$i, $product['product_code']." ");
                $spreadsheet->getActiveSheet()->setCellValue('G' .$i, $product['product_price']);
                $spreadsheet->getActiveSheet()->setCellValue('H' .$i, $product['product_price_old']);
                $spreadsheet->getActiveSheet()->setCellValue('I' .$i, $product['product_weight']);
                $spreadsheet->getActiveSheet()->setCellValue('J' .$i, $product['product_category']);
                $spreadsheet->getActiveSheet()->setCellValue('K' .$i, $product['product_categories']);
                $spreadsheet->getActiveSheet()->setCellValue('L' .$i, $product['product_recommender']);
                $spreadsheet->getActiveSheet()->setCellValue('M' .$i, $strings[$product['product_id']]['ua']['product_name']);
                $spreadsheet->getActiveSheet()->setCellValue('N' .$i, $strings[$product['product_id']]['ua']['product_introtext']);
                $spreadsheet->getActiveSheet()->setCellValue('O' .$i, $strings[$product['product_id']]['ua']['product_description']);
                $spreadsheet->getActiveSheet()->setCellValue('P' .$i, $strings[$product['product_id']]['ua']['product_additional']);
                $spreadsheet->getActiveSheet()->setCellValue('Q' .$i, $strings[$product['product_id']]['ua']['product_content']);
                $spreadsheet->getActiveSheet()->setCellValue('R' .$i, $strings[$product['product_id']]['ru']['product_name']);
                $spreadsheet->getActiveSheet()->setCellValue('S' .$i, $strings[$product['product_id']]['ru']['product_introtext']);
                $spreadsheet->getActiveSheet()->setCellValue('T' .$i, $strings[$product['product_id']]['ru']['product_description']);
                $spreadsheet->getActiveSheet()->setCellValue('U' .$i, $strings[$product['product_id']]['ru']['product_additional']);
                $spreadsheet->getActiveSheet()->setCellValue('V' .$i, $strings[$product['product_id']]['ru']['product_content']);
                $spreadsheet->getActiveSheet()->setCellValue('W' .$i, $strings[$product['product_id']]['en']['product_name']);
                $spreadsheet->getActiveSheet()->setCellValue('X' .$i, $strings[$product['product_id']]['en']['product_introtext']);
                $spreadsheet->getActiveSheet()->setCellValue('Y' .$i, $strings[$product['product_id']]['en']['product_description']);
                $spreadsheet->getActiveSheet()->setCellValue('Z'.$i, $strings[$product['product_id']]['en']['product_additional']);
                $spreadsheet->getActiveSheet()->setCellValue('AA'.$i, $strings[$product['product_id']]['en']['product_content']);
                $spreadsheet->getActiveSheet()->setCellValue('AB'.$i, $product['filter_brand']);
            }
        }
        // Имя вкладки
        $spreadsheet->getActiveSheet()->setTitle("Экспорт товаров");
        // Установите активный индекс листа на первый лист, так что Excel открывает это как первый лист
        $spreadsheet->setActiveSheetIndex(0);
        // Перенаправить вывод в веб-браузер клиента
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Products.xlsx"');
        header('Cache-Control: max-age=0');
        // Для IE 9, может потребоваться следующее
        header('Cache-Control: max-age=1');
        // Для IE через SSL, может понадобиться следующее:
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        die;
        break;
    case "import": // Импорт товаров
        if (isset($_FILES['file']['tmp_name']) && !empty($_FILES['file']['tmp_name'])) {
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            if ($extension == "xls" || $extension == "xlsx") {
                $spreadsheet = IOFactory::load($_FILES['file']['tmp_name']);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                if (is_array($sheetData)) {
                    $f = 0;
                    unset($sheetData[1]);
                    $heads = $sheetData[2];
                    if (is_array($heads)) {
                        foreach ($heads as $k => $v) {
                            if (!empty($v)) {
                                switch ($cell = mb_strtolower(trim($v))) {
                                    case 'id':
                                        $head['product_id'] = $k;
                                        break;
                                    case 'is_parent':
                                        $head['is_parent'] = $k;
                                        break;
                                    case 'active':
                                        $head['product_active'] = $k;
                                        break;
                                    case 'availability':
                                        $head['product_availability'] = $k;
                                        break;
                                    case 'type':
                                        $head['product_type'] = $k;
                                        break;
                                    case 'status':
                                        $head['product_status'] = $k;
                                        break;
                                    case 'alias':
                                        $head['product_url'] = $k;
                                        break;
                                    case 'p_code':
                                        $head['product_code'] = $k;
                                        break;
                                    case 'code':
                                        $head['child_code'] = $k;
                                        break;
                                    case 'price':
                                        $head['product_price'] = $k;
                                        break;
                                    case 'price_old':
                                        $head['product_price_old'] = $k;
                                        break;
                                    case 'category_1_ua':
                                        $head['product_category_1_ua'] = $k;
                                        break;
                                    case 'category_2_ua':
                                        $head['product_category_2_ua'] = $k;
                                        break;
                                    case 'category_3_ua':
                                        $head['product_category_3_ua'] = $k;
                                        break;
                                    case 'category_1_ru':
                                        $head['product_category_1_ru'] = $k;
                                        break;
                                    case 'category_2_ru':
                                        $head['product_category_2_ru'] = $k;
                                        break;
                                    case 'category_3_ru':
                                        $head['product_category_3_ru'] = $k;
                                        break;
                                    case 'categories_ua':
                                        $head['product_categories_ua'] = $k;
                                        break;
                                    case 'categories_ru':
                                        $head['product_categories_ru'] = $k;
                                        break;
                                    case 'p_name_ua':
                                        $head['product_name_ua'] = $k;
                                        break;
                                    case 'p_name_ru':
                                        $head['product_name_ru'] = $k;
                                        break;
                                    case 'name_ua':
                                        $head['name_ua'] = $k;
                                        break;
                                    case 'name_ru':
                                        $head['name_ru'] = $k;
                                        break;
                                    case 'description_ua':
                                        $head['product_description_ua'] = $k;
                                        break;
                                    case 'description_ru':
                                        $head['product_description_ru'] = $k;
                                        break;
                                    default:
                                        if (stristr($cell, 'f_ua')) {
                                            $head[$cell] = $k;
                                            $f++;
                                        } elseif (stristr($cell, 'f_ru')) {
                                            $head[$cell] = $k;
                                            $f++;
                                        } elseif (stristr($cell, 'f_en')) {
                                            $head[$cell] = $k;
                                            $f++;
                                        } else {
                                            $head[$k] = $k;
                                        }
                                        break;
                                }
                            }
                        }
                        unset($sheetData[2]);
                        foreach ($sheetData as $value) {
                            $products[trim($value[$head['product_code']])][trim($value[$head['child_code']])] = $value; // масив продуктов, дети внутри родителя
                        }
                        foreach ($products as $key => $value) {
                            if ($key != '') {
                                $product_db = $modx->db->makeArray($modx->db->query("
                                    SELECT * 
                                    FROM `modx_a_products`
                                    WHERE `product_code` = '{$key}'
                                "));
                                $tmp_value = $value;
                                $parent = array_shift($tmp_value);
                                unset($tmp_value);
                                if ($parent[$head['product_type']] == 'Скидка') {
                                    $parent_status = 2;
                                } elseif ($parent[$head['product_type']] == 'Акция') {
                                    $parent_status = 3;
                                } elseif ($parent[$head['product_type']] == 'Хит') {
                                    $parent_status = 4;
                                } else {
                                    $parent_status = 1;
                                }
                                $alias = $parent[$head['product_url']] != '' ? $shop->validateUrl($parent[$head['product_url']]) : $shop->setUrl($parent[$head['product_name_ua']], "modx_a_products", "product_url");
                                $alias = str_replace('/', '-', $alias);

                                if ($parent[$head['product_categories_ua']] != '') { // начинаем разбирать категории продукта

                                    $parent_categories_arr_strs = explode(',', $parent[$head['product_categories_ua']]);
                                    $parent_categories_arr_strs = array_map('trim', $parent_categories_arr_strs);
                                    $k = 0;
                                    foreach ($parent_categories_arr_strs as $parent_categories_arr_str) {
                                        $parent_categories_arr = explode('>', $parent_categories_arr_str);
                                        $parent_categories_arr = array_map('trim', $parent_categories_arr);
                                        foreach ($parent_categories_arr as $parent_categories_arr_items) {
                                            $parent_categories_sorted[$k]['ua'][] = $parent_categories_arr_items;
                                        }
                                        $k++;
                                    }
                                    if ($parent[$head['product_categories_ru']] != '') {
                                        $parent_categories_arr_strs = explode(',', $parent[$head['product_categories_ru']]);
                                        $parent_categories_arr_strs = array_map('trim', $parent_categories_arr_strs);
                                        $k = 0;
                                        foreach ($parent_categories_arr_strs as $parent_categories_arr_str) {
                                            $parent_categories_arr = explode('>', $parent_categories_arr_str);
                                            $parent_categories_arr = array_map('trim', $parent_categories_arr);
                                            foreach ($parent_categories_arr as $parent_categories_arr_items) {
                                                $parent_categories_sorted[$k]['ru'][] = $parent_categories_arr_items;
                                            }
                                            $k++;
                                        }
                                    }
                                    //получили масив категорий
                                    foreach ($parent_categories_sorted as $ckey => $parent_category_sorted) {
                                        if ($parent_category_sorted['ua'][0] != '') { //Начинаем разбирать категории
                                            $product_category_1_db = $modx->db->getRow($modx->db->query("
                                            SELECT `id`, `pagetitle_ua`, `pagetitle_ru`
                                            FROM `modx_site_content`
                                            WHERE `pagetitle_ua` = '" . $modx->db->escape($parent_category_sorted['ua'][0]) . "'
                                        "));
                                            if (is_array($product_category_1_db) && $modx->db->escape($product_category_1_db['pagetitle_ua']) == $modx->db->escape($parent_category_sorted['ua'][0])) {
                                                $parent_categories_ids[$ckey][] = $product_category_1_db['id'];
                                                if ($parent_category_sorted['ua'][1] != '') {
                                                    $product_category_2_db = $modx->db->getRow($modx->db->query("
                                                    SELECT `id`, `pagetitle_ua`, `pagetitle_ru`
                                                    FROM `modx_site_content`
                                                    WHERE `pagetitle_ua` = '" . $modx->db->escape($parent_category_sorted['ua'][1]) . "'
                                                "));
                                                    if (is_array($product_category_2_db) && $modx->db->escape($product_category_2_db['pagetitle_ua']) == $modx->db->escape($parent_category_sorted['ua'][1])) {
                                                        $parent_categories_ids[$ckey][] = $product_category_2_db['id'];
                                                        if ($parent_category_sorted['ua'][2] != '') {
                                                            $product_category_3_db = $modx->db->getRow($modx->db->query("
                                                            SELECT `id`, `pagetitle_ua`, `pagetitle_ru`
                                                            FROM `modx_site_content`
                                                            WHERE `pagetitle_ua` = '" . $modx->db->escape($parent_category_sorted['ua'][2]) . "'
                                                        "));
                                                            if (is_array($product_category_3_db) && $modx->db->escape($product_category_3_db['pagetitle_ua']) == $modx->db->escape($parent_category_sorted['ua'][2])) {
                                                                $parent_categories_ids[$ckey][] = $product_category_3_db['id'];
                                                            } else { //если нет категории третьего уровня вложенности
                                                                if ($parent_category_sorted['ua'][2] != '') {
                                                                    $modx->db->query("
                                                                    INSERT INTO `modx_site_content` SET
                                                                        `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][2])) . "',
                                                                        `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][2])) . "',
                                                                        `published`     = '0',
                                                                        `parent`        = '{$product_category_2_db['id']}',
                                                                        `isfolder`      = '1',
                                                                        `template`      = '19',
                                                                        `cacheable`     = '0',
                                                                        `menuindex`     = '999999',
                                                                        `alias_visible` = '0'
                                                                ");
                                                                    $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                                }
                                                            }
                                                        }
                                                    } else { //если нет категории второго уровня вложенности
                                                        if ($parent_category_sorted['ua'][1] != '') {
                                                            $modx->db->query("
                                                            INSERT INTO `modx_site_content` SET
                                                                `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][1])) . "',
                                                                `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][1])) . "',
                                                                `published`     = '0',
                                                                `parent`        = '{$product_category_1_db['id']}',
                                                                `isfolder`      = '1',
                                                                `template`      = '19',
                                                                `cacheable`     = '0',
                                                                `menuindex`     = '999999',
                                                                `alias_visible` = '0'
                                                        ");
                                                            $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                            if ($parent_category_sorted['ua'][2] != '') {
                                                                $cid = $modx->db->getInsertId();
                                                                $modx->db->query("
                                                                INSERT INTO `modx_site_content` SET
                                                                    `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][2])) . "',
                                                                    `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][2])) . "',
                                                                    `published`     = '0',
                                                                    `parent`        = '{$cid}',
                                                                    `isfolder`      = '1',
                                                                    `template`      = '19',
                                                                    `cacheable`     = '0',
                                                                    `menuindex`     = '999999',
                                                                    `alias_visible` = '0'
                                                            ");
                                                                $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                            }
                                                        }
                                                    }
                                                }
                                            } else { //если нет категории первого уровня вложенности
                                                $modx->db->query("
                                                INSERT INTO `modx_site_content` SET
                                                    `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][0])) . "',
                                                    `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][0])) . "',
                                                    `published`     = '0',
                                                    `parent`        = '3',
                                                    `isfolder`      = '1',
                                                    `template`      = '19',
                                                    `cacheable`     = '0',
                                                    `menuindex`     = '999999',
                                                    `alias_visible` = '0'
                                            ");
                                                $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                if ($parent_category_sorted['ua'][1] != '') {
                                                    $cid = $modx->db->getInsertId();
                                                    $modx->db->query("
                                                    INSERT INTO `modx_site_content` SET
                                                        `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][1])) . "',
                                                        `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][1])) . "',
                                                        `published`     = '0',
                                                        `parent`        = '{$cid}',
                                                        `isfolder`      = '1',
                                                        `template`      = '19',
                                                        `cacheable`     = '0',
                                                        `menuindex`     = '999999',
                                                        `alias_visible` = '0'
                                                ");
                                                    $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                    if ($parent_category_sorted['ua'][2] != '') {
                                                        $cid = $modx->db->getInsertId();
                                                        $modx->db->query("
                                                        INSERT INTO `modx_site_content` SET
                                                            `pagetitle_ua`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ua'][2])) . "',
                                                            `pagetitle_ru`  = '" . htmlspecialchars($modx->db->escape($parent_category_sorted['ru'][2])) . "',
                                                            `published`     = '0',
                                                            `parent`        = '{$cid}',
                                                            `isfolder`      = '1',
                                                            `template`      = '19',
                                                            `cacheable`     = '0',
                                                            `menuindex`     = '999999',
                                                            `alias_visible` = '0'
                                                    ");
                                                        $parent_categories_ids[$ckey][] = $modx->db->getInsertId();
                                                    }
                                                }
                                            }
                                        }//заканчиваем разбирать категории, получили $parent_categories и $parent_category
                                    }
                                    foreach ($parent_categories_ids as $parent_category_ids) {
                                        $parent_categories[] = array_pop($parent_category_ids);
                                    }
                                }
                                if (count($product_db) > 0) {
                                    //обновляем продукт радитель если такой уже есть
                                    $modx->db->query("
                                        UPDATE `modx_a_products` SET 
                                            `product_active`              = '" . (int)$parent[$head['product_active']] . "',
                                            `product_availability`        = '" . (int)$parent[$head['product_availability']] . "',
                                            `product_type`                = '" . ($parent[$head['product_type']] == 'Груповой' ? 0 : 1) . "',
                                            `product_status`              = '" . (int)$parent_status . "',
                                            `product_category`            = '" . $parent_categories[0] . "',
                                            `product_price`               = '" . (float)$parent[$head['product_price']] . "',
                                            `product_price_old`           = '" . (float)$parent[$head['product_price_old']] . "'
                                        WHERE `product_id` = '{$product_db[0]['product_id']}'
                                    ");//обновляем строки продукт радитель если такой уже есть
                                    $modx->db->query("
                                        UPDATE `modx_a_product_strings` SET
                                            `product_name`            = '" . htmlspecialchars($modx->db->escape($parent[$head['product_name_ua']])) . "',
                                            `product_description`     = '" . htmlspecialchars($modx->db->escape($parent[$head['product_description_ua']])) . "'
                                        WHERE `product_id` = '{$product_db[0]['product_id']}' AND `string_locate` = 'ua'
                                    ");//обновляем строки продукт радитель если такой уже есть
                                    $modx->db->query("
                                        UPDATE `modx_a_product_strings` SET
                                            `product_name`            = '" . htmlspecialchars($modx->db->escape($parent[$head['product_name_ru']])) . "',
                                            `product_description`     = '" . htmlspecialchars($modx->db->escape($parent[$head['product_description_ru']])) . "'
                                        WHERE `product_id` = '{$product_db[0]['product_id']}' AND `string_locate` = 'ru'
                                    ");
                                    //удаляем старые связи с категориями
                                    $modx->db->query("
                                        DELETE FROM `modx_a_product_categories`
                                        WHERE `product_id` = '{$product_db[0]['product_id']}'
                                    ");
                                    //привязываемс к новым категориям
                                    foreach ($parent_categories as $v) {
                                        $modx->db->query("
                                            INSERT INTO `modx_a_product_categories` SET
                                                `product_id` = '" . $product_db[0]['product_id'] . "',
                                                `modx_id`    = '" . $v . "'
                                        ");
                                    }
//                                    // разбор фильтров
//                                    if ((int)($f/3) > 0) {
//                                        for ($cnt = 1; $cnt <= $f / 3; $cnt++) {
//                                            $filters_arr_sorted[$cnt]['ua'] = explode(':', substr($parent[$head['f_ua-' . $cnt]], 3));
//                                            $filters_arr_sorted[$cnt]['ru'] = explode(':', substr($parent[$head['f_ru-' . $cnt]], 3));
//                                            $filters_arr_sorted[$cnt]['en'] = explode(':', substr($parent[$head['f_en-' . $cnt]], 3));
//                                            if (stristr($parent[$head['f_ua-' . $cnt]], 'Ф_')) {
//                                                $filters_arr_sorted[$cnt]['type'] = 1;
//                                            } elseif (stristr($parent[$head['f_ua-' . $cnt]], 'Х_')) {
//                                                $filters_arr_sorted[$cnt]['type'] = 0;
//                                            }
//                                        }
//                                        foreach ($filters_arr_sorted as $filters_arr_sorted_item) {
//                                            if (!empty($filters_arr_sorted_item["ua"][0])) {
//                                                $f_alias = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->setUrl($filters_arr_sorted_item["ua"][0], "modx_a_filters", "filter_url");
//                                                $f_alias = str_replace('/', '-', $f_alias);
//                                                $fv_alias = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->setUrl($filters_arr_sorted_item["ua"][1], "modx_a_filter_values", "filter_url");
//                                                $fv_alias = str_replace('/', '-', $fv_alias);
//
//                                                $f_alias_search = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->validateUrl($filters_arr_sorted_item["ua"][0]);
//                                                $f_alias_search = str_replace('/', '-', $f_alias_search);
//                                                $fv_alias_search = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->validateUrl($filters_arr_sorted_item["ua"][1]);
//                                                $fv_alias_search = str_replace('/', '-', $fv_alias_search);
//
//                                                // проверка есть ли такой фильтр
//                                                $filter_db_id = $modx->db->getValue($modx->db->query("
//                                                        SELECT `filter_id`
//                                                        FROM `modx_a_filters`
//                                                        WHERE `filter_url` = '{$f_alias_search}'
//                                                    "));
//                                                if (isset($filter_db_id) && (int)$filter_db_id > 0) {
//                                                    // проверка есть ли такая вариация фильтра
//                                                    $filter_value_db_id_query = "
//                                                            SELECT `value_id`
//                                                            FROM `modx_a_filter_values`
//                                                            WHERE `filter_url` = '".$filter_db_id."-".$fv_alias_search."' AND `filter_id` = '{$filter_db_id}'
//                                                        ";
//                                                    $filter_value_db_id = $modx->db->getValue($modx->db->query($filter_value_db_id_query));
//                                                    if (isset($filter_value_db_id) && (int)$filter_value_db_id > 0) {
//                                                        // если все есть, подвязываем фильтр к категориям продукта
//                                                        foreach ($parent_categories as $modx_id_item) {
//                                                            $modx->db->query("
//                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
//                                                                    `filter_id` = '{$filter_db_id}',
//                                                                    `modx_id`   = '{$modx_id_item}'
//                                                            ");
//                                                        }
//                                                    } else {
//                                                        // если нет вариации фильтра, создать ее
//                                                        $modx->db->query("
//                                                            INSERT INTO `modx_a_filter_values` SET
//                                                                `filter_id`       = '{$filter_db_id}',
//                                                                `filter_url`      = '".$filter_db_id."-".$fv_alias."',
//                                                                `filter_value_ua` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
//                                                                `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
//                                                        ");
//
//                                                        $filter_value_db_id = $modx->db->getInsertId();
//                                                        $shop->console_log("родитель; код - {$key}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");
//                                                        // подвязываем фильтр к категориям продукта
//                                                        foreach ($parent_categories as $modx_id_item) {
//                                                            $modx->db->query("
//                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
//                                                                    `filter_id` = '{$filter_db_id}',
//                                                                    `modx_id`   = '{$modx_id_item}'
//                                                            ");
//                                                        }
//                                                    }
//                                                } else {
//                                                    // если в базе нет такого фильтра, пишем все
//                                                    $modx->db->query("
//                                                        INSERT INTO `modx_a_filters` SET
//                                                            `filter_feature` = '{$filters_arr_sorted_item['type']}',
//                                                            `filter_type`    = '5',
//                                                            `filter_url`     = '" . $f_alias . "'
//                                                    ");
//                                                    $fid = $modx->db->getInsertId();
//                                                    $shop->console_log("родитель; код - {$key}; новый фильтр => {$f_alias} - id = {$fid}");
//                                                    $modx->db->query("
//                                                        INSERT INTO `modx_a_filter_strings` SET
//                                                            `string_locate` = 'ua',
//                                                            `filter_id`     = '{$fid}',
//                                                            `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ua"][0])) . "'
//                                                    ");
//                                                    $modx->db->query("
//                                                        INSERT INTO `modx_a_filter_strings` SET
//                                                            `string_locate` = 'ru',
//                                                            `filter_id`     = '{$fid}',
//                                                            `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ru"][0])) . "'
//                                                    ");
//                                                    $modx->db->query("
//                                                        INSERT INTO `modx_a_filter_values` SET
//                                                            `filter_id`       = '{$fid}',
//                                                            `filter_url`      = '".$fid."-".$fv_alias."',
//                                                            `filter_value_ua` =w '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
//                                                            `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
//                                                    ");
//                                                    $filter_value_db_id = $modx->db->getInsertId();
//                                                    $shop->console_log("родитель; код - {$key}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");
//
//                                                    foreach ($parent_categories as $modx_id_item) {
//                                                        $modx->db->query("
//                                                            INSERT IGNORE INTO `modx_a_filter_categories` SET
//                                                                `filter_id` = '{$fid}',
//                                                                `modx_id`   = '{$modx_id_item}'
//                                                        ");
//                                                    }
//                                                    $filter_db_id = $fid;
//                                                }
////                                                $filters_arr_to_pfv[$filter_db_id] = $filter_value_db_id;
//                                                $modx->db->query("
//                                                    DELETE FROM `modx_a_product_filter_values`
//                                                    WHERE `product_id` = '{$product_db[0]['product_id']}' AND `filter_id`  = '{$filter_db_id}'
//                                                ");
//                                                $shop->console_log("код - {$key}; фильтр - {$filters_arr_sorted_item["ua"][0]}; фильтр велью - {$filters_arr_sorted_item["ua"][1]}");
//                                                //привязываем фильтра к продукту
//                                                $modx->db->query("
//                                                    INSERT IGNORE INTO `modx_a_product_filter_values` SET
//                                                        `product_id` = '{$product_db[0]['product_id']}',
//                                                        `filter_id`  = '{$filter_db_id}',
//                                                        `value_id`   = '{$filter_value_db_id}'
//                                                ");
//                                            }
//                                        }
//                                    }
                                    // закончили разбирать фильтра родителя(дефолтного товара)
                                    foreach ($value as $k => $item) {
                                        $product_option_db = $modx->db->makeArray($modx->db->query("
                                        SELECT * 
                                        FROM `modx_a_product_options`
                                        WHERE `product_code` = '{$k}' AND `product` = '{$product_db[0]['product_id']}'
                                    "));
                                        if (count($product_option_db) > 0) {
                                            //обновляем опцию продукта если такой уже есть
                                            $modx->db->query("
                                                UPDATE modx_a_product_options SET 
                                                    `active`          = '" . (int)$item[$head['product_active']] . "',
                                                    `availability`    = '" . (int)$item[$head['product_availability']] . "',
                                                    `price`           = '" . (float)$item[$head['product_price']] . "',
                                                    `price_old`       = '" . (float)$item[$head['product_price_old']] . "',
                                                    `ua_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ua']])) . "',
                                                    `ua_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ua']])) . "',
                                                    `ru_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ru']])) . "',
                                                    `ru_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ru']])) . "'
                                                WHERE `product_code` = '{$k}' AND `product` = '{$product_db[0]['product_id']}'
                                            ");
                                            $poid = $product_option_db[0]['id'];
                                        } else {
                                            //дабавляем опцию продукта если такой нет
                                            $modx->db->query("
                                                INSERT INTO `modx_a_product_options` SET
                                                    `active`          = '" . (int)$item[$head['product_active']] . "',
                                                    `availability`    = '" . (int)$item[$head['product_availability']] . "',
                                                    `price`           = '" . (float)$item[$head['product_price']] . "',
                                                    `price_old`       = '" . (float)$item[$head['product_price_old']] . "',
                                                    `ua_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ua']])) . "',
                                                    `ua_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ua']])) . "',
                                                    `ru_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ru']])) . "',
                                                    `ru_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ru']])) . "',
                                                    `product_code`    = '" . $modx->db->escape($k) . "',
                                                    `product`         = '{$product_db[0]['product_id']}'
                                            ");
                                            $poid = $modx->db->getInsertId();
                                        }
                                        // разбор фильтров детей
                                        if ((int)($f/3) > 0) {
                                            for ($cnt = 1; $cnt <= $f / 3; $cnt++) {
                                                $filters_arr_sorted[$cnt]['ua'] = explode(':', substr($item[$head['f_ua-' . $cnt]], 3));
                                                $filters_arr_sorted[$cnt]['ru'] = explode(':', substr($item[$head['f_ru-' . $cnt]], 3));
                                                $filters_arr_sorted[$cnt]['en'] = explode(':', substr($item[$head['f_en-' . $cnt]], 3));
                                                if (stristr($item[$head['f_ua-' . $cnt]], 'Ф_')) {
                                                    $filters_arr_sorted[$cnt]['type'] = 1;
                                                } elseif (stristr($item[$head['f_ua-' . $cnt]], 'Х_')) {
                                                    $filters_arr_sorted[$cnt]['type'] = 0;
                                                }
                                            }
                                            foreach ($filters_arr_sorted as $filters_arr_sorted_item) {
                                                if (!empty($filters_arr_sorted_item["ua"][0])) {
                                                    $f_alias = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->setUrl($filters_arr_sorted_item["ua"][0], "modx_a_filters", "filter_url");
                                                    $f_alias = str_replace('/', '-', $f_alias);
                                                    $fv_alias = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->setUrl($filters_arr_sorted_item["ua"][1], "modx_a_filter_values", "filter_url");
                                                    $fv_alias = str_replace('/', '-', $fv_alias);

                                                    $f_alias_search = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->validateUrl($filters_arr_sorted_item["ua"][0]);
                                                    $f_alias_search = str_replace('/', '-', $f_alias_search);
                                                    $fv_alias_search = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->validateUrl($filters_arr_sorted_item["ua"][1]);
                                                    $fv_alias_search = str_replace('/', '-', $fv_alias_search);

                                                    // проверка есть ли такой фильтр
                                                    $filter_db_id = $modx->db->getValue($modx->db->query("
                                                        SELECT `filter_id`
                                                        FROM `modx_a_filters`
                                                        WHERE `filter_url` = '{$f_alias_search}'
                                                    "));
                                                    if (isset($filter_db_id) && (int)$filter_db_id > 0) {
                                                        // проверка есть ли такая вариация фильтра
                                                        $filter_value_db_id_query = "
                                                            SELECT `value_id`
                                                            FROM `modx_a_filter_values`
                                                            WHERE `filter_url` = '".$filter_db_id."-".$fv_alias_search."' AND `filter_id` = '{$filter_db_id}'
                                                        ";
                                                        $filter_value_db_id = $modx->db->getValue($modx->db->query($filter_value_db_id_query));
                                                        if (isset($filter_value_db_id) && (int)$filter_value_db_id > 0) {
                                                            // если все есть, подвязываем фильтр к категориям продукта
                                                            foreach ($parent_categories as $modx_id_item) {
                                                                $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$filter_db_id}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                            }
                                                        } else {
                                                            // если нет вариации фильтра, создать ее
                                                            $modx->db->query("
                                                            INSERT INTO `modx_a_filter_values` SET
                                                                `filter_id`       = '{$filter_db_id}',
                                                                `filter_url`      = '".$filter_db_id."-".$fv_alias."',
                                                                `filter_value_ua` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
                                                                `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
                                                        ");
                                                            $filter_value_db_id = $modx->db->getInsertId();
                                                            $shop->console_log("ребенок; код - {$k}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");
                                                            // подвязываем фильтр к категориям продукта
                                                            foreach ($parent_categories as $modx_id_item) {
                                                                $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$filter_db_id}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                            }
                                                        }
                                                    } else {
                                                        // если в базе нет такого фильтра, пишем все
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filters` SET
                                                                `filter_feature` = '{$filters_arr_sorted_item['type']}',
                                                                `filter_type`    = '5',
                                                                `filter_url`     = '" . $f_alias . "'
                                                        ");
                                                        $fid = $modx->db->getInsertId();
                                                        $shop->console_log("ребенок; код - {$k}; новый фильтр => {$f_alias} - id = {$fid}");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_strings` SET
                                                                `string_locate` = 'ua',
                                                                `filter_id`     = '{$fid}',
                                                                `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ua"][0])) . "'
                                                        ");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_strings` SET
                                                                `string_locate` = 'ru',
                                                                `filter_id`     = '{$fid}',
                                                                `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ru"][0])) . "'
                                                        ");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_values` SET
                                                                `filter_id`       = '{$fid}',
                                                                `filter_url`      = '".$fid."-".$fv_alias."',
                                                                `filter_value_ua` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
                                                                `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
                                                        ");
                                                        $filter_value_db_id = $modx->db->getInsertId();
                                                        $shop->console_log("ребенок; код - {$k}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");

                                                        foreach ($parent_categories as $modx_id_item) {
                                                            $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$fid}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                        }
                                                        $filter_db_id = $fid;
                                                    }
//                                                $filters_arr_to_pfv[$filter_db_id] = $filter_value_db_id;
                                                    $modx->db->query("
                                                        DELETE FROM `modx_a_product_filter_values`
                                                        WHERE `product_id` = '{$poid}' AND `filter_id`  = '{$filter_db_id}'
                                                    ");
                                                    $shop->console_log("код - {$k}; фильтр - {$filters_arr_sorted_item["ua"][0]}; фильтр велью - {$filters_arr_sorted_item["ua"][1]}");
                                                    $modx->db->query("
                                                        INSERT IGNORE INTO `modx_a_product_filter_values` SET
                                                            `product_id` = '{$poid}',
                                                            `filter_id`  = '{$filter_db_id}',
                                                            `value_id`   = '{$filter_value_db_id}'
                                                    ");
                                                }
                                            }
                                        }
                                        // закончили разбирать фильтра
                                    }
                                } else {
                                    //добавляем продукт радитель если такого нет
                                    $modx->db->query("
                                        INSERT INTO `modx_a_products` SET
                                            `product_active`              = '" . (int)$parent[$head['product_active']] . "',
                                            `product_availability`        = '" . (int)$parent[$head['product_availability']] . "',
                                            `product_type`                = '" . ($parent[$head['product_type']] == 'Груповой' ? 0 : 1) . "',
                                            `product_status`              = '" . (int)$parent_status . "',
                                            `product_category`            = '" . $parent_categories[0] . "',
                                            `product_url`                 = '" . $alias . "',
                                            `product_code`                = '" . $modx->db->escape($parent[$head['product_code']]) . "',
                                            `product_price`               = '" . (float)$parent[$head['product_price']] . "',
                                            `product_price_old`           = '" . (float)$parent[$head['product_price_old']] . "'
                                    ");

                                    $pid = $modx->db->getInsertId();

                                    $modx->db->query("
                                        INSERT INTO `modx_a_product_strings` SET
                                            `product_id`              = '" . $pid . "',
                                            `string_locate`           = 'ua',
                                            `product_name`            = '" . htmlspecialchars($modx->db->escape($parent[$head['product_name_ua']])) . "',
                                            `product_description`     = '" . htmlspecialchars($modx->db->escape($parent[$head['product_description_ua']])) . "'
                                    ");
                                    $modx->db->query("
                                        INSERT INTO `modx_a_product_strings` SET
                                            `product_id`              = '" . $pid . "',
                                            `string_locate`           = 'ru',
                                            `product_name`            = '" . htmlspecialchars($modx->db->escape($parent[$head['product_name_ru']])) . "',
                                            `product_description`     = '" . htmlspecialchars($modx->db->escape($parent[$head['product_description_ru']])) . "'
                                    ");
                                    //привязываемс к новым категориям
                                    foreach ($parent_categories as $v) {
                                        $modx->db->query("
                                            INSERT INTO `modx_a_product_categories` SET
                                                `product_id` = '" . $pid . "',
                                                `modx_id`    = '" . $v . "'
                                        ");
                                    }
                                    $j = 1;
                                    foreach ($value as $k => $item) {
                                        //дабавляем опцию продукта
                                        $modx->db->query("
                                                INSERT INTO `modx_a_product_options` SET
                                                    `active`          = '" . (int)$item[$head['product_active']] . "',
                                                    `availability`    = '" . (int)$item[$head['product_availability']] . "',
                                                    `default_option`  = '" . ($j == 1 ? 1 : 0) . "',
                                                    `price`           = '" . (float)$item[$head['product_price']] . "',
                                                    `price_old`       = '" . (float)$item[$head['product_price_old']] . "',
                                                    `ua_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ua']])) . "',
                                                    `ua_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ua']])) . "',
                                                    `ru_name`         = '" . htmlspecialchars($modx->db->escape($item[$head['name_ru']])) . "',
                                                    `ru_description`  = '" . htmlspecialchars($modx->db->escape($item[$head['product_description_ru']])) . "',
                                                    `product_code`    = '" . $modx->db->escape($k) . "',
                                                    `product`         = '" . (int)$pid . "'
                                            ");
                                        $poid = $modx->db->getInsertId();
                                        $j++;
                                        // разбор фильтров детей
                                        if ((int)($f/3) > 0) {
                                            for ($cnt = 1; $cnt <= $f / 3; $cnt++) {
                                                $filters_arr_sorted[$cnt]['ua'] = explode(':', substr($item[$head['f_ua-' . $cnt]], 3));
                                                $filters_arr_sorted[$cnt]['ru'] = explode(':', substr($item[$head['f_ru-' . $cnt]], 3));
                                                $filters_arr_sorted[$cnt]['en'] = explode(':', substr($item[$head['f_en-' . $cnt]], 3));
                                                if (stristr($item[$head['f_ua-' . $cnt]], 'Ф_')) {
                                                    $filters_arr_sorted[$cnt]['type'] = 1;
                                                } elseif (stristr($item[$head['f_ua-' . $cnt]], 'Х_')) {
                                                    $filters_arr_sorted[$cnt]['type'] = 0;
                                                }
                                            }

                                            foreach ($filters_arr_sorted as $filters_arr_sorted_item) {
                                                if (!empty($filters_arr_sorted_item["ua"][0])) {
                                                    $f_alias = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->setUrl($filters_arr_sorted_item["ua"][0], "modx_a_filters", "filter_url");
                                                    $f_alias = str_replace('/', '-', $f_alias);
                                                    $fv_alias = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->setUrl($filters_arr_sorted_item["ua"][1], "modx_a_filter_values", "filter_url");
                                                    $fv_alias = str_replace('/', '-', $fv_alias);

                                                    $f_alias_search = $filters_arr_sorted_item["en"][0] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][0]) : $shop->validateUrl($filters_arr_sorted_item["ua"][0]);
                                                    $f_alias_search = str_replace('/', '-', $f_alias_search);
                                                    $fv_alias_search = $filters_arr_sorted_item["en"][1] != '' ? $shop->validateUrl($filters_arr_sorted_item["en"][1]) : $shop->validateUrl($filters_arr_sorted_item["ua"][1]);
                                                    $fv_alias_search = str_replace('/', '-', $fv_alias_search);

                                                    // проверка есть ли такой фильтр
                                                    $filter_db_id = $modx->db->getValue($modx->db->query("
                                                        SELECT `filter_id`
                                                        FROM `modx_a_filters`
                                                        WHERE `filter_url` = '{$f_alias_search}'
                                                    "));
                                                    if (isset($filter_db_id) && (int)$filter_db_id > 0) {
                                                        // проверка есть ли такая вариация фильтра
                                                        $filter_value_db_id_query = "
                                                            SELECT `value_id`
                                                            FROM `modx_a_filter_values`
                                                            WHERE `filter_url` = '".$filter_db_id."-".$fv_alias_search."' AND `filter_id` = '{$filter_db_id}'
                                                        ";
                                                        $filter_value_db_id = $modx->db->getValue($modx->db->query($filter_value_db_id_query));
                                                        if (isset($filter_value_db_id) && (int)$filter_value_db_id > 0) {
                                                            // если все есть, подвязываем фильтр к категориям продукта
                                                            foreach ($parent_categories as $modx_id_item) {
                                                                $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$filter_db_id}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                            }
                                                        } else {
                                                            // если нет вариации фильтра, создать ее
                                                            $modx->db->query("
                                                            INSERT INTO `modx_a_filter_values` SET
                                                                `filter_id`       = '{$filter_db_id}',
                                                                `filter_url`      = '".$filter_db_id."-".$fv_alias."',
                                                                `filter_value_ua` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
                                                                `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
                                                        ");
                                                            $filter_value_db_id = $modx->db->getInsertId();
                                                            $shop->console_log("новый товар; код - {$key}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");
                                                            // подвязываем фильтр к категориям продукта
                                                            foreach ($parent_categories as $modx_id_item) {
                                                                $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$filter_db_id}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                            }
                                                        }
                                                    } else {
                                                        // если в базе нет такого фильтра, пишем все
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filters` SET
                                                                `filter_feature` = '{$filters_arr_sorted_item['type']}',
                                                                `filter_type`    = '5',
                                                                `filter_url`     = '" . $f_alias . "'
                                                        ");
                                                        $fid = $modx->db->getInsertId();
                                                        $shop->console_log("новый товар; код - {$key}; новый фильтр => {$f_alias} - id = {$fid}");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_strings` SET
                                                                `string_locate` = 'ua',
                                                                `filter_id`     = '{$fid}',
                                                                `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ua"][0])) . "'
                                                        ");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_strings` SET
                                                                `string_locate` = 'ru',
                                                                `filter_id`     = '{$fid}',
                                                                `filter_name`   = '" . htmlspecialchars($modx->db->escape($filters_arr_sorted_item["ru"][0])) . "'
                                                        ");
                                                        $modx->db->query("
                                                            INSERT INTO `modx_a_filter_values` SET
                                                                `filter_id`       = '{$fid}',
                                                                `filter_url`      = '".$fid."-".$fv_alias."',
                                                                `filter_value_ua` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ua"][1]))) . "',
                                                                `filter_value_ru` = '" . htmlspecialchars(trim($modx->db->escape($filters_arr_sorted_item["ru"][1]))) . "'
                                                        ");
                                                        $filter_value_db_id = $modx->db->getInsertId();
                                                        $shop->console_log("новый товар; код - {$key}; новая вариация => {$filters_arr_sorted_item['ua'][1]} - id = {$filter_value_db_id}");
                                                        foreach ($parent_categories as $modx_id_item) {
                                                            $modx->db->query("
                                                                INSERT IGNORE INTO `modx_a_filter_categories` SET
                                                                    `filter_id` = '{$fid}',
                                                                    `modx_id`   = '{$modx_id_item}'
                                                            ");
                                                        }
                                                        $filter_db_id = $fid;
                                                    }
//                                                $filters_arr_to_pfv[$filter_db_id] = $filter_value_db_id;
                                                    $modx->db->query("
                                                        DELETE FROM `modx_a_product_filter_values`
                                                        WHERE `product_id` = '$poid' AND `filter_id`  = '{$filter_db_id}'
                                                    ");
                                                    $shop->console_log("код - {$k}; фильтр - {$filters_arr_sorted_item["ua"][0]}; фильтр велью - {$filters_arr_sorted_item["ua"][1]}");
                                                    $modx->db->query("
                                                        INSERT IGNORE INTO `modx_a_product_filter_values` SET
                                                            `product_id` = '{$poid}',
                                                            `filter_id`  = '{$filter_db_id}',
                                                            `value_id`   = '{$filter_value_db_id}'
                                                    ");
                                                }
                                            }
                                        }
                                        // закончили разбирать фильтра
                                    }
                                    unset($j);
                                }
                                unset($parent_categories);
                                unset($parent_categories_sorted);

                                $product_id_for_image = (int)$product_db[0]["product_id"] > 0 ? $product_db[0]["product_id"] : $pid;
                                // Картинки
                                $images = array_reverse(glob(MODX_BASE_PATH . 'import/' . $key . '/*'));
                                // Создаем папку сразу для картинок
                                $imagePath = "assets/products/" . $product_id_for_image;
                                $folder = MODX_BASE_PATH . $imagePath;

                                if (!file_exists($folder)) {
                                    mkdir($folder);
                                    chmod($folder, 0755);
                                }

                                if (is_array($images) && count($images) > 0) { // Чистим только если загружают новые картинки
                                    if ($objs = glob($folder . "/*")) {
                                        foreach ($objs as $obj) {
                                            is_dir($obj) ? remover($obj) : unlink($obj);
                                        }
                                    }

                                    // Чистим картинки
                                    $sql = "DELETE FROM `modx_a_product_images` WHERE `image_product_id` = '{$product_id_for_image}' ";
                                    $modx->db->query($sql);

                                    $imgType = 'cover';

                                    foreach ($images as $image) {
                                        $name = preg_replace('/\D+/', '', microtime()) . "." . end(explode(".", basename($image)));
                                        copy($image, $folder . "/" . $name);

                                        // Пишем в базу
                                        $sql = "INSERT INTO `modx_a_product_images` (`image_product_id`, `image_file`, `image_use`, `image_comment`) VALUES ({$product_id_for_image}, '{$name}', '{$imgType}', NULL);";
                                        $modx->db->query($sql);

                                        $imgType = 'slider';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
            $_SESSION['notifier_status'] = "success";
            $_SESSION['notifier_text'] = "Импорт завершен успешно!";
            die(header("Location: " . $url . "imports"));
    case "export_orders": // Экспорт заказов
        $order_status = [
            '0' => 'Удален',
            '1' => 'Новый',
            '2' => 'Отправлен',
            '3' => 'Доставлен'
        ];
        $order_pay = [
            '0' => 'Не оплачен',
            '1' => 'Оплачен'
        ];
        // Выбрать Заказы из БД
        if ($_POST['export_orders']['date_from'] != '' AND $_POST['export_orders']['date_to'] != ''){
            $date = '(order_created between "'.$_POST['export_orders']['date_from'].' 00:00:00'.'" and "'.$_POST['export_orders']['date_to'].' 23:59:59'.'")  ';
        }
        $orders = $modx->db->makeArray($modx->db->query("
                    SELECT o.*
                    FROM `modx_a_order` o
                    WHERE ".$date.($_POST['export_orders']['status'] == -1 ? "" : " and order_status='".$_POST['export_orders']['status']."'")
        ));
        // Обработка списка
        if (is_array($orders)) {
            $i = 1;
            $spreadsheet = new Spreadsheet();
            // Установить настройки документа
            $spreadsheet->getProperties()->setCreator('Seiger')
                ->setLastModifiedBy('Seiger')
                ->setTitle('Office 2007 XLSX Document')
                ->setSubject('Office 2007 XLSX Document')
                ->setDescription('Exporting the list of orders for the orders list')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Export file');
            // Настройки форматирования
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(40);
            // Добавляем заголовки
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ИД')
                ->setCellValue('B1', 'Создан')
                ->setCellValue('C1', 'Статус')
                ->setCellValue('D1', 'Статус оплаты')
                ->setCellValue('E1', 'ФИО клиента')
                ->setCellValue('F1', 'Email клиента')
                ->setCellValue('G1', 'Телефон клиента')
                ->setCellValue('H1', 'Стоимость заказа')
                ->setCellValue('I1', 'Валюта')
                ->setCellValue('J1', 'Промокод')
                ->setCellValue('K1', 'Коментарий');

            foreach ($orders as $order) {
                ++$i;
                $client   = json_decode($order['order_client'], true);
                $delivery = json_decode($order['order_delivery'], true);
                $spreadsheet->getActiveSheet()->setCellValue('A' .$i, $order['order_id']);
                $spreadsheet->getActiveSheet()->setCellValue('B' .$i, $order['order_created']);
                $spreadsheet->getActiveSheet()->setCellValue('C' .$i, $order_status[$order['order_status']]);
                $spreadsheet->getActiveSheet()->setCellValue('D' .$i, $order_pay[$order['order_status_pay']]);
                $spreadsheet->getActiveSheet()->setCellValue('E' .$i, $client['fullname']);
                $spreadsheet->getActiveSheet()->setCellValue('F' .$i, $client['email']);
                $spreadsheet->getActiveSheet()->setCellValue('G' .$i, $client['phone']." ");
                $spreadsheet->getActiveSheet()->setCellValue('H' .$i, ($order['order_cost'] + $order['order_delivery_cost']));
                $spreadsheet->getActiveSheet()->setCellValue('I' .$i, $order['order_currency']);
                $spreadsheet->getActiveSheet()->setCellValue('J' .$i, $order['order_code']);
                $spreadsheet->getActiveSheet()->setCellValue('K' .$i, $order['order_comment']);
            }
        }
        // Имя вкладки
        $spreadsheet->getActiveSheet()->setTitle("Экспорт заказов");
        // Установите активный индекс листа на первый лист, так что Excel открывает это как первый лист
        $spreadsheet->setActiveSheetIndex(0);
        // Перенаправить вывод в веб-браузер клиента
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Orders.xlsx"');
        header('Cache-Control: max-age=0');
        // Для IE 9, может потребоваться следующее
        header('Cache-Control: max-age=1');
        // Для IE через SSL, может понадобиться следующее:
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        die;
        break;
}
$tpl = "imports.tpl";