<!--<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-download" aria-hidden="true"></i> <b>Экспорт товаров EXEL</b></h3>
    </div>
    <div class="panel-body">
        <p><i>Экспорт данных о товарах из сайта</i></p>
        <a href="<?=$url?>imports&c=export" class="btn btn-primary"><i class="fa fa-download"></i>&emsp;Экспортировать</a>
    </div>
</div>-->

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-upload" aria-hidden="true"></i> <b>Импорт товаров EXEL</b></h3>
    </div>
    <div class="panel-body">
        <p><i>Импорт данных о товарах на сайт</i></p>
        <p>Загрузите файл XLSX через форму</p>
        <form action="<?=$url?>imports&c=import" method="post" enctype="multipart/form-data">
            <input name="file" type="file"/><br>
            <button name="submit" value="import" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i>&emsp;Импорт</button>
        </form>
    </div>
</div>

<!--<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-download" aria-hidden="true"></i> <b>Экспорт заказов</b></h3>
    </div>
    <div class="panel-body">
        <p><i>Экспорт данных о заказах за выбранный период в файл. Возможно фильтрация по статусу заказа</i></p>
        <form action="<?=$url?>imports&c=export_orders" method="post" enctype="multipart/form-data">
            <table class="table-condensed">
                <tr>
                    <td class="text-right">с</td>
                    <td>
                        <div class="input-group input-append dt date">
                            <input data-format="yyyy-MM-dd" type="text" name="export_orders[date_from]" id="datetimepicker" class="form-control add-on" style="width: 100px;" value="2017-09-01"></input>
                        </div>
                    </td>
                    <td>по</td>
                    <td>
                        <div class="input-group input-append dt date">
                            <input data-format="yyyy-MM-dd" type="text" name="export_orders[date_to]" id="datetimepicker2" class="form-control add-on" style="width: 100px;" value="<?=date("Y-m-d")?>"></input>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Статус</td>
                    <td colspan="3">
                        <select name="export_orders[status]" class="nc form-control">
                            <option value="-1" selected>Все</option>
                            <option value="0">Удалён</option>
                            <option value="1">Новый</option>
                            <option value="2">Отправлен</option>
                            <option value="3">Доставлен</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <button name="submit" value="export_orders" class="btn btn-primary"><i class="fa fa-download"></i>&emsp;Экспорт</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>-->