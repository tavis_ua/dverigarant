<h2 class="muted">Вызов замерщика</h2>
<?php if (count($meters) == 0 || !is_array($meters)):?>
    <div class="row"><h1 class="screen"><strong>Заявок не найдено</strong></h1></div>
<?php else: ?>
<div class="vertical-align" style="text-align:center; margin: 10px ">
    <a href="<?=$url?>meters&c=edit_arias" class="btn btn-warning"><i class="fa fa-pencil"></i>&emsp;Редактировать области</a>
</div>
    <table class="table table-condensed table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th width="50px" style="text-align:center;">ID</th>
                <th width="350px" style="text-align:center;">Имя</th>
                <th width="110px" style="text-align:center;">Телефон</th>
                <th style="text-align:center;">Район</th>
                <th width="100px" style="text-align:center;">Создано</th>
                <th width="150px" style="text-align:center;">Статус выполнения</th>
                <th width="150px" style="text-align:center;">Действие</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($meters as $meter): ?>
                <tr id="meter-<?=$meter['meter_id']?>">
                    <td class="vertical-align"><?=$meter['meter_id']?></td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$meter['meter_name']?></b></td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$meter['meter_phone']?></b></td>
                    <td>&nbsp;
                        <b><?=(!empty($meter['meter_question'])?$meter['meter_question']:$meter['meter_question_rand'])?></b>
                    </td>
                    <td class="vertical-align" style="text-align:center;">&nbsp;<b><?=$meter['meter_created']?></b></td>
                    <td class="vertical-align" style="text-align:center;">
                        <b>
                            <?php if($meter['meter_active'] == 0):?><a href="<?=$url?>meters&c=change_type&i=<?=$meter['meter_id']?>&act=1" class="btn btn-danger">Не выполнено</a><?php endif;?>
                            <?php if($meter['meter_active'] == 1):?><a href="<?=$url?>meters&c=change_type&i=<?=$meter['meter_id']?>&act=0" class="btn btn-success">Выполнено</a><?php endif;?>
                        </b>
                    </td>
                    <td class="vertical-align" style="text-align:center;">
                    <!--<a href="<?=$url?>meters&c=edit&i=<?=$meter['meter_id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a>-->
                        <a href="<?=$url?>meters&c=delete&i=<?=$meter['meter_id']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                    </td>
                </tr>
                <?php endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>meters&c=add"><i class="fa fa-plus-square"></i>&emsp;Создать</a>
        </li>
    </ul>
</div>