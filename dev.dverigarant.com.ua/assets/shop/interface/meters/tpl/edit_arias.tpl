<form id="editForm" action="<?=$url?>meters&c=update_arias" method="post">
    <h2 class="muted">Редактирование</h2>

    <div class="">
        <table class="table table-bordered table-condensed">
            <tr>
                <th class="" width="10px">ID</th>
                <th class="vertical-align" width="160px">RU</th>
                <th class="vertical-align" width="160px">UA</th>
                <th width="250px" style="text-align:right;">Действие</th>
            </tr>
            <?php foreach ($arias as $aria): ?>
            <tr>
                <td>
                <div class="vertical-align"><?=$aria['aria_id']?></div>
                </td>
                <td>
                    <div class="">
                        <input type="text" name="aria[<?=$aria['aria_id']?>][aria_name_ru]"
                               value="<?=$aria['aria_name_ru']?>" class="form-control"
                               data-validate="textMaxLenght:255"/>
                        <div class="error-text" style="display:none;">Колличество символов в поле не должно превышать 255 символов</div>
                    </div>
                </td>
                <td>
                    <div class="">
                        <input type="text" name="aria[<?=$aria['aria_id']?>][aria_name_ua]"
                               value="<?=$aria['aria_name_ua']?>" class="form-control"
                               data-validate="textMaxLenght:255"/>
                        <div class="error-text" style="display:none;">Колличество символов в поле не должно превышать 255 символов</div>
                    </div>
                </td>
                <td class="vertical-align" style="text-align:center;">
                    <a href="<?=$url?>meters&c=delete_arias&i=<?=$aria['aria_id']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                </td>
            </tr>
            <?php endforeach; ?>
            <tr>
                <td>
                    <div>Новая запись</div>
                </td>
                <td>
                    <div class="">
                        <input type="text" name="aria[<?=end($arias)['aria_id']+1?>][aria_name_ru]" class="form-control" data-validate="textMaxLenght:255"/>
                        <div class="error-text" style="display:none;">Колличество символов в поле не должно превышать 255 символов</div>
                    </div>
                </td>
                <td>
                    <div class="">
                        <input type="text" name="aria[<?=end($arias)['aria_id']+1?>][aria_name_ua]" class="form-control" data-validate="textMaxLenght:255"/>
                        <div class="error-text" style="display:none;">Колличество символов в поле не должно превышать 255 символов</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="actions">
        <ul class="actionButtons">
            <li id="Button1" class="transition">
                <a href="#" class="primary" onclick="documentDirty=false;saveForm('#editForm');"><i
                            class="fa fa-save"></i>&emsp;Сохранить</a>
            </li>
        </ul>
    </div>
</form>
<?=$tiny_mce?>