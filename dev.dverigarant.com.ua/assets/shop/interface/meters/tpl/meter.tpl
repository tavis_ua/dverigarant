<?php if (!isset($meter['meter_id']) || (int)$meter['meter_id'] == 0): ?>
<form id="editForm" action="<?=$url?>meters&c=save" method="post">
    <h2 class="muted">Добавление <small>отзыва</small></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>meters&c=update" method="post">
        <h2 class="muted">Редактирование <small>(<?=$meter['meter_id']?>)</small></h2>
<?php endif; ?>
        <ul class="nav nav-tabs" role="tablist" id="myTab">
            <li class="active"><a href="#base" role="tab" data-toggle="tab"><b>Основное</b></a></li>
            <?php foreach ($languages as $k => $lng): ?>
                <li role="presentation"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><?=$lng?></b></a></li>
            <?php endforeach; ?>
        </ul>
        <br />
        <div class="container-fluid">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="base">
                    <table class="table table-bordered table-condensed">
                        <!--  <tr>
                              <th class="vertical-align" width="160px">Видимость:</th>
                              <td>
                                  <div class="bootstrap-switch">
                                      <input type="checkbox" name="meter[meter_active]" <?=($meter['meter_active'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Да" data-off-text="Нет" class="bsw">
                                  </div>
                              </td>
                          </tr>
                        <tr>
                            <th class="vertical-align">Категория:</th>
                            <td>
                                <select class="form-control chosen-select" data-placeholder="Выберите категорию" name="meter[meter_category]">
                                    <?php foreach ($categories as $id => $title): ?>
                                        <option value="<?=$id;?>" <?php if($id == $meter['meter_category']) echo 'selected'; ?>><?=$title['title'];?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <th class="vertical-align">Имя задавшего:</th>
                            <td>
                                <input type="text" name="meter[meter_name]" value="<?=$meter['meter_name']?>" class="form-control" />
                            </td>
                        </tr>
                       <!-- <tr>
                            <th class="vertical-align">Email задавшего:</th>
                            <td>
                                <input type="text" name="meter[meter_email]" value="<?=$meter['meter_email']?>" class="form-control" />
                            </td>
                        </tr> -->
                        <tr>
                            <th class="vertical-align">Телефон задавшего:</th>
                            <td>
                                <input type="text" name="meter[meter_phone]" value="<?=$meter['meter_phone']?>" class="form-control" />
                            </td>
                        </tr>
                    </table>
                </div>
                <?php foreach ($languages as $k => $lng): ?>
                    <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th class="vertical-align" width="160px">Вопрос:</th>
                                <td>
                                    <div class="">
                                        <input type="text" name="meter[<?=$lng?>][meter_question]" value="<?=$meter[$lng]['meter_question']?>" class="form-control" data-validate="textMaxLenght:255" />
                                        <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> Вопрос"</b> не должно превышать 255 символов</div>
                                    </div>
                                </td>
                            </tr>
                          <!--  <tr>
                                <th class="vertical-align">Ответ:</th>
                                <td>
                                    <textarea id="<?=$lng?>_content" name="meter[<?=$lng?>][meter_answer]" class="form-control" rows="2"><?=$meter[$lng]['meter_answer']?></textarea>
                                </td>
                            </tr>-->
                        </table>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <input type="hidden" name="meter[meter_id]" value="<?=$meter['meter_id']?>" />
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;saveForm('#editForm');"><i class="fa fa-save"></i>&emsp;Сохранить</a>
                </li>
            </ul>
        </div>
    </form>
    <?=$tiny_mce?>