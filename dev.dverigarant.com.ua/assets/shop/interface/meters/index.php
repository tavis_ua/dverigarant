<?php

include dirname(__FILE__)."/class.php";
$class = new Meter($modx);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

switch ($controller) {
    case "add":
        $tiny_mce   = $shop->tinyMCE("content", "300px");
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang']);
        $tpl        = "meter.tpl";
        break;
    case "edit":
        $tiny_mce   = $shop->tinyMCE("content", "300px");
        $categories = $shop->getCategories($cat_templ_id, $modx->config['lang']);
        $meter        = $shop->getMeter($_GET['i']);
        foreach ($languages as $value) {
            $meter[$value] = $shop->getMeterString($_GET['i'], $value);
        }
        $tpl = "meter.tpl";
        break;
    case "save":
        $_POST['meter']['meter_id'] = $shop->saveMeter($_POST['meter']);
        foreach ($languages as $value) {
            $shop->saveMeterString($_POST['meter'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."meters&c=edit&i=".$_POST['meter']['meter_id']));
    case "update":
        $shop->updateMeter($_POST['meter']);
        foreach ($languages as $value) {
            $shop->saveMeterString($_POST['meter'], $value);
        }
        $modx->clearCache();
        die(header("Location: ".$url."meters"));
    case "delete" :
        $shop->deleteMeter($_GET['i']);
        $modx->clearCache();
        die(header("Location: ".$url."meters"));
    case "edit_arias":
        $arias = $modx->db->makeArray($modx->db->query("SELECT * FROM `modx_a_arias`"));
        $tpl = "edit_arias.tpl";
        break;
    case "change_type":
        $modx->db->query("
                UPDATE `modx_a_meters` SET
                    meter_active   = '".(int)$_GET['act']."'
                WHERE meter_id = '".(int)$_GET['i']."'
            ");
        $modx->clearCache();
        die(header("Location: ".$url."meters"));
        break;
    case "delete_arias":
        $modx->db->query("DELETE FROM `modx_a_arias` WHERE `aria_id` = '".(int)$_GET['i']."' LIMIT 1");
        $modx->clearCache();
        die(header("Location: ".$url."meters&c=edit_arias"));
        break;
    case "update_arias":
        foreach ($_POST['aria'] as $kk => $vv) {
            $query = "INSERT INTO `modx_a_arias` SET 
                        `aria_id`      = '".(int)$kk."',
                        `aria_name_ru` = '".$modx->db->escape($vv['aria_name_ru'])."',
                        `aria_name_ua` = '".$modx->db->escape($vv['aria_name_ua'])."'
                      ON DUPLICATE KEY UPDATE
                        `aria_name_ru` = '".$modx->db->escape($vv['aria_name_ru'])."',
                        `aria_name_ua` = '".$modx->db->escape($vv['aria_name_ua'])."'
            ";
            $modx->db->makeArray($modx->db->query($query));
        }
        die(header("Location: ".$url."meters&c=edit_arias"));
        break;
    default:
        $meters  = $shop->getMetersList();
        $modx->setPlaceholder('url', $url."meters&p=");
        $pagin = $shop->getPaginate();
        $tpl   = "meters.tpl";
        break;
}