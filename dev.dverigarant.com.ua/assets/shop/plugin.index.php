<?php

/**
 * Базовый плагин
 */
$modx->config['lang'] = in_array($_GET['lang'], explode(",", $modx->config['lang_list'])) ? $_GET['lang'] : $modx->config['lang_default'];
require_once MODX_BASE_PATH . "assets/shop/class.shop.php";
$shop = new Shop($modx);
switch ($modx->event->name) {
    case "OnWebPageInit": // выполняется перед загрузкой данных о странице раньше всех других событий единоразово
    case 'OnPageNotFound' : // выполняется если ресурс MODx не найден
        // Инициализируем историрю просмотренных товаров, корзину, валюту
        if (!is_array($_SESSION['history'])) $_SESSION['history'] = [];
        if (!is_array($_SESSION['cart'])) $_SESSION['cart'] = [];
        if (!isset($_SESSION['currency'])) $_SESSION['currency'] = $modx->config['shop_curr_default_code'];
        $currencies       = explode("|", $modx->config['shop_curr_code']);
        $signs            = explode("|", $modx->config['shop_curr_sign']);
        $curKey           = array_search($_SESSION['currency'], $currencies);
        $_SESSION['sign'] = $signs[$curKey];
        // Отследить страницу (не ресурс MODx)
        $arrayURL  = explode("/", substr($_GET['q'], 0, -1));
        $needAlias = array_pop($arrayURL);
        $isFilter  = in_array("f", $arrayURL);
        $parentDoc = $arrayURL[0];
        $idDoc     = $modx->documentListing[$parentDoc];
        // Отследить фильтр
        if ($isFilter) {
            $urlParts = explode("/f/", $_GET['q']);
            $document = $modx->documentListing[$urlParts[0]];
            if ((int)$document > 0) {
                // Запись фильтров для передачи на страницу
                $filterArr = explode("/", $urlParts[1]);
                if ($filterArr[0]) {
                    $modx->setPlaceholder("filters", $filterArr[0]);
                }
                // Перенаправление на страницу каталога
                $modx->sendForward($document);
                die;
            }
        }

        // URL новости от корня
        if ((int)$idDoc == 0 && (int)$modx->config['news_catalog_root'] && $needAlias != '') {
            $news = $modx->db->getValue($modx->db->query("SELECT `id` FROM `modx_a_news` WHERE `alias` = '".$modx->db->escape($needAlias)."' AND `published` = '1' LIMIT 1"));
            if ((int)$news > 0  && count($arrayURL) == 0) {
                $modx->sendForward((int)$modx->config['news_page_item']);
                die;
            }
        }
        // URL акции от корня
        if ((int)$idDoc == 0 && (int)$modx->config['discounts_catalog_root'] && $needAlias != '') {
			if ($isFilter) {
				$urlParts = explode("/f/", $_GET['q']);
				$disc_alias = $urlParts[0];

                // Запись фильтров для передачи на страницу
                    $modx->setPlaceholder("filters", trim($urlParts[1], '/'));
			} else {
				$disc_alias = $needAlias;
			}

            $news = $modx->db->getValue($modx->db->query("SELECT `id` FROM `modx_a_discounts` WHERE `alias` = '".$modx->db->escape($disc_alias)."' AND `published` = '1' LIMIT 1"));
            if ((int)$news > 0 && (count($arrayURL) == 0 || $isFilter)) {
                $modx->sendForward((int)$modx->config['discounts_page_item']);
                die;
            }
        }

        // URL товара от корня
        if ((int)$idDoc == 0 && (int)$modx->config['shop_root_item'] && $needAlias != '') {
            $product = $modx->db->getRow($modx->db->query("SELECT `product_id` FROM `modx_a_products` WHERE `product_url` = '".$modx->db->escape($needAlias)."' AND `product_active` = '1' LIMIT 1"));
            if ((int)$product['product_id'] > 0  && count($arrayURL) == 0) {
                $modx->sendForward((int)$modx->config['shop_page_item']);
                die;
            }
        }
        if ((int)$idDoc > 0) {
            switch ($idDoc) {
                // Отследить товар
                case (int)$modx->config['shop_page_item'] :
                    if ((int)$modx->config['shop_root_item'] == 0) {
                        $product = $modx->db->getRow($modx->db->query("SELECT `product_id` FROM `modx_a_products` WHERE `product_url` = '".$modx->db->escape($needAlias)."' AND `product_active` = '1' LIMIT 1"));
                        if ((int)$product['product_id'] > 0) {
                            $modx->sendForward((int)$modx->config['shop_page_item']);
                            die;
                        }
                    }
                    break;
                // Отследить новость
                case (int)$modx->config['news_catalog_root'] :
                    $news = $modx->db->getValue($modx->db->query("SELECT `id` FROM `modx_a_news` WHERE `alias` = '".$modx->db->escape($needAlias)."' AND `published` = '1' LIMIT 1"));
                    if ((int)$news) {
                        $modx->sendForward((int)$modx->config['news_page_item']);
                        die;
                    }
                    break;
            }
        }
        // Определяем язык
        if ($modx->config['lang_enable']) {
            // Получаем массив переводов
            $dump = $_SERVER['DOCUMENT_ROOT']."/assets/modules/language/dump.php";
            if (file_exists($dump)) {
                require $dump;
                foreach ($l as $key => $value) {
                    $translate[$key] = $value[$modx->config['lang']];
                }
            }
        }
        // Oбработчик ajax запросов
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && isset($_REQUEST['ajax'])) {
            require MODX_BASE_PATH. "assets/shop/plugin.ajax.php";
        }
        // Обработчик входящих данных с форм и ответы от платежных систем
        require MODX_BASE_PATH . "assets/shop/plugin.data.php";
        break;
    case 'OnLoadWebDocument' :
// Страница 404
//        if ($modx->documentIdentifier == (int)$modx->config['error_page']) {
//             $_SESSION['js'] = "setTimeout(function(){
//                 document.location.href = '{$modx->makeUrl($modx->config['site_start'])}';
//             },5000);";
//        }

        $modx->documentObject['sing'] = $_SESSION["sign"];
        $count = 0;
        foreach ($_SESSION['cart'] as $item){
            foreach ($item as $key=>$value) {
                $count += $value['count'];
            }
        }
        $modx->documentObject['getCartCount'] = $count > 0 ? $count : "";
        // Сортировка товаров
        $modx->documentObject['get_sort'] = isset($modx->documentObject['get_sort']) ? $modx->documentObject['get_sort'] : "";

        // TV изображения по умолчанию
        $modx->documentObject['tv_img'] = isset($modx->documentObject['tv_img']) ? $modx->documentObject['tv_img'] : "images/logo.png";

        // Просмотренные товары
        if (isset($_SESSION['history'])){
            $modx->documentObject['history'] = implode(",", array_slice($_SESSION['history'], 0, 10));
        }

        // Страница новости
        if ($modx->documentIdentifier == (int)$modx->config['news_page_item']) {

            $urlArr = explode("/", substr($_GET['q'], 0, -1));
            $query = "
                SELECT *
                FROM `modx_a_news` n 
                LEFT JOIN `modx_a_new_strings` nt ON n.`id` = nt.`rid` AND `lang` = '{$modx->config['lang']}'
                WHERE n.`alias` = '".$modx->db->escape(end($urlArr))."'AND published = '1'
                LIMIT 1
            ";
            $row = $modx->db->getRow($modx->db->query($query));

            if (!$row || count($urlArr) != 1) { // Если новость не найдена
                $shop->go404();
            }
//            $row['p'] = isset($_GET['p']) &&  $_GET['p'] != '' ? '?p='.$_GET['p'] : '' ;
            $row['p'] = isset($_SESSION['p']) &&  $_SESSION['p'] != '' ? $_SESSION['p'] : '' ;
            $row['image'] = "/assets/images/news/".$row['id'].'/'.$row['image'];
            $row['news_content'] = $row['content'];
            $row['news_description'] = strip_tags($row['description']);
            $row['tv_img'] = $row['image'];
            if (trim($row['seo_title'] != '')) {
                $row['title'] = $row['seo_title'];
            } else {
                $row['seo_title'] = $row['title'] ." | ". $modx->config['site_name'];
            }

            if (!empty($row['news_category'])) {
                $cat_products_arr = $modx->db->makeArray($modx->db->query("
                    SELECT `product_id`
                    FROM `modx_a_products`
                    WHERE `product_category` IN ({$row['news_category']})
                "), 'product_id');
                $cat_products_arr = array_column($cat_products_arr,  'product_id');
                $cat_products = implode(',', $cat_products_arr);
                $row['news_products']  = !empty($row['news_products']) ? $row['news_products'] . "," . $cat_products : $cat_products;
            }
            $row['news_products'] = !empty($row['news_products']) ? $row['news_products'] : "";

            $modx->documentObject['pagetitle']       = $modx->documentObject['pagetitle_'.$modx->config['lang']]       = $row['title'];
            $modx->documentObject['seo_description'] = $modx->documentObject['seo_description_'.$modx->config['lang']] = $row['seo_description'];
            $modx->documentObject['seo_keywords']    = $modx->documentObject['seo_keywords_'.$modx->config['lang']]    = $row['seo_keywords'];
            $modx->documentObject['seo_title']       = $modx->documentObject['seo_title_'.$modx->config['lang']]       = $row['seo_title'];
            $modx->documentObject['seo_canonical']   = $modx->documentObject['seo_canonical_'.$modx->config['lang']]   = $row['seo_canonical'];
            $modx->documentObject['seo_robots']      = $modx->documentObject['seo_robots_'.$modx->config['lang']]      = $row['seo_robots'];

            // Передача параметров новости в ресурс MODx
            $modx->documentObject = array_merge($modx->documentObject, $row);
            unset($_SESSION['p']);
        }

        // Страница акции
        if ($modx->documentIdentifier == (int)$modx->config['discounts_page_item']) {
			$arrayURL  = explode("/", substr($_GET['q'], 0, -1));
			$needAlias = array_pop($arrayURL);
			$isFilter  = in_array("f", $arrayURL);

			if ($isFilter) {
				$urlParts = explode("/f/", $_GET['q']);
				$disc_alias = $urlParts[0];
			} else {
				$disc_alias = $needAlias;
			}

            $query = "
                SELECT *
                FROM `modx_a_discounts` d
                LEFT JOIN `modx_a_discounts_strings` dt ON d.`id` = dt.`rid` AND `lang` = '{$modx->config['lang']}'
                WHERE d.`alias` = '".$modx->db->escape($disc_alias)."'AND published = '1'
                LIMIT 1
            ";
            $row = $modx->db->getRow($modx->db->query($query));
            if (!$row || (count($arrayURL) == 1 || ($isFilter && count($arrayURL) == 3))) { // Если акция не найдена
                $shop->go404();
            }
            $row['p'] = isset($_SESSION['p']) &&  $_SESSION['p'] != '' ? $_SESSION['p'] : '' ;
            $row['image'] = "/assets/images/discounts/".$row['id'].'/'.$row['image'];
            $row['discounts_content'] = $row['content'];
            $row['discounts_description'] = strip_tags($row['description']);
            $row['tv_img'] = $row['image'];
            if (trim($row['seo_title'] != '')) {
                $row['title'] = $row['seo_title'];
            } else {
                $row['seo_title'] = $row['title'] ." | ". $modx->config['site_name'];
            }

            if (!empty($row['discounts_category'])) {
                $cat_products_arr = $modx->db->makeArray($modx->db->query("
                    SELECT `product_id`
                    FROM `modx_a_products`
                    WHERE `product_category` IN ({$row['discounts_category']})
                "), 'product_id');
                $cat_products_arr = array_column($cat_products_arr,  'product_id');
                $cat_products = implode(',', $cat_products_arr);
                $row['discounts_products']  = !empty($row['discounts_products']) ? $row['discounts_products'] . "," . $cat_products : $cat_products;
            }
            $row['discounts_products'] = !empty($row['discounts_products']) ? $row['discounts_products'] : "";

            $modx->documentObject['pagetitle']       = $modx->documentObject['pagetitle_'.$modx->config['lang']]       = $row['title'];
            $modx->documentObject['seo_description'] = $modx->documentObject['seo_description_'.$modx->config['lang']] = $row['seo_description'];
            $modx->documentObject['seo_keywords']    = $modx->documentObject['seo_keywords_'.$modx->config['lang']]    = $row['seo_keywords'];
            $modx->documentObject['seo_title']       = $modx->documentObject['seo_title_'.$modx->config['lang']]       = $row['seo_title'];
            $modx->documentObject['seo_canonical']   = $modx->documentObject['seo_canonical_'.$modx->config['lang']]   = $row['seo_canonical'];
            $modx->documentObject['seo_robots']      = $modx->documentObject['seo_robots_'.$modx->config['lang']]      = $row['seo_robots'];
            // Передача параметров новости в ресурс MODx
            $modx->documentObject = array_merge($modx->documentObject, $row);
            unset($_SESSION['p']);
        }

        // Страница категории
        if ($modx->documentObject['template'] == $modx->config['shop_tpl_category']) {
            if ((int)$modx->documentObject['tv_product'][1] > 0) {
                $alias = $modx->db->getValue($modx->db->query("
                    SELECT product_url 
                    FROM `modx_a_products` 
                    WHERE product_active = 1 AND product_id = ".(int)$modx->documentObject['tv_product'][1]."
                "));
                if ($alias) {
                    $url = $modx->makeUrl((int)$modx->config['shop_page_item']).$alias."/";
                    header("Cache-Control: no-cache, must-revalidate");
                    header("HTTP/1.0 301 Moved Permanently");
                    header("HTTP/1.1 301 Moved Permanently");
                    header("Status: 301 Moved Permanently");
                    header("Location: ".$url);
                    die;
                }
            }
        }

        // Страница товара
        if ($modx->documentIdentifier == (int)$modx->config['shop_page_item']) {
            $urlArr = explode("/", substr($_GET['q'], 0, -1));
            $query = "
                SELECT *,`p`.`product_code` as `parent_code`, `po`.`product_code` as `chield_code`,
                    IF(
                        `p`.`product_category` > 0,
                        `p`.`product_category`,
                        (SELECT `modx_id` FROM `modx_a_product_categories` WHERE `product_id` = `p`.`product_id` LIMIT 1)
                    ) AS 'product_category',
                    (
                        SELECT `image_file` 
                        FROM `modx_a_product_images` 
                        WHERE `image_use` = 'cover' AND `image_product_id` = `p`.`product_id` 
                        LIMIT 1
                    ) AS 'product_cover',
                    (
                        SELECT CONCAT('[',
                            GROUP_CONCAT(
                                '{\"filter_name\":\"',COALESCE(`fs`.`filter_name`,''),'\",
                                \"filter_type\":\"',COALESCE(`f`.`filter_type`,''),'\",
                                \"filter_url\":\"',COALESCE(`f`.`filter_url`,''),'\",
                                \"filter_value\":\"',COALESCE(`vs`.`filter_value_{$modx->config['lang']}`,''),'\",
                                \"filter_value_num\":\"',COALESCE(`vs`.`filter_value_num`,''),'\"}'
                            ),
                        ']')
                        FROM `modx_a_product_filter_values` `pfv`
                        LEFT JOIN `modx_a_filters` `f` ON `pfv`.`filter_id` = `f`.`filter_id`
                        LEFT JOIN `modx_a_filter_strings` `fs` ON `pfv`.`filter_id` = `fs`.`filter_id` AND `string_locate` = '{$modx->config['lang']}'
                        LEFT JOIN `modx_a_filter_values` `vs` ON `pfv`.`value_id` = `vs`.`value_id` AND `string_locate` = '{$modx->config['lang']}'
                        WHERE `pfv`.`product_id` = `po`.`id` AND `f`.`filter_feature` = '0'
                    ) AS 'characteristics'
                FROM `modx_a_products` `p` 
                LEFT JOIN `modx_a_product_strings` `ps` ON `p`.`product_id` = `ps`.`product_id` AND `string_locate` = '{$modx->config['lang']}'
                LEFT JOIN `modx_a_product_options` `po` ON `p`.`product_id` = `po`.`product` AND `default_option` = 1
                WHERE `p`.`product_url` = '".$modx->db->escape(end($urlArr))."' AND `product_active` = '1'
                LIMIT 1
            ";
            $modx->db->query("SET group_concat_max_len = 16384;");
            $row = $modx->db->getRow($modx->db->query($query));

            if (!$row || ((int)$modx->config['shop_root_item'] == 0 && count($urlArr) != 2) || ((int)$modx->config['shop_root_item'] == 1 && count($urlArr) != 1)) { // Если товар не найден
                $shop->go404();
            }

            $row['product_code'] = (string)$row['chield_code'] != '' ? $row['chield_code'] : ((string)$row['parent_code'] != '' ? $row['parent_code'] : '');


            $row['characteristic'] = '';
            $row['manufacturer'] = '';
            $row['charact'] = '';
            $row['product_look_like'] = isset($row['product_look_like']) ? $row['product_look_like'] : '';
            $row['product_recommender'] = isset($row['product_recommender']) ? $row['product_recommender'] : '';
//            $row['left_charact'] = $row['right_charact'] = '';
            if ($characteristics = $shop->is_json($row['characteristics'])) {
                if (is_array($characteristics)) {
//                    $row['manufacturer'] = '';
                    foreach ($characteristics as &$characteristic) {
                        if ($characteristic['filter_url'] == $modx->documentObject['tv_manufacturer_filter'][1]) {
                            $row['manufacturer'] = $characteristic['filter_value'];
                        }
                            $row['charact'] .= $modx->parseChunk('tpl_product_characteristic', $characteristic);
                    }
                }
            }
            //Значения опций
            $options_ids = $modx->db->getColumn( 'id', "SELECT `id` FROM `modx_a_product_options` WHERE `product` = {$row['product_id']}");
            $options_ids = (is_array($options_ids) && count($options_ids) > 0) ? implode(',', $options_ids): false;
            if ($options_ids) {
                $ofvalues_query = "
	                SELECT
	                    `f`.`filter_id` AS 'filter_id',
	                    `f`.`filter_url` AS 'filter_url',
	                    `fs`.`filter_name` AS 'filter_name',
	                    `fv`.`value_id` AS 'filter_value_id',
	                    `fv`.`filter_value_{$modx->config['lang']}` AS 'filter_value',
	                    `fv`.`image` AS 'filter_value_image',
	                    `fv`.`filter_url` AS 'filter_value_url',
	                    `pfv`.`product_id` AS 'p_id'
	                FROM `modx_a_filter_values` `fv`
	                INNER JOIN `modx_a_filters` `f` ON `f`.`filter_id` = `fv`.`filter_id`
	                INNER JOIN `modx_a_filter_strings` `fs` ON `fs`.`filter_id` = `f`.`filter_id` AND `fs`.`string_locate` = '{$modx->config['lang']}'
	                INNER JOIN `modx_a_product_filter_values` `pfv` ON `pfv`.`value_id` = `fv`.`value_id`
                    INNER JOIN `modx_a_product_options` `po` ON `pfv`.`product_id` = `po`.`id`
	                WHERE `pfv`.`product_id` IN ({$options_ids}) AND `f`.`filter_feature` = '1'
                    ORDER BY `po`.`default_option` DESC,`pfv`.`product_id`,`f`.`filter_id`
	            ";

                $ofvalues = $modx->db->makeArray($modx->db->query($ofvalues_query));
                $row['option_values'] = '';
                if(is_array($ofvalues) && count($ofvalues) > 0) {
                    $of_arr = [];
                    foreach ($ofvalues as $ofvalue) {
                        $of_arr[$ofvalue['filter_id']]['filter_url']                                                     = $ofvalue['filter_url'];
                        $of_arr[$ofvalue['filter_id']]['filter_name']                                                    = $ofvalue['filter_name'];
                        $of_arr[$ofvalue['filter_id']]['filter_values'][$ofvalue['filter_value_id']]['filter_value']     = $ofvalue['filter_value'];
                        $of_arr[$ofvalue['filter_id']]['filter_values'][$ofvalue['filter_value_id']]['filter_value_url'] = $ofvalue['filter_value_url'];
                        $of_arr[$ofvalue['filter_id']]['filter_values'][$ofvalue['filter_value_id']]['filter_value_id']  = $ofvalue['filter_value_id'];
                        $of_arr[$ofvalue['filter_id']]['filter_values'][$ofvalue['filter_value_id']]['p_id']             = $ofvalue['p_id'];
                    }
                    $row['option_values'] = '';
                    foreach ($of_arr as $of_item) {
                            if (is_array($of_item['filter_values']) && count($of_item['filter_values'])) {
                                $of_item['values'] = '';
                                foreach ($of_item['filter_values'] as $of_item_value) {
                                    if (isset($_GET['edit_prod']) && (int)$_GET['edit_prod'] > 0) {
                                        $active_id = (int)$_GET['edit_prod'];
                                        if ($active_id == (int)$of_item_value['p_id']) {
                                            $of_item_value['checked'] = 1;
                                        }
                                    }
                                    $of_item['values'] .= $modx->parseChunk('tpl_product_option_item', $of_item_value);
                                }
                            }
                            if (!empty($of_item['values'])) {
                                $p_option_values .= $modx->parseChunk('tpl_product_option_outer', $of_item);
                            }
                        }
                    $row['option_values'] = $p_option_values;
                }
            }

            $row['tv_img']        = !empty($row['product_cover']) ? 'assets/products/'.$row['product_id'].'/'.$row['product_cover'] : 'assets/snippets/phpthumb/noimage.png';
            $row['product_cover'] = !empty($row['product_cover']) ? 'assets/products/'.$row['product_id'].'/'.$row['product_cover'] : 'assets/snippets/phpthumb/noimage.png';

            // Получаю информацию об основной комплектации
            $addEqp = $shop->getValuesEquipment($row['product_id']);
            // Передача информации о дополнительной комплектации
            $addAddEqp = $shop->getAdditionalEquipment($row['product_id']);
            $eqpTable = '';
            $addEqpPrice = 0;
            if(count($addEqp) > 0){
                $eqpTable.='
                    <div>                        
                        <div class="equipment-box">';
                        foreach ($addEqp as $keyParam => $params){
                            $eqpTable.='<div class="param_item">
                            <div class="collection-title" style="margin-bottom:5px">'.$params[0]['param_name'].'</div>
                            <select id="param_'.$keyParam.'" class="parameters_item" name="param_'.$keyParam.'">';
                            foreach ($params as $param){
                                $eqpTable.='<option value="'.$param['val_id'].'" price="'.$param['val_price'].'">'.$param['val_name'].'</option>';
                            }
                            $eqpTable.='</select></div>';
                        }
                $eqpTable.='</div>
                    </div>';
            }
            if(count($addAddEqp) > 0){
                $eqpTable .= '
                    <div style="padding-left: 30px">                    
                        <div class="collection-title" style="margin-bottom:5px">Додаткова комплектація</div>
                                        <div class="addition-equipment-box">';
                foreach ($addAddEqp as $key => $item){
                    $addEqpPrice += (isset($item['default']) && $item['default'] ? $item['price'] : 0);
                    $eqpTable .= '<div class="equipment clearfix">';
                    $eqpTable .= '<br><div class="checkbox checkbox-default pull-left '.(isset($item['disable']) && $item['disable'] ? 'disabled' : '').'">
                                        <input id="option'.$item['id'].'" addEqp-price = "'.(isset($item['price']) && $item['price'] ? $item['price'] : 0).'" class="styled" type="checkbox" '.(isset($item['disable']) && $item['disable'] ? 'disabled onclick="return false;"' : '').' name="options" value="'.$item['id'].'" '.(isset($item['default']) && $item['default'] ? 'checked="checked"' : '').'>
                                        <label for="option'.$item['id'].'">'.$item['name'].'</label>                                            
                                     </div>';
                    $eqpTable .= '<div class="equipment-price pull-right">+<span class="option_price">'.$item['price'].'</span> грн.</div>';
                    $eqpTable .= '</div>';
                }
                $eqpTable .= '</div></div>';
            }

            if ($_SESSION['currency'] == 'usd') {
                $row['sing']     = "";
                $row['sing_usd'] = $_SESSION["sign"];
            } else {
                $row['sing']     = $_SESSION["sign"];
                $row['sing_usd'] = "";
            }

            $tmp_prc = $addEqpPrice + ((int)$row['price'] > 0 ? $row['price'] : ((int)$row['price'] > 0 ? $row['price'] : 0));
            $tmp_prc_old = (int)$row['price_old'] > 0 ? $row['price_old'] : ((int)$row['product_price_old'] > 0 ? $row['product_price_old'] : 0);

            list($row['prc'], $row['sub']) = explode(".", $shop->getPrice($tmp_prc, $_SESSION['currency'], ""));
            list($row['prc_old'], $row['sub_old']) = explode(".", $shop->getPrice($tmp_prc_old, $_SESSION['currency'], ""));
            $row['cart_count']  = count($_SESSION['cart']);
            $row['product_brand'] = !$row['product_brand'] ? "" : $row['product_brand'];

            array_unshift($_SESSION['history'], $row['product_id']);

            $modx->documentObject['pagetitle'] = $modx->documentObject['pagetitle_'.$modx->config['lang']] = $row['product_name'];

            $seo['seo_title']       = $row['product_seo_title'] == '' ? $row['product_name'].' | '.$modx->config['site_name'] : $row['product_seo_title'];
            $seo['seo_keywords']    = $row['product_seo_keywords'] ?: $row['product_seo_keywords'];
            $seo['seo_content']     = $row['product_seo_content'] ?: $row['product_seo_content'];
            $seo['seo_description'] = $row['product_seo_description'] ?: $row['product_seo_description'];
            $seo['seo_canonical']   = $row['product_seo_canonical'] ?: $row['product_seo_canonical'];
            $seo['seo_robots']      = $row['product_active'] == 1 ? $row['product_seo_robots'] : 'noindex,nofollow';

            $modx->documentObject['addEqp']  = $eqpTable;
            $modx->documentObject['basicPrice']  = $row['price'];
            // Передача параметров товара в ресурс MODx
            $modx->documentObject = array_merge($modx->documentObject, $row, $seo);
            $modx->documentObject['product_cart_comment']  = '';
            $modx->documentObject['product_cart_quantity'] = 1;

            if (isset($_GET['edit_prod']) && (int)$_GET['edit_prod'] > 0) {
                $prod = (int)$_GET['edit_prod'];
                $modx->documentObject['product_cart_comment']  = $_SESSION['cart_string'][$prod] !='' ? $_SESSION['cart_string'][$prod] : '';
                $modx->documentObject['product_cart_quantity'] = (int)$_SESSION['cart'][$prod] > 0 ? $_SESSION['cart'][$prod] : 1;
                $modx->documentObject['id'] = $prod;
            }

        }

        // Страница избранного
        if ($modx->documentIdentifier == 49) { // id страницы Избранное
            $wishlist_count = (int)$modx->runSnippet("Shop", ["get" => "getWishlistCount"]);
            if ($wishlist_count == 0) {
                $shop->go404();
            }
        }

        // Страница корзины
        if ($modx->documentIdentifier == 18) {
            if (count($_SESSION['cart']) == 0) {
                $shop->go404();
            }
            $modx->documentObject['page_back'] = $_SERVER['HTTP_REFERER'];
        }

        // Страница Спасибо за заказ
        if ($modx->documentIdentifier == $modx->config['shop_thanks_order']) {
            if (!isset($_SESSION['order_num']) || $_SESSION['order_num'] == "") {
                $shop->go404();
            }
            $order = $modx->db->getRow($modx->db->query("
                SELECT o.*, 
                    (SELECT SUM(product_price) FROM `modx_a_order_products` WHERE order_id = o.order_id) AS 'sum_products'
                FROM `modx_a_order` o 
                WHERE o.order_id = ".(int)$_SESSION['order_num']
            ));
            $sum_products = $modx->db->makeArray($modx->db->query("
                SELECT product_count, product_price FROM `modx_a_order_products` WHERE order_id = ".(int)$_SESSION['order_num']
            ));
            if (is_array($sum_products)) {
                $order['sum_products'] = 0;
                foreach ($sum_products as $item) {
                    $order['sum_products'] += $item['product_count'] * $item['product_price'];
                }
            }
            $order['order_num']    = str_pad($order['order_id'], 6, 0, STR_PAD_LEFT);
            $order['sum_products'] = number_format($order['sum_products'], 2, '.', '');
            $order['order_total']  = number_format($order['order_cost'] + $order['order_delivery_cost'], 2, '.', '');

            $currencies       = explode("|", $modx->config['shop_curr_code']);
            $signs            = explode("|", $modx->config['shop_curr_sign']);
            if ($order['order_currency'] == 'usd') {
                $order['sing']     = "";
                $order['sing_usd'] = $signs[array_search($order['order_currency'], $currencies)];
            } else {
                $order['sing']     = $signs[array_search($order['order_currency'], $currencies)];
                $order['sing_usd'] = "";
            }

            $modx->documentObject = array_merge($modx->documentObject, $order);
        }

        // Страница личный кабинет
        if ($modx->documentIdentifier == $modx->config['webuser_account']) {  // id страницы личного кабинета
            if (!isset($_SESSION['webuser']) || count($_SESSION['webuser']) == 0) {
                $_SESSION['js'] = $modx->parseDocumentSource("alert('[#98#]');"); // Вам необходимо пройти авторизацию.
                die(header("Location: ".$modx->makeUrl($modx->config['webuser_login']))); // id страницы авторизации
            } else {
                foreach ($_SESSION['webuser'] as $key => $value) {
                    $modx->documentObject['user_'.$key] = $value;
                }
            }
        }

        // Страница оформления заказа
        if ($modx->documentIdentifier == 19) {
            if (count($_SESSION['cart']) == 0) {
                $shop->go404();
            }
            if ($_SESSION['currency'] == 'usd') {
                $config['sing']     = "";
                $config['sing_usd'] = $_SESSION["sign"];
            } else {
                $config['sing']     = $_SESSION["sign"];
                $config['sing_usd'] = "";
            }

            $config['delivery_default'] = $modx->db->getValue($modx->db->query("SELECT delivery_id FROM `modx_a_delivery` WHERE delivery_default = 1"));

            $user['user_fullname'] = isset($_SESSION['webuser']['fullname']) ? $_SESSION['webuser']['fullname'] : "";
            $user['user_email']    = isset($_SESSION['webuser']['email']) ? $_SESSION['webuser']['email'] : "";
            $user['user_phone']    = isset($_SESSION['webuser']['phone']) ? $_SESSION['webuser']['phone'] : "";

            $modx->documentObject = array_merge($modx->documentObject, $user, $config);
        }
        break;
    case 'OnWebPagePrerender' : // Отрабатывает после того, как все теги обработаны и перед тем, как страница отправится в браузер
        // Подключение javascript если необходимо
        if (isset($_SESSION['js']) && !empty($_SESSION['js'])) {
            $js = $_SESSION['js'];
            unset($_SESSION['js']);
        }
        if (isset($js) && !empty($js)) {
            $js = "<script type='text/javascript'>".$js."</script>";
            $modx->documentOutput = str_ireplace(["</body>","</html>"], ["",$js."</body></html>"], $modx->documentOutput);
        }

        // Включение кеширования в браузере
        //header("Cache-control: public");
        //header("Expires: " . gmdate("D, d M Y H:i:s", time() + 60*60*24) . " GMT");
        break;
    case 'OnMakePageCacheKey' : // Создание уникального ключа для кеша фильтров
        $templates = explode(',', $modx->config['filters_templates']);
        if (in_array($modx->documentObject['template'], $templates)) {
            $urlParts = explode('/f/', $_GET['q']);
            $p = isset($_GET['p']) && (int)$_GET['p'] > 1 ? '__'.(int)$_GET['p'] : '';
            if ($urlParts > 0) {
                $modx->event->output($modx->event->params['id'].'__'.md5(serialize($modx->event->params).end($urlParts).$_GET['lang']).$p);
            }
        }
        break;
    case 'OnBeforeWUsrFormSave' : // Действия после сохранения пользователя
        break;
    case 'OnLoadWebPageCache' : // Отрабатывает при загрузке страницы из кеша
        break;
    case 'OnBeforeSaveWebPageCache' : // Отрабатывает перед сохранением страницы в кеш
        // Минификация контента перед сохранением
        if ($modx->config['minify_on'] == 1) {
            $documentContent = $modx->runSnippet("Minify", array("get" => "html","input" => $modx->documentContent));
            if (!empty($documentContent)) $modx->documentContent = $documentContent;
        }
        break;
    case 'OnCacheUpdate' : // Очистка кастомных кешей
        echo $shop->clearCacheRecursive('images', 'snippets');
        break;
    case "OnModFormSave": // Отрабатывает после входа на страницу системных настроек
        /* Проверка корректной установки таблиц модулей*/
        // Поиск установленных модулей
        $modules = $modx->db->makeArray($modx->db->query("SELECT `modulecode` FROM ".$modx->getFullTableName('site_modules')." WHERE `disabled` = '0'"));
        $inDatabase = [];
        if (is_array($modules) && count($modules) > 0) {
            foreach ($modules as $module) {
                preg_match_all("|@dependencies requires files located at (.*+)|U", $module['modulecode'], $out, PREG_SET_ORDER);
                if (is_string($out[0][1])) {
                    $inDatabase[] = str_replace('//', '/', MODX_BASE_PATH.trim($out[0][1]).'seed.database.php');
                }
            }
            // Поиск сидов
            $inFiles = glob(MODX_BASE_PATH.'assets/modules/*/seed.database.php');
            // Сведение в массив
            $lists = array_intersect($inDatabase, $inFiles);
            // Прогонка сидов и настройка необходимых таблиц
            if (is_array($lists) && count($lists) > 0) {
                foreach ($lists as $list) {
                    // Получить массив сида
                    $tables = require_once $list;
                    if (is_array($tables) && count($tables) > 0) {
                        foreach ($tables as $name => $table) {
                            if (($name = preg_replace ("/[^a-zA-Z0-9_\s]/", "", $name)) != '') {
                                $name = $modx->getFullTableName($name);
                                $check = (bool)$modx->db->getValue($modx->db->query("SHOW TABLES LIKE '".str_replace('`', '', end(explode('.', $name)))."'"));
                                // Проверить существование таблицы
                                if ($check === false) {
                                    // Создать структуру таблицы
                                    if (isset($table['builder']) && is_string($table['builder']) && strlen($table['builder']) > 0) {
                                        // Обработка переменных
                                        $builder = str_replace(
                                            ["`{PREFIX_TABLE_NAME}`","{CURRENT_URL}"],
                                            [$name,$modx->config['site_url']],
                                            $table['builder']
                                        );
                                        preg_match_all("|`{PREFIX_(.*)}`|U", $builder, $outs, PREG_SET_ORDER);
                                        if (is_array($outs) && count($outs)) {
                                            foreach ($outs as $out) {
                                                $builder = str_replace($out[0], $modx->getFullTableName($out[1]), $builder);
                                            }
                                        }
                                        $modx->db->query($builder);

                                        // Наполнить таблицу данными
                                        if (isset($table['seeder']) && is_array($table['seeder']) && count($table['seeder']) > 0) {
                                            foreach ($table['seeder'] as $seeder) {
                                                // Обработка переменных
                                                $seeder = str_replace(
                                                    ["`{PREFIX_TABLE_NAME}`","{CURRENT_URL}"],
                                                    [$name,$modx->config['site_url']],
                                                    $seeder
                                                );
                                                preg_match_all("|`{PREFIX_(.*)}`|U", $seeder, $outs, PREG_SET_ORDER);
                                                if (is_array($outs) && count($outs)) {
                                                    foreach ($outs as $out) {
                                                        $seeder = str_replace($out[0], $modx->getFullTableName($out[1]), $seeder);
                                                    }
                                                }
                                                // Выполнение запроса
                                                $modx->db->query($seeder);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        break;
}