<?php
/**
 * Обработчик входящих данных
 */
 // Отписка от рассылки
if (isset($_GET['unsubscribe'])) {
    $modx->db->query("DELETE FROM `modx_a_mailer_users` WHERE user_email = '".$modx->db->escape($_GET['unsubscribe'])."'");
    $_SESSION['js'] = '$("#info-success").find(".title-desc").html("'.$translate[129].'");$("#info-success").modal("show");'; // Вы отписались от рассылки. Нам очень жаль
    die(header("Location: /".$modx->config['lang']."/"));
}

// Автологин
if ((isset($_COOKIE['autologin']) && !empty($_COOKIE['autologin'])) && (!isset($_SESSION['webInternalKey']) || empty($_SESSION['webInternalKey']))) {
    $tokenid = hash('sha512', json_encode($modx->getUserData()));
    if ($tokenid == $_COOKIE['autologin']) {
        $username = $modx->db->getValue("
            SELECT email 
            FROM `modx_web_user_attributes` 
            WHERE internalKey = (SELECT internalKey FROM `modx_web_user_tokens` WHERE tokenid = '".$tokenid."')
        ");
        $modx->runSnippet("Auth", ["login" => $username, "remember" => 1, "autologin" => true]);
    }
}

// Авторизация
if (is_array($_POST['login']) && count($_POST['login']) > 0) {
    $user = $modx->runSnippet("Auth", ["login" => $_POST['login']['email'], "pass" => $_POST['login']['password'], "remember" => $_POST['login']['remember']]);
    switch ($user) {
        case 11:
            $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[13].'");$("#info-error").modal("show");'; // Пользователь не найден
            break;
        case 13:
            $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[14].'");$("#info-error").modal("show");'; // Неверный логин или пароль
            break;
        case 444:
            $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[30].'");$("#info-error").modal("show");'; // Пользователь заблокирован
        case 159263:
            die(header("Location: ".$modx->makeUrl($modx->config['webuser_account'])));
            break;
    }
}

// Выход из личного кабинета
if (isset($_GET['logout']) || isset($_GET['logout/'])) {
    $internalKey = $_SESSION['webInternalKey'];
    $username = $_SESSION['webShortname'];
    // invoke OnBeforeWebLogout event
    $modx->invokeEvent("OnBeforeWebLogout", ["userid" => $internalKey, "username" => $username]);
    unset($_SESSION['webShortname']);
    unset($_SESSION['webFullname']);
    unset($_SESSION['webEmail']);
    unset($_SESSION['webValidated']);
    unset($_SESSION['webInternalKey']);
    unset($_SESSION['webValid']);
    unset($_SESSION['webUser']);
    unset($_SESSION['webFailedlogins']);
    unset($_SESSION['webLastlogin']);
    unset($_SESSION['webnrlogins']);
    unset($_SESSION['webUsrConfigSet']);
    unset($_SESSION['webUserGroupNames']);
    unset($_SESSION['webDocgroups']);
    unset($_SESSION['webDocgrpNames']);
    unset($_SESSION['webuser']);
    unset($_SESSION['cart']);
    unset($_SESSION['order_num']);
    unset($_SESSION['order_cost']);
    unset($_SESSION['hash']['reg']);
    setcookie("autologin", "", time() - 3600);
    // invoke OnWebLogout event
    $modx->invokeEvent("OnWebLogout", ["userid" => $internalKey, "username" => $username]);
    $modx->sendRedirect($modx->makeUrl($modx->config['site_start']));
    die();
}

// Регистрация
if (is_array($_POST['reg']) && count($_POST['reg']) > 0 && $_SESSION['hash']['reg'] != md5(serialize($_POST['reg']))) {
    $data  = $_POST['reg'];
    $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['hash']['reg'] = md5(serialize($_POST['reg']));
        $check = $modx->db->getRow($modx->db->query('
            SELECT 
            COUNT(*) AS "cnt" 
            FROM `modx_web_user_attributes` 
            WHERE email = "'.$modx->db->escape($email).'"
        '));
        if ($check['cnt'] > 0) {
            $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[12].'");$("#info-error").modal("show");'; // Пользователь с таким Email уже зарегистрирован
        } else {
            $password = $modx->runSnippet("Shop", ["get" => "setToken"]);
            $data = Array(
                "username"   => $modx->db->escape($email),
                "password"   => md5($password),
                "email"      => $modx->db->escape($email)
            );
            $modx->db->query("INSERT INTO `modx_web_users` SET username = '{$data['username']}', password = '{$data['password']}'");
            $user_id  = $modx->db->getInsertId();
            $modx->db->query("
                INSERT INTO `modx_web_user_attributes` SET 
				    internalKey = '$user_id', 
					email       = '{$data['email']}'
		    ");
            $modx->db->query("INSERT INTO `modx_web_groups` SET webgroup = 1, webuser = '$user_id'");
            $shop->sendMail($email, "register", ["email" => $email, "password" => $password]);
            $shop->sendMail($modx->config['emailsender'], "register_admin", ["email" => $email]);
            $_SESSION['js'] = '$("#info-success").find(".title-desc").html("'.$translate[16].'");$("#info-success").modal("show");'; // Пользователь успешно зарегистрирован
            die(header("Location: /".$modx->config['lang']."/"));
        }
    } else {
        $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[17].'");$("#info-error").modal("show");'; // Введите корректный Email
    }
}

// Восстановление пароля
if (is_array($_POST['restore']) && count($_POST['restore']) > 0) {
    $back  = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI'];
    $data  = $_POST['restore'];
    $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['hash']['restore'] = md5(serialize($_POST['restore']));
        $check = $modx->db->getValue($modx->db->query('
            SELECT internalKey
            FROM `modx_web_user_attributes` 
            WHERE email = "'.$modx->db->escape($email).'"
        '));

        if ((int)$check > 0) {
            $pwd = $shop->genPass();
            $modx->db->query("
                UPDATE `modx_web_users` 
                SET cachepwd = '".md5($pwd)."' 
                WHERE id = ".(int)$check
            );

            $shop->sendMail($email, "recovery", ["email" => $email, "password" => $pwd, "link" => $modx->makeUrl(1)."?restore=".md5($pwd)]);
            $_SESSION['js'] = '$("#info-success").find(".title-desc").html("'.$translate[27].'");$("#info-success").modal("show");'; // На Ваш email отправлены инструкции.
            die(header("Location: ".$modx->makeUrl($modx->config['site_start']))); // Переадресация на главную
        } else {
            $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[13].'");$("#info-error").modal("show");'; // Пользователь не найден
        }
    } else {
        $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[17].'");$("#info-error").modal("show");'; // Введите корректный Email
    }

    die(header("Location: ".$back));
}

if (isset($_GET['restore']) && $shop->isValidMd5($_GET['restore'])) {
    $check = $modx->db->getRow($modx->db->query("
        SELECT *
        FROM `modx_web_users` 
        WHERE cachepwd = '".$_GET['restore']."'
    "));

    if (is_array($check) && count($check) > 0) {
        $modx->db->query("
            UPDATE `modx_web_users` 
            SET password = '".$check['cachepwd']."', cachepwd = '' 
            WHERE id = ".$check['id']
        );
        $_SESSION['js'] = '$("#info-success").find(".title-desc").html("'.$translate[28].'");$("#info-success").modal("show");'; // Ваш пароль успешно восстановлен. Войдите используя данные из Email.
    } else {
        $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[13].'");$("#info-error").modal("show");'; // Пользователь не найден
    }

    die(header("Location: ".$modx->makeUrl(1)));
}

// Изменения личных данных
if (is_array($_POST['account']) && count($_POST['account']) > 0) {
    $data  = $_POST['account'];
    $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $newEmail = $email != $_SESSION['webuser']['email'] ? $email : null;
        $newPassw = $data['password'] != $_SESSION['webuser']['forgotPass'] ? $data['password'] : null;

        $check = $modx->db->getRow($modx->db->query('
            SELECT 
            COUNT(*) AS "cnt" 
            FROM `modx_web_user_attributes` 
            WHERE email = "'.$modx->db->escape($email).'"
        '));
        if ($check['cnt'] > 0 && $newEmail) {
            $js = '$("#info-error").find(".title-desc").html("'.$translate[12].'");$("#info-error").modal("show");'; // Пользователь с таким Email уже зарегистрирован
        } else {
            if ($newEmail && $newPassw) {
                $modx->db->query("
                    UPDATE `modx_web_user_attributes` SET 
                        fullname    = '".$modx->db->escape($data['fullname'])."', 
                        email       = '".$modx->db->escape($newEmail)."', 
                        phone       = '".$modx->db->escape($data['phone'])."', 
                        forgotPass  = '".$modx->db->escape($newPassw)."'
                    WHERE internalKey = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $modx->db->query("
                    UPDATE `modx_web_users` SET 
                        username = '".$modx->db->escape($newEmail)."', 
                        password = '".md5($newPassw)."' 
                    WHERE id = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $shop->sendMail($email, "user_accept", ["email" => $email, "forgotPass" => $data['password']]);
            } elseif ($newEmail && !$newPassw) {
                $modx->db->query("
                    UPDATE `modx_web_user_attributes` SET 
                        fullname    = '".$modx->db->escape($data['fullname'])."', 
                        phone       = '".$modx->db->escape($data['phone'])."', 
                        email       = '".$modx->db->escape($newEmail)."'
                    WHERE internalKey = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $modx->db->query("
                    UPDATE `modx_web_users` SET 
                        username = '".$modx->db->escape($newEmail)."'
                    WHERE id = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $shop->sendMail($email, "user_accept", ["email" => $email, "forgotPass" => $data['password']]);
            } elseif (!$newEmail && $newPassw) {
                $modx->db->query("
                    UPDATE `modx_web_user_attributes` SET 
                        fullname    = '".$modx->db->escape($data['fullname'])."', 
                        phone       = '".$modx->db->escape($data['phone'])."', 
                        forgotPass  = '".$modx->db->escape($newPassw)."'
                    WHERE internalKey = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $modx->db->query("
                    UPDATE `modx_web_users` SET 
                        password = '".md5($newPassw)."' 
                    WHERE id = ".(int)$_SESSION['webuser']['internalKey']."
                ");
                $shop->sendMail($email, "user_accept", ["email" => $email, "forgotPass" => $data['password']]);
            } else {
                $modx->db->query("
                    UPDATE `modx_web_user_attributes` SET 
                        fullname    = '".$modx->db->escape($data['fullname'])."',
                        phone       = '".$modx->db->escape($data['phone'])."'
                    WHERE internalKey = ".(int)$_SESSION['webuser']['internalKey']."
                ");
            }
            $js = '$("#info-success").find(".title-desc").html("'.$translate[101].'");$("#info-success").modal("show");'; // Данные изменены
            unset($_SESSION['webShortname']);
            unset($_SESSION['webFullname']);
            unset($_SESSION['webEmail']);
            unset($_SESSION['webValidated']);
            unset($_SESSION['webInternalKey']);
            unset($_SESSION['webValid']);
            unset($_SESSION['webUser']);
            unset($_SESSION['webFailedlogins']);
            unset($_SESSION['webLastlogin']);
            unset($_SESSION['webnrlogins']);
            unset($_SESSION['webUsrConfigSet']);
            unset($_SESSION['webUserGroupNames']);
            unset($_SESSION['webDocgroups']);
            unset($_SESSION['webDocgrpNames']);
            unset($_SESSION['webuser']);
            $user = $modx->runSnippet("Auth", ["email" => $email, "autologin" => true]);
        }
    } else {
        $js = '$("#info-error").find(".title-desc").html("'.$translate[17].'");$("#info-error").modal("show");'; // Введите корректный Email
    }
}

// Сохранение данных о доставке в ЛК
if (is_array($_POST['delivery']) && count($_POST['delivery']) > 0) {
    $deliveries = $_POST['delivery'];
    if (is_array($deliveries)) {
        foreach ($deliveries as $key => $delivery) {
            if (
                !empty($delivery['name']) &&
                !empty($delivery['phone']) &&
                !empty($delivery['city']) &&
                !empty($delivery['street']) &&
                !empty($delivery['house'])
            ) {
                if ((int)$key > 0) {
                    $modx->db->query("
                        UPDATE `modx_web_user_deliveries` SET 
                            delivery_name       = '".$modx->db->escape($delivery['name'])."',
                            delivery_phone      = '".$modx->db->escape($delivery['phone'])."',
                            delivery_city       = '".$modx->db->escape($delivery['city'])."',
                            delivery_street     = '".$modx->db->escape($delivery['street'])."',
                            delivery_house      = '".$modx->db->escape($delivery['house'])."',
                            delivery_apartament = '".$modx->db->escape($delivery['apartment'])."',
                            delivery_comment    = '".$modx->db->escape($delivery['comment'])."'
                        WHERE delivery_id = ".(int)$key."
                    ");
                } else {
                    $modx->db->query("
                        INSERT INTO `modx_web_user_deliveries` SET 
                            delivery_user       = ".(int)$_SESSION['webuser']['internalKey'].",
                            delivery_name       = '".$modx->db->escape($delivery['name'])."',
                            delivery_phone      = '".$modx->db->escape($delivery['phone'])."',
                            delivery_city       = '".$modx->db->escape($delivery['city'])."',
                            delivery_street     = '".$modx->db->escape($delivery['street'])."',
                            delivery_house      = '".$modx->db->escape($delivery['house'])."',
                            delivery_apartament = '".$modx->db->escape($delivery['apartment'])."',
                            delivery_comment    = '".$modx->db->escape($delivery['comment'])."'
                    ");
                }
            }
        }
    }
}

// Создание заказа
if (is_array($_POST['checkout']) && count($_POST['checkout']) > 0) {
    $checkout = $_POST["checkout"];
    // Если корзина пуста - переадресовываем на главную
    if (!is_array($_SESSION['cart']) || count($_SESSION['cart']) == 0) {
        die(header("Location: ".$modx->makeUrl($modx->config['site_start'])));
    }

    if ((int)$checkout['price']['cart'] >= PHP_INT_MAX ) { // Целочисленное переполнение суммы заказа
        $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate['22'].'");$("#info-error").modal("show");'; // Что-то пошло не так.
        die(header("Location: ".$modx->makeUrl($modx->config['site_start']))); // В корзину
    }

    if ($_SESSION['hash']['checkout'] != $checkout['token']) {
        $_SESSION['hash']['checkout'] = $checkout['token'];

        $errors = false;
        // Авторегистрация пользователя
        if ((int)$modx->config['auto_create_account'] == 1 && (int)$_SESSION['webuser']['internalKey'] == 0) {
            $email = filter_var($checkout['user']['email'], FILTER_SANITIZE_EMAIL);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $check = $modx->db->getRow($modx->db->query('
                    SELECT
                    COUNT(*) AS "cnt"
                    FROM `modx_web_user_attributes`
                    WHERE email = "'.$modx->db->escape($email).'"
                '));
                if ($check['cnt'] > 0) {
                    $errors = true;
                    $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[12].'");$("#info-error").modal("show");'; // Пользователь с таким Email уже зарегистрирован
                    die(header("Location: ".$modx->makeUrl(19))); // Переадресация на страницу оформление заказа, если уже есть email такой
                } else {
                    $password = $modx->runSnippet("Shop", ["get" => "setToken"]);
                    $data = [
                        "username" => $modx->db->escape($email),
                        "password" => md5($password),
                        "email"    => $modx->db->escape($email)
                    ];
                    $modx->db->query("INSERT INTO `modx_web_users` SET username = '{$data['username']}', password = '{$data['password']}'");
                    $user_id  = $modx->db->getInsertId();
                    $modx->db->query("
                        INSERT INTO `modx_web_user_attributes` SET 
                            internalKey = '$user_id', 
                            email       = '{$data['email']}',
                            fullname    = '{$modx->db->escape($checkout['user']['fullname'])}',
                            phone       = '{$modx->db->escape($checkout['user']['phone'])}'
                    ");
                    $modx->db->query("INSERT INTO `modx_web_groups` SET webgroup = 1, webuser = '$user_id'");
                    $shop->sendMail($email, "register", ["email" => $email, "password" => $password]);
                    $shop->sendMail($modx->config['emailsender'], "register_admin", ["email" => $email]);
                    $modx->runSnippet("Auth", ["email" => $email, "pass" => $password]);
                }
            } else {
                $errors = true;
                $_SESSION['js'] = '$("#info-error").find(".title-desc").html("'.$translate[17].'");$("#info-error").modal("show");'; // Введите корректный Email
            }
        }
//        // Промокод
//        $promocode = "";
//        if (trim($checkout['payment']['code']) != '' && (int)$checkout['price']['discount'] > 0) {
//            $promocode = trim($checkout['payment']['code']);
//            $checkout['price']['cart'] = $checkout['price']['total'] - $checkout['price']['delivery'];
//        }
        // Пишем данные о заказе
        if (!$errors) {
            $modx->db->query("
                INSERT INTO `modx_a_order` SET
                    order_created           = '".date("Y-m-d H:i:s")."',
                    order_client            = '".$modx->db->escape(strip_tags(json_encode($checkout['user'])))."',
                    order_delivery          = '".(int)$checkout['delivery']."',
                    order_delivery_cost     = '".((int)$checkout['delivery'] == 1 ? $modx->config['as_delivery'] : 0)."',
                    order_installation      = '".(int)$checkout['install']."',
                    order_installation_cost = '".((int)$checkout['install'] == 1 ? $modx->config['as_installation'] : 0)."',
                    order_cost              = '".number_format(floatval($checkout['price']['cart']), 0, '.', '')."',
                    order_comment           = '".$modx->db->escape(strip_tags(json_encode($_SESSION['cart_string'])))."',
                    order_lang              = '".$modx->config['lang']."',
                    order_currency          = '".$_SESSION['currency']."'
            ");
            $order_id = $modx->db->getInsertId();
            // Формируем письмо и данные о товарах в заказе
            $pids = implode(",", array_keys($_SESSION['cart']));
            $modx->db->query("SET group_concat_max_len = 16384;");
            $items = $modx->db->makeArray($modx->db->query("
                SELECT 
                	`po`.*,
                    `p`.`product_id`, 
                    `p`.`product_code`, 
                    `p`.`product_price`, 
                    `p`.`product_url`,
                    (SELECT product_name FROM `modx_a_product_strings` WHERE product_id = p.product_id AND string_locate = '{$modx->config['lang']}') AS 'product_name',
                    (SELECT image_file FROM `modx_a_product_images` WHERE image_product_id = p.product_id AND image_use = 'cover') AS 'product_cover',
                    (SELECT CONCAT('[',
                            GROUP_CONCAT(
                                '{\"filter_name\":\"',COALESCE(`fs`.`filter_name`,''),'\",
                                \"filter_type\":\"',COALESCE(`f`.`filter_type`,''),'\",
                                \"filter_url\":\"',COALESCE(`f`.`filter_url`,''),'\",
                                \"filter_value\":\"',COALESCE(`vs`.`filter_value_{$modx->config['lang']}`,''),'\"}'
                            ), ']')
		                    FROM `modx_a_product_filter_values` `pfv`
		                    LEFT JOIN `modx_a_filters` `f` ON `pfv`.`filter_id` = `f`.`filter_id`
		                    LEFT JOIN `modx_a_filter_strings` `fs` ON `pfv`.`filter_id` = `fs`.`filter_id` AND `string_locate` = '{$modx->config['lang']}'
		                    LEFT JOIN `modx_a_filter_values` `vs` ON `pfv`.`value_id` = `vs`.`value_id` AND `string_locate` = '{$modx->config['lang']}'
		                    WHERE `pfv`.`product_id` = `po`.`id`
                    ) AS 'characteristics'
                FROM `modx_a_product_options` `po`
                LEFT JOIN `modx_a_products` `p` ON `p`.`product_id` = `po`.`product`
                WHERE `po`.`id` IN ({$pids})
            "));
            if ($_SESSION['currency'] == 'usd') {
                $mail['sing']     = "";
                $mail['sing_usd'] = $_SESSION["sign"];
            } else {
                $mail['sing']     = $_SESSION["sign"];
                $mail['sing_usd'] = "";
            }

            foreach ($items as $item) {
                $product_price         = $shop->getPrice($item['price'], $_SESSION['currency'], '');
                $item['product_count'] = $_SESSION['cart'][$item['product_id']];
                $item['product_total'] = number_format(($item['product_count'] * $product_price), 2, '.', '&nbsp;');
                $item['product_url']   = $modx->makeUrl($modx->config['site_start']).$item['product_url'].'/';
                $item['product_cover'] = !empty($item['product_cover']) ? 'assets/products/'.$item['product_id'].'/'.$item['product_cover'] : 'assets/snippets/phpthumb/noimage.png';
                $item['product_price'] = $shop->getPrice($item['price'], $_SESSION['currency']);
                $item['sing']          = $mail['sing'];
                $item['sing_usd']      = $mail['sing_usd'];
                $modx->db->query("INSERT INTO `modx_a_order_products` SET 
                        order_id      = {$order_id},
                        product_id    = {$item['product_id']},
                        option_id     = {$item['id']},
                        product_count = {$_SESSION['cart'][$item['id']]},
                        product_price = {$product_price}
                ");
                if ($characteristics = $shop->is_json($item['characteristics'])) {
                    if (is_array($characteristics)) {
                        foreach ($characteristics as $characteristic) {
                            $item['charac'] .= $modx->parseChunk('mail_order_item_characteristic', $characteristic);
                        }
                    }
                }
                $mail['items']        .= $modx->parseChunk("mail_order_item", $item);
            }
            $mail['order_id']        = str_pad($order_id, 9, 0, STR_PAD_LEFT);
            $mail['name']            = $checkout['user']['name'];
            $mail['items']           = $modx->parseDocumentSource($mail['items']);
            if ((int)$checkout['delivery'] == 1) {
                $mail['delivery'] = '[#114#]';
                $mail['delivery_active'] = '439336'; // это цвет
            } else {
                $mail['delivery'] = '[#117#]';
                $mail['delivery_active'] = 'ABABAB'; // это цвет
            }
            if ((int)$checkout['install'] == 1) {
                $mail['install'] = '[#114#]';
                $mail['install_active'] = '439336'; // это цвет
            } else {
                $mail['install'] = '[#117#]';
                $mail['install_active'] = 'ABABAB'; // это цвет
            }
            $mail['total'] = number_format($checkout['price']['cart'], 0, '.', '&nbsp;');

            $mail = array_merge($mail, $checkout['price'], $checkout['user']);
            // Письма покупателю и менеджеру
            if (isset($checkout['user']['email']) && $checkout['user']['email'] != "") {
                $shop->sendMail($checkout['user']['email'], "order", $mail);
            }
            $shop->sendMail($modx->config['emailsender'], "order_admin", $mail);
            // Перенаправление на страницу "Спасибо за заказ"
            $_SESSION['order_num']  = $order_id;
            $_SESSION['order_cost'] = $mail['total'];
            unset($_SESSION['cart']);
        }
        die(header("Location: ".$modx->makeUrl($modx->config['shop_thanks_order'])));
    }
}

// Задать вопрос
if (is_array($_POST['question']) && count($_POST['question']) > 0 && $_SESSION['hash']['question'] != md5(serialize($_POST['question']))) {
    $data  = $_POST['question'];
        $modx->db->query("
            INSERT INTO `modx_a_faqs` SET 
                faq_active   = 0,
                faq_category = 0,
                faq_name     = '".$modx->db->escape($data['name'])."',
                faq_phone    = '".$modx->db->escape($data['phone'])."'
         ");
        $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
        $modx->db->query("
            INSERT INTO `modx_a_faq_strings` SET 
                string_locate = '{$modx->config['lang']}',
                faq_id        = '{$result['last_id']}',
                faq_question  = '".$modx->db->escape($data['question'])."'
        ");
    $shop->sendMail($modx->config['emailsender'], "question", $data);
    die(header("Location: ".$modx->makeUrl($modx->config['success_send'])));
}


