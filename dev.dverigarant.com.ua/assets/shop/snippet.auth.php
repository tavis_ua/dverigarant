<?php
/**
 * Авторизация веб-пользователя
 * [[Auth?&login=`login`&pass=`password`&remember=`1`&autologin=`true`]]
 *
 * @var login     = логин
 * @var pass      = пароль
 * @var remember  = признак "Запомнить меня"
 * @var autologin = авторизация без проверки пароля
 *
 * @return 11 - Логин не найден в базе; 12 - Аккаунт заблокирован; 13 - Неверный пароль; 159263 - Всё хорошо
 */
if ($login == "") {
    return 0;
}
$username = $modx->db->escape(strip_tags($login));
$password = $md5 ? $pass : md5($pass);
$time     = time();
$query    = "
    SELECT *, u.`id` as 'uid' 
    FROM `modx_web_users` `u` 
    JOIN `modx_web_user_attributes` `a` ON `a`.`internalKey` = `u`.`id`
    LEFT JOIN `modx_web_groups` `g` ON `g`.`webuser` = `u`.`id`
    WHERE `u`.`username` = '$username'
";

$valid_user = $modx->db->getRow($modx->db->query($query));

// если авторизирован в админке
if ($_SESSION['mgrValidated']) {
    $autologin = true;
}

// пользователь не найден = логин не верен
if ($valid_user['username'] == "") {
    return 11;
}

// проверка пароля
if (!$autologin) {
    // пользователь ввел неверный пароль
    if ($valid_user['password'] != $password) {
        $modx->db->query("
            UPDATE `modx_web_user_attributes` SET 
                `failedlogincount` = `failedlogincount` + 1
                ".($valid_user['failedlogincount'] + 1 >= $modx->config['failed_login_attempts'] ?
                " , blocked = 1, blockedafter = '".$time."', blockeduntil = '".($time + $modx->config['blocked_minutes']*60)."' " : "")."
                WHERE `internalKey` = ".$valid_user['uid']
        );
        return 13;
    }
}

// логирование входа
$query = "
    UPDATE `modx_web_user_attributes` SET
        `sessionid`  = '".session_id()."',
        `logincount` = `logincount` + 1,
        `lastlogin`  = '".$time."'
    WHERE `internalKey` = ".$valid_user['uid']
;
$modx->db->query($query);

// полчаем группы документов доступных пользоваетлю
$dg  = '';
$i   = 0;
$sql = "
    SELECT `uga`.`documentgroup`
    FROM `modx_web_groups` `ug`
    INNER JOIN `modx_webgroup_access` `uga` ON `uga`.`webgroup` = `ug`.`webgroup`
    WHERE `ug`.`webuser` = ".$valid_user['uid']
;
$ds  = $modx->db->query($sql);
while ($row = $modx->db->getRow($ds,'num')) $dg[$i++] = $row[0];
/* закончили получать в array $dg лежит */
$_SESSION['webShortname']      = $valid_user['username'];
$_SESSION['webFullname']       = $valid_user['fullname'];
$_SESSION['webEmail']          = $valid_user['email'];
$_SESSION['webValidated']      = 1;
$_SESSION['webInternalKey']    = $valid_user['internalKey'];
$_SESSION['webValid']          = base64_encode($_POST['password']);
$_SESSION['webUser']           = base64_encode($valid_user['username']);
$_SESSION['webFailedlogins']   = $valid_user['failedlogincount'];
$_SESSION['webLastlogin']      = $valid_user['lastlogin'];
$_SESSION['webnrlogins']       = $valid_user['logincount'];
$_SESSION['webUserGroupNames'] = $modx->db->getColumn("name", "SELECT `wgn`.`name` FROM `modx_webgroup_names` `wgn` INNER JOIN `modx_web_groups` `wg` ON `wg`.`webgroup` = `wgn`.`id` AND `wg`.`webuser` = ".$valid_user['uid']);
$_SESSION['webDocgroups']      = $dg;

// запомнить меня
if ((int)$remember > 0) {
    $tokenid = hash('sha512', json_encode($modx->getUserData()));
    $_SESSION['modx.web.session.cookie.lifetime'] = intval($modx->config['session.cookie.lifetime']);
    setcookie("autologin", $tokenid, time()+intval($modx->config['session.cookie.lifetime']), '/');
    $modx->db->query("
    	INSERT INTO `modx_web_user_tokens` SET
    		`internalKey` = ".intval($valid_user['internalKey']).",
    		`tokenid`     = '".$tokenid."'
    	ON DUPLICATE KEY UPDATE
    		`tokenid`     = '".$tokenid."'
    ");
} else {
    $_SESSION['modx.web.session.cookie.lifetime'] = 0;
}

$_SESSION['webuser'] = $valid_user;

// для отображения ОНЛАЙН в админ панели
if ($id != $modx->documentIdentifier) {
    if (getenv("HTTP_CLIENT_IP")) {
        $ip = getenv("HTTP_CLIENT_IP");
    } else if (getenv("HTTP_X_FORWARDED_FOR")) {
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    } else if (getenv("REMOTE_ADDR")) {
        $ip = getenv("REMOTE_ADDR");
    } else {
        $ip = "UNKNOWN";
    }
    $_SESSION['ip'] = $ip;
    $itemid = isset($_REQUEST['id']) ? $_REQUEST['id'] : 'NULL' ;
    $a = 998;
    if ($a != 1) {
        // web users are stored with negative id
        $sql = "REPLACE INTO `modx_active_users` (internalKey, username, lasthit, action, id, ip) values(-".$_SESSION['webInternalKey'].", '".$_SESSION['webShortname']."', '".$time."', '".$a."', ".$itemid.", '$ip')";
        $modx->db->query($sql);
    }
}
return 159263;