<h2 class="muted">Товары магазина</h2>
<form action="index.php" method="GET" role="form">
    <input type="hidden" name="a" value="2">
    <input type="hidden" name="b" value="products">
    <input type="text" style="width:20%; float:left; margin:0 5px 0 0;" value="<?=$_GET['srch'];?>" name="srch" placeholder="ID или Название" class="form-control">
    <select name="filter_status" class="select2">
        <option value="" selected>Статус</option>
        <option value="1"<?php if('1' == $_GET['filter_status']) echo ' selected';?>>Новый</option>
        <option value="2"<?php if('2' == $_GET['filter_status']) echo ' selected';?>>Скидка</option>
        <option value="3"<?php if('3' == $_GET['filter_status']) echo ' selected';?>>Акция</option>
        <option value="4"<?php if('4' == $_GET['filter_status']) echo ' selected';?>>Топ</option>
        <option value="5"<?php if('5' == $_GET['filter_status']) echo ' selected';?>>Хит</option>
    </select>
    <select name="filter_publish" class="select2">
        <option value="" selected>Видимость</option>
        <option value="1"<?php if('1' == $_GET['filter_publish']) echo ' selected';?>>Опубликован</option>
        <option value="0"<?php if('0' == $_GET['filter_publish']) echo ' selected';?>>Не опубликован</option>
    </select>
    <select name="filter_availability" class="select2">
        <option value="" selected>Наличие</option>
        <option value="1"<?php if('1' == $_GET['filter_availability']) echo ' selected';?>>Есть в наличии</option>
        <option value="0"<?php if('0' == $_GET['filter_availability']) echo ' selected';?>>Нет в наличии</option>
    </select>
    <input type="submit" style="margin-right:20px;padding-top:5px;min-height:34px;" class="actionButtons" value="Поиск">
    <a href="<?=$url?>products" style="float: right;margin: 7px 0 0 0;display: inline-block;text-decoration: underline;"><b>Сбросить фильтры</b></a>
</form>
<br/>
<?php if (count($products) == 0 || !is_array($products)):?>
<div class="row"><h1 class="screen"><strong>Товаров не найдено</strong></h1></div>
<?php else: ?>
<table class="table table-condensed table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th width="70px" style="text-align:center;">ID</th>
        <th style="text-align:center;">Название</th>
        <th width="100px" style="text-align:center;">Статус</th>
        <th width="120px" style="text-align:center;">Цена</th>
        <th width="110px" style="text-align:center;">Видимость</th>
        <th width="110px" style="text-align:center;">Наличие</th>
        <th width="250px" style="text-align:center;">Действие</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($products as $product): ?>
    <?php $product['product_cover'] = !empty($product['product_cover']) ? '/assets/products/'.$product['product_id'].'/'.$product['product_cover'] : '/assets/snippets/phpthumb/noimage.png';?>

    <tr id="product-<?=$product['product_id']?>">
        <td class="vertical-align"><?=$product['product_id']?></td>
        <td>&nbsp;
            <img src="<?=$modx->runSnippet('R', array('img' => $product['product_cover'], "folder" => "backendProducts/".$product['product_id'], 'opt' => 'w=50&h=30&far=1'))?>" alt="<?php if ($product['product_cover']) echo $product['product_cover'];?>" class="img-thumbnail"/>&emsp;
            <?php if ($modx->config['shop_root_item']): ?>
                <a href="<?=$modx->config['site_url']?><?=$modx->config['lang_default']?>/<?=$product['product_url']?>/" target="_blank">
            <?php else: ?>
                <a href="<?=$modx->makeUrl($modx->config['shop_page_item'], '', '', 'full')?><?=$product['product_url']?>/" target="_blank">
            <?php endif; ?>
                <b><?=$product['product_name']?></b>
            </a>
        </td>
        <td class="vertical-align" style="text-align:center;">
            <?php if($product['product_status'] == 1):?><span class="label label-default">Новый</span><?php endif;?>
            <?php if($product['product_status'] == 2):?><span class="label label-default">Распродажа</span><?php endif;?>
            <?php if($product['product_status'] == 3):?><span class="label label-default">Акция</span><?php endif;?>
            <?php if($product['product_status'] == 4):?><span class="label label-default">Топ</span><?php endif;?>
            <?php if($product['product_status'] == 5):?><span class="label label-default">Хит</span><?php endif;?>
        </td>
        <td class="vertical-align" style="text-align:center;">&nbsp;<?=number_format($product['price'], 0, '.', ' ')?> грн</td>
        <td class="vertical-align" style="text-align:center;">
            <b>
                <?php if($product['product_active'] == 0):?><span class="label label-danger">Не опубликован</span><?php endif;?>
                <?php if($product['product_active'] == 1):?><span class="label label-success">Опубликован</span><?php endif;?>
            </b>
        </td>
        <td class="vertical-align" style="text-align:center;">
            <b>
                <?php if($product['product_availability'] == 1):?><span class="label label-success">Есть в наличии</span><?php endif;?>
                <?php if($product['product_availability'] == 0):?><span class="label label-default">Нет в наличии</span><?php endif;?>
            </b>
        </td>
        <td class="vertical-align" style="text-align:center;">
            <a href="<?=$url?>products&c=edit&i=<?=$product['product_id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a>
            <a href="#" data-href="<?=$url?>products&c=delete&i=<?=$product['product_id']?>" data-toggle="modal" data-target="#confirm-delete" data-id="<?=$product['product_id']?>" data-name="<?=$product['product_name']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">Подтвердить удаление</div>
            <div class="modal-body">
                Вы уверены, что хотите удалить товар <b id="confirm-name"></b> с ID <b id="confirm-id"></b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                <a class="btn btn-danger btn-ok">Удалить</a>
            </div>
        </div>
    </div>
</div>
<div id="actions">
    <ul class="actionButtons">
        <li id="Button2" class="transition">
            <a href="<?=$url?>products&c=add"><i class="fa fa-plus-square"></i>&emsp;Создать</a>
        </li>
    </ul>
</div>
<img src="/inspiration/media/style/MODxRE2/images/misc/ajax-loader.gif" id="img-preview" style="display: none;" class="img-thumbnail">
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("table img").on("mouseenter", function() {
            var alt = jQuery(this).attr("alt");
            if (alt.length > 0) {
                jQuery("#img-preview").attr("src", alt).show();
            }
        });
        jQuery("table img").on("mouseleave", function(){
            jQuery("#img-preview").hide();
        });
        jQuery('#confirm-delete').on('show.bs.modal', function(e) {
            jQuery(this).find('#confirm-id').text(jQuery(e.relatedTarget).data('id'));
            jQuery(this).find('#confirm-name').text(jQuery(e.relatedTarget).data('name'));
            jQuery(this).find('.btn-ok').attr('href', jQuery(e.relatedTarget).data('href'));
        });
    });
</script>
