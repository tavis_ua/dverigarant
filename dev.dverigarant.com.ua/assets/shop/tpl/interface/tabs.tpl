<ul class="nav nav-tabs">
    <?php foreach ($languages as $k => $lng): ?>
    <li <?php if($k == 0) {echo 'class="active"';} ?>><a href="#t-<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><?=$lng?></b></a></li>
    <?php endforeach; ?>
</ul>
<div class="tab-content">
    <?php foreach ($languages as $k => $lng): ?>
        <div class="tab-pane <?php if($k == 0) {echo 'fade in active';} ?>" id="t-<?=$lng?>">
            <table class="table table-condensed table-bordered table-striped">
                <tr>
                    <td class="text-center">
                        <div class="has-success">
                            <input type="text" name="image[<?=$image_id?>][<?=$lng?>][image_alt]" value="<?=$image[$lng]['image_alt']?>" class="form-control" placeholder="Атрибут ALT" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="has-success">
                            <input type="text" name="image[<?=$image_id?>][<?=$lng?>][image_title]" value="<?=$image[$lng]['image_title']?>" class="form-control" placeholder="Название" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <div class="has-success">
                            <textarea name="image[<?=$image_id?>][<?=$lng?>][image_description]" rows="3" class="form-control" placeholder="Описание"><?=$image[$lng]['image_description']?></textarea>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    <?php endforeach; ?>
</div>