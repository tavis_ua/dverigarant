<form action="<?=$url?>variations&c=save" method="POST">
	<input type="hidden" name="options[pid]" value="<?=$_GET['pid'];?>">
	<input type="hidden" name="options[vid]" value="<?=$options['id'];?>">
	<div class="content">
		<ul class="nav nav-tabs">
			<?php //var_dump($options['id']); die; ?>
			<li class="active"><a href="#main" role="tab" data-toggle="tab"><b><i class="fa fa-id-card" aria-hidden="true"></i> Основное</b></a></li>
			<?php foreach ( $languages as $key => $lang): ?>
				<li><a href="#lang_<?php echo $lang; ?>" data-toggle="tab" class="text-uppercase"><span class="glyphicon glyphicon-pencil"></span> <?php echo $lang; ?></a></li>
			<?php endforeach; ?>
			<?php if ((int)$options['id'] > 0): ?>
				<li role="presentation" id="filterTab"><a href="#parameters" role="tab" data-toggle="tab"><b><i class="fa fa-cogs" aria-hidden="true"></i></i> Параметры</b></a></li>
			<?php endif; ?>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="main">
				<table class="table table-bordered table-condensed">
					<tr>
						<th class="vertical-align" width="160px">Видимость:</th>
						<td>
							<div class="bootstrap-switch">
								<input type="checkbox" name="options[active]" <?=($options['active'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Да" data-off-text="Нет" class="bsw">
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align" width="160px">Наличие:</th>
						<td>
							<div class="bootstrap-switch">
								<input type="checkbox" name="options[availability]" <?=($options['availability'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Да" data-off-text="Нет" class="bsw">
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align" width="160px">Использовать как параметры товара по умолчанию:</th>
						<td>
							<div class="bootstrap-switch">
								<input type="checkbox" name="options[default_option]" <?=($options['default_option'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Да" data-off-text="Нет" class="bsw">
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align">Цена:</th>
						<td>
							<div class="">
								<input type="number" name="options[price]" value="<?=$options['price']?>" class="form-control" data-validate="inputMaxSum:9999999.99" />
								<div class="error-text" style="display:none;">Значение суммы в поле <b>"Цена"</b> не должно превышать 9999999.99</div>
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align">Цена старая:</th>
						<td>
							<div class="">
								<input type="number" name="options[price_old]" value="<?=$options['price_old']?>" class="form-control" data-validate="inputMaxSum:9999999.99" />
								<div class="error-text" style="display:none;">Значение суммы в поле <b>"Цена старая"</b> не должно превышать 9999999.99</div>
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align">Артикул:</th>
						<td>
							<input type="text" name="options[product_code]" value="<?=$options['product_code']?>" class="form-control" />
						</td>
					</tr>
					<!--<tr>
						<th class="vertical-align">Вес / объем:</th>
						<td>
							<div class="">
								<input type="number" name="options[weight]" value="<?=$options['weight']?>" class="form-control" data-validate="inputMaxSum:9999999" />
								<div class="error-text" style="display:none;">Значение в поле <b>"Вес"</b> не должно превышать 9999999</div>
							</div>
						</td>
					</tr>
					<tr>
						<th class="vertical-align">Единицы измерения веса / объема:</th>
						<td>
							<div class="">
								<select class="form-control chosen-select" name="options[weight_type]">
									<option value="0" selected>Выберите единицы измерения</option>
									<option value="1"<?php if(1 == $options['weight_type']) echo ' selected'; ?>>г</option>
									<option value="2"<?php if(2 == $options['weight_type']) echo ' selected'; ?>>мл</option>
								</select>
							</div>
						</td>
					</tr> -->
				</table>
			</div>

			<?php foreach ( $languages as $key => $lang): ?>
				<div class="tab-pane" id="lang_<?=$lang?>">
					<table class="table table-condensed table-bordered table-striped">
						<tr>
							<td width="100px"><b>Название</b></td>
							<td><input type="text" required name="options[<?=$lang;?>_name]" class="form-control required" value="<?=htmlspecialchars($options[$lang . '_name']);?>" /></td>
						</tr>
						<tr>
							<td width="100px"><b>Описание:</b></td>
							<td><textarea name="options[<?=$lang;?>_description]" id="<?=$lang;?>_description" class="form-control required"><?=$options[$lang . "_description"]?></textarea></td>
						</tr>
						<!--<tr>
							<td width="100px"><b>Состав:</b></td>
							<td><textarea name="options[<?=$lang;?>_content]" id="<?=$lang;?>_content" class="form-control required"><?=$options[$lang . "_content"]?></textarea></td>
						</tr>-->
					</table>
				</div>
			<?php endforeach ?>
			<div role="tabpanel" class="tab-pane" id="parameters">
				<?php if (is_array($filters) && count($filters)): ?>
				<h3><b><span class="label label-primary">Фильтры</span></b></h3>
				<table class="table table-bordered table-condensed">
					<tr>
						<th class="vertical-align" style="text-align:center;" width="160px">Название</th>
						<th class="vertical-align" style="text-align:center;">Значение</th>
						<th class="vertical-align" style="text-align:center;" width="100px">Тип выбора</th>
					</tr>
					<?php foreach($filters as $fid => $filter): ?>
					<tr>
						<td><b><?=$filter['filter_name'];?></b></td>
						<td>
							<?php if ($filters['filter_type'] == 2): ?>
							<?php foreach ($languages as $k => $lng): ?>
							<div class="input-group">
								<div class="input-group-addon text-uppercase"><?=$lng?></div>
								<input type="text" name="options[option_filters][<?=$chid?>][0][filter_value_<?=$lng?>]" value="<?=$characteristic['filter_values'][$pfvs[$chid]]['filter_value_'.$lng]?>" class="form-control" data-validate="textMaxLenght:64" />
								<div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=$characteristic['filter_name'];?> <?=$lng?> значение"</b> не должно превышать 64 символа</div>
							</div>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php if ($filter['filter_type'] == 5): ?>
							<select class="form-control select2" style="width:100%" name="options[option_filters][<?=$fid?>]">
								<option value="" selected>Выберите значение</option>
								<?php if (is_array($filter['filter_values'])): ?>
									<?php foreach($filter['filter_values'] as $vid => $value): ?>
										<option value="<?=$vid?>" <?php if(in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select>
							<?php endif; ?>
							<?php if ($filter['filter_type'] == 6): ?>
							<select class="form-control select2" style="width:100%" name="options[option_filters][<?=$fid?>][]" multiple>
								<?php if (is_array($filter['filter_values'])): ?>
								<?php foreach($filter['filter_values'] as $vid => $value):?>
								<option value="<?=$vid?>" <?php if((is_array($pfvs[$fid]) && in_array($vid, $pfvs[$fid])) || in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
								<?php endforeach; ?>
								<?php endif; ?>
							</select>
							<?php endif; ?>
						</td>
						<td>
							<?php if($filter['filter_type'] == 1):?><span class="label label-default">Числовой</span><?php endif;?>
							<?php if($filter['filter_type'] == 2):?><span class="label label-default">Текстовый</span><?php endif;?>
							<?php if($filter['filter_type'] == 3):?><span class="label label-default">ИЛИ</span><?php endif;?>
							<?php if($filter['filter_type'] == 4):?><span class="label label-default">И</span><?php endif;?>
							<?php if($filter['filter_type'] == 5):?><span class="label label-default">Моно выбор</span><?php endif;?>
							<?php if($filter['filter_type'] == 6):?><span class="label label-default">Мульти выбор</span><?php endif;?>
						</td>
					</tr>
					<?php endforeach; ?>
				</table>
				<br/>
				<?php endif; ?>
				<?php if (is_array($characteristics) && count($characteristics)): ?>
				<h3><b><span class="label label-info">Характеристики</span></b></h3>
				<table class="table table-bordered table-condensed">
					<tr>
						<th class="vertical-align" style="text-align:center;" width="160px">Название</th>
						<th class="vertical-align" style="text-align:center;">Значение</th>
					</tr>
					<?php foreach($characteristics as $chid => $characteristic): ?>
					<tr>
						<td><b><?=$characteristic['filter_name'];?></b></td>
						<td>
							<?php if ($characteristic['filter_type'] == 2): ?>
							<?php foreach ($languages as $k => $lng): ?>
							<div class="input-group">
								<div class="input-group-addon text-uppercase"><?=$lng?></div>
								<input type="text" name="options[option_filters][<?=$chid?>][0][filter_value_<?=$lng?>]" value="<?=$characteristic['filter_values'][$pfvs[$chid]]['filter_value_'.$lng]?>" class="form-control" data-validate="textMaxLenght:64" />
								<div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=$characteristic['filter_name'];?> <?=$lng?> значение"</b> не должно превышать 64 символа</div>
							</div>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php if ($characteristic['filter_type'] == 5): ?>
							<select class="form-control select2" style="width:100%" name="options[option_filters][<?=$chid?>]">
								<option value="" selected>Выберите значение</option>
								<?php if (is_array($characteristic['filter_values'])): ?>
								<?php foreach($characteristic['filter_values'] as $vid => $value): ?>
								<option value="<?=$vid?>" <?php if(in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
								<?php endforeach; ?>
								<?php endif; ?>
							</select>
							<?php endif; ?>
							<?php if ($characteristic['filter_type'] == 6): ?>
							<select class="form-control select2" style="width:100%" name="options[option_filters][<?=$chid?>][]" multiple>
								<?php if (is_array($characteristic['filter_values'])): ?>
								<?php foreach($characteristic['filter_values'] as $vid => $value):?>
								<option value="<?=$vid?>" <?php if((is_array($pfvs[$chid]) && in_array($vid, $pfvs[$chid])) || in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
								<?php endforeach; ?>
								<?php endif; ?>
							</select>
							<?php endif; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</table>
				<?php endif; ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="content">
					<div class="panel panel-default">
						<div class="panel-body">
							<a href="<?=$url?>products&c=edit&i=<?=$_GET['pid'];?>" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-arrow-left"></span> <b>Назад</b></a>
							<button type="submit" onclick="documentDirty=false;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-save"></span> <b>Сохранить</b></button>
							<?php if (isset($item)): ?>
								<a href="<?=$url?>b=variations&c=delete&pid=<?=$_GET['pid'];?>&vid=<?=$options['id'];?>" class="btn btn-danger btn-lg pull-right js_confirm">
									<span class="glyphicon glyphicon-remove"></span> Удалить
								</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script>
	$(document).ready(function(){
		$(document).on('click', '.js_confirm', function(){
			if(!confirm("Точно удалить ?"))
				return false;
		});
		// $("button[type=submit]").on("click", function(){
		// 	var _errors = '';
		// 	$.each($('.required'), function( key, value ) {
		// 	if ($(this).val() == ''){
		// 		 _errors +=  'Не заполнено поле  "'+ $(this).parents('tr').find('td:eq(0) b').html()+'"\r';
		// 		 $(this).addClass('field_error');
		// 	}
		// });
		// 	if (_errors <= 0){
		// 		$("form").removeAttr("onsubmit").submit();
		// 	} else {
		// 		alert('Заполнените названия вариации на всех языках');
		// 	}
		// });
	});
</script>
<?=$tiny_mce?>