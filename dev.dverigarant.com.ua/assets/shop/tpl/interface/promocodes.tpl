<a id="create" href="#edit" data-toggle="modal" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Создать промо-код</a>
<br><br>
<table class="table table-bordered table-striped table-condensed table-hover">
    <thead>
        <tr>
            <th>Промо-код</th>
            <th>Работает до</th>
            <th>Статус промо-кода</th>
            <th>Тип кода</th>
            <th>Скидка %</th>
            <th>Использован раз</th>
            <th width="350px"></th>
        </tr>
    </thead>
    <tbody>
        <?php while ($c = $modx->db->getRow($codes)) : ?>
        <tr>
            <td><b><?=$c['code']?></b></td>
            <td><?=$c['code_expire']?></td>
            <td><?=($c['code_status'] ? '<span class="label label-success">Активен</span>' : '<span class="label label-danger">Не активен</span>')?></td>
            <td><?=($c['code_freq'] == 2 ? "Многоразовый" : "Одноразовый")?></td>
            <td><?=$c['code_mod']?></td>
            <td><a href="<?=$url?>orders&search=<?=$c['code']?>"><?=$c['used']?></a></td>
            <td>
                <a href="#edit" data-edit-code="<?=$c['code_id']?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                <a href="#" data-href="<?=$url?>promocodes&delete=<?=$c['code_id']?>" data-toggle="modal" data-target="#confirm-delete" data-id="<?=$c['code_id']?>" data-name="<?=$c['code']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                <?php if ($c['code_status']): ?>
                <a href="<?=$url?>promocodes&activate=<?=$c['code_id']?>" class="btn btn-danger"><span class="glyphicon glyphicon-off"></span> Выключить</a>
                <?php else: ?>
                <a href="<?=$url?>promocodes&activate=<?=$c['code_id']?>&status=1" class="btn btn-success"><span class="glyphicon glyphicon-off"></span> Включить</a>
                <?php endif; ?>
            </td>
        </tr>
        <?php endwhile; ?>
    </tbody>
</table>
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <form action="<?=$url?>promocodes" method="post">
                <div class="modal-body">
                    <table class="table-condensed">
                        <tr>
                            <td width="200px">Промо-код</td>
                            <td><input type="text" name="code[code]" required minlength="3" maxlength="32" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Действителен до</td>
                            <td>
                                <div class="input-group input-append dt date">
                                    <input data-format="yyyy-MM-dd" type="text" name="code[expire]" class="form-control add-on" value=""/>
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Размер скидки</td>
                            <td>
                                <div class="input-group">
                                    <input type="number" class="form-control" min="1" max="100" autocomplete="off" name="code[mod]" value="">
                                    <span class="input-group-addon"> % </span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Тип скидки</td>
                            <td>
                                <label class="span3"><input type="radio" name="code[freq]" checked value="1"> Одноразовый</label>
                                <label class="span3"><input type="radio" name="code[freq]" value="2"> Многоразовый</label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="code[id]">
                    <button class="btn btn-danger pull-left" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove-sign"></span> Отмена</button>
                    <button class="btn btn-primary pull-right">Обновить <span class="glyphicon glyphicon-ok-sign"></span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">Подтвердить удаление</div>
            <div class="modal-body">Вы уверены, что хотите удалить промокод <b id="confirm-name"></b></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                <a class="btn btn-danger btn-ok">Удалить</a>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function(){
        jQuery(document).on("click", "[data-edit-code]", function(){
            jQuery.ajax({url:"<?=$url?>promocodes&code="+jQuery(this).attr("data-edit-code"),type:"GET",dataType:"json",success:function(json) {
                jQuery("#edit [name='code[id]']").val(json.code_id);
                jQuery("#edit [name='code[code]']").val(json.code);
                jQuery("#edit [name='code[mod]']").val(json.code_mod);
                jQuery("#edit [name='code[expire]']").val(json.code_expire);
                jQuery("#edit [name='code[freq]'][value='"+json.code_freq+"']").attr("checked", true);
            }});
        });
        jQuery(document).on('show.bs.modal', "#confirm-delete", function(e) {
            jQuery(this).find('#confirm-id').text(jQuery(e.relatedTarget).data('id'));
            jQuery(this).find('#confirm-name').text(jQuery(e.relatedTarget).data('name'));
            jQuery(this).find('.btn-ok').attr('href', jQuery(e.relatedTarget).data('href'));
        });
        jQuery(document).on("click", "#create", function() {
            jQuery("#edit input:not([type='radio']").val("");
        });
    });
</script>