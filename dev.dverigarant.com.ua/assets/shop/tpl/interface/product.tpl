<link rel="stylesheet" href="/assets/shop/tpl/interface/product.css">
<script src="/assets/shop/tpl/interface/product.js"></script>
<?php if (!isset($product['product_id']) || (int)$product['product_id'] == 0): ?>
<form id="editForm" action="<?=$url?>products&c=save" method="post">
    <h2 class="muted">&emsp;Добавление <small>товара</small></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>products&c=update" method="post">
        <h2 class="muted">&emsp;Редактирование <small>(<?=$product[$modx->config['lang']]['product_name']?>)</small></h2>
<?php endif; ?>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="active menu_item"><a href="#product" role="tab" data-toggle="tab"><b><i class="fa fa-id-card" aria-hidden="true"></i> Основное</b></a></li>
        <?php foreach ($languages as $k => $lng): ?>
            <li role="presentation" class="menu_item"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><i class="fa fa-globe" aria-hidden="true"></i> <?=$lng?></b></a></li>
        <?php endforeach; ?>
        <?php if ((int)$product['product_id'] > 0): ?>

            <li role="presentation" class="menu_item" id="variationTab"><a href="#variation" role="tab" data-toggle="tab"><b><i class="fa fa-cogs" aria-hidden="true"></i> Вариации</b></a></li>
            <?php if (!isset($options) || count($options) < 2): ?>
                <li role="presentation"class="menu_item" id="filterTab"><a href="#parameters" role="tab" data-toggle="tab"><b><i class="fa fa-cogs" aria-hidden="true"></i></i> Параметры</b></a></li>
            <?php endif; ?>
            <li role="presentation" class="menu_item" id="mediaTab"><a href="#media" role="tab" data-toggle="tab"><b><i class="fa fa-picture-o" aria-hidden="true"></i> Изображения</b></a></li>
            <li role="presentation" class="menu_item" id="equipmentTab"><a href="#equipment" role="tab" data-toggle="tab"><b><i class="fa fa-cogs" aria-hidden="true"></i> Комплектация</b></a></li>
            <li role="presentation" class="menu_item" id="aditionalTab"><a href="#aditionalEquipment" role="tab" data-toggle="tab"><b><i class="fa fa-cogs" aria-hidden="true"></i> Дополнительная комплектация</b></a></li>
        <?php endif; ?>
    </ul>
    <br />
    <div class="container-fluid">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="product">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" width="160px">Видимость:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="product[product_active]" <?=($product['product_active'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Да" data-off-text="Нет" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Статус наличия:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="product[product_availability]" <?=($product['product_availability'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="Есть" data-off-text="Нет" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Вид товара:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="product[product_type]" <?=($product['product_type'] ? "checked" : "")?> value="1" data-on-color="warning" data-off-color="primary" data-label-text="&nbsp;" data-on-text="Вариативный" data-off-text="Груповой" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Статус товара:</th>
                        <td>
                            <select class="form-control chosen-select" name="product[product_status]">
                                <option value="0" selected>Выберите статус</option>
                                <!--<option value="1"<?php if(1 == $product['product_status']) echo ' selected'; ?>>Новый</option>-->
                                <option value="2"<?// if(2 == $product['product_status']) echo ' selected'; ?>>Скидка</option>
                                <option value="3"<? if(3 == $product['product_status']) echo ' selected'; ?>>Акция</option>
                                <!--<option value="4"<?php// if(4 == $product['product_status']) echo ' selected'; ?>>Топ</option>-->
                                <option value="5"<?php if(5 == $product['product_status']) echo ' selected'; ?>>Хит</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Ссылка:</th>
                        <td>
                            <div class="input-group">
                                <? if ($modx->config['shop_root_item']): ?>
                                <span class="input-group-addon"><?=$modx->config['site_url']?><?=$modx->config['lang_default']?>/</span>
                                <?php else: ?>
                                <span class="input-group-addon"><?=$modx->makeUrl($modx->config['shop_page_item'], '', '', 'full')?></span>
                                <?php endif; ?>
                                <input type="text" class="form-control" id="basic-url" name="product[product_url]" value="<?=$product['product_url']?>" onchange="documentDirty=true;" />
                                <span class="input-group-btn actionButtons">
                                    <?php if ($modx->config['shop_root_item']): ?>
                                        <a href="<?=$modx->config['site_url']?><?=$modx->config['lang_default']?>/<?=$product['product_url']?>/" target="_blank">
                                    <?php else: ?>
                                            <a href="<?=$modx->makeUrl($modx->config['shop_page_item'], '', '', 'full')?><?=$product['product_url']?>/" target="_blank">
                                    <?php endif; ?>
                                                <i class="fa fa-external-link-square"></i>&emsp;Перейти!
                                    </a>
                                </span>
                            </div>
                            &emsp;<small class="text-info">Оставьте пустым для автоматического заполнения</small>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Артикул:</th>
                        <td>
                            <input type="text" name="product[product_code]" value="<?=$product['product_code']?>" class="form-control" />
                        </td>
                    </tr>
                    <?php if (!isset($options) || count($options) < 2): ?>
                        <?php $option = current($options); ?>
                        <input type="hidden" name="options[id]" value="<?=$option['id']?>">
                        <tr>
                            <th class="vertical-align">Цена:</th>
                            <td>
                                <div class="">
                                    <input type="number" name="options[price]" value="<?=$option['price']?>" class="form-control" data-validate="inputMaxSum:9999999.99" />
                                    <div class="error-text" style="display:none;">Значение суммы в поле <b>"Цена"</b> не должно превышать 9999999.99</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Цена старая:</th>
                            <td>
                                <div class="">
                                    <input type="number" name="options[price_old]" value="<?=$option['price_old']?>" class="form-control" data-validate="inputMaxSum:9999999.99" />
                                    <div class="error-text" style="display:none;">Значение суммы в поле <b>"Цена старая"</b> не должно превышать 9999999.99</div>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th class="vertical-align">Категория <small>(Для хлебных крошек)</small></th>
                        <td>
                            <select class="form-control chosen-select" data-placeholder="Выберите категорию" name="product[product_category]">
                                <option value="<?=$modx->config['shop_page_item']?>">Без категории</option>
                                <?php foreach ($categories as $id => $title): ?>
                                    <option value="<?=$id;?>" <?php if($id == $product['product_category']) echo 'selected'; ?>><?=$title['title'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Категории</th>
                        <td>
                            <select class="form-control chosen-select" data-placeholder="Выберите категорию" name="product[product_categories][]" multiple>
                                <option value=""></option>
                                <?php foreach ($categories as $id => $title): ?>
                                    <option value="<?=$id;?>" <?php if(in_array($id, $product['product_categories'])) echo 'selected'; ?>><?=$title['title'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Похожие товары</th>
                        <td>
                            <select class="form-control chosen-select" data-placeholder="Выберите товары" name="product[product_recommender][]" multiple>
                                <option value=""></option>
                                <?php foreach ($recommenders as $id => $title):?>
                                    <option value="<?=$id;?>" <?php if(in_array($id, $product['product_recommender'])) echo 'selected'; ?>><?=$title;?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Подходящие по стилю и дизайну товары</th>
                        <td>
                            <select class="form-control chosen-select" data-placeholder="Выберите товары" name="product[product_look_like][]" multiple>
                                <option value=""></option>
                                <?php foreach ($recommenders as $id => $title):?>
                                    <option value="<?=$id;?>" <?php if(in_array($id, $product['product_look_like'])) echo 'selected'; ?>><?=$title;?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align">Слайдеры на главной</th>
                        <td>
                            <div>
                                <input type="hidden" name="product[product_main_slider_new]" value="0"/>
                                <label class="checkbox-inline" for="product_main_slider_new">
                                <input type="checkbox" id="product_main_slider_new"
                                       name="product[product_main_slider_new]"
                                       value="1" <?=$product['product_main_slider_new'] == 1 ? 'checked' : '' ?> class="form-control"/>
                                Новые поступления</label>
                            </div>
                            <div>
                                <input type="hidden" name="product[product_main_slider_discont]" value="0"/>
                                <label class="checkbox-inline" for="product_main_slider_discont">
                                <input type="checkbox" id="product_main_slider_discont"
                                       name="product[product_main_slider_discont]"
                                       value="1" <?=$product['product_main_slider_discont'] == 1 ? 'checked' : '' ?> class="form-control"/>
                               Акционные товары</label>
                            </div>
                            <div>
                                <input type="hidden" name="product[product_main_slider_popular]" value="0"/>
                                <label class="checkbox-inline" for="product_main_slider_popular">
                                <input type="checkbox" id="product_main_slider_popular"
                                       name="product[product_main_slider_popular]"
                                       value="1" <?=$product['product_main_slider_popular'] == 1 ? 'checked' : '' ?> class="form-control"/>
                                Самые популярные</label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <?php foreach ($languages as $k => $lng): ?>
                <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" width="160px">Название:</th>
                            <td>
                                <input type="text" name="product[<?=$lng?>][product_name]" value="<?php echo htmlspecialchars($product[$lng]['product_name']);?>" class="form-control" data-validate="textMaxLenght:128" />
                                <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> Название"</b> не должно превышать 128 символов</div>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Аннотация:</th>
                            <td>
                                <textarea id="<?=$lng?>_introtext" name="product[<?=$lng?>][product_introtext]" class="form-control" rows="2"><?=$product[$lng]['product_introtext']?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Описание:</th>
                            <td>
                                <textarea id="<?=$lng?>_description" name="product[<?=$lng?>][product_description]" class="form-control" rows="5"><?=$product[$lng]['product_description']?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Дополнительная информация:</th>
                            <td>
                                <textarea id="<?=$lng?>_additional" name="product[<?=$lng?>][product_additional]" class="form-control" rows="5"><?=$product[$lng]['product_additional']?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align">Контент:</th>
                            <td>
                                <textarea id="<?=$lng?>_content" name="product[<?=$lng?>][product_content]" class="form-control" rows="5"><?=$product[$lng]['product_content']?></textarea>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="vertical-align">SEO title:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="product[<?=$lng?>][product_seo_title]" value="<?=$product[$lng]['product_seo_title']?>" class="form-control" data-validate="textMaxLenght:70" />
                                    <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> SEO title"</b> не должно превышать 70 символов</div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default">SEO description:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="product[<?=$lng?>][product_seo_description]" value="<?=$product[$lng]['product_seo_description']?>" class="form-control" data-validate="textMaxLenght:255" />
                                    <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> SEO description"</b> не должно превышать 255 символов</div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="vertical-align">SEO content:</th>
                            <td>
                                <textarea id="<?=$lng?>_seo_content" name="product[<?=$lng?>][product_seo_content]" class="form-control" rows="5"><?=$product[$lng]['product_seo_content']?></textarea>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default">SEO keywords:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="product[<?=$lng?>][product_seo_keywords]" value="<?=$product[$lng]['product_seo_keywords']?>" class="form-control" data-validate="textMaxLenght:70" />
                                    <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> SEO keywords"</b> не должно превышать 70 символов</div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default">SEO canonical:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="product[<?=$lng?>][product_seo_canonical]" value="<?=$product[$lng]['product_seo_canonical']?>" class="form-control" data-validate="textMaxLenght:255" />
                                    <div class="error-text" style="display:none;">Колличество символов в поле <b>"<?=strtoupper($lng)?> SEO canonical"</b> не должно превышать 255 символов</div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default">SEO robots:</th>
                            <td>
                                <select class="form-control" name="product[<?=$lng;?>][product_seo_robots]" style="width: 300px;">
                                    <option value="index,follow" <?php if($product[$lng]['product_seo_robots'] == 'index,follow') echo 'selected'; ?>>index, follow</option>
                                    <option value="noindex,nofollow" <?php if($product[$lng]['product_seo_robots'] == 'noindex,nofollow') echo 'selected'; ?>>noindex, nofollow</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php endforeach; ?>
            <div role="tabpanel" class="tab-pane" id="variation">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <td>
                            <?php if (!isset($options) || count($options) < 2): ?>
                            <div class="row"><h1 class="screen"><strong>У этого товара нет вариаций</strong></h1></div>
                            <?php else: ?>
                            <table class="table table-bordered table-condensed variations_table">
                                <tbody>
                                <?php foreach ($options as &$option): ?>
                                <tr width="100%">
                                    <th>
                                        <?=htmlspecialchars($option[$modx->config['lang'] . '_name']);?>
                                    </th>
                                    <th>
                                        <?=$option['product_code'];?>
                                    </th>
                                    <th>
                                        <?=number_format($option['price'], 0, '.', ' ')?> грн.
                                    </th>
                                    <th>
                                        <?=number_format($option['price_old'], 0, '.', ' ')?> грн.
                                    </th>
                                    <th>
                                        <?php if($option['default_option'] == 1): ?> <span class="label label-success">По умолчнанию</span> <?php endif; ?>
                                    </th>
                                    <td>
                                        <a href="<?=$url?>variations&c=edit&pid=<?php echo (int)$product['product_id']; ?>&vid=<?php echo (int)$option['id']; ?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;Редактировать</a>
                                        <a href="<?=$url?>variations&c=delete&pid=<?php echo (int)$product['product_id']; ?>&vid=<?php echo (int)$option['id'];?>" class="btn btn-danger js_confirm"><i class="fa fa-trash"></i>&emsp;Удалить</a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right"><a href="<?=$url?>variations&c=add&pid=<?php echo (int)$product['product_id']; ?>" class="btn btn-primary">Добавить вариацию</a></td>
                    </tr>
                </table>
            </div>
            <?php if (!isset($options) || count($options) < 2): ?>
                <div role="tabpanel" class="tab-pane" id="parameters">
                    <?php if (is_array($filters) && count($filters)): ?>
                    <h3><b><span class="label label-primary">Фильтры</span></b></h3>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" style="text-align:center;" width="160px">Название</th>
                            <th class="vertical-align" style="text-align:center;">Значение</th>
                            <th class="vertical-align" style="text-align:center;" width="100px">Тип выбора</th>
                        </tr>
                        <?php foreach($filters as $fid => $filter): ?>
                        <tr>
                            <td><b><?=$filter['filter_name'];?></b></td>
                            <td>
                                <?php if ($filters['filter_type'] == 2): ?>
                                <?php foreach ($languages as $k => $lng): ?>
                                <div class="input-group">
                                    <div class="input-group-addon text-uppercase"><?=$lng?></div>
                                    <input type="text"
                                           name="product[product_filters][<?=$chid?>][0][filter_value_<?=$lng?>]"
                                           value="<?=$characteristic['filter_values'][$pfvs[$chid]]['filter_value_'.$lng]?>"
                                           class="form-control" data-validate="textMaxLenght:64"/>
                                    <div class="error-text" style="display:none;">Колличество символов в поле
                                        <b>"<?=$characteristic['filter_name'];?> <?=$lng?> значение"</b> не должно превышать
                                        64 символа
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <?php if ($filter['filter_type'] == 5): ?>
                                <select class="form-control select2" style="width:100%"
                                        name="product[product_filters][<?=$fid?>]">
                                    <option value="" selected>Выберите значение</option>
                                    <?php if (is_array($filter['filter_values'])): ?>
                                    <?php foreach($filter['filter_values'] as $vid => $value): ?>
                                    <option value="<?=$vid?>"
                                    <?php if(in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <?php endif; ?>
                                <?php if ($filter['filter_type'] == 6): ?>
                                <select class="form-control select2" style="width:100%"
                                        name="product[product_filters][<?=$fid?>][]" multiple>
                                    <?php if (is_array($filter['filter_values'])): ?>
                                    <?php foreach($filter['filter_values'] as $vid => $value):?>
                                    <option value="<?=$vid?>"
                                    <?php if((is_array($pfvs[$fid]) && in_array($vid, $pfvs[$fid])) || in_array($vid, $pfvs)) echo 'selected'; ?>
                                    ><?=$value?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if($filter['filter_type'] == 1):?><span
                                        class="label label-default">Числовой</span><?php endif;?>
                                <?php if($filter['filter_type'] == 2):?><span
                                        class="label label-default">Текстовый</span><?php endif;?>
                                <?php if($filter['filter_type'] == 3):?><span
                                        class="label label-default">ИЛИ</span><?php endif;?>
                                <?php if($filter['filter_type'] == 4):?><span
                                        class="label label-default">И</span><?php endif;?>
                                <?php if($filter['filter_type'] == 5):?><span
                                        class="label label-default">Моно выбор</span><?php endif;?>
                                <?php if($filter['filter_type'] == 6):?><span
                                        class="label label-default">Мульти выбор</span><?php endif;?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <br/>
                    <?php endif; ?>
                    <?php if (is_array($characteristics) && count($characteristics)): ?>
                    <h3><b><span class="label label-info">Характеристики</span></b></h3>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" style="text-align:center;" width="160px">Название</th>
                            <th class="vertical-align" style="text-align:center;">Значение</th>
                        </tr>
                        <?php foreach($characteristics as $chid => $characteristic): ?>
                        <tr>
                            <td><b><?=$characteristic['filter_name'];?></b></td>
                            <td>
                                <?php if ($characteristic['filter_type'] == 2): ?>
                                <?php foreach ($languages as $k => $lng): ?>
                                <div class="input-group">
                                    <div class="input-group-addon text-uppercase"><?=$lng?></div>
                                    <input type="text"
                                           name="product[product_filters][<?=$chid?>][0][filter_value_<?=$lng?>]"
                                           value="<?=$characteristic['filter_values'][$pfvs[$chid]]['filter_value_'.$lng]?>"
                                           class="form-control" data-validate="textMaxLenght:64"/>
                                    <div class="error-text" style="display:none;">Колличество символов в поле
                                        <b>"<?=$characteristic['filter_name'];?> <?=$lng?> значение"</b> не должно превышать
                                        64 символа
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <?php if ($characteristic['filter_type'] == 5): ?>
                                <select class="form-control select2" style="width:100%"
                                        name="product[product_filters][<?=$chid?>]">
                                    <option value="" selected>Выберите значение</option>
                                    <?php if (is_array($characteristic['filter_values'])): ?>
                                    <?php foreach($characteristic['filter_values'] as $vid => $value): ?>
                                    <option value="<?=$vid?>"
                                    <?php if(in_array($vid, $pfvs)) echo 'selected'; ?>><?=$value?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <?php endif; ?>
                                <?php if ($characteristic['filter_type'] == 6): ?>
                                <select class="form-control select2" style="width:100%"
                                        name="product[product_filters][<?=$chid?>][]" multiple>
                                    <?php if (is_array($characteristic['filter_values'])): ?>
                                    <?php foreach($characteristic['filter_values'] as $vid => $value):?>
                                    <option value="<?=$vid?>"
                                    <?php if((is_array($pfvs[$chid]) && in_array($vid, $pfvs[$chid])) || in_array($vid, $pfvs)) echo 'selected'; ?>
                                    ><?=$value?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
                <div role="tabpanel" class="tab-pane" id="equipmentValues">
                    <div>
                        <div id="equipmentValuesLabel"></div>
                        <div id="upBTN" class="btn btn-primary" equipment-id="<?php echo $equip_item['id']?>" >Перейти на уровень вверх</div>
                    </div>
                    <table class="table table-bordered table-condensed" id="equipmentValuesList">
                        <tr id="newEqpValue">
                            <td><input id = "name"  type="text" class="newItem" placeholder="наименование"></td>
                            <td class="parameter"><input id = "sort"  onkeyup="validSymbol(event);" type="text" class="newItem" placeholder="порядок сортировки"></td>
                            <td class="parameter"><input id = "price" onkeyup="validSymbol(event);" type="text" class="newItem" placeholder="цена"></td>
                            <td><div class="btn btn-primary" id="addEquipmentValueItem">Добавить</div></td>
                        </tr>
                        <?php foreach($equipmentValues as $equip_item):?>
                        <tr eqp_id = <?php echo $equip_item['eqp_id']?>>
                            <td><?php echo $equip_item['name']?></td>
                            <td class="parameter"><?php echo $equip_item['sort']?></td>
                            <td class="parameter"><?php echo $equip_item['price']?></td>
                            <td>
                                <div class="btn btn-success editEqp" action-parameter = "updateValueEquipment" id="<?php echo $equip_item['id']?>"><i class="fa fa-pencil"></i>&nbsp;Редактировать</div>
                                <div class="btn btn-danger delEqp" action-parameter = "deleteValueEquipment" id="<?php echo $equip_item['id']?>"><i class="fa fa-trash"></i>&nbsp;Удалить</div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="equipment">
                    <table class="table table-bordered table-condensed" id="equipmentList">
                        <tr id="newEqpItem">
                            <td><input id = "name"  type="text" class="newItem" placeholder="наименование"></td>
                            <td class="parameter"><input id = "sort"  type="text" class="newItem" placeholder="порядок сортировки"></td>
                            <td><div class="btn btn-primary" id="addEquipmentItem">Добавить</div></td>
                        </tr>

                        <?php foreach($equipment as $equip_item):?>
                        <tr>
                            <td><?php echo $equip_item['name']?></td>
                            <td class="parameter"><?php echo $equip_item['sort']?></td>
                            <td>
                                <div class="btn btn-primary showEquipmentList" equipment-label="<?php echo $equip_item['name']?>" id="<?php echo $equip_item['id']?>" >Возможные значения</div>
                                <div class="btn btn-success editEqp"  action-parameter = "updateEquipment" id="<?php echo $equip_item['id']?>"><i class="fa fa-pencil"></i>&nbsp;Редактировать</div>
                                <div class="btn btn-danger delEqp"  action-parameter = "deleteEquipment" id="<?php echo $equip_item['id']?>"><i class="fa fa-trash"></i>&nbsp;Удалить</div>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                </div>

                <div role="tabpanel" class="tab-pane" id="aditionalEquipment">
                    <table class="table table-bordered table-condensed" id="additionalEquipmentList">
                        <tr id="newItem">
                            <td><input id = "name"  type="text" class="newItem" placeholder="наименование"></td>
                            <td class="parameter"><input id = "sort"  type="text" class="newItem" placeholder="порядок сортировки"></td>
                            <td class="parameter"><input id = "price" type="text" class="newItem" placeholder="цена"></td>
                            <td>Не активна <input id = "disable" type="checkbox" class="newItem"></td>
                            <td>По умолчанию <input id = "default" type="checkbox" class="newItem"></td>
                            <td><div class="btn btn-primary" id="addItem">Добавить</div></td>
                        </tr>
                        <?php foreach($addEqup as $equipment):?>
                            <tr>
                                <td><?php echo $equipment['name']?></td>
                                <td class="parameter"><?php echo $equipment['sort']?></td>
                                <td class="parameter"><?php echo $equipment['price']?></td>
                                <td><input type="checkbox" class="checkbox" disabled <?php echo ($equipment['disable']?'checked="checked"':'')?>></td>
                                <td><input type="checkbox" class="checkbox" disabled <?php echo ($equipment['default']?'checked="checked"':'')?>></td>
                                <td>
                                    <div class="btn btn-success editAddEqp" record-id="<?php echo $equipment['id']?>"><i class="fa fa-pencil"></i>&nbsp;Редактировать</div>
                                    <div class="btn btn-danger delAddEqp" record-id="<?php echo $equipment['id']?>"><i class="fa fa-trash"></i>&nbsp;Удалить</div>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="media">
                    <!--<p>Внимание! Для корректного отображения изображений товара, картинки должны иметь соотношение сторон <b style="color:red;">ширина:высота</b> как <b style="color:red;">1:1.4</b>. Идеальный размер <b style="color:red;">800:1120 пикс</b>.</p>-->
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" style="text-align:center;" width="160px">
                                <a href="#" id="cover" class="btn btn-primary"><i class="fa fa-camera-retro"></i>&emsp;Превью</a>
                            </th>
                            <td>
                                <ul class="cover"></ul>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align" style="text-align:center;"">
                                <a href="#" id="second" class="btn btn-primary"><i class="fa fa-bullseye"></i>&emsp;Превью ховер</a>
                            </th>
                            <td>
                                <ul class="second"></ul>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align" style="text-align:center;">
                                <a href="#" id="slider" class="btn btn-primary"><i class="fa fa-slideshare"></i>&emsp;Слайдер</a>
                            </th>
                            <td>
                                <ul class="slider"></ul>
                            </td>
                        </tr>
                        <!--<tr>
                            <th class="vertical-align" style="text-align:center;">
                                <a href="#" id="product_tags" class="btn btn-primary"><i class="fa fa-slideshare"></i>&emsp;Теги товара</a>
                            </th>
                            <td>
                                <ul class="product_tags"></ul>
                            </td>
                        </tr>-->
                    </table>
                </div>
            </div>
        </div>
        <input type="hidden" name="product[product_id]" value="<?=$product['product_id']?>" />
        <input type="hidden" name="product[option_id]" value="<?=$product['option_id']?>" />
        <div id="actions">
            <ul class="actionButtons">
                <li id="Button1" class="transition">
                    <a href="#" class="primary" onclick="documentDirty=false;saveForm('#editForm');"><i class="fa fa-save"></i>&emsp;Сохранить</a>
                </li>
                <?php if ((int)$product['product_id'] > 0): ?>
                <li id="Button3" class="transition">
                    <a href="<?=$url?>products&c=copy&i=<?=$product['product_id']?>"><i class="fa fa-files-o"></i>&emsp;Скопировать</a>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </form>
    <div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form id="translates" action="<?=$url?>&b=save_image_fields" method="post">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Редактирование полей изображения</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Отменить</button>
                        <button type="submit" class="btn btn-primary pull-right">Сохранить <span class="glyphicon glyphicon-ok-sign"></span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php if ((int)$product['product_id'] > 0): ?>
    <script src="/assets/site/ajaxupload.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.select2').select2();
        });
        jQuery(document).on("click", "#mediaTab", function() {
            getCover();
            getSecond();
            getSlider();
            getProductTags();
            jQuery(".slider").sortable();
        });

        //Перейти на уровень вверх
        jQuery(document).on("click", "#upBTN", function (e) {
            let element = $(this);
            while(element.hasClass('tab-pane') != true){
                element = element.parentNode;
            }
            let content_box = $(element.parentNode);
            let tab_panel   = $(element);
            let btn = $(this);
            tab_panel.removeClass('active');
            jQuery.each(content_box.getElementsByTagName('div'), function (key, item) {
                if(item != null && item.id == 'equipment') {
                    item.addClass('active');
                }
            });
        });

        //Открываю список значений параметра комплектации
        jQuery(document).on("click", ".showEquipmentList", function() {
            let element = $(this);
            while(element.hasClass('tab-pane') != true){
                element = element.parentNode;
            }
            let content_box = $(element.parentNode);
            let tab_panel   = $(element);
            let btn = $(this);
            tab_panel.removeClass('active');
            jQuery.each(content_box.getElementsByTagName('div'), function (key, item) {
                if(item != null && item.id == 'equipmentValues') {
                    item.addClass('active');
                    jQuery.each(item.getElementsByTagName('div'), function (key, value) {
                        if (value != null) {
                            if (value.id == 'equipmentValuesLabel') {
                                value.innerHTML = btn.getAttribute('equipment-label');
                            } else if (value.id == 'addEquipmentValueItem') {
                                value.setAttribute('equipment-id', btn.getAttribute('id'));
                            }
                        }
                    });
                    let trList = jQuery('#equipmentValuesList').find('tr');
                    jQuery.each(trList, function (key, trItem) {
                        if(jQuery(trItem).attr('eqp_id') == btn.getAttribute('id') || jQuery(trItem).attr('id') == 'newEqpValue') {
                            jQuery(trItem).show();
                        }else {
                            jQuery(trItem).hide();
                        }
                    });
                }
            });
//            jQuery.ajax({
//                    url: "<?=$url?>showEquipmentList",
//                    eqp_id: jQuery(element.target).attr('equipment-id'),
//                    type:'post',
//                    success: function (result) {
//
//                    }
//            })
        });

        //Добавление записи дополнительной комплектации
        jQuery(document).on("click", "#addItem", function() {
            let tdList = $(this).parentElement.parentElement.getElementsByTagName('input');
            let param = {product_id:<?php echo (int)$product['product_id']; ?>};
            let saveParams = true;
            let html = '<tr>'
            for (let i = 0; i < tdList.length; i++) {
                let item = tdList[i];
                if((item.getAttribute('id') == 'name' || item.getAttribute('id') == 'price') && item.value.length == 0) {
                    alert('Не заполнены имя или цена дополнительной комплектации');
                    saveParams = false;
                }
                if(jQuery(item).attr('type') == 'checkbox') {
                    param[item.getAttribute('id')] = item.checked ? 'on' : 'off';
                    html += '<td><input class="checkbox" type="checkbox" '+(item.checked ?'checked="checked"':'')+'></td>';
                }else {
                    param[item.getAttribute('id')] = item.value;
                    html += '<td>' + item.value + '</td>';
                }
            }
            html += '<td></td></tr>';
            if(saveParams) {
                console.log(param);
                jQuery.ajax({
                    url: "<?=$url?>addAditionalEquipment",
                    data: param,
                    type:'post',
                    success: function (result) {
                        jQuery('#newItem').after(html);
                        console.log(result);
                    }
                })
            }
        });

        //Добавление записи значение параметра комплектации
        jQuery(document).on("click", "#addEquipmentValueItem", function() {
            let inputList = $(this).parentElement.parentElement.getElementsByTagName('input');
            let param = {
                eqp_id:$(this).getAttribute('equipment-id'),
                name  :$(this).getAttribute('name'),
                sort  :$(this).getAttribute('sort'),
            };
            let saveParams = true;
            let html = '<tr>'
            for (let i = 0; i < inputList.length; i++) {
                let item = inputList[i];
                if(saveParams == true && (item.getAttribute('id') == 'name' || item.getAttribute('id') == 'price') && item.value.length == 0) {
                    alert('Не заполнены имя или цена значения комплектации');
                    saveParams = false;
                }
                param[item.getAttribute('id')] = item.value;
                html += '<td>' + item.value + '</td>';

            }
            html += '<td><div class="btn btn-primary" id="addEquipmentList" equipment-id="id-item" >Добавить значения</div></td></tr>';
            if(saveParams) {
                console.log(param);
                jQuery.ajax({
                    url: "<?=$url?>addEquipmentValueItem",
                    data: param,
                    type:'post',
                    success: function (result) {
                        html = html.replace('id-item', result);
                        jQuery('#newItem').after(html);
                        console.log(result);
                    }
                })
            }
        });

        //Редактирование записи значения параметра комплектации


        //Добавление записи комплектации
        jQuery(document).on("click", "#addEquipmentItem", function() {

            let tdList = $(this).parentElement.parentElement.getElementsByTagName('input');
            let param = {product_id:<?php echo (int)$product['product_id']; ?>};
            let saveParams = true;
            let html = '<tr>'
            for (let i = 0; i < tdList.length; i++) {
                let item = tdList[i];
                if((item.getAttribute('id') == 'name' || item.getAttribute('id') == 'price') && item.value.length == 0) {
                    alert('Не заполнены имя или цена комплектации');
                    saveParams = false;
                }
                param[item.getAttribute('id')] = item.value;
                html += '<td>' + item.value + '</td>';

            }
            html += '<td><div class="btn btn-primary showEquipmentList" equipment-label="'+param['name']+'" id="id-item">Возможные значения</div>'+
            '<div class="btn btn-success editEqp"  action-parameter = "updateEquipment" id="id-item"><i class="fa fa-pencil"></i>&nbsp;Редактировать</div>'+
            '<div class="btn btn-danger delEqp"  action-parameter = "deleteEquipment" id="id-item"><i class="fa fa-trash"></i>&nbsp;Удалить</div></td></tr>';
            if(saveParams) {
                console.log(param);
                jQuery.ajax({
                    url: "<?=$url?>addEquipment",
                    data: param,
                    type:'post',
                    success: function (result) {
                        html = html.split('id-item').join(result);
                        jQuery('#newEqpItem').after(html);
                        console.log(result);
                    }
                })
            }
        });

        //Удаление записи дополнительной комплектации
        jQuery(document).on("click", ".delAddEqp", function (e) {

            if(confirm('Удалить запись?')) {
                $($(this).parentElement.parentElement).remove();
                let param = {id:$(this).getAttribute('record-id')};
                jQuery.ajax({
                    url:"<?=$url?>deleteAditionalEquipment",
                    type:'post',
                    data:param,
                    success:function (result) {
                        console.log(result);
                        alert('Удаление успешно выполнено');
                    }
                });
            }
        });

        //Удаление записи дополнительной комплектации
        jQuery(document).on("click", ".delEqp", function (e) {
            if(confirm('Удалить запись?')) {
                $($(this).parentElement.parentElement).remove();
                let param = {id:$(this).getAttribute('id')};
                jQuery.ajax({
                    url:"<?=$url?>"+$(this).getAttribute('action-parameter'),
                    type:'post',
                    data:param,
                    success:function (result) {
                        alert('Удаление успешно выполнено');
                    }
                });
            }
        });

        /*Редактирование записи дополнительной комплектации*/
        jQuery(document).on("click", ".editEqp", function (e) {
            if($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-warning');
                $(this).innerHTML = '<i class="fa fa-floppy-o"></i>&nbsp;Сохранить';
                let tdList = $(this).parentElement.parentElement.getElementsByTagName('td');
                for (let i = 0; i < tdList.length - 1; i++) {
                    let item = tdList[i];
                    let item_id;
                    switch (i) {
                        case 0: {
                            item_id = 'edit_name';
                        }
                            break;
                        case 1: {
                            item_id = 'edit_sort';
                        }
                            break;
                        case 2: {
                            item_id = 'edit_price';
                        }
                            break;
                    }
                    if(jQuery(item).find('input').length == 0) {
                        item.innerHTML = '<input type="text" id="' + item_id + '" value = ' + item.innerHTML + '>';
                    }else {
                        jQuery(item).find('input')[0].removeAttribute("disabled");
                        jQuery(jQuery(item).find('input')[0]).attr('id', item_id);
                    }
                }
            }else {
                let tdList = $(this).parentElement.parentElement.getElementsByTagName('input');
                let param = {id:$(this).getAttribute('id')};
                for (let i = 0; i < tdList.length; i) {
                    let item = tdList[i];
                    param[item.getAttribute('id')] = item.value;
                    item.parentElement.innerHTML = item.value;
                }

                let btn = jQuery(this);
                jQuery.ajax({
                    url:"<?=$url?>"+$(this).getAttribute('action-parameter'),
                    type:'post',
                    data:param,
                    success:function (result) {
                        console.log(result);
                        jQuery(btn).html('<i class="fa fa-pencil"></i>&nbsp;Редактировать');
                        btn.removeClass('btn-warning');
                        btn.addClass('btn-success');

                    }
                })
            }
        });

        /*Редактирование записи дополнительной комплектации*/
        jQuery(document).on("click", ".editAddEqp", function (e) {
            if($(this).hasClass('btn-success')) {
                $(this).removeClass('btn-success');
                $(this).addClass('btn-warning');
                $(this).innerHTML = '<i class="fa fa-floppy-o"></i>&nbsp;Сохранить';
                let tdList = $(this).parentElement.parentElement.getElementsByTagName('td');
                for (let i = 0; i < tdList.length - 1; i++) {
                    let item = tdList[i];
                    let item_id;
                    switch (i) {
                        case 0: {
                            item_id = 'edit_name';
                        }
                            break;
                        case 1: {
                            item_id = 'edit_sort';
                        }
                            break;
                        case 2: {
                            item_id = 'edit_price';
                        }
                            break;
                        case 3: {
                            item_id = 'edit_disable';
                        }
                            break;
                        case 4: {
                            item_id = 'edit_default';
                        }
                            break;
                    }
                    if(jQuery(item).find('input').length == 0) {
                        item.innerHTML = '<input type="text" id="' + item_id + '" value = ' + item.innerHTML + '>';
                    }else {
                        jQuery(item).find('input')[0].removeAttribute("disabled");
                        jQuery(jQuery(item).find('input')[0]).attr('id', item_id);
                    }
                }
            }else {
                let tdList = $(this).parentElement.parentElement.getElementsByTagName('input');
                let param = {id:$(this).getAttribute('record-id')};
                for (let i = 0; i < tdList.length; i) {
                    let item = tdList[i];
                    param[item.getAttribute('id')] = item.value;
                    if(jQuery(item).attr('type') == 'checkbox') {
                        i++;
                        param[item.getAttribute('id')] = item.checked ? 'on' : 'off';
                        jQuery(item).attr('disabled', 'disabled');
                        jQuery(item).removeAttr('id');
                    }else {
                        item.parentElement.innerHTML = item.value;
                    }
                }
                console.log(param);
                let btn = jQuery(this);
                jQuery.ajax({
                    url:"<?=$url?>updateAditionalEquipment",
                    type:'post',
                    data:param,
                    success:function (result) {
                        console.log(result, btn);
                        jQuery(btn).html('<i class="fa fa-pencil"></i>&nbsp;Редактировать');
                        btn.removeClass('btn-warning');
                        btn.addClass('btn-success');

                    }
                })
            }
        });

        jQuery(document).on("click", ".js_delete", function() {
            t = jQuery(this).prevAll("img");
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=delete_image&folder=<?=$product['product_id']?>&delete="+t.attr("alt"),success:function(ajax){
            t.parents("li").fadeOut().remove();
            }});
        });

        jQuery(document).on("click", '#Button1', function() {
            jQuery.ajax({url:"<?=$url?>images&c=set_sort",type:"POST",data:jQuery('.slider input').serialize()});
            return true;
        });

        jQuery(document).on("click", "[data-image-edit]", function(){
            var _this = jQuery(this);
            jQuery.ajax({url:"<?=$url?>images&c=edit_image&file="+_this.attr("data-image-edit")+"&product_id=<?=$product['product_id']?>",cache:false,success:function(ajax){
            jQuery("#translate .modal-body").html(ajax);
            jQuery('#translate').modal('show');
            }});
        return false;
        });

        jQuery(document).on("submit", "#translates", function(){
            jQuery.ajax({ url:'<?=$url?>images&c=set_image_fields',type:"POST",data:jQuery('#translates').serialize(),cache:false,success:function(ajax){
                if (ajax == 'Ok') {
                    jQuery('#translate').modal('hide');
                    alert('Изменения сохранены');
                }
            }});
            return false;
        });

        function getCover() {
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_cover&folder=<?=$product['product_id']?>",success:function(ajax){
            jQuery(".cover").html(ajax);
            }});
        }

        function getSecond() {
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_second&folder=<?=$product['product_id']?>",success:function(ajax){
            jQuery(".second").html(ajax);
            }});
        }

        function getSlider(){
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_slider&folder=<?=$product['product_id']?>", success:function(ajax){
            jQuery(".slider").html(ajax);
            }});
        }

        function getProductTags(){
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_product_tags&folder=<?=$product['product_id']?>", success:function(ajax){
            jQuery(".product_tags").html(ajax);
            }});
        }

        jQuery(document).ready(function(){
            new AjaxUpload(jQuery("#cover"), {
                action: "<?=$url?>images&c=set_cover",
                multiple: false,
                name: "uploader[]",
                data: {
                    "size"  : 2048576,
                    "folder": '<?=$product["product_id"]?>'
                },
                onSubmit: function(file, ext){
                    if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                        alert('<?=$_lang["shop_file_format"]?>');
                        return false;
                    }
                },
                onComplete: function(file, response) {
                    if (response.length > 3) {
                        JSON.parse(response, function(k, v) {
                            if (k != "") {
                                alert("<?=$_lang['image_image']?> "+k+" "+v);
                            }
                        });
                    }
                    getCover();
                }
            });

            new AjaxUpload(jQuery("#second"), {
                action: "<?=$url?>images&c=set_second",
                multiple: false,
                name: "uploader[]",
                data: {
                    "size"  : 2048576,
                    "folder": '<?=$product["product_id"]?>'
                },
                onSubmit: function(file, ext){
                    if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                        alert('<?=$_lang["shop_file_format"]?>');
                        return false;
                    }
                },
                onComplete: function(file, response){
                    getSecond();
                }
            });

            new AjaxUpload(jQuery("#slider"), {
                action: "<?=$url?>images&c=set_slider",
                multiple: true,
                name: "uploader[]",
                data: {
                    "size"  : 2048576,
                    "folder": '<?=$product["product_id"]?>'
                },
                onSubmit: function(file, ext){
                    if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                        alert('<?=$_lang["shop_file_format"]?>');
                        return false;
                    }
                },
                onComplete: function(file, response){
                    getSlider();
                }
            });

            new AjaxUpload(jQuery("#product_tags"), {
                action: "<?=$url?>images&c=set_product_tags",
                multiple: true,
                name: "uploader[]",
                data: {
                    "size"  : 2048576,
                    "folder": '<?=$product["product_id"]?>'
                },
                onSubmit: function(file, ext){
                    if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                        alert('<?=$_lang["shop_file_format"]?>');
                        return false;
                    }
                },
                onComplete: function(file, response){
                    getProductTags();
                }
            });
        });
    </script>
    <?php endif; ?>
    <?=$tiny_mce?>