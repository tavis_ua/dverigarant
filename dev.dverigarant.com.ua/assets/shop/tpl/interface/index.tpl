<div class="notifier">
    <div class="notifier-txt"></div>
</div>
<h1 class="pagetitle">
    <span class="pagetitle-icon"><i class="fa fa-registered"></i></span>
    <span class="pagetitle-text">Управление магазином</span>
</h1>
<div class="sectionBody">
    <div class="dynamic-tab-pane-control tab-pane">
        <div class="tab-row">
            <h2 class="tab<?=($bootstrap=='dashboard'?' selected':'');?>"><a href="<?=$url?>dashboard"><span><i class="fa fa-tachometer"></i> Dashboard</span></a></h2>
            <h2 class="tab<?=($bootstrap=='orders'?' selected':'');?>"><a href="<?=$url?>orders"><span><i class="fa fa-shopping-cart"></i> Заказы</span></a></h2>
            <h2 class="tab<?=($bootstrap=='products'?' selected':'');?>"><a href="<?=$url?>products"><span><i class="fa fa-cubes"></i> Товары</span></a></h2>
            <h2 class="tab<?=($bootstrap=='filters'?' selected':'');?>"><a href="<?=$url?>filters"><span><i class="fa fa-filter"></i> Фильтры</span></a></h2>
            <!--<h2 class="tab<?=($bootstrap=='promocodes'?' selected':'');?>"><a href="<?=$url?>promocodes"><span><i class="fa fa-qrcode"></i> Промо-коды</span></a></h2>
            <h2 class="tab<?=($bootstrap=='reviews'?' selected':'');?>"><a href="<?=$url?>reviews"><span><i class="fa fa-comments"></i> Отзывы</span></a></h2>-->
            <h2 class="tab<?=($bootstrap=='faqs'?' selected':'');?>"><a href="<?=$url?>faqs"><span><i class="fa fa-graduation-cap"></i> Заданные вопросы</span></a></h2>
            <h2 class="tab<?=($bootstrap=='meters'?' selected':'');?>"><a href="<?=$url?>meters"><span><i class="fa fa-graduation-cap"></i> Вызов замерщика</span></a></h2>
            <h2 class="tab<?=($bootstrap=='imports'?' selected':'');?>"><a href="<?=$url?>imports"><span><i class="fa fa-folder-open"></i> Импорт/Экспорт</span></a></h2>
        </div>
        <div class="tab-page" id="tabContents">
            <?=$res['content']?>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.title = "<?=$res['title']?> <?=$res['version']?>";
    jQuery(document).ready(function() {
        jQuery(".bsw").bootstrapSwitch();
        jQuery(".chosen-select").chosen({width:"100%"});
    });
    // Валидация и сохранение формы
    function saveForm(selector) {
        var errors    = 0;
        var messages  = "";
        var validates = jQuery(selector+" [data-validate]");
        validates.each(function(k, v) {
            var rule = jQuery(v).attr("data-validate").split(":");
            switch (rule[0]) {
                case "inputMaxSum":
                    if (parseFloat(jQuery(v).val()) > parseFloat(rule[1])) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
                case "inputMinSum":
                    if (parseInt(jQuery(v).val()) < parseInt(rule[1])) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
                case "textMaxLenght":
                    if (jQuery(v).val().length > parseInt(rule[1])) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
            }
        });
        if (errors == 0) {
            jQuery(selector).submit();
        } else {
            jQuery('.notifier').addClass("notifier-error");
            jQuery('.notifier').fadeIn(700);
            jQuery('.notifier').find('.notifier-txt').html(messages);
            setTimeout(function() {
                jQuery('.notifier').fadeOut(7000);
            },2000);
            setTimeout(function() {
                jQuery('.notifier').removeClass("notifier-error");
            },7000);
        }
    }
</script>
<?php if (isset($_SESSION['notifier_status']) && isset($_SESSION['notifier_text'])) :?>
<script type="text/javascript">
    jQuery('.notifier').addClass("notifier-<?=$_SESSION['notifier_status']?>");
    jQuery('.notifier').fadeIn(700);
    jQuery('.notifier').find('.notifier-txt').html("<?=$_SESSION['notifier_text']?>");
    setTimeout(function() {
        jQuery('.notifier').fadeOut(7000);
    },2000);
    setTimeout(function() {
        jQuery('.notifier').removeClass("notifier-<?=$_SESSION['notifier_status']?>");
    },7000);
</script>
<?php unset($_SESSION['notifier_status']); ?>
<?php unset($_SESSION['notifier_text']); ?>
<?php endif; ?>