<?php
/**
 * Обработчик AJAX запросов
 */
$ajax['status'] = 0;
switch (rtrim($_REQUEST['ajax'], '/')) {
    // Форма перезвоните мне
    case "call_me":
        if (is_array($_POST['call_me']) && count($_POST['call_me']) > 0 && $_SESSION['hash']['call_me'] != md5(serialize($_POST['call_me']))) {
            $data = $_POST['call_me'];
            $shop->sendMail($modx->config['emailsender'], "call_me", $data);
//            $_SESSION['js'] = "$.fancybox.open({src: '#call_me-send'});"; // Ваш вопрос успешно задан
            $ajax['modal'] = 'call_me-send';
            $ajax['status'] = 1;
        }
        break;
    // Форма вызвать замерщика
    case "call_meter":
        if (is_array($_POST['meter']) && count($_POST['meter']) > 0 && $_SESSION['hash']['meter'] != md5(serialize($_POST['meter']))) {
            $data  = $_POST['meter'];
            $modx->db->query("
            INSERT INTO `modx_a_meters` SET 
                meter_active   = 0,
                meter_category = 0,
                meter_name     = '".$modx->db->escape($data['name'])."',
                meter_phone    = '".$modx->db->escape($data['phone'])."'
         ");
            $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
            $modx->db->query("
            INSERT INTO `modx_a_meter_strings` SET 
                string_locate = '{$modx->config['lang']}',
                meter_id        = '{$result['last_id']}',
                meter_question  = '".$modx->db->escape($data['area'])."'
        ");
            $shop->sendMail($modx->config['emailsender'], "meter", $data);
//            $_SESSION['js'] = "$.fancybox.open({src: '#request-send'});"; // Ваш вопрос успешно задан
            $ajax['modal'] = 'request-send';
            $ajax['status'] = 1;
        }
        break;
    //Получить доступные вариации товара в зависимости от выбираемых фильтров (карточка товара)
    case "change_option" :
    $product_parent = (int)$_POST['parent_id'];
        //Запись в массив выбранных фильтров пользователем
        $filters = $_POST['product'];
        if (is_array($filters) && count($filters) > 0) {
            foreach ($filters as $key => $filter) {
                $whereValue[] = "pfv.product_id IN (
                    SELECT product_id
                    FROM `modx_a_product_filter_values`
                    WHERE
                        filter_id = (
                            SELECT filter_id FROM `modx_a_filters` WHERE filter_url = '" . $modx->db->escape($key) . "'
                        ) AND
                        value_id = (
                            SELECT value_id FROM `modx_a_filter_values` WHERE filter_url = '" . $modx->db->escape($filter) . "'
                        )
                )";
            }
            $where = "AND " . implode(" AND ", $whereValue);
        } else {
            $where = '';
        }
        if ($product_parent > 0) {
            //Получить id-ки вариаций товара
            $child_products = $modx->db->getColumn('id', "SELECT `id` FROM `modx_a_product_options` WHERE `product` = {$product_parent}");
            $child_products = (is_array($child_products) && count($child_products) > 0) ? implode(',', $child_products) : false;

            if (isset($child_products) && $child_products != '') {
                $product_option_id = intval($modx->db->getValue($modx->db->query("
                    SELECT DISTINCT pfv.`product_id` AS `product_id`
                    FROM `modx_a_product_filter_values` `pfv`
                    WHERE `pfv`.`product_id` IN ({$child_products}) {$where}
                    ORDER BY `pfv`.`product_id`
                ")));
            }

            if (isset($product_option_id) && $product_option_id > 0) {
                $res_product = $modx->db->getRow($modx->db->query("
                    SELECT `product_code`, `price`, `price_old`, `id`
                    FROM `modx_a_product_options`
                    WHERE `id` = '{$product_option_id}' AND `active` = '1'
                "));
            } else {
                $res_product = $modx->db->getRow($modx->db->query("
                    SELECT `product_code`, `price`, `price_old`, `id`
                    FROM `modx_a_product_options`
                    WHERE `product` = '{$product_parent}' AND `default_option` = 1 AND `active` = '1' 
                "));
            }
                $ajax['price'] = $res_product['price'] * $_POST['page_quantity'];
                $ajax['price_old'] = $res_product['price_old'];
                $ajax['id'] = $res_product['id'];
                $ajax['product_code'] = $res_product['product_code'];
                $ajax['status'] = 1;


        }
        break;

    // Покупка в 1 клик
    case "buy_click" :
        if (is_array($_POST['buy-click']) && count($_POST['buy-click']) > 0) {
            $buy_click = $_POST['buy-click'];
            $pid = (int)$buy_click['product'];
            $count = (int)$buy_click['product_count'];
            if($pid > 0) {
                $product = $modx->db->getRow($modx->db->query("
                    SELECT
                        p.`product_id`,
                        p.`product_code`,
                        p.`product_price`,
                        p.`product_url`,
                        p.`product_discont`,
                        (SELECT `product_name` FROM `modx_a_product_strings` WHERE `product_id` = p.`product_id` AND `string_locate` = '{$modx->config['lang']}') AS 'product_name',
                        (SELECT `image_file` FROM `modx_a_product_images` WHERE `image_product_id` = p.`product_id` AND `image_use` = 'cover') AS 'product_cover'
                    FROM `modx_a_products` p
                    WHERE p.`product_id` = '{$pid}'
                "));
            }
            if ($modx->config['shop_root_item']) {
                $product['product_url'] = $modx->config['site_url'].$modx->config['lang'].'/'.$product['product_url'].'/';
            } else {
                $product['product_url'] = $modx->makeUrl($modx->config['shop_page_item']).$product['product_url'].'/';
            }
            $product['product_price'] = ceil($product['product_price']);
            $product['sign'] = $_SESSION['sign'];
            $product['product_cover'] = '/assets/products/'.$product['product_id'].'/'.$product['product_cover'];
            $product['product_count'] = $count;

            //Формируем запись товара для письма админу
            $prod_res = $modx->parseDocumentSource($modx->parseChunk('mail_order_item', $product));
            
            //Информация о пользователе для заказа
            $checkout['user']['fullname'] = $modx->db->escape($buy_click['name']);
            $checkout['user']['phone'] = $modx->db->escape($buy_click['phone']);
            $checkout['user']['email'] = $modx->db->escape($buy_click['email']);

            //Пишем данные о заказе
            $modx->db->query("
                INSERT INTO `modx_a_order` SET
                    `order_created`       = '".date("Y-m-d H:i:s")."',
                    `order_client_id`     = '".(int)$_SESSION['webuser']['internalKey']."',
                    `order_client`        = '".$modx->db->escape(json_encode($checkout['user']))."',
                    `order_status`        = '1',
                    `order_cost`          = '".$product['product_price'] * $count."',
                    `order_lang`          = '".$modx->config['lang']."',
                    `order_currency`      = '".$_SESSION['currency']."',
                    `order_buy_click`     = '1'
            ");
            $order_id = $modx->db->getInsertId();

            $modx->db->query("INSERT INTO `modx_a_order_products` SET
                    `order_id`      = '{$order_id}',
                    `product_id`    = '{$pid}',
                    `product_count` = '{$count}',
                    `product_price` = '{$product['product_price']}'
            ");

            //Все данные для письма
            $data['order_id'] = $order_id;
            $data['product'] = $prod_res;
            $data['client_name'] = $buy_click['name'];
            $data['client_phone'] = $buy_click['phone'];
            $data['total'] = $product['product_price'] * $count;
            $data['sign'] = $_SESSION['sign'];

            //Отправка сообщения на почту администратору
            $shop->sendMail($modx->config['emailsender'], "buy_click", $data);

            $ajax['buy_click'] = 1;
            $ajax['title'] = $translate[19]; // Спасибо! [#19#]
            $ajax['content'] = $translate[36]; // Ваша заявка принята! Наш менеджер свяжется с вами в ближайшее время! [#36#]
            $ajax['status']  = 1;
        } else {
            $ajax['buy_click'] = 1;
            $ajax['title'] = $translate[21]; // Внимание! [#21#]
            $ajax['content'] = $translate[22]; // Что-то пошло не так! [#22#]
            $ajax['status']  = 0;
        }
        break;

    // Аякс поиск
    case "search":
        if (trim($_POST['search']) != '') {
            $content         = $modx->runSnippet("Shop", array("get" => "searchProduct", "tpl" => "tpl_ajaxSearch"));
            $ajax['status']  = 1;
            $ajax['content'] = $modx->parseDocumentSource($content);
        }
        break;

    // Подписка на рассылку
    case "subscribe":
        if (isset($_POST['subscribe']) && count($_POST['subscribe']) > 0 && $_SESSION['hash']['subscribe'] != md5(serialize($_POST['subscribe'])) && filter_var($_POST['subscribe']['email'], FILTER_VALIDATE_EMAIL)){
            $user = isset($_SESSION['webuser']['fullname']) ? $_SESSION['webuser']['fullname'] : "";
            $modx->db->query('
                  INSERT INTO `modx_a_mailer_users` SET 
                  `user_name`  = "'.$modx->db->escape($user).'",
                  `user_email` = "'.$modx->db->escape($_POST['subscribe']['email']).'" 
                  ON DUPLICATE KEY UPDATE `user_email` = `user_email`');
            if($modx->db->getAffectedRows()) {
                $_SESSION['hash']['subscribe'] = md5(serialize($_POST['subscribe']));
                $ajax['content'] = $translate[49]; // Вы были успешно подписаны на рассылку! [#49#]
            } else {
                $ajax['content'] = $translate[50]; // Вы уже подписаны на рассылку. [#50#]
            }
        } elseif (count($_POST['subscribe']) > 0) {
            $ajax['content'] = $translate[50]; // Вы уже подписаны на рассылку. [#50#]
        }
        $ajax['status'] = 1;
        break;

    // Загрузка аватарки
    case "upload_avatar":
        if (isset($_SESSION['webuser']['internalKey']) && $_SESSION['webuser']['internalKey'] > 0) {
            $user = (int)$_SESSION['webuser']['internalKey'];
            $dir  = MODX_BASE_PATH . "assets/images/avatars/";

            if (!file_exists($dir)) {
                mkdir($dir);
                chmod($dir, 0755);
            }

            if (!file_exists($dir . $user)) {
                mkdir($dir . $user);
                chmod($dir . $user, 0755);
            }

            if (is_array($_FILES['upload_avatar']['tmp_name']) && count($_FILES['upload_avatar']['tmp_name']) == 1) { // Заготовка на случай загрузки нескольких файлов
                if ($objs = glob($dir.$user."/*")){ // Удаляем старую аватарку
                    foreach($objs as $obj){
                        is_dir($obj) ? remover($obj) : unlink($obj);
                    }
                }

                foreach ($_FILES['upload_avatar']['tmp_name'] as $key => $value){
                    $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['upload_avatar']['name'][$key]));
                    $filename = $dir.$user."/". $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                }
                $modx->db->query("UPDATE `modx_web_user_attributes` SET `photo` = 'assets/images/avatars/{$user}/{$name}' WHERE `internalKey` = '{$user}';");
                $_SESSION['webuser']['photo'] = "assets/images/avatars/{$user}/{$name}";
                $ajax['image'] = $modx->runSnippet('R', array('img' => "assets/images/avatars/{$user}/{$name}", 'opt' => 'w=100&h=100&zc=1'));
                $ajax['status'] = 1;
            }
        } else {
            $ajax['message'] = $translate['22']; // Что-то пошло не так.
        }
        break;

    // Авторизация через соц.сети
    case "socialAuth":
        if(isset($_POST['token'])) {
            $profile = file_get_contents('https://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
            $profile = json_decode($profile,true);

            $email = filter_var($profile['email'], FILTER_SANITIZE_EMAIL);

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $check = $modx->db->getRow($modx->db->query('
                    SELECT 
                    COUNT(*) AS "cnt" 
                    FROM `modx_web_user_attributes` 
                    WHERE `email` = "'.$modx->db->escape($email).'"
                '));
                if ($check['cnt'] > 0) { // Если такой email есть - авторизуем
                    $modx->runSnippet("Auth", ["login" => $email, "autologin" => true]);
                    $ajax['status']  = 1;
                } else { // Если нет - создаем нового пользователя
                    $password = $modx->runSnippet("Shop", ["get" => "setToken"]);
                    $data = Array(
                        "username"   => $modx->db->escape($email),
                        "password"   => md5($password),
                        "email"      => $modx->db->escape($email),
                        "fullname"   => $modx->db->escape($profile['first_name'].' '.$profile['last_name'])
                    );

                    $modx->db->query("INSERT INTO `modx_web_users` SET `username` = '{$data['username']}', `password` = '{$data['password']}'");
                    $user_id  = $modx->db->getInsertId();
                    $modx->db->query("
                        INSERT INTO `modx_web_user_attributes` SET 
                            `internalKey` = '{$user_id}', 
                            `email`       = '{$data['email']}',
                            `fullname`    = '{$data['fullname']}'
                    ");

                    $modx->db->query("INSERT INTO `modx_web_groups` SET webgroup = '1', webuser = '{$user_id}'");
                    $shop->sendMail($email, "register", ["email" => $email, "password" => $password]);
                    $shop->sendMail($modx->config['emailsender'], "register_admin", ["email" => $email]);
                    $modx->runSnippet("Auth", ["login" => $email, "autologin" => true]);
                    $ajax['status']  = 1;
                }
                $ajax['link'] = $modx->makeUrl($modx->config['webuser_account']);
            } else {
                // Не авторизован
            }
        }
        break;

   // Избранное
    case "wishlist":
        $product_id = (int)$_GET['product'];

        if (isset($_SESSION['webuser']['internalKey']) && $_SESSION['webuser']['internalKey'] > 0) { // Авторизованный пользователь
            $sql = "SELECT COUNT(product_id) FROM `modx_a_wishlist` WHERE user_id = " . (int)$_SESSION['webuser']['internalKey'] . "  AND product_id = {$product_id}";
            $val = $modx->db->getValue($modx->db->query($sql));

            if ($val == 0) {
                if (!empty($product_id) && $product_id > 0) {
                    $modx->db->query("INSERT INTO `modx_a_wishlist` (user_id, product_id) VALUE ('" . (int)$_SESSION['webuser']['internalKey'] . "', '{$product_id}')  ");
                }
                $res = $modx->db->getValue($modx->db->query("SELECT COUNT(product_id) FROM `modx_a_wishlist` WHERE user_id = " . (int)$_SESSION['webuser']['internalKey']));

                if ( (int)$res == 0 ) {
                    $ajax['count'] = '';
                } else {
                    $ajax['count'] = (int)$res;
                }

                $ajax['status']  = 1;
                $ajax['text']    = $translate['211']; // Убрать из избранного (надпись на кнопке)
                $ajax['message'] = $translate['178']; // Товар добавлен в список желаний
            } else {
                if (!empty($product_id) && $product_id > 0) {
                    $modx->db->query("DELETE FROM `modx_a_wishlist` WHERE user_id = " . (int)$_SESSION['webuser']['internalKey'] . " AND product_id = {$product_id}");
                    $res = $modx->db->getValue($modx->db->query("SELECT COUNT(product_id) FROM `modx_a_wishlist` aw WHERE user_id = " . (int)$_SESSION['webuser']['internalKey'] ));

                    if ( (int)$res == 0 ) { // Ноль прячем из за дизайна
                        $ajax['count'] = '';
                    } else {
                        $ajax['count'] = (int)$res;
                    }

                    $ajax['status']  = 0;
                    $ajax['text']    = $translate['64']; // Добавить в избранное (надпись на кнопке)
                    if ((int)$res > 0) {
                        $ajax['message'] = $translate['179']; // Товар удален из списока желаний
                    } else {
                        $ajax['message'] = $translate['187']; // Ваш список желаний пуст
                    }
                } else {
                    $ajax['message'] = $translate['22']; // Что-то пошло не так.
                }
            }
        } else { // Не авторизованный пользователь.
            if (isset($_SESSION['wishlist']) && isset($_SESSION['wishlist'][$product_id])) {
                unset($_SESSION['wishlist'][$product_id]);
                $ajax['status']  = 0;
                $ajax['text']    = $translate['64']; // Добавить в избранное (надпись на кнопке)
                if ( is_array($_SESSION['wishlist']) && count($_SESSION['wishlist']) > 0 ) {
                    $ajax['message'] = $translate['179']; // Товар удален из списока желаний
                } else {
                    $ajax['message'] = $translate['187']; // Ваш список желаний пуст
                }
            } else {
                $ajax['status']  = 1;
                $ajax['text']    = $translate['211']; // Убрать из избранного (надпись на кнопке)
                $ajax['message'] = $translate['178']; // Товар добавлен в список желаний
                if (!empty($product_id) && $product_id > 0) {
                    $_SESSION['wishlist'][$product_id] = 1;
                }
            }

            if ( is_array($_SESSION['wishlist']) && count($_SESSION['wishlist']) > 0 ) { // Ноль прячем из за дизайна
                $ajax['count'] = count($_SESSION['wishlist']);
            } else {
                $ajax['count'] = '';
            }

            $_SESSION['webuser']['wishlist'] = $ajax['count'] > 0;
        }
        break;

        // изменить/отправить комментарий товара корзину
    case "change_comment":
        $buy = (int)$_GET['product'];
        if (isset($_GET['comment']) && strip_tags(stripslashes(trim($_GET['comment'], '/'))) != '' ) {
            $_SESSION['cart_string'][$buy] = strip_tags(stripslashes(trim($_GET['comment'], '/')));
        }
        $ajax['status'] = 1;
        break;

    // Положить товар в корзину
    case "buy":
        $buy = (int)$_GET['product'];
        if ($buy) {
            $count = (int)$_GET['count'] > 1 ? (int)$_GET['count'] : 1;
            $_GET['add_eqp'] = trim($_GET['add_eqp'],'/');

            if ($count > 1 || (int)$_GET['strong'] == 1) {
                if(empty($_SESSION['cart'][$buy]) || !is_array($_SESSION['cart'][$buy])){
                    $_SESSION['cart'][$buy] = [];
                    $_SESSION['cart'][$buy][] = ['count'=>$count, 'eqp'=>$_GET['eqp'], 'add_eqp'=>$_GET['add_eqp']];
                }else {
                    $addProduct = false;
                    foreach ($_SESSION['cart'][$buy] as $key=>$item){
                        if($item['eqp'] == $_GET['eqp'] && $item['add_eqp'] == $_GET['add_eqp']){
                            $_SESSION['cart'][$buy][$key]['count'] += $count;
                            $addProduct = true;
                        }
                    }
                    if(!$addProduct){
                        $_SESSION['cart'][$buy][] = ['count'=>$count, 'eqp'=>$_GET['eqp'], 'add_eqp'=>$_GET['add_eqp']];
                    }
                }
            } else {
                if(empty($_SESSION['cart'][$buy]) || !is_array($_SESSION['cart'][$buy])){
                    $_SESSION['cart'][$buy] = [];
                    $_SESSION['cart'][$buy][] = ['count'=>$count, 'eqp'=>$_GET['eqp'], 'add_eqp'=>$_GET['add_eqp']];
                }else {
                    $addProduct = false;
                    foreach ($_SESSION['cart'][$buy] as $key=>$item){
                        if($item['eqp'] == $_GET['eqp'] && $item['add_eqp'] == $_GET['add_eqp']){
                            $_SESSION['cart'][$buy][$key]['count'] += $count;
                            $addProduct = true;
                        }
                    }
                    if(!$addProduct){
                        $_SESSION['cart'][$buy][] = ['count'=>$count, 'eqp'=>$_GET['eqp'], 'add_eqp'=>$_GET['add_eqp']];
                    }
                }
            }
            if (isset($_GET['comment']) && strip_tags(stripslashes(trim($_GET['comment'], '/'))) != '' ) {
                $_SESSION['cart_string'][$buy] = strip_tags(stripslashes(trim($_GET['comment'], '/')));
            }
            $ajax['count']  = array_sum($_SESSION['cart']);
            $ajax['total']       = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCartTotal")));
            $ajax['cart']        = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCart", 'tpl' => 'tpl_cart', 'tplCharac' => 'tpl_cart_characteristics')));
            $ajax['declination'] = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCartCountDeclination")));

            $ajax['status'] = 1;
        }
        break;

    // Удалить товар из корзины
    case "deleteProductCart":
        $product = (int)$_GET['product'];
        if ($product) {
            unset($_SESSION['cart'][$product]);
            unset($_SESSION['cart_string'][$product]);
            $ajax['count']       = array_sum($_SESSION['cart']);
            $ajax['total']       = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCartTotal")));
            $ajax['cart']        = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCart", 'tpl' => 'tpl_cart', 'tplCharac' => 'tpl_cart_characteristics')));
            $ajax['declination'] = $modx->parseDocumentSource($modx->runSnippet("Shop", array("get" => "getCartCountDeclination")));
            $ajax['status'] = 1;
//            if ($ajax['count'] == 0) {
//                $_SESSION['js'] = '$("#info-success").find(".title-desc").html("'.$translate[177].'");$("#info-success").modal("show");'; // Ваша корзина пуста.
//            }
        }
        break;

    // Применить промокод
    case "promocode":
        $code = $modx->db->escape(trim($_GET['code']));
        if ($code) {
            $result = $modx->db->getRow($modx->db->query("
                SELECT *,
                    (CASE WHEN 	code_freq = 1
                        THEN (SELECT count(*) from `modx_a_order` WHERE order_code = code_id) 
                        ELSE 0 
                    END) AS 'code_used'
                FROM `modx_a_codes` WHERE `code` = '{$code}' AND `code_status` = '1' AND `code_expire` >= CURRENT_DATE()
            "));
            if ($result && !$result['code_used']) {
                $ajax['summa']  = $result['code_mod'];
                $ajax['status'] = 1;
                $ajax['text'] = $translate[130]; // Промокод применен
            } else {
                $ajax['status'] = 0;
                $ajax['text'] = $translate[129]; // Промокод не найден
            }
        }
        break;

    // Вывести форму для добавление нового адреса
    case "addAccountAddress":
        $tpl  = "tpl_accountDeliveryList";
        $item = [
            "delivery_id"         => "i_".$_REQUEST['index'],
            "delivery_name"       => $_SESSION['webuser']['fullname'],
            "delivery_phone"      => $_SESSION['webuser']['mobilephone'],
            "delivery_city"       => $_SESSION['webuser']['city'],
            "delivery_street"     => $_SESSION['webuser']['street'],
            "delivery_house"      => "",
            "delivery_apartament" => "",
            "delivery_comment"    => "",
        ];
        $ajax['content'] = $modx->parseDocumentSource($modx->parseChunk($tpl, $item));
        $ajax['status']  = 1;
        break;

    // Смена валюты
    case "changeCurrency":
        $currencies           = explode("|", $modx->config['shop_curr_code']);
        $signs                = explode("|", $modx->config['shop_curr_sign']);
        $curKey               = array_search(trim($_GET['code']), $currencies);
        $_SESSION['sign']     = $signs[$curKey];
        $_SESSION['currency'] = trim($_GET['code']);
        $ajax['status']       = 1;
        break;

    // Вывести форму для с методом доставки
    case "deliveryMethod":
        $content                         = $modx->runSnippet("Shop", ["get" => "getDeliveryLists", "delivery" => (int)$_POST['delivery'], "tpl" => "tpl_deliveryAddress"]);
        $ajax['status']                  = 1;
        $ajax['content']                 = $modx->parseDocumentSource($content);
        $ajax['delivery_price']          = $modx->getPlaceholder('delivery_price');
        $ajax['delivery_price_formated'] = $modx->getPlaceholder('delivery_price');
        break;

    // Вывести список отделений новой почты для города
    case "novaPoshtaDepartaments":
        $content = $modx->runSnippet("Shop", ["get" => "novaPoshtaDepartaments", "cityRef" => $_POST['cityRef'], "tpl" => "tpl_deliveryInner_3"]);
        $ajax['status']  = 1;
        $ajax['content'] = $content;
        break;

    // Добавить отзыв к товару
    case "addReviews":
        if (isset($_POST['review']) && count($_POST['review']) > 0) {
            $modx->db->query("
                INSERT INTO `modx_a_reviews` SET 
                    `review_product`    = '".(int)$_POST['review']['product']."',
                    `review_rating`     = '".(int)$_POST['review']['rating']."',
                    `review_user_email` = '".$this->db->escape($_POST['review']['email'])."',
                    `review_webuser`    = '".(int)$_SESSION['webuser']['internalKey']."'
            ");
            $id = $modx->db->getRow($modx->db->query("SELECT LAST_INSERT_ID() AS last_id"));
            $modx->db->query("
                INSERT INTO `modx_a_review_strings` SET 
                    `string_locate`    = '".$this->db->escape($_POST['review']['lang'])."',
                    `review_id`        = '".(int)$id['last_id']."',
                    `review_user_name` = '".$this->db->escape($_POST['review']['name'])."',
                    `review_content`   = '".$this->db->escape($_POST['review']['message'])."'
            ");

            $folder = MODX_BASE_PATH."assets/images/reviews/";

            if (!file_exists($folder)) {
                mkdir($folder);
                chmod($folder, 0755);
            }

            if (!file_exists($folder . (int)$id['last_id'])) {
                mkdir($folder . (int)$id['last_id']);
                chmod($folder . (int)$id['last_id'], 0755);
            }

            $folder .= (int)$id['last_id'];
            $allowedType = ['image/jpeg', 'image/png']; // Разрешенные к загрузке файлы

            foreach ($_FILES['review']['tmp_name']['file'] as $key => $value) {
                if (in_array($_FILES['review']['type']['file'][$key], $allowedType) && $_FILES['review']['size']['file'][$key] < $modx->config['upload_maxsize'] && $_FILES['review']['size']['file'][$key] > 0 ) {
                    $name = preg_replace('/\D+/', '', microtime()) . "." . end(explode(".", $_FILES['review']['name']['file'][$key]));
                    $filename = $folder . "/" . $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                    $modx->db->query("INSERT INTO `modx_a_review_images` SET `file` = '" . $name . "', `review_id` = '" . (int)$id['last_id'] . "'");
                }
            }

            $ajax['status']  = 1;
            $ajax['content'] = $translate[127]; // Ваш отзыв успешно добавлен
        }
        break;

    // Получить консультацию
    case "getConsultation":
        if (count($_POST['consult']) > 0) {
            $shop->sendMail($modx->config['emailsender'], "consultation", $_POST['consult']);
            $ajax['status']  = 1;
            $ajax['content'] = $translate[175]; // Ваш запрос успешно добавлен
        }
        break;
}

die(json_encode($ajax));