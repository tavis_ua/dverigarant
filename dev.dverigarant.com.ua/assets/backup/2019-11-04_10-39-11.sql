#
# Двери Гарант Database Dump
# MODX Version:1.2 Artjoker
# 
# Host: 
# Generation Time: 04-11-2019 10:39:11
# Server version: 5.7.27-0ubuntu0.18.04.1
# PHP Version: 7.2.24-0ubuntu0.18.04.1
# Database: `dverigarant_DB`
# Description: 
#

# --------------------------------------------------------

#
# Table structure for table `modx_a_arias`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_arias`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_arias` (
  `aria_id` int(11) NOT NULL AUTO_INCREMENT,
  `aria_name_ru` varchar(255) NOT NULL,
  `aria_name_ua` varchar(255) NOT NULL,
  PRIMARY KEY (`aria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_arias`
#

INSERT INTO `modx_a_arias` VALUES ('3','левый берег','Лівий берег');

INSERT INTO `modx_a_arias` VALUES ('4','Правый берег','Правий берег');

INSERT INTO `modx_a_arias` VALUES ('5','Центр','яыктяыктывтыв');

INSERT INTO `modx_a_arias` VALUES ('6','ыыыффкыркыиывки wsegv. g','wrwabwarbasrb');

INSERT INTO `modx_a_arias` VALUES ('7','325hdsffb','aerttjaetj');

INSERT INTO `modx_a_arias` VALUES ('8','tot','ewe');

INSERT INTO `modx_a_arias` VALUES ('9','тестовый берег','тестовий берег');

INSERT INTO `modx_a_arias` VALUES ('10','','');


# --------------------------------------------------------

#
# Table structure for table `modx_a_codes`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_codes`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_codes` (
  `code_id` int(10) NOT NULL AUTO_INCREMENT,
  `code_date` datetime NOT NULL,
  `code_expire` date NOT NULL,
  `code_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 off 1 on',
  `code` varchar(32) NOT NULL,
  `code_freq` tinyint(1) NOT NULL COMMENT '1 single 2 multi',
  `code_mod` int(10) NOT NULL,
  PRIMARY KEY (`code_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_codes`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_delivery`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_delivery`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_delivery` (
  `delivery_id` int(10) NOT NULL,
  `delivery_icon` varchar(64) NOT NULL DEFAULT '',
  `delivery_title_ru` varchar(64) NOT NULL DEFAULT '0',
  `delivery_title_ua` varchar(64) NOT NULL DEFAULT '0',
  `delivery_title_en` varchar(64) NOT NULL DEFAULT '0',
  `delivery_cost` int(11) NOT NULL DEFAULT '0' COMMENT 'Стоимость доставки',
  `delivery_default` tinyint(4) NOT NULL DEFAULT '0',
  `delivery_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`delivery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_delivery`
#

INSERT INTO `modx_a_delivery` VALUES ('1','<i class=\"fa fa-bicycle\" aria-hidden=\"true\"></i>','Самовывоз','Самовивіз','Self delivery','0','0','0');

INSERT INTO `modx_a_delivery` VALUES ('2','<i class=\"fa fa-car\" aria-hidden=\"true\"></i>','Доставка курьером','Доставка кур&apos;єром','Courier delivery','50','1','1');

INSERT INTO `modx_a_delivery` VALUES ('3','<i class=\"fa fa-truck\" aria-hidden=\"true\"></i>','Новая почта','Нова Пошта','New post','16','0','0');


# --------------------------------------------------------

#
# Table structure for table `modx_a_delivery_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_delivery_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_delivery_settings` (
  `setting_id` int(10) NOT NULL,
  `setting_delivery` int(10) NOT NULL,
  `setting_key_all` varchar(100) NOT NULL DEFAULT '',
  `setting_value_all` varchar(100) NOT NULL DEFAULT '',
  `setting_name_ua` varchar(100) NOT NULL DEFAULT '',
  `setting_name_ru` varchar(100) NOT NULL DEFAULT '',
  `setting_name_en` varchar(100) NOT NULL DEFAULT '',
  `setting_value_num` float(5,2) NOT NULL DEFAULT '0.00',
  `setting_value_ua` varchar(100) NOT NULL DEFAULT '',
  `setting_value_ru` varchar(100) NOT NULL DEFAULT '',
  `setting_value_en` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_delivery_settings`
#

INSERT INTO `modx_a_delivery_settings` VALUES ('1','1','city','','Харків','Харьков','Kharkiv','0.00','вул. Міклухи Маклая 48','ул. Миклухи Маклая 48','str. Miklouho Maclay 48');

INSERT INTO `modx_a_delivery_settings` VALUES ('2','1','city','','Одеса','Одесса','Odessa','0.00','пл. Морський Порт 99','пл. Морской Порт 99','pl. Seaport 99');

INSERT INTO `modx_a_delivery_settings` VALUES ('3','2','city','','Харків','Харьков','Kharkiv','40.00','','','');

INSERT INTO `modx_a_delivery_settings` VALUES ('4','2','city','','Одеса','Одесса','Odessa','50.00','','','');

INSERT INTO `modx_a_delivery_settings` VALUES ('5','2','city','','Київ','Киев','Kiev','70.00','','','');

INSERT INTO `modx_a_delivery_settings` VALUES ('6','3','novaPoshtaKey','3123c81426543007db06821570d83bb1','','','','0.00','','','');

INSERT INTO `modx_a_delivery_settings` VALUES ('7','3','priceDefault','','','','','50.00','','','');

INSERT INTO `modx_a_delivery_settings` VALUES ('8','2','priceCourierDefault','','','','','50.00','','','');


# --------------------------------------------------------

#
# Table structure for table `modx_a_discounts`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_discounts`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_discounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `alias` varchar(255) NOT NULL COMMENT 'Alias',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image link',
  `published` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Published',
  `gallery` text COMMENT 'Gallery',
  `home_at` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Show at home page',
  `success_at` enum('0','1') NOT NULL,
  `thanks_at` enum('0','1') NOT NULL,
  `discounts_products` varchar(255) DEFAULT NULL,
  `discounts_category` varchar(255) DEFAULT NULL,
  `published_at` int(10) NOT NULL DEFAULT '0' COMMENT 'Published time',
  `beginning_at` int(10) NOT NULL,
  `ending_at` int(10) NOT NULL,
  `created_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Create time',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Update time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `published_time` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_discounts`
#

INSERT INTO `modx_a_discounts` VALUES ('1','onlajn-wetwqetqwet-smartlink','0231316001568033442.jpeg','1',NULL,'0','0','1','2','','1567468800','1567296000','1577750400','1568028994','1572536232');

INSERT INTO `modx_a_discounts` VALUES ('2','12352365677','0343478001568033542.jpeg','1',NULL,'0','1','0','2','','1567296000','1569542400','1577750400','1568029636','1572536768');


# --------------------------------------------------------

#
# Table structure for table `modx_a_discounts_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_discounts_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_discounts_strings` (
  `rid` int(11) unsigned NOT NULL COMMENT 'Related news ID',
  `lang` varchar(16) NOT NULL COMMENT 'Language',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `content` text COMMENT 'Content',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_canonical` varchar(255) DEFAULT NULL,
  `seo_robots` varchar(255) DEFAULT NULL,
  UNIQUE KEY `rid` (`rid`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_discounts_strings`
#

INSERT INTO `modx_a_discounts_strings` VALUES ('1','ru','Действует Акция – бесплатная установка!','<p>Акция на определенную группу товаров и только на входные двери</p>','<p>Акция распространяется на определенную группу товаров и только на входные двери, все вопросы уточняйте у менеджера по&nbsp; телефонам: (067) 830-88-85, (093) 557-68-80 Наш отдел логистики и мастера по установке готовы сделать свою работу в максимально удобных обстоятельствах для Вас. Мы планируем все действия по перемещению бригад иготовы доставить и установить дверь именно в удобное для Вас время.. Установка двери- важный шаг, как для Вас, как покупателя двери, так и для нас, как продавца. Мы несем гарантийные обязательства и заинтерисованы в качестве установки, поэтому процесс установки необходимо начинать с замера дверного проема. Поэтому рекомендуем Вам воспользоваться услугой-выезд мастера по замеру дверного проема. Тогда в случае Вашего заказа, мы сможем контролировать и гарантировать качество работ и долгосрочность работы изделия.&nbsp;</p>','','','','','index,follow');

INSERT INTO `modx_a_discounts_strings` VALUES ('1','ua','Діє Акція - безкоштовна установка!','<p>Акція на певну групу товарів і тільки на вхідні двері</p>','<p>Акція поширюється на певну групу товарів і тільки на вхідні двері, всі питання уточнюйте у менеджера за телефонами: (067) 830-88-85, (093) 557-68-80 Наш відділ логістики і майстри по установці готові зробити свою роботу в максимально зручних обставин для Вас. Ми плануємо всі дії по переміщенню бригад іготови доставити і встановити двері саме в зручне для Вас час .. Установка двери- важливий крок, як для Вас, як покупця двері, так і для нас, як продавця. Ми несемо гарантійні зобов\'язання і зацікавлених в якості установки, тому процес установки необхідно починати з виміру дверного отвору. Тому рекомендуємо Вам скористатися послугою-виїзд майстра по виміру дверного отвору. Тоді в разі Вашого замовлення, ми зможемо контролювати і гарантувати якість робіт і довгостроковість роботи вироби.</p>','','','','','index,follow');

INSERT INTO `modx_a_discounts_strings` VALUES ('2','ru','Утеплить свое жилье и сэкономить до 20%!','<p>Если замер осуществляется за городом- оплату уточняйте у менеджера</p>','<p>Осень-время, чтобы побеспокоиться об утеплении жилья. Именно поэтому, мыпредлагаем Вам совершить выгодную покупку в нашем интернет- магазине. Вы решили утеплить свою квартиру или частный дом, ищите качественные дверные конструкции. Спешите воспользоваться нашим акционным предложением. В нашем магазине предоставлен широкий ассортимент дверей, которые максимально помогут утеплить Ваше жилье. Воспользоваться акционным предложением может каждый клиент. Для уточнения условий акции Вы сможете связаться с нашими менеджерами, которые с удовольствием ответят на все Ваши вопросы. Не откладывайте покупку дверей по акции на завтра. Торопитесь оформить заказ уже сегодня, чтобы утеплить свое жилье качественными дверными конструкциями с экономией 20%.&nbsp;</p>','','','','','index,follow');

INSERT INTO `modx_a_discounts_strings` VALUES ('2','ua','Утеплити своє житло і заощадити до 20%!','<p>Якщо замір здійснюється за містом-оплату уточнюйте у менеджера</p>','<p>Осінь-час, щоб потурбуватися про утеплення житла. Саме тому, мипредлагаем Вам зробити вигідну покупку в нашому інтернет-магазині. Ви вирішили утеплити свою квартиру або приватний будинок, шукайте якісні дверні конструкції. Поспішайте скористатися нашим акційною пропозицією. У нашому магазині представлений широкий асортимент дверей, які максимально допоможуть утеплити Ваше житло. Скористатися акційною пропозицією може кожен клієнт. Для уточнення умов акції Ви зможете зв\'язатися з нашими менеджерами, які з задоволенням дадуть відповідь на всі Ваші запитання. Не відкладайте покупку дверей по акції на завтра. Поспішайте оформити замовлення вже сьогодні, щоб утеплити своє житло якісними дверними конструкціями з економією 20%.</p>','','','','','index,follow');


# --------------------------------------------------------

#
# Table structure for table `modx_a_faq_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_faq_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_faq_strings` (
  `string_id` int(11) NOT NULL AUTO_INCREMENT,
  `string_locate` varchar(4) NOT NULL,
  `faq_id` int(11) NOT NULL,
  `faq_question` varchar(512) NOT NULL DEFAULT '',
  `faq_answer` longtext,
  PRIMARY KEY (`string_id`),
  UNIQUE KEY `str_locate_faq_id` (`string_locate`,`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_faq_strings`
#

INSERT INTO `modx_a_faq_strings` VALUES ('1','ru','1','Laboriosam temporib',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('2','ru','2','Laboriosam temporib',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('3','ru','3','Ab sed adipisci quas',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('4','ru','4','тестовый комментанийи тут он жевот?','');

INSERT INTO `modx_a_faq_strings` VALUES ('5','ua','5','qqqqaaaazzzzxx x x x x x x x',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('6','ru','6','Как заказать? ',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('7','ua','7','ари орв мров ','');

INSERT INTO `modx_a_faq_strings` VALUES ('9','ru','7','','');

INSERT INTO `modx_a_faq_strings` VALUES ('10','ua','4','','');

INSERT INTO `modx_a_faq_strings` VALUES ('12','ru','8','выафыафываыфва',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('13','ru','9','увцйувфыв',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('14','ru','10','фывыфвыфвы',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('15','ru','11','фывыфвыфвы',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('16','ua','12','Есть ли в наличие что-то там? ',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('17','ua','13','Qui quia fugiat id ',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('18','ua','14','Aut ipsa mollit sin',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('19','ua','15','тест',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('20','ua','16','Qui ad recusandae A',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('21','ua','17','Qui ad recusandae A',NULL);

INSERT INTO `modx_a_faq_strings` VALUES ('22','ua','18','Eiusmod ut iure sit ',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_faqs`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_faqs`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_faqs` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category` int(11) NOT NULL DEFAULT '0',
  `faq_name` varchar(100) NOT NULL DEFAULT '',
  `faq_phone` varchar(100) NOT NULL DEFAULT '',
  `faq_email` varchar(100) NOT NULL DEFAULT '',
  `faq_active` tinyint(4) NOT NULL DEFAULT '0',
  `faq_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `faq_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_faqs`
#

INSERT INTO `modx_a_faqs` VALUES ('1','0','Charde Matthews','+38(1)2222222','','0','2019-10-02 15:05:13','2019-10-02 15:05:13');

INSERT INTO `modx_a_faqs` VALUES ('2','0','Charde Matthews','+38(1)2222222','','0','2019-10-02 15:23:45','2019-10-02 15:23:45');

INSERT INTO `modx_a_faqs` VALUES ('3','0','Uriah Rutledge','+38(1)1111111','','0','2019-10-02 17:12:46','2019-10-02 17:12:46');

INSERT INTO `modx_a_faqs` VALUES ('4','0','тесст третий','+38(987)9879879','','1','2019-10-03 10:38:59','2019-10-15 13:04:29');

INSERT INTO `modx_a_faqs` VALUES ('5','0','тест Арт','+38(907)7339500','','0','2019-10-08 13:52:12','2019-10-08 13:52:12');

INSERT INTO `modx_a_faqs` VALUES ('6','0','Анна','+38(251)5315415','','0','2019-10-10 10:19:40','2019-10-10 10:19:40');

INSERT INTO `modx_a_faqs` VALUES ('7','0','Анна','+38(954)171287','','1','2019-10-14 12:52:36','2019-10-15 13:02:24');

INSERT INTO `modx_a_faqs` VALUES ('8','0','фывыфвыфв','+38(213)1321322','','0','2019-10-15 16:28:00','2019-10-15 16:28:00');

INSERT INTO `modx_a_faqs` VALUES ('9','0','ыфвыфв','+38(138)3213123','','0','2019-10-15 16:33:17','2019-10-15 16:33:17');

INSERT INTO `modx_a_faqs` VALUES ('10','0','ыфвыфв','+38(133)2132138','','0','2019-10-15 16:38:00','2019-10-15 16:38:00');

INSERT INTO `modx_a_faqs` VALUES ('11','0','ыфвыфв','+38(133)2132138','','0','2019-10-15 16:39:36','2019-10-15 16:39:36');

INSERT INTO `modx_a_faqs` VALUES ('12','0','Анна','+38(095)4171287','','0','2019-10-17 10:13:50','2019-10-17 10:13:50');

INSERT INTO `modx_a_faqs` VALUES ('13','0','Idona Britt','+38(1)5421545','','0','2019-10-17 15:20:51','2019-10-17 15:20:51');

INSERT INTO `modx_a_faqs` VALUES ('14','0','Chanda Sloan','+38(1)','','0','2019-10-17 15:22:56','2019-10-17 15:22:56');

INSERT INTO `modx_a_faqs` VALUES ('15','0','тест','+38(022)2000000','','0','2019-10-31 17:21:23','2019-10-31 17:21:23');

INSERT INTO `modx_a_faqs` VALUES ('16','0','Emerald Terrell','+38(1)1111111','','0','2019-11-01 09:32:17','2019-11-01 09:32:17');

INSERT INTO `modx_a_faqs` VALUES ('17','0','Emerald Terrell','+38(1)1111111','','0','2019-11-01 09:32:18','2019-11-01 09:32:18');

INSERT INTO `modx_a_faqs` VALUES ('18','0','Hedley Spencer','+38(1)1111111','','0','2019-11-01 14:55:25','2019-11-01 14:55:25');


# --------------------------------------------------------

#
# Table structure for table `modx_a_filter_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_filter_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_filter_categories` (
  `filter_id` int(10) NOT NULL,
  `modx_id` int(10) NOT NULL,
  UNIQUE KEY `filter_id_modx_id` (`filter_id`,`modx_id`),
  KEY `modx_id` (`modx_id`),
  KEY `filter_id` (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_filter_categories`
#

INSERT INTO `modx_a_filter_categories` VALUES ('3','6');

INSERT INTO `modx_a_filter_categories` VALUES ('4','6');

INSERT INTO `modx_a_filter_categories` VALUES ('4','7');

INSERT INTO `modx_a_filter_categories` VALUES ('4','8');

INSERT INTO `modx_a_filter_categories` VALUES ('9','8');

INSERT INTO `modx_a_filter_categories` VALUES ('4','9');

INSERT INTO `modx_a_filter_categories` VALUES ('9','9');

INSERT INTO `modx_a_filter_categories` VALUES ('4','10');

INSERT INTO `modx_a_filter_categories` VALUES ('9','10');

INSERT INTO `modx_a_filter_categories` VALUES ('4','11');

INSERT INTO `modx_a_filter_categories` VALUES ('9','11');

INSERT INTO `modx_a_filter_categories` VALUES ('3','12');

INSERT INTO `modx_a_filter_categories` VALUES ('4','12');

INSERT INTO `modx_a_filter_categories` VALUES ('9','12');

INSERT INTO `modx_a_filter_categories` VALUES ('4','13');

INSERT INTO `modx_a_filter_categories` VALUES ('9','13');

INSERT INTO `modx_a_filter_categories` VALUES ('4','14');

INSERT INTO `modx_a_filter_categories` VALUES ('9','14');

INSERT INTO `modx_a_filter_categories` VALUES ('4','15');

INSERT INTO `modx_a_filter_categories` VALUES ('4','16');

INSERT INTO `modx_a_filter_categories` VALUES ('9','16');

INSERT INTO `modx_a_filter_categories` VALUES ('9','17');

INSERT INTO `modx_a_filter_categories` VALUES ('9','21');

INSERT INTO `modx_a_filter_categories` VALUES ('9','22');

INSERT INTO `modx_a_filter_categories` VALUES ('9','23');

INSERT INTO `modx_a_filter_categories` VALUES ('9','24');

INSERT INTO `modx_a_filter_categories` VALUES ('9','25');

INSERT INTO `modx_a_filter_categories` VALUES ('9','26');

INSERT INTO `modx_a_filter_categories` VALUES ('9','27');

INSERT INTO `modx_a_filter_categories` VALUES ('9','28');

INSERT INTO `modx_a_filter_categories` VALUES ('20','46');

INSERT INTO `modx_a_filter_categories` VALUES ('2','47');

INSERT INTO `modx_a_filter_categories` VALUES ('22','47');

INSERT INTO `modx_a_filter_categories` VALUES ('23','47');

INSERT INTO `modx_a_filter_categories` VALUES ('29','47');

INSERT INTO `modx_a_filter_categories` VALUES ('30','47');

INSERT INTO `modx_a_filter_categories` VALUES ('31','47');

INSERT INTO `modx_a_filter_categories` VALUES ('32','47');

INSERT INTO `modx_a_filter_categories` VALUES ('34','48');

INSERT INTO `modx_a_filter_categories` VALUES ('6','51');

INSERT INTO `modx_a_filter_categories` VALUES ('10','51');

INSERT INTO `modx_a_filter_categories` VALUES ('11','51');

INSERT INTO `modx_a_filter_categories` VALUES ('12','51');

INSERT INTO `modx_a_filter_categories` VALUES ('13','51');

INSERT INTO `modx_a_filter_categories` VALUES ('14','51');

INSERT INTO `modx_a_filter_categories` VALUES ('6','52');

INSERT INTO `modx_a_filter_categories` VALUES ('8','52');

INSERT INTO `modx_a_filter_categories` VALUES ('10','52');

INSERT INTO `modx_a_filter_categories` VALUES ('21','52');

INSERT INTO `modx_a_filter_categories` VALUES ('33','52');

INSERT INTO `modx_a_filter_categories` VALUES ('35','52');

INSERT INTO `modx_a_filter_categories` VALUES ('36','52');

INSERT INTO `modx_a_filter_categories` VALUES ('37','52');

INSERT INTO `modx_a_filter_categories` VALUES ('38','52');

INSERT INTO `modx_a_filter_categories` VALUES ('2','58');

INSERT INTO `modx_a_filter_categories` VALUES ('22','58');

INSERT INTO `modx_a_filter_categories` VALUES ('23','58');

INSERT INTO `modx_a_filter_categories` VALUES ('29','58');

INSERT INTO `modx_a_filter_categories` VALUES ('30','58');

INSERT INTO `modx_a_filter_categories` VALUES ('31','58');

INSERT INTO `modx_a_filter_categories` VALUES ('15','61');

INSERT INTO `modx_a_filter_categories` VALUES ('16','61');

INSERT INTO `modx_a_filter_categories` VALUES ('17','61');

INSERT INTO `modx_a_filter_categories` VALUES ('18','61');

INSERT INTO `modx_a_filter_categories` VALUES ('19','61');

INSERT INTO `modx_a_filter_categories` VALUES ('28','61');

INSERT INTO `modx_a_filter_categories` VALUES ('1','64');

INSERT INTO `modx_a_filter_categories` VALUES ('2','64');

INSERT INTO `modx_a_filter_categories` VALUES ('3','64');

INSERT INTO `modx_a_filter_categories` VALUES ('4','64');

INSERT INTO `modx_a_filter_categories` VALUES ('5','64');

INSERT INTO `modx_a_filter_categories` VALUES ('6','64');

INSERT INTO `modx_a_filter_categories` VALUES ('7','64');

INSERT INTO `modx_a_filter_categories` VALUES ('8','64');

INSERT INTO `modx_a_filter_categories` VALUES ('9','64');

INSERT INTO `modx_a_filter_categories` VALUES ('22','64');

INSERT INTO `modx_a_filter_categories` VALUES ('23','64');

INSERT INTO `modx_a_filter_categories` VALUES ('29','64');

INSERT INTO `modx_a_filter_categories` VALUES ('30','64');

INSERT INTO `modx_a_filter_categories` VALUES ('31','64');

INSERT INTO `modx_a_filter_categories` VALUES ('32','64');

INSERT INTO `modx_a_filter_categories` VALUES ('6','68');

INSERT INTO `modx_a_filter_categories` VALUES ('23','79');

INSERT INTO `modx_a_filter_categories` VALUES ('29','79');

INSERT INTO `modx_a_filter_categories` VALUES ('30','79');

INSERT INTO `modx_a_filter_categories` VALUES ('31','79');

INSERT INTO `modx_a_filter_categories` VALUES ('32','79');


# --------------------------------------------------------

#
# Table structure for table `modx_a_filter_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_filter_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_filter_strings` (
  `string_id` int(10) NOT NULL AUTO_INCREMENT,
  `string_locate` varchar(4) NOT NULL,
  `filter_id` int(10) NOT NULL,
  `filter_name` varchar(512) DEFAULT NULL,
  `filter_description` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`string_id`),
  UNIQUE KEY `str_locate_filter_id` (`string_locate`,`filter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_filter_strings`
#

INSERT INTO `modx_a_filter_strings` VALUES ('4','ua','2','Стиль','');

INSERT INTO `modx_a_filter_strings` VALUES ('5','ru','2','Стиль','');

INSERT INTO `modx_a_filter_strings` VALUES ('6','en','2','Style','');

INSERT INTO `modx_a_filter_strings` VALUES ('13','ua','5','Ширина','');

INSERT INTO `modx_a_filter_strings` VALUES ('14','ru','5','Ширина','');

INSERT INTO `modx_a_filter_strings` VALUES ('15','en','5','Width','');

INSERT INTO `modx_a_filter_strings` VALUES ('28','ru','6','Производитель','');

INSERT INTO `modx_a_filter_strings` VALUES ('29','en','6','Manufacturer','');

INSERT INTO `modx_a_filter_strings` VALUES ('30','ua','6','Виробник','');

INSERT INTO `modx_a_filter_strings` VALUES ('34','ru','7','Спальный размер','');

INSERT INTO `modx_a_filter_strings` VALUES ('35','en','7','Sleeping size','');

INSERT INTO `modx_a_filter_strings` VALUES ('36','ua','7','Спальний розмір','');

INSERT INTO `modx_a_filter_strings` VALUES ('37','ru','8','Тип','');

INSERT INTO `modx_a_filter_strings` VALUES ('38','en','8','Type','');

INSERT INTO `modx_a_filter_strings` VALUES ('39','ua','8','Тип','');

INSERT INTO `modx_a_filter_strings` VALUES ('40','ru','9','test','test');

INSERT INTO `modx_a_filter_strings` VALUES ('41','en','9','test','test');

INSERT INTO `modx_a_filter_strings` VALUES ('42','ua','9','test','test');

INSERT INTO `modx_a_filter_strings` VALUES ('43','ru','1','Батарея','Емкость аккумуляторной батареи');

INSERT INTO `modx_a_filter_strings` VALUES ('44','ua','1','Батарея','Ємність акумуляторної батареї');

INSERT INTO `modx_a_filter_strings` VALUES ('49','ua','10','Короб металл / глубина (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('50','ru','10','Короб металл / глубина (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('51','ua','11','Нижний замок','');

INSERT INTO `modx_a_filter_strings` VALUES ('52','ru','11','Нижний замок','');

INSERT INTO `modx_a_filter_strings` VALUES ('55','ua','12','Утепление полотна, минвата «Isover» (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('56','ru','12','Утепление полотна, минвата «Isover» (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('57','ua','13','Внешняя накладка (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('58','ru','13','Внешняя накладка (мм)','');

INSERT INTO `modx_a_filter_strings` VALUES ('59','ua','14','Глазок','');

INSERT INTO `modx_a_filter_strings` VALUES ('60','ru','14','Глазок','');

INSERT INTO `modx_a_filter_strings` VALUES ('61','ua','15','ширина полотна','');

INSERT INTO `modx_a_filter_strings` VALUES ('62','ru','15','ширина полотна','');

INSERT INTO `modx_a_filter_strings` VALUES ('63','ua','16','Коробка полотна','');

INSERT INTO `modx_a_filter_strings` VALUES ('64','ru','16','Коробка полотна','');

INSERT INTO `modx_a_filter_strings` VALUES ('69','ua','17','Наличник','');

INSERT INTO `modx_a_filter_strings` VALUES ('70','ru','17','Наличник','');

INSERT INTO `modx_a_filter_strings` VALUES ('71','ua','18','Коллекция','');

INSERT INTO `modx_a_filter_strings` VALUES ('72','ru','18','Коллекция','');

INSERT INTO `modx_a_filter_strings` VALUES ('73','ua','19','Модель','');

INSERT INTO `modx_a_filter_strings` VALUES ('74','ru','19','Модель','');

INSERT INTO `modx_a_filter_strings` VALUES ('75','ua','20','тестовый фильтр','');

INSERT INTO `modx_a_filter_strings` VALUES ('76','ru','20','тестовый фильтр','');

INSERT INTO `modx_a_filter_strings` VALUES ('77','ua','21','Тест фильтр характиристика моно выбор','Описание Тест фильтр характиристика моно выбор');

INSERT INTO `modx_a_filter_strings` VALUES ('78','ru','21','Тест фильтр характиристика моно выбор','Описание Тест фильтр характиристика моно выбор');

INSERT INTO `modx_a_filter_strings` VALUES ('79','ua','22','Тип покриття',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('80','ru','22','Тип покрытия',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('81','ua','23','Виробник',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('82','ru','23','Производитель',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('91','ua','28','Доборная планка','');

INSERT INTO `modx_a_filter_strings` VALUES ('92','ru','28','Доборная планка','');

INSERT INTO `modx_a_filter_strings` VALUES ('93','ua','29','Товщина сталі',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('94','ru','29','Толщина стали',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('95','ua','30','Фарба',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('96','ru','30','Краска',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('97','ua','31','Кольор грунту','');

INSERT INTO `modx_a_filter_strings` VALUES ('98','ru','31','Цвет грунта','');

INSERT INTO `modx_a_filter_strings` VALUES ('101','ua','32','Производитель',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('102','ru','32','virobnik',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('143','ua','36','Нижній замок / накладка',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('144','ru','36','Нижний замок / накладка',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('145','ua','37','Девіатор','');

INSERT INTO `modx_a_filter_strings` VALUES ('146','ru','37','Девиатор','');

INSERT INTO `modx_a_filter_strings` VALUES ('147','ua','38','Створка',NULL);

INSERT INTO `modx_a_filter_strings` VALUES ('148','ru','38','Створка',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_filter_values`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_filter_values`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_filter_values` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `filter_url` varchar(128) NOT NULL,
  `filter_name` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `filter_value_num` float DEFAULT NULL,
  `filter_value_ru` varchar(128) NOT NULL,
  `filter_value_ua` varchar(128) NOT NULL,
  `filter_value_en` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  UNIQUE KEY `filter_url` (`filter_url`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_filter_values`
#

INSERT INTO `modx_a_filter_values` VALUES ('1','2','modern','','',NULL,'Модерн','Модерн','Modern');

INSERT INTO `modx_a_filter_values` VALUES ('2','2','classic','','',NULL,'Классический','Класичний','Classic');

INSERT INTO `modx_a_filter_values` VALUES ('3','1','153-mah','','',NULL,'153 mAh','153 mAh','');

INSERT INTO `modx_a_filter_values` VALUES ('4','1','3500-mah','','',NULL,'3500 мАч','3500 мАг','');

INSERT INTO `modx_a_filter_values` VALUES ('5','2','wei-shi','','',NULL,'Wei Shi','Wei Shi','Wei Shi');

INSERT INTO `modx_a_filter_values` VALUES ('6','3','110-lm',NULL,NULL,NULL,'110 Лм','110 Лм','110 Lm');

INSERT INTO `modx_a_filter_values` VALUES ('7','4','no',NULL,NULL,NULL,'Нет','Ні','No');

INSERT INTO `modx_a_filter_values` VALUES ('8','4','yes',NULL,NULL,NULL,'Да','Так','Yes');

INSERT INTO `modx_a_filter_values` VALUES ('9','5','10-cm','',NULL,NULL,'10 см','10 см','10 cm');

INSERT INTO `modx_a_filter_values` VALUES ('10','5','150-cm','',NULL,NULL,'150 см','150 см','150 cm');

INSERT INTO `modx_a_filter_values` VALUES ('12','2','minimalism','','',NULL,'Минимализм','Мінімалізм','Minimalism');

INSERT INTO `modx_a_filter_values` VALUES ('13','1','100-years','','',NULL,'100 лет','100 років','');

INSERT INTO `modx_a_filter_values` VALUES ('15','3','dsgsdg','',NULL,NULL,'dsgsdg','sdgdsg','dfhdfh');

INSERT INTO `modx_a_filter_values` VALUES ('16','2','contry','','',NULL,'Кантри','Кантрі','Contry');

INSERT INTO `modx_a_filter_values` VALUES ('17','2','provans','','',NULL,'Прованс','Прованс','Provans');

INSERT INTO `modx_a_filter_values` VALUES ('22','6','dom-uyut','','',NULL,'Дом-уют','Дім-уют','');

INSERT INTO `modx_a_filter_values` VALUES ('23','7','250x180','','',NULL,'250x180','250x180','250x180');

INSERT INTO `modx_a_filter_values` VALUES ('24','7','300x220','','',NULL,'300x220','300x220','300x220');

INSERT INTO `modx_a_filter_values` VALUES ('25','7','200x220','','',NULL,'200x220','200x220','200x220');

INSERT INTO `modx_a_filter_values` VALUES ('26','8','trehspalnye-krovati','','',NULL,'Трехспальные кровати','Трьохспальні ліжка','');

INSERT INTO `modx_a_filter_values` VALUES ('27','8','dvuspalnye-krovati','','',NULL,'Двуспальные кровати','двоспальні ліжка','');

INSERT INTO `modx_a_filter_values` VALUES ('28','9','novyj-cvet','','0397253001567438004.jpg',NULL,'новый цвет','новый цвет','новый цвет');

INSERT INTO `modx_a_filter_values` VALUES ('29','6','qwe','','',NULL,'QWE','QWE','');

INSERT INTO `modx_a_filter_values` VALUES ('30','6','straj','','',NULL,'Страж','Страж','');

INSERT INTO `modx_a_filter_values` VALUES ('31','6','raysharp','','',NULL,'Raysharp','Raysharp','');

INSERT INTO `modx_a_filter_values` VALUES ('32','10','1-8-145','','',NULL,'1,8 / 145','1,8 / 145','');

INSERT INTO `modx_a_filter_values` VALUES ('33','10','1-8-120','','',NULL,'1,8 / 120','1,8 / 120','');

INSERT INTO `modx_a_filter_values` VALUES ('34','11','mottura-54.797-matic','','',NULL,'Mottura 54.797 Matic','Mottura 54.797 Matic','');

INSERT INTO `modx_a_filter_values` VALUES ('35','11','mottura-54.797','','',NULL,'Mottura 54.797','Mottura 54.797','');

INSERT INTO `modx_a_filter_values` VALUES ('36','11','mul-t-lock-354m','','',NULL,'Mul-t-lock 354M','Mul-t-lock 354M','');

INSERT INTO `modx_a_filter_values` VALUES ('37','12','50','','',NULL,'50','50','');

INSERT INTO `modx_a_filter_values` VALUES ('38','13','12-mdfpvh','','',NULL,'12 МДФ/ПВХ','12 МДФ/ПВХ','');

INSERT INTO `modx_a_filter_values` VALUES ('39','14','armadillo','','',NULL,'Armadillo','Armadillo','');

INSERT INTO `modx_a_filter_values` VALUES ('40','14','apecs','','',NULL,'Apecs','Apecs','');

INSERT INTO `modx_a_filter_values` VALUES ('41','15','900','','',NULL,'900','900','');

INSERT INTO `modx_a_filter_values` VALUES ('42','15','800','','',NULL,'800','800','');

INSERT INTO `modx_a_filter_values` VALUES ('43','15','700','','',NULL,'700','700','');

INSERT INTO `modx_a_filter_values` VALUES ('44','15','600','','',NULL,'600','600','');

INSERT INTO `modx_a_filter_values` VALUES ('45','16','mdf-8032-mm-s-uplotn.-k-t-418-uah','','',NULL,'мдф 80*32 мм с уплотн. к-т (+418 UAH)','мдф 80*32 мм с уплотн. к-т (+418 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('46','16','derevo-8032-mm-s-uplotn.-k-t-685-uah','','',NULL,'дерево 80*32 мм с уплотн. к-т (+685 UAH)','дерево 80*32 мм с уплотн. к-т (+685 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('47','16','derevo-10032-mm-komplekt-716-uah','','',NULL,'дерево 100*32 мм комплект (+716 UAH)','дерево 100*32 мм комплект (+716 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('48','16','derevo-8032-mm-komplekt-665-uah','','',NULL,'дерево 80*32 мм комплект (+665 UAH)','дерево 80*32 мм комплект (+665 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('49','16','ne-nuzhna','','',NULL,'не нужна','не нужна','');

INSERT INTO `modx_a_filter_values` VALUES ('50','17','70-mm-dva-komplekta-558-uah','','',NULL,'70 мм два комплекта (+558 UAH)','70 мм два комплекта (+558 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('51','17','70-mm-odin-komplekt-279-uah','','',NULL,'70 мм один комплект (+279 UAH)','70 мм один комплект (+279 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('52','17','64-mm-odin-komplekt-253-uah','','',NULL,'64 мм один комплект (+253 UAH)','64 мм один комплект (+253 UAH)','');

INSERT INTO `modx_a_filter_values` VALUES ('53','17','ne-nuzhen','','',NULL,'не нужен','не нужен','');

INSERT INTO `modx_a_filter_values` VALUES ('54','18','kolori','','',NULL,'Колори','Колори','');

INSERT INTO `modx_a_filter_values` VALUES ('55','18','kvadra','','',NULL,'Квадра','Квадра','');

INSERT INTO `modx_a_filter_values` VALUES ('56','18','intera','','',NULL,'Интера','Интера','');

INSERT INTO `modx_a_filter_values` VALUES ('57','19','antre','','',NULL,'Антре','Антре','');

INSERT INTO `modx_a_filter_values` VALUES ('58','19','villa','','',NULL,'Вилла','Вилла','');

INSERT INTO `modx_a_filter_values` VALUES ('59','19','rokka','','',NULL,'Рокка','Рокка','');

INSERT INTO `modx_a_filter_values` VALUES ('60','20','check','','',NULL,'тест','тест','');

INSERT INTO `modx_a_filter_values` VALUES ('62','22','farba',NULL,NULL,NULL,'Краска','Фарба',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('63','23','fortis',NULL,NULL,NULL,'Фортис','Фортіс',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('67','22','shpon-duba',NULL,NULL,NULL,' Шпон дуба','Шпон дуба',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('68','22','plastik',NULL,NULL,NULL,' Пластик',' Пластик',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('69','22','plivka-pvh',NULL,NULL,NULL,' Пленка ПВХ',' Плівка ПВХ',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('70','28','120','','',NULL,'120','120','');

INSERT INTO `modx_a_filter_values` VALUES ('71','28','55','','',NULL,'55','55','');

INSERT INTO `modx_a_filter_values` VALUES ('72','29','15mm',NULL,NULL,NULL,' 1,5мм',' 1,5мм',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('73','30','guma',NULL,NULL,NULL,' Ризина',' Гума',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('74','31','sirij','','',NULL,' Серый',' Сірій','');

INSERT INTO `modx_a_filter_values` VALUES ('76','29','2mm',NULL,NULL,NULL,' 2мм',' 2мм',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('77','30','emal',NULL,NULL,NULL,' Эмаль',' Емаль',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('78','31','bilij','','',NULL,'Белый','Білий','');

INSERT INTO `modx_a_filter_values` VALUES ('80','32','',NULL,NULL,NULL,' fortis','Фортис',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('81','33','1111111111','','',NULL,'111111111','1111111111','');

INSERT INTO `modx_a_filter_values` VALUES ('82','33','2222222222','','',NULL,'222222222','22222','');

INSERT INTO `modx_a_filter_values` VALUES ('84','21','russkie-bukvy','','',NULL,'фильтр1','фильтр1','');

INSERT INTO `modx_a_filter_values` VALUES ('85','29','1-5mm',NULL,NULL,NULL,' 1,5мм',' 1,5мм',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('87','35','1.8-100',NULL,NULL,NULL,' 1.8 / 100',' 1.8 / 100',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('88','36','fuaro-v25-pro',NULL,NULL,NULL,' Fuaro V25 Pro',' Fuaro V25 Pro',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('89','37','1','','',NULL,' -',' -','');

INSERT INTO `modx_a_filter_values` VALUES ('90','38','ordinarni',NULL,NULL,NULL,' Ординарные',' Ординарні',NULL);

INSERT INTO `modx_a_filter_values` VALUES ('94','41','1-8-1451','','',NULL,'','1,8 / 145','');

INSERT INTO `modx_a_filter_values` VALUES ('95','41','1.8130','','',NULL,'','1.8/130','');

INSERT INTO `modx_a_filter_values` VALUES ('98','37','mottura-2-sht','','',NULL,'Mottura 2 шт.','Mottura 2 шт.','');

INSERT INTO `modx_a_filter_values` VALUES ('99','37','mottura','','',NULL,'Mottura','Mottura','');

INSERT INTO `modx_a_filter_values` VALUES ('100','37','fuaro','','',NULL,'Fuaro','Fuaro','');

INSERT INTO `modx_a_filter_values` VALUES ('102','10','1.81301','','',NULL,'1.8/130','1.8/130','');


# --------------------------------------------------------

#
# Table structure for table `modx_a_filters`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_filters`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_filters` (
  `filter_id` int(10) NOT NULL AUTO_INCREMENT,
  `filter_feature` tinyint(4) NOT NULL DEFAULT '1',
  `filter_type` tinyint(4) NOT NULL DEFAULT '1',
  `filter_url` varchar(512) NOT NULL,
  `filter_product_active` tinyint(1) DEFAULT NULL,
  `filter_seo_active` tinyint(1) DEFAULT NULL,
  `filter_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `filter_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_filters`
#

INSERT INTO `modx_a_filters` VALUES ('2','1','5','brand','1','1','2017-11-02 13:30:00','2019-09-09 11:29:48');

INSERT INTO `modx_a_filters` VALUES ('5','1','5','length',NULL,NULL,'2017-11-02 13:30:00','2019-07-25 11:30:30');

INSERT INTO `modx_a_filters` VALUES ('6','0','5','proizvoditel','0','0','2019-07-25 13:33:22','2019-11-03 11:32:07');

INSERT INTO `modx_a_filters` VALUES ('7','1','5','spalnyj-razmer','1','1','2019-07-25 14:00:53','2019-09-09 12:00:55');

INSERT INTO `modx_a_filters` VALUES ('8','1','5','tip','0','0','2019-07-28 17:36:25','2019-10-09 13:06:48');

INSERT INTO `modx_a_filters` VALUES ('9','0','5','test','1',NULL,'2019-09-02 15:25:50','2019-09-02 15:26:46');

INSERT INTO `modx_a_filters` VALUES ('10','1','5','korob-metall-glubina-mm','0','0','2019-10-14 13:00:56','2019-11-03 13:03:31');

INSERT INTO `modx_a_filters` VALUES ('11','1','5','nizhnij-zamok','0','0','2019-10-14 13:04:26','2019-10-14 13:04:44');

INSERT INTO `modx_a_filters` VALUES ('12','1','5','uteplenie-polotna-minvata-isover-mm','0','0','2019-10-14 13:05:35','2019-10-14 13:05:35');

INSERT INTO `modx_a_filters` VALUES ('13','1','5','vneshnyaya-nakladka-mm','0','0','2019-10-14 13:06:30','2019-10-14 13:06:30');

INSERT INTO `modx_a_filters` VALUES ('14','1','6','glazok','0','0','2019-10-14 13:08:09','2019-11-03 00:11:34');

INSERT INTO `modx_a_filters` VALUES ('15','1','5','shirina-polotna','0','0','2019-10-14 13:25:18','2019-10-14 13:25:18');

INSERT INTO `modx_a_filters` VALUES ('16','1','5','korobka-polotna','0','0','2019-10-14 13:26:07','2019-10-14 13:27:46');

INSERT INTO `modx_a_filters` VALUES ('17','1','5','nalichnik','0','0','2019-10-14 13:29:18','2019-10-14 13:29:18');

INSERT INTO `modx_a_filters` VALUES ('18','0','5','kollekciya','0','0','2019-10-14 13:30:43','2019-10-14 13:30:43');

INSERT INTO `modx_a_filters` VALUES ('19','0','5','model','0','0','2019-10-14 13:31:55','2019-10-14 13:31:55');

INSERT INTO `modx_a_filters` VALUES ('20','1','5','testovyj-filtr','0','0','2019-10-15 11:09:33','2019-10-15 11:09:33');

INSERT INTO `modx_a_filters` VALUES ('21','0','5','test-filtr-haraktiristika-mono-vybor','0','0','2019-10-15 17:53:57','2019-11-01 11:56:26');

INSERT INTO `modx_a_filters` VALUES ('22','1','5','tippocrittya',NULL,NULL,'2019-10-16 16:44:02','2019-10-16 16:44:02');

INSERT INTO `modx_a_filters` VALUES ('23','0','5','virobnik',NULL,NULL,'2019-10-16 16:44:31','2019-10-16 16:44:31');

INSERT INTO `modx_a_filters` VALUES ('28','1','5','dobornaya-planka','0','0','2019-10-17 10:29:54','2019-10-17 10:29:54');

INSERT INTO `modx_a_filters` VALUES ('29','1','5','tovshchina-stali',NULL,NULL,'2019-10-17 15:37:48','2019-10-17 15:37:48');

INSERT INTO `modx_a_filters` VALUES ('30','1','5','farba',NULL,NULL,'2019-10-17 15:37:48','2019-10-17 15:37:48');

INSERT INTO `modx_a_filters` VALUES ('31','0','5','coler-gruntu','0','0','2019-10-17 15:37:48','2019-10-18 09:56:35');

INSERT INTO `modx_a_filters` VALUES ('32','0','5','',NULL,NULL,'2019-10-21 13:43:21','2019-10-21 13:43:21');

INSERT INTO `modx_a_filters` VALUES ('36','1','5','nizhnij-zamok-nakladka',NULL,NULL,'2019-11-03 10:30:23','2019-11-03 10:30:23');

INSERT INTO `modx_a_filters` VALUES ('37','1','5','deviator','0','0','2019-11-03 10:30:25','2019-11-03 12:58:53');

INSERT INTO `modx_a_filters` VALUES ('38','0','5','stvorka',NULL,NULL,'2019-11-03 10:30:26','2019-11-03 10:30:26');


# --------------------------------------------------------

#
# Table structure for table `modx_a_images`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_images`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL DEFAULT '0',
  `file` varchar(64) NOT NULL,
  `type` varchar(7) NOT NULL DEFAULT 'image',
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `image_parent_image_file` (`parent`,`file`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_images`
#

INSERT INTO `modx_a_images` VALUES ('5','6','e5eb5dc8bc7fcc223b2efbae1516b7f6.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('6','6','30f4a5de5353de5cdfad901f90c6c230.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('7','7','4c2ddb1c0f3cf5a33689e8adf5707123.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('8','7','d08bb60e7f1f00c88bd4448b0b39f928.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('9','8','d07064b5a5400e14e8629173312e0d2f.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('10','8','1d7765a50beac52dabf07e86c27a4d30.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('11','8','b8559d7b34c84a5638c59dcc793f8f1b.jpeg','image','2');

INSERT INTO `modx_a_images` VALUES ('12','9','c6c1663c46711e1b4fcf67b64f664551.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('13','10','8d70029d556e8d10b814f354084d9733.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('14','10','538346d3e3ccc598a0a0a1da1fa8e13c.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('15','10','1b93dd5f1379e43e76da13e0d090ecc0.jpeg','image','2');

INSERT INTO `modx_a_images` VALUES ('16','11','a0a1b425a1f9a8b4f8367662eb44d252.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('17','11','7884db6644883ccaf40d74dff44abf3c.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('18','12','0f28d2419a087c2e9432d52031a008f0.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('19','12','3474d0a96128999b0ee6e612d7433830.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('20','12','d742d71f6e192d9be1d4db8915d89d2d.jpeg','image','2');

INSERT INTO `modx_a_images` VALUES ('21','12','49ab416997ae9ed42a853abd38fc6f6e.jpeg','image','3');

INSERT INTO `modx_a_images` VALUES ('22','12','e0bb369cde2797d582d0927f21f0c549.jpeg','image','4');

INSERT INTO `modx_a_images` VALUES ('23','13','ac0b03a35d3510bb25986ea03aaef10a.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('24','13','97200d0b074336598f5b6417d77cb831.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('25','13','cb1d597c2984cca33b487f8a34bc5aaa.jpeg','image','2');

INSERT INTO `modx_a_images` VALUES ('26','14','e7c68c053827fbc4988d5c0c1522fa76.jpeg','image','0');

INSERT INTO `modx_a_images` VALUES ('27','14','1cd665222c3078c771055ed62bb0590c.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('28','14','be82208e5622f8ef589ed5b11f63e750.jpeg','image','2');

INSERT INTO `modx_a_images` VALUES ('31','1','b55f8afe11decd890a3565986d81d92b.jpeg','image','1');

INSERT INTO `modx_a_images` VALUES ('59','1','48e6bc83bd95bbe549b4ded20fc94d61.jpeg','image','0');


# --------------------------------------------------------

#
# Table structure for table `modx_a_images_fields`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_images_fields`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_images_fields` (
  `rid` int(11) NOT NULL,
  `lang` varchar(16) NOT NULL DEFAULT 'ru',
  `alt` varchar(128) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `link` varchar(512) DEFAULT NULL,
  UNIQUE KEY `image_image_id` (`rid`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_images_fields`
#

INSERT INTO `modx_a_images_fields` VALUES ('31','ru','Входные двери','Входные двери','Цельнометаллические входные двери Proof от 6001 грн','21');

INSERT INTO `modx_a_images_fields` VALUES ('31','ua','Вхідні двері','Вхідні двері','Суцільнометалеві вхідні двері Proof від 6001 грн','21');

INSERT INTO `modx_a_images_fields` VALUES ('59','ru','Вхідні двері','Вхідні двері','Цельнометаллические входные двери Proof со скидкой 5%','http://dverigarant.app.artjoker.ua/ru/dlya-doma/');

INSERT INTO `modx_a_images_fields` VALUES ('59','ua','Вхідні двері','Вхідні двері','Тест Суцільнометалеві вхідні двері Proof зі знижкою 5%','http://dverigarant.app.artjoker.ua/ua/dlya-doma/');


# --------------------------------------------------------

#
# Table structure for table `modx_a_lang`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_lang`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `ua` text COMMENT 'ua',
  `ru` text COMMENT 'ru',
  `en` text COMMENT 'en',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_lang`
#

INSERT INTO `modx_a_lang` VALUES ('1','Для повної функціональності цього сайту необхідно включити JavaScript. Ось','Для полной функциональности этого сайта необходимо включить JavaScript. Вот','For the full functionality of this site, you must enable JavaScript. Here are the');

INSERT INTO `modx_a_lang` VALUES ('2','інструкції','инструкции','instructions');

INSERT INTO `modx_a_lang` VALUES ('3',', як увімкнути JavaScript у вашому браузері.',', как включить JavaScript в вашем браузере.',', how to enable JavaScript in your browser.');

INSERT INTO `modx_a_lang` VALUES ('4','Ваш браузер застарів, будь ласка &lt;b&gt; поновіть &lt;/ b&gt; його.','Ваш браузер устарел, пожалуйста &lt;b&gt;обновите&lt;/b&gt; его.','Your browser is out of date, please &lt;b&gt; update &lt;/ b&gt; it.');

INSERT INTO `modx_a_lang` VALUES ('5','Фільтр','Фильтр','Filters');

INSERT INTO `modx_a_lang` VALUES ('6','Ціна','Цена','Price');

INSERT INTO `modx_a_lang` VALUES ('7','товарів','товаров','Items');

INSERT INTO `modx_a_lang` VALUES ('8','товар','товар','Item');

INSERT INTO `modx_a_lang` VALUES ('9','товара','товара','Items');

INSERT INTO `modx_a_lang` VALUES ('10','Увійти','Войти','Login');

INSERT INTO `modx_a_lang` VALUES ('11','Зареєструватися','Зарегистрироваться','Register');

INSERT INTO `modx_a_lang` VALUES ('12','Користувача з таким Email вже зареєстровано','Пользователь с таким Email уже зарегистрирован','Email already registered');

INSERT INTO `modx_a_lang` VALUES ('13','Користувача не знайдено','Пользователь не найден','User is not found');

INSERT INTO `modx_a_lang` VALUES ('14','Невірний логін або пароль','Неверный логин или пароль','Wrong login or password');

INSERT INTO `modx_a_lang` VALUES ('15','Пароль','Пароль','Password');

INSERT INTO `modx_a_lang` VALUES ('16','Користувача успішно зареєстровано','Пользователь успешно зарегистрирован','User successfully registered');

INSERT INTO `modx_a_lang` VALUES ('17','Введіть коректний Email','Введите корректный Email','Please enter a valid Email');

INSERT INTO `modx_a_lang` VALUES ('18','Запам&#039;ятати мене','Запомнить меня','Remember me');

INSERT INTO `modx_a_lang` VALUES ('19','Дякуємо!','Спасибо!','Thank you!');

INSERT INTO `modx_a_lang` VALUES ('20','Операція пройшла успішно.','Операция выполнена успешно.','The operation was successful.');

INSERT INTO `modx_a_lang` VALUES ('21','Увага!','Внимание!','Attention!');

INSERT INTO `modx_a_lang` VALUES ('22','Щось пішло не так.','Что-то пошло не так.','Something went wrong.');

INSERT INTO `modx_a_lang` VALUES ('23','Вихід','Выход','Logout');

INSERT INTO `modx_a_lang` VALUES ('24','Забули пароль?','Забыли пароль?','Forgot your password?');

INSERT INTO `modx_a_lang` VALUES ('25','Введіть ваш E-mail','Введіть ваш E-mail','Введіть ваш E-mail');

INSERT INTO `modx_a_lang` VALUES ('26','Відправити','Отправить','Send');

INSERT INTO `modx_a_lang` VALUES ('27','На Ваш email відправлені інструкції.','На Ваш email отправлены инструкции.','Your instructions have been sent to your email.');

INSERT INTO `modx_a_lang` VALUES ('28','Ваш пароль успішно відновлено. Увійдіть використовуючи дані з Email.','Ваш пароль успешно восстановлен. Войдите используя данные из Email.','Your password has been successfully restored. Log in using data from Email.');

INSERT INTO `modx_a_lang` VALUES ('29','сторінка-','страница-','page-');

INSERT INTO `modx_a_lang` VALUES ('30','Користувача заблоковано','Пользователь заблокирован','User is blocked');

INSERT INTO `modx_a_lang` VALUES ('31','Придбати в один дотик','Купить в один клик','Buy click');

INSERT INTO `modx_a_lang` VALUES ('32','Ім&#039;я','Имя','Name');

INSERT INTO `modx_a_lang` VALUES ('33','Телефон','Телефон','Phone');

INSERT INTO `modx_a_lang` VALUES ('34','E-mail','E-mail','E-mail');

INSERT INTO `modx_a_lang` VALUES ('35','Придбати','Купить','Buy');

INSERT INTO `modx_a_lang` VALUES ('36','Ваша заявка прийнята! Наш менеджер зв&#039;яжеться з вами найближчим часом!','Ваша заявка принята! Наш менеджер свяжется с вами в ближайшее время!','Your application is accepted! Our manager will contact you soon!');

INSERT INTO `modx_a_lang` VALUES ('37','м. Київ, вул. Урлівська, 1/8','г. Киев, ул. Урловская, 1/8',NULL);

INSERT INTO `modx_a_lang` VALUES ('38','Введіть запит для пошуку','Введите запрос для поиска',NULL);

INSERT INTO `modx_a_lang` VALUES ('39','Безкоштовний виклик замірника','Бесплатный вызов замерщика',NULL);

INSERT INTO `modx_a_lang` VALUES ('40','Передзвоніть мені','Перезвоните мне',NULL);

INSERT INTO `modx_a_lang` VALUES ('41','Службова інформація','Служебная информация',NULL);

INSERT INTO `modx_a_lang` VALUES ('42','Розробка і підтримка сайту','Разработка и поддержка сайта',NULL);

INSERT INTO `modx_a_lang` VALUES ('43','Працюємо по Києву і області','Работаем по Киеву и области',NULL);

INSERT INTO `modx_a_lang` VALUES ('44','ПН-ПТ з 9 до 18','ПН-ПТ с 9 до 18',NULL);

INSERT INTO `modx_a_lang` VALUES ('45','СБ з 10 до 17','СБ с 10 до 17',NULL);

INSERT INTO `modx_a_lang` VALUES ('46','НД вихідний','ВС выходной',NULL);

INSERT INTO `modx_a_lang` VALUES ('47','Україна, м Київ','Украина, г. Киев',NULL);

INSERT INTO `modx_a_lang` VALUES ('48','вул. Урлівська, 1/8','ул. Урловская, 1/8',NULL);

INSERT INTO `modx_a_lang` VALUES ('49','ст.м. Назва','ст.м. Название',NULL);

INSERT INTO `modx_a_lang` VALUES ('50','Поле &quot;Ім&#039;я&quot; не заповнено','Поле &quot;Имя&quot; не заполнено',NULL);

INSERT INTO `modx_a_lang` VALUES ('51','Ваше питання або повідомлення','Ваш вопрос или сообщение',NULL);

INSERT INTO `modx_a_lang` VALUES ('52','Введіть достатню кількість символів','Введите достаточное количество символов',NULL);

INSERT INTO `modx_a_lang` VALUES ('53','грн','грн',NULL);

INSERT INTO `modx_a_lang` VALUES ('54','від','от',NULL);

INSERT INTO `modx_a_lang` VALUES ('55','10.00 до 19.00','10.00 до 19.00',NULL);

INSERT INTO `modx_a_lang` VALUES ('56','10.00 до 16.00','10.00 до 16.00',NULL);

INSERT INTO `modx_a_lang` VALUES ('57','пн-пт','пн-пт',NULL);

INSERT INTO `modx_a_lang` VALUES ('58','сб-нд','сб-вс',NULL);

INSERT INTO `modx_a_lang` VALUES ('59','Корисні статті','Полезные статьи',NULL);

INSERT INTO `modx_a_lang` VALUES ('60','Час проведення акції: з','Время проведения акции: с ',NULL);

INSERT INTO `modx_a_lang` VALUES ('61','До закінчення акції','До окончания акции',NULL);

INSERT INTO `modx_a_lang` VALUES ('62','Подивитися акційні товари','Посмотреть акционные товары',NULL);

INSERT INTO `modx_a_lang` VALUES ('63','день','день',NULL);

INSERT INTO `modx_a_lang` VALUES ('64','дня','дня',NULL);

INSERT INTO `modx_a_lang` VALUES ('65','днів','дней',NULL);

INSERT INTO `modx_a_lang` VALUES ('66','по','до',NULL);

INSERT INTO `modx_a_lang` VALUES ('67','Нові надходження','Новые поступления',NULL);

INSERT INTO `modx_a_lang` VALUES ('68','Акційні товари','Акционные товары',NULL);

INSERT INTO `modx_a_lang` VALUES ('69','Найпопулярніші','Самые популярные',NULL);

INSERT INTO `modx_a_lang` VALUES ('70','Товари з цієї статті','Товары из этой статьи',NULL);

INSERT INTO `modx_a_lang` VALUES ('71','Показати/Приховати текст','Показать/Скрыть текст',NULL);

INSERT INTO `modx_a_lang` VALUES ('72','Акція','Акция',NULL);

INSERT INTO `modx_a_lang` VALUES ('73','Знижка','Скидка',NULL);

INSERT INTO `modx_a_lang` VALUES ('74','Хіт','Хит',NULL);

INSERT INTO `modx_a_lang` VALUES ('75','Максимальна ціна','Максимальная цена',NULL);

INSERT INTO `modx_a_lang` VALUES ('76','Скинути фільтри','Сбросить фильтры',NULL);

INSERT INTO `modx_a_lang` VALUES ('77','За ціною','По цене',NULL);

INSERT INTO `modx_a_lang` VALUES ('78','За зменшенням ціни','По убыванию цены',NULL);

INSERT INTO `modx_a_lang` VALUES ('79','За зростанням ціни','По возрастанию цены',NULL);

INSERT INTO `modx_a_lang` VALUES ('80','За новизною','По новизне',NULL);

INSERT INTO `modx_a_lang` VALUES ('81','За популярністю','По популярности',NULL);

INSERT INTO `modx_a_lang` VALUES ('82','Надішліть заявку на замір дверних прорізів. Вам передзвонить менеджер і уточнить час і адресу. Наш фахівець перевірить умови монтажу дверей і проконсультує з питань вибору','Отправьте заявку на замер дверных проемов. Вам перезвонит менеджер и уточнит время и адрес. Наш специалист проверит условия монтажа дверей и проконсультирует по вопросам выбора',NULL);

INSERT INTO `modx_a_lang` VALUES ('83','Номер телефону','Номер телефона',NULL);

INSERT INTO `modx_a_lang` VALUES ('84','Виберіть район Києва','Выберите район Киева',NULL);

INSERT INTO `modx_a_lang` VALUES ('85','Назва району','Название района',NULL);

INSERT INTO `modx_a_lang` VALUES ('86','Це поле не повинно бути порожнім','Это поле не должно быть пустым',NULL);

INSERT INTO `modx_a_lang` VALUES ('87','Передзвонити','Перезвонить',NULL);

INSERT INTO `modx_a_lang` VALUES ('88','Замовити дзвінок','Заказать звонок',NULL);

INSERT INTO `modx_a_lang` VALUES ('89','Ваша заявка відправлена','Ваша заявка отправлена',NULL);

INSERT INTO `modx_a_lang` VALUES ('90','Ваш заявка на виклик замірника відправлена ​​на обробку. З вами зв&#039;яжеться наш менеджер для уточнення деталей','Ваш заявка на вызов замерщика отправлена на обработку. С вами свяжется наш менеджер для уточнения деталей',NULL);

INSERT INTO `modx_a_lang` VALUES ('91','Продовжити','Продолжить',NULL);

INSERT INTO `modx_a_lang` VALUES ('92','Ваш заявка на зворотний дзвінок відправлена ​​на обробку.','Ваш заявка на обратный звонок отправлена на обработку.',NULL);

INSERT INTO `modx_a_lang` VALUES ('93','Виробники','Производители',NULL);

INSERT INTO `modx_a_lang` VALUES ('94','В кошику','В корзине',NULL);

INSERT INTO `modx_a_lang` VALUES ('95','Викликати замірника','Вызвать замерщика',NULL);

INSERT INTO `modx_a_lang` VALUES ('96','Додати коментар до замовлення','Добавить комментарий к заказу',NULL);

INSERT INTO `modx_a_lang` VALUES ('97','Текст повідомлення','Текст сообщения',NULL);

INSERT INTO `modx_a_lang` VALUES ('98','Підсумкова вартість','Итоговая стоимость',NULL);

INSERT INTO `modx_a_lang` VALUES ('99','Опис','Описание',NULL);

INSERT INTO `modx_a_lang` VALUES ('100','Характеристики','Характеристики',NULL);

INSERT INTO `modx_a_lang` VALUES ('101','Оплата і доставка','Оплата и доставка',NULL);

INSERT INTO `modx_a_lang` VALUES ('102','Схожі товари','Похожие товары',NULL);

INSERT INTO `modx_a_lang` VALUES ('103','Відповідні по стилю і дизайну вхідні двері','Подходящие по стилю и дизайну входные двери',NULL);

INSERT INTO `modx_a_lang` VALUES ('104','Продовжити покупки','Продолжить покупки',NULL);

INSERT INTO `modx_a_lang` VALUES ('105','Разом','Итого',NULL);

INSERT INTO `modx_a_lang` VALUES ('106','Оформити замовлення','Оформить заказ',NULL);

INSERT INTO `modx_a_lang` VALUES ('107','Артикул','Артикул',NULL);

INSERT INTO `modx_a_lang` VALUES ('108','Інформація для оформлення замовлення','Информация для оформления заказа',NULL);

INSERT INTO `modx_a_lang` VALUES ('109','Район','Район',NULL);

INSERT INTO `modx_a_lang` VALUES ('110','Додаткові послуги','Дополнительные услуги',NULL);

INSERT INTO `modx_a_lang` VALUES ('111','Доставка','Доставка',NULL);

INSERT INTO `modx_a_lang` VALUES ('112','Установка','Установка',NULL);

INSERT INTO `modx_a_lang` VALUES ('113','підтвердити замовлення','подтвердить заказ',NULL);

INSERT INTO `modx_a_lang` VALUES ('114','Так','Да',NULL);

INSERT INTO `modx_a_lang` VALUES ('115','В заказі','В заказе',NULL);

INSERT INTO `modx_a_lang` VALUES ('116','на','на',NULL);

INSERT INTO `modx_a_lang` VALUES ('117','Ні','Нет',NULL);

INSERT INTO `modx_a_lang` VALUES ('118','По вашому запиту','По вашему запросу',NULL);

INSERT INTO `modx_a_lang` VALUES ('119','знайдені наступні товари','найдены следующие товары',NULL);

INSERT INTO `modx_a_lang` VALUES ('120','Знайдено в категоріях','Найдено в категориях',NULL);

INSERT INTO `modx_a_lang` VALUES ('121','Усі','Все',NULL);

INSERT INTO `modx_a_lang` VALUES ('122','Показано','Показано',NULL);

INSERT INTO `modx_a_lang` VALUES ('123','Фільтри','Фильтры',NULL);

INSERT INTO `modx_a_lang` VALUES ('124','Показати всі характеристики','Показать все характеристики',NULL);

INSERT INTO `modx_a_lang` VALUES ('125','Приховати збігаються характеристики','Скрыть совпадающие характеристики',NULL);

INSERT INTO `modx_a_lang` VALUES ('126','Ваш кошик порожній','Ваша корзина пуста',NULL);

INSERT INTO `modx_a_lang` VALUES ('127','Акції','Акции',NULL);

INSERT INTO `modx_a_lang` VALUES ('128','Контакти','Контакты',NULL);

INSERT INTO `modx_a_lang` VALUES ('129','Немає у наявності','Нет у наличии',NULL);

INSERT INTO `modx_a_lang` VALUES ('130','Товар доданий до кошика','Товар добавлен в корзину',NULL);

INSERT INTO `modx_a_lang` VALUES ('131','Показувати тільки в наявності','Показывать только в наличии',NULL);

INSERT INTO `modx_a_lang` VALUES ('132','До кошику','В корзину',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_mail_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_mail_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_mail_templates` (
  `mail_name` varchar(16) NOT NULL,
  `mail_lang` char(2) NOT NULL,
  `mail_title` varchar(32) NOT NULL,
  `mail_subject` varchar(1024) NOT NULL,
  `mail_template` mediumtext NOT NULL,
  UNIQUE KEY `mail_name_mail_lang` (`mail_name`,`mail_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_mail_templates`
#

INSERT INTO `modx_a_mail_templates` VALUES ('meta','ru','Общий шаблон (обертка)','','<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n<html>\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n    <title>[(site_name)]</title>\n    <style type=\"text/css\">\n        a {\n            text-decoration: none;\n        }\n    </style>\n</head>\n<body bgcolor=\"#ededed\" style=\"background:#ededed;\" marginheight=\"0\" marginwidth=\"0\" leftmargin=\"0\" topmargin=\"0\">\n<div style=\"background-color:#ededed;\">\n    <table style=\"font-family: \'Roboto\', sans-serif;\" width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr height=\"50\" style=\"line-height:0; font-size:0;\">\n            <td></td>\n        </tr>\n        <tr>\n            <td style=\"line-height:0; font-size:0;\">\n                <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"vertical-align:top;\">\n                    <tr>\n                        <td colspan=\"3\" bgcolor=\"#ffffff\">\n                            <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n                                <tr>\n                                    <td style=\"padding-top: 50px; padding-bottom: 40px\">\n                                        <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"\n                                               style=\"border-collapse:collapse;vertical-align:top;\">\n                                            <tr>\n                                                <td align=\"left\" style=\"line-height:0; font-size:0; padding-left: 50px\">\n                                                    <div style=\"text-align: left;\"><a href=\"#\" target=\"_blank\"><img src=\"[(site_url)]images/logo.png\" ></a></div>\n                                                </td>\n                                                <td align=\"right\" style=\"padding-right: 50px\">\n                                                    <img style=\"vertical-align: middle\" src=\"[(site_url)]images/locale.png\" alt=\"\">\n                                                    <span style=\"color: #939393; font-size: 12px; line-height: 14px; padding-left: 10px;\"> г. Киев, ул. Урловская, 1/8</span>\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                    <!--тело-->\n					{body}\n                    <!--тело-->\n                    <tr height=\"40\" style=\"line-height:0; font-size:0;\">\n                        <td bgcolor=\"#ffffff\" style=\"background-color: #ffffff\"></td>\n                    </tr>\n                    <tr>\n                        <td bgcolor=\"#183A42\" style=\"background-color: #183A42\">\n                            <table width=\"600\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n                                   style=\"border-collapse:collapse;vertical-align:top;\">\n                                <tbody>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                    <td colspan=\"5\"></td>\n                                </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td colspan=\"3\">\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: left;\">Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам. Добавьте наш адрес <a href=\"mailto:[(emailsender)]\">[(emailsender)]</a> в ваш список контактов, чтобы не пропустить ни одной скидки!\n                                            </p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                        <td colspan=\"5\"></td>\n                                    </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td colspan=\"3\">\n                                        <p style=\"border-bottom: 1px solid #8b9192; \"></p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                        <td colspan=\"5\"></td>\n                                    </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td >\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: left;\">Строка со служебной информацией</p>\n                                    </td>\n                                    <td colspan=\"2\">\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: right;\">\n                                                Разработка и поддержка сайта - <a href=\"https://artjoker.ua\" target=\"_blank\"><img style=\"vertical-align: middle; opacity: 0.5\" src=\"[(site_url)]images/aj-logo.png\" alt=\"artjoker\"></a>\n                                        </p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                \n                                <tr height=\"30\" style=\"line-height:0; font-size:0;\">\n                                    <td colspan=\"5\"></td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n        <tr height=\"50\" style=\"line-height:0; font-size:0;\">\n            <td></td>\n        </tr>\n    </table>\n</div>\n</body>\n</html>');

INSERT INTO `modx_a_mail_templates` VALUES ('meta','ua','Общий шаблон (обертка)','','<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n<html>\n<head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n    <title>[(site_name)]</title>\n    <style type=\"text/css\">\n        a {\n            text-decoration: none;\n        }\n    </style>\n</head>\n<body bgcolor=\"#ededed\" style=\"background:#ededed;\" marginheight=\"0\" marginwidth=\"0\" leftmargin=\"0\" topmargin=\"0\">\n<div style=\"background-color:#ededed;\">\n    <table style=\"font-family: \'Roboto\', sans-serif;\" width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr height=\"50\" style=\"line-height:0; font-size:0;\">\n            <td></td>\n        </tr>\n        <tr>\n            <td style=\"line-height:0; font-size:0;\">\n                <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"vertical-align:top;\">\n                    <tr>\n                        <td colspan=\"3\" bgcolor=\"#ffffff\">\n                            <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n                                <tr>\n                                    <td style=\"padding-top: 50px; padding-bottom: 40px\">\n                                        <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"\n                                               style=\"border-collapse:collapse;vertical-align:top;\">\n                                            <tr>\n                                                <td align=\"left\" style=\"line-height:0; font-size:0; padding-left: 50px\">\n                                                    <div style=\"text-align: left;\"><a href=\"#\" target=\"_blank\"><img src=\"[(site_url)]images/logo.png\" ></a></div>\n                                                </td>\n                                                <td align=\"right\" style=\"padding-right: 50px\">\n                                                    <img style=\"vertical-align: middle\" src=\"[(site_url)]images/locale.png\" alt=\"\">\n                                                    <span style=\"color: #939393; font-size: 12px; line-height: 14px; padding-left: 10px;\"> м. Київ, вул. Урлівська, 1/8</span>\n                                                </td>\n                                            </tr>\n                                        </table>\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                    <!--тело-->\n					{body}\n                    <!--тело-->\n                    <tr height=\"40\" style=\"line-height:0; font-size:0;\">\n                        <td bgcolor=\"#ffffff\" style=\"background-color: #ffffff\"></td>\n                    </tr>\n                    <tr>\n                        <td bgcolor=\"#183A42\" style=\"background-color: #183A42\">\n                            <table width=\"600\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n                                   style=\"border-collapse:collapse;vertical-align:top;\">\n                                <tbody>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                    <td colspan=\"5\"></td>\n                                </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td colspan=\"3\">\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: left;\">Лист містить дані для доступу в особистий кабінет, не передавайте його третім особам. Додайте нашу адресу <a href=\"mailto:[(emailsender)]\"> [(emailsender)]</a> в ваш список контактів, щоб не пропустити жодної знижки!\n                                            </p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                        <td colspan=\"5\"></td>\n                                    </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td colspan=\"3\">\n                                        <p style=\"border-bottom: 1px solid #8b9192; \"></p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                <tr height=\"25\" style=\"line-height:0; font-size:0;\">\n                                        <td colspan=\"5\"></td>\n                                    </tr>\n                                <tr>\n                                    <td width=\"50\"></td>\n                                    <td >\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: left;\">Рядок із службовою інформацією</p>\n                                    </td>\n                                    <td colspan=\"2\">\n                                        <p style=\"font-size:12px;line-height:14px;color:#8b9192; text-align: right;\">\n                                                Розробка і підтримка сайту - <a href=\"https://artjoker.ua\" target=\"_blank\"><img style=\"vertical-align: middle; opacity: 0.5\" src=\"[(site_url)]images/aj-logo.png\" alt=\"artjoker\"></a>\n                                        </p>\n                                    </td>\n                                    <td width=\"50\"></td>\n                                </tr>\n                                \n                                <tr height=\"30\" style=\"line-height:0; font-size:0;\">\n                                    <td colspan=\"5\"></td>\n                                </tr>\n                                </tbody>\n                            </table>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n        <tr height=\"50\" style=\"line-height:0; font-size:0;\">\n            <td></td>\n        </tr>\n    </table>\n</div>\n</body>\n</html>');

INSERT INTO `modx_a_mail_templates` VALUES ('meta','en','Общий шаблон (обертка)','','<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n	<meta charset=\"UTF-8\">\n	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n	<title>Document</title>\n</head>\n<body style=\"margin:0;padding:0\" bgcolor=\"#ffffff\">\n{body}\n</body>\n</html>');

INSERT INTO `modx_a_mail_templates` VALUES ('order_admin','ru','Заказ админ','Новый заказ','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr height=\"38\">\n							<td width=\"100%\">\n								<p style=\"color: #353535;font-size:20px; line-height: 28px; text-align:left;\">Информация о заказе</p>\n							</td>\n							<td bgcolor=\"#439336\" style=\"background-color: #439336; padding-left: 24px; padding-right: 24px\">\n								<p style=\"color: #ffffff; font-size: 16px; line-height: 18px\">#{order_id}</p>\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"60%\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td>\n											<p style=\"color: #353535;font-size:22px; line-height: 33px; text-align:left;\">{name}</p>\n										</td>\n									</tr>\n									<tr height=\"20\"><td></td></tr>\n									<tr>\n										<td>\n											<p style=\"font-size: 16px; color: #515151; line-height:2\">{area}</p>\n										</td>\n									</tr>\n									<tr>\n										<td>\n											<a style=\"font-size: 16px; color: #515151; line-height:2; text-decoration: none\" href=\"tel:[[S? &get=`setMiniPhone` &string=`{phone}`]]\">{phone}</a>\n										</td>\n									</tr>\n									[[if?&is=`{email}:empty`&else=`<tr>\n									<td>\n										<a style=\"font-size: 16px; color: #515151; line-height:2; text-decoration: none\" href=\"tel:{email}\">{email}</a>\n									</td>\n									</tr>`]]\n								</table>\n							</td>\n							<td width=\"40%\">\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<!--товары-->\n					{items}\n				<td width=\"50\"></td>\n			</tr>\n			<tr>\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td style=\"vertical-align: baseline\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr height=\"14\"><td colspan=\"2\"></td></tr>\n									<tr>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4; padding-bottom: 5px\">Доставка</p>\n										</td>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4;padding-bottom: 5px\">Установка</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{delivery_active};\">{delivery}</p>\n										</td>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{install_active};\">{install}</p>\n										</td>\n									</tr>\n								</table>\n							</td>\n							<td width=\"45%\">\n								<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td style=\"font-size: 17px; line-height: 24px; color: #000000;\">\n											<p style=\"font-size: 17px; line-height: 24px; color: #000000;\">Итого</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">\n											<p style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">{total} <span>{sing}</span></p>\n										</td>\n									</tr>\n									<tr><td></td></tr>\n								</table>\n							</td>\n						</tr>\n					</table>\n				</td>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('order_admin','ua','Заказ админ','Нове замовлення','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr height=\"38\">\n							<td width=\"100%\">\n								<p style=\"color: #353535;font-size:20px; line-height: 28px; text-align:left;\">Інформація про замовлення</p>\n							</td>\n							<td bgcolor=\"#439336\" style=\"background-color: #439336; padding-left: 24px; padding-right: 24px\">\n								<p style=\"color: #ffffff; font-size: 16px; line-height: 18px\">#{order_id}</p>\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"60%\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td>\n											<p style=\"color: #353535;font-size:22px; line-height: 33px; text-align:left;\">{name}</p>\n										</td>\n									</tr>\n									<tr height=\"20\"><td></td></tr>\n									<tr>\n										<td>\n											<p style=\"font-size: 16px; color: #515151; line-height:2\">{area}</p>\n										</td>\n									</tr>\n									<tr>\n										<td>\n											<a style=\"font-size: 16px; color: #515151; line-height:2; text-decoration: none\" href=\"tel:[[S? &get=`setMiniPhone` &string=`{phone}`]]\">{phone}</a>\n										</td>\n									</tr>\n									[[if?&is=`{email}:empty`&else=`<tr>\n									<td>\n										<a style=\"font-size: 16px; color: #515151; line-height:2; text-decoration: none\" href=\"tel:{email}\">{email}</a>\n									</td>\n									</tr>`]]\n								</table>\n							</td>\n							<td width=\"40%\">\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<!--товары-->\n					{items}\n				<td width=\"50\"></td>\n			</tr>\n			<tr>\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td style=\"vertical-align: baseline\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr height=\"14\"><td colspan=\"2\"></td></tr>\n									<tr>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4; padding-bottom: 5px\">Доставка</p>\n										</td>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4;padding-bottom: 5px\">Встановлення</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{delivery_active};\">{delivery}</p>\n										</td>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{install_active};\">{install}</p>\n										</td>\n									</tr>\n								</table>\n							</td>\n							<td width=\"45%\">\n								<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td style=\"font-size: 17px; line-height: 24px; color: #000000;\">\n											<p style=\"font-size: 17px; line-height: 24px; color: #000000;\">Разом</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">\n											<p style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">{total} <span>{sing}</span></p>\n										</td>\n									</tr>\n									<tr><td></td></tr>\n								</table>\n							</td>\n						</tr>\n					</table>\n				</td>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('order','ru','Заказ','Заказ','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Здравствуйте, {name}!</p>\n							</td>\n						</tr>\n						<tr height=\"20\"><td></td></tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr height=\"38\">\n							<td>\n								<p style=\"color: #787878;font-size:14px; line-height: 23px;font-family:Arial,sans-serif; text-align:left; padding-right: 25px\"> Ваш номер заказа:</p>\n							</td>\n							<td bgcolor=\"#439336\" style=\"background-color: #439336; padding-left: 24px; padding-right: 24px\">\n								<p style=\"color: #ffffff; font-size: 16px; line-height: 18px\">#{order_id}</p>\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<!--товары-->\n					{items}\n				<td width=\"50\"></td>\n			</tr>\n			<tr>\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td style=\"vertical-align: baseline\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr height=\"14\"><td colspan=\"2\"></td></tr>\n									<tr>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4; padding-bottom: 5px\">Доставка</p>\n										</td>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4;padding-bottom: 5px\">Установка</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{delivery_active};\">{delivery}</p>\n										</td>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{install_active};\">{install}</p>\n										</td>\n									</tr>\n								</table>\n							</td>\n							<td width=\"45%\">\n								<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td style=\"font-size: 17px; line-height: 24px; color: #000000;\">\n											<p style=\"font-size: 17px; line-height: 24px; color: #000000;\">Итого</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">\n											<p style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">{total} <span>{sing}</span></p>\n										</td>\n									</tr>\n									<tr><td></td></tr>\n								</table>\n							</td>\n						</tr>\n					</table>\n				</td>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('order','ua','Заказ','Замовлення','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Вітаємо, {name}!</p>\n							</td>\n						</tr>\n						<tr height=\"20\"><td></td></tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr height=\"38\">\n							<td>\n								<p style=\"color: #787878;font-size:14px; line-height: 23px;font-family:Arial,sans-serif; text-align:left; padding-right: 25px\"> Ваш номер замовлення:</p>\n							</td>\n							<td bgcolor=\"#439336\" style=\"background-color: #439336; padding-left: 24px; padding-right: 24px\">\n								<p style=\"color: #ffffff; font-size: 16px; line-height: 18px\">#{order_id}</p>\n							</td>\n						</tr>\n						<tr height=\"40\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n					<!--товары-->\n					{items}\n				<td width=\"50\"></td>\n			</tr>\n			<tr>\n				<td width=\"50\"></td>\n				<td>\n					<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td style=\"vertical-align: baseline\">\n								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr height=\"14\"><td colspan=\"2\"></td></tr>\n									<tr>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4; padding-bottom: 5px\">Доставка</p>\n										</td>\n										<td style=\"color: #353535; font-size: 14px; line-height: 1.4\">\n											<p style=\"color: #353535; font-size: 14px; line-height: 1.4;padding-bottom: 5px\">Встановлення</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{delivery_active};\">{delivery}</p>\n										</td>\n										<td style=\"font-size: 18px; line-height: 1.4; color: #439336;\">\n											<p style=\"font-size: 18px; line-height: 1.4; color: #{install_active};\">{install}</p>\n										</td>\n									</tr>\n								</table>\n							</td>\n							<td width=\"45%\">\n								<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n									<tr>\n										<td style=\"font-size: 17px; line-height: 24px; color: #000000;\">\n											<p style=\"font-size: 17px; line-height: 24px; color: #000000;\">Разом</p>\n										</td>\n									</tr>\n									<tr>\n										<td style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">\n											<p style=\"font-size: 37px; line-height: 52px; color: #FF6928;\">{total} <span>{sing}</span></p>\n										</td>\n									</tr>\n									<tr><td></td></tr>\n								</table>\n							</td>\n						</tr>\n					</table>\n				</td>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('call_me','ru','Запрос на обратный звонок','Запрос на обратный звонок','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрий день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Новый запрос на обратный звонок</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Имя: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('call_me','ua','Запрос на обратный звонок','Запит на зворотний дзвінок','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрый день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Новий запит на зворотний дзвінок</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Ім\'я: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('meter','ru','Вызов замерщика','Вызов замерщика','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрый день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Новый запрос на вызов замерщика</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Имя: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Район: {area}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('meter','ua','Вызов замерщика','Виклик замірника','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрий день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Новий запит на виклик замірника</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Ім\'я: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Район: {area}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('question','ru','Новый вопрос','Новый вопрос','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрый день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Новый вопрос</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Имя: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Вопрос: {question}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');

INSERT INTO `modx_a_mail_templates` VALUES ('question','ua','Новый вопрос','Нове запитання','<tr>\n	<td bgcolor=\"#ffffff\" width=\"600\" style=\"line-height:0; font-size:0;\">\n		<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n			<tr align=\"left\" cellpadding=\"0\" cellspacing=\"0\"\n				style=\"border-collapse:collapse;vertical-align:top;\">\n			</tr>\n			<tr height=\"150\" style=\"line-height:0; font-size:0;\">\n				<td width=\"50\"></td>\n				<td>\n					<table align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr>\n							<td width=\"600\" >\n								<p style=\"color: #353535;font-size:20px; line-height: 28px;font-family:Arial,sans-serif; text-align:left;\">Добрий день!</p>\n							</td>\n						</tr>\n					</table>\n					<table cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;vertical-align:top;\">\n						<tr >\n							<td width=\"600\" style=\"padding-top:5px\">\n								<p style=\"font-size:14px;line-height:23px;color:#787878; font-family:Arial,sans-serif; text-align:left;\">Нове запитання</p>\n							</td>\n						</tr>\n						<tr height=\"30\" style=\"line-height:0; font-size:0;\"></tr>\n					</table>\n\n					<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n						<tr>\n							<td width=\"100%\">\n								<ul width=\"100%\" style=\"margin: 10px 0;\">\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Ім\'я: {name}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Телефон: {phone}</li>\n									<li style=\"color:#000000; font-size: 16px; line-height: 24px\">Запитання: {question}</li>\n								</ul>\n							</td>\n						</tr>\n						<tr height=\"20\"></tr>\n					</table>\n				<td width=\"50\"></td>\n			</tr>\n		</table>\n	</td>\n</tr>');


# --------------------------------------------------------

#
# Table structure for table `modx_a_mailer`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_mailer`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_mailer` (
  `tpl_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mailed` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `tpl_id_user_id` (`tpl_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_mailer`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_mailer_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_mailer_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_mailer_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `template_subject` varchar(512) NOT NULL,
  `template_post` text NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_mailer_templates`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_mailer_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_mailer_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_mailer_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) NOT NULL DEFAULT '',
  `user_email` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_mailer_users`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_meter_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_meter_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_meter_strings` (
  `string_id` int(11) NOT NULL AUTO_INCREMENT,
  `string_locate` varchar(4) NOT NULL,
  `meter_id` int(11) NOT NULL,
  `meter_question` varchar(512) NOT NULL DEFAULT '',
  `meter_answer` longtext,
  PRIMARY KEY (`string_id`),
  UNIQUE KEY `str_locate_faq_id` (`string_locate`,`meter_id`),
  KEY `meter_id` (`meter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_meter_strings`
#

INSERT INTO `modx_a_meter_strings` VALUES ('1','ru','1','3',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('2','ru','2','3',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('3','ru','3','ыыыффкыркыиывки wsegv. g',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('4','ru','4','фуеруыевртывте',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('5','ru','5','ыыыффкыркыиывки wsegv. g',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('6','ru','6','tot',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('7','ru','7','325hdsffb',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('8','ru','8','ыыыффкыркыиывки wsegv. g',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('9','ru','9','Центр',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('11','ru','11','левый берег',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('12','ru','12','тестовый берег',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('13','ua','13','135312616',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('14','ua','14','Лівий берег',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('15','ua','15','wrwabwarbasrb',NULL);

INSERT INTO `modx_a_meter_strings` VALUES ('16','ru','16','Правый берег',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_meters`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_meters`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_meters` (
  `meter_id` int(11) NOT NULL AUTO_INCREMENT,
  `meter_category` int(11) NOT NULL DEFAULT '0',
  `meter_name` varchar(100) NOT NULL DEFAULT '',
  `meter_phone` varchar(100) NOT NULL DEFAULT '',
  `meter_email` varchar(100) NOT NULL DEFAULT '',
  `meter_active` tinyint(4) NOT NULL DEFAULT '0',
  `meter_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `meter_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`meter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_meters`
#

INSERT INTO `modx_a_meters` VALUES ('1','0','asfa<db<sabsrb<sd','+38(333)3333333','','0','2019-09-27 14:55:36','2019-09-27 14:55:36');

INSERT INTO `modx_a_meters` VALUES ('2','0','asfa<db<sabsrb<sd','+38(333)3333333','','0','2019-09-27 16:49:56','2019-09-27 16:49:56');

INSERT INTO `modx_a_meters` VALUES ('3','0','1624375w846e579r68r0 ewarhsdrhsearh','+38(999)9999999','','1','2019-09-27 16:50:19','2019-10-15 13:22:38');

INSERT INTO `modx_a_meters` VALUES ('4','0','Zephania Park','+38(222)2222238','','0','2019-10-02 15:03:41','2019-10-02 15:03:41');

INSERT INTO `modx_a_meters` VALUES ('5','0','Dolan Benton','+38(1)2222222','','1','2019-10-02 15:27:44','2019-10-15 13:22:36');

INSERT INTO `modx_a_meters` VALUES ('6','0','тест второй','+38(098)7654533','','0','2019-10-03 10:38:27','2019-10-03 10:38:27');

INSERT INTO `modx_a_meters` VALUES ('7','0','Quemby Kidd','+38(1)2332222','','0','2019-10-09 14:27:10','2019-10-09 14:27:10');

INSERT INTO `modx_a_meters` VALUES ('8','0','Анна','+38(095)4171287','','1','2019-10-10 10:17:55','2019-10-15 13:21:26');

INSERT INTO `modx_a_meters` VALUES ('9','0','Brenden Morris','+38(000)0000000','','1','2019-10-11 14:27:58','2019-10-15 13:21:20');

INSERT INTO `modx_a_meters` VALUES ('11','0','тест','+38(213)2132132','','0','2019-10-15 12:06:30','2019-10-15 12:06:30');

INSERT INTO `modx_a_meters` VALUES ('12','0','фывыфвыф','+38(213)1321212','','1','2019-10-15 12:08:26','2019-10-15 13:21:18');

INSERT INTO `modx_a_meters` VALUES ('13','0','Тест','+38(095)4171287','','1','2019-10-17 10:06:58','2019-10-17 10:07:24');

INSERT INTO `modx_a_meters` VALUES ('14','0','Simone Bean','+38(1)4321234','','0','2019-10-17 15:20:40','2019-10-17 15:20:40');

INSERT INTO `modx_a_meters` VALUES ('15','0','Kelly Lyons','+38(1)2332222','','0','2019-10-17 15:22:50','2019-10-17 15:22:50');

INSERT INTO `modx_a_meters` VALUES ('16','0','Елена','+38(662)211672','','0','2019-10-31 18:18:35','2019-10-31 18:18:35');


# --------------------------------------------------------

#
# Table structure for table `modx_a_new_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_new_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_new_strings` (
  `rid` int(11) unsigned NOT NULL COMMENT 'Related news ID',
  `lang` varchar(16) NOT NULL COMMENT 'Language',
  `title` varchar(255) NOT NULL COMMENT 'Title',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description',
  `content` text COMMENT 'Content',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `seo_canonical` varchar(255) DEFAULT NULL,
  `seo_robots` varchar(255) DEFAULT NULL,
  UNIQUE KEY `rid` (`rid`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_new_strings`
#

INSERT INTO `modx_a_new_strings` VALUES ('1','ru','Как выбрать бронированные двери - 06.08.2019','<p>С другой стороны рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности требуют от нас анализа системы обучения кадров, соот','<h2>Что такое бронированные двери?</h2>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<h2>Преимущества бронированных дверей</h2>\n<ul>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>В отличие от деревянных дверей, металлическая конструкция бронедверей не деформируется со временем.</li>\n<li>Установив металлическую бронированную дверь, человек чувствует себя в безопасности.</li>\n</ul>\n<p><img src=\"images/news-img.jpeg\" alt=\"\" /></p>\n<h4>Недостатки бронированных дверей</h4>\n<ul>\n<li>Все вышеперечисленные достоинства окажутся бесполезными, если не уделить достаточное внимание замкам и защитным механизмам. Даже самую хорошую дверь злоумышленники смогут вскрыть, если установить дешевые и простые замки.</li>\n<li>В чрезвычайной ситуации возможно затруднительное попадание спасателей.</li>\n</ul>\n<h2>Признаки качественной бронированной двери</h2>\n<p>Качественная бронированная дверь зависит от толщины обшивки, от наличия защитной фурнитуры, а также от замочной конструкции. Если правильно учесть все тонкости этих элементов, можно получить отличный надежный вариант качественной бронированной двери.</p>\n<p>При выборе бронированных дверей нужно обращать внимание на толщину листов. Оптимальным вариантом является толщина листа 2 - 2,5 мм. Двери с толщиной 3 - 4 мм довольно тяжелые и обычно используются для загородных домов и коттеджей. Также бронированная дверь будет безопаснее, если металлические листы, которыми обшита дверная конструкция, цельные, без швов. Такие двери намного дороже, но и качественнее.</p>\n<h4>Тепло- и звукоизоляционный слой</h4>\n<ul>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Наиболее востребованным изолирующим материалом является минеральная вата. Такой материал экологически безвреден, хорошо защищает от посторонних шумов, не подвержен горению, не выводит тепло из квартиры и не пропускает холод с улицы.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Достичь эффективной теплоизоляции можно при помощи качественно изготовленного изделия. Дверь должна хорошо прилегать к коробу.</li>\n<li>Еще одним способом изоляции является отделка металлических дверей декоративными панелями МДФ.</li>\n</ul>\n<h2>Ребра жесткости и другая дополнительная защита</h2>\n<p>Ребра жесткости повышают защитные свойства двери. Так как дверное полотно имеет большую площадь, ребра жесткости защищают полотно, повышают его взломостойкость, предотвращают деформацию полотна от внешних нагрузок. Кроме того, что они улучшают защитные свойства дверного блока, ребра жесткости создают толщину. Между листами образуется пространство, которое заполняется изоляционным материалом.</p>\n<p>Дополнительной защитой для бронированных дверей являются петли и наличники. Лучше всего когда петли находятся внутри дверной конструкции, в таком случае их невозможно будет сбить или срезать. Но, если они расположены снаружи, для их дополнительной защиты используют противосъемные штыри. Количество петель всегда зависит от веса двери.</p>\n<h4>Замки</h4>\n<p>Для входных дверей выделяются следующие разновидности замков:</p>\n<ul>\n<li>Классическим механизмом запирания являются сувальдные замки. Особенность этого замка состоит в специальных пластинах. Для максимальной защиты от взлома лучше всего использовать бронированные конструкции с числом от 6 до 8 сувальд.</li>\n<li>Самым популярным механизмом запирания являются цилиндровые замки. Внутри замочного механизма расположены несколько небольших цилиндра, в них и заключается принцип работы. Важно отметить, что для дверей с таким замком нужно еще устанавливать дополнительные бронированные накладки, что обезопасит цилиндровую конструкцию от выбивания.</li>\n<li>Замок с моноблочной конструкцией объединяет в своей работе сувальдный и цилиндровый механизмы. Принцип работы заключается в закрывании цилиндрового замка совместно с поднятием шторки в сувальдном механизме. Данная конструкция повышает уровень взломостойкости дверного полотна.</li>\n<li>Электромеханические замки не пользуются огромным спросом, из-за достаточно высокой стоимости, но многие специалисты считают что электромеханические замки наиболее практичны и надежны. Принцип работы такого механизма основан на вводе комбинации цифр или букв на электронной панели. К тому же используется еще и физический ключ. В случае, если вы введете код неправильно, только физическим ключом дверь не открыть.</li>\n<li>Для того, чтобы открыть дверь с кодовым замком ключи не потребуются, необходима лишь комбинация цифр. Но, это же выступает их большим недостатком. Так как со временем эксплуатации цифры стираются,</li>\n</ul>\n<h2>Как выбрать отделку фасада бронедверей</h2>\n<h3>Для отделки фасадов бронедверей используют различные материалы</h3>\n<ul>\n<li>панели МДФ</li>\n<li>кожвинил</li>\n<li>краска</li>\n</ul>\n<p>Один из вариантов отделки - панели МДФ. Материал обладает такими свойствами: устойчив к воздействию влаги и температурным перепадам, имеет хороший эстетический внешний вид, обладает высоким уровнем звуко- и теплоизоляции.</p>\n<p>Для отделки входных дверей, также используют кожвинил. Материал обладает такими свойствами: простой в эксплуатации, имеет неплохой уровень звуко- и теплоизоляции. Кожвинил бывает гладкий и тисненный. Достаточно распространенный и бюджетный вариант.</p>\n<p>В качестве отделки бронедверей часто выбирают покраску. Такой вид отделки обеспечивает двери устойчивостью к влаге, УФ-лучам, придает дверному полотну антикоррозийных свойств. Для отделки фасадов используют порошковые краски, молотковые или нитроцеллюлозные эмали. Каждый красящий состав имеет свои особенности.</p>\n<p>Перед тем, как выбрать бронированную дверь, рекомендуем вам изучить все тонкости и особенности этого изделия, ознакомиться с отзывами покупателей и, в итоге, приобрести качественный товар, который прослужит много лет.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('1','ua','Як вибрати броньовані двері','<p>З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','<h2>Що таке броньовані двері?</h2>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<h3>Переваги броньованих дверей</h3>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<h4>Недоліки броньованих дверей</h4>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<h2>Ознаки якісної броньованих дверей</h2>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<h4>Тепло- та звукоізоляційний шар</h4>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<h2>Ребра жорсткості і інша додатковий захист</h2>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<h4>Замки</h4>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,\n<h2>Як вибрати обробку фасаду бронедверей</h2>\n<h3>Для обробки фасадів бронедверей використовують різні матеріали</h3>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>\n</li>\n</ul>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('2','ru','Как выбрать бронированные двери - 07.08.2019','<p>С другой стороны рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности требуют от нас анализа системы обучения кадров, соот','<h2>Что такое бронированные двери?</h2>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<h2>Преимущества бронированных дверей</h2>\n<ul>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>В отличие от деревянных дверей, металлическая конструкция бронедверей не деформируется со временем.</li>\n<li>Установив металлическую бронированную дверь, человек чувствует себя в безопасности.</li>\n</ul>\n<p><img src=\"images/news-img.jpeg\" alt=\"\" /></p>\n<h4>Недостатки бронированных дверей</h4>\n<ul>\n<li>Все вышеперечисленные достоинства окажутся бесполезными, если не уделить достаточное внимание замкам и защитным механизмам. Даже самую хорошую дверь злоумышленники смогут вскрыть, если установить дешевые и простые замки.</li>\n<li>В чрезвычайной ситуации возможно затруднительное попадание спасателей.</li>\n</ul>\n<h2>Признаки качественной бронированной двери</h2>\n<p>Качественная бронированная дверь зависит от толщины обшивки, от наличия защитной фурнитуры, а также от замочной конструкции. Если правильно учесть все тонкости этих элементов, можно получить отличный надежный вариант качественной бронированной двери.</p>\n<p>При выборе бронированных дверей нужно обращать внимание на толщину листов. Оптимальным вариантом является толщина листа 2 - 2,5 мм. Двери с толщиной 3 - 4 мм довольно тяжелые и обычно используются для загородных домов и коттеджей. Также бронированная дверь будет безопаснее, если металлические листы, которыми обшита дверная конструкция, цельные, без швов. Такие двери намного дороже, но и качественнее.</p>\n<h4>Тепло- и звукоизоляционный слой</h4>\n<ul>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Наиболее востребованным изолирующим материалом является минеральная вата. Такой материал экологически безвреден, хорошо защищает от посторонних шумов, не подвержен горению, не выводит тепло из квартиры и не пропускает холод с улицы.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Достичь эффективной теплоизоляции можно при помощи качественно изготовленного изделия. Дверь должна хорошо прилегать к коробу.</li>\n<li>Еще одним способом изоляции является отделка металлических дверей декоративными панелями МДФ.</li>\n</ul>\n<h2>Ребра жесткости и другая дополнительная защита</h2>\n<p>Ребра жесткости повышают защитные свойства двери. Так как дверное полотно имеет большую площадь, ребра жесткости защищают полотно, повышают его взломостойкость, предотвращают деформацию полотна от внешних нагрузок. Кроме того, что они улучшают защитные свойства дверного блока, ребра жесткости создают толщину. Между листами образуется пространство, которое заполняется изоляционным материалом.</p>\n<p>Дополнительной защитой для бронированных дверей являются петли и наличники. Лучше всего когда петли находятся внутри дверной конструкции, в таком случае их невозможно будет сбить или срезать. Но, если они расположены снаружи, для их дополнительной защиты используют противосъемные штыри. Количество петель всегда зависит от веса двери.</p>\n<h4>Замки</h4>\n<p>Для входных дверей выделяются следующие разновидности замков:</p>\n<ul>\n<li>Классическим механизмом запирания являются сувальдные замки. Особенность этого замка состоит в специальных пластинах. Для максимальной защиты от взлома лучше всего использовать бронированные конструкции с числом от 6 до 8 сувальд.</li>\n<li>Самым популярным механизмом запирания являются цилиндровые замки. Внутри замочного механизма расположены несколько небольших цилиндра, в них и заключается принцип работы. Важно отметить, что для дверей с таким замком нужно еще устанавливать дополнительные бронированные накладки, что обезопасит цилиндровую конструкцию от выбивания.</li>\n<li>Замок с моноблочной конструкцией объединяет в своей работе сувальдный и цилиндровый механизмы. Принцип работы заключается в закрывании цилиндрового замка совместно с поднятием шторки в сувальдном механизме. Данная конструкция повышает уровень взломостойкости дверного полотна.</li>\n<li>Электромеханические замки не пользуются огромным спросом, из-за достаточно высокой стоимости, но многие специалисты считают что электромеханические замки наиболее практичны и надежны. Принцип работы такого механизма основан на вводе комбинации цифр или букв на электронной панели. К тому же используется еще и физический ключ. В случае, если вы введете код неправильно, только физическим ключом дверь не открыть.</li>\n<li>Для того, чтобы открыть дверь с кодовым замком ключи не потребуются, необходима лишь комбинация цифр. Но, это же выступает их большим недостатком. Так как со временем эксплуатации цифры стираются,</li>\n</ul>\n<h2>Как выбрать отделку фасада бронедверей</h2>\n<h3>Для отделки фасадов бронедверей используют различные материалы</h3>\n<ul>\n<li>панели МДФ</li>\n<li>кожвинил</li>\n<li>краска</li>\n</ul>\n<p>Один из вариантов отделки - панели МДФ. Материал обладает такими свойствами: устойчив к воздействию влаги и температурным перепадам, имеет хороший эстетический внешний вид, обладает высоким уровнем звуко- и теплоизоляции.</p>\n<p>Для отделки входных дверей, также используют кожвинил. Материал обладает такими свойствами: простой в эксплуатации, имеет неплохой уровень звуко- и теплоизоляции. Кожвинил бывает гладкий и тисненный. Достаточно распространенный и бюджетный вариант.</p>\n<p>В качестве отделки бронедверей часто выбирают покраску. Такой вид отделки обеспечивает двери устойчивостью к влаге, УФ-лучам, придает дверному полотну антикоррозийных свойств. Для отделки фасадов используют порошковые краски, молотковые или нитроцеллюлозные эмали. Каждый красящий состав имеет свои особенности.</p>\n<p>Перед тем, как выбрать бронированную дверь, рекомендуем вам изучить все тонкости и особенности этого изделия, ознакомиться с отзывами покупателей и, в итоге, приобрести качественный товар, который прослужит много лет.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('2','ua','Як вибрати броньовані двері','<p>З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','<h2>Що таке броньовані двері?</h2>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<h3>Переваги броньованих дверей</h3>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<h4>Недоліки броньованих дверей</h4>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<h2>Ознаки якісної броньованих дверей</h2>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<h4>Тепло- та звукоізоляційний шар</h4>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<h2>Ребра жорсткості і інша додатковий захист</h2>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<h4>Замки</h4>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,\n<h2>Як вибрати обробку фасаду бронедверей</h2>\n<h3>Для обробки фасадів бронедверей використовують різні матеріали</h3>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>\n</li>\n</ul>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('4','ru','Как выбрать бронированные двери - 08.08.2019','<p>С другой стороны рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности требуют от нас анализа системы обучения кадров, соот','<h2>Что такое бронированные двери?</h2>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<h2>Преимущества бронированных дверей</h2>\n<ul>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>В отличие от деревянных дверей, металлическая конструкция бронедверей не деформируется со временем.</li>\n<li>Установив металлическую бронированную дверь, человек чувствует себя в безопасности.</li>\n</ul>\n<p><img src=\"images/news-img.jpeg\" alt=\"\" /></p>\n<h4>Недостатки бронированных дверей</h4>\n<ul>\n<li>Все вышеперечисленные достоинства окажутся бесполезными, если не уделить достаточное внимание замкам и защитным механизмам. Даже самую хорошую дверь злоумышленники смогут вскрыть, если установить дешевые и простые замки.</li>\n<li>В чрезвычайной ситуации возможно затруднительное попадание спасателей.</li>\n</ul>\n<h2>Признаки качественной бронированной двери</h2>\n<p>Качественная бронированная дверь зависит от толщины обшивки, от наличия защитной фурнитуры, а также от замочной конструкции. Если правильно учесть все тонкости этих элементов, можно получить отличный надежный вариант качественной бронированной двери.</p>\n<p>При выборе бронированных дверей нужно обращать внимание на толщину листов. Оптимальным вариантом является толщина листа 2 - 2,5 мм. Двери с толщиной 3 - 4 мм довольно тяжелые и обычно используются для загородных домов и коттеджей. Также бронированная дверь будет безопаснее, если металлические листы, которыми обшита дверная конструкция, цельные, без швов. Такие двери намного дороже, но и качественнее.</p>\n<h4>Тепло- и звукоизоляционный слой</h4>\n<ul>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Наиболее востребованным изолирующим материалом является минеральная вата. Такой материал экологически безвреден, хорошо защищает от посторонних шумов, не подвержен горению, не выводит тепло из квартиры и не пропускает холод с улицы.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Достичь эффективной теплоизоляции можно при помощи качественно изготовленного изделия. Дверь должна хорошо прилегать к коробу.</li>\n<li>Еще одним способом изоляции является отделка металлических дверей декоративными панелями МДФ.</li>\n</ul>\n<h2>Ребра жесткости и другая дополнительная защита</h2>\n<p>Ребра жесткости повышают защитные свойства двери. Так как дверное полотно имеет большую площадь, ребра жесткости защищают полотно, повышают его взломостойкость, предотвращают деформацию полотна от внешних нагрузок. Кроме того, что они улучшают защитные свойства дверного блока, ребра жесткости создают толщину. Между листами образуется пространство, которое заполняется изоляционным материалом.</p>\n<p>Дополнительной защитой для бронированных дверей являются петли и наличники. Лучше всего когда петли находятся внутри дверной конструкции, в таком случае их невозможно будет сбить или срезать. Но, если они расположены снаружи, для их дополнительной защиты используют противосъемные штыри. Количество петель всегда зависит от веса двери.</p>\n<h4>Замки</h4>\n<p>Для входных дверей выделяются следующие разновидности замков:</p>\n<ul>\n<li>Классическим механизмом запирания являются сувальдные замки. Особенность этого замка состоит в специальных пластинах. Для максимальной защиты от взлома лучше всего использовать бронированные конструкции с числом от 6 до 8 сувальд.</li>\n<li>Самым популярным механизмом запирания являются цилиндровые замки. Внутри замочного механизма расположены несколько небольших цилиндра, в них и заключается принцип работы. Важно отметить, что для дверей с таким замком нужно еще устанавливать дополнительные бронированные накладки, что обезопасит цилиндровую конструкцию от выбивания.</li>\n<li>Замок с моноблочной конструкцией объединяет в своей работе сувальдный и цилиндровый механизмы. Принцип работы заключается в закрывании цилиндрового замка совместно с поднятием шторки в сувальдном механизме. Данная конструкция повышает уровень взломостойкости дверного полотна.</li>\n<li>Электромеханические замки не пользуются огромным спросом, из-за достаточно высокой стоимости, но многие специалисты считают что электромеханические замки наиболее практичны и надежны. Принцип работы такого механизма основан на вводе комбинации цифр или букв на электронной панели. К тому же используется еще и физический ключ. В случае, если вы введете код неправильно, только физическим ключом дверь не открыть.</li>\n<li>Для того, чтобы открыть дверь с кодовым замком ключи не потребуются, необходима лишь комбинация цифр. Но, это же выступает их большим недостатком. Так как со временем эксплуатации цифры стираются,</li>\n</ul>\n<h2>Как выбрать отделку фасада бронедверей</h2>\n<h3>Для отделки фасадов бронедверей используют различные материалы</h3>\n<ul>\n<li>панели МДФ</li>\n<li>кожвинил</li>\n<li>краска</li>\n</ul>\n<p>Один из вариантов отделки - панели МДФ. Материал обладает такими свойствами: устойчив к воздействию влаги и температурным перепадам, имеет хороший эстетический внешний вид, обладает высоким уровнем звуко- и теплоизоляции.</p>\n<p>Для отделки входных дверей, также используют кожвинил. Материал обладает такими свойствами: простой в эксплуатации, имеет неплохой уровень звуко- и теплоизоляции. Кожвинил бывает гладкий и тисненный. Достаточно распространенный и бюджетный вариант.</p>\n<p>В качестве отделки бронедверей часто выбирают покраску. Такой вид отделки обеспечивает двери устойчивостью к влаге, УФ-лучам, придает дверному полотну антикоррозийных свойств. Для отделки фасадов используют порошковые краски, молотковые или нитроцеллюлозные эмали. Каждый красящий состав имеет свои особенности.</p>\n<p>Перед тем, как выбрать бронированную дверь, рекомендуем вам изучить все тонкости и особенности этого изделия, ознакомиться с отзывами покупателей и, в итоге, приобрести качественный товар, который прослужит много лет.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('4','ua','Як вибрати броньовані двері','<p>З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','<h2>Що таке броньовані двері?</h2>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<h3>Переваги броньованих дверей</h3>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<h4>Недоліки броньованих дверей</h4>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<h2>Ознаки якісної броньованих дверей</h2>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<h4>Тепло- та звукоізоляційний шар</h4>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<h2>Ребра жорсткості і інша додатковий захист</h2>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<h4>Замки</h4>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,\n<h2>Як вибрати обробку фасаду бронедверей</h2>\n<h3>Для обробки фасадів бронедверей використовують різні матеріали</h3>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>\n</li>\n</ul>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('5','ru','Как выбрать бронированные двери - 09.08.2019','<p>С другой стороны рамки и место обучения кадров позволяет оценить значение дальнейших направлений развития. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности требуют от нас анализа системы обучения кадров, соот','<h2>Что такое бронированные двери?</h2>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<h2>Преимущества бронированных дверей</h2>\n<ul>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>В отличие от деревянных дверей, металлическая конструкция бронедверей не деформируется со временем.</li>\n<li>Установив металлическую бронированную дверь, человек чувствует себя в безопасности.</li>\n</ul>\n<p><img src=\"images/news-img.jpeg\" alt=\"\" /></p>\n<h4>Недостатки бронированных дверей</h4>\n<ul>\n<li>Все вышеперечисленные достоинства окажутся бесполезными, если не уделить достаточное внимание замкам и защитным механизмам. Даже самую хорошую дверь злоумышленники смогут вскрыть, если установить дешевые и простые замки.</li>\n<li>В чрезвычайной ситуации возможно затруднительное попадание спасателей.</li>\n</ul>\n<h2>Признаки качественной бронированной двери</h2>\n<p>Качественная бронированная дверь зависит от толщины обшивки, от наличия защитной фурнитуры, а также от замочной конструкции. Если правильно учесть все тонкости этих элементов, можно получить отличный надежный вариант качественной бронированной двери.</p>\n<p>При выборе бронированных дверей нужно обращать внимание на толщину листов. Оптимальным вариантом является толщина листа 2 - 2,5 мм. Двери с толщиной 3 - 4 мм довольно тяжелые и обычно используются для загородных домов и коттеджей. Также бронированная дверь будет безопаснее, если металлические листы, которыми обшита дверная конструкция, цельные, без швов. Такие двери намного дороже, но и качественнее.</p>\n<h4>Тепло- и звукоизоляционный слой</h4>\n<ul>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Наиболее востребованным изолирующим материалом является минеральная вата. Такой материал экологически безвреден, хорошо защищает от посторонних шумов, не подвержен горению, не выводит тепло из квартиры и не пропускает холод с улицы.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Достичь эффективной теплоизоляции можно при помощи качественно изготовленного изделия. Дверь должна хорошо прилегать к коробу.</li>\n<li>Еще одним способом изоляции является отделка металлических дверей декоративными панелями МДФ.</li>\n</ul>\n<h2>Ребра жесткости и другая дополнительная защита</h2>\n<p>Ребра жесткости повышают защитные свойства двери. Так как дверное полотно имеет большую площадь, ребра жесткости защищают полотно, повышают его взломостойкость, предотвращают деформацию полотна от внешних нагрузок. Кроме того, что они улучшают защитные свойства дверного блока, ребра жесткости создают толщину. Между листами образуется пространство, которое заполняется изоляционным материалом.</p>\n<p>Дополнительной защитой для бронированных дверей являются петли и наличники. Лучше всего когда петли находятся внутри дверной конструкции, в таком случае их невозможно будет сбить или срезать. Но, если они расположены снаружи, для их дополнительной защиты используют противосъемные штыри. Количество петель всегда зависит от веса двери.</p>\n<h4>Замки</h4>\n<p>Для входных дверей выделяются следующие разновидности замков:</p>\n<ul>\n<li>Классическим механизмом запирания являются сувальдные замки. Особенность этого замка состоит в специальных пластинах. Для максимальной защиты от взлома лучше всего использовать бронированные конструкции с числом от 6 до 8 сувальд.</li>\n<li>Самым популярным механизмом запирания являются цилиндровые замки. Внутри замочного механизма расположены несколько небольших цилиндра, в них и заключается принцип работы. Важно отметить, что для дверей с таким замком нужно еще устанавливать дополнительные бронированные накладки, что обезопасит цилиндровую конструкцию от выбивания.</li>\n<li>Замок с моноблочной конструкцией объединяет в своей работе сувальдный и цилиндровый механизмы. Принцип работы заключается в закрывании цилиндрового замка совместно с поднятием шторки в сувальдном механизме. Данная конструкция повышает уровень взломостойкости дверного полотна.</li>\n<li>Электромеханические замки не пользуются огромным спросом, из-за достаточно высокой стоимости, но многие специалисты считают что электромеханические замки наиболее практичны и надежны. Принцип работы такого механизма основан на вводе комбинации цифр или букв на электронной панели. К тому же используется еще и физический ключ. В случае, если вы введете код неправильно, только физическим ключом дверь не открыть.</li>\n<li>Для того, чтобы открыть дверь с кодовым замком ключи не потребуются, необходима лишь комбинация цифр. Но, это же выступает их большим недостатком. Так как со временем эксплуатации цифры стираются,</li>\n</ul>\n<h2>Как выбрать отделку фасада бронедверей</h2>\n<h3>Для отделки фасадов бронедверей используют различные материалы</h3>\n<ul>\n<li>панели МДФ</li>\n<li>кожвинил</li>\n<li>краска</li>\n</ul>\n<p>Один из вариантов отделки - панели МДФ. Материал обладает такими свойствами: устойчив к воздействию влаги и температурным перепадам, имеет хороший эстетический внешний вид, обладает высоким уровнем звуко- и теплоизоляции.</p>\n<p>Для отделки входных дверей, также используют кожвинил. Материал обладает такими свойствами: простой в эксплуатации, имеет неплохой уровень звуко- и теплоизоляции. Кожвинил бывает гладкий и тисненный. Достаточно распространенный и бюджетный вариант.</p>\n<p>В качестве отделки бронедверей часто выбирают покраску. Такой вид отделки обеспечивает двери устойчивостью к влаге, УФ-лучам, придает дверному полотну антикоррозийных свойств. Для отделки фасадов используют порошковые краски, молотковые или нитроцеллюлозные эмали. Каждый красящий состав имеет свои особенности.</p>\n<p>Перед тем, как выбрать бронированную дверь, рекомендуем вам изучить все тонкости и особенности этого изделия, ознакомиться с отзывами покупателей и, в итоге, приобрести качественный товар, который прослужит много лет.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('5','ua','Як вибрати броньовані двері','<p>З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','<h2>Що таке броньовані двері?</h2>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<h3>Переваги броньованих дверей</h3>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<h4>Недоліки броньованих дверей</h4>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<h2>Ознаки якісної броньованих дверей</h2>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<h4>Тепло- та звукоізоляційний шар</h4>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<h2>Ребра жорсткості і інша додатковий захист</h2>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<h4>Замки</h4>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,\n<h2>Як вибрати обробку фасаду бронедверей</h2>\n<h3>Для обробки фасадів бронедверей використовують різні матеріали</h3>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>\n</li>\n</ul>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('6','ru','Как выбрать бронированные двери - 10.09.2019','<p>С другой стороны рамки и место</p>','<h2>Что такое бронированные двери?</h2>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<p>Бронированные двери - это металлическая конструкция, обшитая листами стали с двух сторон, внутри которой установлены ребра жесткости, а пустое пространство заполнено пенопластом или минеральной ватой. Принято считать, что металлические бронированные двери отлично защищают от взлома и других внешних воздействий. Но, бронедвери обладают как плюсами так и минусами.</p>\n<h3>Преимущества бронированных дверей</h3>\n<ul>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>Бронированные двери лучше защищают от огня при пожаре, чем обычные деревянные двери.</li>\n<li>В отличие от деревянных дверей, металлическая конструкция бронедверей не деформируется со временем.</li>\n<li>Установив металлическую бронированную дверь, человек чувствует себя в безопасности.</li>\n</ul>\n<p><img src=\"images/news-img.jpeg\" alt=\"\" /></p>\n<h4>Недостатки бронированных дверей</h4>\n<ul>\n<li>Все вышеперечисленные достоинства окажутся бесполезными, если не уделить достаточное внимание замкам и защитным механизмам. Даже самую хорошую дверь злоумышленники смогут вскрыть, если установить дешевые и простые замки.</li>\n<li>В чрезвычайной ситуации возможно затруднительное попадание спасателей.</li>\n</ul>\n<h2>Признаки качественной бронированной двери</h2>\n<p>Качественная бронированная дверь зависит от толщины обшивки, от наличия защитной фурнитуры, а также от замочной конструкции. Если правильно учесть все тонкости этих элементов, можно получить отличный надежный вариант качественной бронированной двери.</p>\n<p>При выборе бронированных дверей нужно обращать внимание на толщину листов. Оптимальным вариантом является толщина листа 2 - 2,5 мм. Двери с толщиной 3 - 4 мм довольно тяжелые и обычно используются для загородных домов и коттеджей. Также бронированная дверь будет безопаснее, если металлические листы, которыми обшита дверная конструкция, цельные, без швов. Такие двери намного дороже, но и качественнее.</p>\n<h4>Тепло- и звукоизоляционный слой</h4>\n<ul>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Для того чтобы бронированные двери хорошо сохраняли тепло в вашем доме, а также защищали от посторонних шумов и звуков, двери утепляют различными дополнительными материалами.</li>\n<li>Наиболее востребованным изолирующим материалом является минеральная вата. Такой материал экологически безвреден, хорошо защищает от посторонних шумов, не подвержен горению, не выводит тепло из квартиры и не пропускает холод с улицы.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Для изоляции еще используют пенопласт. Он влагоустойчивый, хорошо защищает от посторонних звуков, но пенопласт не достаточно пожаробезопасный материал, и со временем он будет пропускать холод в квартиру.</li>\n<li>Достичь эффективной теплоизоляции можно при помощи качественно изготовленного изделия. Дверь должна хорошо прилегать к коробу.</li>\n<li>Еще одним способом изоляции является отделка металлических дверей декоративными панелями МДФ.</li>\n</ul>\n<h2>Ребра жесткости и другая дополнительная защита</h2>\n<p>Ребра жесткости повышают защитные свойства двери. Так как дверное полотно имеет большую площадь, ребра жесткости защищают полотно, повышают его взломостойкость, предотвращают деформацию полотна от внешних нагрузок. Кроме того, что они улучшают защитные свойства дверного блока, ребра жесткости создают толщину. Между листами образуется пространство, которое заполняется изоляционным материалом.</p>\n<p>Дополнительной защитой для бронированных дверей являются петли и наличники. Лучше всего когда петли находятся внутри дверной конструкции, в таком случае их невозможно будет сбить или срезать. Но, если они расположены снаружи, для их дополнительной защиты используют противосъемные штыри. Количество петель всегда зависит от веса двери.</p>\n<h4>Замки</h4>\n<p>Для входных дверей выделяются следующие разновидности замков:</p>\n<ul>\n<li>Классическим механизмом запирания являются сувальдные замки. Особенность этого замка состоит в специальных пластинах. Для максимальной защиты от взлома лучше всего использовать бронированные конструкции с числом от 6 до 8 сувальд.</li>\n<li>Самым популярным механизмом запирания являются цилиндровые замки. Внутри замочного механизма расположены несколько небольших цилиндра, в них и заключается принцип работы. Важно отметить, что для дверей с таким замком нужно еще устанавливать дополнительные бронированные накладки, что обезопасит цилиндровую конструкцию от выбивания.</li>\n<li>Замок с моноблочной конструкцией объединяет в своей работе сувальдный и цилиндровый механизмы. Принцип работы заключается в закрывании цилиндрового замка совместно с поднятием шторки в сувальдном механизме. Данная конструкция повышает уровень взломостойкости дверного полотна.</li>\n<li>Электромеханические замки не пользуются огромным спросом, из-за достаточно высокой стоимости, но многие специалисты считают что электромеханические замки наиболее практичны и надежны. Принцип работы такого механизма основан на вводе комбинации цифр или букв на электронной панели. К тому же используется еще и физический ключ. В случае, если вы введете код неправильно, только физическим ключом дверь не открыть.</li>\n<li>Для того, чтобы открыть дверь с кодовым замком ключи не потребуются, необходима лишь комбинация цифр. Но, это же выступает их большим недостатком. Так как со временем эксплуатации цифры стираются,</li>\n</ul>\n<h2>Как выбрать отделку фасада бронедверей</h2>\n<h3>Для отделки фасадов бронедверей используют различные материалы</h3>\n<ul>\n<li>панели МДФ</li>\n<li>кожвинил</li>\n<li>краска</li>\n</ul>\n<p>Один из вариантов отделки - панели МДФ. Материал обладает такими свойствами: устойчив к воздействию влаги и температурным перепадам, имеет хороший эстетический внешний вид, обладает высоким уровнем звуко- и теплоизоляции.</p>\n<p>Для отделки входных дверей, также используют кожвинил. Материал обладает такими свойствами: простой в эксплуатации, имеет неплохой уровень звуко- и теплоизоляции. Кожвинил бывает гладкий и тисненный. Достаточно распространенный и бюджетный вариант.</p>\n<p>В качестве отделки бронедверей часто выбирают покраску. Такой вид отделки обеспечивает двери устойчивостью к влаге, УФ-лучам, придает дверному полотну антикоррозийных свойств. Для отделки фасадов используют порошковые краски, молотковые или нитроцеллюлозные эмали. Каждый красящий состав имеет свои особенности.</p>\n<p>Перед тем, как выбрать бронированную дверь, рекомендуем вам изучить все тонкости и особенности этого изделия, ознакомиться с отзывами покупателей и, в итоге, приобрести качественный товар, который прослужит много лет.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('6','ua','Як вибрати броньовані двері','<p>З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','<h2>Що таке броньовані двері?</h2>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<h3>Переваги броньованих дверей</h3>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<h4>Недоліки броньованих дверей</h4>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<h2>Ознаки якісної броньованих дверей</h2>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<h4>Тепло- та звукоізоляційний шар</h4>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<h2>Ребра жорсткості і інша додатковий захист</h2>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<h4>Замки</h4>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються</li>\n</ul>\n<h2>Як вибрати обробку фасаду бронедверей</h2>\n<h3>Для обробки фасадів бронедверей використовують різні матеріали</h3>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('7','ru','тестовая новость','<p>РУ</p>\n<p>ОписаниеОписаниеОписаниеОписаниеОписание</p>\n<p>Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;</p>','<p>Контент&nbsp;КонтентКонтентКонтент<br />Контент&nbsp;Контент&nbsp;Контент&nbsp;</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('7','ua','Тестова новина','<p>ЮА</p>\n<p>ОписаниеОписаниеОписаниеОписаниеОписание</p>\n<p>Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;Описание&nbsp;</p>','<p>ЮА</p>\n<p>Контент&nbsp;КонтентКонтентКонтент<br />Контент&nbsp;Контент&nbsp;Контент&nbsp;</p>','','','','','noindex,nofollow');

INSERT INTO `modx_a_new_strings` VALUES ('8','ru','Название','<p>Описание</p>','<p>Контент</p>','title','description','keywords','canonical','index,follow');

INSERT INTO `modx_a_new_strings` VALUES ('8','ua','Тест новость','<p>Описание</p>','<p>Контент</p>','','','','','index,follow');


# --------------------------------------------------------

#
# Table structure for table `modx_a_news`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_news`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `alias` varchar(255) NOT NULL COMMENT 'Alias',
  `image` varchar(255) DEFAULT NULL COMMENT 'Image link',
  `published` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Published',
  `gallery` text COMMENT 'Gallery',
  `home_at` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Show at home page',
  `news_products` varchar(255) DEFAULT NULL,
  `news_category` varchar(255) DEFAULT NULL,
  `published_at` int(10) NOT NULL DEFAULT '0' COMMENT 'Published time',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Create time',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Update time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `published_time` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_news`
#

INSERT INTO `modx_a_news` VALUES ('1','kak-vybrat-bronirovannye-dveri','0549518001567768545.jpeg','1',NULL,'0','','','1565049600','1567768485','1571141367');

INSERT INTO `modx_a_news` VALUES ('2','kak-vybrat-bronirovannye-dveri-2','0401605001567770551.jpg','1',NULL,'0','1,3','','1565136000','1567768485','1571141238');

INSERT INTO `modx_a_news` VALUES ('4','kak-vybrat-bronirovannye-dveri-3','0854921001567770542.jpeg','1',NULL,'0','1,2,3','','1565222400','1567768485','1571141207');

INSERT INTO `modx_a_news` VALUES ('5','kak-vybrat-bronirovannye-dveri-4','0201763001567770524.jpg','1',NULL,'0','2','','1565308800','1567768485','1571141184');

INSERT INTO `modx_a_news` VALUES ('6','kak-vybrat-bronirovannye-dveri-5','0789943001568013374.png','1',NULL,'0','1','64','1568073600','1567768485','1571140718');

INSERT INTO `modx_a_news` VALUES ('7','testova-novina','0900306001571126619.jpg','1',NULL,'0','31,32','64','1571097600','1571126599','1571140871');

INSERT INTO `modx_a_news` VALUES ('8','test-novost','0994541001571744331.jpg','1',NULL,'0','36,37,38','51','1571702400','1571744313','1571744462');


# --------------------------------------------------------

#
# Table structure for table `modx_a_order`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_order`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_order` (
  `order_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_created` datetime DEFAULT NULL,
  `order_status` tinyint(4) DEFAULT '1' COMMENT '0 del 1 new 2 work 3 done',
  `order_status_pay` tinyint(4) DEFAULT '0' COMMENT '0 wait pay 1 payed',
  `order_client_id` int(10) DEFAULT NULL,
  `order_client` text,
  `order_manager` int(10) DEFAULT '0',
  `order_delivery` text,
  `order_delivery_cost` int(10) DEFAULT NULL,
  `order_installation` text,
  `order_installation_cost` int(10) DEFAULT NULL,
  `order_cost` float(9,2) DEFAULT NULL,
  `order_currency` varchar(8) DEFAULT NULL,
  `order_comment` text,
  `order_code` varchar(64) DEFAULT '',
  `order_discount` int(11) DEFAULT '0',
  `order_key` char(32) DEFAULT NULL,
  `order_lang` varchar(8) NOT NULL DEFAULT 'ua',
  `order_buy_click` tinyint(1) DEFAULT '0',
  `order_individual` tinyint(1) DEFAULT NULL,
  `order_payment` int(255) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_order`
#

INSERT INTO `modx_a_order` VALUES ('140','2019-10-17 10:32:27','1','0',NULL,'{\"phone\":\"+38(095)4171287\",\"name\":\"\\u0410\\u043d\\u043d\\u0430\",\"email\":\"avdeeva@artjoker.net\",\"area\":\"\\u041b\\u0456\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','1','200','1','300','69020.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('138','2019-10-08 17:56:14','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"hofara\",\"email\":\"pasytiq@mailinator.com\",\"area\":\"\\u044b\\u044b\\u044b\\u0444\\u0444\\u043a\\u044b\\u0440\\u043a\\u044b\\u0438\\u044b\\u0432\\u043a\\u0438 wsegv. g\"}','0','0','200','1','300','1808.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('139','2019-10-14 13:17:38','1','0',NULL,'{\"phone\":\"+38(095)417\",\"name\":\"ntcn\",\"email\":\"avdeeva@aerjoker.net\",\"area\":\"wrwabwarbasrb\"}','0','1','200','0','0','11880.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('136','2019-10-08 17:51:32','1','0',NULL,'{\"phone\":\"+38(123)4567899\",\"name\":\"qweqasfq qwrqw qwrqwrqwrqwtqwg\",\"email\":\"a.gorban@artjoker.team\",\"area\":\"tot\"}','0','0','0','1','300','1808.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('137','2019-10-08 17:55:33','1','0',NULL,'{\"phone\":\"+38(907)7339500\",\"name\":\"wwwwww\",\"email\":\"\",\"area\":\"\\u0444\\u0443\\u0435\\u0440\\u0443\\u044b\\u0435\\u0432\\u0440\\u0442\\u044b\\u0432\\u0442\\u0435\"}','0','0','0','0','0','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('134','2019-10-08 17:48:47','1','0',NULL,'{\"phone\":\"+38(907)7339500\",\"name\":\"wwwwww\",\"email\":\"a.gorban@artjoker.team\",\"area\":\"\\u0444\\u0443\\u0435\\u0440\\u0443\\u044b\\u0435\\u0432\\u0440\\u0442\\u044b\\u0432\\u0442\\u0435\"}','0','0','0','0','0','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('135','2019-10-08 17:49:11','1','0',NULL,'{\"phone\":\"+38(907)7339500\",\"name\":\"wwwwww\",\"email\":\"a.gorban@artjoker.team\",\"area\":\"\\u0444\\u0443\\u0435\\u0440\\u0443\\u044b\\u0435\\u0432\\u0440\\u0442\\u044b\\u0432\\u0442\\u0435\"}','0','0','0','0','0','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('132','2019-10-08 17:42:22','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"kevugix\",\"email\":\"qobyjubixy@mailinator.net\",\"area\":\"tot\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('133','2019-10-08 17:46:45','1','0',NULL,'{\"phone\":\"+38(121)1111111\",\"name\":\"111111111111111111111111111\",\"email\":\"a.gorban@artjoker.team\",\"area\":\"tot\"}','0','1','200','0','0','1708.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('130','2019-10-08 17:38:43','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"kevugix\",\"email\":\"qobyjubixy@mailinator.net\",\"area\":\"tot\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('131','2019-10-08 17:41:16','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"kevugix\",\"email\":\"qobyjubixy@mailinator.net\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('129','2019-10-08 17:37:54','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('127','2019-10-08 17:36:57','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('128','2019-10-08 17:37:36','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('125','2019-10-08 17:23:05','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('126','2019-10-08 17:36:16','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('124','2019-10-08 17:22:50','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('122','2019-10-08 17:20:36','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('123','2019-10-08 17:22:42','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('120','2019-10-08 16:47:35','1','0',NULL,'{\"phone\":\"+38(907)7339500\",\"name\":\"\",\"area\":\"\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0440\\u0430\\u0439\\u043e\\u043d\\u0430\"}','0','0','0','0','0','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('121','2019-10-08 17:18:17','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"sixobomu\",\"email\":\"supa@mailinator.com\",\"area\":\"\\u044b\\u0430\\u0442\\u0442\\u044b\\u044f\\u0442\\u044b\\u044f\\u043a\\u0442\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('117','2019-10-08 16:17:28','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"gibexe\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('118','2019-10-08 16:18:01','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"gibexe\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('119','2019-10-08 16:18:40','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"gibexe\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('115','2019-10-08 16:16:39','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"papoxos\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('116','2019-10-08 16:16:46','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"gibexe\",\"area\":\"tot\"}','0','1','200','1','300','1808.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('112','2019-10-08 16:13:38','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"papoxos\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('113','2019-10-08 16:14:40','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"papoxos\",\"area\":\"tot\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('114','2019-10-08 16:16:24','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wewegatyn\",\"area\":\"\\u044f\\u044b\\u043a\\u0442\\u044f\\u044b\\u043a\\u0442\\u044b\\u0432\\u0442\\u044b\\u0432\"}','0','1','200','1','300','1708.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('109','2019-10-08 16:00:43','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"cagutuq\",\"area\":\"ewe\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('110','2019-10-08 16:01:35','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"hekyl\",\"area\":\"135312616\"}','0','1','200','1','300','2008.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('111','2019-10-08 16:01:56','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"hekyl\",\"area\":\"135312616\"}','0','1','200','1','300','1508.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('108','2019-10-08 16:00:20','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"cagutuq\",\"area\":\"ewe\"}','0','1','200','1','300','1708.00','uah','{\"112\":\"qwertyu\",\"87\":\"\\u0432\\u043f\\u0442\\u044f\\u0432\\u0442\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('104','2019-10-08 15:50:21','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('105','2019-10-08 15:50:42','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('106','2019-10-08 15:51:09','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('107','2019-10-08 15:52:10','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('101','2019-10-08 15:48:55','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('102','2019-10-08 15:49:26','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('103','2019-10-08 15:50:06','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('97','2019-10-08 15:47:43','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','2452.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('98','2019-10-08 15:48:11','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('99','2019-10-08 15:48:22','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('100','2019-10-08 15:48:29','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"wwwwww\",\"area\":\"wwwwwww\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('96','2019-10-08 15:47:31','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('95','2019-10-08 15:47:02','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('94','2019-10-08 15:46:04','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('93','2019-10-08 15:45:50','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('92','2019-10-08 15:44:36','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('91','2019-10-08 15:44:18','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('90','2019-10-08 15:40:06','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('89','2019-10-08 15:35:41','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('88','2019-10-08 15:33:37','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','1952.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('87','2019-10-08 15:33:04','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"rugebeh\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','2152.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('86','2019-10-08 15:17:44','1','0',NULL,'null','0','1','200','1','300','1.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('85','2019-10-08 15:17:07','1','0',NULL,'null','0','1','200','1','300','1.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('84','2019-10-08 15:13:27','1','0',NULL,'null','0','1','200','1','300','1.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('83','2019-10-08 15:13:25','1','0',NULL,'null','0','1','200','1','300','1.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('82','2019-10-08 15:12:58','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"coqevoke\",\"area\":\"ewe\"}','0','1','200','1','300','2152.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('81','2019-10-08 15:11:19','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"xinolik\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','2452.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('80','2019-10-08 15:08:39','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"xinolik\",\"area\":\"aerttjaetj\"}','0','0','0','0','0','1652.00','uah','{\"112\":\"qwertyu\"}','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('141','2019-10-22 11:31:21','1','0',NULL,'{\"phone\":\"+38(999)9999999\",\"name\":\"Test\",\"email\":\"\",\"area\":\"\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0440\\u0430\\u0439\\u043e\\u043d\\u0430\"}','0','0','0','0','0','2000.00','uah','null','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('142','2019-10-22 11:50:37','1','0',NULL,'{\"phone\":\"+38(232)1213213\",\"name\":\"\\u0444\\u044b\\u0432\\u0444\\u044b\\u0432\\u044b\\u0444\",\"email\":\"denis15baibak@gmail.com\",\"area\":\"\\u0442\\u0435\\u0441\\u0442\\u043e\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','1','200','1','300','12824.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('143','2019-10-22 15:20:30','1','0',NULL,'{\"phone\":\"+38(666)7788888\",\"name\":\"\\u043e\\u043b\\u043e\\u043b\\u043e\\u043b\",\"email\":\"\",\"area\":\"\\u041d\\u0430\\u0437\\u0432\\u0430 \\u0440\\u0430\\u0439\\u043e\\u043d\\u0443\"}','0','0','0','0','0','2000.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('144','2019-10-23 17:11:40','1','0',NULL,'{\"phone\":\"+38(044)2312321\",\"name\":\"\\u044b\\u0444\\u0432\\u044b\\u0444\\u0432\",\"email\":\"denis15baibak@gmail.com\",\"area\":\"\\u0426\\u0435\\u043d\\u0442\\u0440\"}','0','1','200','1','300','60217.00','uah','null','','0',NULL,'ru','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('145','2019-10-23 17:16:12','1','0',NULL,'{\"phone\":\"+38(232)1312312\",\"name\":\"\\u044b\\u0432\\u0444\\u0444\\u044b\\u0432\\u044b\\u0444\\u0432\",\"email\":\"denis15baibak@gmail.com\",\"area\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','1','200','1','300','14679.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('146','2019-10-23 17:17:07','1','0',NULL,'{\"phone\":\"+38(213)1312312\",\"name\":\"asdasdsad\",\"email\":\"denis15baibak@gmail.com\",\"area\":\"\\u041f\\u0440\\u0430\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','1','200','1','300','14679.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('147','2019-10-23 17:46:23','1','0',NULL,'{\"phone\":\"+38(21)3213213\",\"name\":\"\\u0432\\u044b\\u0444\\u0432\",\"email\":\"\",\"area\":\"\\u041d\\u0430\\u0437\\u0432\\u0430 \\u0440\\u0430\\u0439\\u043e\\u043d\\u0443\"}','0','0','0','0','0','42940.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('148','2019-10-30 11:02:54','1','0',NULL,'{\"phone\":\"+38(099)9253859\",\"name\":\"\\u0420\\u043e\\u043c\\u0430\\u043d\",\"email\":\"r.ratinov@artjoker.net\",\"area\":\"\\u041b\\u0456\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','1','200','1','300','620.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('149','2019-10-31 14:05:01','1','0',NULL,'{\"phone\":\"+38(213)2132132\",\"name\":\"sad\",\"email\":\"denis15baibak@gmail.com\",\"area\":\"\\u041b\\u0456\\u0432\\u0438\\u0439 \\u0431\\u0435\\u0440\\u0435\\u0433\"}','0','0','0','0','0','400.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('150','2019-10-31 16:47:06','1','0',NULL,'{\"phone\":\"+38(1)1111111\",\"name\":\"niquvicux\",\"email\":\"vedytuhumu@mailinator.net\",\"area\":\"aerttjaetj\"}','0','1','200','1','300','2510.00','uah','null','','0',NULL,'ua','0',NULL,NULL);

INSERT INTO `modx_a_order` VALUES ('151','2019-10-31 17:54:02','1','0',NULL,'{\"phone\":\"+38(111)1111\",\"name\":\"tozyvujon\",\"email\":\"pihy@mailinator.com\",\"area\":\"\"}','0','1','200','0','0','11360.00','uah','null','','0',NULL,'ua','0',NULL,NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_order_products`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_order_products`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_order_products` (
  `order_id` int(10) NOT NULL DEFAULT '0',
  `product_id` int(10) DEFAULT '0',
  `option_id` int(255) NOT NULL,
  `product_count` int(10) DEFAULT '0',
  `product_price` float(9,2) DEFAULT '0.00',
  UNIQUE KEY `order_id_product_id_option_id` (`order_id`,`product_id`,`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_order_products`
#

INSERT INTO `modx_a_order_products` VALUES ('140','32','135','4','3000.00');

INSERT INTO `modx_a_order_products` VALUES ('140','31','132','4','14130.00');

INSERT INTO `modx_a_order_products` VALUES ('139','31','131','1','11680.00');

INSERT INTO `modx_a_order_products` VALUES ('138','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('138','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('137','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('137','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('136','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('136','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('135','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('135','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('134','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('134','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('133','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('133','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('132','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('132','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('131','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('131','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('130','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('130','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('129','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('127','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('127','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('126','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('126','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('125','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('125','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('124','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('124','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('123','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('123','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('122','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('122','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('121','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('121','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('120','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('120','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('119','2','112','2','222.00');

INSERT INTO `modx_a_order_products` VALUES ('119','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('118','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('117','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('116','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('115','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('114','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('112','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('110','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('109','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('108','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('107','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('106','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('105','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('104','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('103','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('102','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('101','2','112','4','222.00');

INSERT INTO `modx_a_order_products` VALUES ('101','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('100','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('99','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('98','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('97','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('96','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('95','2','112','4','222.00');

INSERT INTO `modx_a_order_products` VALUES ('95','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('94','2','112','4','222.00');

INSERT INTO `modx_a_order_products` VALUES ('94','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('93','2','112','4','222.00');

INSERT INTO `modx_a_order_products` VALUES ('93','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('92','2','112','4','222.00');

INSERT INTO `modx_a_order_products` VALUES ('92','1','87','2','532.00');

INSERT INTO `modx_a_order_products` VALUES ('141','36','142','1','2000.00');

INSERT INTO `modx_a_order_products` VALUES ('142','40','148','1','12324.00');

INSERT INTO `modx_a_order_products` VALUES ('143','36','142','1','2000.00');

INSERT INTO `modx_a_order_products` VALUES ('144','40','147','2','400.00');

INSERT INTO `modx_a_order_products` VALUES ('145','40','147','1','400.00');

INSERT INTO `modx_a_order_products` VALUES ('146','40','147','1','400.00');

INSERT INTO `modx_a_order_products` VALUES ('147','31','131','1','11680.00');

INSERT INTO `modx_a_order_products` VALUES ('148','55','174','1','120.00');

INSERT INTO `modx_a_order_products` VALUES ('149','40','147','1','400.00');

INSERT INTO `modx_a_order_products` VALUES ('150','31','131','68','11680.00');

INSERT INTO `modx_a_order_products` VALUES ('150','31','132','80','14130.00');

INSERT INTO `modx_a_order_products` VALUES ('150','31','133','5','17130.00');

INSERT INTO `modx_a_order_products` VALUES ('151','55','174','93','120.00');


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_categories` (
  `product_id` int(10) NOT NULL,
  `modx_id` int(10) NOT NULL,
  UNIQUE KEY `product_id_modx_id` (`product_id`,`modx_id`),
  KEY `modx_id` (`modx_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_categories`
#

INSERT INTO `modx_a_product_categories` VALUES ('55','23');

INSERT INTO `modx_a_product_categories` VALUES ('34','46');

INSERT INTO `modx_a_product_categories` VALUES ('1','47');

INSERT INTO `modx_a_product_categories` VALUES ('20','47');

INSERT INTO `modx_a_product_categories` VALUES ('35','47');

INSERT INTO `modx_a_product_categories` VALUES ('46','47');

INSERT INTO `modx_a_product_categories` VALUES ('47','47');

INSERT INTO `modx_a_product_categories` VALUES ('49','47');

INSERT INTO `modx_a_product_categories` VALUES ('50','47');

INSERT INTO `modx_a_product_categories` VALUES ('66','47');

INSERT INTO `modx_a_product_categories` VALUES ('52','48');

INSERT INTO `modx_a_product_categories` VALUES ('8','50');

INSERT INTO `modx_a_product_categories` VALUES ('3','51');

INSERT INTO `modx_a_product_categories` VALUES ('8','51');

INSERT INTO `modx_a_product_categories` VALUES ('17','51');

INSERT INTO `modx_a_product_categories` VALUES ('31','51');

INSERT INTO `modx_a_product_categories` VALUES ('37','51');

INSERT INTO `modx_a_product_categories` VALUES ('40','51');

INSERT INTO `modx_a_product_categories` VALUES ('56','51');

INSERT INTO `modx_a_product_categories` VALUES ('57','51');

INSERT INTO `modx_a_product_categories` VALUES ('58','51');

INSERT INTO `modx_a_product_categories` VALUES ('59','51');

INSERT INTO `modx_a_product_categories` VALUES ('60','51');

INSERT INTO `modx_a_product_categories` VALUES ('61','51');

INSERT INTO `modx_a_product_categories` VALUES ('62','51');

INSERT INTO `modx_a_product_categories` VALUES ('63','51');

INSERT INTO `modx_a_product_categories` VALUES ('67','51');

INSERT INTO `modx_a_product_categories` VALUES ('68','51');

INSERT INTO `modx_a_product_categories` VALUES ('69','51');

INSERT INTO `modx_a_product_categories` VALUES ('70','51');

INSERT INTO `modx_a_product_categories` VALUES ('71','51');

INSERT INTO `modx_a_product_categories` VALUES ('72','51');

INSERT INTO `modx_a_product_categories` VALUES ('73','51');

INSERT INTO `modx_a_product_categories` VALUES ('74','51');

INSERT INTO `modx_a_product_categories` VALUES ('8','52');

INSERT INTO `modx_a_product_categories` VALUES ('32','52');

INSERT INTO `modx_a_product_categories` VALUES ('36','52');

INSERT INTO `modx_a_product_categories` VALUES ('38','52');

INSERT INTO `modx_a_product_categories` VALUES ('64','52');

INSERT INTO `modx_a_product_categories` VALUES ('65','52');

INSERT INTO `modx_a_product_categories` VALUES ('75','52');

INSERT INTO `modx_a_product_categories` VALUES ('76','52');

INSERT INTO `modx_a_product_categories` VALUES ('77','52');

INSERT INTO `modx_a_product_categories` VALUES ('78','52');

INSERT INTO `modx_a_product_categories` VALUES ('79','52');

INSERT INTO `modx_a_product_categories` VALUES ('80','52');

INSERT INTO `modx_a_product_categories` VALUES ('81','52');

INSERT INTO `modx_a_product_categories` VALUES ('82','52');

INSERT INTO `modx_a_product_categories` VALUES ('83','52');

INSERT INTO `modx_a_product_categories` VALUES ('84','52');

INSERT INTO `modx_a_product_categories` VALUES ('85','52');

INSERT INTO `modx_a_product_categories` VALUES ('86','52');

INSERT INTO `modx_a_product_categories` VALUES ('87','52');

INSERT INTO `modx_a_product_categories` VALUES ('88','52');

INSERT INTO `modx_a_product_categories` VALUES ('89','52');

INSERT INTO `modx_a_product_categories` VALUES ('90','52');

INSERT INTO `modx_a_product_categories` VALUES ('91','52');

INSERT INTO `modx_a_product_categories` VALUES ('92','52');

INSERT INTO `modx_a_product_categories` VALUES ('98','52');

INSERT INTO `modx_a_product_categories` VALUES ('99','52');

INSERT INTO `modx_a_product_categories` VALUES ('100','52');

INSERT INTO `modx_a_product_categories` VALUES ('101','52');

INSERT INTO `modx_a_product_categories` VALUES ('1','58');

INSERT INTO `modx_a_product_categories` VALUES ('2','58');

INSERT INTO `modx_a_product_categories` VALUES ('20','58');

INSERT INTO `modx_a_product_categories` VALUES ('46','58');

INSERT INTO `modx_a_product_categories` VALUES ('47','58');

INSERT INTO `modx_a_product_categories` VALUES ('49','58');

INSERT INTO `modx_a_product_categories` VALUES ('66','58');

INSERT INTO `modx_a_product_categories` VALUES ('32','61');

INSERT INTO `modx_a_product_categories` VALUES ('76','61');

INSERT INTO `modx_a_product_categories` VALUES ('77','61');

INSERT INTO `modx_a_product_categories` VALUES ('78','61');

INSERT INTO `modx_a_product_categories` VALUES ('79','61');

INSERT INTO `modx_a_product_categories` VALUES ('80','61');

INSERT INTO `modx_a_product_categories` VALUES ('81','61');

INSERT INTO `modx_a_product_categories` VALUES ('82','61');

INSERT INTO `modx_a_product_categories` VALUES ('83','61');

INSERT INTO `modx_a_product_categories` VALUES ('84','61');

INSERT INTO `modx_a_product_categories` VALUES ('85','61');

INSERT INTO `modx_a_product_categories` VALUES ('86','61');

INSERT INTO `modx_a_product_categories` VALUES ('87','61');

INSERT INTO `modx_a_product_categories` VALUES ('88','61');

INSERT INTO `modx_a_product_categories` VALUES ('89','61');

INSERT INTO `modx_a_product_categories` VALUES ('90','61');

INSERT INTO `modx_a_product_categories` VALUES ('91','61');

INSERT INTO `modx_a_product_categories` VALUES ('92','61');

INSERT INTO `modx_a_product_categories` VALUES ('1','64');

INSERT INTO `modx_a_product_categories` VALUES ('2','64');

INSERT INTO `modx_a_product_categories` VALUES ('3','64');

INSERT INTO `modx_a_product_categories` VALUES ('4','64');

INSERT INTO `modx_a_product_categories` VALUES ('5','64');

INSERT INTO `modx_a_product_categories` VALUES ('6','64');

INSERT INTO `modx_a_product_categories` VALUES ('7','64');

INSERT INTO `modx_a_product_categories` VALUES ('9','64');

INSERT INTO `modx_a_product_categories` VALUES ('10','64');

INSERT INTO `modx_a_product_categories` VALUES ('11','64');

INSERT INTO `modx_a_product_categories` VALUES ('12','64');

INSERT INTO `modx_a_product_categories` VALUES ('13','64');

INSERT INTO `modx_a_product_categories` VALUES ('16','64');

INSERT INTO `modx_a_product_categories` VALUES ('20','64');

INSERT INTO `modx_a_product_categories` VALUES ('46','64');

INSERT INTO `modx_a_product_categories` VALUES ('47','64');

INSERT INTO `modx_a_product_categories` VALUES ('49','64');

INSERT INTO `modx_a_product_categories` VALUES ('50','64');

INSERT INTO `modx_a_product_categories` VALUES ('66','64');

INSERT INTO `modx_a_product_categories` VALUES ('3','67');

INSERT INTO `modx_a_product_categories` VALUES ('48','78');

INSERT INTO `modx_a_product_categories` VALUES ('50','79');


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_filter_values`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_filter_values`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_filter_values` (
  `product_id` int(10) NOT NULL,
  `filter_id` int(10) NOT NULL,
  `value_id` int(10) NOT NULL,
  UNIQUE KEY `product_id_filter_id_value_id` (`product_id`,`filter_id`,`value_id`),
  KEY `product_id` (`product_id`),
  KEY `filter_id` (`filter_id`),
  KEY `value_id` (`value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_filter_values`
#

INSERT INTO `modx_a_product_filter_values` VALUES ('1','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('1','22','62');

INSERT INTO `modx_a_product_filter_values` VALUES ('1','22','67');

INSERT INTO `modx_a_product_filter_values` VALUES ('1','22','68');

INSERT INTO `modx_a_product_filter_values` VALUES ('1','22','69');

INSERT INTO `modx_a_product_filter_values` VALUES ('1','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('3','2','12');

INSERT INTO `modx_a_product_filter_values` VALUES ('3','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('3','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('3','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('5','2','16');

INSERT INTO `modx_a_product_filter_values` VALUES ('5','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('5','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('5','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('49','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('49','22','62');

INSERT INTO `modx_a_product_filter_values` VALUES ('49','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('50','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('50','29','85');

INSERT INTO `modx_a_product_filter_values` VALUES ('50','30','77');

INSERT INTO `modx_a_product_filter_values` VALUES ('50','31','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('50','32','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('66','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('66','22','62');

INSERT INTO `modx_a_product_filter_values` VALUES ('66','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('85','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('85','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('85','8','27');

INSERT INTO `modx_a_product_filter_values` VALUES ('85','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('86','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('86','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('86','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('86','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','2','16');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','8','27');

INSERT INTO `modx_a_product_filter_values` VALUES ('89','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('91','2','5');

INSERT INTO `modx_a_product_filter_values` VALUES ('91','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('91','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('91','7','24');

INSERT INTO `modx_a_product_filter_values` VALUES ('95','2','1');

INSERT INTO `modx_a_product_filter_values` VALUES ('95','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('95','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('95','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('95','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','2','17');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','6','31');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','7','24');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('97','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('98','2','1');

INSERT INTO `modx_a_product_filter_values` VALUES ('98','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('98','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('98','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('98','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','2','17');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','6','30');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('101','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','2','1');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','5','10');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','6','29');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','7','23');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','8','27');

INSERT INTO `modx_a_product_filter_values` VALUES ('110','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('112','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('112','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('112','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('112','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('113','5','9');

INSERT INTO `modx_a_product_filter_values` VALUES ('113','6','29');

INSERT INTO `modx_a_product_filter_values` VALUES ('113','8','27');

INSERT INTO `modx_a_product_filter_values` VALUES ('113','9','28');

INSERT INTO `modx_a_product_filter_values` VALUES ('131','10','33');

INSERT INTO `modx_a_product_filter_values` VALUES ('131','11','36');

INSERT INTO `modx_a_product_filter_values` VALUES ('131','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('131','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('131','14','40');

INSERT INTO `modx_a_product_filter_values` VALUES ('132','10','33');

INSERT INTO `modx_a_product_filter_values` VALUES ('132','11','35');

INSERT INTO `modx_a_product_filter_values` VALUES ('132','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('132','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('132','14','40');

INSERT INTO `modx_a_product_filter_values` VALUES ('133','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('133','11','34');

INSERT INTO `modx_a_product_filter_values` VALUES ('133','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('133','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('133','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('134','15','44');

INSERT INTO `modx_a_product_filter_values` VALUES ('134','16','49');

INSERT INTO `modx_a_product_filter_values` VALUES ('134','17','53');

INSERT INTO `modx_a_product_filter_values` VALUES ('134','18','56');

INSERT INTO `modx_a_product_filter_values` VALUES ('134','19','57');

INSERT INTO `modx_a_product_filter_values` VALUES ('135','15','44');

INSERT INTO `modx_a_product_filter_values` VALUES ('135','16','48');

INSERT INTO `modx_a_product_filter_values` VALUES ('135','17','51');

INSERT INTO `modx_a_product_filter_values` VALUES ('135','19','59');

INSERT INTO `modx_a_product_filter_values` VALUES ('135','28','70');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','15','41');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','16','45');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','17','50');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','18','56');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','19','57');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','28','70');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','33','81');

INSERT INTO `modx_a_product_filter_values` VALUES ('136','33','82');

INSERT INTO `modx_a_product_filter_values` VALUES ('140','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('140','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('140','21','61');

INSERT INTO `modx_a_product_filter_values` VALUES ('141','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('141','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('141','21','61');

INSERT INTO `modx_a_product_filter_values` VALUES ('142','6','29');

INSERT INTO `modx_a_product_filter_values` VALUES ('142','8','27');

INSERT INTO `modx_a_product_filter_values` VALUES ('142','21','84');

INSERT INTO `modx_a_product_filter_values` VALUES ('143','6','29');

INSERT INTO `modx_a_product_filter_values` VALUES ('143','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('143','21','61');

INSERT INTO `modx_a_product_filter_values` VALUES ('145','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('145','11','34');

INSERT INTO `modx_a_product_filter_values` VALUES ('145','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('145','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('145','14','40');

INSERT INTO `modx_a_product_filter_values` VALUES ('146','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('146','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('146','21','61');

INSERT INTO `modx_a_product_filter_values` VALUES ('148','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('148','11','35');

INSERT INTO `modx_a_product_filter_values` VALUES ('148','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('148','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('148','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('149','10','33');

INSERT INTO `modx_a_product_filter_values` VALUES ('149','11','34');

INSERT INTO `modx_a_product_filter_values` VALUES ('149','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('149','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('149','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('154','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('154','22','62');

INSERT INTO `modx_a_product_filter_values` VALUES ('154','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('155','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('155','22','67');

INSERT INTO `modx_a_product_filter_values` VALUES ('155','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('156','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('156','22','68');

INSERT INTO `modx_a_product_filter_values` VALUES ('156','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('157','2','2');

INSERT INTO `modx_a_product_filter_values` VALUES ('157','22','69');

INSERT INTO `modx_a_product_filter_values` VALUES ('157','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('158','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('158','29','85');

INSERT INTO `modx_a_product_filter_values` VALUES ('158','30','77');

INSERT INTO `modx_a_product_filter_values` VALUES ('158','31','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('158','32','80');

INSERT INTO `modx_a_product_filter_values` VALUES ('159','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('159','29','85');

INSERT INTO `modx_a_product_filter_values` VALUES ('159','30','73');

INSERT INTO `modx_a_product_filter_values` VALUES ('159','31','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('159','32','80');

INSERT INTO `modx_a_product_filter_values` VALUES ('160','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('160','29','76');

INSERT INTO `modx_a_product_filter_values` VALUES ('160','30','77');

INSERT INTO `modx_a_product_filter_values` VALUES ('160','31','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('160','32','80');

INSERT INTO `modx_a_product_filter_values` VALUES ('161','23','63');

INSERT INTO `modx_a_product_filter_values` VALUES ('161','29','76');

INSERT INTO `modx_a_product_filter_values` VALUES ('161','30','73');

INSERT INTO `modx_a_product_filter_values` VALUES ('161','31','74');

INSERT INTO `modx_a_product_filter_values` VALUES ('161','32','80');

INSERT INTO `modx_a_product_filter_values` VALUES ('172','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('193','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('193','11','34');

INSERT INTO `modx_a_product_filter_values` VALUES ('196','15','41');

INSERT INTO `modx_a_product_filter_values` VALUES ('196','16','49');

INSERT INTO `modx_a_product_filter_values` VALUES ('196','17','52');

INSERT INTO `modx_a_product_filter_values` VALUES ('196','28','70');

INSERT INTO `modx_a_product_filter_values` VALUES ('196','33','81');

INSERT INTO `modx_a_product_filter_values` VALUES ('198','2','1');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','6','30');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','11','35');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('199','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('230','10','33');

INSERT INTO `modx_a_product_filter_values` VALUES ('230','11','35');

INSERT INTO `modx_a_product_filter_values` VALUES ('230','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('230','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('230','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('231','10','32');

INSERT INTO `modx_a_product_filter_values` VALUES ('231','11','34');

INSERT INTO `modx_a_product_filter_values` VALUES ('231','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('231','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('231','14','39');

INSERT INTO `modx_a_product_filter_values` VALUES ('232','10','33');

INSERT INTO `modx_a_product_filter_values` VALUES ('232','11','36');

INSERT INTO `modx_a_product_filter_values` VALUES ('232','12','37');

INSERT INTO `modx_a_product_filter_values` VALUES ('232','13','38');

INSERT INTO `modx_a_product_filter_values` VALUES ('232','14','40');

INSERT INTO `modx_a_product_filter_values` VALUES ('233','35','0');

INSERT INTO `modx_a_product_filter_values` VALUES ('233','36','0');

INSERT INTO `modx_a_product_filter_values` VALUES ('233','37','0');

INSERT INTO `modx_a_product_filter_values` VALUES ('233','38','0');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','6','22');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','8','26');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','21','84');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','35','87');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','36','88');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','37','89');

INSERT INTO `modx_a_product_filter_values` VALUES ('237','38','90');

INSERT INTO `modx_a_product_filter_values` VALUES ('248','10','102');


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_image_fields`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_image_fields`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_image_fields` (
  `image_field_id` int(10) NOT NULL AUTO_INCREMENT,
  `image_image_id` int(10) NOT NULL,
  `image_lang` char(8) NOT NULL DEFAULT 'ua',
  `image_alt` varchar(128) NOT NULL,
  `image_title` varchar(128) NOT NULL,
  `image_description` varchar(512) NOT NULL,
  PRIMARY KEY (`image_field_id`),
  UNIQUE KEY `image_image_id_image_lang` (`image_image_id`,`image_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_image_fields`
#

INSERT INTO `modx_a_product_image_fields` VALUES ('1','24','ru','test','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('2','24','en','test','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('3','24','ua','test','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('4','25','ru','dgsdg','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('5','25','en','sdgsdg','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('6','25','ua','ghfghfgh','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('7','56','ru','ДСП','ДСП','ДСП');

INSERT INTO `modx_a_product_image_fields` VALUES ('8','56','en','','ДСП','');

INSERT INTO `modx_a_product_image_fields` VALUES ('9','56','ua','','ДСП','');

INSERT INTO `modx_a_product_image_fields` VALUES ('10','57','ru','КРТ','КРТ','КРТ');

INSERT INTO `modx_a_product_image_fields` VALUES ('11','57','en','','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('12','57','ua','','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('19','55','ru','АВТ','АВТ','АВТ');

INSERT INTO `modx_a_product_image_fields` VALUES ('20','55','en','','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('21','55','ua','','','');

INSERT INTO `modx_a_product_image_fields` VALUES ('22','89','ua','альт','название','описание');

INSERT INTO `modx_a_product_image_fields` VALUES ('23','89','ru','','','');


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_images`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_images`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_images` (
  `image_id` int(10) NOT NULL AUTO_INCREMENT,
  `image_product_id` int(10) NOT NULL,
  `image_file` varchar(64) NOT NULL,
  `image_position` int(10) NOT NULL DEFAULT '0',
  `image_use` varchar(64) NOT NULL,
  `image_comment` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `image_product_id_image_file` (`image_product_id`,`image_file`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_images`
#

INSERT INTO `modx_a_product_images` VALUES ('27','5','0197191001565157978.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('28','5','0745552001565157981.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('29','5','0869611001565157986.jpg','10','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('30','5','0911530001565157986.jpg','11','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('31','5','0951779001565157986.jpg','12','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('32','5','0992187001565157986.jpg','13','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('33','5','0030060001565157987.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('34','5','0068677001565157987.jpg','2','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('35','5','0111567001565157987.jpg','3','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('36','5','0145049001565157987.jpg','4','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('37','5','0181480001565157987.jpg','5','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('38','5','0226144001565157987.jpg','6','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('39','5','0278514001565157987.jpg','7','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('40','5','0366659001565157987.jpg','8','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('41','5','0441115001565157987.jpg','9','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('42','5','0554884001565354965.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('43','5','0569782001565354965.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('44','11','0760817001567154856.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('45','11','0780184001567154858.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('46','11','0593592001567154862.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('47','11','0912248001567154865.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('48','11','0254641001567155955.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('49','11','0724417001567155960.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('50','11','0670291001567155963.jpg','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('51','13','0437834001567430410.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('52','13','0027305001567430413.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('53','13','0655263001567430415.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('55','13','0391047001567513569.png','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('56','13','0101706001567513574.png','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('57','13','0216965001567513577.png','0','product_tag',NULL);

INSERT INTO `modx_a_product_images` VALUES ('58','16','0582814001568110563.png','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('59','6','0332593001568110705.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('60','6','0971079001568110707.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('62','7','0961637001568116552.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('76','31','0210580001571047907.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('77','31','0522170001571047917.jpg','2','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('78','31','0739697001571047917.jpg','3','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('79','31','0011738001571047918.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('80','32','0047612001571048602.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('81','32','0297379001571048613.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('82','32','0416928001571048613.jpg','2','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('83','32','0586347001571048613.jpg','3','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('84','34','0550037001571145843.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('85','34','0635873001571145848.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('86','34','0621388001571145852.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('87','36','0644360001571150255.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('88','36','0947690001571150265.jpg','4','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('89','36','0034812001571150266.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('90','36','0092410001571150266.jpg','2','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('91','36','0184262001571150266.jpg','3','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('92','37','0481136001571152350.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('93','38','0500813001571152496.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('94','48','0205717001571227847.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('126','56','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('127','57','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('128','58','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('129','59','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('130','60','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('131','61','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('132','62','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('133','63','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('134','64','0500813001571152496.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('135','65','0500813001571152496.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('136','66','0031300001571654600.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('137','66','0293930001571654599.jpeg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('138','66','0518708001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('139','66','0589269001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('140','66','0696187001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('141','66','0785075001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('142','66','0885250001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('143','66','0962257001571654599.jpeg','0','slider','');

INSERT INTO `modx_a_product_images` VALUES ('144','67','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('145','68','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('146','69','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('147','70','0316431001571739722.jpg','0','cover','');

INSERT INTO `modx_a_product_images` VALUES ('154','75','0845131001572340447.png','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('155','75','0370968001572340451.jpg','0','second',NULL);

INSERT INTO `modx_a_product_images` VALUES ('156','55','0250152001572509024.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('157','55','0051553001572509053.jpg','1','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('158','55','0068550001572509053.jpeg','2','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('159','55','0073073001572509053.jpg','3','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('187','50','0125517001572728034.jpg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('188','50','0394576001572728034.jpg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('189','50','0524697001572728034.jpg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('190','50','0613200001572728034.jpg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('191','50','0739310001572728034.jpg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('192','50','0807479001572728034.jpg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('193','49','0278193001572728292.jpeg','0','cover',NULL);

INSERT INTO `modx_a_product_images` VALUES ('194','49','0331957001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('195','49','0389298001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('196','49','0426339001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('197','49','0504864001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('198','49','0568049001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('199','49','0676041001572728292.jpeg','0','slider',NULL);

INSERT INTO `modx_a_product_images` VALUES ('200','49','0694303001572728292.jpeg','0','slider',NULL);


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_options`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_options`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) DEFAULT NULL,
  `product_code` varchar(64) NOT NULL DEFAULT '123',
  `position` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `availability` tinyint(1) DEFAULT '1',
  `default_option` tinyint(1) DEFAULT NULL,
  `price` float(9,0) NOT NULL DEFAULT '0',
  `price_old` float(9,0) NOT NULL DEFAULT '0',
  `ru_name` varchar(128) DEFAULT NULL,
  `en_name` varchar(128) DEFAULT NULL,
  `ua_name` varchar(128) DEFAULT NULL,
  `ru_description` text,
  `en_description` text,
  `ua_description` text,
  `ru_content` text,
  `en_content` text,
  `ua_content` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_product_default_option_product_code` (`id`,`product`,`default_option`,`product_code`)
) ENGINE=MyISAM AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_options`
#

INSERT INTO `modx_a_product_options` VALUES ('128','17','','0','1','1',NULL,'111','2222','1111',NULL,'1111','<p>1111</p>',NULL,'<p>1111</p>',NULL,NULL,NULL,'2019-10-11 11:17:16','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('254','101','4802-7','0','1','1','0','26230','0','Premium MyKey System',NULL,'Premium MyKey System','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('196','32','123','0','1','1','0','4000','5000','вариация',NULL,'вариация','',NULL,'',NULL,NULL,NULL,'2019-10-31 15:20:49','2019-10-31 16:39:36');

INSERT INTO `modx_a_product_options` VALUES ('231','31','','0','1','1','0','24660','0','Premium Rotor',NULL,'Premium Rotor','',NULL,'',NULL,NULL,NULL,'2019-11-02 14:59:13','2019-11-03 11:33:32');

INSERT INTO `modx_a_product_options` VALUES ('93','7','','0','1','1','0','45','436','Сет лета + Сет №19 с рваной свениной',NULL,'zsrh','<p>aevbshrbsr hasrysrdyn esr ydz</p>',NULL,'<p>zsrh zsdr hzdsr hzs rh&nbsp;</p>',NULL,NULL,NULL,'2019-07-28 16:11:06','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('183','63','123','0','1','1','1','4243','23444',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:29:13','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('184','64','123','0','1','1','1','213','435',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:29:41','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('179','59','123','0','1','1','1','1233','657',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:28:10','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('180','60','123','0','1','1','1','475','457',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:28:21','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('181','61','123','0','1','1','1','3245','658',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:28:30','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('182','62','123','0','1','1','1','5467','65765',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:28:41','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('173','38','123','0','1','1',NULL,'200','250',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:13:19','2019-10-22 13:13:19');

INSERT INTO `modx_a_product_options` VALUES ('174','55','123','0','1','1','1','120','148','test',NULL,'test',NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:20:03','2019-10-22 13:20:03');

INSERT INTO `modx_a_product_options` VALUES ('175','56','123','0','1','1','1','0','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:26:10','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('177','57','123','0','1','1','1','213213','213213',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:27:42','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('178','58','123','0','1','1','1','213123','1222',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:27:52','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('111','7','','0','1','1','1','444','333','Сет лета + Сет №19 с рваной свениной',NULL,'Сет лета + Сет №19 с рваной свениной','<p>Сет лета + Сет №19 с рваной свениной</p>',NULL,'<p>Сет лета + Сет №19 с рваной свениной</p>',NULL,NULL,NULL,'2019-09-10 13:20:04','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('252','101','4802-5','0','1','1','0','17270','0','Premium Secureme',NULL,'Premium Secureme','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('195','75','123','0','1','1','1','11500','13540','',NULL,'Мела D',NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-29 11:11:39','2019-10-29 11:11:39');

INSERT INTO `modx_a_product_options` VALUES ('232','31','391706-2','0','1','1','0','26230','0','Premium MyKey System',NULL,'Premium MyKey System','',NULL,'',NULL,NULL,NULL,'2019-11-02 15:00:15','2019-11-03 11:33:32');

INSERT INTO `modx_a_product_options` VALUES ('127','30','','0','0','0',NULL,'0','0','11111',NULL,'2345','',NULL,'',NULL,NULL,NULL,'2019-10-10 13:10:58','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('132','31','391706-3','0','1','1','0','16750','0','Standard Plus',NULL,'Standard Plus','',NULL,'',NULL,NULL,NULL,'2019-10-14 13:13:03','2019-11-02 17:55:21');

INSERT INTO `modx_a_product_options` VALUES ('131','31','391706-4','0','1','1','0','14990','0','Standard Hook',NULL,'Standard Hook','',NULL,'',NULL,NULL,NULL,'2019-10-14 13:10:16','2019-11-02 17:55:21');

INSERT INTO `modx_a_product_options` VALUES ('133','31','391706-5','0','1','1','0','18220','0','Prestige',NULL,'Prestige','',NULL,'',NULL,NULL,NULL,'2019-10-14 13:15:18','2019-11-02 17:55:21');

INSERT INTO `modx_a_product_options` VALUES ('230','31','','0','1','1','0','17270','0','Premium Securemme',NULL,'Premium Securemme','',NULL,'',NULL,NULL,NULL,'2019-11-02 14:53:07','2019-11-02 17:55:21');

INSERT INTO `modx_a_product_options` VALUES ('135','32','','0','1','1','0','3000','4000','Рокка 1',NULL,'Рокка 1','',NULL,'<p>1111111111111111111</p>',NULL,NULL,NULL,'2019-10-14 13:33:37','2019-10-31 15:20:03');

INSERT INTO `modx_a_product_options` VALUES ('136','32','','0','1','1','1','3500','0','Рокка 2',NULL,'Рокка 2 ','',NULL,'',NULL,NULL,NULL,'2019-10-14 13:34:34','2019-10-31 16:48:44');

INSERT INTO `modx_a_product_options` VALUES ('190','70','123','0','1','1','1','2134','2131',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:31:31','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('189','69','123','0','1','1','1','2342','4354',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:31:13','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('140','36','','0','1','1','0','1500','2200','супер дверь с козырьком',NULL,'супер дверь с козырьком','',NULL,'',NULL,NULL,NULL,'2019-10-15 17:35:53','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('141','36','','0','1','1','0','1000','1500','супер дверь с красной ручкой',NULL,'супер дверь с красной ручкой','<p>Duis distinctio. Cor.</p>',NULL,'<p>Iste eum laboriosam.</p>',NULL,NULL,NULL,'2019-10-15 17:36:25','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('142','36','','0','1','1','1','2000','2500','супер дверь с двумя замками',NULL,'супер дверь с двумя замками','<p>Repudiandae tenetur .</p>',NULL,'<p>Reprehenderit itaque.</p>',NULL,NULL,NULL,'2019-10-15 17:36:54','2019-11-01 11:57:24');

INSERT INTO `modx_a_product_options` VALUES ('143','36','','0','1','0','0','1700','1900','супер двойная дверь',NULL,'супер двойная дверь','<p>Nesciunt, eu dolor r.</p>',NULL,'<p>Consectetur, aut lib.</p>',NULL,NULL,NULL,'2019-10-15 17:37:14','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('188','68','123','0','1','1','1','342','3242',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:31:02','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('187','67','123','0','1','1','1','2134','4343',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:30:33','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('147','40','','0','1','1','1','400','500','тест',NULL,'еуыфв','',NULL,'',NULL,NULL,NULL,'2019-10-15 18:16:17','2019-10-18 16:42:15');

INSERT INTO `modx_a_product_options` VALUES ('148','40','','0','1','1','0','12324','12321','вариация 1',NULL,'вариация 1','',NULL,'',NULL,NULL,NULL,'2019-10-15 18:16:36','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('149','40','','0','1','0','0','1455','1233','вариация 2',NULL,'вариация 2','',NULL,'',NULL,NULL,NULL,'2019-10-15 18:21:14','2019-10-17 16:00:57');

INSERT INTO `modx_a_product_options` VALUES ('154','49','4815162342-1','0','1','1','1','5999','6999','Стандарт',NULL,'Стандарт','Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери ',NULL,'Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері ',NULL,NULL,NULL,'2019-10-17 17:53:43','2019-11-02 22:53:49');

INSERT INTO `modx_a_product_options` VALUES ('155','49','4815162342-2','0','1','1','0','5800','0','Стандарт плюс',NULL,'Стандарт плюс','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:53:43','2019-11-02 22:53:49');

INSERT INTO `modx_a_product_options` VALUES ('156','49','4815162342-3','0','1','1','0','5700','0','Стандарт єкстра',NULL,'Стандарт єкстра','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:53:43','2019-10-17 18:21:02');

INSERT INTO `modx_a_product_options` VALUES ('157','49','4815162342-4','0','1','1','0','5600','0','Премиум',NULL,'Преміум','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:53:44','2019-10-17 17:53:44');

INSERT INTO `modx_a_product_options` VALUES ('158','50','A230385-1','0','1','1','1','10000','16000','',NULL,'','Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые ',NULL,'Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові ',NULL,NULL,NULL,'2019-10-17 17:58:31','2019-11-02 22:53:52');

INSERT INTO `modx_a_product_options` VALUES ('159','50','A230385-2','0','1','1','0','11000','0','',NULL,'','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:58:32','2019-10-17 17:58:32');

INSERT INTO `modx_a_product_options` VALUES ('160','50','A230385-3','0','1','1','0','12000','0','',NULL,'','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:58:32','2019-10-17 17:58:32');

INSERT INTO `modx_a_product_options` VALUES ('161','50','A230385-4','0','1','1','0','13000','0','',NULL,'','',NULL,'',NULL,NULL,NULL,'2019-10-17 17:58:32','2019-10-17 17:58:32');

INSERT INTO `modx_a_product_options` VALUES ('253','101','4802-6','0','1','1','0','24660','0','Premium Rotor',NULL,'Premium Rotor','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('172','37','123','0','1','1','1','200','250','Название',NULL,'Название','',NULL,'',NULL,NULL,NULL,'2019-10-22 13:12:46','2019-11-01 12:46:33');

INSERT INTO `modx_a_product_options` VALUES ('185','65','123','0','1','1','1','2343','3544',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:29:55','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('168','52','123','0','1','1',NULL,'123','0','Название вариации',NULL,'Название вариации','',NULL,'',NULL,NULL,NULL,'2019-10-18 17:24:51','2019-10-18 17:24:51');

INSERT INTO `modx_a_product_options` VALUES ('186','66','123','0','1','1','1','2134','213',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-22 13:30:13','2019-11-01 13:51:57');

INSERT INTO `modx_a_product_options` VALUES ('170','54','123','0','1','1','1','0','0','Название ru',NULL,' Название ua',NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-18 17:38:44','2019-10-18 17:38:44');

INSERT INTO `modx_a_product_options` VALUES ('197','49','123','0','0','1',NULL,'100','200','1235',NULL,'1235','',NULL,'',NULL,NULL,NULL,'2019-10-31 15:47:54','2019-10-31 15:47:54');

INSERT INTO `modx_a_product_options` VALUES ('198','49','123','0','1','0',NULL,'0','0','7777777',NULL,'7777777','',NULL,'',NULL,NULL,NULL,'2019-10-31 15:53:18','2019-10-31 15:53:18');

INSERT INTO `modx_a_product_options` VALUES ('199','31','391706-1','0','1','1','1','13850','0','Standart',NULL,'Standart','<p>Описание</p>',NULL,'<p>Описание</p>',NULL,NULL,NULL,'2019-11-01 11:05:52','2019-11-03 11:33:32');

INSERT INTO `modx_a_product_options` VALUES ('244','100','4801-1','0','1','1','1','8150','8150','Vero Pro',NULL,'Vero Pro','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('245','100','4801-2','0','1','1','0','8700','8700','Standard',NULL,'Standard','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('246','100','4801-3','0','1','1','0','9400','9400','Standard Hook',NULL,'Standard Hook','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('247','100','4801-4','0','1','1','0','11','11','Premium',NULL,'Premium','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('248','101','4802-1','0','1','1','1','13850','0','Standard',NULL,'Standard','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 13:05:12');

INSERT INTO `modx_a_product_options` VALUES ('249','101','4802-2','0','1','1','0','14990','0','Standard Hook',NULL,'Standard Hook','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('250','101','4802-3','0','1','1','0','16750','0','Standard Plus',NULL,'Standard Plus','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');

INSERT INTO `modx_a_product_options` VALUES ('251','101','4802-4','0','1','1','0','18220','0','Prestige',NULL,'Prestige','',NULL,'',NULL,NULL,NULL,'2019-11-03 12:24:17','2019-11-03 12:24:17');


# --------------------------------------------------------

#
# Table structure for table `modx_a_product_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_product_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_product_strings` (
  `string_id` int(10) NOT NULL AUTO_INCREMENT,
  `string_locate` varchar(4) NOT NULL,
  `product_id` int(10) NOT NULL,
  `product_name` varchar(512) NOT NULL,
  `product_introtext` text,
  `product_additional` text,
  `product_description` text NOT NULL,
  `product_content` mediumtext,
  `product_seo_title` varchar(128) DEFAULT NULL,
  `product_seo_description` varchar(264) DEFAULT NULL,
  `product_seo_content` varchar(512) DEFAULT NULL,
  `product_seo_keywords` varchar(128) DEFAULT NULL,
  `product_seo_canonical` varchar(264) DEFAULT NULL,
  `product_seo_robots` varchar(32) NOT NULL DEFAULT 'noindex,nofollow',
  PRIMARY KEY (`string_id`),
  UNIQUE KEY `str_locate_product_id` (`string_locate`,`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=586 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_product_strings`
#

INSERT INTO `modx_a_product_strings` VALUES ('22','ru','5','Шкаф-кровать HELFER-90-V','','<p>Дополнительная информация</p>','<p>Текст описание</p>','<p>Контент текст</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('23','en','5','Шкаф-кровать HELFER-90-V','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('24','ua','5','Шкаф-кровать HELFER-90-V','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('28','ru','6','Товар 6','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('29','en','6','fdhdfh','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('30','ua','6','hdfhdfh','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('49','ru','7','Товар 5','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('50','en','7','gdsgsdg','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('51','ua','7','dsfsdfs','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('52','ru','8','Товар 4','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('53','en','8','gdsg','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('54','ua','8','gdgsdsg','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('58','ru','9','Товар 1','','<p>Доп информация о товаре</p>','<p>Небольшой текст</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('59','en','9','yrtrturtu','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('60','ua','9','erteryery','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('79','ru','10','pm_test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('80','en','10','pm_test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('81','ua','10','pm_test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('91','ru','11','Товар 3','<p>анотация&nbsp;</p>','<p>доп инфо&nbsp;</p>','<p>описание&nbsp;</p>','<p>контент&nbsp;</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('92','en','11','апоапо','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('93','ua','11','апоапо','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('127','ru','13','Contrary to popular belief, Lorem Ipsum is not simply random text. It has r','<ol>\n<li>Contrary to popular belief,</li>\n<li>Lorem Ipsum is not simply</li>\n<li>random text. It has roots in a</li>\n</ol>\n<ul>\n<li>piece of classical Latin literatu</li>\n<li>re from 45Contrary to popular</li>\n<li>belief, Lorem Ipsum is not simp</li>\n</ul>\n<blockquote>\n<p>ly random text. It has roots in a piece of class</p>\n</blockquote>\n<p>ical Latin literature from12Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12аа</p>','<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12аа</p>','<ol>\n<li>Contrary to popular belief,</li>\n<li>Lorem Ipsum is not simply</li>\n<li>random text. It has roots in a</li>\n</ol>\n<ul>\n<li>piece of classical Latin literatu</li>\n<li>re from 45Contrary to popular</li>\n<li>belief, Lorem Ipsum is not simp</li>\n</ul>\n<blockquote>\n<p>ly random text. It has roots in a piece of class</p>\n</blockquote>\n<p>ical Latin literature from12Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12аа</p>','<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from12аа</p>','TEST_INPUT','TEST_INPUT','<p>TEST_INPUT</p>','TEST_INPUT','TEST_INPUT','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('128','en','13','','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('129','ua','13','','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('160','ru','16','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('161','en','16','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('162','ua','16','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('197','ru','17','Товар групповой','<p>ТЕСТ</p>','','<p>ТЕСТ</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('198','ua','17','ТЕСТ','<p>ТЕСТ</p>','','<p>ТЕСТ</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('215','ua','31','Andora','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('216','ru','31','Andora','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('219','ua','32','Рокка','','','<p>Отличительной особенностью коллекции&nbsp;<b>\"Интера Delux\"</b>&nbsp;является прочная, элегантная и долговечная ПВХ пленка толщиной 0,24 мм. (с которой Вы уже знакомы по коллекции &laquo;Маэстра&raquo;). А &laquo;изюминкой&raquo; выступает фрезеровка рисунка на филенчатых элементах двери. Это позволяет создать уют и красоту в Вашем доме.</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('220','ru','32','Рокка','','','<p>Отличительной особенностью коллекции&nbsp;<b>\"Интера Delux\"</b>&nbsp;является прочная, элегантная и долговечная ПВХ пленка толщиной 0,24 мм. (с которой Вы уже знакомы по коллекции &laquo;Маэстра&raquo;). А &laquo;изюминкой&raquo; выступает фрезеровка рисунка на филенчатых элементах двери. Это позволяет создать уют и красоту в Вашем доме.</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('231','ua','34','test','<p>test</p>','<p>test</p>','<p>test</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('232','ru','34','test','<p>test</p>','<p>test</p>','<p>test</p>','<p>test</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('237','ua','35','test','<p>test</p>','<p>testr</p>','<p>test</p>','<p>test</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('238','ru','35','test','<p>test</p>','<p>test</p>','<p>test</p>','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('239','ua','36','Супер дверь','<p>Accusamus aut id in .</p>','<p>Accusamus tenetur es.</p>','<p>A rerum adipisci dol.</p>','<p>Est minima aliquam m.</p>','Est facilis quas vol','Esse eaque quia quia','<p>Harum velit sed debi</p>','Duis quisquam et exc','Molestiae voluptates','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('240','ru','36','Супер дверь','<p>Explicabo. Deleniti .</p>','<p>Id nisi sapiente nes.</p>','<p>Odio quidem velit mo.</p>','<p>Magni dolor ut aliqu.</p>','Ad asperiores eligen','Consequatur proident','<p>In aliquid quis ut n</p>','Et aut vel excepteur','Dolores aut dignissi','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('261','ua','37','обычная дверь','<p>Аннотация</p>','<p>Аннотация</p>','<p>Аннотация</p>','<p>Аннотация</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('262','ru','37','обычная дверь','<p>Аннотация</p>','<p>Аннотация</p>','<p>Аннотация</p>','<p>Аннотация</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('265','ua','38','Груповая одиночная дверь','<p>Аннотация</p>','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('266','ru','38','Груповая одиночная дверь','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('269','ua','40','групповой вариативный дверь','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('270','ru','40','групповой вариативный дверь','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('285','ua','48','Denton Beasley','<p>Itaque nihil occaeca.</p>','<p>Sit, laudantium, com.</p>','<p>Ducimus, explicabo. .</p>','<p>Laborum quia vitae d.</p>','Cum sed aut ex paria','Commodo ut vel ea au','<p>Aut aut quae officii</p>','Sed et aut hic numqu','Modi reprehenderit r','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('286','ru','48','Xander Drake','<p>Quas culpa, ipsam au.</p>','<p>Voluptatem sit, saep.</p>','<p>Ipsum, mollit sit ad.</p>','<p>Voluptas similique c.</p>','Repellendus Aute qu','Minus ea elit volup','<p>Lorem recusandae Re</p>','Quibusdam deserunt v','Et aut optio veniam','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('287','ua','49','Двері міжкімнатні EXPO Light Standart','','','Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері ','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('288','ru','49','Двери межкомнатные EXPO Light Standart','','','Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери ','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('289','ua','50','Ворота Листові','','','Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові Ворота листові ','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('290','ru','50','Ворота листовые','','','Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые Ворота Листовые ','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('305','ua','54',' Название ua','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('306','ru','54','Название ru','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('315','ua','55','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('316','ru','55','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('319','ua','56','Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('320','ru','56','Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('326','ua','57','Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('327','ru','57','Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('331','ua','58','Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('332','ru','58','Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('338','ua','59','Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('339','ru','59','Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('343','ua','60','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('344','ru','60','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('348','ua','61','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('349','ru','61','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('353','ua','62','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('354','ru','62','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('358','ua','63','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('359','ru','63','Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('363','ua','64','Duplicate of Груповая одиночная дверь','<p>Аннотация</p>','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('364','ru','64','Duplicate of Груповая одиночная дверь','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('368','ua','65','Duplicate of Duplicate of Груповая одиночная дверь','<p>Аннотация</p>','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('369','ru','65','Duplicate of Duplicate of Груповая одиночная дверь','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('373','ua','66','Duplicate of Двері міжкімнатні EXPO Light Standart','','','<p>Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері Міжкімнатні двері</p>','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('374','ru','66','Duplicate of Двери межкомнатные EXPO Light Standart','','','<p>Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери Межкомнатные двери</p>','','','','','','','noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('378','ua','67','Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('379','ru','67','Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('383','ua','68','Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('384','ru','68','Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('388','ua','69','Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('389','ru','69','Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('393','ua','70','Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('394','ru','70','Duplicate of Duplicate of Duplicate of Duplicate of test','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('419','ua','75','Мела D','<p>Короб металл / глубина (мм)</p>','<p>Нижняя дверь</p>','','<p>контент</p>','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('420','ru','75','','','','','','','','','','','index,follow');

INSERT INTO `modx_a_product_strings` VALUES ('582','ua','100','Трэк',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('583','ru','100','Трэк',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('584','ua','101','Expo light',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'noindex,nofollow');

INSERT INTO `modx_a_product_strings` VALUES ('585','ru','101','Expo light',NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'noindex,nofollow');


# --------------------------------------------------------

#
# Table structure for table `modx_a_products`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_products`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_products` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_price` float(9,0) NOT NULL DEFAULT '0',
  `product_price_old` float(9,0) NOT NULL DEFAULT '0',
  `product_weight` float(9,2) NOT NULL DEFAULT '0.00',
  `product_sale` int(10) NOT NULL DEFAULT '0',
  `product_rating` int(10) NOT NULL DEFAULT '0',
  `product_code` varchar(64) DEFAULT NULL,
  `product_recommender` varchar(64) DEFAULT NULL,
  `product_look_like` varchar(255) DEFAULT NULL,
  `product_status` tinyint(4) NOT NULL DEFAULT '0',
  `product_active` tinyint(4) NOT NULL DEFAULT '0',
  `product_type` tinyint(4) NOT NULL DEFAULT '0',
  `product_availability` tinyint(4) NOT NULL DEFAULT '0',
  `product_main_slider_new` tinyint(4) NOT NULL DEFAULT '0',
  `product_main_slider_discont` tinyint(4) NOT NULL DEFAULT '0',
  `product_main_slider_popular` tinyint(4) NOT NULL DEFAULT '0',
  `product_url` varchar(512) NOT NULL,
  `product_parameter` varchar(512) DEFAULT NULL,
  `product_discont` varchar(64) DEFAULT '',
  `product_category` int(10) NOT NULL DEFAULT '0',
  `product_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_products`
#

INSERT INTO `modx_a_products` VALUES ('5','0','0','0.00','0','0','H2556BT','3','7','0','1','1','1','1','1','1','test1',NULL,'','64','2019-07-27 17:49:26','2019-10-09 10:44:44');

INSERT INTO `modx_a_products` VALUES ('6','0','0','0.00','0','0','H2556BT','','','1','1','1','1','1','1','1','gdfg',NULL,'','22','2019-07-27 18:11:24','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('7','0','0','0.00','0','0','H2556BT','13,3','3,6','1','1','1','1','1','1','1','sdgsdg',NULL,'','22','2019-07-28 16:11:06','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('8','0','0','0.00','0','0','H2556BT','3,1','7,2','0','1','1','1','1','1','1','dsdsg',NULL,'','4','2019-07-28 16:14:41','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('9','0','0','0.00','0','0','H2556BT','16','15','1','1','1','1','1','1','1','tterert',NULL,'','22','2019-07-28 16:43:20','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('10','0','0','0.00','0','0','32646466','','','0','1','1','1','1','1','1','pmtest',NULL,'','4','2019-07-31 11:56:58','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('11','0','0','0.00','0','0','32646466','7','2','1','1','1','1','1','1','1','rparoapo',NULL,'','22','2019-07-31 13:08:53','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('13','0','0','0.00','0','0','22334455','10','10','1','1','1','1','1','1','1','testtest',NULL,'','22','2019-09-02 13:17:49','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('16','0','0','0.00','0','0','123','10','10','3','1','1','1','1','1','1','test',NULL,'','64','2019-09-09 13:05:30','2019-10-07 14:44:09');

INSERT INTO `modx_a_products` VALUES ('17','0','0','0.00','0','0','123456','9,8,1','13,10,9','2','0','0','0','0','0','0','tovar-gruppovoj',NULL,'','51','2019-10-10 10:32:51','2019-10-10 10:32:51');

INSERT INTO `modx_a_products` VALUES ('31','0','0','0.00','0','0','391706','','','0','1','0','1','0','0','0','andora',NULL,'','51','2019-10-14 13:10:16','2019-10-17 10:23:38');

INSERT INTO `modx_a_products` VALUES ('32','0','0','0.00','0','0','391923','','','0','1','1','1','1','0','0','rokka',NULL,'','52','2019-10-14 13:22:28','2019-10-31 16:45:10');

INSERT INTO `modx_a_products` VALUES ('34','0','0','0.00','0','0','14422','20','20','0','1','0','1','0','1','0','test2',NULL,'','46','2019-10-15 16:08:03','2019-10-15 16:27:07');

INSERT INTO `modx_a_products` VALUES ('35','0','0','0.00','0','0','2134124','31','16','0','1','0','1','1','0','0','test3',NULL,'','46','2019-10-15 17:27:27','2019-10-15 17:27:27');

INSERT INTO `modx_a_products` VALUES ('36','0','0','0.00','0','0','test artikul','31','31','5','1','1','1','1','0','0','karina-molina',NULL,'','52','2019-10-15 17:35:53','2019-10-15 18:02:46');

INSERT INTO `modx_a_products` VALUES ('37','0','0','0.00','0','0','обычный','36','36','0','1','0','1','0','0','0','obychnaya-dver',NULL,'','51','2019-10-15 18:12:01','2019-11-01 12:28:52');

INSERT INTO `modx_a_products` VALUES ('38','0','0','0.00','0','0','груповой обычный','37','37','0','1','0','1','0','1','0','grupovaya-odinochnaya-dver',NULL,'','52','2019-10-15 18:14:33','2019-10-22 13:13:19');

INSERT INTO `modx_a_products` VALUES ('40','0','0','0.00','0','0','не обычный','38','38','5','1','0','1','0','0','0','gruppovoj-variativnyj-dver',NULL,'','51','2019-10-15 18:16:17','2019-10-18 16:42:28');

INSERT INTO `modx_a_products` VALUES ('48','0','0','0.00','0','0','Quam beatae dolor cu','13','13','0','1','1','0','1','1','0','in-libero-sed-deseru',NULL,'','78','2019-10-16 15:10:28','2019-10-16 15:11:14');

INSERT INTO `modx_a_products` VALUES ('49','5999','6999','0.00','0','0','4815162342','','','1','1','0','1','0','0','0','dveri-mijkimnatni-expo-light-standart',NULL,'','58','2019-10-17 17:53:42','2019-11-02 22:53:48');

INSERT INTO `modx_a_products` VALUES ('50','10000','16000','0.00','0','0','a230385','','','1','1','1','1','0','0','0','vorota-listovi',NULL,'','79','2019-10-17 17:58:31','2019-11-02 22:53:51');

INSERT INTO `modx_a_products` VALUES ('54','0','0','0.00','0','0','','','','0','0','0','0','0','0','0','nazvanie-ua',NULL,'','4','2019-10-18 17:38:44','2019-10-18 17:38:44');

INSERT INTO `modx_a_products` VALUES ('55','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4',NULL,'','23','2019-10-22 13:20:03','2019-11-01 10:38:47');

INSERT INTO `modx_a_products` VALUES ('56','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:26:41');

INSERT INTO `modx_a_products` VALUES ('57','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:27:42');

INSERT INTO `modx_a_products` VALUES ('58','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:27:57');

INSERT INTO `modx_a_products` VALUES ('59','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:28:10');

INSERT INTO `modx_a_products` VALUES ('60','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:28:21');

INSERT INTO `modx_a_products` VALUES ('61','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:28:30');

INSERT INTO `modx_a_products` VALUES ('62','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:28:41');

INSERT INTO `modx_a_products` VALUES ('63','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:29:13');

INSERT INTO `modx_a_products` VALUES ('64','0','0','0.00','0','0','груповой обычный','37','37','0','1','0','1','0','1','0','grupovaya-odinochnaya-dver-duplicate',NULL,'','52','2019-10-15 18:14:33','2019-10-22 13:29:41');

INSERT INTO `modx_a_products` VALUES ('65','0','0','0.00','0','0','груповой обычный','37','37','0','1','0','1','0','1','0','grupovaya-odinochnaya-dver-duplicate-duplicate',NULL,'','52','2019-10-15 18:14:33','2019-10-22 13:29:55');

INSERT INTO `modx_a_products` VALUES ('66','0','0','0.00','0','0','4815162342','','','0','1','0','1','0','0','0','dveri-mijkimnatni-expo-light-standart-duplicate',NULL,'','58','2019-10-17 17:53:42','2019-10-22 13:30:13');

INSERT INTO `modx_a_products` VALUES ('67','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:30:33');

INSERT INTO `modx_a_products` VALUES ('68','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:31:02');

INSERT INTO `modx_a_products` VALUES ('69','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:31:13');

INSERT INTO `modx_a_products` VALUES ('70','0','0','0.00','0','0','test','13','13','0','1','0','1','0','0','1','test4-duplicate-duplicate-duplicate-duplicate',NULL,'','51','2019-10-22 13:20:03','2019-10-22 13:31:31');

INSERT INTO `modx_a_products` VALUES ('75','0','0','0.00','0','0','','','','3','1','0','1','0','1','0','mela-d',NULL,'','52','2019-10-29 11:11:39','2019-10-29 13:38:40');

INSERT INTO `modx_a_products` VALUES ('100','8150','8150','0.00','0','0','4801',NULL,NULL,'1','1','0','1','0','0','0','trek',NULL,'','52','2019-11-03 12:24:16','2019-11-03 12:24:16');

INSERT INTO `modx_a_products` VALUES ('101','13850','0','0.00','0','0','4802',NULL,NULL,'1','1','0','1','0','0','0','expo-light',NULL,'','52','2019-11-03 12:24:17','2019-11-03 12:24:17');


# --------------------------------------------------------

#
# Table structure for table `modx_a_redirect`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_redirect`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_redirect` (
  `redirect_id` int(11) NOT NULL AUTO_INCREMENT,
  `redirect_code` int(11) NOT NULL DEFAULT '301',
  `redirect_source` varchar(1024) NOT NULL,
  `redirect_target` varchar(1024) NOT NULL,
  PRIMARY KEY (`redirect_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_redirect`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_review_images`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_review_images`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_review_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) NOT NULL,
  `file` varchar(64) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `review_id_position` (`review_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_review_images`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_review_strings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_review_strings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_review_strings` (
  `string_id` int(11) NOT NULL AUTO_INCREMENT,
  `string_locate` varchar(4) NOT NULL,
  `review_id` int(11) NOT NULL,
  `review_user_name` varchar(512) NOT NULL,
  `review_content` mediumtext NOT NULL,
  PRIMARY KEY (`string_id`),
  UNIQUE KEY `str_locate_review_id` (`string_locate`,`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_review_strings`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_reviews`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_reviews`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_active` tinyint(4) NOT NULL DEFAULT '0',
  `review_home` tinyint(4) NOT NULL DEFAULT '0',
  `review_product` int(11) NOT NULL DEFAULT '0',
  `review_webuser` int(11) NOT NULL DEFAULT '0',
  `review_rating` int(11) NOT NULL DEFAULT '0',
  `review_cover` varchar(64) DEFAULT NULL,
  `review_user_email` varchar(64) DEFAULT NULL,
  `review_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `review_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`review_id`),
  KEY `review_product` (`review_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_a_reviews`
#


# --------------------------------------------------------

#
# Table structure for table `modx_a_wishlist`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_a_wishlist`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_a_wishlist` (
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_a_wishlist`
#


# --------------------------------------------------------

#
# Table structure for table `modx_active_user_locks`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_user_locks`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_user_locks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `firsthit` int(20) NOT NULL DEFAULT '0',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `element` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_element_id` (`internalKey`,`element`,`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COMMENT='Contains data about all elements that are locked by active users.';

#
# Dumping data for table `modx_active_user_locks`
#


# --------------------------------------------------------

#
# Table structure for table `modx_active_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_active_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `id` int(10) DEFAULT NULL,
  `action` varchar(10) NOT NULL DEFAULT '',
  `ip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data about active users.';

#
# Dumping data for table `modx_active_users`
#

INSERT INTO `modx_active_users` VALUES ('1','adminko','1572856751',NULL,'93','192.168.10.1');

INSERT INTO `modx_active_users` VALUES ('2','kolomak9','1572806795',NULL,'2','192.168.10.1');


# --------------------------------------------------------

#
# Table structure for table `modx_categories`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_categories`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='Categories to be used snippets,tv,chunks, etc';

#
# Dumping data for table `modx_categories`
#

INSERT INTO `modx_categories` VALUES ('1','Base');

INSERT INTO `modx_categories` VALUES ('2','Catalog');

INSERT INTO `modx_categories` VALUES ('3','Cart');

INSERT INTO `modx_categories` VALUES ('4','SEO');

INSERT INTO `modx_categories` VALUES ('5','Content');

INSERT INTO `modx_categories` VALUES ('6','Manager and Admin');

INSERT INTO `modx_categories` VALUES ('7','Navigation');

INSERT INTO `modx_categories` VALUES ('8','Search');

INSERT INTO `modx_categories` VALUES ('9','Mail');

INSERT INTO `modx_categories` VALUES ('10','Delivery');

INSERT INTO `modx_categories` VALUES ('11','1');

INSERT INTO `modx_categories` VALUES ('12','Popup');

INSERT INTO `modx_categories` VALUES ('13','Menu');

INSERT INTO `modx_categories` VALUES ('14','Сommon');

INSERT INTO `modx_categories` VALUES ('15','Product');


# --------------------------------------------------------

#
# Table structure for table `modx_document_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_document_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_document_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_document_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_documentgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_documentgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(4) DEFAULT '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) DEFAULT '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_documentgroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_event_log`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_event_log`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_event_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventid` int(11) DEFAULT '0',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL DEFAULT '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) NOT NULL DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=562 DEFAULT CHARSET=utf8mb4 COMMENT='Stores event and error logs';

#
# Dumping data for table `modx_event_log`
#


# --------------------------------------------------------

#
# Table structure for table `modx_keyword_xref`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_keyword_xref`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_keyword_xref` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `keyword_id` int(11) NOT NULL DEFAULT '0',
  KEY `content_id` (`content_id`),
  KEY `keyword_id` (`keyword_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Cross reference bewteen keywords and content';

#
# Dumping data for table `modx_keyword_xref`
#


# --------------------------------------------------------

#
# Table structure for table `modx_manager_log`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_manager_log`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_manager_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` int(20) NOT NULL DEFAULT '0',
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `action` int(10) NOT NULL DEFAULT '0',
  `itemid` varchar(10) DEFAULT '0',
  `itemname` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5324 DEFAULT CHARSET=utf8mb4 COMMENT='Contains a record of user interaction.';

#
# Dumping data for table `modx_manager_log`
#

INSERT INTO `modx_manager_log` VALUES ('2401','1568978225','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2402','1568978287','1','adminko','79','6','tpl_filterPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2403','1568978287','1','adminko','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2404','1568978316','1','adminko','79','6','tpl_filterPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2405','1568978316','1','adminko','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2406','1568978324','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2407','1568978326','1','adminko','79','6','tpl_filterPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2408','1568978326','1','adminko','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2409','1568979321','1','adminko','79','6','tpl_filterPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2410','1568979321','1','adminko','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2411','1568979322','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2412','1568979879','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2413','1568979886','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2414','1568979963','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2415','1568979966','1','adminko','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2416','1568979971','1','adminko','79','6','tpl_filterPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2417','1568979971','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2418','1568980080','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2419','1568980158','1','adminko','79','-','tpl_getOption','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2420','1568980158','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2421','1568980373','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2422','1568980373','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2423','1568980408','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2424','1568980743','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2425','1568980743','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2426','1568980899','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2427','1568980899','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2428','1568981247','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2429','1568981250','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2430','1568981419','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2431','1568981425','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2432','1568981434','1','adminko','97','56','Duplicate of tpl_getOption','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2433','1568981434','1','adminko','78','57','Duplicate of tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2434','1568981486','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2435','1568981488','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2436','1568981775','1','adminko','79','57','tpl_getOption_single','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2437','1568981775','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2438','1568982038','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2439','1568982053','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2440','1568982054','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2441','1568982276','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2442','1568982276','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2443','1568983015','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2444','1568983025','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2445','1568983033','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2446','1568983069','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2447','1568983069','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2448','1568983074','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2449','1568983150','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2450','1568983205','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2451','1568983205','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2452','1568984103','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2453','1568984118','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2454','1568984118','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2455','1568984122','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2456','1568984143','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2457','1568984190','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2458','1568984190','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2459','1568984219','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2460','1568984220','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2461','1568984517','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2462','1568984519','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2463','1568984533','1','adminko','79','4','tpl_filtersInner','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2464','1568984534','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2465','1568984542','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2466','1568984544','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2467','1568984598','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2468','1568984598','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2469','1568985227','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2470','1569248964','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('2471','1569248971','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2472','1569249005','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2473','1569249007','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2474','1569249220','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2475','1569249228','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2476','1569249255','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2477','1569249376','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2478','1569249376','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2479','1569249402','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2480','1569249412','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2481','1569249412','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2482','1569249418','1','adminko','27','4','Product','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2483','1569249455','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2484','1569249457','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2485','1569249466','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2486','1569249466','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2487','1569249474','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2488','1569249474','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2489','1569331500','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('2490','1569331503','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2491','1569331506','1','adminko','301','25','Background image','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2492','1569331511','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2493','1569416495','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('2494','1569416499','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2495','1569416914','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2496','1569416973','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2497','1569942424','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2498','1569942436','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2499','1569942536','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2500','1569942536','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2501','1569942539','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2502','1569942788','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2503','1569942788','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2504','1569942797','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2505','1569942804','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2506','1569943025','1','adminko','79','21','call-me','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2507','1569943025','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2508','1569943109','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2509','1569943113','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2510','1569943186','1','adminko','79','22','request-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2511','1569943186','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2512','1569943241','1','adminko','300','-','Новый параметр (TV)','Create Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2513','1569943243','1','adminko','301','10','General image','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2514','1569943245','1','adminko','304','10','tv_img','Duplicate Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2515','1569943245','1','adminko','301','31','Duplicate of General image','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2516','1569943273','1','adminko','302','31','Second image','Save Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2517','1569943273','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2518','1569943283','1','adminko','27','28','Гарантия и возврат','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2519','1569943291','1','adminko','5','28','Гарантия и возврат','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2520','1569943292','1','adminko','27','27','Оплата и доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2521','1569943297','1','adminko','5','27','Оплата и доставка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2522','1569943298','1','adminko','3','27','Оплата и доставка','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2523','1569943299','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2524','1569943302','1','adminko','16','14','pay-and-delivery','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2525','1569943317','1','adminko','20','14','pay-and-delivery','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2526','1569943317','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2527','1569943342','1','adminko','78','34','tpl_questions','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2528','1569943405','1','adminko','79','34','tpl_questions','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2529','1569943405','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2530','1570017237','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('2531','1570017241','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2532','1570017247','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2533','1570017250','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2534','1570017250','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2535','1570017251','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2536','1570017251','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2537','1570017251','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2538','1570017252','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2539','1570017257','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2540','1570017344','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2541','1570017347','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2542','1570017355','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2543','1570017356','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2544','1570017410','1','adminko','79','-','tpl_aria_option','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2545','1570017410','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2546','1570017495','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2547','1570017516','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2548','1570017516','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2549','1570017520','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2550','1570017538','1','adminko','79','21','call-me','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2551','1570017538','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2552','1570017541','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2553','1570017617','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2554','1570017654','1','adminko','79','22','request-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2555','1570017654','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2556','1570017658','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2557','1570017659','1','adminko','97','22','Duplicate of request-send','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2558','1570017659','1','adminko','78','59','Duplicate of request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2559','1570017796','1','adminko','79','59','call_me-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2560','1570017796','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2561','1570017803','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2562','1570018001','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2563','1570018001','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2564','1570018001','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2565','1570018441','1','adminko','76','-','Блог','Element management');

INSERT INTO `modx_manager_log` VALUES ('2566','1570018450','1','adminko','19','-','Новый шаблон','Creating a new template');

INSERT INTO `modx_manager_log` VALUES ('2567','1570018467','1','adminko','19','-','Новый шаблон','Creating a new template');

INSERT INTO `modx_manager_log` VALUES ('2568','1570018714','1','adminko','20','-','Success send','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2569','1570018714','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2570','1570018717','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2571','1570018719','1','adminko','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2572','1570018850','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2573','1570018856','1','adminko','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2574','1570018890','1','adminko','20','20','Success send','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2575','1570018890','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2576','1570018892','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2577','1570019002','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2578','1570019011','1','adminko','5','-','Успех','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2579','1570019012','1','adminko','3','74','Успех','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2580','1570019094','1','adminko','27','74','Успех','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2581','1570019102','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2582','1570019105','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2583','1570019117','1','adminko','20','20','Success send','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2584','1570019117','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2585','1570019122','1','adminko','27','74','Успех','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2586','1570019126','1','adminko','5','74','Успех','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2587','1570019127','1','adminko','3','74','Успех','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2588','1570019128','1','adminko','27','74','Успех','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2589','1570019133','1','adminko','5','74','Успех','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2590','1570019134','1','adminko','3','74','Успех','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2591','1570019141','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2592','1570019144','1','adminko','16','3','Catalog_inside_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2593','1570019164','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2594','1570019170','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2595','1570019228','1','adminko','79','22','request-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2596','1570019228','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2597','1570019234','1','adminko','78','59','call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2598','1570019250','1','adminko','79','59','call_me-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2599','1570019250','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2600','1570019313','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2601','1570019343','1','adminko','20','13','Contacts','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2602','1570019343','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2603','1570019403','1','adminko','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('2604','1570019405','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2605','1570019446','1','adminko','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('2606','1570019537','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('2607','1570019549','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2608','1570019552','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2609','1570019586','1','adminko','20','2','Home page','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2610','1570019586','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2611','1570019587','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2612','1570019597','1','adminko','16','6','Cart','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2613','1570019607','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2614','1570019612','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2615','1570019616','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2616','1570019627','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2617','1570019675','1','adminko','20','8','ThanksForOrder','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2618','1570019675','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2619','1570019686','1','adminko','27','20','Thanks for your order','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2620','1570019715','1','adminko','5','20','Спасибо за заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2621','1570019716','1','adminko','3','20','Спасибо за заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2622','1570019717','1','adminko','27','20','Спасибо за заказ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2623','1570019763','1','adminko','27','74','Успех','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2624','1570020082','1','adminko','5','20','Спасибо за заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2625','1570020083','1','adminko','3','20','Спасибо за заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2626','1570020090','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2627','1570020092','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2628','1570020334','1','adminko','27','20','Спасибо за заказ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2629','1570020344','1','adminko','5','20','Спасибо за заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2630','1570020346','1','adminko','3','20','Спасибо за заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2631','1570020346','1','adminko','27','20','Спасибо за заказ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2632','1570020624','1','adminko','5','20','Спасибо за заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2633','1570020626','1','adminko','3','20','Спасибо за заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2634','1570020631','1','adminko','20','8','ThanksForOrder','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2635','1570020631','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2636','1570020650','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2637','1570020651','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2638','1570020658','1','adminko','27','20','Спасибо за заказ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2639','1570020671','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2640','1570020673','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2641','1570020683','1','adminko','20','8','ThanksForOrder','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2642','1570020683','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2643','1570020686','1','adminko','27','74','Успех','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2644','1570020692','1','adminko','27','20','Спасибо за заказ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2645','1570020695','1','adminko','5','20','Спасибо за заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2646','1570020696','1','adminko','3','20','Спасибо за заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2647','1570020833','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2648','1570020931','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2649','1570020960','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2650','1570020961','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2651','1570020970','1','adminko','19','-','Новый шаблон','Creating a new template');

INSERT INTO `modx_manager_log` VALUES ('2652','1570021208','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2653','1570021214','1','adminko','20','-','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2654','1570021214','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2655','1570021392','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2656','1570021393','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2657','1570021424','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2658','1570021424','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2659','1570021432','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2660','1570021443','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2661','1570021694','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2662','1570021788','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2663','1570021788','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2664','1570021793','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2665','1570021852','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2666','1570021852','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2667','1570021862','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2668','1570021894','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2669','1570021896','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2670','1570021917','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('2671','1570021929','1','adminko','5','-','Результаты поиска','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2672','1570021930','1','adminko','3','75','Результаты поиска','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2673','1570021934','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2674','1570021937','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2675','1570021942','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2676','1570021949','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2677','1570021949','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2678','1570021956','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2679','1570021956','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2680','1570021957','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2681','1570021985','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2682','1570021988','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2683','1570022003','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2684','1570022003','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2685','1570022093','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2686','1570023523','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2687','1570023523','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2688','1570023529','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2689','1570023534','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2690','1570023534','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2691','1570023544','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2692','1570023555','1','adminko','78','45','tpl_menu_catalog_third','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2693','1570023557','1','adminko','97','45','Duplicate of tpl_menu_catalog_third','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2694','1570023557','1','adminko','78','60','Duplicate of tpl_menu_catalog_third','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2695','1570023582','1','adminko','79','60','tpl_menu_catalog_third_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2696','1570023582','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2697','1570023627','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2698','1570023628','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2699','1570023628','1','adminko','78','44','tpl_menu_catalog_second','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2700','1570023629','1','adminko','78','46','tpl_menu_catalog_second_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2701','1570023629','1','adminko','78','45','tpl_menu_catalog_third','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2702','1570023631','1','adminko','78','60','tpl_menu_catalog_third_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2703','1570023711','1','adminko','79','28','tpl_menu_catalog_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2704','1570023711','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2705','1570023759','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2706','1570023763','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2707','1570023774','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2708','1570023776','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2709','1570023792','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2710','1570023796','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2711','1570023824','1','adminko','79','28','tpl_menu_catalog_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2712','1570023824','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2713','1570023826','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2714','1570023826','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2715','1570024134','1','adminko','106','-','Language','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('2716','1570024135','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2717','1570024139','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2718','1570024141','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2719','1570024217','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2720','1570024217','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2721','1570024224','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2722','1570024234','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2723','1570024234','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2724','1570024244','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2725','1570024257','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2726','1570024257','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2727','1570024264','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2728','1570024265','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2729','1570024265','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2730','1570024485','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2731','1570024487','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2732','1570024494','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2733','1570025645','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2734','1570026446','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2735','1570026446','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2736','1570026865','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2737','1570026871','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2738','1570026987','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2739','1570026987','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2740','1570026997','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2741','1570027005','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2742','1570027005','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2743','1570027086','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2744','1570027458','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2745','1570027458','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2746','1570027483','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2747','1570027484','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2748','1570027484','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2749','1570027643','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2750','1570027682','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2751','1570027837','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2752','1570027841','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2753','1570027867','1','adminko','78','3','tpl_filters','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2754','1570027927','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2755','1570027936','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2756','1570027949','1','adminko','79','-','tpl_categoryPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2757','1570027949','1','adminko','78','61','tpl_categoryPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2758','1570028071','1','adminko','79','61','tpl_categoryPrice','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2759','1570028071','1','adminko','78','61','tpl_categoryPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2760','1570028078','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2761','1570028085','1','adminko','78','3','tpl_filters','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2762','1570028092','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2763','1570028131','1','adminko','78','61','tpl_categoryPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2764','1570028171','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2765','1570028175','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2766','1570028181','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2767','1570028188','1','adminko','78','61','tpl_categoryPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2768','1570028192','1','adminko','80','61','tpl_categoryPrice','Deleting Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2769','1570028192','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2770','1570029824','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2771','1570029827','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2772','1570029861','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2773','1570031897','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2774','1570031897','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2775','1570032038','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2776','1570032041','1','adminko','27','75','Результаты поиска','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2777','1570032267','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2778','1570032267','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2779','1570032280','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2780','1570032280','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2781','1570032356','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2782','1570032356','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2783','1570032389','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2784','1570032389','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2785','1570033826','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2786','1570033826','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2787','1570033994','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2788','1570033994','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2789','1570034236','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('2790','1570034276','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('2791','1570085158','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('2792','1570086800','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2793','1570087298','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2794','1570088268','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2795','1570088268','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2796','1570088268','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2797','1570088384','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2798','1570088390','1','adminko','78','59','call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2799','1570088438','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2800','1570088456','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2801','1570088456','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2802','1570089037','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2803','1570089131','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2804','1570089145','1','adminko','20','5','Product-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2805','1570089145','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2806','1570089188','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2807','1570089190','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2808','1570089380','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2809','1570089783','1','adminko','20','5','Product-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2810','1570089783','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2811','1570089786','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2812','1570089789','1','adminko','20','5','Product-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2813','1570089790','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2814','1570089844','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2815','1570089846','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2816','1570089915','1','adminko','20','5','Product-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2817','1570089915','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2818','1570089918','1','adminko','27','4','Product','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2819','1570090053','1','adminko','5','4','Товар','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2820','1570090055','1','adminko','3','4','Товар','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2821','1570090075','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2822','1570090077','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2823','1570091230','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2824','1570091368','1','adminko','79','-','tpl_productImages','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2825','1570091368','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2826','1570091459','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2827','1570091534','1','adminko','79','-','full-gallery','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2828','1570091534','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2829','1570091536','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2830','1570091608','1','adminko','20','5','Product-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2831','1570091608','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2832','1570091609','1','adminko','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2833','1570091610','1','adminko','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2834','1570091644','1','adminko','5','4','Товар вариативный','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2835','1570091645','1','adminko','27','4','Товар вариативный','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2836','1570091674','1','adminko','5','4','Товар вариативный','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2837','1570091674','1','adminko','27','4','Товар вариативный','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2838','1570091679','1','adminko','94','4','Duplicate of Товар вариативный','Duplicate resource');

INSERT INTO `modx_manager_log` VALUES ('2839','1570091679','1','adminko','3','76','Duplicate of Товар вариативный','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2840','1570091681','1','adminko','27','76','Duplicate of Товар вариативный','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2841','1570091711','1','adminko','5','76','Товар груповой','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2842','1570091712','1','adminko','3','76','Товар груповой','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2843','1570091713','1','adminko','27','76','Товар груповой','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2844','1570091716','1','adminko','5','76','Товар груповой','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2845','1570091718','1','adminko','3','76','Товар груповой','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2846','1570092055','1','adminko','27','4','Товар вариативный','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2847','1570092064','1','adminko','5','4','Товар','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2848','1570092065','1','adminko','27','76','Товар груповой','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2849','1570092067','1','adminko','27','76','Товар груповой','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2850','1570092071','1','adminko','6','76','Товар груповой','Deleting resource');

INSERT INTO `modx_manager_log` VALUES ('2851','1570092073','1','adminko','3','76','Товар груповой','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2852','1570092076','1','adminko','64','-','-','Removing deleted content');

INSERT INTO `modx_manager_log` VALUES ('2853','1570092148','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2854','1570092154','1','adminko','16','5','Product-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2855','1570092159','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2856','1570092159','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2857','1570092165','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2858','1570092176','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2859','1570092176','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2860','1570092243','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2861','1570092243','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2862','1570092246','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2863','1570092255','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2864','1570092257','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2865','1570092320','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2866','1570092320','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2867','1570092375','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2868','1570092408','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2869','1570092424','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2870','1570092425','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2871','1570092425','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2872','1570092462','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2873','1570092469','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2874','1570092789','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2875','1570092792','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2876','1570092894','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2877','1570092927','1','adminko','79','-','tpl_product_characteristic','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2878','1570092927','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2879','1570093196','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2880','1570093196','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2881','1570093532','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2882','1570093534','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2883','1570093594','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2884','1570093594','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2885','1570093720','1','adminko','27','1','Главная','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2886','1570093731','1','adminko','76','-','Pictures','Element management');

INSERT INTO `modx_manager_log` VALUES ('2887','1570093735','1','adminko','301','28','URL фильтра \"Производитель\"','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2888','1570093741','1','adminko','302','28','URL фильтра \\\"Производитель\\\"','Save Template Variable');

INSERT INTO `modx_manager_log` VALUES ('2889','1570093741','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2890','1570093743','1','adminko','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('2891','1570093746','1','adminko','5','4','Товар','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('2892','1570093748','1','adminko','3','4','Товар','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('2893','1570093756','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2894','1570093759','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2895','1570093769','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2896','1570093769','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2897','1570093955','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2898','1570094860','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2899','1570094860','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2900','1570094863','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2901','1570094876','1','adminko','79','-','tpl_product_option_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2902','1570094876','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2903','1570094880','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2904','1570094901','1','adminko','79','-','tpl_product_option_outer','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2905','1570094901','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2906','1570095162','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2907','1570095274','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2908','1570095586','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2909','1570095939','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2910','1570096296','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2911','1570096357','1','adminko','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2912','1570096708','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2913','1570096866','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2914','1570096870','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2915','1570096897','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2916','1570096897','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2917','1570097110','1','adminko','79','65','tpl_product_option_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2918','1570097110','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2919','1570097116','1','adminko','78','66','tpl_product_option_outer','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2920','1570097240','1','adminko','79','66','tpl_product_option_outer','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2921','1570097240','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2922','1570097567','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2923','1570097756','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2924','1570097756','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2925','1570097758','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2926','1570097797','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2927','1570097803','1','adminko','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2928','1570097806','1','adminko','78','66','tpl_product_option_outer','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2929','1570097826','1','adminko','79','66','tpl_product_option_outer','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2930','1570097826','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2931','1570100930','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2932','1570100935','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2933','1570101058','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2934','1570101060','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2935','1570101475','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2936','1570101476','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2937','1570101964','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2938','1570101967','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2939','1570102283','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('2940','1570102283','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2941','1570103725','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2942','1570103728','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2943','1570103740','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2944','1570103747','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2945','1570103751','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2946','1570103812','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2947','1570103818','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2948','1570104416','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2949','1570104416','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2950','1570104421','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2951','1570104426','1','adminko','79','19','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2952','1570104426','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2953','1570104766','1','adminko','78','19','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2954','1570105755','1','adminko','76','-','Language','Element management');

INSERT INTO `modx_manager_log` VALUES ('2955','1570105766','1','adminko','78','19','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2956','1570105902','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2957','1570105902','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2958','1570105906','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2959','1570105913','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2960','1570105916','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2961','1570105941','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2962','1570105941','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2963','1570105948','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2964','1570106086','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2965','1570106086','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2966','1570106089','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2967','1570106109','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2968','1570106454','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2969','1570106454','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2970','1570106525','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2971','1570106525','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2972','1570106537','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2973','1570106539','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2974','1570106841','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2975','1570106844','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2976','1570106851','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2977','1570106922','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2978','1570106922','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2979','1570106928','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2980','1570106935','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2981','1570106935','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2982','1570107352','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2983','1570107359','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2984','1570107374','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2985','1570107386','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2986','1570107413','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2987','1570107413','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2988','1570107452','1','adminko','78','42','tpl_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2989','1570107510','1','adminko','79','-','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2990','1570107510','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2991','1570107522','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2992','1570108075','1','adminko','79','-','tpl_cart_characteristics','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('2993','1570108075','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2994','1570108101','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2995','1570108106','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2996','1570108106','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('2997','1570108107','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('2998','1570108408','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('2999','1570108429','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3000','1570108429','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3001','1570108579','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3002','1570108738','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3003','1570108739','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3004','1570108891','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3005','1570108906','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3006','1570108907','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3007','1570108923','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3008','1570108923','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3009','1570108969','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3010','1570108969','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3011','1570108975','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3012','1570108977','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3013','1570108979','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3014','1570109242','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3015','1570109242','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3016','1570109245','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3017','1570109247','1','adminko','78','68','tpl_cart_characteristics','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3018','1570109279','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3019','1570109284','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3020','1570109296','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3021','1570109442','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3022','1570109442','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3023','1570109602','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3024','1570109602','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3025','1570109875','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3026','1570109875','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3027','1570110205','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3028','1570110253','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3029','1570110260','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3030','1570110285','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3031','1570110288','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3032','1570110389','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3033','1570110389','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3034','1570110965','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3035','1570110972','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3036','1570111350','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3037','1570111350','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3038','1570111355','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3039','1570111697','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3040','1570111743','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3041','1570111743','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3042','1570111776','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3043','1570111779','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3044','1570111789','1','adminko','78','22','request-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3045','1570111830','1','adminko','79','22','request-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3046','1570111831','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3047','1570111832','1','adminko','78','63','full-gallery','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3048','1570111837','1','adminko','78','59','call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3049','1570111843','1','adminko','79','59','call_me-send','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3050','1570111843','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3051','1570112021','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3052','1570112021','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3053','1570112101','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3054','1570112112','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3055','1570114137','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3056','1570114297','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3057','1570114297','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3058','1570114605','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3059','1570114628','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3060','1570114628','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3061','1570114690','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3062','1570114779','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3063','1570114779','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3064','1570114953','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3065','1570114953','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3066','1570115006','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3067','1570115006','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3068','1570115325','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3069','1570115325','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3070','1570115935','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3071','1570115936','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3072','1570174956','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3073','1570176235','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3074','1570176240','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3075','1570176270','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3076','1570177382','1','adminko','76','-','Language','Element management');

INSERT INTO `modx_manager_log` VALUES ('3077','1570177405','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3078','1570177510','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3079','1570177510','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3080','1570177542','1','adminko','27','19','Checkout','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3081','1570177603','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3082','1570177608','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3083','1570177639','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3084','1570177639','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3085','1570177641','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3086','1570177643','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3087','1570177691','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3088','1570177691','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3089','1570177694','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3090','1570177885','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3091','1570177886','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3092','1570178454','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3093','1570178454','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3094','1570178456','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3095','1570178457','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3096','1570178503','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3097','1570178505','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3098','1570178628','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3099','1570178628','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3100','1570178650','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3101','1570178718','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3102','1570178718','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3103','1570178722','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3104','1570178780','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3105','1570178813','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3106','1570178813','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3107','1570178885','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3108','1570178885','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3109','1570179317','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3110','1570179317','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3111','1570179750','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3112','1570179750','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3113','1570179750','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3114','1570180041','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3115','1570180041','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3116','1570180416','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3117','1570180435','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3118','1570180435','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3119','1570183008','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3120','1570183013','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3121','1570183035','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3122','1570183035','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3123','1570183059','1','adminko','78','63','full-gallery','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3124','1570183061','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3125','1570183073','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3126','1570183073','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3127','1570183561','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3128','1570183564','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3129','1570183792','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3130','1570183792','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3131','1570183803','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3132','1570183803','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3133','1570183805','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3134','1570183853','1','adminko','79','-','pl_ico_cart_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3135','1570183853','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3136','1570183873','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3137','1570183873','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3138','1570183885','1','adminko','78','69','pl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3139','1570183889','1','adminko','79','69','tpl_ico_cart_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3140','1570183889','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3141','1570183891','1','adminko','78','69','tpl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3142','1570183895','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3143','1570183901','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3144','1570183904','1','adminko','78','69','tpl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3145','1570183913','1','adminko','79','69','tpl_ico_cart_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3146','1570183913','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3147','1570183919','1','adminko','78','69','tpl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3148','1570183921','1','adminko','97','69','Duplicate of tpl_ico_cart_active','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3149','1570183921','1','adminko','78','70','Duplicate of tpl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3150','1570184001','1','adminko','79','70','tpl_ico_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3151','1570184001','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3152','1570184006','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3153','1570184006','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3154','1570184052','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3155','1570184053','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3156','1570184467','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3157','1570184467','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3158','1570184535','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3159','1570184535','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3160','1570184649','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3161','1570184649','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3162','1570184652','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3163','1570184656','1','adminko','78','70','tpl_ico_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3164','1570184659','1','adminko','80','70','tpl_ico_cart','Deleting Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3165','1570184659','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3166','1570184661','1','adminko','78','69','tpl_ico_cart_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3167','1570184663','1','adminko','80','69','tpl_ico_cart_active','Deleting Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3168','1570184663','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3169','1570184701','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3170','1570184840','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3171','1570184841','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3172','1570184996','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3173','1570184997','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3174','1570185008','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3175','1570185008','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3176','1570185027','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3177','1570185027','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3178','1570185061','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3179','1570185061','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3180','1570185102','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3181','1570185102','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3182','1570185368','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3183','1570185368','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3184','1570185369','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3185','1570186493','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3186','1570186493','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3187','1570186497','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3188','1570186705','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3189','1570186709','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3190','1570188546','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3191','1570188547','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3192','1570188548','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3193','1570188913','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3194','1570188913','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3195','1570189184','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3196','1570189489','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3197','1570189489','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3198','1570189496','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3199','1570189509','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3200','1570189823','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3201','1570189823','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3202','1570192384','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3203','1570192388','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3204','1570192402','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3205','1570192402','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3206','1570192695','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3207','1570192695','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3208','1570192848','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3209','1570192848','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3210','1570193041','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3211','1570193041','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3212','1570193063','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3213','1570193063','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3214','1570193121','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3215','1570193121','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3216','1570193336','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3217','1570193336','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3218','1570193366','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3219','1570193366','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3220','1570194154','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3221','1570194154','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3222','1570194155','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3223','1570194155','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3224','1570194570','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3225','1570194572','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3226','1570194601','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3227','1570194601','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3228','1570194604','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3229','1570194604','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3230','1570194604','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3231','1570194639','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3232','1570194645','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3233','1570194645','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3234','1570194751','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3235','1570194795','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3236','1570194795','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3237','1570194909','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3238','1570194912','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3239','1570194922','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3240','1570194923','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3241','1570194978','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3242','1570194986','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3243','1570195015','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3244','1570195058','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3245','1570195058','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3246','1570195065','1','adminko','27','19','Checkout','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3247','1570195096','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3248','1570195096','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3249','1570195137','1','adminko','5','19','Оформить заказ','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('3250','1570195139','1','adminko','3','19','Оформить заказ','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('3251','1570195476','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3252','1570195488','1','adminko','19','-','Новый шаблон','Creating a new template');

INSERT INTO `modx_manager_log` VALUES ('3253','1570195521','1','adminko','20','-','Untitled template','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3254','1570195521','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3255','1570195537','1','adminko','27','45','Акция','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3256','1570195542','1','adminko','27','45','Акция','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3257','1570195543','1','adminko','5','45','Акция','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('3258','1570195545','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3259','1570195545','1','adminko','3','45','Акция','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('3260','1570195546','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3261','1570195547','1','adminko','16','22','Untitled template','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3262','1570195582','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3263','1570195582','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3264','1570195609','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3265','1570195665','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3266','1570195677','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3267','1570195679','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3268','1570195744','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3269','1570195745','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3270','1570195818','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3271','1570195833','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3272','1570195833','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3273','1570196029','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3274','1570196184','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3275','1570196364','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3276','1570196364','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3277','1570196502','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3278','1570196542','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('3279','1570196549','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3280','1570196563','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3281','1570196635','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3282','1570196635','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3283','1570196667','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3284','1570196680','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3285','1570196680','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3286','1570197128','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3287','1570197186','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3288','1570197189','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3289','1570197191','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3290','1570197192','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3291','1570197194','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3292','1570197201','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3293','1570197239','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3294','1570197239','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3295','1570197256','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3296','1570197256','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3297','1570197442','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3298','1570197444','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3299','1570197447','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3300','1570197451','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3301','1570197459','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3302','1570197459','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3303','1570197597','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3304','1570197597','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3305','1570197611','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3306','1570197611','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3307','1570197620','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3308','1570197620','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3309','1570197632','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3310','1570197632','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3311','1570197674','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3312','1570197674','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3313','1570197682','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3314','1570197682','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3315','1570197715','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3316','1570197715','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3317','1570197857','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3318','1570197857','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3319','1570197866','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3320','1570197866','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3321','1570197890','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3322','1570197890','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3323','1570197913','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3324','1570197913','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3325','1570197933','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3326','1570197933','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3327','1570197955','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3328','1570197955','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3329','1570198028','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3330','1570198028','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3331','1570198044','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3332','1570198044','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3333','1570198052','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3334','1570198052','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3335','1570198107','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3336','1570198107','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3337','1570198179','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3338','1570198179','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3339','1570198190','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3340','1570198190','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3341','1570198201','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3342','1570198201','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3343','1570198270','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3344','1570198270','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3345','1570198305','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3346','1570198305','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3347','1570198349','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3348','1570198349','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3349','1570198376','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3350','1570198376','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3351','1570198399','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3352','1570198399','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3353','1570198417','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3354','1570198417','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3355','1570198435','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3356','1570198435','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3357','1570198545','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3358','1570198545','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3359','1570198557','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3360','1570198557','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3361','1570198570','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3362','1570198570','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3363','1570198578','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3364','1570198614','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3365','1570198614','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3366','1570198654','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3367','1570198654','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3368','1570198673','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3369','1570198673','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3370','1570198704','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3371','1570198704','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3372','1570198720','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3373','1570198720','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3374','1570199303','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3375','1570199303','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3376','1570199356','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3377','1570199356','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3378','1570199431','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3379','1570199431','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3380','1570199536','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3381','1570199613','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3382','1570199628','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3383','1570199629','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3384','1570199631','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3385','1570199634','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/snippet.shop.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3386','1570199841','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3387','1570199844','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3388','1570199847','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3389','1570199848','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3390','1570199850','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3391','1570199853','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3392','1570199857','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3393','1570199954','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3394','1570199954','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3395','1570200102','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('3396','1570200105','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3397','1570201267','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3398','1570201269','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3399','1570437471','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3400','1570437475','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3401','1570437476','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3402','1570437483','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3403','1570437489','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3404','1570437729','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3405','1570437729','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3406','1570437737','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3407','1570437737','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3408','1570437757','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3409','1570437761','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3410','1570437761','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3411','1570437819','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3412','1570437879','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3413','1570438209','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3414','1570438254','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3415','1570438571','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3416','1570438572','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3417','1570438576','1','adminko','78','9','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3418','1570438584','1','adminko','79','9','HEAD','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3419','1570438584','1','adminko','78','9','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3420','1570439426','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3421','1570439429','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3422','1570439549','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3423','1570439549','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3424','1570440793','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3425','1570440801','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3426','1570440928','1','adminko','79','-','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3427','1570440929','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3428','1570440944','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3429','1570440948','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3430','1570440952','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3431','1570441026','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3432','1570441026','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3433','1570441031','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3434','1570441371','1','adminko','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3435','1570441387','1','adminko','78','64','tpl_product_characteristic','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3436','1570441407','1','adminko','79','-','tpl_getOptionGroup','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3437','1570441407','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3438','1570441413','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3439','1570441418','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3440','1570441418','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3441','1570441471','1','adminko','79','64','tpl_product_characteristic','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3442','1570441471','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3443','1570441481','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3444','1570441803','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3445','1570441803','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3446','1570441858','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3447','1570441858','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3448','1570441957','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3449','1570441957','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3450','1570442109','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3451','1570442109','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3452','1570442367','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3453','1570442367','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3454','1570442421','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3455','1570442421','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3456','1570442422','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3457','1570442429','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3458','1570442430','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3459','1570442697','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3460','1570442697','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3461','1570442742','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3462','1570442743','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3463','1570442790','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3464','1570442790','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3465','1570442824','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3466','1570442824','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3467','1570443022','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3468','1570443022','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3469','1570443038','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3470','1570443065','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3471','1570443551','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3472','1570443551','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3473','1570443728','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3474','1570443770','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3475','1570443779','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3476','1570443797','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3477','1570443816','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3478','1570443892','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3479','1570443913','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3480','1570443915','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3481','1570444142','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3482','1570444142','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3483','1570444645','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3484','1570444645','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3485','1570444657','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3486','1570444657','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3487','1570444680','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3488','1570444680','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3489','1570444746','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3490','1570444746','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3491','1570444784','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3492','1570444784','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3493','1570444867','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3494','1570444867','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3495','1570444908','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3496','1570444908','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3497','1570445268','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3498','1570445268','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3499','1570446373','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3500','1570446420','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3501','1570446652','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3502','1570446652','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3503','1570446998','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3504','1570447010','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3505','1570448831','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3506','1570448842','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3507','1570449288','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3508','1570449294','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3509','1570449795','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3510','1570449795','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3511','1570449824','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3512','1570449824','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3513','1570449867','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3514','1570449869','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3515','1570449944','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3516','1570449944','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3517','1570449978','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3518','1570449980','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3519','1570450000','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3520','1570450002','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3521','1570450021','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3522','1570450022','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3523','1570450044','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3524','1570450044','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3525','1570450063','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3526','1570450081','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3527','1570450081','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3528','1570450099','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3529','1570450099','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3530','1570450286','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3531','1570450286','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3532','1570450299','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3533','1570450302','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3534','1570450509','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3535','1570450550','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3536','1570450552','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3537','1570450566','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3538','1570450566','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3539','1570450870','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3540','1570450881','1','adminko','78','51','tpl_seo_block','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3541','1570451165','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3542','1570451213','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3543','1570451218','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3544','1570451243','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3545','1570451243','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3546','1570451289','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3547','1570451304','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3548','1570451306','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3549','1570451308','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3550','1570451771','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3551','1570451779','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3552','1570451782','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3553','1570451783','1','adminko','16','9','Empty','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3554','1570451783','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3555','1570451783','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3556','1570451784','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3557','1570451815','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3558','1570451815','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3559','1570451824','1','adminko','16','6','Cart','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3560','1570451824','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3561','1570451824','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3562','1570451842','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3563','1570451849','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3564','1570451849','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3565','1570451856','1','adminko','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3566','1570451857','1','adminko','16','19','Catalog_gates','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3567','1570451857','1','adminko','16','3','Catalog_inside_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3568','1570451857','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3569','1570451871','1','adminko','20','18','Catalog_entrance_doors','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3570','1570451871','1','adminko','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3571','1570451884','1','adminko','20','19','Catalog_gates','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3572','1570451884','1','adminko','16','19','Catalog_gates','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3573','1570451894','1','adminko','20','3','Catalog_inside_doors','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3574','1570451894','1','adminko','16','3','Catalog_inside_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3575','1570451906','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3576','1570451906','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3577','1570451915','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3578','1570451918','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3579','1570451919','1','adminko','16','16','Blog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3580','1570451919','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3581','1570451920','1','adminko','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3582','1570451929','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3583','1570451929','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3584','1570451939','1','adminko','20','16','Blog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3585','1570451939','1','adminko','16','16','Blog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3586','1570451951','1','adminko','20','13','Contacts','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3587','1570451951','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3588','1570451964','1','adminko','20','17','Discounts','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3589','1570451964','1','adminko','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3590','1570451969','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3591','1570451971','1','adminko','16','14','pay-and-delivery','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3592','1570451971','1','adminko','16','10','Service','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3593','1570451972','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3594','1570451972','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3595','1570451988','1','adminko','20','14','pay-and-delivery','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3596','1570451988','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3597','1570451998','1','adminko','20','10','Service','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3598','1570451999','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3599','1570452012','1','adminko','20','15','Single news','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3600','1570452012','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3601','1570452032','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3602','1570452032','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3603','1570452196','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3604','1570452215','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3605','1570452215','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3606','1570452565','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3607','1570452573','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3608','1570452573','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3609','1570452581','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3610','1570452581','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3611','1570452582','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3612','1570452908','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3613','1570452913','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3614','1570452916','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3615','1570454504','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3616','1570454667','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3617','1570454668','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3618','1570454880','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('3619','1570454884','1','adminko','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3620','1570454930','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3621','1570455275','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3622','1570455284','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3623','1570455965','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('3624','1570455967','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3625','1570456442','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3626','1570456447','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3627','1570456463','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3628','1570456463','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3629','1570456470','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3630','1570456471','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3631','1570456481','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3632','1570456493','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3633','1570456514','1','adminko','78','44','tpl_menu_catalog_second','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3634','1570456525','1','adminko','78','45','tpl_menu_catalog_third','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3635','1570456529','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3636','1570456542','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3637','1570456542','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3638','1570457126','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3639','1570457175','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3640','1570457175','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3641','1570457203','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3642','1570457303','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3643','1570457304','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3644','1570457310','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3645','1570457622','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3646','1570457657','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3647','1570457657','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3648','1570457660','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3649','1570457689','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3650','1570457689','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3651','1570458070','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3652','1570458513','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3653','1570458513','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3654','1570458516','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3655','1570458517','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3656','1570458517','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3657','1570458517','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3658','1570459316','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3659','1570459316','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3660','1570460022','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3661','1570460022','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3662','1570460118','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3663','1570460118','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3664','1570460137','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3665','1570460139','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3666','1570460145','1','adminko','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3667','1570460490','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3668','1570460708','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3669','1570460708','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3670','1570460708','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3671','1570461046','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3672','1570461054','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3673','1570461064','1','adminko','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3674','1570461072','1','adminko','79','65','tpl_product_option_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3675','1570461073','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3676','1570461387','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3677','1570461402','1','adminko','16','8','ThanksForOrder','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3678','1570461403','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3679','1570461419','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3680','1570461419','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3681','1570461421','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3682','1570461424','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3683','1570461424','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3684','1570461742','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3685','1570462333','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3686','1570462337','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3687','1570472054','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3688','1570472054','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3689','1570472057','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3690','1570472069','1','adminko','27','30','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3691','1570472069','1','adminko','27','30','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3692','1570472076','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3693','1570472079','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3694','1570472134','1','adminko','20','13','Contacts','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3695','1570472134','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3696','1570472136','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3697','1570472159','1','adminko','20','13','Contacts','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3698','1570472159','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3699','1570521548','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3700','1570521659','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3701','1570521824','1','adminko','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('3702','1570521827','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3703','1570521828','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3704','1570521832','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3705','1570521832','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3706','1570521857','1','adminko','16','9','Empty','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3707','1570521864','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3708','1570522306','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3709','1570522326','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3710','1570522378','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3711','1570522378','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3712','1570522435','1','adminko','79','19','order','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3713','1570522435','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3714','1570522760','1','adminko','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('3715','1570522875','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3716','1570522875','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3717','1570522947','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3718','1570522947','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3719','1570523667','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3720','1570523671','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3721','1570523723','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3722','1570523723','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3723','1570525578','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3724','1570525578','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3725','1570526897','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3726','1570526942','1','adminko','8','-','-','Logged out');

INSERT INTO `modx_manager_log` VALUES ('3727','1570527647','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3728','1570527650','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3729','1570527651','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3730','1570527653','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3731','1570527654','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3732','1570527658','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3733','1570527690','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3734','1570527690','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/plugin.index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3735','1570527793','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3736','1570527795','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3737','1570527797','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3738','1570527800','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3739','1570527809','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3740','1570527810','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3741','1570527813','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('3742','1570527866','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('3743','1570527866','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3744','1570527868','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3745','1570528032','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3746','1570531939','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3747','1570532270','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3748','1570532272','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3749','1570532462','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3750','1570532462','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3751','1570532502','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3752','1570532504','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3753','1570532522','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3754','1570532522','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3755','1570533655','1','adminko','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('3756','1570533663','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3757','1570533664','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3758','1570534173','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3759','1570534184','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3760','1570534272','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3761','1570534272','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3762','1570534315','1','adminko','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('3763','1570534316','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('3764','1570534319','1','adminko','115','152','-','View event log details');

INSERT INTO `modx_manager_log` VALUES ('3765','1570534324','1','adminko','116','-','-','Delete event log');

INSERT INTO `modx_manager_log` VALUES ('3766','1570534324','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('3767','1570534429','1','adminko','70','-','-','Viewing site schedule');

INSERT INTO `modx_manager_log` VALUES ('3768','1570534429','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3769','1570534431','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3770','1570534443','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3771','1570534443','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3772','1570534505','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3773','1570534559','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3774','1570534559','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3775','1570535160','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3776','1570535160','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3777','1570536016','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3778','1570536016','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3779','1570536019','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3780','1570536109','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3781','1570536115','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3782','1570536169','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3783','1570536170','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3784','1570537080','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3785','1570537094','1','adminko','78','14','tpl_deliveryAddress_1','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3786','1570537109','1','adminko','78','14','tpl_deliveryAddress_1','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3787','1570537123','1','adminko','78','13','mail_order_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3788','1570537488','1','adminko','79','13','mail_order_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3789','1570537488','1','adminko','78','13','mail_order_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3790','1570537492','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3791','1570537496','1','adminko','78','19','order','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3792','1570538178','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3793','1570538179','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3794','1570538210','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3795','1570538214','1','adminko','78','68','tpl_cart_characteristics','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3796','1570538241','1','adminko','79','-','mail_order_item_characteristic','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3797','1570538241','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3798','1570538289','1','adminko','79','13','mail_order_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3799','1570538289','1','adminko','78','13','mail_order_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3800','1570538804','1','adminko','78','73','mail_order_item_characteristic','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3801','1570538808','1','adminko','78','13','mail_order_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3802','1570538811','1','adminko','79','13','mail_order_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3803','1570538812','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3804','1570540810','1','adminko','79','13','mail_order_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3805','1570540810','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3806','1570540814','1','adminko','13','-','-','Viewing logging');

INSERT INTO `modx_manager_log` VALUES ('3807','1570540815','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('3808','1570540817','1','adminko','116','-','-','Delete event log');

INSERT INTO `modx_manager_log` VALUES ('3809','1570540817','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('3810','1570542262','1','adminko','76','-','Language','Element management');

INSERT INTO `modx_manager_log` VALUES ('3811','1570542265','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3812','1570542268','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3813','1570542346','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3814','1570542346','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3815','1570542370','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3816','1570542383','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3817','1570542383','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3818','1570542441','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3819','1570542441','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3820','1570542800','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3821','1570542800','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3822','1570543019','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3823','1570543019','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3824','1570543891','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3825','1570543891','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3826','1570544970','1','adminko','76','-','Language','Element management');

INSERT INTO `modx_manager_log` VALUES ('3827','1570544973','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3828','1570545010','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3829','1570545012','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3830','1570545719','1','adminko','78','73','mail_order_item_characteristic','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3831','1570545721','1','adminko','78','13','mail_order_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3832','1570545731','1','adminko','79','13','mail_order_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3833','1570545731','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3834','1570546228','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3835','1570546258','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3836','1570546266','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3837','1570546266','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('3838','1570546351','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3839','1570546353','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3840','1570546412','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('3841','1570550996','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3842','1570551492','1','adminko','19','-','Новый шаблон','Creating a new template');

INSERT INTO `modx_manager_log` VALUES ('3843','1570551668','1','adminko','20','-','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3844','1570551668','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3845','1570551674','1','adminko','77','-','Новый чанк','Creating a new Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3846','1570551679','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3847','1570551682','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3848','1570551715','1','adminko','79','-','tpl_sitemap_item','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3849','1570551715','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3850','1570551718','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3851','1570551718','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3852','1570551724','1','adminko','27','31','Карта сайта','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3853','1570551729','1','adminko','27','31','Карта сайта','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3854','1570551731','1','adminko','5','31','Карта сайта','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('3855','1570551733','1','adminko','3','31','Карта сайта','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('3856','1570551743','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3857','1570551751','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3858','1570551751','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3859','1570551753','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3860','1570551769','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3861','1570551769','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3862','1570551832','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3863','1570551852','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3864','1570551852','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3865','1570551867','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3866','1570551955','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3867','1570551955','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3868','1570552636','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3869','1570552641','1','adminko','16','23','Site map','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3870','1570552659','1','adminko','20','23','Site map','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3871','1570552659','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3872','1570552802','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3873','1570552804','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('3874','1570606441','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3875','1570608793','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3876','1570608812','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3877','1570608815','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3878','1570609067','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3879','1570609073','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3880','1570609087','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3881','1570609101','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3882','1570609102','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3883','1570609283','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3884','1570609283','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3885','1570609291','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3886','1570609295','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3887','1570609297','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3888','1570609524','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3889','1570609524','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3890','1570609731','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3891','1570611315','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3892','1570611320','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3893','1570611343','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3894','1570611359','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3895','1570611389','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3896','1570611396','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3897','1570611398','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3898','1570611502','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3899','1570611511','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3900','1570611512','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3901','1570611544','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3902','1570611561','1','adminko','27','75','Результаты поиска','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3903','1570611568','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3904','1570611570','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3905','1570611651','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3906','1570611653','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3907','1570611681','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3908','1570611684','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3909','1570611686','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('3910','1570611689','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/snippet.shop.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3911','1570611857','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/snippet.shop.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('3912','1570611857','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/snippet.shop.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('3913','1570612081','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3914','1570612081','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3915','1570613661','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3916','1570613664','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3917','1570613685','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3918','1570613685','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3919','1570613800','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3920','1570613800','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3921','1570613841','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3922','1570613841','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3923','1570613844','1','adminko','27','45','Акция','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3924','1570613859','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3925','1570613861','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3926','1570614032','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3927','1570614036','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3928','1570614138','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3929','1570614138','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3930','1570614183','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3931','1570614183','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3932','1570614295','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3933','1570614299','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3934','1570614345','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3935','1570614345','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3936','1570614430','1','adminko','16','21','Search','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3937','1570614474','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3938','1570614477','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3939','1570614577','1','adminko','79','4','tpl_filtersInner','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3940','1570614577','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3941','1570614747','1','adminko','20','21','Search','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3942','1570614747','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3943','1570615554','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3944','1570617633','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3945','1570617858','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3946','1570620185','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3947','1570620194','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3948','1570620208','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3949','1570620208','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3950','1570620399','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3951','1570620416','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3952','1570620416','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3953','1570620587','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3954','1570620592','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3955','1570620596','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3956','1570620600','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3957','1570620628','1','adminko','79','21','call-me','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3958','1570620628','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3959','1570620678','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3960','1570620693','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3961','1570620693','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3962','1570620714','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3963','1570620718','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3964','1570620718','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3965','1570620721','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3966','1570620725','1','adminko','79','21','call-me','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3967','1570620725','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3968','1570620951','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('3969','1570621152','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3970','1570621156','1','adminko','27','2','404','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3971','1570621164','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3972','1570621167','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3973','1570621251','1','adminko','20','1','404','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3974','1570621251','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3975','1570621252','1','adminko','27','2','404','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3976','1570621275','1','adminko','5','2','404','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('3977','1570621277','1','adminko','3','2','404','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('3978','1570621278','1','adminko','27','2','404','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('3979','1570621284','1','adminko','5','2','404','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('3980','1570621286','1','adminko','3','2','404','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('3981','1570621295','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3982','1570621299','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3983','1570621352','1','adminko','20','1','404','Saving template');

INSERT INTO `modx_manager_log` VALUES ('3984','1570621352','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3985','1570621419','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3986','1570621429','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3987','1570621433','1','adminko','16','13','Contacts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3988','1570621452','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3989','1570621459','1','adminko','78','58','tpl_aria_option','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3990','1570621464','1','adminko','79','58','tpl_aria_option','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3991','1570621464','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3992','1570621471','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3993','1570621689','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3994','1570621689','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('3995','1570623652','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('3996','1570623655','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3997','1570623656','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('3998','1570623970','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('3999','1570623972','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4000','1570624033','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4001','1570624035','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4002','1570624546','1','adminko','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4003','1570624632','1','adminko','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4004','1570624641','1','adminko','5','3','Каталог','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4005','1570624642','1','adminko','3','3','Каталог','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4006','1570625969','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('4007','1570625970','1','adminko','116','-','-','Delete event log');

INSERT INTO `modx_manager_log` VALUES ('4008','1570625970','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('4009','1570625972','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('4010','1570625975','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4011','1570625978','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4012','1570627179','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4013','1570627189','1','adminko','27','21','Входные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4014','1570627192','1','adminko','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4015','1570627201','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4016','1570627208','1','adminko','27','21','Входные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4017','1570627209','1','adminko','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4018','1570627213','1','adminko','27','22','Межкомнатные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4019','1570627219','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4020','1570627220','1','adminko','27','56','В гостинную','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4021','1570627224','1','adminko','27','22','Межкомнатные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4022','1570627226','1','adminko','27','54','Материал','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4023','1570627226','1','adminko','27','54','Материал','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4024','1570627228','1','adminko','27','66','Пленка ПВХ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4025','1570627814','1','adminko','26','-','Pictures','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4026','1570627814','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4027','1570627815','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4028','1570627815','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4029','1570627816','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4030','1570632611','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4031','1570632611','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4032','1570632612','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4033','1570633639','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4034','1570691849','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4035','1570692034','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4036','1570692055','1','adminko','30','-','-','Saving settings');

INSERT INTO `modx_manager_log` VALUES ('4037','1570695214','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4038','1570699366','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4039','1570702701','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4040','1570702704','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4041','1570702830','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4042','1570702830','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4043','1570702836','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4044','1570702841','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4045','1570703272','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4046','1570703285','1','adminko','79','72','tpl_getOptionGroup','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4047','1570703285','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4048','1570704713','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4049','1570704780','1','adminko','79','72','tpl_getOptionGroup','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4050','1570704780','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4051','1570708904','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4052','1570708913','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4053','1570709259','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4054','1570709259','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4055','1570709336','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4056','1570709528','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4057','1570715211','1','adminko','6','61','В спальню','Deleting resource');

INSERT INTO `modx_manager_log` VALUES ('4058','1570715213','1','adminko','3','53','Назначение','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4059','1570715218','1','adminko','63','61','В спальню','Un-deleting a resource');

INSERT INTO `modx_manager_log` VALUES ('4060','1570715219','1','adminko','3','53','Назначение','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4061','1570715224','1','adminko','6','60','В прихожую','Deleting resource');

INSERT INTO `modx_manager_log` VALUES ('4062','1570715225','1','adminko','3','53','Назначение','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4063','1570715231','1','adminko','27','60','В прихожую','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4064','1570715231','1','adminko','27','60','В прихожую','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4065','1570715235','1','adminko','27','56','В гостинную','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4066','1570715246','1','adminko','27','60','В прихожую','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4067','1570715251','1','adminko','63','60','В прихожую','Un-deleting a resource');

INSERT INTO `modx_manager_log` VALUES ('4068','1570715252','1','adminko','3','53','Назначение','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4069','1570715253','1','adminko','27','60','В прихожую','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4070','1570715258','1','adminko','27','56','В гостинную','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4071','1570715276','1','adminko','5','56','У гостинну','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4072','1570715278','1','adminko','3','56','У гостинну','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4073','1570719918','1','adminko','27','54','Материал','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4074','1570723817','1','adminko','27','47','Распашные','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4075','1570723831','1','adminko','27','47','Распашные','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4076','1570775955','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4077','1570786063','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4078','1570786065','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4079','1570786066','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4080','1570786071','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4081','1570786079','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4082','1570786410','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4083','1570786410','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4084','1570786415','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4085','1570786416','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4086','1570786418','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4087','1570786439','1','adminko','20','1','404','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4088','1570786439','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4089','1570786443','1','adminko','27','2','404','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4090','1570788236','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4091','1570788236','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4092','1570788240','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4093','1570788241','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4094','1570788243','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4095','1570788351','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4096','1570788367','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4097','1570788384','1','adminko','96','15','Duplicate of Single news','Duplicate Template');

INSERT INTO `modx_manager_log` VALUES ('4098','1570788384','1','adminko','16','24','Duplicate of Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4099','1570788404','1','adminko','20','24','Content Page','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4100','1570788404','1','adminko','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4101','1570788406','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4102','1570788682','1','adminko','78','51','tpl_seo_block','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4103','1570789058','1','adminko','78','51','tpl_seo_block','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4104','1570791688','1','adminko','79','51','tpl_seo_block','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4105','1570791688','1','adminko','78','51','tpl_seo_block','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4106','1570798358','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4107','1570802594','1','adminko','27','21','Входные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4108','1570802649','1','adminko','27','23','Фурнитура двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4109','1570802653','1','adminko','27','23','Фурнитура двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4110','1570802656','1','adminko','5','23','Фурнітура двері','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4111','1570802657','1','adminko','3','23','Фурнітура двері','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4112','1570802676','1','adminko','27','23','Фурнітура двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4113','1570802688','1','adminko','5','23','Фурнітура','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4114','1570802689','1','adminko','3','23','Фурнітура','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4115','1570802691','1','adminko','27','23','Фурнітура','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4116','1570803280','1','adminko','27','24','Автоворота','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4117','1570803298','1','adminko','5','24','Автоворота','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4118','1570803300','1','adminko','3','24','Автоворота','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4119','1570803550','1','adminko','27','24','Автоворота','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4120','1571046469','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4121','1571047851','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4122','1571047853','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4123','1571047856','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4124','1571047859','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/inspiration/index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('4125','1571047867','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/inspiration/index.php','Modified File');

INSERT INTO `modx_manager_log` VALUES ('4126','1571047867','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/inspiration/index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('4127','1571124280','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4128','1571124296','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4129','1571124297','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4130','1571124306','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4131','1571124330','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4132','1571125239','1','adminko','114','-','Блог','View event log');

INSERT INTO `modx_manager_log` VALUES ('4133','1571125241','1','adminko','115','261','-','View event log details');

INSERT INTO `modx_manager_log` VALUES ('4134','1571125634','1','adminko','76','-','Блог','Element management');

INSERT INTO `modx_manager_log` VALUES ('4135','1571125648','1','adminko','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4136','1571125792','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4137','1571125934','1','adminko','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4138','1571126165','1','adminko','20','24','Content Page','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4139','1571126165','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4140','1571126168','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('4141','1571126677','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4142','1571126684','1','adminko','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4143','1571126740','1','adminko','5','-','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4144','1571126741','1','adminko','3','77','Контентна сторинка','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4145','1571126746','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4146','1571126754','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4147','1571126792','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4148','1571126794','1','adminko','3','77','Контентна сторинка','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4149','1571126826','1','adminko','20','24','Content Page','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4150','1571126826','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4151','1571127037','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4152','1571127038','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4153','1571127186','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4154','1571127326','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4155','1571127328','1','adminko','3','77','Контентна сторинка','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4156','1571128960','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4157','1571129158','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4158','1571129189','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4159','1571129194','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4160','1571129199','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4161','1571129248','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4162','1571129248','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4163','1571129266','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4164','1571129274','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4165','1571129275','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4166','1571129277','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4167','1571129279','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4168','1571129293','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4169','1571129356','1','adminko','27','1','Главная','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4170','1571129384','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4171','1571129384','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4172','1571129393','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4173','1571129394','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4174','1571129426','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4175','1571129441','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4176','1571129449','1','adminko','78','37','tpl_about_faq','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4177','1571129483','1','adminko','79','37','tpl_about_faq','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4178','1571129483','1','adminko','78','37','tpl_about_faq','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4179','1571129800','1','adminko','17','-','Pictures','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4180','1571129810','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4181','1571129811','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4182','1571129811','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4183','1571129813','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4184','1571129838','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4185','1571129838','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4186','1571129853','1','adminko','30','-','-','Saving settings');

INSERT INTO `modx_manager_log` VALUES ('4187','1571129889','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4188','1571129892','1','adminko','27','41','Оплата и доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4189','1571129925','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4190','1571129932','1','adminko','78','67','tpl_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4191','1571129939','1','adminko','79','67','tpl_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4192','1571129939','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4193','1571129962','1','adminko','27','42','Акции','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4194','1571129963','1','adminko','27','43','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4195','1571129965','1','adminko','27','41','Оплата и доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4196','1571130032','1','adminko','5','41','Оплата і доставка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4197','1571130032','1','adminko','27','41','Оплата і доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4198','1571130068','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4199','1571130068','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4200','1571130100','1','adminko','5','41','Оплата і доставка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4201','1571130100','1','adminko','27','41','Оплата і доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4202','1571130106','1','adminko','5','41','Оплата і доставка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4203','1571130106','1','adminko','27','41','Оплата і доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4204','1571130154','1','adminko','5','41','Оплата і доставка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4205','1571130154','1','adminko','27','41','Оплата і доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4206','1571130177','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4207','1571130267','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4208','1571130267','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4209','1571130346','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4210','1571130346','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4211','1571130361','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4212','1571130361','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4213','1571130371','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4214','1571130371','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4215','1571130411','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4216','1571130411','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4217','1571130430','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4218','1571130430','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4219','1571130441','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4220','1571130441','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4221','1571130460','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4222','1571130460','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4223','1571130483','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4224','1571130483','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4225','1571130665','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4226','1571130665','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4227','1571130701','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4228','1571130701','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4229','1571130713','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4230','1571130713','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4231','1571131127','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4232','1571131127','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4233','1571131208','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4234','1571131208','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4235','1571131485','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4236','1571131485','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4237','1571131606','1','adminko','20','11','About','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4238','1571131606','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4239','1571131607','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4240','1571131610','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4241','1571131610','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4242','1571131610','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4243','1571131658','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4244','1571131659','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4245','1571131797','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4246','1571131797','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4247','1571131925','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4248','1571131926','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4249','1571131955','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4250','1571131955','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4251','1571131968','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4252','1571131968','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4253','1571131976','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4254','1571131983','1','adminko','78','21','call-me','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4255','1571131991','1','adminko','78','59','call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4256','1571132034','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4257','1571132039','1','adminko','78','59','call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4258','1571132043','1','adminko','97','59','Duplicate of call_me-send','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4259','1571132043','1','adminko','78','75','Duplicate of call_me-send','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4260','1571132106','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4261','1571132109','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4262','1571132126','1','adminko','79','75','empty_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4263','1571132126','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4264','1571132159','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4265','1571132172','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4266','1571132172','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4267','1571132199','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4268','1571132237','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4269','1571132238','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4270','1571132238','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4271','1571132521','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4272','1571132811','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4273','1571132818','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4274','1571135448','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4275','1571137168','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4276','1571137171','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4277','1571138059','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4278','1571138060','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4279','1571138062','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4280','1571138703','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4281','1571138709','1','adminko','78','34','tpl_questions','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4282','1571138716','1','adminko','79','34','tpl_questions','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4283','1571138716','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4284','1571139086','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4285','1571139088','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4286','1571139095','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4287','1571139117','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4288','1571139135','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4289','1571139137','1','adminko','27','44','Статья','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4290','1571139148','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4291','1571139178','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4292','1571139187','1','adminko','27','44','Статья','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4293','1571139248','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4294','1571139251','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4295','1571139290','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4296','1571139291','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4297','1571139294','1','adminko','27','34','Шаги для успешной покупки двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4298','1571139296','1','adminko','27','35','Замер','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4299','1571139298','1','adminko','27','36','Доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4300','1571139312','1','adminko','27','44','Статья','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4301','1571139313','1','adminko','27','29','Блог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4302','1571139317','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4303','1571139458','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4304','1571139458','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4305','1571139481','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4306','1571139481','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4307','1571139516','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4308','1571139516','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4309','1571139518','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4310','1571139520','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4311','1571139520','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4312','1571139520','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4313','1571139521','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4314','1571139527','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4315','1571139532','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4316','1571139698','1','adminko','5','77','Контентна сторинка','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4317','1571139699','1','adminko','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4318','1571139738','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4319','1571139748','1','adminko','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4320','1571139760','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4321','1571139763','1','adminko','16','16','Blog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4322','1571139774','1','adminko','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4323','1571139782','1','adminko','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4324','1571139825','1','adminko','27','44','Статья','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4325','1571139831','1','adminko','27','44','Статья','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4326','1571139956','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4327','1571140420','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4328','1571141323','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4329','1571141324','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4330','1571141908','1','adminko','26','-','Блог','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4331','1571141909','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4332','1571141912','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4333','1571142276','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4334','1571142278','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4335','1571142419','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4336','1571142419','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4337','1571143104','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4338','1571143108','1','adminko','27','1','Главная','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4339','1571143445','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4340','1571143445','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4341','1571143447','1','adminko','26','-','Pictures','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4342','1571143448','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4343','1571143596','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4344','1571143605','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4345','1571143606','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4346','1571143708','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4347','1571143719','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4348','1571143720','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4349','1571144467','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4350','1571144471','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4351','1571144484','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4352','1571144544','1','adminko','17','-','Pictures','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4353','1571144593','1','adminko','30','-','-','Saving settings');

INSERT INTO `modx_manager_log` VALUES ('4354','1571144598','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4355','1571144612','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4356','1571144629','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4357','1571144631','1','adminko','3','1','Головна','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4358','1571144638','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4359','1571144649','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4360','1571144749','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4361','1571144751','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4362','1571144756','1','adminko','78','32','tpl_img_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4363','1571145002','1','adminko','79','32','tpl_img_main_slider','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4364','1571145002','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4365','1571145006','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4366','1571145006','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4367','1571145006','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4368','1571145134','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4369','1571145136','1','adminko','78','32','tpl_img_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4370','1571145147','1','adminko','79','32','tpl_img_main_slider','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4371','1571145147','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4372','1571145153','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4373','1571146861','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4374','1571146862','1','adminko','3','1','Головна','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4375','1571146865','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4376','1571147998','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4377','1571148666','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4378','1571148667','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4379','1571151961','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4380','1571212158','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4381','1571218614','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4382','1571220643','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4383','1571220648','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4384','1571220648','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4385','1571225911','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4386','1571225914','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4387','1571225916','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4388','1571225920','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4389','1571225922','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4390','1571225924','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4391','1571225930','1','adminko','31','-','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/shop/interface/imports/index.php','Viewing File');

INSERT INTO `modx_manager_log` VALUES ('4392','1571225949','1','adminko','26','-','SEO','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4393','1571225950','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4394','1571227369','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4395','1571227372','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4396','1571227706','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('4397','1571227742','1','adminko','5','-','Для будки','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4398','1571227742','1','adminko','27','78','Для будки','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4399','1571230520','1','adminko','27','78','Для будки','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4400','1571230526','1','adminko','6','78','Для будки','Deleting resource');

INSERT INTO `modx_manager_log` VALUES ('4401','1571230528','1','adminko','3','21','Входные двери','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4402','1571230529','1','adminko','27','78','Для будки','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4403','1571230532','1','adminko','64','-','Pictures','Removing deleted content');

INSERT INTO `modx_manager_log` VALUES ('4404','1571235790','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4405','1571235795','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4406','1571235860','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4407','1571235860','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4408','1571235881','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4409','1571235902','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4410','1571235903','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4411','1571237829','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4412','1571237829','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4413','1571294736','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4414','1571294746','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4415','1571294756','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4416','1571294771','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4417','1571294780','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4418','1571294781','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4419','1571294789','1','adminko','30','-','-','Saving settings');

INSERT INTO `modx_manager_log` VALUES ('4420','1571295930','1','adminko','8','-','-','Logged out');

INSERT INTO `modx_manager_log` VALUES ('4421','1571295951','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4422','1571296140','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4423','1571298737','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4424','1571298770','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4425','1571298771','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4426','1571298796','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4427','1571298802','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4428','1571298804','1','adminko','3','1','Головна','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4429','1571298805','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4430','1571313227','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4431','1571313242','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4432','1571313250','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4433','1571313265','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4434','1571313267','1','adminko','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('4435','1571313289','1','adminko','75','-','Акции','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4436','1571313290','1','adminko','99','-','-','Manage Web Users');

INSERT INTO `modx_manager_log` VALUES ('4437','1571313291','1','adminko','86','-','-','Role management');

INSERT INTO `modx_manager_log` VALUES ('4438','1571313292','1','adminko','40','-','-','Editing Access Permissions');

INSERT INTO `modx_manager_log` VALUES ('4439','1571313293','1','adminko','91','-','-','Editing Web Access Permissions');

INSERT INTO `modx_manager_log` VALUES ('4440','1571313295','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4441','1571313357','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4442','1571313361','1','adminko','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('4443','1571313398','1','adminko','106','-','Language','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('4444','1571313402','1','adminko','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('4445','1571313411','1','adminko','54','-','modx_a_mail_templates','Optimizing a table');

INSERT INTO `modx_manager_log` VALUES ('4446','1571313411','1','adminko','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('4447','1571313414','1','adminko','3','4','Товар','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4448','1571313423','1','adminko','31','-','Advanced settings','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4449','1571313426','1','adminko','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('4450','1571313433','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4451','1571313444','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4452','1571313449','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4453','1571314215','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4454','1571314219','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4455','1571314808','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4456','1571314809','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4457','1571314918','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4458','1571314930','1','adminko','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('4459','1571314931','1','adminko','13','-','-','Viewing logging');

INSERT INTO `modx_manager_log` VALUES ('4460','1571314932','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4461','1571314934','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4462','1571314952','1','adminko','30','-','-','Saving settings');

INSERT INTO `modx_manager_log` VALUES ('4463','1571314954','1','adminko','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('4464','1571315196','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4465','1571315201','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4466','1571316030','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4467','1571316031','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4468','1571316033','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4469','1571316050','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4470','1571316050','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4471','1571316077','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4472','1571316083','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4473','1571316083','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4474','1571316098','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4475','1571316109','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4476','1571316109','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4477','1571316167','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4478','1571316276','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4479','1571316277','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4480','1571317761','1','adminko','27','58','В детскую','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4481','1571324805','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4482','1571324807','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4483','1571325255','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4484','1571325510','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4485','1571325649','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4486','1571325651','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4487','1571325840','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4488','1571325840','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4489','1571325863','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4490','1571325865','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4491','1571325894','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4492','1571325895','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4493','1571325900','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4494','1571325987','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4495','1571325987','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4496','1571325999','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4497','1571326064','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4498','1571326064','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4499','1571326070','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4500','1571326101','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4501','1571326101','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4502','1571326114','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4503','1571326114','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4504','1571326133','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4505','1571326133','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4506','1571326339','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4507','1571326342','1','adminko','16','1','404','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4508','1571326355','1','adminko','20','1','404','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4509','1571326355','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4510','1571379655','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4511','1571388173','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4512','1571389300','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4513','1571389346','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4514','1571393559','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4515','1571393807','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4516','1571393839','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4517','1571393988','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4518','1571394031','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4519','1571394093','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4520','1571394110','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4521','1571400278','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4522','1571400294','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4523','1571400332','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4524','1571400340','1','adminko','78','75','empty_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4525','1571400494','1','adminko','97','75','Duplicate of empty_cart','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4526','1571400494','1','adminko','78','76','Duplicate of empty_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4527','1571400560','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4528','1571400560','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4529','1571400616','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4530','1571400635','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4531','1571400635','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4532','1571400643','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4533','1571400653','1','adminko','79','1','FOOTER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4534','1571400653','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4535','1571400657','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4536','1571401025','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4537','1571401030','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4538','1571401720','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4539','1571401727','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4540','1571402498','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4541','1571402498','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4542','1571402505','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4543','1571405664','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4544','1571405666','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4545','1571405687','1','adminko','78','72','tpl_getOptionGroup','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4546','1571405690','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4547','1571405721','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4548','1571405721','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4549','1571406157','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4550','1571406157','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4551','1571407858','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4552','1571407862','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4553','1571407876','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4554','1571407888','1','adminko','79','62','tpl_productImages','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4555','1571407888','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4556','1571407904','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4557','1571407905','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4558','1571407905','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4559','1571407905','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4560','1571407906','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4561','1571407971','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4562','1571408000','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4563','1571415856','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4564','1571415912','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4565','1571416353','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4566','1571416356','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4567','1571416925','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4568','1571416927','1','adminko','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('4569','1571647936','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4570','1571647959','1','adminko','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4571','1571654972','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4572','1571655771','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4573','1571681449','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4574','1571681450','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4575','1571681632','2','kolomak9','76','-','Advanced settings','Element management');

INSERT INTO `modx_manager_log` VALUES ('4576','1571681634','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4577','1571733525','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4578','1571736146','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4579','1571739428','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4580','1571742148','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4581','1571745035','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4582','1571839325','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4583','1571839326','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4584','1571839362','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4585','1571839915','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4586','1571840210','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4587','1571997632','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4588','1572247645','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4589','1572247651','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4590','1572247653','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4591','1572247654','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4592','1572247654','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4593','1572247703','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4594','1572247718','1','adminko','78','42','tpl_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4595','1572247744','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4596','1572247749','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4597','1572247762','1','adminko','20','2','Home page','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4598','1572247762','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4599','1572247783','1','adminko','79','42','tpl_main_slider','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4600','1572247783','1','adminko','78','42','tpl_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4601','1572251445','1','adminko','79','42','tpl_main_slider','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4602','1572251445','1','adminko','78','42','tpl_main_slider','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4603','1572253295','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4604','1572253306','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4605','1572253323','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4606','1572253326','1','adminko','78','53','tpl_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4607','1572253355','1','adminko','79','53','tpl_catalog','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4608','1572253355','1','adminko','78','53','tpl_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4609','1572253570','1','adminko','79','53','tpl_catalog','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4610','1572253570','1','adminko','78','53','tpl_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4611','1572254115','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4612','1572261173','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4613','1572261175','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4614','1572265039','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4615','1572265055','1','adminko','78','31','tpl_menu_footer_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4616','1572265056','1','adminko','78','24','tpl_menu_top_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4617','1572265153','1','adminko','79','31','tpl_menu_footer_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4618','1572265153','1','adminko','78','31','tpl_menu_footer_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4619','1572265303','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4620','1572265321','1','adminko','78','24','tpl_menu_top_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4621','1572265327','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4622','1572265355','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4623','1572265358','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4624','1572265360','1','adminko','78','44','tpl_menu_catalog_second','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4625','1572265361','1','adminko','78','46','tpl_menu_catalog_second_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4626','1572265362','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4627','1572265373','1','adminko','79','28','tpl_menu_catalog_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4628','1572265373','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4629','1572265396','1','adminko','79','46','tpl_menu_catalog_second_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4630','1572265396','1','adminko','78','46','tpl_menu_catalog_second_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4631','1572265638','1','adminko','78','3','tpl_filters','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4632','1572265644','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4633','1572265654','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4634','1572265669','1','adminko','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4635','1572265690','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4636','1572265694','1','adminko','76','-','Pictures','Element management');

INSERT INTO `modx_manager_log` VALUES ('4637','1572265696','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4638','1572265867','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4639','1572265871','1','adminko','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4640','1572265932','1','adminko','78','57','tpl_getOption_single','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4641','1572265944','1','adminko','78','56','tpl_getOption','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4642','1572265949','1','adminko','78','57','tpl_getOption_single','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4643','1572265963','1','adminko','97','57','Duplicate of tpl_getOption_single','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4644','1572265963','1','adminko','78','77','Duplicate of tpl_getOption_single','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4645','1572265977','1','adminko','79','77','tpl_getOption_single_active','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4646','1572265977','1','adminko','78','77','tpl_getOption_single_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4647','1572265998','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4648','1572265998','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4649','1572266026','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4650','1572266026','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4651','1572266027','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4652','1572266033','1','adminko','78','77','tpl_getOption_single_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4653','1572266040','1','adminko','80','77','tpl_getOption_single_active','Deleting Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4654','1572266040','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4655','1572269680','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4656','1572269682','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4657','1572270572','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4658','1572270579','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4659','1572270583','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4660','1572270586','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4661','1572270649','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4662','1572270658','1','adminko','78','27','tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4663','1572270659','1','adminko','78','28','tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4664','1572270742','1','adminko','97','28','Duplicate of tpl_menu_catalog_active','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4665','1572270742','1','adminko','78','78','Duplicate of tpl_menu_catalog_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4666','1572270817','1','adminko','79','78','tpl_menu_catalog_active_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4667','1572270817','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4668','1572270822','1','adminko','97','27','Duplicate of tpl_menu_catalog','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4669','1572270822','1','adminko','78','79','Duplicate of tpl_menu_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4670','1572270834','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4671','1572270834','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4672','1572270844','1','adminko','79','18','HEADER','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4673','1572270844','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4674','1572271025','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4675','1572271026','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4676','1572271062','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4677','1572271063','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4678','1572271357','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4679','1572271357','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4680','1572271400','1','adminko','79','78','tpl_menu_catalog_active_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4681','1572271400','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4682','1572271540','1','adminko','79','78','tpl_menu_catalog_active_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4683','1572271540','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4684','1572273625','1','adminko','79','78','tpl_menu_catalog_active_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4685','1572273626','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4686','1572273627','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4687','1572273633','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4688','1572273646','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4689','1572273646','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4690','1572274672','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4691','1572274676','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4692','1572274677','1','adminko','78','79','tpl_menu_catalog_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4693','1572274684','1','adminko','79','78','tpl_menu_catalog_active_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4694','1572274684','1','adminko','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4695','1572274687','1','adminko','79','79','tpl_menu_catalog_mob','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4696','1572274687','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4697','1572293251','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4698','1572293252','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4699','1572293254','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4700','1572293650','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4701','1572299474','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4702','1572338902','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4703','1572338912','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4704','1572339029','2','kolomak9','27','21','Входные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4705','1572339416','2','kolomak9','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('4706','1572342382','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4707','1572343615','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4708','1572343618','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4709','1572343618','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4710','1572343623','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4711','1572343670','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4712','1572343671','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4713','1572343706','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4714','1572343734','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4715','1572343734','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4716','1572348552','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4717','1572348588','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4718','1572348623','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4719','1572348645','1','adminko','27','34','Шаги для успешной покупки двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4720','1572348647','2','kolomak9','9','-','-','Viewing help');

INSERT INTO `modx_manager_log` VALUES ('4721','1572348655','1','adminko','27','35','Замер','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4722','1572348667','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4723','1572348756','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4724','1572348756','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4725','1572348937','2','kolomak9','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4726','1572348958','2','kolomak9','33','1','adminko','Deleting user');

INSERT INTO `modx_manager_log` VALUES ('4727','1572348958','2','kolomak9','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4728','1572348987','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4729','1572348994','2','kolomak9','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4730','1572349206','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4731','1572349206','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4732','1572349224','1','adminko','5','1','Головна','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4733','1572349224','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4734','1572349491','1','adminko','27','32','Преимущества покупки в нашем магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4735','1572349794','1','adminko','27','35','Замер','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4736','1572353049','1','adminko','27','22','Межкомнатные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4737','1572353129','1','adminko','27','21','Входные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4738','1572353140','1','adminko','5','21','Вхідні двері','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4739','1572353140','1','adminko','27','21','Вхідні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4740','1572353186','1','adminko','27','22','Межкомнатные двери','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4741','1572353238','1','adminko','5','22','Міжкімнатні двері','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4742','1572353238','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4743','1572353261','1','adminko','5','22','Міжкімнатні двері','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4744','1572353262','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4745','1572353784','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4746','1572353790','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4747','1572353819','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4748','1572353824','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4749','1572353864','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4750','1572353866','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('4751','1572353867','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4752','1572353868','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4753','1572353887','1','adminko','16','2','Home page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4754','1572353904','1','adminko','78','43','tpl_main_manufacturer','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4755','1572364351','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4756','1572364354','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4757','1572364356','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4758','1572364364','1','adminko','12','1','adminko','Editing user');

INSERT INTO `modx_manager_log` VALUES ('4759','1572364370','1','adminko','32','1','adminko','Saving user');

INSERT INTO `modx_manager_log` VALUES ('4760','1572364370','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4761','1572364416','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4762','1572364418','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4763','1572364435','1','adminko','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('4764','1572364441','1','adminko','86','-','-','Role management');

INSERT INTO `modx_manager_log` VALUES ('4765','1572364443','1','adminko','99','-','-','Manage Web Users');

INSERT INTO `modx_manager_log` VALUES ('4766','1572364446','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4767','1572365057','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4768','1572365069','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4769','1572365071','1','adminko','12','2','kolomak9','Editing user');

INSERT INTO `modx_manager_log` VALUES ('4770','1572365077','1','adminko','32','2','kolomak9','Saving user');

INSERT INTO `modx_manager_log` VALUES ('4771','1572365077','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4772','1572366791','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4773','1572366793','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4774','1572366794','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4775','1572366804','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4776','1572433001','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4777','1572433237','1','adminko','27','21','Вхідні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4778','1572433260','1','adminko','5','21','Вхідні двері','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4779','1572433260','1','adminko','27','21','Вхідні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4780','1572433560','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4781','1572433622','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4782','1572433636','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4783','1572433707','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4784','1572433709','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4785','1572433712','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4786','1572433721','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4787','1572433723','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4788','1572433725','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4789','1572433725','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4790','1572433736','1','adminko','27','24','Автоворота','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4791','1572433740','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4792','1572433743','1','adminko','27','21','Вхідні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4793','1572433752','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4794','1572433906','1','adminko','27','22','Міжкімнатні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4795','1572433910','1','adminko','27','51','Для дома ','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4796','1572433921','1','adminko','5','51','Для будинку','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4797','1572433921','1','adminko','27','51','Для будинку','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4798','1572433935','1','adminko','5','51','Для будинку','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4799','1572433935','1','adminko','27','51','Для будинку','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4800','1572435307','1','adminko','27','39','Доставка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4801','1572456082','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4802','1572456088','1','adminko','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4803','1572456095','1','adminko','12','2','kolomak9','Editing user');

INSERT INTO `modx_manager_log` VALUES ('4804','1572456118','1','adminko','32','2','kolomak9','Saving user');

INSERT INTO `modx_manager_log` VALUES ('4805','1572456730','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4806','1572457086','2','kolomak9','99','-','-','Manage Web Users');

INSERT INTO `modx_manager_log` VALUES ('4807','1572457088','2','kolomak9','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('4808','1572508851','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4809','1572517887','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4810','1572517889','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4811','1572517890','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4812','1572517899','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4813','1572517941','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4814','1572517949','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4815','1572518559','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4816','1572519100','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4817','1572519101','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4818','1572519103','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4819','1572519435','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4820','1572519450','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4821','1572521726','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4822','1572521741','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4823','1572521749','1','adminko','27','51','Для будинку','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4824','1572521753','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4825','1572521852','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4826','1572522886','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4827','1572523016','1','adminko','94','73','Duplicate of Распашные','Duplicate resource');

INSERT INTO `modx_manager_log` VALUES ('4828','1572523016','1','adminko','3','80','Duplicate of Распашные','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4829','1572523019','1','adminko','27','80','Duplicate of Распашные','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4830','1572523035','1','adminko','5','80','Новый тип открытия','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4831','1572523036','1','adminko','3','80','Новый тип открытия','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4832','1572523198','1','adminko','27','53','Назначение','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4833','1572523203','1','adminko','27','51','Для будинку','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4834','1572523211','1','adminko','27','51','Для будинку','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4835','1572523216','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('4836','1572523229','1','adminko','5','-','тестовая категория','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4837','1572523231','1','adminko','3','81','тестовая категория','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4838','1572523234','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4839','1572523237','1','adminko','27','56','У гостинну','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4840','1572523244','1','adminko','27','52','Для квартиры','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4841','1572523455','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4842','1572523455','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4843','1572523507','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4844','1572523511','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4845','1572523518','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4846','1572523522','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4847','1572523526','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4848','1572523532','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4849','1572523541','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4850','1572523562','1','adminko','79','62','tpl_productImages','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4851','1572523563','1','adminko','79','62','tpl_productImages','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4852','1572523563','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4853','1572523565','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4854','1572523565','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4855','1572523570','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4856','1572523570','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4857','1572523792','1','adminko','78','63','full-gallery','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4858','1572523807','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4859','1572524928','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4860','1572524930','1','adminko','78','62','tpl_productImages','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4861','1572525028','1','adminko','79','62','tpl_productImages_420','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4862','1572525029','1','adminko','78','62','tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4863','1572525033','1','adminko','97','62','Duplicate of tpl_productImages_420','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4864','1572525033','1','adminko','78','80','Duplicate of tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4865','1572525111','1','adminko','79','80','tpl_productImages_90','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4866','1572525111','1','adminko','78','80','tpl_productImages_90','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4867','1572525113','1','adminko','97','80','Duplicate of tpl_productImages_90','Duplicate Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4868','1572525113','1','adminko','78','81','Duplicate of tpl_productImages_90','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4869','1572525144','1','adminko','79','81','tpl_productImages_full','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4870','1572525144','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4871','1572525150','1','adminko','78','81','tpl_productImages_full','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4872','1572525158','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4873','1572525239','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4874','1572525239','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4875','1572525244','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4876','1572525244','1','adminko','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4877','1572525265','1','adminko','78','63','full-gallery','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4878','1572525271','1','adminko','79','63','full-gallery','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4879','1572525271','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4880','1572525289','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4881','1572525289','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4882','1572525294','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4883','1572526785','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('4884','1572526795','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4885','1572526869','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4886','1572527287','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4887','1572527336','1','adminko','78','62','tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4888','1572527353','1','adminko','79','62','tpl_productImages_420','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4889','1572527353','1','adminko','78','62','tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4890','1572527367','1','adminko','79','62','tpl_productImages_420','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4891','1572527367','1','adminko','78','62','tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4892','1572527406','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4893','1572527407','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4894','1572527410','1','adminko','78','80','tpl_productImages_90','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4895','1572527418','1','adminko','79','80','tpl_productImages_90','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4896','1572527418','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4897','1572527436','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4898','1572527457','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4899','1572527459','1','adminko','78','62','tpl_productImages_420','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4900','1572527706','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4901','1572527706','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4902','1572527726','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4903','1572527726','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4904','1572527906','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4905','1572527906','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4906','1572527918','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4907','1572527918','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4908','1572527918','1','adminko','27','81','тестовая категория','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4909','1572527923','1','adminko','6','81','тестовая категория','Deleting resource');

INSERT INTO `modx_manager_log` VALUES ('4910','1572527924','1','adminko','3','52','Для квартиры','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4911','1572527939','1','adminko','64','-','-','Removing deleted content');

INSERT INTO `modx_manager_log` VALUES ('4912','1572527942','1','adminko','27','52','Для квартиры','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4913','1572527946','1','adminko','5','52','Для квартири','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4914','1572527948','1','adminko','3','52','Для квартири','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4915','1572528636','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4916','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4917','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4918','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4919','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4920','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4921','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4922','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4923','1572528653','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4924','1572529297','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4925','1572529301','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4926','1572531301','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4927','1572531301','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4928','1572531569','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('4929','1572531592','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4930','1572531592','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4931','1572531883','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4932','1572532101','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4933','1572532104','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4934','1572532176','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4935','1572532176','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4936','1572532216','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4937','1572532216','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4938','1572532232','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4939','1572532349','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4940','1572532365','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4941','1572532365','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4942','1572532392','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4943','1572532405','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4944','1572532405','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4945','1572533180','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4946','1572534360','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4947','1572534685','1','adminko','27','25','Акции','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4948','1572536252','1','adminko','26','-','Акции','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4949','1572536690','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4950','1572536690','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4951','1572536693','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4952','1572536739','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('4953','1572536739','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4954','1572536938','1','adminko','27','26','Услуги','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4955','1572536941','1','adminko','27','39','Доставка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4956','1572537168','1','adminko','27','26','Услуги','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4957','1572537286','1','adminko','27','39','Доставка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4958','1572537565','1','adminko','27','40','Профессиональная установка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4959','1572537595','1','adminko','5','40','Професійна установка дверей','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4960','1572537595','1','adminko','27','40','Професійна установка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4961','1572537602','1','adminko','5','40','Професійна установка дверей','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4962','1572537602','1','adminko','27','40','Професійна установка дверей','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4963','1572538062','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('4964','1572538064','1','adminko','27','26','Услуги','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4965','1572538199','1','adminko','4','-','Новый ресурс','Creating a resource');

INSERT INTO `modx_manager_log` VALUES ('4966','1572538852','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4967','1572538934','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4968','1572538949','1','adminko','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4969','1572538959','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4970','1572538969','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4971','1572538979','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4972','1572538982','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4973','1572538985','1','adminko','16','11','About','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4974','1572539015','1','adminko','27','5','О магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4975','1572539068','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4976','1572539070','1','adminko','3','5','Про магазин','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4977','1572539093','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4978','1572539100','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4979','1572539102','1','adminko','3','5','Про магазин','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('4980','1572539380','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4981','1572539466','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4982','1572539566','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4983','1572539566','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4984','1572539585','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4985','1572539585','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4986','1572539596','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4987','1572539596','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4988','1572539608','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4989','1572539608','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4990','1572539638','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4991','1572539638','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4992','1572539650','1','adminko','5','5','Про магазин','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('4993','1572539650','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4994','1572539657','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('4995','1572539660','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('4996','1572539940','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('4997','1572539944','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4998','1572539944','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('4999','1572539959','1','adminko','78','57','tpl_getOption_single','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5000','1572539959','1','adminko','78','57','tpl_getOption_single','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5001','1572540037','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5002','1572540041','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5003','1572540070','1','adminko','27','32','Преимущества покупки в нашем магазине','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5004','1572540075','1','adminko','5','32','Переваги покупки в нашому магазині','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('5005','1572540077','1','adminko','3','32','Переваги покупки в нашому магазині','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5006','1572540081','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5007','1572540088','1','adminko','27','32','Переваги покупки в нашому магазині','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5008','1572540092','1','adminko','5','32','Переваги покупки в нашому магазині','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('5009','1572540093','1','adminko','3','32','Переваги покупки в нашому магазині','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5010','1572540129','1','adminko','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5011','1572540481','1','adminko','27','41','Оплата і доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5012','1572540502','1','adminko','27','27','Оплата и доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5013','1572540512','1','adminko','27','43','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5014','1572540522','1','adminko','27','30','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5015','1572540670','1','adminko','27','28','Гарантия и возврат','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5016','1572541505','1','adminko','27','27','Оплата и доставка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5017','1572541901','1','adminko','27','28','Гарантия и возврат','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5018','1572542056','1','adminko','27','30','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5019','1572542339','1','adminko','26','-','Language','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5020','1572542373','1','adminko','26','-','Language','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5021','1572542530','1','adminko','27','30','Контакты','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5022','1572592408','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5023','1572592418','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5024','1572592428','1','adminko','78','58','tpl_aria_option','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5025','1572592428','1','adminko','78','58','tpl_aria_option','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5026','1572592453','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5027','1572592453','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5028','1572592500','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5029','1572592500','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5030','1572593124','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5031','1572593125','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5032','1572593125','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5033','1572593126','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5034','1572593126','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5035','1572593321','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5036','1572593321','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5037','1572593328','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5038','1572593354','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5039','1572593354','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5040','1572593359','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5041','1572593361','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5042','1572593361','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5043','1572593384','1','adminko','79','20','call','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5044','1572593384','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5045','1572593611','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5046','1572594079','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5047','1572594081','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5048','1572594082','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5049','1572594114','1','adminko','78','20','call','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5050','1572594137','1','adminko','78','58','tpl_aria_option','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5051','1572594799','1','adminko','76','-','Акции','Element management');

INSERT INTO `modx_manager_log` VALUES ('5052','1572594814','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5053','1572595050','1','adminko','20','22','Discount-single','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5054','1572595050','1','adminko','16','22','Discount-single','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5055','1572596614','1','adminko','99','-','-','Manage Web Users');

INSERT INTO `modx_manager_log` VALUES ('5056','1572597501','1','adminko','27','23','Фурнітура','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5057','1572597501','1','adminko','27','23','Фурнітура','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5058','1572597509','1','adminko','27','23','Фурнітура','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5059','1572597511','1','adminko','5','23','Фурнітура','Saving resource');

INSERT INTO `modx_manager_log` VALUES ('5060','1572597512','1','adminko','3','23','Фурнітура','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5061','1572597859','1','adminko','27','54','Материал','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5062','1572597896','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5063','1572597920','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5064','1572597923','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5065','1572598604','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5066','1572598604','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5067','1572598818','1','adminko','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5068','1572598818','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5069','1572598821','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5070','1572598870','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5071','1572598870','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5072','1572598903','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5073','1572598903','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5074','1572598927','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5075','1572598927','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5076','1572598990','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5077','1572598990','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5078','1572599011','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5079','1572599011','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5080','1572599032','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5081','1572599032','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5082','1572599053','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5083','1572599063','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5084','1572599119','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5085','1572599119','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5086','1572599121','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5087','1572599121','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5088','1572599198','1','adminko','24','4','Breadcrumbs','Saving Snippet');

INSERT INTO `modx_manager_log` VALUES ('5089','1572599198','1','adminko','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5090','1572599214','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5091','1572599214','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5092','1572599396','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5093','1572599397','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5094','1572602406','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5095','1572602409','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5096','1572602409','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5097','1572602411','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5098','1572602479','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5099','1572602479','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5100','1572602518','1','adminko','20','4','SubCatalog','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5101','1572602518','1','adminko','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5102','1572612856','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5103','1572612862','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5104','1572612862','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5105','1572612873','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5106','1572612890','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5107','1572612913','1','adminko','20','20','Success send','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5108','1572612913','1','adminko','16','20','Success send','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5109','1572612915','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5110','1572613683','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5111','1572613689','1','adminko','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5112','1572613752','1','adminko','20','7','Checkout','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5113','1572613752','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5114','1572615068','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5115','1572615072','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5116','1572615072','1','adminko','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5117','1572615146','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5118','1572615146','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5119','1572615207','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5120','1572615207','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5121','1572615209','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5122','1572615244','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5123','1572615244','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5124','1572615328','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5125','1572615328','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5126','1572615354','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5127','1572615354','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5128','1572615389','1','adminko','79','76','added_to_cart','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5129','1572615389','1','adminko','78','76','added_to_cart','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5130','1572615413','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5131','1572684561','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5132','1572684606','2','kolomak9','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('5133','1572684626','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5134','1572684648','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5135','1572684797','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5136','1572684808','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5137','1572684817','2','kolomak9','3','4','Товар','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5138','1572684842','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5139','1572684859','2','kolomak9','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('5140','1572684864','2','kolomak9','108','4','Advanced settings','Edit module');

INSERT INTO `modx_manager_log` VALUES ('5141','1572684879','2','kolomak9','75','-','-','User/ role management');

INSERT INTO `modx_manager_log` VALUES ('5142','1572685216','2','kolomak9','31','-','Advanced settings','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5143','1572685234','2','kolomak9','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('5144','1572686121','2','kolomak9','17','-','-','Editing settings');

INSERT INTO `modx_manager_log` VALUES ('5145','1572686130','2','kolomak9','93','-','-','Backup Manager');

INSERT INTO `modx_manager_log` VALUES ('5146','1572698528','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5147','1572699703','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5148','1572699723','2','kolomak9','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5149','1572699754','2','kolomak9','114','-','Pictures','View event log');

INSERT INTO `modx_manager_log` VALUES ('5150','1572699768','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5151','1572699788','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5152','1572699794','2','kolomak9','115','508','-','View event log details');

INSERT INTO `modx_manager_log` VALUES ('5153','1572699803','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5154','1572699806','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5155','1572699810','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5156','1572699815','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5157','1572699818','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5158','1572699820','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5159','1572699826','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5160','1572699832','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5161','1572699840','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5162','1572699845','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5163','1572699851','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5164','1572699856','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5165','1572699865','2','kolomak9','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5166','1572699876','2','kolomak9','70','-','-','Viewing site schedule');

INSERT INTO `modx_manager_log` VALUES ('5167','1572699878','2','kolomak9','13','-','-','Viewing logging');

INSERT INTO `modx_manager_log` VALUES ('5168','1572699882','2','kolomak9','53','-','-','Viewing system info');

INSERT INTO `modx_manager_log` VALUES ('5169','1572699941','2','kolomak9','3','77','Контентна сторинка','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5170','1572699962','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5171','1572703619','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5172','1572707766','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5173','1572707766','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5174','1572708634','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5175','1572708641','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5176','1572708670','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5177','1572708671','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5178','1572709696','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5179','1572709700','1','adminko','20','5','Product','Saving template');

INSERT INTO `modx_manager_log` VALUES ('5180','1572709700','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5181','1572709822','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5182','1572709825','1','adminko','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5183','1572709838','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5184','1572709886','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5185','1572709886','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5186','1572709930','1','adminko','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5187','1572709991','1','adminko','79','71','tpl_getOptionGroup_wrap','Saving Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5188','1572709992','1','adminko','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5189','1572727365','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5190','1572727651','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5191','1572727669','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5192','1572727672','2','kolomak9','27','1','Головна','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5193','1572727715','2','kolomak9','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5194','1572727717','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5195','1572729570','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5196','1572730719','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5197','1572731968','2','kolomak9','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('5198','1572732001','2','kolomak9','31','-','SEO','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5199','1572732014','2','kolomak9','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5200','1572732041','2','kolomak9','27','75','Результаты поиска','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5201','1572732065','2','kolomak9','27','31','Карта сайта','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5202','1572732089','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5203','1572732157','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5204','1572769772','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5205','1572770499','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5206','1572770530','2','kolomak9','27','5','Про магазин','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5207','1572770546','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5208','1572770559','2','kolomak9','27','52','Для квартири','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5209','1572770648','2','kolomak9','27','77','Контентна сторинка','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5210','1572770671','2','kolomak9','27','35','Замер','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5211','1572774300','2','kolomak9','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5212','1572774319','2','kolomak9','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5213','1572774504','2','kolomak9','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5214','1572775207','2','kolomak9','16','6','Cart','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5215','1572775228','2','kolomak9','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5216','1572775235','2','kolomak9','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5217','1572775281','2','kolomak9','301','25','Background image','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5218','1572775293','2','kolomak9','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5219','1572775440','2','kolomak9','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5220','1572775445','2','kolomak9','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5221','1572775474','2','kolomak9','22','6','Auth','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5222','1572775488','2','kolomak9','102','2','Shop plugin','Edit plugin');

INSERT INTO `modx_manager_log` VALUES ('5223','1572775565','2','kolomak9','16','3','Catalog_inside_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5224','1572775594','2','kolomak9','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5225','1572775635','2','kolomak9','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5226','1572775646','2','kolomak9','78','66','tpl_product_option_outer','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5227','1572775654','2','kolomak9','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5228','1572775672','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5229','1572775704','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5230','1572775734','2','kolomak9','106','-','-','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('5231','1572775743','2','kolomak9','108','4','Advanced settings','Edit module');

INSERT INTO `modx_manager_log` VALUES ('5232','1572776007','2','kolomak9','27','-','Новый ресурс','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5233','1572776020','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5234','1572777284','2','kolomak9','27','4','Товар','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5235','1572779540','2','kolomak9','27','21','Вхідні двері','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5236','1572779555','2','kolomak9','27','52','Для квартири','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5237','1572779580','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5238','1572779589','2','kolomak9','3','3','Каталог','Viewing data for resource');

INSERT INTO `modx_manager_log` VALUES ('5239','1572779641','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5240','1572779664','2','kolomak9','22','2','Ditto','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5241','1572779678','2','kolomak9','22','5','Minify','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5242','1572779682','2','kolomak9','22','3','R','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5243','1572779690','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5244','1572779706','2','kolomak9','22','4','Breadcrumbs','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5245','1572779717','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5246','1572779720','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5247','1572779795','2','kolomak9','102','7','TransAlias','Edit plugin');

INSERT INTO `modx_manager_log` VALUES ('5248','1572779799','2','kolomak9','102','2','Shop plugin','Edit plugin');

INSERT INTO `modx_manager_log` VALUES ('5249','1572779842','2','kolomak9','112','1','Doc Manager','Execute module');

INSERT INTO `modx_manager_log` VALUES ('5250','1572779869','2','kolomak9','16','19','Catalog_gates','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5251','1572779879','2','kolomak9','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5252','1572779917','2','kolomak9','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5253','1572780009','2','kolomak9','16','19','Catalog_gates','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5254','1572780010','2','kolomak9','16','3','Catalog_inside_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5255','1572780022','2','kolomak9','16','6','Cart','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5256','1572780030','2','kolomak9','16','7','Checkout','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5257','1572780039','2','kolomak9','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5258','1572780058','2','kolomak9','16','17','Discounts','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5259','1572780065','2','kolomak9','16','14','pay-and-delivery','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5260','1572780073','2','kolomak9','16','15','Single news','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5261','1572780075','2','kolomak9','16','10','Service','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5262','1572780160','2','kolomak9','16','24','Content Page','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5263','1572780179','2','kolomak9','16','18','Catalog_entrance_doors','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5264','1572780194','2','kolomak9','301','28','URL фильтра \"Производитель\"','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5265','1572780227','2','kolomak9','16','4','SubCatalog','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5266','1572780237','2','kolomak9','301','27','Набор строк','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5267','1572780258','2','kolomak9','16','5','Product','Editing template');

INSERT INTO `modx_manager_log` VALUES ('5268','1572780369','2','kolomak9','301','28','URL фильтра \"Производитель\"','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5269','1572780395','2','kolomak9','301','28','URL фильтра \"Производитель\"','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5270','1572780403','2','kolomak9','301','27','Набор строк','Edit Template Variable');

INSERT INTO `modx_manager_log` VALUES ('5271','1572780423','2','kolomak9','78','50','tpl_door_types','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5272','1572780447','2','kolomak9','78','55','tpl_filters_del','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5273','1572780451','2','kolomak9','78','54','tpl_filtersInner_del','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5274','1572780455','2','kolomak9','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5275','1572780463','2','kolomak9','78','48','tpl_appointment','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5276','1572780468','2','kolomak9','78','53','tpl_catalog','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5277','1572780475','2','kolomak9','78','8','tpl_categoriesImg','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5278','1572780477','2','kolomak9','78','41','tpl_discount','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5279','1572780490','2','kolomak9','78','50','tpl_door_types','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5280','1572780494','2','kolomak9','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5281','1572780524','2','kolomak9','78','38','tpl_about_facts','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5282','1572780534','2','kolomak9','78','66','tpl_product_option_outer','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5283','1572780541','2','kolomak9','78','65','tpl_product_option_item','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5284','1572780546','2','kolomak9','78','71','tpl_getOptionGroup_wrap','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5285','1572780574','2','kolomak9','78','46','tpl_menu_catalog_second_active','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5286','1572780587','2','kolomak9','78','58','tpl_aria_option','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5287','1572780602','2','kolomak9','78','49','tpl_materials','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5288','1572780624','2','kolomak9','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5289','1572780629','2','kolomak9','78','3','tpl_filters','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5290','1572780680','2','kolomak9','78','3','tpl_filters','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5291','1572780684','2','kolomak9','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5292','1572780713','2','kolomak9','78','4','tpl_filtersInner','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5293','1572797858','2','kolomak9','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5294','1572799154','2','kolomak9','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5295','1572802160','2','kolomak9','78','9','HEAD','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5296','1572802169','2','kolomak9','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5297','1572802364','2','kolomak9','106','-','Advanced settings','Viewing Modules');

INSERT INTO `modx_manager_log` VALUES ('5298','1572802370','2','kolomak9','108','2','Pictures','Edit module');

INSERT INTO `modx_manager_log` VALUES ('5299','1572802397','2','kolomak9','22','8','Shop','Editing Snippet');

INSERT INTO `modx_manager_log` VALUES ('5300','1572802416','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5301','1572802674','2','kolomak9','78','18','HEADER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5302','1572802870','2','kolomak9','78','1','FOOTER','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5303','1572804874','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5304','1572804877','2','kolomak9','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5305','1572804899','2','kolomak9','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5306','1572804903','2','kolomak9','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5307','1572804926','2','kolomak9','31','-','-','Using file manager');

INSERT INTO `modx_manager_log` VALUES ('5308','1572804953','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5309','1572804964','2','kolomak9','78','6','tpl_filterPrice','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5310','1572804984','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5311','1572804992','2','kolomak9','78','78','tpl_menu_catalog_active_mob','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5312','1572805002','2','kolomak9','76','-','-','Element management');

INSERT INTO `modx_manager_log` VALUES ('5313','1572805019','2','kolomak9','78','63','full-gallery','Editing Chunk (HTML Snippet)');

INSERT INTO `modx_manager_log` VALUES ('5314','1572805198','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5315','1572805221','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5316','1572805229','2','kolomak9','27','3','Каталог','Editing resource');

INSERT INTO `modx_manager_log` VALUES ('5317','1572856729','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5318','1572856737','1','adminko','58','-','MODX','Logged in');

INSERT INTO `modx_manager_log` VALUES ('5319','1572856741','1','adminko','26','-','-','Refreshing site');

INSERT INTO `modx_manager_log` VALUES ('5320','1572856742','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5321','1572856743','1','adminko','116','-','-','Delete event log');

INSERT INTO `modx_manager_log` VALUES ('5322','1572856744','1','adminko','114','-','-','View event log');

INSERT INTO `modx_manager_log` VALUES ('5323','1572856748','1','adminko','93','-','-','Backup Manager');


# --------------------------------------------------------

#
# Table structure for table `modx_manager_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_manager_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_manager_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='Contains login information for backend users.';

#
# Dumping data for table `modx_manager_users`
#

INSERT INTO `modx_manager_users` VALUES ('1','adminko','uncrypt>5a0803463a7309b8909197fb887187ea26f0bdbd');

INSERT INTO `modx_manager_users` VALUES ('2','kolomak9','uncrypt>1d950f1b4a9c9431cb7b02fd8846212db1dea9fe');


# --------------------------------------------------------

#
# Table structure for table `modx_member_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_member_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_member_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_group` int(10) NOT NULL DEFAULT '0',
  `member` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_member` (`user_group`,`member`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_member_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_membergroup_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_membergroup_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_membergroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `membergroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_membergroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_membergroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_membergroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_membergroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_content`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_content`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `pagetitle_ua` varchar(255) NOT NULL DEFAULT '',
  `pagetitle_ru` varchar(255) NOT NULL DEFAULT '',
  `pagetitle_en` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle_ua` varchar(255) NOT NULL DEFAULT '',
  `longtitle_ru` varchar(255) NOT NULL DEFAULT '',
  `longtitle_en` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(512) NOT NULL DEFAULT '',
  `description_ua` varchar(512) NOT NULL DEFAULT '',
  `description_ru` varchar(512) NOT NULL DEFAULT '',
  `description_en` varchar(512) NOT NULL DEFAULT '',
  `alias` varchar(245) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `link_attributes_ua` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `link_attributes_ru` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `link_attributes_en` varchar(255) NOT NULL DEFAULT '' COMMENT 'Link attriubtes',
  `published` int(1) NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` int(1) NOT NULL DEFAULT '0',
  `introtext` text COMMENT 'Used to provide quick summary of the document',
  `introtext_ua` text COMMENT 'Used to provide quick summary of the document',
  `introtext_ru` text COMMENT 'Used to provide quick summary of the document',
  `introtext_en` text COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext,
  `content_ua` mediumtext,
  `content_ru` mediumtext,
  `content_en` mediumtext,
  `richtext` tinyint(1) NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `cacheable` int(1) NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0' COMMENT 'Date the document was published',
  `publishedby` int(10) NOT NULL DEFAULT '0' COMMENT 'ID of user who published the document',
  `menutitle` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `menutitle_ua` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `menutitle_ru` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `menutitle_en` varchar(255) NOT NULL DEFAULT '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Disable page hit count',
  `haskeywords` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to keywords',
  `hasmetatags` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'has links to meta tags',
  `privateweb` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Hide document from menu',
  `alias_visible` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `parent` (`parent`),
  KEY `aliasidx` (`alias`),
  KEY `typeidx` (`type`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`pagetitle_ua`,`pagetitle_ru`,`pagetitle_en`,`description`,`description_ua`,`description_ru`,`description_en`,`content`,`content_ua`,`content_ru`,`content_en`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COMMENT='Contains the site document tree.';

#
# Dumping data for table `modx_site_content`
#

INSERT INTO `modx_site_content` VALUES ('1','document','text/html','Головна','Головна','Главная','Home','Кроки для успішної покупки двері','Кроки для успішної покупки двері','Шаги для успешной покупки двери','Lorem Ipsum is simply dummy text','','','','','glavnaya','','','','','1','0','0','0','1','','','','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','','','','','1','2','0','1','0','1','1144904400','1','1572349224','0','0','0','0','1','Головна','Головна','Главная','Home','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('33','document','text/html','Остались вопросы','Залишилися питання','Остались вопросы','','Если у вас возникли какие-либо вопросы, заполните, пожалуйста, форму и мы свяжемся с вами в ближайшее время','Якщо у вас виникли будь-які питання, заповніть, будь ласка, форму і ми зв\'яжемося з вами найближчим часом','Если у вас возникли какие-либо вопросы, заполните, пожалуйста, форму и мы свяжемся с вами в ближайшее время','','','','','','ostalis-voprosy','','','','','1','0','0','0','0','','','',NULL,'_ru_ru_ru','_ru_ru_ua','_ru_ru_ru',NULL,'1','9','16','0','0','1','1567685515','1','1567692122','0','0','0','1567685515','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('2','document','text/html','404','404','404','404','Извините, мы не смогли найти эту страницу','Вибачте, ми не змогли знайти цю сторінку','Извините, мы не смогли найти эту страницу','It is not my fault buddy! <br> I think you got lost.','Главная страница откроется в течение','Головна сторінка відкриється протягом','Главная страница откроется в течение','','not-found','5','5','5','','1','0','0','0','0','секунд','секунд','секунд','','','','','','1','1','1','0','0','1','1144904400','1','1570621284','0','0','0','0','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('3','document','text/html','Каталог','Каталог','Каталог','Catalog','','','','','','','','','catalog','','','','','1','0','0','0','1','','','','','','','','','1','4','3','1','0','1','1144904400','1','1570624641','0','0','0','0','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('31','document','text/html','Карта сайта','Карта сайту','Карта сайта','','','','','','','','','','karta-sajta','','','','','1','0','0','0','0','','','',NULL,'','','',NULL,'1','23','14','1','0','1','1567679033','1','1570551731','0','0','0','1567679033','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('4','document','text/html','Товар','Товар','Товар','Product','','','','','','','','','item','','','','','1','0','0','0','0','','','','','<div class=\"strong\">Наличный расчет</div>\n<ul>\n<li>Оплата в салоне в национальной валюте</li>\n<li>Безналичный расчет</li>\n<li>Банковская карта-оплата картой VISA или Master Card в салоне через терминал</li>\n<li>Оплата онлайн. При оформлении заказа через наш интернет- магазин для дистанционной оплаты( не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.</li>\n</ul>\n<div class=\"strong\">Доставка</div>\n<ul>\n<li>Доставка заказов по г.Киев</li>\n<li>Доставка заказов за пределы г. Киев</li>\n<li>Доставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери.</li>\n</ul>','<div class=\"strong\">Готівковий розрахунок</div>\n<ul>\n<li>Оплата в салоні в національній валюті</li>\n<li>Безготівковий розрахунок</li>\n<li>Компанії-оплата карткою VISA або Master Card в салоні через термінал</li>\n<li>Оплата онлайн. При оформленні замовлення через наш інтернет- магазин для дистанційної оплати (не відвідуючи салон) ми відправляємо рахунок на e-mail або смс. Рахунок можна оплатити будь-карткою VISA і Master Card, через Приват 24 або іншим способом.</li>\n</ul>\n<div class=\"strong\">Доставка</div>\n<ul>\n<li>Доставка замовлень по м.Київ</li>\n<li>Доставка замовлень за межі м Київ</li>\n<li>Доставка замовлень в регіони України: Нова Пошта, Автолюкс, Делівері.</li>\n</ul>','<div class=\"strong\">Наличный расчет</div>\n<ul>\n<li>Оплата в салоне в национальной валюте</li>\n<li>Безналичный расчет</li>\n<li>Банковская карта-оплата картой VISA или Master Card в салоне через терминал</li>\n<li>Оплата онлайн. При оформлении заказа через наш интернет- магазин для дистанционной оплаты( не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.</li>\n</ul>\n<div class=\"strong\">Доставка</div>\n<ul>\n<li>Доставка заказов по г.Киев</li>\n<li>Доставка заказов за пределы г. Киев</li>\n<li>Доставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери.</li>\n</ul>','','1','5','3','1','0','1','1144904400','1','1570093746','0','0','0','0','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('5','document','text/html','Про магазин','Про магазин','О магазине','About Us','Наша компанія','Наша компанія','Наша компания','','Наша компанія - це, перш за все колектив, який формувався роками ізпрофессіоналов високого класу. Крім знань, набутих за роки роботи, ми маємо величезний досвід і розуміння як ринку в цілому, на якому працюємо, так і кожного конкретного покупця.\n','Наша компанія - це, перш за все колектив, який формувався роками ізпрофессіоналов високого класу. Крім знань, набутих за роки роботи, ми маємо величезний досвід і розуміння як ринку в цілому, на якому працюємо, так і кожного конкретного покупця.\n','Наша компания – это, прежде всего коллектив, который формировался годами изпрофессионалов высокого класса. Помимо знаний, приобретенных за годы работы, мы имеем огромный опыт и понимание как рынка в целом, на котором работаем, так и каждого конкретного покупателя.','','about-us','Факти','Факти','Факты','','1','0','0','0','1','Наша компанія - це, перш за все колектив, який формувався роками ізпрофессіоналов високого класу. Крім знань, набутих за роки роботи, ми маємо величезний досвід і розуміння як ринку в цілому, на якому працюємо, так і кожного конкретного покупця.','Наша компанія - це, перш за все колектив, який формувався роками ізпрофессіоналов високого класу. Крім знань, набутих за роки роботи, ми маємо величезний досвід і розуміння як ринку в цілому, на якому працюємо, так і кожного конкретного покупця.','Наша компания – это, прежде всего коллектив, который формировался годами изпрофессионалов высокого класса. Помимо знаний, приобретенных за годы работы, мы имеем огромный опыт и понимание как рынка в целом, на котором работаем, так и каждого конкретного покупателя.','','<p>Консультування та замовлення як вхідних, так і міжкімнатних дверей можна робити одним із зручних для Вас способів. Серед них: online-чат, Skype, або ж класичний телефонну розмову. Ми працюємо без перерви на обід і без вихідних! Для забезпечення максимальної зручності покупки, ми пропонуємо послугу безкоштовного виїзду майстра по виміру дверного отвору. Мало того, ця ж людина є менеджеромконсультантом здатним дати відповідь на будь-яке Ваше питання.</p>\n<p>Телефонуйте нам, і ми зможемо надати для вас такі умови замовлення, відмовитися откоторого буде просто неможливо! Ми раді співпраці, ваше бажання для нас - закон, і ми завжди готові йти назустріч клієнту.</p>\n<div class=\"heading-h3\">Компанія \"Гарант\" повністю впевнена в найвищій якості своєї продукції:</div>\n<ol>\n<li>Кожному покупцеві надається письмова гарантія на тривалий термін, а також післягарантійне обслуговування.</li>\n<li>Якщо монтаж виконується фахівцями компанії \"Гарант\", то можна бути впевненим, що двері встановлена надійно і правильно.</li>\n<li>Весь персонал проходить спеціальне навчання і постійно вдосконалюється.</li>\n</ol>\n<p>Так наша компанія забезпечує своїм клієнтам сервіс високого класу, який повністю відповідає світовим стандартам і нормам.</p>\n<p>В нашому інтернет-магазині знайти різні варіанти броньованих дверей, від самих бюджетних до моделей преміум-класу. Але якою б не була ціна, якість дверей завжди на найвищому рівні.</p>\n<p>Нехай комфорт і домашній затишок починаються з порога, а надійність і захищеність - зі сталевих дверей компанії \"Гарант\"!</p>','<p>Консультування та замовлення як вхідних, так і міжкімнатних дверей можна робити одним із зручних для Вас способів. Серед них: online-чат, Skype, або ж класичний телефонну розмову. Ми працюємо без перерви на обід і без вихідних! Для забезпечення максимальної зручності покупки, ми пропонуємо послугу безкоштовного виїзду майстра по виміру дверного отвору. Мало того, ця ж людина є менеджеромконсультантом здатним дати відповідь на будь-яке Ваше питання.</p>\n<p>Телефонуйте нам, і ми зможемо надати для вас такі умови замовлення, відмовитися откоторого буде просто неможливо! Ми раді співпраці, ваше бажання для нас - закон, і ми завжди готові йти назустріч клієнту.</p>\n<div class=\"heading-h3\">Компанія \"Гарант\" повністю впевнена в найвищій якості своєї продукції:</div>\n<ol>\n<li>Кожному покупцеві надається письмова гарантія на тривалий термін, а також післягарантійне обслуговування.</li>\n<li>Якщо монтаж виконується фахівцями компанії \"Гарант\", то можна бути впевненим, що двері встановлена надійно і правильно.</li>\n<li>Весь персонал проходить спеціальне навчання і постійно вдосконалюється.</li>\n</ol>\n<p>Так наша компанія забезпечує своїм клієнтам сервіс високого класу, який повністю відповідає світовим стандартам і нормам.</p>\n<p>В нашому інтернет-магазині знайти різні варіанти броньованих дверей, від самих бюджетних до моделей преміум-класу. Але якою б не була ціна, якість дверей завжди на найвищому рівні.</p>\n<p>Нехай комфорт і домашній затишок починаються з порога, а надійність і захищеність - зі сталевих дверей компанії \"Гарант\"!</p>','<p>Консультирование и заказ как входных, так и межкомнатных дверей можно производить одним из удобных для Вас способов. Среди них: online-чат, Skype, или же классический телефонный разговор. Мы работаем без перерыва на обед и без выходных! Для обеспечения максимального удобства покупки, мы предлагаем услугу бесплатного выезда мастера по замеру дверного проема. Мало того, этот же человек является менеджеромконсультантом способным дать ответ на любой Ваш вопрос.</p>\n<p>Звоните нам, и мы сможем предоставить для вас такие условия заказа, отказаться откоторых будет просто невозможно! Мы рады сотрудничеству, ваше желание для нас - закон, и мы всегда готовы идти навстречу клиенту.</p>\n<div class=\"heading-h3\">Компания \"Гарант\" полностью уверена в высочайшем качестве своей продукции:</div>\n<ol>\n<li>Каждому покупателю предоставляется письменная гарантия на длительный срок, а также постгарантийное обслуживание.</li>\n<li>Если монтаж выполняется специалистами компании \"Гарант\", то можно быть уверенным, что дверь установлена надежно и правильно.</li>\n<li>Весь персонал проходит специальное обучение и постоянно совершенствуется.</li>\n</ol>\n<p>Так наша компания обеспечивает своим клиентам сервис высокого класса, который полностью соответствует мировым стандартам и нормам.</p>\n<p>В нашем интернет- магазине найти различные варианты бронированных дверей, от самых бюджетных до моделей премиум-класса. Но какой бы ни была цена, качество дверей всегда на самом высоком уровне.</p>\n<p>Пусть комфорт и домашний уют начинаются с порога, а надежность и защищенность - со стальных дверей компании \"Гарант\"!</p>','','1','11','4','1','0','1','1508420260','1','1572539650','0','0','0','0','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('27','document','text/html','Оплата и доставка','Оплата і доставка','Оплата и доставка','','Оплата','Оплата','Оплата','','Наличный расчет\nОплата в салоне в национальной валюте\nБезналичный расчет\nБанковская карта-оплата картой VISA или Master Card в салоне через терминал.\nОплата онлайн\nПри оформлении заказа через наш интернет- магазин для дистанционной оплаты (не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.','Готівковий розрахунок\nОплата в салоні в національній валюті\nБезготівковій розрахунок\nКомпанії-оплата карткою VISA або Master Card в салоні через термінал.\nоплата онлайн\nПри оформленні замовлення через наш інтернет- магазин для дистанційної оплати (не відвідуючи салон) ми відправляємо рахунок на e-mail або смс. Рахунок можна оплатити будь-карткою VISA і Master Card, через Приват 24 або іншим способом.','Наличный расчет\nОплата в салоне в национальной валюте\nБезналичный расчет\nБанковская карта-оплата картой VISA или Master Card в салоне через терминал.\nОплата онлайн\nПри оформлении заказа через наш интернет- магазин для дистанционной оплаты (не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.','','oplata-i-dostavka','Доставка','Доставка','Доставка','','1','0','0','0','0','Доставка заказов по г.Киев\nДоставка заказов за пределы г. Киев\nДоставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери. Самовывоз возможен в отдельных случаях.','Доставка замовлень по м.Київ\nДоставка замовлень за межі м Київ\nДоставка замовлень в регіони України: Нова Пошта, Автолюкс, Делівері. Самовивіз можливий в окремих випадках.','Доставка заказов по г.Киев\nДоставка заказов за пределы г. Киев\nДоставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери. Самовывоз возможен в отдельных случаях.',NULL,'','','',NULL,'1','14','10','1','0','1','1567606141','1','1569943297','0','0','0','1567606141','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('28','document','text/html','Гарантия и возврат','Гарантія та повернення','Гарантия и возврат','','Гарантия','Гарантія','Гарантия','','Наличный расчет\nОплата в салоне в национальной валюте\nБезналичный расчет\nБанковская карта-оплата картой VISA или Master Card в салоне через терминал.\nОплата онлайн\nПри оформлении заказа через наш интернет- магазин для дистанционной оплаты (не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.','Готівковий розрахунок\nОплата в салоні в національній валюті\nБезготівковій розрахунок\nКомпанії-оплата карткою VISA або Master Card в салоні через термінал.\nоплата онлайн\nПри оформленні замовлення через наш інтернет- магазин для дистанційної оплати (не відвідуючи салон) ми відправляємо рахунок на e-mail або смс. Рахунок можна оплатити будь-карткою VISA і Master Card, через Приват 24 або іншим способом.','Наличный расчет\nОплата в салоне в национальной валюте\nБезналичный расчет\nБанковская карта-оплата картой VISA или Master Card в салоне через терминал.\nОплата онлайн\nПри оформлении заказа через наш интернет- магазин для дистанционной оплаты (не посещая салон) мы отправляем счет на e-mail или смс. Счет можно оплатить любой карточкой VISA и Master Card, через Приват 24 или другим способом.','','garantiya-i-vozvrat','Возврат','Повернення','Возврат','','1','0','0','0','0','Доставка заказов по г.Киев\nДоставка заказов за пределы г. Киев\nДоставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери. Самовывоз возможен в отдельных случаях.','Готівковий розрахунок\nОплата в салоні в національній валюті\nБезготівковій розрахунок\nКомпанії-оплата карткою VISA або Master Card в салоні через термінал.\nоплата онлайн\nПри оформленні замовлення через наш інтернет- магазин для дистанційної оплати (не відвідуючи салон) ми відправляємо рахунок на e-mail або смс. Рахунок можна оплатити будь-карткою VISA і Master Card, через Приват 24 або іншим способом.','Доставка заказов по г.Киев\nДоставка заказов за пределы г. Киев\nДоставка заказов в регионы Украины: Новая Почта, Автолюкс, Деливери. Самовывоз возможен в отдельных случаях.',NULL,'','','',NULL,'1','14','11','1','0','1','1567606179','1','1569943291','0','0','0','1567606179','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('29','document','text/html','Блог','Блог','Блог','','','','','','','','','','blog','','','','','1','0','0','0','1','','','',NULL,'','','',NULL,'1','16','12','1','0','1','1567606196','1','1567772052','0','0','0','1567606196','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('30','document','text/html','Контакты','Контакти','Контакты','','Посетите наш фирменный салон дверей по адресу:','Відвідайте наш фірмовий салон дверей за адресою:','Посетите наш фирменный салон дверей по адресу:','','','','','','kontakty','','','','','1','0','0','0','0','','','',NULL,'','','',NULL,'1','13','13','1','0','1','1567606390','1','1567772059','0','0','0','1567606390','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('32','document','text/html','Переваги покупки в нашому магазині','Переваги покупки в нашому магазині','Преимущества покупки в нашем магазине','','Індивідуальний підхід','Індивідуальний підхід','Индивидуальный подход','','Висока якість','Висока якість','Высокое качество','','preimushhestva-pokupki-v-nashem-magazine','Першокласний сервіс','Першокласний сервіс','Первоклассный сервис','','1','0','0','0','0','Ринкові ціни','Ринкові ціни','Рыночные цены',NULL,'','','',NULL,'1','0','15','0','0','1','1567684762','1','1572540092','0','0','0','1567684762','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('26','document','text/html','Услуги','Послуги','Услуги','','','','','','','','','','uslugi','','','','','1','0','0','0','1','','','',NULL,'','','',NULL,'1','10','9','1','0','1','1567606059','1','1567690689','0','0','0','1567606059','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('25','document','text/html','Акции','Акції','Акции','','','','','','','','','','akcii','','','','','1','0','0','0','1','','','',NULL,'','','',NULL,'1','17','8','1','0','1','1567606014','1','1568032997','0','0','0','1567606014','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('24','document','text/html','Автоворота','Автоворота','Автоворота','','За назвою','За назвою','По названию','','','','','','avtovorota','','','','','1','0','0','3','1','','','',NULL,'','','',NULL,'1','19','3','1','0','1','1567605863','1','1570803298','0','0','0','1567605863','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('23','document','text/html','Фурнітура','Фурнітура','Фурнитура','','','','','','','','','','furnitura-dveri','','','','','1','0','0','3','0','','','',NULL,'','','',NULL,'1','4','2','1','0','1','1567605837','1','1572597511','0','0','0','1567605837','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('22','document','text/html','Міжкімнатні двері','Міжкімнатні двері','Межкомнатные двери','','Топ продажів серед міжкімнатних дверей','Топ продажів серед міжкімнатних дверей','Топ продаж среди межкомнатных дверей','','','','','','mezhkomnatnye-dveri','','','','','1','0','0','3','1','','','',NULL,'','','',NULL,'1','3','1','1','0','1','1567605817','1','1572353261','0','0','0','1567605817','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('18','document','text/html','Cart','Кошик','Корзина','Cart','','','','','','','','','cart','','','','','1','0','0','0','0',NULL,'','','',NULL,'','','','1','6','5','1','0','1','1509975278','1','1510842651','0','0','0','1509975278','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('19','document','text/html','Оформить заказ','Оформити замовлення','Оформить заказ','Checkout','','','','','','','','','checkout','','','','','1','0','0','0','0','Как купить','Як купити','Как купить','','<ul>\n<li>Добавить в корзину двери, которые понравились. Заполнить контактные данные, чтобы мы могли с вами связаться</li>\n<li>В ближайшее время, мы перезвоним для уточнения заказа</li>\n<li>После подтверждения заказа отправляем счет для оплаты по e-mail или смс</li>\n<li>Отправляем заказ удобной для вас транспортной компанией Новая Почта, Автолюкс, Деливери.</li>\n</ul>','<ul>\n<li>Додати в кошик двері, які сподобалися. Заповнити контактні дані, щоб ми могли з вами зв\'язатися</li>\n<li>Найближчим часом, ми передзвонимо для уточнення замовлення</li>\n<li>Після підтвердження замовлення відправляємо рахунок для оплати по e-mail або смс</li>\n<li>Відправляємо замовлення зручною для вас транспортною компанією Нова Пошта, Автолюкс, Делівері.</li>\n</ul>','<ul>\n<li>Добавить в корзину двери, которые понравились. Заполнить контактные данные, чтобы мы могли с вами связаться</li>\n<li>В ближайшее время, мы перезвоним для уточнения заказа</li>\n<li>После подтверждения заказа отправляем счет для оплаты по e-mail или смс</li>\n<li>Отправляем заказ удобной для вас транспортной компанией Новая Почта, Автолюкс, Деливери.</li>\n</ul>','','1','7','6','1','0','1','1509985779','1','1570195137','0','0','0','1509985779','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('20','document','text/html','Спасибо за заказ','Спасибі за замовлення','Спасибо за заказ','Thanks for your order','Заказа №','Замовлення №','Заказа №','Below are the details of your order #[*order_num*]','Продолжить покупки','Продовжити покупки','Продолжить покупки','','order-success','','','','','1','0','0','0','0','Успех','Успіх','Успех','','<p>Ваш заказ отправлен на обработку</p>\n<p>С вами свяжется наш менеджер для уточнения деталей</p>','<p>Ваше замовлення відправлений на обробку</p>\n<p>З вами зв\'яжеться наш менеджер для уточнення деталей</p>','<p>Ваш заказ отправлен на обработку</p>\n<p>С вами свяжется наш менеджер для уточнения деталей</p>','','1','8','7','1','0','1','1510842637','1','1570020695','0','0','0','1510842637','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('21','document','text/html','Вхідні двері','Вхідні двері','Входные двери','','Тип відкриття','Тип відкриття','Тип открытия','','','','','','vhodnye-dveri','','','','','1','0','0','3','1','','','',NULL,'','','',NULL,'1','18','0','1','0','1','1567605796','1','1572433260','0','0','0','1567605796','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('34','document','text/html','Шаги для успешной покупки двери','Кроки для успішної покупки двері','Шаги для успешной покупки двери','','','','','','','','','','shagi-dlya-uspeshnoj-pokupki-dveri','','','','','1','0','0','1','1','','','',NULL,'','','',NULL,'1','0','0','0','0','1','1567686640','1','1567686640','0','0','0','1567686640','1','','','','','1','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('35','document','text/html','Замер','Замір','Замер','','После вызова замерщика на нашем сайте или по телефону, наш специалист приедет и сделает все нужные расчеты, а также проконсультирует Вас по всем вопросам.','Після виклику замірника на нашому сайті або по телефону, наш фахівець приїде і зробить всі необхідні розрахунки, а також проконсультує Вас з усіх питань.','После вызова замерщика на нашем сайте или по телефону, наш специалист приедет и сделает все нужные расчеты, а также проконсультирует Вас по всем вопросам.','','','','','','zamer','','','','','1','0','0','34','0','','','',NULL,'','','',NULL,'1','0','0','0','0','1','1567686932','1','1567686932','0','0','0','1567686932','1','','','','','1','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('36','document','text/html','Доставка','Доставка','Доставка','','После оформление заказа на сайте, наша компания гарантирует быструю доставку по Киеву.','Після оформлення замовлення на сайті, наша компанія гарантує швидку доставку по Києву.','После оформление заказа на сайте, наша компания гарантирует быструю доставку по Киеву.','','','','','','dostavka','','','','','1','0','0','34','0','','','',NULL,'','','',NULL,'1','0','1','0','0','1','1567686982','1','1567686982','0','0','0','1567686982','1','','','','','1','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('37','document','text/html','Установка','Установка','Установка','','Вам не придется переживать за процесс установки двери. Все будет сделано нашими работниками в кратчайшие сроки и удобное для Вас время.','Вам не доведеться переживати за процес установки двері. Все буде зроблено нашими працівниками в найкоротші терміни і зручне для Вас час.','Вам не придется переживать за процесс установки двери. Все будет сделано нашими работниками в кратчайшие сроки и удобное для Вас время.','','','','','','ustanovka','','','','','1','0','0','34','0','','','',NULL,'','','',NULL,'1','0','2','0','0','1','1567687040','1','1567687040','0','0','0','1567687040','1','','','','','1','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('38','document','text/html','Официальная гарантия','Офіційна гарантія','Официальная гарантия','','На всю продукцию мы предоставляем гарантийный период.','На всю продукцію ми надаємо гарантійний період.','На всю продукцию мы предоставляем гарантийный период.','','','','','','oficialnaya-garantiya','','','','','1','0','0','34','0','','','',NULL,'','','',NULL,'1','0','3','0','0','1','1567687103','1','1567687103','0','0','0','1567687103','1','','','','','1','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('39','document','text/html','Доставка дверей','Доставка дверей','Доставка дверей','','Доставка дверей по Киеву - бесплатно','Доставка дверей по Києву - безкоштовно','Доставка дверей по Киеву - бесплатно','','Доставка дверей по Киеву - бесплатно, если Вы оформляете заказ с установкой. В другом случае доставка по Киеву до подъезда 300грн. За пределы города Киева - 10грн/км от Киева.','Доставка дверей по Києву - безкоштовно, якщо Ви оформляєте замовлення з установкою. В іншому випадку доставка по Києву до під\'їзду 300грн. За межі міста Києва - 10грн / км від Києва.','Доставка дверей по Киеву - бесплатно, если Вы оформляете заказ с установкой. В другом случае доставка по Киеву до подъезда 300грн. За пределы города Киева - 10грн/км от Киева.','','dostavka-dverej','300','300','300','','1','0','0','26','0','Заказать доставку','Замовити доставку','Заказать доставку',NULL,'','','',NULL,'1','9','0','0','0','1','1567689947','1','1567693505','0','0','0','1567689947','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('40','document','text/html','Професійна установка дверей','Професійна установка дверей','Профессиональная установка дверей','','Якщо завмер здійснюється за містом-оплату уточнюйте у менеджера.','Якщо завмер здійснюється за містом-оплату уточнюйте у менеджера.','Если замер осуществляется за городом- оплату уточняйте у менеджера.','','Установка двері від 800грн., В залежності від ваги дверей і від складності дверногопроема. Замір вхідних дверей -120грн., Міжкімнатних -150 грн., (До десяти дверних прорізів). Якщо завмер здійснюється за містом-оплату уточнюйте у менеджера.','Установка двері від 800грн., В залежності від ваги дверей і від складності дверногопроема. Замір вхідних дверей -120грн., Міжкімнатних -150 грн., (До десяти дверних прорізів). Якщо завмер здійснюється за містом-оплату уточнюйте у менеджера.','Установка двери от 800грн., в зависимости от веса дверей и от сложности дверногопроема. Замер входной двери -120грн., межкомнатной -150 грн.,( до десяти дверных проемов). Если замер осуществляется за городом- оплату уточняйте у менеджера.','','professionalnaya-ustanovka-dverej','300','300','300','','1','0','0','26','0','Замовити установка','Замовити установка','Заказать установка',NULL,'','','',NULL,'1','9','1','1','0','1','1567690099','1','1572537602','0','0','0','1567690099','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('41','document','text/html','Оплата і доставка','Оплата і доставка','Оплата и доставка','','Компанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%','Компанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%','','','','','','oplata-i-dostavka1','','','','','1','0','0','5','0','','','',NULL,'','','',NULL,'1','0','0','0','0','1','1567693346','1','1571130154','0','0','0','1567693346','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('42','document','text/html','Акции','Акції','Акции','','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%','Компанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%','','','','','','akcii1','','','','','1','0','0','5','0','','','','','','','','','1','0','1','0','0','1','1567693351','1','1567757293','0','0','0','1567693478','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('43','document','text/html','Контакты','Контакти','Контакты','','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%','Компанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%','','','','','','kontakty1','','','','','1','0','0','5','0','','','','','','','','','1','0','2','0','0','1','1567693396','1','1567757289','0','0','0','1567693467','1','','','','','1','0','0','0','0','0','1','0');

INSERT INTO `modx_site_content` VALUES ('44','document','text/html','Статья','Стаття','Статья','','','','','','','','','','statya','','','','','1','0','0','29','0','','','',NULL,'','','',NULL,'1','15','0','1','0','1','1567768629','1','1567772056','0','0','0','1567768629','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('45','document','text/html','Акция','Акція','Акция','','','','','','','','','','akciya','','','','','1','0','0','25','0','','','',NULL,'','','',NULL,'1','22','0','1','0','1','1568027454','1','1570195543','0','0','0','1568027454','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('46','document','text/html','Откатные ворота','Відкатні ворота','Откатные ворота','','','','','','./images/catalogue-images/gates-1.svg','','./images/catalogue-images/gates-1.svg','','otkatnye','','','','','1','0','0','24','0','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568106477','1','1568899030','0','0','0','1568106477','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('47','document','text/html','Распашные','Розкривні','Распашные','','','','','','./images/catalogue-images/gates-1.svg','','./images/catalogue-images/gates-1.svg','','raspashnye','','','','','1','0','0','24','0','','','','','','','','','1','4','1','1','0','1','1568124152','1','1568899043','0','0','0','1568124244','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('48','document','text/html','Гаражные','Гаражні','Гаражные','','','','','','./images/catalogue-images/gates-1.svg','','./images/catalogue-images/gates-1.svg','','garazhnye','','','','','1','0','0','24','0','','','','','','','','','1','4','2','1','0','1','1568124156','1','1568899057','0','0','0','1568124291','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('49','document','text/html','Секционные','Секційні','Секционные','','','','','','./images/catalogue-images/gates-1.svg','','./images/catalogue-images/gates-1.svg','','sekcionnye','','','','','1','0','0','24','0','','','','','','','','','1','4','3','1','0','1','1568124159','1','1568899068','0','0','0','1568124315','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('50','document','text/html','Ролеты','Ролети','Ролеты','','','','','','./images/catalogue-images/gates-1.svg','','./images/catalogue-images/gates-1.svg','','rolety','','','','','1','0','0','24','0','','','','','','','','','1','4','4','1','0','1','1568124165','1','1568899085','0','0','0','1568124343','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('51','document','text/html','Для будинку','Для будинку','Для дома ','','','','','','','','','','dlya-doma','','','','','1','0','0','21','0','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568124422','1','1572433935','0','0','0','1568124422','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('52','document','text/html','Для квартири','Для квартири','Для квартиры','','','','','','','','','','dlya-kvartiry','','','','','1','0','0','21','0','','','',NULL,'','','',NULL,'1','4','1','1','0','1','1568124445','1','1572527946','0','0','0','1568124445','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('53','document','text/html','Назначение','Призначення','Назначение','','','','','','','','','','naznachenie','','','','','1','0','0','22','1','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568124514','1','1568898486','0','0','0','1568124514','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('54','document','text/html','Материал','Матеріал','Материал','','','','','','','','','','material','','','','','1','0','0','22','1','','','',NULL,'','','',NULL,'1','4','1','1','0','1','1568124618','1','1568898493','0','0','0','1568124618','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('55','document','text/html','Тип открытия','Тип відкриття','Тип открытия','','','','','','','','','','tip-otkrytiya','','','','','1','0','0','22','1','','','',NULL,'','','',NULL,'1','4','2','1','0','1','1568124635','1','1568898498','0','0','0','1568124635','1','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('56','document','text/html','У гостинну','У гостинну','В гостинную','','','','','','','','','','v-gostinnuyu','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568124678','1','1570715276','0','0','0','1568124678','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('57','document','text/html','В ванную, В туалет','У ванну, В туалет','В ванную, В туалет','','','','','','','','','','v-vannuyu-v-tualet','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','1','1','0','1','1568124699','1','1568894143','0','0','0','1568124699','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('58','document','text/html','В детскую','У дитячу','В детскую','','','','','','','','','','v-detskuyu','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','2','1','0','1','1568124717','1','1568894720','0','0','0','1568124717','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('59','document','text/html','В коридор','У коридор','В коридор','','','','','','','','','','v-koridor','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','3','1','0','1','1568124732','1','1568894733','0','0','0','1568124732','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('60','document','text/html','В прихожую','У вітальню','В прихожую','','','','','','','','','','v-prihozhuyu','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','4','1','0','1','1568124768','1','1568894752','0','0','0','1568124768','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('61','document','text/html','В спальню','У спальню','В спальню','','','','','','','','','','v-spalnyu','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','5','1','0','1','1568124787','1','1568894766','0','0','0','1568124787','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('62','document','text/html','На кухню','На кухню','На кухню','','','','','','','','','','na-kuhnyu','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','6','1','0','1','1568124805','1','1568894779','0','0','0','1568124805','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('63','document','text/html','Офисные','Офісні','Офисные','','','','','','','','','','ofisnye','','','','','1','0','0','53','0','','','',NULL,'','','',NULL,'1','4','7','1','0','1','1568124856','1','1568894790','0','0','0','1568124856','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('64','document','text/html','Шпонированные','Шпоновані','Шпонированные','','','','','','','','','','shponirovannye','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568124881','1','1568894616','0','0','0','1568124881','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('65','document','text/html','Эко-шпон','Еко-шпон','Эко-шпон','','','','','','','','','','eko-shpon','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','1','1','0','1','1568124903','1','1568894631','0','0','0','1568124903','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('66','document','text/html','Пленка ПВХ','Плівка ПВХ','Пленка ПВХ','','','','','','','','','','plenka-pvh','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','2','1','0','1','1568124920','1','1568894845','0','0','0','1568124920','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('67','document','text/html','МДФ','МДФ','МДФ','','','','','','','','','','mdf','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','3','1','0','1','1568124936','1','1568894877','0','0','0','1568124936','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('68','document','text/html','Массив сосны','Масив сосни','Массив сосны','','','','','','','','','','massiv-sosny','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','4','1','0','1','1568124969','1','1568894931','0','0','0','1568124969','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('69','document','text/html','Шпон дуба','Шпон дуба','Шпон дуба','','','','','','','','','','shpon-duba','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','5','1','0','1','1568124988','1','1568894945','0','0','0','1568124988','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('70','document','text/html','Шпон ясеня','Шпон ясеня','Шпон ясеня','','','','','','','','','','shpon-yasenya','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','6','1','0','1','1568125013','1','1568894989','0','0','0','1568125013','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('71','document','text/html','Шпон орех ','Шпон горіх','Шпон орех ','','','','','','','','','','shpon-oreh','','','','','1','0','0','54','0','','','',NULL,'','','',NULL,'1','4','7','1','0','1','1568125036','1','1568895016','0','0','0','1568125036','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('72','document','text/html','Раздвижные/Подвесные','Розсувні/Підвісні','Раздвижные/Подвесные','','','','','','','','','','razdvizhnye/podvesnye','','','','','1','0','0','55','0','','','',NULL,'','','',NULL,'1','4','0','1','0','1','1568125051','1','1568894670','0','0','0','1568125051','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('73','document','text/html','Распашные','Розкривні','Распашные','','','','','','','','','','raspashnye1','','','','','1','0','0','55','0','','','',NULL,'','','',NULL,'1','4','1','1','0','1','1568125084','1','1568894681','0','0','0','1568125084','1','','','','','0','0','0','0','0','0','0','1');

INSERT INTO `modx_site_content` VALUES ('74','document','text/html','Успех','Успіх','Успех','','Ваше письмо отправлено','Ваш лист відправлений','Ваше письмо отправлено','','Продолжить покупки','Продовжити покупки','Продолжить покупки','','uspeh','','','','','1','0','0','0','0','','','',NULL,'','','',NULL,'1','20','17','1','0','1','1570019011','1','1570019133','0','0','0','1570019011','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('75','document','text/html','Результаты поиска','Результати пошуку','Результаты поиска','','','','','','','','','','rezultaty-poiska','','','','','1','0','0','0','0','','','',NULL,'','','',NULL,'1','21','18','1','0','1','1570021929','1','1570021929','0','0','0','1570021929','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('77','document','text/html','Контентна сторинка','Контентна сторинка','Контентная страница','','','','','','З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','З іншого боку рамки і місце навчання кадрів дозволяє оцінити значення подальших напрямків розвитку. Завдання організації, особливо ж постійний кількісний ріст і сфера нашої активності вимагають від нас аналізу системи навчання кадрів, відповідає нагаль','','kontentna-storinka','','','','','1','0','0','0','0','','','',NULL,'<div class=\"heading-h2\">Що таке броньовані двері?</div>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<div class=\"heading-h3\">Переваги броньованих дверей</div>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<div class=\"heading-h4\">Недоліки броньованих дверей</div>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<div class=\"heading-h2\">Ознаки якісної броньованих дверей</div>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<div class=\"heading-h4\">Тепло- та звукоізоляційний шар</div>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<div class=\"heading-h2\">Ребра жорсткості і інша додатковий захист</div>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<div class=\"heading-h4\">Замки</div>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,</li>\n</ul>\n<div class=\"heading-h2\">Як вибрати обробку фасаду бронедверей</div>\n<div class=\"heading-h3\">Для обробки фасадів бронедверей використовують різні матеріали</div>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>','<div class=\"heading-h2\">Що таке броньовані двері?</div>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<div class=\"heading-h3\">Переваги броньованих дверей</div>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<div class=\"heading-h4\">Недоліки броньованих дверей</div>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.</li>\n</ul>\n<div class=\"heading-h2\">Ознаки якісної броньованих дверей</div>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<div class=\"heading-h4\">Тепло- та звукоізоляційний шар</div>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n<div class=\"heading-h2\">Ребра жорсткості і інша додатковий захист</div>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<div class=\"heading-h4\">Замки</div>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються,</li>\n</ul>\n<div class=\"heading-h2\">Як вибрати обробку фасаду бронедверей</div>\n<div class=\"heading-h3\">Для обробки фасадів бронедверей використовують різні матеріали</div>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>','<div class=\"heading-h2\">Що таке броньовані двері?</div>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<p>Броньовані двері - це металева конструкція, обшита листами стали з двох боків, всередині якої встановлені ребра жорсткості, а порожній простір заповнене пінопластом або мінеральною ватою. Прийнято вважати, що металеві броньовані двері відмінно захищають від злому і інших зовнішніх впливів. Але, бронедвері мають як плюсами так і мінусами.</p>\n<div class=\"heading-h3\">Переваги броньованих дверей</div>\n<ul>\n<ul>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>Броньовані двері краще захищають від вогню під час пожежі, ніж звичайні дерев\'яні двері.</li>\n<li>На відміну від дерев\'яних дверей, металева конструкція бронедверей не деформується з часом.</li>\n<li>Встановивши металеву броньовані двері, людина відчуває себе в безпеці.</li>\n</ul>\n</ul>\n<p><img src=\"images / news-img.jpeg\" alt=\"\" /></p>\n<div class=\"heading-h4\">Недоліки броньованих дверей</div>\n<ul>\n<ul>\n<ul>\n<li>Всі перераховані вище переваги виявляться марними, якщо не приділити достатню увагу замкам і захисних механізмів. Навіть найкращу двері зловмисники зможуть розкрити, якщо встановити дешеві і прості замки.</li>\n<li>У надзвичайній ситуації можливо скрутне потрапляння рятувальників.\n<div class=\"heading-h2\">Ознаки якісної броньованих дверей</div>\n</li>\n</ul>\n</ul>\n</ul>\n<p>Якісна броньовані двері залежить від товщини обшивки, від наявності захисної фурнітури, а також від замкової конструкції. Якщо правильно врахувати всі тонкощі цих елементів, можна отримати відмінний надійний варіант якісної броньованих дверей.</p>\n<p>При виборі броньованих дверей потрібно звертати увагу на товщину листів. Оптимальним варіантом є товщина листа 2 - 2,5 мм. Двері з товщиною 3 - 4 мм досить важкі і зазвичай використовуються для заміських будинків і котеджів. Також броньовані двері буде безпечніше, якщо металеві листи, якими обшита дверна конструкція, цільні, без швів. Такі двері набагато дорожче, але і якісніше.</p>\n<div class=\"heading-h4\">Тепло- та звукоізоляційний шар</div>\n<ul>\n<ul>\n<ul>\n<ul>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Для того щоб броньовані двері добре зберігали тепло у вашому домі, а також захищали від сторонніх шумів і звуків, двері утеплюють різними додатковими матеріалами.</li>\n<li>Найбільш затребуваним ізолюючим матеріалом є мінеральна вата. Такий матеріал екологічно нешкідливий, добре захищає від сторонніх шумів, не схильний до горіння, не виводить тепло з квартири і не пропускає холод з вулиці.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Для ізоляції ще використовують пінопласт. Він вологостійкий, добре захищає від сторонніх звуків, але пінопласт мало пожежобезпечний матеріал, і з часом він буде пропускати холод в квартиру.</li>\n<li>Досягти ефективної теплоізоляції можна за допомогою якісно виготовленого вироби. Двері повинні добре прилягати до короба.</li>\n<li>Ще одним способом ізоляції є обробка металевих дверей декоративними панелями МДФ.</li>\n</ul>\n</ul>\n</ul>\n</ul>\n<div class=\"heading-h2\">Ребра жорсткості і інша додатковий захист</div>\n<p>Ребра жорсткості підвищують захисні властивості дверей. Так як дверне полотно має велику площу, ребра жорсткості захищають полотно, підвищують його взломостойкость, запобігають деформацію полотна від зовнішніх навантажень. Крім того, що вони покращують захисні властивості дверного блоку, ребра жорсткості створюють товщину. Між листами утворюється простір, який заповнюється ізоляційним матеріалом.</p>\n<p>Додатковим захистом для броньованих дверей є петлі і наличники. Найкраще коли петлі знаходяться усередині дверної конструкції, в такому випадку їх неможливо буде збити або зрізати. Але, якщо вони розташовані зовні, для їх додаткового захисту використовують протизнімні штирі. Кількість петель завжди залежить від ваги дверей.</p>\n<div class=\"heading-h4\">Замки</div>\n<p>Для вхідних дверей виділяються такі різновиди замків:</p>\n<ul>\n<ul>\n<ul>\n<ul>\n<li>Класичним механізмом запирання є сувальдні замки. Особливість цього замку полягає в спеціальних пластинах. Для максимального захисту від злому найкраще використовувати броньовані конструкції з числом від 6 до 8 сувальд.</li>\n<li>Найпопулярнішим механізмом запирання є циліндрові замки. Всередині замкового механізму розташовані кілька невеликих циліндра, в них і полягає принцип роботи. Важливо відзначити, що для дверей з таким замком потрібно ще встановлювати додаткові броньовані накладки, що убезпечить циліндричну конструкцію від вибивання.</li>\n<li>Замок з моноблочною конструкцією об\'єднує в своїй роботі сувальдний і циліндровий механізми. Принцип роботи полягає в закриванні циліндрового замку спільно з підняттям шторки в сувальдном механізмі. Дана конструкція підвищує рівень взломостойкості дверного полотна.</li>\n<li>Електромеханічні замки не користуються величезним попитом, через досить високу вартість, але багато фахівців вважають що електромеханічні замки найбільш практичні і надійні. Принцип роботи такого механізму заснований на введенні комбінації цифр або букв на електронній панелі. До того ж використовується ще і фізичний ключ. У разі, якщо ви введете код неправильно, тільки фізичним ключем двері не відкрити.</li>\n<li>Для того, щоб відкрити двері з кодовим замком ключі не будуть потрібні, необхідна лише комбінація цифр. Але, це ж виступає їх великим недоліком. Так як з часом експлуатації цифри стираються</li>\n</ul>\n</ul>\n</ul>\n</ul>\n<div class=\"heading-h2\">Як вибрати обробку фасаду бронедверей</div>\n<div class=\"heading-h3\">Для обробки фасадів бронедверей використовують різні матеріали</div>\n<ul>\n<ul>\n<ul>\n<ul>\n<li>панелі МДФ</li>\n<li>кожвинил</li>\n<li>фарба</li>\n</ul>\n</ul>\n</ul>\n</ul>\n<p>Один з варіантів обробки - панелі МДФ. Матеріал має такі властивості: стійкий до впливу вологи і температурних перепадів, має хороший естетичний зовнішній вигляд, має високий рівень звуко- і теплоізоляції.</p>\n<p>Для обробки вхідних дверей, також використовують кожвинил. Матеріал має такі властивості: простий в експлуатації, має непоганий рівень звуко- і теплоізоляції. Кожвинил буває гладкий і тисненням. Досить поширений і бюджетний варіант.</p>\n<p>Для оздоблення бронедверей часто вибирають фарбування. Такий вид обробки забезпечує двері стійкістю до вологи, УФ-променів, надає дверного полотна антикорозійних властивостей. Для обробки фасадів використовують порошкові фарби, молоткові або нітроцелюлозні емалі. Кожен фарбувальний склад має свої особливості.</p>\n<p>Перед тим, як вибрати броньовані двері, рекомендуємо вам вивчити всі тонкощі і особливості цього виробу, ознайомитися з відгуками покупців і, в підсумку, придбати якісний товар, який прослужить багато років.</p>',NULL,'1','24','19','1','0','1','1571126740','1','1571139698','0','0','0','1571126740','1','','','','','0','0','0','0','0','0','1','1');

INSERT INTO `modx_site_content` VALUES ('79','document','text/html','','Розкрівні','Распашные','','','','','','','','','','','','','','','0','0','0','24','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','19','999999','1','0','0','0','0','0','0','0','0','0','0','','','','','0','0','0','0','0','0','0','0');

INSERT INTO `modx_site_content` VALUES ('80','document','text/html','Новый тип открытия','Новый тип открытия','Новый тип открытия','','','','','','','','','','novyj-tip-otkrytiya','','','','','1','0','0','55','0','','','','','','','','','1','4','1','1','0','1','1572523016','1','1572523035','0','0','0','1572523035','1','','','','','0','0','0','0','0','0','0','1');


# --------------------------------------------------------

#
# Table structure for table `modx_site_content_metatags`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_content_metatags`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_content_metatags` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `metatag_id` int(11) NOT NULL DEFAULT '0',
  KEY `content_id` (`content_id`),
  KEY `metatag_id` (`metatag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Reference table between meta tags and content';

#
# Dumping data for table `modx_site_content_metatags`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_htmlsnippets`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_htmlsnippets`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `editor_name` varchar(50) NOT NULL DEFAULT 'none',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COMMENT='Contains the site chunks.';

#
# Dumping data for table `modx_site_htmlsnippets`
#

INSERT INTO `modx_site_htmlsnippets` VALUES ('1','FOOTER','Base footer chunk.','2','none','1','0','<footer class=\"footer\">\n	<div class=\"footer-top\">\n		<div class=\"container\">\n			<div class=\"footer__row\">\n				<div class=\"footer-block actions\">\n					<div class=\"logo\"><img src=\"./images/logo-white.svg\" alt=\"[(site_name)]-2\" alt=\"[(site_name)]-2\"></div>\n					<div class=\"phones\">\n						<a href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_1)]`]]\">[(as_phone_1)]</a>\n						<a href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_2)]`]]\">[(as_phone_2)]</a>\n					</div><a class=\"btn js_call-me\" href=\"#call-me\">[#40#]</a>\n				</div>\n				<div class=\"footer-block footer__nav\">\n					<div class=\"footer__nav-col\">\n						<ul class=\"footer__nav-list\">\n							[[Shop?&get=`getMenu`&tpl=`tpl_menu_footer`&tplActive=`tpl_menu_footer_active` &exclude=`25,26`]]\n						</ul>\n					</div>\n					<div class=\"footer__nav-col\">\n						<ul class=\"footer__nav-list\">\n							[[Shop?&get=`getMenu`&ids=`25,5,22,21,24`&tpl=`tpl_menu_footer`&tplActive=`tpl_menu_footer_active`]]\n						</ul><a class=\"btn js_call\" href=\"#call\">[#39#]</a>\n					</div>\n				</div>\n				<div class=\"footer-block contacts\">\n					<p class=\"contacts-title\">[#43#]</p>\n					<div class=\"contacts-wrapper\">\n						<div class=\"contacts__item schedule\"> \n							<p>[#44#]</p>\n							<p>[#45#]</p>\n							<p>[#46#]</p>\n						</div>\n						<address class=\"contacts__item address\">\n							<p>[#47#]</p>\n							<p>[#48#]</p>\n							<p>[#49#]</p>\n						</address>\n						<div class=\"contacts__item email\"><a href=\"mailto:[(emailsender)]\"> [(emailsender)]</a></div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n	<div class=\"footer-bottom\">\n		<div class=\"container\">\n			<div class=\"footer__row\">\n				<div class=\"service-info\">[#41#]</div>\n				<div class=\"dev-block\">[#42#] - &nbsp;<a class=\"aj-logo\" href=\"//artjoker.ua\" target=\"_blank\"></a></div>\n			</div>\n		</div>\n	</div>\n</footer>\n<div id=\"cart_wrap\">\n	{{order}}\n</div>\n{{call}}\n{{call-me}}\n{{request-send}}\n{{call_me-send}}\n{{empty_cart}}\n{{added_to_cart}}\n[!if?&is=`[*template*]:=:5`&then=`{{full-gallery}}`!]\n</div>\n<script type=\"text/javascript\" src=\"js/lib.min.js\"></script>\n<script type=\"text/javascript\" src=\"js/main.js\"></script>\n<script type=\"text/javascript\" src=\"js/function.js\"></script>\n</body>\n</html>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('32','tpl_img_main_slider','','2','none','5','0','<div class=\"slider__item\">\n	<div class=\"slider-img\"><img src=\"[[R?&img=`{image}`&opt=`w=1920&h=765&far=1`]]\" alt=\"{alt}\" title=\"{title}\"></div>\n	<div class=\"container\"> \n		<div class=\"slider-caption\">\n			<div class=\"slider__text\">{description}</div>\n			<a href=\"{link}\" class=\"btn btn-orange btn-md\">{title}</a>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('33','tpl_advantages','','2','none','14','0','<div class=\"container\">\n	<div class=\"section-title section-title--before\">{pagetitle}</div>\n</div>\n<div class=\"container container-advantages\">\n	<ul class=\"advantages-list\">	\n		<li class=\"advantages__item\">\n			<div class=\"icon-wrapper\">\n				<div class=\"advantage-img advantage-img--individual\"></div>\n			</div>\n			<div class=\"text-wrapper\"> \n				<div class=\"text\">{longtitle}</div>\n			</div>\n		</li>\n		<li class=\"advantages__item\">\n			<div class=\"icon-wrapper\">\n				<div class=\"advantage-img advantage-img--service\"></div>\n			</div>\n			<div class=\"text-wrapper\"> \n				<div class=\"text\">{description}</div>\n			</div>\n		</li>\n		<li class=\"advantages__item\">\n			<div class=\"icon-wrapper\">\n				<div class=\"advantage-img advantage-img--quality\"></div>\n			</div>\n			<div class=\"text-wrapper\"> \n				<div class=\"text\">{link_attributes}</div>\n			</div>\n		</li>\n		<li class=\"advantages__item\">\n			<div class=\"icon-wrapper\">\n				<div class=\"advantage-img advantage-img--price\"></div>\n			</div>\n			<div class=\"text-wrapper\"> \n				<div class=\"text\">{introtext}</div>\n			</div>\n		</li>\n	</ul>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('19','order','popup order','2','none','12','0','<div class=\"order-popup\" id=\"order\" style=\"display:none\">\n	<div class=\"order-wrapper\">\n		<div class=\"order-header\">\n			<div class=\"heading-h3\">\n				[#94#] \n				<span class=\"order-amount js_count\">[!Shop?&get=`getCartCount`!]</span>\n				<span class=\"js_declination\">[!Shop?&get=`getCartCountDeclination`!] </span>\n				[#116#] \n				<span class=\"order-sum js_total\">[!Shop?&get=`getCartTotal`!]</span>\n				 [*sing*]\n			</div>\n		</div>\n		<ul class=\"order-body js_cart_list\">\n			[!Shop?&get=`getCart`&tpl=`tpl_cart`&tplCharac=`tpl_cart_characteristics`!]\n		</ul>\n		<div class=\"order-footer\"><button class=\"btn btn-bordered btn-big\" data-fancybox-close>[#104#]</button>\n			<div class=\"submit-block\">\n				<div class=\"order-total\">\n					[#105#]&nbsp;<span class=\"sum js_total\">[!Shop?&get=`getCartTotal`!]</span> [*sing*]</div>\n				<a class=\"btn btn-orange btn-big\" href=\"[~~[(checkout)]~~]\">[#106#]</a>\n			</div>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('67','tpl_cart','','2','none','3','0','<li class=\"order__item\">\n	<button class=\"btn-remove\" data-delete=\"{id}\"><i class=\"icon icon-trash\"></i></button>\n	<a class=\"btn-edit\" href=\"{product_url}&edit_prod={id}\"><i class=\"icon icon-pencil\"></i></a>\n	<div class=\"order__item-content\">\n		<div class=\"order__item-info\">\n			<div class=\"order__item-img\"><img src=\"[[R?&img=`{product_cover}`&opt=`w=62&h=118&far=1`]]\" alt=\"{alt}\" title=\"{alt}\"></div>\n			<div class=\"order__item-desc\">\n				<div class=\"order__item-title\">{product_name}</div>\n				<div class=\"order__item-vendor\">\n					[#107#]: <span>{product_code}</span></div>\n				<ul class=\"order__item-desc-list\">\n					{characteristic}\n				</ul>\n			</div>\n		</div>\n		<div class=\"order__item-total\">\n			<div class=\"order__item-quantity js_quantity\">\n				<button class=\"btn-qvt btn-qvt--minus\" data-field=\"quantity-{id}\"><i class=\"icon icon-minus\" data-field=\"quantity-{id}\"></i></button>\n				<div class=\"order__item-quantity__input\">\n					<input type=\"number\" class=\"js_cart_quantity\" data-pid=\"{id}\" name=\"quantity-{id}\" value=\"{product_count}\" min=\"1\" max=\"99\" step=\"1\">\n				</div>\n				<button class=\"btn-qvt btn-qvt--plus\" data-field=\"quantity-{id}\"><i class=\"icon icon-plus\" data-field=\"quantity-{id}\"></i></button>\n			</div>\n			<div class=\"order__item-sum\"><span>{product_total} </span><span> {sing}</span></div>\n		</div>\n	</div>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('68','tpl_cart_characteristics','','2','none','3','0','<li class=\"order__item-desc-list__item\"><span>{filter_name}: </span><span>{filter_value}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('71','tpl_getOptionGroup_wrap','','2','none','15','0','<div class=\"equipment-list__item-wrapper\">\n	<a data-poid=\"{id}\" data-po_price=\"{price}\" data-po_price_old=\"{price_old}\" data-product_code=\"{product_code}\" class=\"equipment-list__item{active}\">\n		<div class=\"equipment-list__item-header\">\n			<div class=\"equipment-list__item-title\">{name}</div>\n			<div class=\"equipment-list__item-price\"> <span class=\"cost\">{price}&nbsp;</span><span class=\"currency\">{sing}</span></div>\n		</div>\n		<div class=\"equipment-list__item-body\">\n			<div class=\"properties-list\">\n				{charac_list}\n			</div>\n		</div>\n	</a>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('72','tpl_getOptionGroup','','2','none','15','0','<div class=\"properties-list__item\" data-f_url=\"{filter_url}\">\n	<div class=\"title\">{filter_name}</div>\n	<div class=\"value\">{filter_value}</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('2','pagination_backend_enable','<span style=\"color:red\">Admin</span> Pagination in backend.','0','none','6','0','<li><a href=\"{url}\" class=\"{class}\">{title}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('3','tpl_filters','Item from filters.','2','none','2','0','<div class=\"filters-block__row\">\n	<div class=\"filters-box\">\n		<div class=\"filters-title active\">{filter_name}</div>\n		<ul class=\"filters-list active\">\n			{filter_inner}\n		</ul>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('4','tpl_filtersInner','Item values from filter.','2','none','2','0','<li class=\"filters-list__item\">\n	<a class=\"filters-list-link{class}[[if?&is=`{filter_active}:gt:0`&then=` active`]]\" data-filter=\"{filter_url}={value_url}\" href=\"[~~[*id*]~~]f/{filter_url}={value_url}/\">\n		<span class=\"filters-list-lbl\">{filter_value}&nbsp;<span class=\"amount\">({value_count})</span></span>\n	</a>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('5','mm_rules','<span style=\"color:red\">Admin</span> Default ManagerManager rules.','0','none','6','0','// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php\n// example of how PHP is allowed - check that a TV named documentTags exists before creating rule\n\nif ($modx->db->getValue($modx->db->select(\'count(id)\', $modx->getFullTableName(\'site_tmplvars\'), \"name=\'documentTags\'\"))) {\n	mm_widget_tags(\'documentTags\', \' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV\n}\nmm_widget_showimagetvs(); // Always give a preview of Image TVs\nmm_images(2, \"Images\", \"2,4\"); // Create a table with an image module (Module Id, \"Title\", \"Templates (with comma)\")\nmm_ddGMap(\'tv_gmap\');','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('6','tpl_filterPrice','Slider from filter price.','2','none','2','0','<div class=\"filters-block__row\">\n	<div class=\"filters-box\">\n		<div class=\"filters-title--no-active\">[#75#]</div>\n		<div class=\"filters-range\">\n			<div class=\"form__item\">\n				<input class=\"filters-input form-control\" id=\"range-amount\" type=\"number\" min=\"{minimum}\" max=\"{maximum}\" name=\"price[max]\" value=\"{maxmax}\"><span class=\"currency\">{sing}</span>\n			</div>\n			<div class=\"range-line\">\n				<input class=\"range-slider\" name=\"price[max]\" type=\"text\" data-slider-id=\"runDifficultySlider\" data-slider-min=\"{minimum}\" data-slider-max=\"{maximum}\" data-slider-step=\"1\" data-slider-value=\"{maxmax}\">\n			</div>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('7','tpl_paginateNext','Next page from pagination.','2','none','7','0','<li class=\"pagination-list__item\"><a class=\"pagination-nav__item pagination-nav__item--next icon icon-arrow\" href=\"{page_next}\"></a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('8','tpl_categoriesImg','List images for category item.','0','none','2','0','<div class=\"swiper-slide\"><img src=\"[[R?&img=`{img}`&opt=`w=297&h=283&zc=1`]]\" alt=\"{alt}\"></div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('9','HEAD','Base head chunk.','2','none','1','0','<!DOCTYPE html>\n<html lang=\"[(lang)]\">\n<head>\n    <base href=\"[(site_url)]\" />\n    <meta charset=\"[(modx_charset)]\" />\n    <meta http-equiv=\"Cache-Control\" content=\"no-cache\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\n    <link rel=\"icon\" sizes=\"32x32\" type=\"image/x-icon\" href=\"images/favicon/favicon-32x32.ico\">\n	<link rel=\"icon\" sizes=\"16x16\" type=\"image/x-icon\" href=\"images/favicon/favicon-16x16.ico\">\n    <title>[*seo_title*]</title>[*seo_head*][+rel_previous+][+rel_next+]\n    <!-- SOCIAL MEDIA META -->\n    <meta property=\"og:description\" content=\"[(site_name)]\">\n    [[if?&is=`[*tv_img*]:empty`\n        &then=`<meta property=\"og:image\" content=\"[(site_url)]images/login-logo.png\"><meta name=\"twitter:image\" content=\"[(site_url)]images/login-logo.png\">`\n        &else=`<meta property=\"og:image\" content=\"[(site_url)][*tv_img*]\"><meta name=\"twitter:image\" content=\"[(site_url)][*tv_img*]\">`\n    ]]\n    <meta property=\"og:site_name\" content=\"[(site_name)]\">\n    <meta property=\"og:title\" content=\"[(site_name)]\">\n    <meta property=\"og:type\" content=\"website\">\n    <meta property=\"og:url\" content=\"[*seo_canonical_link*]\">\n    <!-- TWITTER META -->\n    <meta name=\"twitter:card\" content=\"summary\">\n    <meta name=\"twitter:site\" content=\"@[(site_name)]\">\n    <meta name=\"twitter:creator\" content=\"@[(site_name)]\">\n    <meta name=\"twitter:title\" content=\"[(site_name)]\">\n    <meta name=\"twitter:description\" content=\"[(site_name)]\">\n	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/lib.min.css\">\n	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\n</head>\n<body class=\"trans\">\n<noscript>\n    <div class=\"error-js\">[#1#] <a href=\'https://artjoker.ua/ru/blog/how-to-enable-javascript/\' target=\'_blank\' class=\'text-link\' rel=\'nofollow\'>[#2#]</a>[#3#]</div>\n</noscript>\n<!--[if lte IE 8]>\n<p class=\"off-js\">[#4#]</p>\n<![endif]-->','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('10','tpl_paginateCurrent','Current page from pagination.','2','none','7','0','<li class=\"pagination-list__item active\"><span class=\"pagination-list-link\">{page_title}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('11','tpl_ajaxSearch','Currency menu item.','0','none','8','0','<p class=\"item-name h3\"><a href=\"{product_url}\">{product_name}</a></p>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('12','pagination_backend_disable','<span style=\"color:red\">Admin</span> Pagination in backend.','0','none','6','0','<li class=\"active\"><span>{title}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('13','mail_order_item','Base mail_order_item chunk.','2','none','9','0','<table bgcolor=\"#ffffff\" style=\"width: 100%\" cellpadding=\"0\" cellspacing=\"0\">\n	<tr>\n		<td width=\"600\">\n			<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border: 1px solid #e3e3e3; vertical-align:top;\">\n				<tr>\n					<td style=\"padding: 20px 0px 20px 20px;vertical-align:top;\">\n						<img width=\"62px\" src=\"[(site_url)][[R?&img=`{product_cover}`&opt=`w=62&h=117&far=1`]]\" alt=\"{product_name}\" title=\"{product_name}\">\n					</td>\n					<td style=\"padding: 20px 10px 0px 0px;vertical-align:top;\">\n						<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n							<tr>\n								<td colspan=\"2\">\n									<table cellpadding=\"0\" cellspacing=\"0\">\n										<tr>\n											<td style=\"color: #000000; font-size: 18px;line-height: 21px; font-weight: 300\">\n												<a href=\"{product_url}\" target=\"_blank\" style=\"text-decoration: none\">\n													<div style=\"color: #000000; font-size: 18px;line-height: 21px; font-weight: 300\">{product_name}</div>\n												</a>\n											</td>\n										</tr>\n										<tr height=\"10\"><td></td></tr>\n									</table>\n								</td>\n							</tr>\n							<tr>\n								<td>\n									<table cellpadding=\"0\" cellspacing=\"0\">\n										<tr>\n											<td style=\"color: #FF6928; font-size: 12px; line-height: 14px; font-weight: 600\">\n												<p style=\"color: #FF6928; font-size: 12px; line-height: 14px; font-weight: 600\">[#107#]: <span>{product_code}</span></p>\n											</td>\n										</tr>\n										<tr height=\"10\"><td></td></tr>\n										{charac}\n									</table>\n								</td>\n								<td style=\"vertical-align: bottom\">\n									<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n										<tr>\n											<td style=\"color: #DC4C4C; font-size: 28px; line-height: 33px; font-weight: 300; text-align: right\">\n												<p style=\"color: #DC4C4C; font-size: 28px; line-height: 33px; font-weight: 300; text-align: right; margin-bottom: -15px\">\n													{product_price} <span>{sing}</span>\n												</p>\n											</td>\n										</tr>\n									</table>\n								</td>\n							</tr>\n						</table>\n					</td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr height=\"30\"></tr>\n</table>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('14','tpl_deliveryAddress_1','Self-delivery address.','0','none','10','0','<div class=\"tab-title tab-title-sub\">Выберите адрес</div>\n<div class=\"input-group\">\n    <span class=\"input-group-addon\"><span class=\"input-icon input-icon-city\"></span><span class=\"input-text\">Город</span></span>\n    <div class=\"large-selectbox clearfix\">\n        <select id=\"self-city\" name=\"checkout[delivery][self_city]\" class=\"chosen-select\">\n            {settings}\n        </select>\n    </div>\n</div>\n<p class=\"self-city\">{setting_value}</p>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('15','tpl_paginateIndent','Item indent pages from pagination.','2','none','7','0','<li class=\"pagination-list__item\"><span class=\"pagination-nav__item pagination-nav__item--more\">...</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('16','tpl_paginatePrev','Previous page from pagination.','2','none','7','0','<li class=\"pagination-list__item\"><a class=\"pagination-nav__item pagination-nav__item--prev icon icon-arrow\" href=\"{page_prev}\"></a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('17','tpl_paginate','Item page from pagination.','2','none','7','0','<li class=\"pagination-list__item\"><a class=\"pagination-list-link\" href=\"{page_value}\">{page_title}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('18','HEADER','Base header chunk.','2','none','1','0','<div class=\"wrapper\">\n	<header class=\"header header-desktop\">\n		<div class=\"container container-header\">\n			<div class=\"header__top\">\n				<div class=\"header__row\">\n					<ul class=\"nav\">\n						[[Shop?&get=`getMenu`&tpl=`tpl_menu_top`&tplActive=`tpl_menu_top_active` &exclude=`25,26`]]\n					</ul>\n					<div class=\"right-block right-block--top\">\n						<div class=\"languages\"> <span class=\"lang__item\">[+lang+]</span>\n							<ul class=\"dropdown\" style=\"display:none\">\n								[[Shop? &get=`getLangList` &tpl=`tpl_lang` &tplActive=`tpl_lang_active`]]\n							</ul>\n						</div>\n						<div class=\"phones-block\">\n							<a class=\"phone__item\" href=\"tel:+380678308885\">(067) 830 88 85</a>\n							<div class=\"phones-block-dropdown\">\n								<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_1)]`]]\">[(as_phone_1)]</a>\n								<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_2)]`]]\">[(as_phone_2)]</a>\n								<div class=\"schedule\">\n									<p>[#44#]</p>\n									<p>[#45#]</p>\n									<p>[#46#]</p>\n								</div>\n							</div>\n						</div>\n						<div class=\"address\">\n							<address class=\"address__text\">[#37#]</address>\n						</div>\n					</div>\n				</div>\n				<div class=\"header__row header__row--main\">\n					<div class=\"logo\">\n						[[if?&is=`[*id*]:is:[(site_start)]`&then=`\n						<span><img src=\"/images/logo.svg\" alt=\"[(site_name)]\" title=\"[(site_name)]\"></span>`&else=`\n						<a href=\"[~~[(site_start)]~~]\"><img src=\"/images/logo.svg\" alt=\"[(site_name)]\" title=\"[(site_name)]\"></a>`]]\n					</div>\n					<div class=\"search-wrapper\">\n						<form class=\"js_validate\" id=\"search-form\" action=\"[~~75~~]\" method=\"GET\">\n							<div class=\"search-form\">\n								<div class=\"form-row form-row--wide\">\n									<input class=\"form-input--search\" id=\"search\" type=\"search\" name=\"search\" inputmode=\"search\" placeholder=\"[#38#]\">\n								</div>\n								<button type=\"submit\"><span class=\"icon icon-search\"></span></button>\n							</div>\n						</form>\n					</div>\n					<a class=\"btn btn-orange btn-call hide-call\" href=\"#call\">[#39#]</a>\n					<div class=\"right-block\"> \n						<a class=\"btn btn-bordered btn-sm js_call-me\" href=\"#call-me\">[#40#]</a>\n						<div class=\"actions\">\n							<button class=\"btn-search js_search\" type=\"button\"><span class=\"icon icon-search\"></span></button>\n							[[if?&is=`[*getCartCount*]:empty`&then=`\n							<a class=\"js_ico_cart btn-order\" href=\"#empty_cart\">\n								<span class=\"order-circle js_count\"></span>\n								<span class=\"icon icon-order\"></span>\n							</a>`&else=`							\n							<a class=\"js_ico_cart btn-order active\" href=\"#order\">\n								<span class=\"order-circle js_count\">[*getCartCount*]</span>\n								<span class=\"icon icon-order\"></span>\n							</a>`]]\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class=\"header__bottom\">\n				<nav class=\"header-menu-wrapper\">\n					<ul class=\"main-menu\">			\n						[[Shop?&get=`getMenu`&dept=`1`&did=`[(shop_catalog_root)]`&tpl=`tpl_menu_catalog`&tplActive=`tpl_menu_catalog_active`&include=`25,26`]]\n					</ul>\n				</nav><a class=\"btn btn-orange btn-call hide-call-bottom\" href=\"#call\">[#39#]</a>\n			</div>\n		</div>\n	</header>\n	<header class=\"header header-mobile\">\n		<div class=\"container container-header\">\n			<div class=\"header__top\">\n				<div class=\"header__row\">					\n					[[if?&is=`[*id*]:is:[(site_start)]`&then=`\n					<span class=\"logo\"><img src=\"/images/logo.svg\" alt=\"[(site_name)]-mob\" title=\"[(site_name)]-mob\"></span>`&else=`\n					<a class=\"logo\" href=\"[~~[(site_start)]~~]\"><img src=\"/images/logo.svg\" alt=\"[(site_name)]-mob\" title=\"[(site_name)]-mob\"></a>`]]\n					<div class=\"right-block\"> <a class=\"btn btn-bordered btn-sm js_call-me\" href=\"#call-me\">[#40#]</a>\n						<div class=\"languages\"> <span class=\"lang__item\">[+lang+]</span>\n							<ul class=\"dropdown\" style=\"display:none\">\n								[[Shop? &get=`getLangList` &tpl=`tpl_lang` &tplActive=`tpl_lang_active`]]\n							</ul>\n						</div>\n					</div>\n				</div>\n				<div class=\"header__row header__row--phones\">\n					<div class=\"phones-block\">\n						<span class=\"phone__item\">(067) 830 88 85</span>\n						<div class=\"phones-block-dropdown\">\n							<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_1)]`]]\">[(as_phone_1)]</a>\n							<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_2)]`]]\">[(as_phone_2)]</a>\n							<div class=\"schedule\">\n								<p>[#44#]</p>\n								<p>[#45#]</p>\n								<p>[#46#]</p>\n							</div>\n						</div>\n					</div>\n				</div>\n			</div>\n			<div class=\"header__bottom\">\n				<div class=\"header__row\">\n					<div class=\"menu-btn\"> <span class=\"burger-line\"></span><span class=\"burger-line\"></span><span class=\"burger-line\"></span></div>\n					<div class=\"search-wrapper\">\n						<form class=\"js_validate\" id=\"search-form\" action=\"[~~75~~]\" method=\"GET\">\n							<div class=\"search-form\">\n								<div class=\"form-row form-row--wide\">\n									<input class=\"form-input--search\" id=\"search\" type=\"search\" name=\"search\" inputmode=\"search\" placeholder=\"[#38#]\">\n								</div>\n								<button type=\"submit\"><span class=\"icon icon-search\"></span></button>\n							</div>\n						</form>\n					</div>\n					<a class=\"btn btn-orange btn-call hide-call\" href=\"#call\">[#39#]</a>\n					<div class=\"actions\">\n						<button class=\"btn-search js_search\" type=\"button\"><span class=\"icon icon-search\"></span></button>\n						<a class=\"btn-order js_ico_cart[[if?&is=`[*getCartCount*]:empty`&else=` active`]]\" href=\"#order\"><span class=\"order-circle js_count\">[*getCartCount*]</span><span class=\"icon icon-order\"></span></a>\n					</div>\n					<div class=\"header-menu-wrapper\">\n						<div class=\"phones-block phones-block--mobile\">\n							<div class=\"phones-block-items\">\n								<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_1)]`]]\">[(as_phone_1)]</a>\n								<a class=\"phone__item\" href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_2)]`]]\">[(as_phone_2)]</a>\n								<div class=\"schedule\">\n									<p>[#44#]</p>\n									<p>[#45#]</p>\n									<p>[#46#]</p>\n								</div>\n							</div>\n						</div>\n						<ul class=\"main-menu\">\n							[[Shop?&get=`getMenu`&dept=`1`&did=`[(shop_catalog_root)]`&tpl=`tpl_menu_catalog_mob`&tplActive=`tpl_menu_catalog_active_mob`&include=`25,26`]]\n						</ul>\n					</div>\n				</div>\n				<div class=\"header__row\"><a class=\"btn btn-orange btn-call hide-call-bottom\" href=\"#call\">[#39#]</a></div>\n			</div>\n		</div>\n	</header>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('60','tpl_menu_catalog_third_active','','2','none','13','0','<li class=\"submenu__item\"><span class=\"submenu__link\">{menutitle}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('30','tpl_menu_footer','','2','none','13','0','<li class=\"menu__item\"><a class=\"menu__link\" href=\"{link}\" title=\"{menutitle}\">{menutitle}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('31','tpl_menu_footer_active','','2','none','13','0','<li class=\"menu__item\"><span class=\"menu__link active\">{menutitle}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('34','tpl_questions','','2','none','14','0','<section style=\"background-image: url({tv_img});\" class=\"section questions\">\n	<div class=\"container\">\n		<div class=\"section-title text-white\">{pagetitle}</div>\n		<div class=\"section-subtitle\">{longtitle}</div>\n		<form class=\"feedback-form js_validate\" action=\"[~~[(site_start)]~~]\" method=\"post\"> \n			<input type=\"hidden\" name=\"\">\n			<div class=\"form-row\">\n				<div class=\"form__item\">\n					<label for=\"name\">[#32#]</label>\n					<input id=\"name\" type=\"text\" name=\"question[name]\" required>\n					<div class=\"text-error\">[#50#]</div>\n				</div>\n				<div class=\"form__item\">\n					<label for=\"phone\">[#33#]</label>\n					<input id=\"phone\" type=\"tel\" name=\"question[phone]\" inputmode=\"tel\" required>\n					<div class=\"text-error\">[#52#]</div>\n				</div>\n			</div>\n			<div class=\"form__item\">\n				<label for=\"message\">[#51#]</label>\n				<textarea id=\"message\" name=\"question[question]\" cols=\"30\" rows=\"10\"></textarea>\n			</div>\n			<div class=\"form__item\">\n				<button class=\"btn btn-orange\" type=\"submit\">[#26#]</button>\n			</div>\n		</form>\n	</div>\n</section>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('35','tpl_main_steps','','2','none','5','0','<li class=\"steps__item\">\n	<div class=\"count-wrapper\">{i}</div>\n	<div class=\"text-wrapper\">\n		<div class=\"steps__title\">{pagetitle}</div>\n		<div class=\"steps__description\">{longtitle}</div>\n	</div>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('36','tpl_services','','2','none','0','0','<section class=\"discount__item\">\n	<div class=\"discount-content\">\n		<div class=\"heading-h2\">{pagetitle}</div>\n		<div class=\"discount-text\">{description}</div>\n		<div class=\"discount-comment\">{longtitle}</div>\n		<div class=\"price__item\">[#54#] &nbsp;{link_attributes}<span class=\"currency\">&nbsp;[#53#]</span>\n		</div><a class=\"btn btn-bordered btn-big js_call-me\" href=\"#call-me\">{introtext}</a>\n	</div>\n	<div class=\"discount-img\" style=\"background-image: url(\'{tv_img}\')\">\n		<img src=\"[[R?&img=`{tv_img}`&opt=`w=650&far=1`]]\" alt=\"{pagetitle}\" title=\"{pagetitle}\">\n	</div>\n</section>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('37','tpl_about_faq','','2','none','5','0','<a href=\"\" class=\"accordion-list__item\">\n	<div class=\"title\">{pagetitle}</div>\n	<div class=\"content\">{longtitle}</div>\n</a>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('38','tpl_about_facts','','2','none','5','0','<div class=\"facts__item\">\n	<div class=\"text\">{line}</div>\n	<div class=\"counter\"></div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('39','tpl_li','','2','none','14','0','<li>{line}</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('40','tpl_news','','2','none','0','0','<a class=\"article__item\" href=\"{link}\">\n	<div class=\"title\">{title}</div>\n	<div class=\"description\">{description}</div>\n</a>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('41','tpl_discount','','2','none','2','0','<section class=\"discount__item\">\n	<div class=\"discount-content\">\n		<div class=\"heading-h2\">{title}</div>\n		<div class=\"discount-counter\">[#60#] <span>{beginning_at}</span>&nbsp;[#66#] <span>{ending_at}</span>. [#61#]<span>&nbsp;{count_days}</span></div>\n		<div class=\"discount-text\">\n			{content}\n		</div>\n		<div class=\"discount-comment\">{description}</div><a class=\"btn btn-bordered btn-big\" href=\"{link}\">[#62#]</a>\n	</div>\n	<div class=\"discount-img\" style=\"background-image: url(\'[[R?&img=`{image}`&opt=`w=512&h=690&far=1`]]\')\"><img src=\"[[R?&img=`{image}`&opt=`w=286&h=332&far=1`]]\" alt=\"{alt}\" title=\"{alt}\"></div>\n</section>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('42','tpl_main_slider','','2','none','2','0','<div class=\"slider__item height\">\n	<a class=\"product-wrapper\" href=\"{product_url}\">\n		<div class=\"product-body\">\n			<img src=\"[[R?&img=`{cover}`&opt=`w=144&h=273&far=1`]]\" alt=\"{product_alt}\" title=\"{product_name}\">\n		</div>\n		<div class=\"product-footer\">\n			<div class=\"product__name\">{product_name}</div>\n			<div class=\"product__price-wrapper\">\n				[[if?&is=`{price_old}:empty`&else=`<span class=\"product__price--old\">{price_old}&nbsp;{sing}</span>`]]\n				<span class=\"product__price\">{price} {sing}</span>\n			</div>\n		</div>\n	</a>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('43','tpl_main_manufacturer','','2','none','2','0','<li class=\"tabs__item\"><a href=\"{filter_link}\" class=\"tabs__link--rounded\">{filter_value}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('26','tpl_lang_active','','2','none','7','0','<li><span class=\"lang__link active\">{title}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('25','tpl_lang','','2','none','7','0','<li><a class=\"lang__link\" href=\"{link}\">{title}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('20','call','popup Вызов замерщика','2','none','12','0','<div class=\"call-popup\" id=\"call\" style=\"display:none\">\n	<form class=\"js_validate js_ajax\" method=\"POST\">\n		<input type=\"hidden\" name=\"ajax\" value=\"call_meter\">\n		<div class=\"call-wrapper\">\n			<div class=\"call-header\">\n				<div class=\"section-title text-black\">[#39#]</div>\n				<div class=\"call-text\">[#82#]</div>\n			</div>\n			<div class=\"call-body\">\n				<div class=\"form-row\">\n					<div class=\"form__item\">\n						<label class=\"form-label\" for=\"name2\">[#32#]</label>\n						<input class=\"form-input\" id=\"name2\" type=\"text\" inputmode=\"text\" name=\"meter[name]\" required>\n						<div class=\"text-error\">[#50#]</div>\n					</div>\n				</div>\n				<div class=\"form-row\">\n					<div class=\"form__item\">\n						<label class=\"form-label\" for=\"phone-number-call\">[#83#]</label>\n						<input class=\"form-input\" id=\"phone-number-call\" type=\"tel\" inputmode=\"tel\" name=\"meter[phone]\" required data-validate=\"phone\" maxlenght=\"13\">\n						<div class=\"text-error\">[#52#]</div>\n					</div>\n				</div>\n				<div class=\"form-row\">\n					<div class=\"form__item\">\n						<label class=\"form-label\" for=\"area-1\">[#84#]</label>\n						<select class=\"js_select-call\" id=\"area-1\" name=\"meter[area]\" data-validate=\"select2\" required>\n							[!Shop?&get=`getArias`&tpl=`tpl_aria_option`!]\n						</select>\n						<div class=\"text-error\">[#86#]</div>\n					</div>\n				</div>\n			</div>\n			<div class=\"call-footer\"> \n				<div class=\"form-row form-row--confirm\">\n					<button class=\"btn btn-orange btn-big\" type=\"submit\">[#26#]</button>\n				</div>\n			</div>\n		</div>\n	</form>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('21','call-me','popup перезвоните мне','2','none','12','0','<div class=\"call-popup\" id=\"call-me\" style=\"display:none\">\n	<form class=\"js_validate js_ajax\" method=\"POST\">\n		<input type=\"hidden\" name=\"ajax\" value=\"call_me\">\n		<div class=\"call-wrapper\">\n			<div class=\"call-header\">\n				<div class=\"section-title\">[#88#]</div>\n			</div>\n			<div class=\"call-body\">\n				<div class=\"form-row\">\n					<div class=\"form__item\">\n						<label class=\"form-label\" for=\"name2\">[#32#]</label>\n						<input class=\"form-input\" id=\"name2\" type=\"text\" inputmode=\"text\" name=\"call_me[name]\" required>\n						<div class=\"text-error\">[#50#]</div>\n					</div>\n				</div>\n				<div class=\"form-row\">\n					<div class=\"form__item\">\n						<label class=\"form-label\" for=\"phone-number-call\">[#83#]</label>\n						<input class=\"form-input\" id=\"phone-number-call\" type=\"tel\" inputmode=\"tel\" name=\"call_me[phone]\" required data-validate=\"phone\" maxlenght=\"13\">\n						<div class=\"text-error\">[#52#]</div>\n					</div>\n				</div>\n			</div>\n			<div class=\"call-footer\"> \n				<button class=\"btn btn-orange btn-big btn-wide\" type=\"submit\">[#87#]</button>\n			</div>\n		</div>\n	</form>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('59','call_me-send','popap вам перезвоним','2','none','12','0','<div class=\"request-send\" id=\"call_me-send\" style=\"display:none\">\n	<div class=\"call-wrapper\">\n		<div class=\"thank-img\"><img src=\"[[Shop?&get=`getFieldTV`&did=`[(success_send)]`&field=`tv_img`]]\" alt=\"mail\" title=\"mail\" width=\"150\"></div>\n		<div class=\"call-header\">\n			<div class=\"section-title\">[#89#]</div>\n		</div>\n		<div class=\"call-body\">\n			<div class=\"text\">[#92#]</div>\n		</div>\n		<div class=\"call-footer\"> \n			<button class=\"btn btn-bordered btn-sm\" data-fancybox-close type=\"submit\"> [#91#]</button>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('22','request-send','popap замерщик вызван','2','none','12','0','<div class=\"request-send\" id=\"request-send\" style=\"display:none\">\n	<div class=\"call-wrapper\">\n		<div class=\"thank-img\"><img src=\"[[Shop?&get=`getFieldTV`&did=`[(success_send)]`&field=`tv_img`]]\" alt=\"mail\" title=\"mail\" width=\"150\"></div>\n		<div class=\"call-header\">\n			<div class=\"section-title\">[#89#]</div>\n		</div>\n		<div class=\"call-body\">\n			<div class=\"text\">[#90#]</div>\n		</div>\n		<div class=\"call-footer\"> \n			<button class=\"btn btn-bordered btn-sm\" data-fancybox-close type=\"submit\"> [#91#]</button>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('58','tpl_aria_option','','2','none','10','0','<option value=\"{aria_name}\">{aria_name}</option>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('23','tpl_menu_top','','2','none','13','0','<li class=\"nav__item\"><a class=\"nav__link\" href=\"{link}\" title=\"{menutitle}\">{menutitle}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('24','tpl_menu_top_active','','2','none','13','0','<li class=\"nav__item\"><span class=\"nav__link\">{menutitle}</span></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('27','tpl_menu_catalog','','2','none','13','0','<li class=\"menu__item\"><a class=\"menu__link\" href=\"{link}\">{menutitle}</a>\n	[[if?&is=`{isfolder}:=:1:and:{parent}:=:[(shop_catalog_root)]`&then=`\n	<div class=\"submenu-wrapper\">\n		<div class=\"container\">\n			<div class=\"submenu\">\n				[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_second`&tplActive=`tpl_menu_catalog_second_active`]]\n			</div>\n		</div>\n	</div>`]]\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('28','tpl_menu_catalog_active','','2','none','13','0','<li class=\"menu__item\"><span class=\"menu__link active\">{menutitle}</span>\n	[[if?&is=`{isfolder}:=:1:and:{parent}:=:[(shop_catalog_root)]`&then=`\n	<div class=\"submenu-wrapper\">\n		<div class=\"container\">\n			<div class=\"submenu\">\n				[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_second`&tplActive=`tpl_menu_catalog_second_active`]]\n			</div>\n		</div>\n	</div>`]]\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('62','tpl_productImages_420','','2','none','15','0','<li class=\"product-gallery__item\">\n	<div class=\"product-img\"><img src=\"[[R?&img=`{image}`&opt=`w=420&far=1`]]\" alt=\"{alt}\" title=\"{alt}\"></div>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('63','full-gallery','','2','none','12','0','<div class=\"slider-wrapper full-gallery\" id=\"full-gallery\" style=\"display:none\">\n	<div class=\"product-gallery-wrapper\">\n		<div class=\"slider-nav\">\n			<div class=\"slider-nav__item slider-nav__item--next\"><span class=\"icon icon-arrow\"></span></div>\n			<div class=\"slider-nav__item slider-nav__item--prev\"><span class=\"icon icon-arrow\"></span></div>\n		</div>\n		<ul class=\"product-gallery-fs\">\n			[[Shop?&get=`getProductImages`&tpl=`tpl_productImages_full`]]\n		</ul>\n	</div>\n	<div class=\"product-gallery-wrapper\">\n		<ul class=\"product-gallery-fs-nav\">\n			[[Shop?&get=`getProductImages`&tpl=`tpl_productImages_full`]]\n		</ul>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('64','tpl_product_characteristic','','2','none','15','0','<div class=\"features__item\">\n	<div class=\"title\">{filter_name}</div>\n	<div class=\"feature\">{filter_value}</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('65','tpl_product_option_item','','2','none','15','0','<option value=\"{filter_value_url}\" data-value_url=\"{filter_value_url}\" data-value_id=\"{filter_value_id}\"[[if?&is=`{checked}:=:1`&then=` selected`]]>{filter_value}</option>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('66','tpl_product_option_outer','','2','none','15','0','<div class=\"form-row\">\n	<div class=\"form__item\">\n		<label class=\"form-label\" for=\"{filter_url}\">{filter_name}</label>\n		<select class=\"js_select\" id=\"{filter_url}\" name=\"product[{filter_url}]\">\n			{values}\n		</select>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('44','tpl_menu_catalog_second','','2','none','13','0','<div class=\"submenu-block\">\n	<div class=\"submenu-row\">\n		<div class=\"submenu-block__title\">\n			[[if?&is=`{isfolder}:=:1`&then=`{menutitle}\n		</div>\n	</div>	\n	<div class=\"submenu-row\">	\n		[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_third`&tplWrap=`tpl_menu_catalog_third_wrap`&tplActive=`tpl_menu_catalog_third_active`]]\n	</div>\n</div>\n`&else=`<a class=\"menu__link\" href=\"{link}\">{menutitle}</a>\n</div>\n</div>\n</div>`]]','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('48','tpl_appointment','','2','none','2','0','<li class=\"appointment-list__item\">\n	<a class=\"appointment__item-wrapper\" href=\"{link}\">\n		<div class=\"appointment__item\">\n			<div class=\"appointment-icon\">\n				<img src=\"{tv_img}\" alt=\"{pagetitle}\" title=\"{pagetitle}\">\n			</div>\n			<div class=\"appointment-title\">{pagetitle}</div>\n		</div>\n	</a>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('45','tpl_menu_catalog_third','','2','none','13','0','<li class=\"submenu__item\"><a href=\"{link}\" class=\"submenu__link\">{menutitle}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('49','tpl_materials','','2','none','2','0','<li class=\"materials-list__item\">\n	<a class=\"materials__item-wrapper\" href=\"{link}\">\n		<div class=\"materials__item\">\n			<div class=\"materials-img\">\n				<img src=\"[[R?&img=`{tv_img}`&opt=`w=358&h=358&far=1`]]\" alt=\"{pagetitle}\" title=\"{pagetitle}\">\n			</div>\n			<div class=\"materials-title\">{pagetitle}</div>\n		</div>\n	</a>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('47','tpl_menu_catalog_third_wrap','','2','none','13','0','<ul class=\"submenu-col\">\n	{inner_wrap}\n</ul>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('46','tpl_menu_catalog_second_active','','2','none','13','0','<div class=\"submenu-block\">\n	<div class=\"submenu-row\">\n		<div class=\"submenu-block__title\">\n			[[if?&is=`{isfolder}:=:1`&then=`{menutitle}\n		</div>\n	</div>\n	[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_third`&tplActive=`tpl_menu_catalog_third_active`]]\n</div>\n`&else=`<span class=\"menu__link active\">{menutitle}</span>\n		</div>\n	</div>\n</div>`]]','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('50','tpl_door_types','','2','none','2','0','<li class=\"door-types-list__item\">\n	<a class=\"door-types__item-wrapper\" href=\"{link}\">\n		<div class=\"door-types__item\">\n			<div class=\"door-types-img\">\n				<img src=\"{tv_img}\" alt=\"{pagetitle}\" title=\"{pagetitle}\">\n			</div>\n			<div class=\"door-types-title\">{pagetitle}</div>\n		</div>\n	</a>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('51','tpl_seo_block','','2','none','4','0','[[if?&is=`[*seo_content*]:empty`&else=`\n<div class=\"container\">\n	<section class=\"content\">\n		<h1 style=\"color: #377483\">[*seo_title*]</h1>\n		<div class=\"content--columns content--hide\">\n			[*seo_content*]\n		</div>\n		<div class=\"content-toggle js_text-toggle\"><span>[#71#]</span></div>\n	</section>\n</div>`]]','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('53','tpl_catalog','','2','none','2','0','<li class=\"product-list__item\">\n	<a class=\"product-wrapper\" href=\"{product_url}\">\n		<div class=\"product-body\">\n			<img src=\"[[R?&img=`{cover}`&opt=`h=305&far=1`]]\" alt=\"{product_alt}\" title=\"{product_name}\">\n		</div>\n		<div class=\"product-footer\">\n			<div class=\"product__name\">{product_name}</div>\n			<div class=\"product__price-wrapper\">\n				[[if?&is=`{price_old}:empty`&else=`<span class=\"product__price--old\">{price_old}&nbsp;{sing}</span>`]]\n				<span class=\"product__price\">{price}&nbsp;<span class=\"currency\">{sing}</span></span>\n			</div>\n		</div>\n		[[if?&is=`{product_status}:=:2`&then=`<div class=\"product__label\">[#73#]</div>`]]\n		[[if?&is=`{product_status}:=:3`&then=`<div class=\"product__label product__label--discount\">[#72#]</div>`]]\n		[[if?&is=`{product_status}:=:5`&then=`<div class=\"product__label product__label--hit\">[#74#]</div>`]]\n	</a>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('56','tpl_getOption','','2','none','2','0','<option value=\"{value}\" data-{catch}=\"{value}\" {selected}>{title}</option>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('57','tpl_getOption_single','','2','none','2','0','<span class=\"filters-lbl filters-lbl--orange {selected}\" data-{catch}=\"{value}\">{title}</span>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('54','tpl_filtersInner_del','','2','none','2','0','<a class=\"filters-lbl js_remove-lbl\" data-filter=\"{filter_url}={value_url}\"><span class=\"icon icon-close\"></span>{filter_value}</a>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('55','tpl_filters_del','','2','none','2','0','{filter_inner}','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('74','tpl_sitemap_item','','2','none','0','0','<li><a href=\"{link}\">{title}</a></li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('75','empty_cart','popap вам перезвоним','2','none','12','0','<div class=\"request-send\" id=\"empty_cart\" style=\"display:none\">\n	<div class=\"call-wrapper\">\n		<div class=\"call-header\">\n			<div class=\"section-title\">[#126#]</div>\n		</div>\n		<!-- <div class=\"call-body\">\n			<div class=\"text\">[#92#]</div>\n		</div> -->\n		<div class=\"call-footer\"> \n			<button class=\"btn btn-bordered btn-sm\" data-fancybox-close type=\"submit\"> [#91#]</button>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('73','mail_order_item_characteristic','','2','none','9','0','<tr>\n	<td style=\"color: #656565; font-size: 12px; line-height: 14px;\">\n		<p style=\"color: #656565; font-size: 12px; line-height: 14px;\">{filter_name}: <span>{filter_value}</span></p>\n	</td>\n</tr>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('76','added_to_cart','popap товар в корзине','2','none','12','0','<div class=\"request-send\" id=\"added_to_cart\" style=\"display:none\">\n	<div class=\"call-wrapper\">\n		<div class=\"call-header\">\n			<div class=\"section-title\">[#130#]</div>\n		</div>\n		<!-- <div class=\"call-body\">\n<div class=\"text\">[#92#]</div>\n</div> -->\n		<div class=\"call-footer\"> \n			<button class=\"btn btn-bordered btn-sm\" data-fancybox-close type=\"submit\"> [#91#]</button>\n			<button class=\"btn btn-bordered btn-sm btn-order active\" href=\"#order\"> [#132#]</button>\n		</div>\n	</div>\n</div>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('78','tpl_menu_catalog_active_mob','','2','none','13','0','<li class=\"menu__item mob\">\n	[[if?&is=`{isfolder}:=:1:and:{parent}:=:[(shop_catalog_root)]`&then=`\n	<span class=\"menu__link active\">{menutitle}</span>\n	<div class=\"submenu-arrow js_sub-open\">\n		<span class=\"icon icon-arrow-down\"></span>\n	</div>\n	<div class=\"submenu-wrapper\">\n		<div class=\"container\">\n			<div class=\"submenu\">\n				[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_second`&tplActive=`tpl_menu_catalog_second_active`]]\n			</div>\n		</div>\n	</div>`&else=`\n	<span class=\"menu__link active\">{menutitle}</span>\n	`]]\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('79','tpl_menu_catalog_mob','','2','none','13','0','<li class=\"menu__item mob\">\n	[[if?&is=`{isfolder}:=:1:and:{parent}:=:[(shop_catalog_root)]`&then=`\n	<a class=\"menu__link\" href=\"{link}\">{menutitle}</a>\n	<div class=\"submenu-arrow js_sub-open\">\n		<span class=\"icon icon-arrow-down\"></span>\n	</div>\n	<div class=\"submenu-wrapper\">\n		<div class=\"container\">\n			<div class=\"submenu\">\n				[[Shop?&get=`getMenu`&did=`{id}`&tpl=`tpl_menu_catalog_second`&tplActive=`tpl_menu_catalog_second_active`]]\n			</div>\n		</div>\n	</div>` \n	&else=`\n	<a class=\"menu__link\" href=\"{link}\">{menutitle}</a>\n	`]]\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('80','tpl_productImages_90','','2','none','15','0','<li class=\"product-gallery__item\">\n	<div class=\"product-img\"><img src=\"[[R?&img=`{image}`&opt=`w=90&far=1`]]\" alt=\"{alt}\" title=\"{alt}\"></div>\n</li>','0');

INSERT INTO `modx_site_htmlsnippets` VALUES ('81','tpl_productImages_full','','2','none','15','0','<li class=\"product-gallery__item\">\n	<div class=\"product-img\"><img src=\"{image}\" alt=\"{alt}\" title=\"{alt}\"></div>\n</li>','0');


# --------------------------------------------------------

#
# Table structure for table `modx_site_keywords`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_keywords`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Site keyword list';

#
# Dumping data for table `modx_site_keywords`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_metatags`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_metatags`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_metatags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `tag` varchar(50) NOT NULL DEFAULT '' COMMENT 'tag name',
  `tagvalue` varchar(255) NOT NULL DEFAULT '',
  `http_equiv` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 - use http_equiv tag style, 0 - use name',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Site meta tags';

#
# Dumping data for table `modx_site_metatags`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_module_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_module_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_module_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `usergroup` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Module users group access permission';

#
# Dumping data for table `modx_site_module_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_module_depobj`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_module_depobj`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_module_depobj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0',
  `type` int(2) NOT NULL DEFAULT '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Module Dependencies';

#
# Dumping data for table `modx_site_module_depobj`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_modules`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_modules`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '0',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) NOT NULL DEFAULT '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL DEFAULT '0',
  `editedon` int(11) NOT NULL DEFAULT '0',
  `guid` varchar(32) NOT NULL DEFAULT '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text,
  `modulecode` mediumtext COMMENT 'module boot up code',
  `moduleshow` tinyint(4) DEFAULT '1' COMMENT '1==show|0==hide',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='Site Modules';

#
# Dumping data for table `modx_site_modules`
#

INSERT INTO `modx_site_modules` VALUES ('1','Doc Manager','<strong>1.1</strong> Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions','0','0','0','0','0','','0','','0','0','docman435243542tf542t5t','1','',' \n/**\n * Doc Manager\n * \n * Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions\n * \n * @category	module\n * @version 	1.1\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@guid docman435243542tf542t5t	\n * @internal	@shareparams 1\n * @internal	@moduleshow 0\n * @internal	@dependencies requires files located at /assets/modules/docmanager/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  09/04/2016\n */\n\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/docmanager.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_frontend.class.php\');\ninclude_once(MODX_BASE_PATH.\'assets/modules/docmanager/classes/dm_backend.class.php\');\n\n$dm = new DocManager($modx);\n$dmf = new DocManagerFrontend($dm, $modx);\n$dmb = new DocManagerBackend($dm, $modx);\n\n$dm->ph = $dm->getLang();\n$dm->ph[\'theme\'] = $dm->getTheme();\n$dm->ph[\'ajax.endpoint\'] = MODX_SITE_URL.\'assets/modules/docmanager/tv.ajax.php\';\n$dm->ph[\'datepicker.offset\'] = $modx->config[\'datepicker_offset\'];\n$dm->ph[\'datetime.format\'] = $modx->config[\'datetime_format\'];\n\nif (isset($_POST[\'tabAction\'])) {\n    $dmb->handlePostback();\n} else {\n    $dmf->getViews();\n    echo $dm->parseTemplate(\'main.tpl\', $dm->ph);\n}','0');

INSERT INTO `modx_site_modules` VALUES ('2','Pictures','<strong>1.0</strong> The module allows you to attach a collection of images to a resource','0','0','0','0','0','','0','','0','0','','1','',' \n/**\n * Pictures\n * \n * The module allows you to attach a collection of images to a resource\n * \n * @category	module\n * @version 	1.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@guid	\n * @internal	@shareparams 1\n * @internal	@moduleshow 0\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n * @lastupdate  11/11/2016\n */\n\n//AUTHORS: Adminko & Seiger \nrequire MODX_BASE_PATH . \"assets/modules/pictures/index.php\";','0');

INSERT INTO `modx_site_modules` VALUES ('3','Language','<strong>2.0</strong> The module allows you to configure transfer elements templates','0','0','11','0','0','','0','','0','0','','1','',' \n/**\n * Language\n * \n * The module allows you to configure transfer elements templates\n * \n * @category	module\n * @version 	2.0\n * @internal	@properties\n * @internal	@guid	\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/language/\n * @internal	@modx_category Manager and Admin\n * @internal	@installset base, sample\n * @author	Seiger\n * @lastupdate	08/10/2018\n */\n\nrequire MODX_BASE_PATH . \"assets/modules/language/index.php\";','1');

INSERT INTO `modx_site_modules` VALUES ('4','Advanced settings','<strong>1.0</strong> The module makes it easy to manage the advanced system settings Site','0','0','11','0','0','','0','','0','0','','1','','\n/**\n * Advanced settings\n *\n * The module makes it easy to manage the advanced system settings Site\n *\n * @category	module\n * @version 	1.0\n * @internal	@properties\n * @internal	@guid\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/settings/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  11/11/2016\n */\n\n//AUTHORS: Seiger\nrequire MODX_BASE_PATH . \"assets/modules/settings/index.php\";\n','1');

INSERT INTO `modx_site_modules` VALUES ('5','Mailer','<strong>2.0</strong> Mailing lists management module','0','0','11','0','0','','0','','0','0','','1','','\n/**\n * Mailer\n *\n * Mailing lists management module\n *\n * @category	module\n * @version 	2.0\n * @internal	@properties\n * @internal	@guid\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/mailer/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  11/11/2016\n */\n\n//AUTHORS: Adminko & Seiger\n/*cron файл лежит рядом в папке модуля*/\nrequire MODX_BASE_PATH.\"assets/modules/mailer/index.php\";\n','1');

INSERT INTO `modx_site_modules` VALUES ('6','SEO','<strong>1.0</strong> The module allows you to configure redirect, as well as create a sitemap file and Robots manage the counters analytics','0','0','11','0','0','','0','','0','0','','1','','\n/**\n * SEO\n *\n * The module allows you to configure redirect, as well as create a sitemap file and Robots manage the counters analytics\n *\n * @category	module\n * @version 	1.0\n * @internal	@properties\n * @internal	@guid\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/seo/\n * @internal	@modx_category Manager and Admin\n * @internal    @installset base, sample\n * @lastupdate  11/11/2016\n */\n\n//AUTHORS: Adminko & Seiger\nrequire MODX_BASE_PATH . \"assets/modules/seo/index.php\";\n','1');

INSERT INTO `modx_site_modules` VALUES ('7','Блог','<b>1.0</b> The module allows you to management news articles','0','0','6','0','0','','0','','0','0','0c99b8ba9b752bf3307a5b3ea54e740e','1','{}','// <?php \n/**\n * News\n * \n * The module allows you to management news articles\n * \n * @category	module\n * @version 	1.0\n * @internal	@properties\n * @internal	@guid	\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/news/\n * @internal	@modx_category Manager and Admin\n * @internal	@installset base, sample\n * @author	Seiger\n * @lastupdate	11/06/2018\n */\n\nrequire MODX_BASE_PATH . \"assets/modules/news/index.php\";','1');

INSERT INTO `modx_site_modules` VALUES ('8','Акции','<b>1.0</b> The module allows you to management discounts articles','0','0','6','0','0','','0','','0','0','ea596499a6ccc280e1f91ab4bc123bad','1','{}','//<?php \n/**\n * Discounts\n * \n * The module allows you to management discounts articles\n * \n * @category	module\n * @version 	1.0\n * @internal	@properties\n * @internal	@guid	\n * @internal	@shareparams 1\n * @internal	@moduleshow 1\n * @internal	@dependencies requires files located at /assets/modules/discounts/\n * @internal	@modx_category Manager and Admin\n * @internal	@installset base, sample\n * @author	Seiger\n * @lastupdate	11/06/2018\n */\n\nrequire MODX_BASE_PATH . \"assets/modules/discounts/index.php\";','1');


# --------------------------------------------------------

#
# Table structure for table `modx_site_plugin_events`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_plugin_events`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL,
  `evtid` int(10) NOT NULL DEFAULT '0',
  `priority` int(10) NOT NULL DEFAULT '0' COMMENT 'determines plugin run order',
  PRIMARY KEY (`pluginid`,`evtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Links to system events';

#
# Dumping data for table `modx_site_plugin_events`
#

INSERT INTO `modx_site_plugin_events` VALUES ('1','34','0');

INSERT INTO `modx_site_plugin_events` VALUES ('1','35','0');

INSERT INTO `modx_site_plugin_events` VALUES ('1','36','0');

INSERT INTO `modx_site_plugin_events` VALUES ('1','40','0');

INSERT INTO `modx_site_plugin_events` VALUES ('1','41','0');

INSERT INTO `modx_site_plugin_events` VALUES ('1','42','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','3','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','19','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','107','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','20','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','21','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','66','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','77','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','90','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','91','0');

INSERT INTO `modx_site_plugin_events` VALUES ('2','1000','0');

INSERT INTO `modx_site_plugin_events` VALUES ('3','19','0');

INSERT INTO `modx_site_plugin_events` VALUES ('3','90','0');

INSERT INTO `modx_site_plugin_events` VALUES ('3','91','0');

INSERT INTO `modx_site_plugin_events` VALUES ('3','92','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','23','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','29','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','35','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','41','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','47','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','73','0');

INSERT INTO `modx_site_plugin_events` VALUES ('4','88','0');

INSERT INTO `modx_site_plugin_events` VALUES ('5','81','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','205','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','28','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','29','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','35','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','53','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','30','0');

INSERT INTO `modx_site_plugin_events` VALUES ('6','31','0');

INSERT INTO `modx_site_plugin_events` VALUES ('7','100','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','25','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','27','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','37','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','39','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','43','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','45','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','49','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','51','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','55','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','57','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','75','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','77','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','206','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','210','0');

INSERT INTO `modx_site_plugin_events` VALUES ('8','211','0');

INSERT INTO `modx_site_plugin_events` VALUES ('9','19','0');

INSERT INTO `modx_site_plugin_events` VALUES ('9','31','0');

INSERT INTO `modx_site_plugin_events` VALUES ('9','90','0');

INSERT INTO `modx_site_plugin_events` VALUES ('9','91','0');

INSERT INTO `modx_site_plugin_events` VALUES ('9','1000','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','3','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','20','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','85','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','87','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','88','0');

INSERT INTO `modx_site_plugin_events` VALUES ('10','92','0');


# --------------------------------------------------------

#
# Table structure for table `modx_site_plugins`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_plugins`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_plugins` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Plugin',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `plugincode` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='Contains the site plugins.';

#
# Dumping data for table `modx_site_plugins`
#

INSERT INTO `modx_site_plugins` VALUES ('1','FileSource','<strong>0.1</strong> Save snippet and plugins to file','0','6','0','require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';','0','','0','');

INSERT INTO `modx_site_plugins` VALUES ('2','Shop plugin','<strong>2.0</strong> The plugin is base for this site.','0','6','0','\n/**\n * Shop plugin\n *\n * The plugin is base for this site.\n *\n * @category 	plugin\n * @version 	2.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties\n * @internal	@events OnLoadWebDocument,OnWebPageInit,OnWebPagePrerender,OnCacheUpdate,OnMakePageCacheKey,OnLoadWebPageCache,OnPageNotFound,OnBeforeWUsrFormSave,OnBeforeSaveWebPageCache,OnModFormSave\n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names Base\n * @internal    @installset base, sample\n * @reportissues https://github.com/modxcms/evolution\n * @documentation\n * @link\n * @author      Seiger\n * @author      Since 2017: Seiger\n * @lastupdate  05/10/2017\n */\n\nrequire MODX_BASE_PATH . \"assets/shop/plugin.index.php\";','0','','0','');

INSERT INTO `modx_site_plugins` VALUES ('3','Language Manager','<strong>2.0</strong> The plugin allows you to manage a multilanguage website.','0','6','0','\n/**\n * Language Manager\n * \n * The plugin allows you to manage a multilanguage website.\n *\n * @category 	plugin\n * @version 	2.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties \n * @internal	@events OnLoadWebDocument,OnParseDocument,OnWebPageInit,OnCacheUpdate \n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names Language\n * @internal    @installset base, sample\n * @reportissues https://github.com/modxcms/evolution\n * @documentation \n * @link        \n * @author      Seiger\n * @author      Since 2017: Seiger, adminko\n * @lastupdate  06/01/2017\n */\n\nrequire MODX_BASE_PATH . \"assets/modules/language/plugin.php\";','0','','0','');

INSERT INTO `modx_site_plugins` VALUES ('4','CodeMirror','<strong>1.4</strong> JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.12','0','6','0','\n/**\n * CodeMirror\n *\n * JavaScript library that can be used to create a relatively pleasant editor interface based on CodeMirror 5.12\n *\n * @category    plugin\n * @version     1.4\n * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @package     modx\n * @internal    @events OnDocFormRender,OnChunkFormRender,OnModFormRender,OnPluginFormRender,OnSnipFormRender,OnTempFormRender,OnRichTextEditorInit\n * @internal    @modx_category Manager and Admin\n * @internal    @properties &theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250\n * @internal    @installset base\n * @reportissues https://github.com/modxcms/evolution\n * @documentation Official docs https://codemirror.net/doc/manual.html\n * @author      hansek from http://www.modxcms.cz\n * @author      update Mihanik71\n * @author      update Deesen\n * @lastupdate  11/04/2016\n */\n\n$_CM_BASE = \'assets/plugins/codemirror/\';\n\n$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;\n\nrequire(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');','0','&theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light;default &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;true &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;false &indentWithTabs=indentWithTabs;list;true,false;true &undoDepth=undoDepth;int;200 &historyEventDelay=historyEventDelay;int;1250','0','');

INSERT INTO `modx_site_plugins` VALUES ('5','Manager SMS Auth','<strong>1.0</strong> The plugin is base for this site.','0','6','0','\n/**\n * Manager SMS Auth\n *\n * The plugin is base for this site.\n *\n * @category 	plugin\n * @version 	1.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties\n * @internal	@events OnManagerAuthentication\n * @internal	@modx_category Manager and Admin\n * @internal    @legacy_names ManagerSMSAuth\n * @internal    @installset base, sample\n * @reportissues https://github.com/modxcms/evolution\n * @documentation\n * @link\n * @author      Evgeniy Gololobov\n * @author      Since 2017: Evgeniy Gololobov\n * @lastupdate  26/11/2017\n */\n\nrequire MODX_BASE_PATH . \"assets/plugins/managersmsauth/plugin.managersmsauth.php\";','0','','0','');

INSERT INTO `modx_site_plugins` VALUES ('6','ManagerManager','<strong>0.6.2</strong> Customize the MODX Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.','0','6','0','/**\n * ManagerManager\n *\n * Customize the MODX Manager to offer bespoke admin functions for end users or manipulate the display of document fields in the manager.\n *\n * @category plugin\n * @version 0.6.2\n * @license http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)\n * @internal @properties &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &config_chunk=Configuration Chunk;text;mm_rules\n * @internal @events OnDocFormRender,OnDocFormPrerender,OnBeforeDocFormSave,OnDocFormSave,OnDocDuplicate,OnPluginFormRender,OnTVFormRender\n * @internal @modx_category Manager and Admin\n * @internal @installset base\n * @internal @legacy_names Image TV Preview, Show Image TVs\n * @reportissues https://github.com/DivanDesign/MODXEvo.plugin.ManagerManager/\n * @documentation README [+site_url+]assets/plugins/managermanager/readme.html\n * @documentation Official docs http://code.divandesign.biz/modx/managermanager\n * @link        Latest version http://code.divandesign.biz/modx/managermanager\n * @link        Additional tools http://code.divandesign.biz/modx\n * @link        Full changelog http://code.divandesign.biz/modx/managermanager/changelog\n * @author      Inspired by: HideEditor plugin by Timon Reinhard and Gildas; HideManagerFields by Brett @ The Man Can!\n * @author      DivanDesign studio http://www.DivanDesign.biz\n * @author      Nick Crossland http://www.rckt.co.uk\n * @author      Many others\n * @lastupdate  06/03/2016\n */\n\n// Run the main code\ninclude($modx->config[\'base_path\'].\'assets/plugins/managermanager/mm.inc.php\');','0','{\n  \"remove_deprecated_tv_types_pref\": [\n    {\n      \"label\": \"Remove deprecated TV types\",\n      \"type\": \"list\",\n      \"value\": \"yes\",\n      \"options\": \"yes,no\",\n      \"default\": \"yes\",\n      \"desc\": \"\"\n    }\n  ],\n  \"config_chunk\": [\n    {\n      \"label\": \"Configuration Chunk\",\n      \"type\": \"text\",\n      \"value\": \"mm_rules\",\n      \"default\": \"mm_rules\",\n      \"desc\": \"\"\n    }\n  ]\n}','0',' ');

INSERT INTO `modx_site_plugins` VALUES ('7','TransAlias','<strong>1.0.4</strong> Human readible URL translation supporting multiple languages and overrides','0','6','0','require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';','0','&table_name=Trans table;list;common,dutch,german,czech,utf8,utf8lowercase,russian;russian &char_restrict=Restrict alias to;list;lowercase alphanumeric,alphanumeric,legal characters;legal characters &remove_periods=Remove Periods;list;Yes,No;No &word_separator=Word Separator;list;dash,underscore,none;dash &override_tv=Override TV name;string;','0','');

INSERT INTO `modx_site_plugins` VALUES ('8','ElementsInTree','<strong>1.3.2</strong> Get access to all Elements and Modules inside Manager sidebar','0','6','0','\n/**\n * ElementsInTree\n *\n * Get access to all Elements and Modules inside Manager sidebar\n *\n * @category    plugin\n * @version     1.3.2\n * @license     http://creativecommons.org/licenses/GPL/2.0/ GNU Public License (GPL v2)\n * @internal    @properties &tabTreeTitle=Tree Tab Title;text;Site Tree;;Custom title of Site Tree tab. &useIcons=Use icons in tabs;list;yes,no;yes;;Icons available in MODX version 1.2 or newer. &treeButtonsInTab=Tree Buttons in tab;list;yes,no;yes;;Move Tree Buttons into Site Tree tab. &unifyFrames=Unify Frames;list;yes,no;yes;;Unify Tree and Main frame style. Right now supports MODxRE2 theme only.\n * @internal    @events OnManagerTreePrerender,OnManagerTreeRender,OnManagerMainFrameHeaderHTMLBlock,OnTempFormSave,OnTVFormSave,OnChunkFormSave,OnSnipFormSave,OnPluginFormSave,OnModFormSave,OnTempFormDelete,OnTVFormDelete,OnChunkFormDelete,OnSnipFormDelete,OnPluginFormDelete,OnModFormDelete\n * @internal    @modx_category Manager and Admin\n * @internal    @installset base\n * @documentation Requirements: This plugin requires MODX Evolution 1.2 or later\n * @reportissues https://github.com/pmfx/ElementsInTree\n * @link        Original Github thread https://github.com/modxcms/evolution/issues/783\n * @author      Dmi3yy https://github.com/dmi3yy\n * @author      pmfx https://github.com/pmfx\n * @author      Nicola1971 https://github.com/Nicola1971\n * @author      Deesen https://github.com/Deesen\n * @lastupdate  31/10/2016\n */\n\nglobal $_lang;\n\n$e = &$modx->Event;\n\nif(!isset($_SESSION[\'elementsInTree\'])) $_SESSION[\'elementsInTree\'] = array();\n\n// Set reloadTree = true for this events\nif( in_array($e->name, array(\n		\'OnTempFormSave\',\n		\'OnTVFormSave\',\n		\'OnChunkFormSave\',\n		\'OnSnipFormSave\',\n		\'OnPluginFormSave\',\n		\'OnModFormSave\',\n\n		\'OnTempFormDelete\',\n		\'OnTVFormDelete\',\n		\'OnChunkFormDelete\',\n		\'OnSnipFormDelete\',\n		\'OnPluginFormDelete\',\n		\'OnModFormDelete\',\n\n	)) || $_GET[\"r\"] == 2) {\n	$_SESSION[\'elementsInTree\'][\'reloadTree\'] = true;\n}\n\n// Trigger reloading tree for relevant actions when reloadTree = true\nif ( $e->name == \"OnManagerMainFrameHeaderHTMLBlock\" ) {\n	$relevantActions = array(16,19,23,300,301,77,78,22,101,102,108,76,106,107);\n	if(in_array($_GET[\'a\'],$relevantActions) && $_SESSION[\'elementsInTree\'][\'reloadTree\'] == true) {\n		$_SESSION[\'elementsInTree\'][\'reloadTree\'] = false;\n		$html  = \"<!-- elementsInTree Start -->\\n\";\n		$html .= \"<script>\";\n		$html .= \"jQuery(document).ready(function() {\";\n		$html .= \"top.tree.location.reload();\";\n		$html .= \"})\\n\";\n		$html .= \"</script>\\n\";\n		$html .= \"<!-- elementsInTree End -->\\n\";\n		$e->output($html);\n	};\n}\n\n// Main elementsInTree-part\nif ($e->name == \'OnManagerTreePrerender\') {\n	\n	// use icons\n	if ($useIcons == \'yes\') {\n		$tabPadding = \'10px\';\n	}\n	else {\n		$tabPadding = \'9px\';\n	}\n	\n	// unify frames\n	if ($unifyFrames == \'yes\') {\n		$unifyFrames_css = \'\n			body,\n			div.treeframebody {\n				background-color: #f2f2f2 !important;\n			}\n\n			div.treeframebody {\n				background-color: transparent !important;\n				-webkit-box-shadow: none !important;\n				box-shadow: none !important;\n			}\n\n			#treeMenu {\n				background-color: transparent !important;\n				border-bottom-color: transparent !important;\n			}\n	  \';\n	}\n	\n	// tree buttons in tab\n	if ($treeButtonsInTab == \'yes\') {\n       \n        $treeButtonsInTab_js  = \'\n          jQuery(\"#treeMenu\").detach().prependTo(\"#tabDoc\");\n          jQuery(\"#treeMenu\").addClass(\"is-intab\");\n          parent.tree.resizeTree();\n        \';\n        \n        $treeButtonsInTab_css = \'\n      #treeHolder {\n        padding-top: 10px;\n        padding-left: 10px;\n      }\n      \n      #treeMenu {\n        display: none;\n        margin-left: 0;\n        margin-bottom: 6px;\n        background-color: transparent !important;\n        border-bottom-width: 0;\n      }\n\n      #treeMenu.is-intab {\n        display: table;\n      }\n\n      .treeButton,\n      .treeButtonDisabled {\n        padding: 2px 3px;\n      }\n\n      #tabDoc {\n        padding-top: 11px !important;\n        padding-left: 13px !important;\n        padding-right: 13px !important;\n      }\n      \n      #floater {\n        width: 99%;\n        top: 94px;\n      }\n	  \';\n	}\n	\n	// start main output\n	$output = \'\n		<style>\n		#tabDoc {\n			overflow: hidden;\n		}\n		\n		#tabDoc::before {\n			position: absolute;\n			content: \"\";\n			right: 0;\n			top: 0;\n			bottom: 0;\n			width: 30px;\n			background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 90%, rgba(255,255,255,1) 100%);\n			background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 90%,rgba(255,255,255,1) 100%);\n			background: linear-gradient(to right, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 90%,rgba(255,255,255,1) 100%);\n		}\n\n		#treePane .tab-page ul {\n			margin: 0;\n			margin-bottom: 5px;\n			padding: 0;\n		}\n\n		#treePane .tab-page ul li {\n			list-style: none;\n			padding-left: 10px;\n		}\n\n		#treePane .tab-page ul li li {\n			list-style: none;\n			padding-left: 5px;\n			line-height: 1.6;\n		}\n\n		#treePane .tab-page ul li a {\n			text-decoration: none;\n		}\n\n		#treePane .tab-page ul li a:hover {\n			text-decoration: underline;\n		}\n\n		#treePane .tab {\n			padding-left: 7px;\n			padding-right: 7px;\n		}\n\n		#treePane .tab > span > .fa {\n			margin-right: 2px;\n			margin-left: 2px;\n		}\n\n		#treePane .tab.selected {\n			padding-bottom: 6px;\n		}\n\n		#treePane .tab-row .tab span {\n			font-size: 14px;\n		}\n\n		#treePane .ext-ico {\n			text-decoration:none!important;\n			color:#97D19C!important;\n		}\n\n		#treePane ul > li > strong > a.catname\n		{\n			color: #444;\n		}\n\n		#treePane .fade {\n			opacity: 0;\n			-webkit-transition: opacity .15s linear;\n			-o-transition: opacity .15s linear;\n			transition: opacity .15s linear;\n		}\n\n		#treePane .fade.in {\n			opacity: 1;\n		}\n\n		#treePane .collapse {\n			display: none;\n		}\n\n		#treePane .collapse.in {\n			display: block;\n		}\n\n		#treePane tr.collapse.in {\n			display: table-row;\n		}\n\n		#treePane tbody.collapse.in {\n			display: table-row-group;\n		}\n\n		#treePane .collapsing {\n			position: relative;\n			height: 0;\n			overflow: hidden;\n			-webkit-transition-timing-function: ease;\n					 -o-transition-timing-function: ease;\n							transition-timing-function: ease;\n			-webkit-transition-duration: .35s;\n					 -o-transition-duration: .35s;\n							transition-duration: .35s;\n			-webkit-transition-property: height;\n			-o-transition-property: height;\n			transition-property: height;\n		}\n\n		#treePane.no-transition .collapsing {\n			-webkit-transition: none;\n			-o-transition: none;\n    		transition: none;\n		}\n\n		#treePane .panel-title a{\n			display: block;\n			padding: 4px 0 4px 15px;\n			color: #657587;\n			font-weight: bold;\n		}\n\n		#treePane .panel-title > a::before {\n			content: \"\\f107\"; /* fa-angle-down */\n			font-family: \"FontAwesome\";\n			position: absolute;\n			left: 15px;\n		}\n		#treePane .panel-title > a.collapsed::before {\n			content: \"\\f105\"; /* fa-angle-right */\n		}\n		#treePane .panel-title > a[aria-expanded=\"true\"] {\n			color: #657587;\n		}\n\n		#treePane li.eltree {\n			margin-left: 5px;\n			line-height: 1.4em;\n		}\n\n		#treePane li.eltree:before {\n			font-family: FontAwesome;\n			padding:0 5px 0 0;\n			margin-right:2px;\n			color: #657587;\n		}\n\n		#tabTemp li.eltree:before {content: \"\\f1ea\";}\n		#tabCH   li.eltree:before {content: \"\\f009\";}\n		#tabSN   li.eltree:before {content: \"\\f121\";}\n		#tabTV   li.eltree:before {content: \"\\f022\";}\n		#tabPL   li.eltree:before {content: \"\\f1e6\";}\n		#tabMD   li.eltree:before {content: \"\\f085\";}\n		\n		.no-events { pointer-events: none; }\n		\n		\'.$unifyFrames_css.\'\n		\'.$treeButtonsInTab_css.\'\n		\n		</style>\n\n		<div class=\"tab-pane no-transition\" id=\"treePane\" style=\"border:0;\">\n		<script type=\"text/javascript\" src=\"media/script/tabpane.js\"></script>\n		<script src=\"media/script/bootstrap/js/bootstrap.min.js\"></script>\n		<script type=\"text/javascript\" src=\"media/script/jquery.quicksearch.js\"></script>\n        <script>\n            function initQuicksearch(inputId, listId) {\n                jQuery(\"#\"+inputId).quicksearch(\"#\"+listId+\" ul li\", {\n                    selector: \".elementname\",\n                    \"show\": function () { jQuery(this).removeClass(\"hide\"); },\n                    \"hide\": function () { jQuery(this).addClass(\"hide\"); },\n                    \"bind\":\"keyup\",\n                    \"onAfter\": function() {\n                        jQuery(\"#\"+listId).find(\".panel-collapse\").each( function() {\n                            var parentLI = jQuery(this);\n                            var totalLI  = jQuery(this).find(\"li\").length;\n                            var hiddenLI = jQuery(this).find(\"li.hide\").length;\n                            if (hiddenLI == totalLI) { parentLI.prev(\".panel-heading\").addClass(\"hide\"); }\n                            else { parentLI.prev(\".panel-heading\").removeClass(\"hide\"); }\n                        });\n                    }\n                });\n				jQuery(\".filterElements-form\").keydown(function (e) {\n					if (e.keyCode == 13) {\n						e.preventDefault();\n					}\n				});\n            }\n            \n            var storageKey = \"MODX_elementsInTreeParams\";\n            \n            // localStorage reset :\n            // localStorage.removeItem(storageKey);\n            \n			// Prepare remember collapsed categories function\n	        var storage = localStorage.getItem(storageKey);\n	        var elementsInTreeParams = {};\n	        var searchFieldCache = {};\n\n			try {\n	            if(storage != null) {\n	                try {\n						elementsInTreeParams = JSON.parse( storage );\n	                } catch(err) {\n	                    console.log(err);\n	                    elementsInTreeParams = { \"cat_collapsed\": {} };\n	                }\n	            } else {\n                    elementsInTreeParams = { \"cat_collapsed\": {} };\n                }\n                \n                // Remember collapsed categories functions\n                function setRememberCollapsedCategories(obj=null) {\n                    obj = obj == null ? elementsInTreeParams.cat_collapsed : obj;\n					for (var type in obj) {\n						if (!elementsInTreeParams.cat_collapsed.hasOwnProperty(type)) continue;\n						for (var category in elementsInTreeParams.cat_collapsed[type]) {\n							if (!elementsInTreeParams.cat_collapsed[type].hasOwnProperty(category)) continue;\n							state = elementsInTreeParams.cat_collapsed[type][category];\n							if(state == null) continue;\n							var collapseItem = jQuery(\"#collapse\" + type + category);\n							var toggleItem = jQuery(\"#toggle\" + type + category);\n							if(state == 0) {\n								// Collapsed\n								collapseItem.collapse(\"hide\");\n								toggleItem.addClass(\"collapsed\");\n							} else {\n								// Open\n								collapseItem.collapse(\"show\");\n								toggleItem.removeClass(\"collapsed\");\n							} \n						}\n					}\n					// Avoid first category collapse-flicker on reload\n					setTimeout(function() {\n				       jQuery(\"#treePane\").removeClass(\"no-transition\");\n					}, 50);\n				}\n\n                function setLastCollapsedCategory(type, id, state) {\n	                  state = state != 1 ? 1 : 0;\n	                  if(typeof elementsInTreeParams.cat_collapsed[type] == \"undefined\") elementsInTreeParams.cat_collapsed[type] = {};\n	                  elementsInTreeParams.cat_collapsed[type][id] = state;\n                }\n				function writeElementsInTreeParamsToStorage() {\n					var jsonString = JSON.stringify(elementsInTreeParams);\n					localStorage.setItem(storageKey, jsonString );\n				}\n				\n	            jQuery(document).ready(function() {\n\n                jQuery(\".filterElements-form\").keydown(function (e) {\n                    if(e.keyCode == 13) e.preventDefault();\n                });\n              \n                \'.$treeButtonsInTab_js.\'\n                \n                // Shift-Mouseclick opens/collapsed all categories\n                jQuery(\".accordion-toggle\").click(function(e) {\n					      e.preventDefault();\n					      var thisItemCollapsed = jQuery(this).hasClass(\"collapsed\");\n					      if (e.shiftKey) {\n					          // Shift-key pressed\n					          var toggleItems = jQuery(this).closest(\".panel-group\").find(\"> .panel .accordion-toggle\");\n					          var collapseItems = jQuery(this).closest(\".panel-group\").find(\"> .panel > .panel-collapse\");\n					          if(thisItemCollapsed) {\n					            toggleItems.removeClass(\"collapsed\");\n					            collapseItems.collapse(\"show\");\n					          } else {\n					            toggleItems.addClass(\"collapsed\");\n					            collapseItems.collapse(\"hide\");\n					          }\n					          // Save states to localStorage\n					          toggleItems.each(function() {\n					            state = jQuery(this).hasClass(\"collapsed\") ? 1 : 0;\n					            setLastCollapsedCategory(jQuery(this).data(\"cattype\"), jQuery(this).data(\"catid\"), state);\n					          });\n					          writeElementsInTreeParamsToStorage();\n					      } else {\n					        jQuery(this).toggleClass(\"collapsed\");\n					        jQuery(jQuery(this).attr(\"href\")).collapse(\"toggle\");\n					        // Save state to localStorage\n					        state = thisItemCollapsed ? 0 : 1;\n					        setLastCollapsedCategory(jQuery(this).data(\"cattype\"), jQuery(this).data(\"catid\"), state);\n					        writeElementsInTreeParamsToStorage();\n					      }\n					});\n					  \n					setRememberCollapsedCategories();\n\n	            });\n	        } catch(err) {\n	            alert(\"document.ready error: \" + err);\n	        }\n        </script>\n		<script type=\"text/javascript\">\n		treePane = new WebFXTabPane(document.getElementById( \"treePane\" ),true);\n		</script>\n		<div class=\"tab-page\" id=\"tabDoc\" style=\"padding-left:0; padding-right:0;\">\n		<h2 class=\"tab\">\'.$tabTreeTitle.\'</h2>\n		<script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabDoc\" ) );</script>\n	\';\n	$e->output($output);\n}\n\nif ( $modx->hasPermission(\'edit_template\') || $modx->hasPermission(\'edit_snippet\') || $modx->hasPermission(\'edit_chunk\') || $modx->hasPermission(\'edit_plugin\') || $modx->hasPermission(\'exec_module\') ) {\n	if($e->name == \'OnManagerTreeRender\'){\n		\n		if ($useIcons==\'yes\') {\n			$tabLabel_template  = \'<i class=\"fa fa-newspaper-o\"></i>\';\n			$tabLabel_tv        = \'<i class=\"fa fa-list-alt\"></i>\';\n			$tabLabel_chunk     = \'<i class=\"fa fa-th-large\"></i>\';\n			$tabLabel_snippet   = \'<i class=\"fa fa-code\"></i>\';\n			$tabLabel_plugin    = \'<i class=\"fa fa-plug\"></i>\';\n			$tabLabel_module    = \'<i class=\"fa fa-cogs\"></i>\';\n			$tabLabel_refresh   = \'<i class=\"fa fa-refresh\"></i>\';\n		}\n		else {\n			$tabLabel_template  = \'TPL\';\n			$tabLabel_tv        = \'TV\';\n			$tabLabel_chunk     = \'CH\';\n			$tabLabel_snippet   = \'SN\';\n			$tabLabel_plugin    = \'PL\';\n			$tabLabel_module    = \'MD\';\n			$tabLabel_refresh   = \'Refresh\';\n		}\n		\n		$tablePre = $modx->db->config[\'dbase\'] . \'.`\' . $modx->db->config[\'table_prefix\'];\n		\n		// createResourceList function\n		\n		function createResourceList($resourceTable,$action,$tablePre,$nameField = \'name\') {\n			global $modx, $_lang;\n			\n			$output  = \'\n				<form class=\"filterElements-form\" style=\"margin-top: 0;\">\n				  <input class=\"form-control\" type=\"text\" placeholder=\"Type here to filter list\" id=\"tree_\'.$resourceTable.\'_search\">\n				</form>\';\n				\n			$output .= \'<div class=\"panel-group\"><div class=\"panel panel-default\" id=\"tree_\'.$resourceTable.\'\">\';\n			$pluginsql = $resourceTable == \'site_plugins\' ? $tablePre.$resourceTable.\'`.disabled, \' : \'\';\n			$tvsql = $resourceTable == \'site_tmplvars\' ? $tablePre.$resourceTable.\'`.caption, \' : \'\';\n			//$orderby = $resourceTable == \'site_plugins\' ? \'6,2\' : \'5,1\';\n\n			if ($resourceTable == \'site_plugins\' || $resourceTable == \'site_tmplvars\') {\n				$orderby= \'6,2\';\n			}\n			\n			else{\n				$orderby= \'5,1\';\n			}\n\n			$sql = \'SELECT \'.$pluginsql.$tvsql.$tablePre.$resourceTable.\'`.\'.$nameField.\' as name, \'.$tablePre.$resourceTable.\'`.id, \'.$tablePre.$resourceTable.\'`.description, \'.$tablePre.$resourceTable.\'`.locked, if(isnull(\'.$tablePre.\'categories`.category),\\\'\'.$_lang[\'no_category\'].\'\\\',\'.$tablePre.\'categories`.category) as category, \'.$tablePre.\'categories`.id  as catid FROM \'.$tablePre.$resourceTable.\'` left join \'.$tablePre.\'categories` on \'.$tablePre.$resourceTable.\'`.category = \'.$tablePre.\'categories`.id ORDER BY \'.$orderby;\n			\n			$rs = $modx->db->query($sql);\n			$limit = $modx->db->getRecordCount($rs);\n			\n			if($limit<1){\n				return \'\';\n			}\n			\n			$preCat = \'\';\n			$insideUl = 0;\n			\n			for($i=0; $i<$limit; $i++) {\n				$row = $modx->db->getRow($rs);\n				$row[\'category\'] = stripslashes($row[\'category\']); //pixelchutes\n				if ($preCat !== $row[\'category\']) {\n					$output .= $insideUl? \'</div>\': \'\';\n					$row[\'catid\'] = intval($row[\'catid\']);\n                    $output .= \'<div class=\"panel-heading\"><span class=\"panel-title\"><a class=\"accordion-toggle\" id=\"toggle\'.$resourceTable.$row[\'catid\'].\'\" href=\"#collapse\'.$resourceTable.$row[\'catid\'].\'\" data-cattype=\"\'.$resourceTable.\'\" data-catid=\"\'.$row[\'catid\'].\'\" title=\"Click to toggle collapse. Shift+Click to toggle all.\"> \'.$row[\'category\'].\'</a></span></div><div class=\"panel-collapse in \'.$resourceTable.\'\"  id=\"collapse\'.$resourceTable.$row[\'catid\'].\'\"><ul>\';\n					$insideUl = 1;\n				}\n				if ($resourceTable == \'site_plugins\') $class = $row[\'disabled\'] ? \' class=\"disabledPlugin\"\' : \'\';\n				$output .= \'<li class=\"eltree\"><span\'.$class.\'><a href=\"index.php?id=\'.$row[\'id\'].\'&amp;a=\'.$action.\'\" target=\"main\"><span class=\"elementname\">\'.$row[\'name\'].\'</span><small> (\' . $row[\'id\'] . \')</small></a>\n                  <a class=\"ext-ico\" href=\"#\" title=\"Open in new window\" onclick=\"window.open(\\\'index.php?id=\'.$row[\'id\'].\'&a=\'.$action.\'\\\',\\\'gener\\\',\\\'width=800,height=600,top=\\\'+((screen.height-600)/2)+\\\',left=\\\'+((screen.width-800)/2)+\\\',toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no\\\')\"> <small><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></small></a>\'.($modx_textdir ? \'&rlm;\' : \'\').\'</span>\';\n				\n				$output .= $row[\'locked\'] ? \' <em>(\'.$_lang[\'locked\'].\')</em>\' : \"\" ;\n				$output .= \'</li>\';\n				$preCat = $row[\'category\'];\n			}\n			$output .= $insideUl? \'</ul></div></div>\': \'\';\n			$output .= \'</div>\';\n			$output .= \'\n    \n        <script>\n          jQuery(\\\'#collapse\'.$resourceTable.$row[\'catid\'].\'\\\').collapse();\n          initQuicksearch(\\\'tree_\'.$resourceTable.\'_search\\\', \\\'tree_\'.$resourceTable.\'\\\');\n          jQuery(\\\'#tree_\'.$resourceTable.\'_search\\\').on(\\\'focus\\\', function () {\n            searchFieldCache = elementsInTreeParams.cat_collapsed;\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').removeClass(\"collapsed\");\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').addClass(\"no-events\");\n            jQuery(\\\'.\'.$resourceTable.\'\\\').collapse(\\\'show\\\');\n          }).on(\\\'blur\\\', function () {\n            setRememberCollapsedCategories(searchFieldCache);\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').removeClass(\"no-events\");\n          });\n        </script>\';\n			return $output;\n		}\n		\n		// end createResourceList function\n		\n		// createModulesList function\n		\n		function createModulesList($resourceTable,$action,$tablePre,$nameField = \'name\') {\n		\n			global $modx, $_lang;\n			\n			$output  = \'\n				<form class=\"filterElements-form\" style=\"margin-top: 0;\">\n				  <input class=\"form-control\" type=\"text\" placeholder=\"Type here to filter list\" id=\"tree_\'.$resourceTable.\'_search\">\n				</form>\';\n				\n			$output .= \'<div class=\"panel-group\"><div class=\"panel panel-default\" id=\"tree_\'.$resourceTable.\'\">\';\n\n			if ($_SESSION[\'mgrRole\'] != 1) {\n				$rs = $modx->db->query(\'SELECT sm.id, sm.name, sm.category, sm.disabled, cats.category AS catname, cats.id AS catid, mg.member\n				FROM \' . $modx->getFullTableName(\'site_modules\') . \' AS sm\n				LEFT JOIN \' . $modx->getFullTableName(\'site_module_access\') . \' AS sma ON sma.module = sm.id\n				LEFT JOIN \' . $modx->getFullTableName(\'member_groups\') . \' AS mg ON sma.usergroup = mg.user_group\n				LEFT JOIN \' . $modx->getFullTableName(\'categories\') . \' AS cats ON sm.category = cats.id\n				WHERE (mg.member IS NULL OR mg.member = \' . $modx->getLoginUserID() . \') AND sm.disabled != 1 AND sm.locked != 1\n				ORDER BY 5,1\');\n			} \n			\n			else {\n				$rs = $modx->db->query(\'SELECT sm.id, sm.name, sm.category, sm.disabled, cats.category AS catname, cats.id AS catid\n				FROM \' . $modx->getFullTableName(\'site_modules\') . \' AS sm\n				LEFT JOIN \' . $modx->getFullTableName(\'categories\') . \' AS cats ON sm.category = cats.id\n				WHERE sm.disabled != 1\n				ORDER BY 5,1\');\n			}\n			\n			$limit = $modx->db->getRecordCount($rs);\n			\n			if($limit<1){\n                return \'\';\n			}\n			\n			$preCat   = \'\';\n			$insideUl = 0;\n			\n			for($i=0; $i<$limit; $i++) {\n				$row = $modx->db->getRow($rs);\n				if($row[\'catid\'] > 0) {\n					$row[\'catid\'] = stripslashes($row[\'catid\']);\n				} else {\n					$row[\'catname\'] = $_lang[\"no_category\"];\n				}\n				if ($preCat !== $row[\'category\']) {\n					$output .= $insideUl? \'</div>\': \'\';\n					$row[\'catid\'] = intval($row[\'catid\']);\n                    $output .= \'<div class=\"panel-heading\"><span class=\"panel-title\"><a class=\"accordion-toggle\" id=\"toggle\'.$resourceTable.$row[\'catid\'].\'\" href=\"#collapse\'.$resourceTable.$row[\'catid\'].\'\" data-cattype=\"\'.$resourceTable.\'\" data-catid=\"\'.$row[\'catid\'].\'\" title=\"Click to toggle collapse. Shift+Click to toggle all.\"> \'.$row[\'catname\'].\'</a></span></div><div class=\"panel-collapse in \'.$resourceTable.\'\"  id=\"collapse\'.$resourceTable.$row[\'category\'].\'\"><ul>\';\n					$insideUl = 1;\n				}\n				$output .= \'<li class=\"eltree\"><span><a href=\"index.php?id=\'.$row[\'id\'].\'&amp;a=\'.$action.\'\" target=\"main\"><span class=\"elementname\">\'.$row[\'name\'].\'</span><small> (\' . $row[\'id\'] . \')</small></a>\n                  <a class=\"ext-ico\" href=\"#\" title=\"Open in new window\" onclick=\"window.open(\\\'index.php?id=\'.$row[\'id\'].\'&a=\'.$action.\'\\\',\\\'gener\\\',\\\'width=800,height=600,top=\\\'+((screen.height-600)/2)+\\\',left=\\\'+((screen.width-800)/2)+\\\',toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no\\\')\"> <small><i class=\"fa fa-external-link\" aria-hidden=\"true\"></i></small></a>\'.($modx_textdir ? \'&rlm;\' : \'\').\'</span>\';\n				$output .= $row[\'locked\'] ? \' <em>(\'.$_lang[\'locked\'].\')</em>\' : \"\" ;\n				$output .= \'</li>\';\n				$preCat  = $row[\'category\'];\n			}\n			$output .= $insideUl? \'</ul></div></div>\': \'\';\n			$output .= \'</div>\';\n			$output .= \'\n    \n        <script>\n          initQuicksearch(\\\'tree_\'.$resourceTable.\'_search\\\', \\\'tree_\'.$resourceTable.\'\\\');\n          jQuery(\\\'#tree_\'.$resourceTable.\'_search\\\').on(\\\'focus\\\', function () {\n            searchFieldCache = elementsInTreeParams.cat_collapsed;\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').addClass(\"no-events\");\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').removeClass(\"collapsed\");\n            jQuery(\\\'.\'.$resourceTable.\'\\\').collapse(\\\'show\\\');\n          }).on(\\\'blur\\\', function () {\n            jQuery(\\\'#tree_\'.$resourceTable.\' .accordion-toggle\\\').removeClass(\"no-events\");\n            setRememberCollapsedCategories(searchFieldCache);\n          });\n        </script>\';\n			return $output;\n		}\n		\n		// end createModulesList function\n		\n		$temp    = createResourceList(\'site_templates\',16,$tablePre,\'templatename\');\n		$tv      = createResourceList(\'site_tmplvars\',301,$tablePre);\n		$chunk   = createResourceList(\'site_htmlsnippets\',78,$tablePre);\n		$snippet = createResourceList(\'site_snippets\',22,$tablePre);\n		$plugin  = createResourceList(\'site_plugins\',102,$tablePre);\n		$module  = createModulesList(\'site_modules\',112,$tablePre);\n\n		if ( $modx->hasPermission(\'edit_template\') || $modx->hasPermission(\'edit_snippet\') || $modx->hasPermission(\'edit_chunk\') || $modx->hasPermission(\'edit_plugin\') || $modx->hasPermission(\'exec_module\') ) {\n			$output = \'</div>\';\n		}\n\n		if ($modx->hasPermission(\'edit_template\')) {\n			$output .= \'\n              <div class=\"tab-page\" id=\"tabTemp\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Templates\">\'.$tabLabel_template.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabTemp\" ) );</script>\n              \'.$temp.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              <li><a href=\"index.php?a=19\" target=\"main\">\'.$_lang[\'new_template\'].\'</a></li>\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was added or deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n              <div class=\"tab-page\" id=\"tabTV\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Template Variables\">\'.$tabLabel_tv.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabTV\" ) );</script>\n              \'.$tv.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              <li><a href=\"index.php?a=300\" target=\"main\">\'.$_lang[\'new_tmplvars\'].\'</a></li>\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was added or deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n	        \';\n		}\n\n		if ($modx->hasPermission(\'edit_chunk\')) {\n			$output .= \'\n              <div class=\"tab-page\" id=\"tabCH\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Chunks\">\'.$tabLabel_chunk.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabCH\" ) );</script>\n              \'.$chunk.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              <li><a href=\"index.php?a=77\" target=\"main\">\'.$_lang[\'new_htmlsnippet\'].\'</a></li>\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was added or deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n	        \';\n		}\n\n		if ($modx->hasPermission(\'edit_snippet\')) {\n			$output .= \'\n              <div class=\"tab-page\" id=\"tabSN\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Snippets\">\'.$tabLabel_snippet.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabSN\" ) );</script>\n              \'.$snippet.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              <li><a href=\"index.php?a=23\" target=\"main\">\'.$_lang[\'new_snippet\'].\'</a></li>\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was added or deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n	        \';\n		}\n\n		if ($modx->hasPermission(\'edit_plugin\')) {\n			$output .= \'\n              <div class=\"tab-page\" id=\"tabPL\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Plugins\">\'.$tabLabel_plugin.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabPL\" ) );</script>\n              \'.$plugin.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              <li><a href=\"index.php?a=101\" target=\"main\">\'.$_lang[\'new_plugin\'].\'</a></li>\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was enabled/disabled/added/deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n	        \';\n		}\n		\n		if ($modx->hasPermission(\'exec_module\')) {\n			\n			$new_module_button = \'\';\n      \n			if ($modx->hasPermission(\'new_module\')) {\n				$new_module_button = \'<li><a href=\"index.php?a=107\" target=\"main\">\'.$_lang[\'new_module\'].\'</a></li>\';\n			}\n			\n			$output .= \'\n              <div class=\"tab-page\" id=\"tabMD\" style=\"padding-left:0; padding-right:0;\">\n              <h2 class=\"tab\" title=\"Modules\">\'.$tabLabel_module.\'</h2>\n              <script type=\"text/javascript\">treePane.addTabPage( document.getElementById( \"tabMD\" ) );</script>\n              \'.$module.\'\n              <br/>\n              <ul class=\"actionButtons\">\n              \'.$new_module_button.\'\n              <li><a href=\"javascript:location.reload();\" title=\"Click here if element was enabled/disabled/added/deleted to refresh the list.\">\'.$tabLabel_refresh.\'</a></li>\n              </ul>\n              </div>\n	      \';\n		}\n\n        if ($modx->hasPermission(\'edit_template\') || $modx->hasPermission(\'edit_snippet\') || $modx->hasPermission(\'edit_chunk\') || $modx->hasPermission(\'edit_plugin\') || $modx->hasPermission(\'exec_module\') ) {\n			$output .= \'</div>\';\n			$e->output($output);\n		}\n\n	}\n}','0','&tabTreeTitle=Tree Tab Title;text;Site Tree;;Custom title of Site Tree tab. &useIcons=Use icons in tabs;list;yes,no;yes;;Icons available in MODX version 1.2 or newer. &treeButtonsInTab=Tree Buttons in tab;list;yes,no;yes;;Move Tree Buttons into Site Tree tab. &unifyFrames=Unify Frames;list;yes,no;yes;;Unify Tree and Main frame style. Right now supports MODxRE2 theme only.','0','');

INSERT INTO `modx_site_plugins` VALUES ('9','SEO Manager','<strong>2.0</strong> The plugin allows you to manage SEO for a multilingual site.','0','4','0','\n/**\n * SEO Manager\n *\n * The plugin allows you to manage SEO for a multilingual site.\n *\n * @category 	plugin\n * @version 	2.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL v3)\n * @internal    @properties\n * @internal	@events OnWebPageInit,OnPageNotFound,OnLoadWebDocument,OnDocFormSave,OnCacheUpdate\n * @internal	@modx_category SEO\n * @internal    @legacy_names SEO\n * @internal    @installset base, sample\n * @reportissues https://github.com/modxcms/evolution\n * @documentation\n * @link\n * @author      Seiger\n * @author      Since 2017: Seiger\n * @lastupdate  14/07/2017\n */\n\nrequire MODX_BASE_PATH . \"assets/modules/seo/plugin.php\";','0','','0','');

INSERT INTO `modx_site_plugins` VALUES ('10','TinyMCE4','<strong>4.3.7.2</strong> Javascript WYSIWYG editor','0','6','0','require MODX_BASE_PATH.\'assets/plugins/tinymce4/plugin.tinymce.php\';','0','&styleFormats=Custom Style Formats;textarea;Title,cssClass|Title2,cssClass &customParams=Custom Parameters <b>(Be careful or leave empty!)</b>;textarea; &entityEncoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &pathOptions=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &resizing=Advanced Resizing;list;true,false;false &disabledButtons=Disabled Buttons;text; &webTheme=Web Theme;test;webuser &webPlugins=Web Plugins;text; &webButtons1=Web Buttons 1;text;bold italic underline strikethrough removeformat alignleft aligncenter alignright &webButtons2=Web Buttons 2;text;link unlink image undo redo &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;100% &height=Height;text;400px &introtextRte=<b>Introtext RTE</b><br/>add richtext-features to \"introtext\";list;enabled,disabled;disabled &inlineMode=<b>Inline-Mode</b>;list;enabled,disabled;disabled &inlineTheme=<b>Inline-Mode</b><br/>Theme;text;inline','0','');


# --------------------------------------------------------

#
# Table structure for table `modx_site_snippets`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_snippets`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_snippets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Snippet',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `properties` text COMMENT 'Default Properties',
  `moduleguid` varchar(32) NOT NULL DEFAULT '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='Contains the site snippets.';

#
# Dumping data for table `modx_site_snippets`
#

INSERT INTO `modx_site_snippets` VALUES ('1','Wayfinder','<strong>2.0.5</strong> Completely template-driven and highly flexible menu builder','0','7','0','return require MODX_BASE_PATH.\'assets/snippets/wayfinder/snippet.wayfinder.php\';\n','0','','');

INSERT INTO `modx_site_snippets` VALUES ('2','Ditto','<strong>2.1.2</strong> Summarizes and lists pages to create blogs, catalogs, PR archives, bio listings and more','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/ditto/snippet.ditto.php\';','0','','');

INSERT INTO `modx_site_snippets` VALUES ('3','R','<strong>1.3</strong> PHPThumb creates thumbnails and altered images on the fly and caches them','0','5','0','return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';\n','0','','');

INSERT INTO `modx_site_snippets` VALUES ('4','Breadcrumbs','<strong>1.0.5</strong> Configurable breadcrumb page-trail navigation','0','7','0','return require MODX_BASE_PATH.\'assets/snippets/breadcrumbs/snippet.breadcrumbs.php\';','0','{}',' ');

INSERT INTO `modx_site_snippets` VALUES ('5','Minify','<strong>1.0</strong> Minify html, css and js','0','5','0','\n/**\n * Minify\n *\n * Minify html, css and js\n *\n * @category	snippet\n * @version 	1.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n */\n\n$res = \"\";\nrequire MODX_BASE_PATH . \"assets/shop/snippet.minify.php\";\nreturn $res;','0','','');

INSERT INTO `modx_site_snippets` VALUES ('6','Auth','<strong>1.0</strong> Web User Authorization','0','5','0','\n/**\n * Auth\n *\n * Web User Authorization\n *\n * @category	snippet\n * @version 	1.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n */\n\nreturn require MODX_BASE_PATH . \"assets/shop/snippet.auth.php\";','0','','');

INSERT INTO `modx_site_snippets` VALUES ('7','if','<strong>1.3</strong> A simple conditional snippet. Allows for eq/neq/lt/gt/etc logic within templates, resources, chunks, etc.','0','7','0','return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';','0','','');

INSERT INTO `modx_site_snippets` VALUES ('8','Shop','<strong>1.0</strong> Base snippet','0','5','0','\n/**\n * Shop\n *\n * Base snippet\n *\n * @category	snippet\n * @version 	1.0\n * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)\n * @internal	@properties\n * @internal	@modx_category Content\n * @internal    @installset base, sample\n */\n\n$lang = $modx->config[\'lang_enable\'] ? $modx->config[\'lang\'] : $modx->config[\'lang_default\'];\n$res  = \"\";\nrequire MODX_BASE_PATH . \"assets/shop/snippet.shop.php\";\nreturn $res;','0','','');


# --------------------------------------------------------

#
# Table structure for table `modx_site_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_templates` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `templatename` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-page,1-content',
  `content` mediumtext,
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `selectable` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COMMENT='Contains the site templates.';

#
# Dumping data for table `modx_site_templates`
#

INSERT INTO `modx_site_templates` VALUES ('1','404','','0','1','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main error-page\">\n	<section class=\"page-top page-top--no-bg page-top--error\"></section>\n	<div class=\"container\">\n		<div class=\"col-lg-8 col-lg-offset-2 col-xs-12\">\n			<section class=\"section-error\">\n				<div class=\"content\">\n					<div class=\"text\">[*pagetitle*]</div>\n					<div class=\"title\">[*longtitle*]</div>\n				</div>\n				<!-- <a href=\"[~~[(site_start)]~~]\">[*description*] <span class=\"seconds\">[*link_attributes*]&nbsp;</span>[*introtext*]</a> -->\n			</section>\n		</div>\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('2','Home page','<strong>1.0</strong> Home page template','0','1','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main\">\n	<section class=\"main-slider\">\n		[[Shop?&get=`getImages`&tpl=`tpl_img_main_slider`]]\n	</section>\n	<section class=\"section advantages\">\n		[[Shop?&get=`getContent`&did=`[(advantages)]`&tpl=`tpl_advantages`]]\n	</section>\n	<section class=\"section tabs-wrapper\">\n		<div class=\"container\">\n			<ul class=\"tabs-list\">\n				<li class=\"tabs__item\"><a class=\"tabs__link active\" href=\"#new\">[#67#]</a></li>\n				<li class=\"tabs__item\"><a class=\"tabs__link\" href=\"#promotions\">[#68#]</a></li>\n				<li class=\"tabs__item\"><a class=\"tabs__link\" href=\"#popular\">[#69#]</a></li>\n			</ul>\n			<div class=\"tabs-wrapper\">\n				<div class=\"tabs-block\" id=\"new\">\n					<div class=\"product-slider js_height-block\">\n						[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&where=`product_main_slider_new = \'1\'`]]\n					</div>\n				</div>\n				<div class=\"tabs-block\" id=\"promotions\">\n					<div class=\"product-slider js_height-block\">\n						[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&where=`product_main_slider_discont = \'1\'`]]\n					</div>\n				</div>\n				<div class=\"tabs-block\" id=\"popular\">\n					<div class=\"product-slider js_height-block\">\n						[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&where=`product_main_slider_popular = \'1\'`]]\n					</div>\n				</div>\n			</div>\n		</div>\n	</section>\n	<section class=\"section steps\">\n		<div class=\"container\">\n			<div class=\"section-title text-white\">[*longtitle*]</div>\n			<ul class=\"steps-list\">\n				[[Shop?&get=`getContents`&did=`[*tv_child_id_1*]`&tpl=`tpl_main_steps`]]\n			</ul>\n		</div>\n	</section>\n	<section class=\"section section-manufacturers section-slider\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#93#]</div>\n			<ul class=\"tabs-list\">\n				[[Shop?&get=`getFilterList`&tpl=`tpl_main_manufacturer`&filter_url=`[*tv_manufacturer_filter*]`]]\n			</ul>\n			<div class=\"product-slider\">\n				[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&where=`product_status = \'1\'`]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-articles\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#59#]</div>\n		</div>\n		<div class=\"container\">\n			<div class=\"articles-list\">\n				[[Shop? &get=`getNews` &tpl=`tpl_news` &limit=`4`]]\n			</div>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('17','Discounts','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main discount-page\">\n	<section class=\"page-top page-top--discount\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"discount-wrapper\">\n		<div class=\"container\">\n			[[Shop? &get=`getDiscounts` &tpl=`tpl_discount`]]\n		</div>\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('10','Service','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main services-page\">\n	<section class=\"page-top page-top--services\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"discount-wrapper services-wrapper\">\n		<div class=\"container\">[*id*]\n			[[Shop?&get=`getContents`&did=`26`&tpl=`tpl_services`&tvs=`tv_img`]]\n		</div>\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('15','Single news','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main news-single-page\">\n	<section class=\"page-top page-top--news-single\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-article\">\n		<div class=\"container\"> \n			<div class=\"article-header\">\n				<div class=\"article-header-img\"><img src=\"[[R?&img=`[*image*]`&opt=`w=1170&h=878&far=1`]]\" alt=\"[*pagetitle*]\" title=\"[*pagetitle*]\"></div>\n				<div class=\"article-header-text\">[*news_description*]</div>\n			</div>\n			<article class=\"article-body\">\n				<div class=\"content\">\n					[*news_content*]\n				</div>\n			</article>\n		</div>\n	</section>\n	<section class=\"article-products section-slider\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#70#]</div>\n			<div class=\"slider-nav\">\n				<div class=\"slider-nav__item slider-nav__item--prev\"><i class=\"icon icon-arrow\"></i></div>\n				<div class=\"slider-nav__item slider-nav__item--next\"><i class=\"icon icon-arrow\"></i></div>\n			</div>\n			<div class=\"article-products-slider\">\n				[[Shop?&get=`getCatalog`&onlyIds=`[*news_products*]`&tpl=`tpl_main_slider`]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-articles\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#59#]</div>\n		</div>\n		<div class=\"container\">\n			<div class=\"articles-list\">\n				[[Shop? &get=`getNews` &tpl=`tpl_news` &limit=`4` &exclude=`[*id*]`]]\n			</div>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('9','Empty','','0','1','','0','','0','1');

INSERT INTO `modx_site_templates` VALUES ('11','About','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main about-page\">\n	<section class=\"page-top page-top--about\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section-entry\">\n		<div class=\"container\"> \n			<div class=\"row\">\n				<div class=\"col-lg-6 col-lg-offset-3 col-xs-12\">\n					<div class=\"heading-h2\">[*longtitle*]</div>\n					<p>[*description*] [*introtext*]</p>\n				</div>\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-facts\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[*link_attributes*]</div>\n			<div class=\"facts\">\n				[[Shop? &get=`getParseString` &tpl=`tpl_about_facts` &string=`[*tv_content*]`]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section advantages\">\n		[[Shop?&get=`getContent`&did=`[(advantages)]`&tpl=`tpl_advantages`]]\n	</section>\n	<section class=\"section\">\n		<div class=\"content\">\n			<div class=\"container\">\n				<div class=\"row\">\n					<div class=\"col-lg-6 col-xs-12\">   \n						[*content*]\n					</div>\n					<div class=\"col-lg-6 col-xs-12\">\n						<div class=\"accordion-wrapper\">\n							<div class=\"accordion-list\">\n							[[	[[Shop?&get=`getContents`&did=`[*id*]`&tpl=`tpl_about_faq`]] ]]\n								<a href=\"[(site_url)][(lang)]/oplata-i-dostavka/\" class=\"accordion-list__item\">\n									<div class=\"title\">[#101#]</div>\n								</a>\n								<a href=\"[(site_url)][(lang)]/akcii/\" class=\"accordion-list__item\">\n									<div class=\"title\">[#127#]</div>\n								</a>\n								<a href=\"[(site_url)][(lang)]/kontakty/\" class=\"accordion-list__item\">\n									<div class=\"title\">[#128#]</div>\n								</a>\n							</div>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('13','Contacts','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main contacts\">\n	<section class=\"page-top page-top--contacts\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section-contacts\">\n		<div class=\"container\">\n			<div class=\"contacts-wrap\">\n				<div class=\"contacts-info\">\n					<div class=\"heading-h2\">[*longtitle*]</div>\n					<div class=\"contacts-row\">\n						<address class=\"address\"> <span class=\"icon icon-location\"> </span>[#37#]</address>\n					</div>\n					<div class=\"contacts-row\">\n						<div class=\"schedule__item\"><span class=\"icon icon-clock\"></span><span class=\"days\">[#57#]&nbsp;</span><span class=\"time\">[#55#]</span></div>\n						<div class=\"schedule__item\"><span class=\"days\">[#58#]&nbsp;</span><span class=\"time\">[#56#]</span></div>\n					</div>\n					<div class=\"contacts-row\">\n						<div class=\"phone__item\"><span class=\"icon-phone\"></span>\n							<a href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_1)]`]]\">[(as_phone_1)]</a>\n						</div>\n						<div class=\"phone__item\">\n							<a href=\"tel:[(as_country_code)][[S? &get=`setMiniPhone` &string=`[(as_phone_2)]`]]\">[(as_phone_2)]</a>\n						</div>\n					</div>\n					<div class=\"social-block\">\n						[[if?&is=`[(as_facebook)]:empty`&else=`<a class=\"social-link\" href=\"[(as_facebook)]\" target=\"_blank\" rel=\"nofollow\"><span class=\"icon icon-facebook\"></span></a>`]]\n						[[if?&is=`[(as_instagram)]:empty`&else=`<a class=\"social-link\" href=\"[(as_instagram)]\" target=\"_blank\" rel=\"nofollow\"><span class=\"icon icon-instagram\"></span></a>`]]\n					</div>\n				</div>\n				<div class=\"contacts-map\">\n					<div class=\"mapouter\">\n						<div class=\"gmap_canvas\">\n							<iframe width=\"100%\" height=\"470\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?q=[#37#]&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n							<!-- <a href=\"https://www.pureblack.de\">Pureblack.de - Webseite erstellen lassen</a> -->\n						</div>\n						<style>.mapouter{position:relative;text-align:right;height:470px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:470px;width:100%;}</style>\n					</div>\n				</div>\n			</div>\n		</div>\n	</section>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('14','pay-and-delivery','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main pay-and-delivery\">\n	<section class=\"page-top page-top--sub-catalogue\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section-content\"> \n		<div class=\"container\">\n			<div class=\"content\">\n				<div class=\"row bottom-xs\">\n					<div class=\"col-md-6 col-xs-12\">\n						<div class=\"heading-h2\">[*longtitle*]</div>\n						<ul>\n							[[Shop? &get=`getParseString` &tpl=`tpl_li` &string=`[*description*]`]]\n						</ul>\n					</div>\n					<div class=\"col-md-6 col-xs-12\"><img src=\"[[R?&img=`[*tv_img2*]`&opt=`w=745&h=430&far=1`]]\" alt=\"[*longtitle*]\" title=\"[*longtitle*]\"></div>\n				</div>\n				<div class=\"row middle-xs\">\n					<div class=\"col-md-6 first-md col-xs-12 last-xs\"><img src=\"[[R?&img=`[*tv_img*]`&opt=`w=745&h=430&far=1`]]\" alt=\"[*link_attributes*]\" title=\"[*link_attributes*]\"></div>\n					<div class=\"col-md-6 col-xs-12\">\n						<div class=\"heading-h2\">[*link_attributes*]</div>\n						<ul>\n							[[Shop? &get=`getParseString` &tpl=`tpl_li` &string=`[*introtext*]`]]\n						</ul>\n					</div>\n				</div>\n			</div>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('3','Catalog_inside_doors','<strong>1.0</strong> Catalog list template','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main catalogue\">\n	<section class=\"page-top page-top--interior-doors\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"section-wrapper section-wrapper--first\">\n		<div class=\"container\">\n			<section class=\"top-sale\">\n				<div class=\"section-title section-title--white section-title--before\">[*longtitle*]</div>\n				<div class=\"slider-nav\"> \n					<div class=\"slider-nav__item slider-nav__item--prev\"><i class=\"icon icon-arrow\"></i></div>\n					<div class=\"slider-nav__item slider-nav__item--next\"><i class=\"icon icon-arrow\"></i></div>\n				</div>\n				<div class=\"top-sale-slider\">\n					[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&sort=`top-sales`&where=`product_category = \'[*id*]\'`]]\n				</div>\n			</section>\n		</div>\n	</div>\n\n	<div class=\"section-bg section-bg--appointment\" style=\"background: url(\'[[Shop?&get=`getFieldTV`&did=`[*tv_child_id_1*]`&field=`tv_bg`]]\');background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-position: center bottom -270px;\"></div>\n	<div class=\"section-wrapper\">\n		<div class=\"container\">\n			<section class=\"section-appointment\">\n				<div class=\"section-title section-title--white section-title--before\">[[Shop?&get=`getField`&did=`[*tv_child_id_1*]`&field=`pagetitle_[(lang)]`]]</div>\n				<div class=\"appointment-wrapper\">\n					<ul class=\"appointment-list\">\n						[[Shop?&get=`getContents`&did=`[*tv_child_id_1*]`&tpl=`tpl_appointment`&tvs=`tv_img`]]\n					</ul>\n				</div>\n			</section>\n		</div>\n	</div>\n	\n	<div class=\"section-bg section-bg--materials\" style=\"background: url(\'[[Shop?&get=`getFieldTV`&did=`[*tv_child_id_2*]`&field=`tv_bg`]]\');background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-position: center bottom -270px;\"></div>\n	<div class=\"section-wrapper\">\n		<div class=\"container\">\n			<section class=\"section-appointment\">\n				<div class=\"section-title section-title--white section-title--before\">[[Shop?&get=`getField`&did=`[*tv_child_id_2*]`&field=`pagetitle_[(lang)]`]]</div>\n				<div class=\"materials-wrapper\">\n					<ul class=\"materials-list\">\n						[[Shop?&get=`getContents`&did=`[*tv_child_id_2*]`&tpl=`tpl_materials`&tvs=`tv_img`]]\n					</ul>\n				</div>\n			</section>\n		</div>\n	</div>\n	<div class=\"section-bg section-bg--types\" style=\"background: url(\'[[Shop?&get=`getFieldTV`&did=`[*tv_child_id_3*]`&field=`tv_bg`]]\');background-repeat: no-repeat;-webkit-background-size: cover;background-size: cover;background-position: center bottom -270px;\"></div>\n	<div class=\"section-wrapper\">\n		<div class=\"container\">\n			<section class=\"section-appointment\">\n				<div class=\"section-title section-title--white section-title--before\">[[Shop?&get=`getField`&did=`[*tv_child_id_3*]`&field=`pagetitle_[(lang)]`]]</div>\n				<div class=\"door-types-wrapper\">\n					<ul class=\"door-types-list\">\n						[[Shop?&get=`getContents`&did=`[*tv_child_id_3*]`&tpl=`tpl_door_types`&tvs=`tv_img`]]\n					</ul>\n				</div>\n			</section>\n		</div>\n	</div>\n	{{tpl_seo_block}}\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('4','SubCatalog','<strong>1.0</strong> Sub catalog list template','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main sub-catalogue\">\n	<section class=\"page-top page-top--sub-catalogue\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"container\">\n		<div class=\"sub-catalogue-wrapper\">\n			<div class=\"filters-wrapper\" id=\"filters\">\n				<div class=\"section-title section-title--before\">[#5#]</div>\n				<div class=\"filters-block\">\n					<div class=\"filters-block__row\">\n						<div class=\"labels-wrapper\">\n							[[Shop?&get=`getFilters`&tpl=`tpl_filters_del`&tplInner=`tpl_filtersInner_del`&del=`1`]]\n						</div>\n						<div class=\"filters-box\">\n							<div class=\"labels-wrapper\"></div><a class=\"btn btn-bordered btn-big btn-wide\" href=\"[~~[*id*]~~]\">[#76#]</a>\n						</div>\n					</div>\n					<div class=\"filters-block__row\">\n						<div class=\"filters-box\">\n							<ul class=\"filters-list active\">\n								<li class=\"filters-list__item no-margin\"><a class=\"filters-list-link[[if?&is=`[+only_available+]:=:1`&then=` active`]]\" data-filter=\"available=true\" href=\"[~~[*id*]~~]f/available=true/\"<span class=\"filters-list-lbl\">Показывать только в наличии</span></a></li>\n							</ul>\n						</div>\n					</div>\n					[!Shop?&get=`getFilterPrice`&tpl=`tpl_filterPrice`!]\n\n					[[Shop?&get=`getFilters`&tpl=`tpl_filters`&tplInner=`tpl_filtersInner`]]\n				</div>\n			</div>\n			<div class=\"products-list-wrapper\">\n				<div class=\"products-list-header\">\n					<div class=\"left-block\">\n						<div class=\"text-shown\">Показано <span class=\"amount\">[!if?&is=`[+maxitem+]:empty`&then=`0`&else=`[+maxitem+]`!]&nbsp;</span>[+prod_items+]</div><a class=\"btn btn-lg btn-orange js_show-filters\" href=\"#filters\">Фильтры</a>\n					</div>\n					<div class=\"selects-block\">\n						[!Shop?\n						&get=`getOption`\n						&tpl=`tpl_getOption_single`\n						&catch=`sort`\n						&select=`active`\n						&options=`price-up=[#79#],price-down=[#78#],new-up=[#80#],first-hit=[#81#]`\n						!]\n					</div>\n				</div>\n				<ul class=\"product-list\">\n					[!Shop?\n					&get=`getCatalog`\n					&tpl=`tpl_catalog`\n					&tplEmpty=`tpl_catalogEmpty`\n					&pagin=`1`&limit=`24`\n					!]\n					<div class=\"products-list-footer\"> \n						<div class=\"pagination-wrapper\">\n							<ul class=\"pagination-list\">\n								[!Shop?\n								&get=`getPaginate`\n								&tpl=`tpl_paginate`\n								&tpl_current=`tpl_paginateCurrent`\n								&tpl_prev=`tpl_paginatePrev`\n								&tpl_next=`tpl_paginateNext`\n								&tpl_indent=`tpl_paginateIndent`\n								!]\n							</ul>\n						</div>\n					</div>\n					</div>\n			</div>\n		</div>\n		{{tpl_seo_block}}\n		[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n		</main>\n	{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('5','Product','<strong>1.0</strong> Product template','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main product--single\">\n	<section class=\"page-top page-top--product-single\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"page-wrapper\">\n		<div class=\"product-card-wrapper\">\n			<div class=\"container\">\n				<section class=\"product-card\">\n					<div class=\"product-card-left\">\n						<div class=\"product-gallery-wrapper\">\n							<div class=\"slider-nav\">\n								<div class=\"slider-nav__item slider-nav__item--next\"><span class=\"icon icon-arrow\"></span></div>\n								<div class=\"slider-nav__item slider-nav__item--prev\"><span class=\"icon icon-arrow\"></span></div>\n							</div>\n							<a class=\"product-gallery-link\" href=\"#full-gallery\">\n								<ul class=\"product-gallery\">\n									[[Shop?&get=`getProductImages`&tpl=`tpl_productImages_420`]]\n								</ul>\n							</a>\n						</div>\n						<div class=\"product-gallery-wrapper\">\n							<ul class=\"product-gallery-nav\"> \n								[[Shop?&get=`getProductImages`&tpl=`tpl_productImages_90`]]\n							</ul>\n						</div>\n					</div>\n					<div class=\"product-card-right\">\n						<div class=\"product-card-header\">\n							<div class=\"heading-h1\">[*product_name*]</div>\n							<div class=\"product-card-info\">\n								[!if?&is=`[*manufacturer*]:empty`&else=`<div class=\"product-card-lbl\">[*manufacturer*]</div>`!]\n								<div class=\"product-card-vendor\">[#107#]: <span class=\"js_product_code\">[*product_code*]</span></div>\n							</div>\n							<!-- товара нет в наличии -->\n							[!if?&is=`[*product_availability*]:=:1`&else=`<div class=\"product-card-lbl\">[#129#]</div>`!]\n							<!-- товара нет в наличии -->\n						</div>\n						<div class=\"product-card-body\">\n							[!if?&is=`[*product_type*]:=:1`&then=`<form class=\"js_select_options\" method=\"post\"><!-- ЕСЛИ ТОВАР ВАРИАТИВНЫЙ-->\n							<input type=\"hidden\" name=\"parent_id\" value=\"[*product_id*]\">\n							<div class=\"selects-wrapper\">\n								[*option_values*]\n							</div>\n\n							<div class=\"form-row\">\n								<div class=\"form__item\">\n									<div class=\"comments-block\">\n										<div class=\"comments-block-title js_add-comment\">[#96#]</div>\n										<div class=\"comments-block-body\"> \n											<textarea class=\"form-textarea order-msg\" data-pcid=[*id*] name=\"comment\" cols=\"30\" rows=\"10\" placeholder=\"[#97#]\">[*product_cart_comment*]</textarea>\n											<button class=\"btn-save-msg js_save-msg\"><span class=\"icon icon-send\"></span></button>\n										</div>\n									</div>\n									<div class=\"comments-block comments-block--edit[!-if?&is=`[*product_cart_comment*]:empty`&else=` active`!]\">\n										<button class=\"btn-edit-msg js_edit-msg\"><span class=\"icon icon-pencil\"></span></button>\n										<div class=\"text edit-msg\">[*product_cart_comment*]</div>\n									</div>\n								</div>\n							</div>\n\n							<div class=\"product-card-footer\"> \n								<div class=\"total-wrapper\"> \n									<div class=\"title\">[#98#]\n										<div class=\"old-price\"[!if?&is=`[*prc_old*]:empty`&then=` style=\"display: none;\"`!]>\n											<span class=\"js_price_old\">[*prc_old*]&nbsp;</span>\n											<span class=\"currency\">[*sing*]</span>\n										</div>\n									</div>\n									<span class=\"total-sum js_price\">[*prc*]</span><span class=\"total-currency\">&nbsp;[*sing*]</span>\n								</div>\n								<div class=\"order__item-quantity js_quantity\">\n									<button class=\"btn-qvt btn-qvt--minus\" data-field=\"page_quantity\"><i class=\"icon icon-minus\" data-field=\"page_quantity\"></i></button>\n									<div class=\"order__item-quantity__input\">\n										<input class=\"js-changeAmount\" type=\"number\" name=\"page_quantity\" value=\"[*product_cart_quantity*]\" min=\"1\" max=\"99\" step=\"1\">\n									</div>\n									<button class=\"btn-qvt btn-qvt--plus\" data-field=\"page_quantity\"><i class=\"icon icon-plus\" data-field=\"page_quantity\"></i></button>\n								</div>\n								<div class=\"product-buttons\"> \n									<button class=\"btn btn-big btn-orange\" data-buy=\"[*id*]\" data-bought=\"[#94#]\" data-buy_string=\"[#35#]\" type=\"submit\">[#35#]</button>\n									<a class=\"btn btn-big btn-bordered js_call\" href=\"#call\">[#95#]</a>\n								</div>\n							</div>\n							</form>`&else=`<!-- ИНАЧЕ ГРУПОВОЙ-->\n							<form method=\"post\">\n								<div class=\"equipment-wrapper\">\n									<div class=\"slider-nav\">\n										<div class=\"slider-nav__item--next\"><span class=\"icon icon-arrow\"></span></div>\n									</div>\n									<div class=\"equipment-list\">\n										[!Shop?&get=`getOptionGroup`&pid=`[*product_id*]` &tpl=`tpl_getOptionGroup` &tplWrap=`tpl_getOptionGroup_wrap`!]\n									</div>\n								</div>\n								<div class=\"toggle-equipment\"> \n									<span class=\"show-equip\" data-unique=\"show\" style=\"display:none\">[#124#]</span>\n									<span class=\"hide-equip active\" data-unique=\"hide\">[#125#]</span>\n								</div>\n								<div class=\"form-row\">\n									<div class=\"form__item\">\n										<div class=\"comments-block\">\n											<div class=\"comments-block-title js_add-comment\">[#96#]</div>\n											<div class=\"comments-block-body\"> \n												<textarea class=\"form-textarea order-msg\" data-pcid=[*id*] name=\"comment\" cols=\"30\" rows=\"10\" placeholder=\"[#97#]\">[*product_cart_comment*]</textarea>\n												<button class=\"btn-save-msg js_save-msg\"><span class=\"icon icon-send\"></span></button>\n											</div>\n										</div>\n										<div class=\"comments-block comments-block--edit[!if?&is=`[*product_cart_comment*]:empty`&else=` active`!]\">\n											<button class=\"btn-edit-msg js_edit-msg\"><span class=\"icon icon-pencil\"></span></button>\n											<div class=\"text edit-msg\">[*product_cart_comment*]</div>\n										</div>\n									</div>\n								</div>\n\n								<div class=\"product-card-footer\"> \n									<div class=\"total-wrapper\"> \n										<div class=\"title\">[#98#]\n											<div class=\"old-price\"[!if?&is=`[*prc_old*]:empty`&then=` style=\"display: none;\"`!]>\n												<span class=\"js_price_old\">[*prc_old*]&nbsp;</span>\n												<span class=\"currency\">[*sing*]</span>\n											</div>\n											</div>\n										<span class=\"total-sum js_price\">[*prc*]&nbsp;</span><span class=\"total-currency\">&nbsp;[*sing*]</span>\n									</div>\n									<div class=\"order__item-quantity js_quantity\">\n										<button class=\"btn-qvt btn-qvt--minus\" data-field=\"page_quantity\"><i class=\"icon icon-minus\" data-field=\"page_quantity\"></i></button>\n										<div class=\"order__item-quantity__input\">\n											<input class=\"js-changeAmount\" type=\"number\" name=\"page_quantity\" value=\"[*product_cart_quantity*]\" min=\"1\" max=\"99\" step=\"1\">\n										</div>\n										<button class=\"btn-qvt btn-qvt--plus\" data-field=\"page_quantity\"><i class=\"icon icon-plus\" data-field=\"page_quantity\"></i></button>\n									</div>\n									<div class=\"product-buttons\"> \n										<button class=\"btn btn-big btn-orange\" data-buy=\"[*id*]\" data-bought=\"[#94#]\" data-buy_string=\"[#35#]\" type=\"submit\">[#35#]</button>\n										<a class=\"btn btn-big btn-bordered js_call\" href=\"#call\">[#95#]</a>\n									</div>\n								</div>\n							</form>`!]\n							<div class=\"product-card-tabs\">\n								<ul class=\"tabs-list\">\n									[!if?&is=`[*product_description*]:empty`&else=`<li class=\"tabs__item\"> <a class=\"tabs__link active\" href=\"#description\">[#99#]</a></li>`!]\n									[!if?&is=`[*charact*]:empty`&else=`<li class=\"tabs__item active\"> <a class=\"tabs__link\" href=\"#features\">[#100#]</a></li>`!]\n									<li class=\"tabs__item\"> <a class=\"tabs__link active\" href=\"#delivery\">[#101#]</a></li>\n								</ul>\n								<div class=\"tabs-wrapper\">\n									[!if?&is=`[*product_description*]:empty`&else=`<div class=\"tabs-block active\" id=\"description\">\n									<div class=\"content\">[*product_description*]</div>\n									</div>`!]\n									[!if?&is=`[*charact*]:empty`&else=`<div class=\"tabs-block\" id=\"features\">\n									<div class=\"content\"> \n										<div class=\"features-block\">\n											<!-- tpl_product_characteristic -->\n											[*charact*]\n										</div>\n									</div>\n									</div>`!]\n									<div class=\"tabs-block\" id=\"delivery\">\n										<div class=\"content\">\n											<div class=\"columns-box\">\n												[*content*]\n											</div>\n										</div>\n									</div>\n								</div>\n							</div>\n						</div>\n					</div>\n				</section>\n			</div>\n		</div>\n	</div>\n	[[if?&is=`[*product_recommender*]:empty`&else=`\n	<section class=\"section section-slider slider-first\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#102#]</div>\n			<div class=\"slider-nav\">\n				<div class=\"slider-nav__item slider-nav__item--prev\"><i class=\"icon icon-arrow\"></i></div>\n				<div class=\"slider-nav__item slider-nav__item--next\"><i class=\"icon icon-arrow\"></i></div>\n			</div>\n			<div class=\"product-page-slider\">\n				[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&onlyIds=`[*product_recommender*]`]]\n			</div>\n		</div>\n	</section>`]]\n	[[if?&is=`[*product_look_like*]:empty`&else=`\n	<section class=\"section section-slider slider-second\">\n		<div class=\"container\">\n			<div class=\"section-title section-title--before\">[#103#]</div>\n			<div class=\"slider-nav\">\n				<div class=\"slider-nav__item slider-nav__item--prev\"><i class=\"icon icon-arrow\"></i></div>\n				<div class=\"slider-nav__item slider-nav__item--next\"><i class=\"icon icon-arrow\"></i></div>\n			</div>\n			<div class=\"product-page-slider\">\n				[[Shop?&get=`getCatalog`&tpl=`tpl_main_slider`&onlyIds=`[*product_look_like*]`]]\n			</div>\n		</div>\n	</section>`]]\n	{{tpl_seo_block}}\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('22','Discount-single','','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main discount-page--single\">\n	<section class=\"page-top page-top--discount-single\" style=\"background-image: url(\'[*tv_img*]\')\">\n		<div class=\"container container-sp\">\n			<div class=\"breadcrumbs breadcrumbs--black\">\n				[[Breadcrumbs]]\n			</div>\n			<div class=\"caption\"> \n				<div class=\"heading-h1\">[*pagetitle*]</div>\n			</div>\n		</div>\n	</section>\n	<div class=\"container\">\n		<div class=\"sub-catalogue-wrapper\">\n			<div class=\"filters-wrapper\" id=\"filters\">\n				<div class=\"section-title section-title--before\">[#5#]</div>\n				<div class=\"filters-block\">\n					<div class=\"filters-block__row\">\n						<div class=\"labels-wrapper\">\n							[[Shop?&get=`getFilters`&tpl=`tpl_filters_del`&tplInner=`tpl_filtersInner_del`&del=`1`]]\n						</div>\n						<div class=\"filters-box\">\n							<div class=\"labels-wrapper\"></div><a class=\"btn btn-bordered btn-big btn-wide\" href=\"[~~[(site_start)]~~][*alias*]/\">[#76#]</a>\n						</div>\n					</div>\n					<div class=\"filters-block__row\">\n						<div class=\"filters-box\">\n							<ul class=\"filters-list active\">\n								<li class=\"filters-list__item no-margin\"><a class=\"filters-list-link[!if?&is=`[+only_available+]:=:1`&then=` active`!]\" data-filter=\"available=true\" href=\"[~~[*id*]~~]f/available=true/\"<span class=\"filters-list-lbl\">[#131#]</span></a></li>\n					</ul>\n			</div>\n		</div>\n		[!Shop?&get=`getFilterPriceSearch`&tpl=`tpl_filterPrice`!]\n		[[Shop?&get=`getFilters`&tpl=`tpl_filters`&tplInner=`tpl_filtersInner`]]\n	</div>\n	</div>\n<div class=\"products-list-wrapper\">\n	<div class=\"products-list-header\">\n		<div class=\"left-block\">\n			<div class=\"text-shown\">[#122#] <span class=\"amount\">[!if?&is=`[+maxitem+]:empty`&then=`0`&else=`[+maxitem+]`!]&nbsp;</span>[+prod_items+]</div><a class=\"btn btn-lg btn-orange js_show-filters\" href=\"#filters\">[#123#]</a>\n		</div>\n		<div class=\"selects-block\">\n			[!Shop?\n			&get=`getOption`\n			&tpl=`tpl_getOption_single`\n			&catch=`sort`\n			&select=`active`\n			&options=`price-up=[#79#],price-down=[#78#],new-up=[#80#],first-hit=[#81#]`\n			!]\n		</div>\n	</div>\n	<ul class=\"product-list\">\n		[!Shop?\n		&get=`getCatalog`\n		&tpl=`tpl_catalog`\n		&tplEmpty=`tpl_catalogEmpty`\n		&onlyIds=`[*discounts_products*]`\n		&pagin=`1`&limit=`24`\n		!]\n		<div class=\"products-list-footer\"> \n			<div class=\"pagination-wrapper\">\n				<ul class=\"pagination-list\">\n					[!Shop?\n					&get=`getPaginate`\n					&tpl=`tpl_paginate`\n					&tpl_current=`tpl_paginateCurrent`\n					&tpl_prev=`tpl_paginatePrev`\n					&tpl_next=`tpl_paginateNext`\n					&tpl_indent=`tpl_paginateIndent`\n					!]\n				</ul>\n			</div>\n		</div>\n		</div>\n</div>\n</div>\n[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('24','Content Page','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main news-single-page\">\n	<section class=\"page-top page-top--news-single\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-article\">\n		<div class=\"container\"> \n			<div class=\"article-header\">\n				<div class=\"article-header-img\"><img src=\"[[R?&img=`[*tv_img*]`&opt=`w=1170&h=878&far=1`]]\" alt=\"[*pagetitle*]\" title=\"[*pagetitle*]\"></div>\n				<div class=\"article-header-text\">[*description*]</div>\n			</div>\n			<article class=\"article-body\">\n				<div class=\"content\">\n					[*content*]\n				</div>\n			</article>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('6','Cart','<strong>1.0</strong> Cart template','0','3','','0','{{HEAD}}\n{{HEADER}}\n<div class=\"clearfix\"></div>\n<div class=\"cart-wraper page-top\">\n    <div class=\"container\">\n        <section class=\"breadcrumb\">\n            <ul class=\"breadcrumb-list\">\n                <li><a href=\"/[(lang)]/\">Главная</a> <span class=\"divider\">-</span></li>\n                <li class=\"active\">[*pagetitle*]</li>\n            </ul>\n        </section>\n        <div class=\"page-title\">\n            <p>[*pagetitle*]</p>\n        </div>\n        [!Shop?&get=`getCart`&tpl=`tpl_cart`&tplCharac=`tpl_сharacteristic`!]\n        <div class=\"total-block\">\n            <p class=\"quantity-order-title title-total-prise\">Итого к оплате: <span class=\"js_result result-cart\"></span>\n            </p>\n            <a href=\"[~~19~~]\" class=\"btn-black\">Оформить заказ</a>\n        </div>\n        <div class=\"bottom-block-cart\">\n            <a hpef=\"#\" onclick=\"javascript:history.back(); return false;\" class=\"more trans\">Продолжить покупки</a>\n            <div class=\"clear-cart trans\"><span class=\"js_remove-cart-all\"><img src=\"/images/trash.png\" alt=\"Очистить корзину\"></span>Очистить корзину</div>\n        </div>\n    </div>\n</div>\n</div>\n</div>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('7','Checkout','<strong>1.0</strong> Checkout template','0','3','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main checkout\">\n	<section class=\"page-top\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"page-wrapper\">\n		<div class=\"container\">\n			<div class=\"checkout-wrapper\">\n				<div class=\"checkout-col checkout-col--left\">\n					<section class=\"order-info\">\n						<form class=\"js_validate\" action=\"[~~[*id*]~~]\" method=\"post\">\n							<input type=\"hidden\" name=\"checkout[token]\" value=\"[!Shop?&get=`setToken`!]\">\n							<input type=\"hidden\" name=\"checkout[price][cart]\" value=\"[!Shop?&get=`getCartTotal` &delimiter=``!]\">\n							<div class=\"info-header\"> \n								<div class=\"section-title text-black\">[#108#]</div>\n							</div>\n							<div class=\"info-wrapper\">\n								<div class=\"form-row\">\n									<div class=\"form__item\">\n										<label class=\"form-label\" for=\"phone-number\">[#83#] *</label>\n										<input class=\"form-input\" id=\"phone-number\" type=\"tel\" inputmode=\"tel\" name=\"checkout[user][phone]\" required data-validate=\"phone\">\n										<div class=\"text-error\">[#52#]</div>\n									</div>\n								</div>\n								<div class=\"form-row\">\n									<div class=\"form__item\">\n										<label class=\"form-label\" for=\"name\">[#32#] *</label>\n										<input class=\"form-input\" id=\"name\" type=\"text\" inputmode=\"text\" name=\"checkout[user][name]\" required>\n										<div class=\"text-error\">[#50#]</div>\n									</div>\n								</div>\n								<div class=\"form-row\">\n									<div class=\"form__item\">\n										<label class=\"form-label\" for=\"email\">Email</label>\n										<input class=\"form-input\" id=\"email\" type=\"email\" inputmode=\"email\" name=\"checkout[user][email]\">\n									</div>\n								</div>\n								<div class=\"form-row\">\n									<div class=\"form__item\">\n										<label class=\"form-label\" for=\"area\">[#109#] *</label>\n										<select class=\"js_select-checkout\" name=\"checkout[user][area]\" id=\"area\" data-validate=\"select2\" required>\n											<option value=\"default\" selected>[#85#]</option>\n											[[ <option>[#85#]</option> ]]\n											[!Shop?&get=`getArias`&tpl=`tpl_aria_option`!]\n										</select>\n										<div class=\"text-error\">[#86#]</div>\n									</div>\n								</div>\n								<div class=\"form-row form-row--group\">\n									<div class=\"form-label\">[#110#]</div>\n									<div class=\"form__item\">\n										<input class=\"input-checkbox js_services\" name=\"checkout[delivery]\" value=\"1\" data-cost=\"[(as_delivery)]\" data-yes=\"[#114#]\" data-no=\"[#117#]\" id=\"delivery\" type=\"checkbox\">\n										<label class=\"form-checkbox\" for=\"delivery\">[#111#]</label>\n									</div>\n									<div class=\"form__item\">\n										<input class=\"input-checkbox js_services\" name=\"checkout[install]\" value=\"1\" data-cost=\"[(as_installation)]\" data-yes=\"[#114#]\" data-no=\"[#117#]\" id=\"install\" type=\"checkbox\">\n										<label class=\"form-checkbox\" for=\"install\">[#112#]</label>\n									</div>\n								</div>\n							</div>\n							<div class=\"form-row form-row--confirm\">\n								<button class=\"btn btn-orange btn-big js_checkout_submit\" type=\"submit\">[#113#]</button>\n							</div>\n						</form>\n					</section>\n				</div>\n				<div class=\"checkout-col checkout-col--right\">\n					<div class=\"order-wrapper\">\n						<div class=\"order-header\">\n							<div class=\"section-title text-green\">\n								[#115#] <span class=\"count js_count\">[!Shop?&get=`getCartCount`!] </span>\n								<span class=\"js_declination\">[!Shop?&get=`getCartCountDeclination`!] </span>\n							</div>\n							<div class=\"order-items-switch\"></div>\n						</div>\n						<ul class=\"order-body js_cart_list\">\n							[!Shop?&get=`getCart`&tpl=`tpl_cart`&tplCharac=`tpl_cart_characteristics`!]\n						</ul>\n						<div class=\"order-footer\">\n							<div class=\"order-check-wrapper\">\n								<div class=\"order-check__item order-check__item--delivery\">[#111#]\n									<div class=\"order-check-status js_delivery\">[#117#]</div>\n								</div>\n								<div class=\"order-check__item order-check__item--install\">[#112#]\n									<div class=\"order-check-status js_install\">[#117#]</div>\n								</div>\n							</div>\n							<div class=\"order-footer__total\">[#105#]\n								<div class=\"total-wrapper\"><span class=\"total-sum js_total js_total_checkout\">[!Shop?&get=`getCartTotal`!]</span><span class=\"total-currency\"> [*sing*]</span></div>\n							</div>\n						</div>\n					</div>\n				</div>\n				<div class=\"checkout-col checkout-col--content\">\n					<div class=\"container\"> \n						<div class=\"content checkout-content\">\n							<section class=\"how-to-buy\">\n								<div class=\"heading-h4\">[*introtext*]</div>\n								[*content*]\n							</section>\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('23','Site map','','0','7','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main thank-page\">\n	<section class=\"page-top page-top--no-bg\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs breadcrumbs--black\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"container\">\n		<section class=\"content\">\n			<ul>\n				[[Shop?&get=`getHtmlSitemap`&type=`resources`&exclude=`31`]]\n				[[Shop?&get=`getHtmlSitemap`&type=`products`]]\n				[[Shop?&get=`getHtmlSitemap`&type=`news`]]\n				[[Shop?&get=`getHtmlSitemap`&type=`disconts`]]\n			</ul>\n		</section>\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('8','ThanksForOrder','<strong>1.0</strong> Thanks for your order template','0','3','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main thank-page\">\n	<section class=\"page-top page-top--no-bg\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1\">[*introtext*]</div>\n			<div class=\"breadcrumbs breadcrumbs--black\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"container\">\n		<div class=\"col-md-6 col-md-offset-3 col-xs-12\">\n			<section class=\"section section-thank\">\n				<div class=\"thank-img\"><img src=\"[*tv_img*]\" alt=\"[*pagetitle*]\" title=\"[*pagetitle*]\"></div>\n				<div class=\"thank-title\">[*longtitle*] <span class=\"order-num\">[*order_num*]</span></div>\n				<div class=\"thank-text\">\n					[*content*]\n				</div>\n				<div class=\"btn btn-bordered btn-sm\">[*description*]</div>\n			</section>\n		</div>\n	</div>\n	<div class=\"container\">\n		[[Shop? &get=`getDiscounts` &tpl=`tpl_discount` &where=`AND thanks_at = \'1\'`]]\n	</div>\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('21','Search','','0','1','','0','{{HEAD}}\n{{HEADER}}\n[!Shop?\n&get=`searchProduct`\n&tpl=`tpl_catalog`\n&tplEmpty=`tpl_catalogEmpty`\n!]\n<main class=\"main search-result\">\n	<section class=\"page-top page-top--sub-catalogue\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"container\"> \n		<div class=\"result-title\">[#118#] <span>\"[+searched+]\" </span>[#119#]: </div>\n	</div>\n	<div class=\"container\">\n		<div class=\"sub-catalogue-wrapper\">\n			<div class=\"filters-wrapper\" id=\"filters\">\n				<div class=\"section-title section-title--before\">[#5#]</div>\n				<div class=\"filters-block\">\n					<div class=\"filters-block__row\">\n						<div class=\"labels-wrapper\">\n							[!Shop?&get=`getFilters`&tpl=`tpl_filters_del`&tplInner=`tpl_filtersInner_del`&del=`1`!]\n						</div>\n						<div class=\"filters-box\">\n							<div class=\"labels-wrapper\"></div><a class=\"btn btn-bordered btn-big btn-wide\" href=\"[~~[*id*]~~]?search=[+searched+]/\">[#76#]</a>\n						</div>\n					</div>\n					<div class=\"filters-block__row\">\n						<div class=\"filters-box\">\n							<ul class=\"filters-list active\">\n								<li class=\"filters-list__item no-margin\"><a class=\"filters-list-link[!if?&is=`[+only_available+]:=:1`&then=` active`!]\" data-filter=\"available=true\" href=\"[~~[*id*]~~]f/available=true/\"<span class=\"filters-list-lbl\">Показывать только в наличии</span></a></li>\n							</ul>\n						</div>\n					</div>\n					[!Shop?&get=`getFilterPriceSearch`&tpl=`tpl_filterPrice`!]\n		<div class=\"filters-block__row\">\n						<div class=\"filters-box\">\n							<div class=\"filters-title--no-active\">[#120#]\n								<ul class=\"filters-list active\">\n									<li class=\"filters-list__item\">\n										<a class=\"filters-list-link js_get_all [!if?&is=`[+filter_cat_search_def+]:=:1`&then=`active`!]\" href=\"[~~[*id*]~~]?search=[+searched+]/\">\n											<span class=\"filters-list-lbl\">[#121#]&nbsp;<span class=\"amount\">([+maxitem+])</span></span>\n										</a>\n									</li>\n									[+filter_cat_search+]\n								</ul>\n							</div>\n						</div>\n					</div>\n					[!Shop?&get=`getFilters`&tpl=`tpl_filters`&tplInner=`tpl_filtersInner`!]\n				</div>\n			</div>\n			<div class=\"products-list-wrapper\">\n				<div class=\"products-list-header\">\n					<div class=\"left-block\">\n						<div class=\"text-shown\">[#122#] <span class=\"amount\">[!if?&is=`[+maxitem+]:!empty`&else=`0`&then=`[+maxitem+]`!]&nbsp;</span>[+prod_items+]</div><a class=\"btn btn-lg btn-orange js_show-filters\" href=\"#filters\">[#123#]</a>\n					</div>\n					<div class=\"selects-block\">\n						[!Shop?\n						&get=`getOption`\n						&tpl=`tpl_getOption_single`\n						&catch=`sort`\n						&select=`active`\n						&options=`price-up=[#79#],price-down=[#78#],new-up=[#80#],first-hit=[#81#]`\n						!]\n					</div>\n				</div>\n				<ul class=\"product-list\">\n					[+search+]\n				</ul>\n				<div class=\"products-list-footer\"> \n					<div class=\"pagination-wrapper\">\n						<ul class=\"pagination-list\">\n							[!Shop?\n							&get=`getPaginate`\n							&tpl=`tpl_paginate`\n							&tpl_current=`tpl_paginateCurrent`\n							&tpl_prev=`tpl_paginatePrev`\n							&tpl_next=`tpl_paginateNext`\n							&tpl_indent=`tpl_paginateIndent`\n							!]\n						</ul>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n	{{tpl_seo_block}}\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('16','Blog','','0','5','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main news-all-page\">\n	<section class=\"page-top page-top--news-all\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<section class=\"section section-news\">\n		<div class=\"container\">\n			<div class=\"news-wrapper\">\n				[[Shop? &get=`getNews` &tpl=`tpl_news` &limit=`12`&pagin=`1`]]\n			</div>\n			<div class=\"pagination-wrapper\">\n				<ul class=\"pagination-list\">\n					[!Shop?\n					&get=`getPaginate`\n					&tpl=`tpl_paginate`\n					&tpl_current=`tpl_paginateCurrent`\n					&tpl_prev=`tpl_paginatePrev`\n					&tpl_next=`tpl_paginateNext`\n					&tpl_indent=`tpl_paginateIndent`\n					!]\n				</ul>\n			</div>\n		</div>\n	</section>\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('18','Catalog_entrance_doors','<strong>1.0</strong> Catalog list template','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main catalogue\">\n	<section class=\"page-top page-top--interior-doors\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"section-wrapper section-wrapper--first\">\n		<div class=\"container\">\n			<section class=\"section-appointment\">\n				<div class=\"section-title section-title--white section-title--before\">[*longtitle*]</div>\n			</section>\n			<div class=\"door-types-wrapper\">\n				<ul class=\"door-types-list\">\n					[[Shop?&get=`getContents`&did=`[*id*]`&tpl=`tpl_door_types`&tvs=`tv_img`]]\n				</ul>\n			</div>\n		</div>\n	</div>\n	{{tpl_seo_block}}\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('19','Catalog_gates','<strong>1.0</strong> Catalog list template','0','2','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main catalogue\">\n	<section class=\"page-top page-top--interior-doors\" style=\"background-image: url(\'[*tv_bg*]\');\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1 text-white\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"section-wrapper section-wrapper--first\">\n		<div class=\"container\">\n			<section class=\"section-appointment\">\n				<div class=\"section-title section-title--white section-title--before\">[*longtitle*]</div>\n				<div class=\"appointment-wrapper\">\n					<ul class=\"appointment-list\">\n						[[Shop?&get=`getContents`&did=`[*id*]`&tpl=`tpl_appointment`&tvs=`tv_img`]]\n					</ul>\n				</div>\n			</section>\n		</div>\n	</div>\n	{{tpl_seo_block}}\n	[[Shop?&get=`getContent`&did=`[(questions)]`&tpl=`tpl_questions`]]\n</main>\n{{FOOTER}}','0','1');

INSERT INTO `modx_site_templates` VALUES ('20','Success send','','0','1','','0','{{HEAD}}\n{{HEADER}}\n<main class=\"main thank-page\">\n	<section class=\"page-top page-top--no-bg\">\n		<div class=\"container container-sp\">\n			<div class=\"heading-h1\">[*pagetitle*]</div>\n			<div class=\"breadcrumbs breadcrumbs--black\">\n				[[Breadcrumbs]]\n			</div>\n		</div>\n	</section>\n	<div class=\"container\">\n		<div class=\"col-md-6 col-md-offset-3 col-xs-12\">\n			<section class=\"section section-success\">\n				<div class=\"thank-img\"><img src=\"[*tv_img*]\" alt=\"[*pagetitle*]\" title=\"[*pagetitle*]\"></div>\n				<div class=\"thank-title\">[*longtitle*]</div>\n				<div class=\"btn btn-bordered btn-sm\"><a href=\"[~~[(site_start)]~~]\">[*description*]</a></div>\n			</section>\n		</div>\n	</div>\n	<div class=\"container\">\n		[[Shop? &get=`getDiscounts` &tpl=`tpl_discount` &where=`AND success_at = \'1\'`]]\n	</div>\n</main>\n{{FOOTER}}','0','1');


# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_tmplvar_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for template variable access permissions.';

#
# Dumping data for table `modx_site_tmplvar_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_contentvalues`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_tmplvar_contentvalues`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL DEFAULT '0' COMMENT 'Site Content Id',
  `value` mediumtext,
  PRIMARY KEY (`id`),
  KEY `idx_tmplvarid` (`tmplvarid`),
  KEY `idx_id` (`contentid`),
  FULLTEXT KEY `value_ft_idx` (`value`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COMMENT='Site Template Variables Content Values Link Table';

#
# Dumping data for table `modx_site_tmplvar_contentvalues`
#

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('1','10','33','/images/questions-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('2','24','1','34');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('3','10','39','/images/service-img-1.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('4','10','40','/images/service-img-2.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('5','25','26','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('7','27','5','Компанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%\nКомпанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%\nКомпанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%\nКомпанія «Гарант» продає вхідні та міжкімнатні двері 10 років, понад 1000 моделей, що дозволяє нам бути впевненими в якості своєї продукції на 100%');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('8','26','5','Компания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%\nКомпания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%\nКомпания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%\nКомпания « Гарант» продает входные и межкомнатные двери 10лет, более 1000 моделей, что позволяет нам быть уверенными в качестве своей продукции на 100%');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('53','31','27','/images/pay-img.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('10','10','27','/images/delivery-img.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('52','31','28','/images/delivery-img.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('12','10','28','/images/pay-img.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('13','25','29','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('14','25','44','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('15','25','30','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('16','25','25','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('17','28','1','proizvoditel');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('18','24','22','53');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('19','29','22','54');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('20','30','22','55');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('21','8','22','<p>Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.<br /> Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('22','17','22','<p>Lorem Ipsum - це текст- \"риба\", часто використовуваний у пресі та веб-дизайні. Lorem Ipsum є стандартною \"рибою\" для текстів на латиниці з початку XVI століття. У той час якийсь безіменний друкар створив велику колекцію розмірів і форм шрифтів, використовуючи Lorem Ipsum для роздруківки зразків. Lorem Ipsum не тільки успішно пережив без помітних змін п\'ять століть, але й прижилася в електронному дизайн. Його популяризації в новий час послужили публікація листів Letraset із зразками Lorem Ipsum в 60-х роках і, в більш недавні часи, програми електронної верстки типу Aldus PageMaker, в шаблонах яких використовується Lorem Ipsum.<br />Lorem Ipsum - це текст- \"риба\", часто використовуваний у пресі та веб-дизайні. Lorem Ipsum є стандартною \"рибою\" для текстів на латиниці з початку XVI століття. У той час якийсь безіменний друкар створив велику колекцію розмірів і форм шрифтів, використовуючи Lorem Ipsum для роздруківки зразків. Lorem Ipsum не тільки успішно пережив без помітних змін п\'ять століть, але й прижилася в електронному дизайн. Його популяризації в новий час послужили публікація листів Letraset із зразками Lorem Ipsum в 60-х роках і, в більш недавні часи, програми електронної верстки типу Aldus PageMaker, в шаблонах яких використовується Lorem Ipsum.</p>');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('23','25','54','/images/bg-materials.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('24','25','53','/images/bg-appointment.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('25','25','55','/images/bg-types.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('26','10','56','/images/catalogue-images/living-room.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('27','10','57','/images/catalogue-images/bathroom.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('28','10','64','/images/materials/mat-shpon.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('29','10','65','/images/materials/mat-eco-shpon.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('30','10','72','/images/catalogue-images/door-type-1.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('31','10','73','/images/catalogue-images/door-type-2.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('32','10','58','/images/catalogue-images/child-room.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('33','10','59','/images/catalogue-images/corridor.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('34','10','60','/images/catalogue-images/entrance-hall.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('35','10','61','/images/catalogue-images/bedroom.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('36','10','62','/images/catalogue-images/kitchen.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('37','10','63','/images/catalogue-images/office-room.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('38','10','66','/images/materials/mat-pvx.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('39','10','67','/images/materials/mat-mdf.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('40','10','68','/images/materials/mat-pine.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('41','10','69','/images/materials/mat-oak.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('42','10','70','/images/materials/mat-ash.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('43','10','55','/images/materials/mat-nut.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('44','10','71','/images/materials/mat-nut.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('45','10','51','/images/catalogue-images/doors-1.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('46','10','52','/images/catalogue-images/doors-2.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('47','10','46','/images/catalogue-images/gates-1.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('48','10','47','/images/catalogue-images/gates-2.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('49','10','48','/images/catalogue-images/gates-3.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('50','10','49','/images/catalogue-images/gates-4.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('51','10','50','/images/catalogue-images/gates-5.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('54','10','74','/images/email.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('55','10','20','/images/email.svg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('56','28','4','proizvoditel');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('57','10','77','assets/images/slide1.jpeg');

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('60','10','80','/images/catalogue-images/door-type-2.svg');


# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_templates`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_tmplvar_templates`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Site Template Variables Templates Link Table';

#
# Dumping data for table `modx_site_tmplvar_templates`
#

INSERT INTO `modx_site_tmplvar_templates` VALUES ('24','11','15');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','3','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','9','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('28','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','4','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','8','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','11','14');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','14','14');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','14','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','13','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','10','17');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','14','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','14','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','13','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','10','16');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','10','14');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','10','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','10','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','11','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','10','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','10','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('28','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','11','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','11','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','11','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','13','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','11','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','11','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','11','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','13','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','11','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','11','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','13','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','13','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','13','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','13','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','13','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','13','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','13','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','14','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','11','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','10','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','10','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','11','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','10','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','10','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','1','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','11','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','16','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','10','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','13','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','11','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('27','11','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','14','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','6','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('24','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','14','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','14','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','14','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','14','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','14','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','14','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('31','14','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('26','11','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','15','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','15','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','15','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','15','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','15','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','15','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','15','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','15','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','15','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','15','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','15','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','15','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','16','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('30','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','16','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','16','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','16','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','16','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','16','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','16','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','16','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','16','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','16','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','16','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','16','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','8','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','17','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','17','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','17','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','17','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','17','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','17','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','17','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','17','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','17','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','17','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','17','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','17','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','7','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('13','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','13','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','17','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','14','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','10','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','15','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('29','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('24','3','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('5','2','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','4','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','18','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','18','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','19','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','19','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','20','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','20','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','20','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','20','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','14','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','20','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','20','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','20','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','20','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','20','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','20','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','20','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','20','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','20','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','21','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','21','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','21','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','21','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','21','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','21','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','21','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','21','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','21','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','21','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','21','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','21','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','21','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','5','0');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','22','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','22','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','22','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','22','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','22','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','22','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','22','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','22','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','22','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','22','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','22','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','22','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','23','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','23','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','23','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','23','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','23','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','23','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','23','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','23','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','23','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','23','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','23','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','23','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('10','24','13');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','24','12');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','24','11');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('19','24','10');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('11','24','9');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('22','24','8');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('9','24','7');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('15','24','6');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('7','24','5');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('8','24','4');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('17','24','3');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('12','24','2');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('6','24','1');

INSERT INTO `modx_site_tmplvar_templates` VALUES ('25','24','0');


# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvars`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_site_tmplvars`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_site_tmplvars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL DEFAULT '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '' COMMENT 'Display Control',
  `display_params` text COMMENT 'Display Control Properties',
  `default_text` text,
  PRIMARY KEY (`id`),
  KEY `indx_rank` (`rank`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COMMENT='Site Template Variables';

#
# Dumping data for table `modx_site_tmplvars`
#

INSERT INTO `modx_site_tmplvars` VALUES ('1','text','seo_title_ru','SEO TITLE','Title tags technically called title elements define the title of a document.','0','4','0','','0','','','[*pagetitle*] | [(site_name)]');

INSERT INTO `modx_site_tmplvars` VALUES ('2','text','seo_title_ua','SEO TITLE','Title tags technically called title elements define the title of a document.','0','4','0','','0','','','[*pagetitle*] | [(site_name)]');

INSERT INTO `modx_site_tmplvars` VALUES ('25','image','tv_bg','Background image','Main picture for a resource.','0','5','0','','0','','','/images/page-top-bg.jpeg');

INSERT INTO `modx_site_tmplvars` VALUES ('5','text','tv_img_alt_ru','ALT for General image','Alt attribute for the main image.','0','5','0','','0','','','[*pagetitle*]');

INSERT INTO `modx_site_tmplvars` VALUES ('6','text','seo_canonical_ru','SEO CANONICAL','Canonical tags—technically called title elements—define the canonical of a document.','0','4','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('7','textareamini','seo_description_ua','SEO DESCRIPTION','Description tags—technically called title elements—define the description of a document.','0','4','0','','0','','','[*description_ua*]');

INSERT INTO `modx_site_tmplvars` VALUES ('8','richtext','seo_content_ru','SEO CONTENT','RTE SEO for the new content entries','0','4','0','','0','RichText','&w=383px&h=450px&edt=TinyMCE','');

INSERT INTO `modx_site_tmplvars` VALUES ('9','text','seo_keywords_ru','SEO KEYWORDS','Keywords tags—technically called title elements—define the keywords of a document.','0','4','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('10','image','tv_img','General image','Main picture for a resource.','0','5','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('11','dropdown','seo_robots_ru','SEO ROBOTS','Robots tags—technically called title elements—define the robots of a document.','0','4','0','index, follow==index,follow||noindex, nofollow==noindex,nofollow||Disable==0','0','','','noindex,nofollow');

INSERT INTO `modx_site_tmplvars` VALUES ('12','text','seo_canonical_ua','SEO CANONICAL','Canonical tags—technically called title elements—define the canonical of a document.','0','4','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('13','text','tv_img_alt_ua','ALT for General image','Alt attribute for the main image.','0','5','0','','0','','','[*pagetitle*]');

INSERT INTO `modx_site_tmplvars` VALUES ('15','textareamini','seo_description_ru','SEO DESCRIPTION','Description tags—technically called title elements—define the description of a document.','0','4','0','','0','','','[*description_ru*]');

INSERT INTO `modx_site_tmplvars` VALUES ('17','richtext','seo_content_ua','SEO CONTENT','RTE SEO for the new content entries','0','4','0','','0','RichText','&w=383px&h=450px&edt=TinyMCE','');

INSERT INTO `modx_site_tmplvars` VALUES ('24','text','tv_child_id_1','ID дочернего ресурса 1','','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('19','dropdown','seo_robots_ua','SEO ROBOTS','Robots tags—technically called title elements—define the robots of a document.','0','4','0','index, follow==index,follow||noindex, nofollow==noindex,nofollow||Disable==0','0','','','noindex,nofollow');

INSERT INTO `modx_site_tmplvars` VALUES ('27','textarea','tv_content_ua','Набор строк','for getParseString','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('28','text','tv_manufacturer_filter','URL фильтра \"Производитель\"','filter_url','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('22','text','seo_keywords_ua','SEO KEYWORDS','Keywords tags—technically called title elements—define the keywords of a document.','0','4','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('26','textarea','tv_content_ru','Набор строк','for getParseString','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('29','text','tv_child_id_2','ID дочернего ресурса 2','','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('30','text','tv_child_id_3','ID дочернего ресурса 3','','0','0','0','','0','','','');

INSERT INTO `modx_site_tmplvars` VALUES ('31','image','tv_img2','Second image','Main picture for a resource.','0','5','0','','0','','','');


# --------------------------------------------------------

#
# Table structure for table `modx_system_eventnames`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_system_eventnames`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_system_eventnames` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `service` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'System Service number',
  `groupname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1036 DEFAULT CHARSET=utf8mb4 COMMENT='System Event Names.';

#
# Dumping data for table `modx_system_eventnames`
#

INSERT INTO `modx_system_eventnames` VALUES ('1','OnDocPublished','5','');

INSERT INTO `modx_system_eventnames` VALUES ('2','OnDocUnPublished','5','');

INSERT INTO `modx_system_eventnames` VALUES ('3','OnWebPagePrerender','5','');

INSERT INTO `modx_system_eventnames` VALUES ('4','OnWebLogin','3','');

INSERT INTO `modx_system_eventnames` VALUES ('5','OnBeforeWebLogout','3','');

INSERT INTO `modx_system_eventnames` VALUES ('6','OnWebLogout','3','');

INSERT INTO `modx_system_eventnames` VALUES ('7','OnWebSaveUser','3','');

INSERT INTO `modx_system_eventnames` VALUES ('8','OnWebDeleteUser','3','');

INSERT INTO `modx_system_eventnames` VALUES ('9','OnWebChangePassword','3','');

INSERT INTO `modx_system_eventnames` VALUES ('10','OnWebCreateGroup','3','');

INSERT INTO `modx_system_eventnames` VALUES ('11','OnManagerLogin','2','');

INSERT INTO `modx_system_eventnames` VALUES ('12','OnBeforeManagerLogout','2','');

INSERT INTO `modx_system_eventnames` VALUES ('13','OnManagerLogout','2','');

INSERT INTO `modx_system_eventnames` VALUES ('14','OnManagerSaveUser','2','');

INSERT INTO `modx_system_eventnames` VALUES ('15','OnManagerDeleteUser','2','');

INSERT INTO `modx_system_eventnames` VALUES ('16','OnManagerChangePassword','2','');

INSERT INTO `modx_system_eventnames` VALUES ('17','OnManagerCreateGroup','2','');

INSERT INTO `modx_system_eventnames` VALUES ('18','OnBeforeCacheUpdate','4','');

INSERT INTO `modx_system_eventnames` VALUES ('19','OnCacheUpdate','4','');

INSERT INTO `modx_system_eventnames` VALUES ('107','OnMakePageCacheKey','4','');

INSERT INTO `modx_system_eventnames` VALUES ('20','OnLoadWebPageCache','4','');

INSERT INTO `modx_system_eventnames` VALUES ('21','OnBeforeSaveWebPageCache','4','');

INSERT INTO `modx_system_eventnames` VALUES ('22','OnChunkFormPrerender','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('23','OnChunkFormRender','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('24','OnBeforeChunkFormSave','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('25','OnChunkFormSave','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('26','OnBeforeChunkFormDelete','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('27','OnChunkFormDelete','1','Chunks');

INSERT INTO `modx_system_eventnames` VALUES ('28','OnDocFormPrerender','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('29','OnDocFormRender','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('30','OnBeforeDocFormSave','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('31','OnDocFormSave','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('32','OnBeforeDocFormDelete','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('33','OnDocFormDelete','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('1033','OnDocFormUnDelete','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('1034','onBeforeMoveDocument','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('1035','onAfterMoveDocument','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('34','OnPluginFormPrerender','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('35','OnPluginFormRender','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('36','OnBeforePluginFormSave','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('37','OnPluginFormSave','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('38','OnBeforePluginFormDelete','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('39','OnPluginFormDelete','1','Plugins');

INSERT INTO `modx_system_eventnames` VALUES ('40','OnSnipFormPrerender','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('41','OnSnipFormRender','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('42','OnBeforeSnipFormSave','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('43','OnSnipFormSave','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('44','OnBeforeSnipFormDelete','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('45','OnSnipFormDelete','1','Snippets');

INSERT INTO `modx_system_eventnames` VALUES ('46','OnTempFormPrerender','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('47','OnTempFormRender','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('48','OnBeforeTempFormSave','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('49','OnTempFormSave','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('50','OnBeforeTempFormDelete','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('51','OnTempFormDelete','1','Templates');

INSERT INTO `modx_system_eventnames` VALUES ('52','OnTVFormPrerender','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('53','OnTVFormRender','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('54','OnBeforeTVFormSave','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('55','OnTVFormSave','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('56','OnBeforeTVFormDelete','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('57','OnTVFormDelete','1','Template Variables');

INSERT INTO `modx_system_eventnames` VALUES ('58','OnUserFormPrerender','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('59','OnUserFormRender','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('60','OnBeforeUserFormSave','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('61','OnUserFormSave','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('62','OnBeforeUserFormDelete','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('63','OnUserFormDelete','1','Users');

INSERT INTO `modx_system_eventnames` VALUES ('64','OnWUsrFormPrerender','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('65','OnWUsrFormRender','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('66','OnBeforeWUsrFormSave','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('67','OnWUsrFormSave','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('68','OnBeforeWUsrFormDelete','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('69','OnWUsrFormDelete','1','Web Users');

INSERT INTO `modx_system_eventnames` VALUES ('70','OnSiteRefresh','1','');

INSERT INTO `modx_system_eventnames` VALUES ('71','OnFileManagerUpload','1','');

INSERT INTO `modx_system_eventnames` VALUES ('72','OnModFormPrerender','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('73','OnModFormRender','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('74','OnBeforeModFormDelete','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('75','OnModFormDelete','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('76','OnBeforeModFormSave','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('77','OnModFormSave','1','Modules');

INSERT INTO `modx_system_eventnames` VALUES ('78','OnBeforeWebLogin','3','');

INSERT INTO `modx_system_eventnames` VALUES ('79','OnWebAuthentication','3','');

INSERT INTO `modx_system_eventnames` VALUES ('80','OnBeforeManagerLogin','2','');

INSERT INTO `modx_system_eventnames` VALUES ('81','OnManagerAuthentication','2','');

INSERT INTO `modx_system_eventnames` VALUES ('82','OnSiteSettingsRender','1','System Settings');

INSERT INTO `modx_system_eventnames` VALUES ('83','OnFriendlyURLSettingsRender','1','System Settings');

INSERT INTO `modx_system_eventnames` VALUES ('84','OnUserSettingsRender','1','System Settings');

INSERT INTO `modx_system_eventnames` VALUES ('85','OnInterfaceSettingsRender','1','System Settings');

INSERT INTO `modx_system_eventnames` VALUES ('86','OnMiscSettingsRender','1','System Settings');

INSERT INTO `modx_system_eventnames` VALUES ('87','OnRichTextEditorRegister','1','RichText Editor');

INSERT INTO `modx_system_eventnames` VALUES ('88','OnRichTextEditorInit','1','RichText Editor');

INSERT INTO `modx_system_eventnames` VALUES ('89','OnManagerPageInit','2','');

INSERT INTO `modx_system_eventnames` VALUES ('90','OnWebPageInit','5','');

INSERT INTO `modx_system_eventnames` VALUES ('101','OnLoadDocumentObject','5','');

INSERT INTO `modx_system_eventnames` VALUES ('104','OnBeforeLoadDocumentObject','5','');

INSERT INTO `modx_system_eventnames` VALUES ('105','OnAfterLoadDocumentObject','5','');

INSERT INTO `modx_system_eventnames` VALUES ('91','OnLoadWebDocument','5','');

INSERT INTO `modx_system_eventnames` VALUES ('92','OnParseDocument','5','');

INSERT INTO `modx_system_eventnames` VALUES ('106','OnParseProperties','5','');

INSERT INTO `modx_system_eventnames` VALUES ('93','OnManagerLoginFormRender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('94','OnWebPageComplete','5','');

INSERT INTO `modx_system_eventnames` VALUES ('95','OnLogPageHit','5','');

INSERT INTO `modx_system_eventnames` VALUES ('96','OnBeforeManagerPageInit','2','');

INSERT INTO `modx_system_eventnames` VALUES ('97','OnBeforeEmptyTrash','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('98','OnEmptyTrash','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('99','OnManagerLoginFormPrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('100','OnStripAlias','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('102','OnMakeDocUrl','5','');

INSERT INTO `modx_system_eventnames` VALUES ('103','OnBeforeLoadExtension','5','');

INSERT INTO `modx_system_eventnames` VALUES ('200','OnCreateDocGroup','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('201','OnManagerWelcomePrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('202','OnManagerWelcomeHome','2','');

INSERT INTO `modx_system_eventnames` VALUES ('203','OnManagerWelcomeRender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('204','OnBeforeDocDuplicate','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('205','OnDocDuplicate','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('206','OnManagerMainFrameHeaderHTMLBlock','2','');

INSERT INTO `modx_system_eventnames` VALUES ('207','OnManagerPreFrameLoader','2','');

INSERT INTO `modx_system_eventnames` VALUES ('208','OnManagerFrameLoader','2','');

INSERT INTO `modx_system_eventnames` VALUES ('209','OnManagerTreeInit','2','');

INSERT INTO `modx_system_eventnames` VALUES ('210','OnManagerTreePrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('211','OnManagerTreeRender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('212','OnManagerNodePrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('213','OnManagerNodeRender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('214','OnManagerMenuPrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('215','OnManagerTopPrerender','2','');

INSERT INTO `modx_system_eventnames` VALUES ('224','OnDocFormTemplateRender','1','Documents');

INSERT INTO `modx_system_eventnames` VALUES ('999','OnPageUnauthorized','1','');

INSERT INTO `modx_system_eventnames` VALUES ('1000','OnPageNotFound','1','');

INSERT INTO `modx_system_eventnames` VALUES ('1001','OnFileBrowserUpload','1','File Browser Events');


# --------------------------------------------------------

#
# Table structure for table `modx_system_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_system_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_system_settings` (
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains Content Manager settings.';

#
# Dumping data for table `modx_system_settings`
#

INSERT INTO `modx_system_settings` VALUES ('manager_theme','MODxRE2');

INSERT INTO `modx_system_settings` VALUES ('settings_version','1.2 Artjoker');

INSERT INTO `modx_system_settings` VALUES ('show_meta','0');

INSERT INTO `modx_system_settings` VALUES ('server_offset_time','0');

INSERT INTO `modx_system_settings` VALUES ('server_protocol','https');

INSERT INTO `modx_system_settings` VALUES ('manager_language','russian-UTF8');

INSERT INTO `modx_system_settings` VALUES ('modx_charset','UTF-8');

INSERT INTO `modx_system_settings` VALUES ('site_name','Двери Гарант');

INSERT INTO `modx_system_settings` VALUES ('site_start','1');

INSERT INTO `modx_system_settings` VALUES ('error_page','2');

INSERT INTO `modx_system_settings` VALUES ('unauthorized_page','1');

INSERT INTO `modx_system_settings` VALUES ('site_status','1');

INSERT INTO `modx_system_settings` VALUES ('site_unavailable_message','The site is currently unavailable');

INSERT INTO `modx_system_settings` VALUES ('track_visitors','0');

INSERT INTO `modx_system_settings` VALUES ('top_howmany','10');

INSERT INTO `modx_system_settings` VALUES ('auto_template_logic','sibling');

INSERT INTO `modx_system_settings` VALUES ('default_template','3');

INSERT INTO `modx_system_settings` VALUES ('old_template','3');

INSERT INTO `modx_system_settings` VALUES ('publish_default','1');

INSERT INTO `modx_system_settings` VALUES ('cache_default','1');

INSERT INTO `modx_system_settings` VALUES ('search_default','1');

INSERT INTO `modx_system_settings` VALUES ('friendly_urls','1');

INSERT INTO `modx_system_settings` VALUES ('friendly_url_prefix','');

INSERT INTO `modx_system_settings` VALUES ('friendly_url_suffix','/');

INSERT INTO `modx_system_settings` VALUES ('friendly_alias_urls','1');

INSERT INTO `modx_system_settings` VALUES ('use_alias_path','1');

INSERT INTO `modx_system_settings` VALUES ('use_udperms','1');

INSERT INTO `modx_system_settings` VALUES ('udperms_allowroot','0');

INSERT INTO `modx_system_settings` VALUES ('failed_login_attempts','10');

INSERT INTO `modx_system_settings` VALUES ('blocked_minutes','60');

INSERT INTO `modx_system_settings` VALUES ('use_captcha','0');

INSERT INTO `modx_system_settings` VALUES ('captcha_words','MODX,Access,Better,BitCode,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Tattoo,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote');

INSERT INTO `modx_system_settings` VALUES ('emailsender','a.gorban@artjoker.team');

INSERT INTO `modx_system_settings` VALUES ('email_method','smtp');

INSERT INTO `modx_system_settings` VALUES ('smtp_auth','1');

INSERT INTO `modx_system_settings` VALUES ('smtp_host','mail.artjoker.org');

INSERT INTO `modx_system_settings` VALUES ('smtp_port','587');

INSERT INTO `modx_system_settings` VALUES ('smtp_username','test@artjoker.org');

INSERT INTO `modx_system_settings` VALUES ('smtppw','Y2tsNGdsKWxXKlghykV3gTq');

INSERT INTO `modx_system_settings` VALUES ('emailsubject','Your login details');

INSERT INTO `modx_system_settings` VALUES ('number_of_logs','100');

INSERT INTO `modx_system_settings` VALUES ('number_of_messages','30');

INSERT INTO `modx_system_settings` VALUES ('number_of_results','20');

INSERT INTO `modx_system_settings` VALUES ('use_editor','1');

INSERT INTO `modx_system_settings` VALUES ('use_browser','1');

INSERT INTO `modx_system_settings` VALUES ('which_browser','mcpuk');

INSERT INTO `modx_system_settings` VALUES ('rb_base_dir','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/assets/');

INSERT INTO `modx_system_settings` VALUES ('rb_base_url','assets/');

INSERT INTO `modx_system_settings` VALUES ('which_editor','TinyMCE4');

INSERT INTO `modx_system_settings` VALUES ('fe_editor_lang','russian-UTF8');

INSERT INTO `modx_system_settings` VALUES ('fck_editor_toolbar','standard');

INSERT INTO `modx_system_settings` VALUES ('fck_editor_autolang','0');

INSERT INTO `modx_system_settings` VALUES ('editor_css_path','');

INSERT INTO `modx_system_settings` VALUES ('editor_css_selectors','');

INSERT INTO `modx_system_settings` VALUES ('strip_image_paths','1');

INSERT INTO `modx_system_settings` VALUES ('upload_images','bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff');

INSERT INTO `modx_system_settings` VALUES ('upload_media','au,avi,mp3,mp4,mpeg,mpg,wav,wmv');

INSERT INTO `modx_system_settings` VALUES ('upload_flash','fla,flv,swf');

INSERT INTO `modx_system_settings` VALUES ('upload_files','bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff,fla,flv,swf,aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip,JPG,JPEG,PNG,GIF');

INSERT INTO `modx_system_settings` VALUES ('upload_maxsize','10485760');

INSERT INTO `modx_system_settings` VALUES ('new_file_permissions','0644');

INSERT INTO `modx_system_settings` VALUES ('new_folder_permissions','0755');

INSERT INTO `modx_system_settings` VALUES ('filemanager_path','/hosting/www/dverigarant/dverigarant.app.artjoker.ua/');

INSERT INTO `modx_system_settings` VALUES ('theme_refresher','');

INSERT INTO `modx_system_settings` VALUES ('manager_layout','4');

INSERT INTO `modx_system_settings` VALUES ('custom_contenttype','application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json');

INSERT INTO `modx_system_settings` VALUES ('auto_menuindex','1');

INSERT INTO `modx_system_settings` VALUES ('session.cookie.lifetime','31556926');

INSERT INTO `modx_system_settings` VALUES ('mail_check_timeperiod','60');

INSERT INTO `modx_system_settings` VALUES ('manager_direction','ltr');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_theme','custom');

INSERT INTO `modx_system_settings` VALUES ('tree_show_protected','0');

INSERT INTO `modx_system_settings` VALUES ('rss_url_news','http://feeds.feedburner.com/modx-announce');

INSERT INTO `modx_system_settings` VALUES ('rss_url_security','http://feeds.feedburner.com/modxsecurity');

INSERT INTO `modx_system_settings` VALUES ('validate_referer','1');

INSERT INTO `modx_system_settings` VALUES ('datepicker_offset','-10');

INSERT INTO `modx_system_settings` VALUES ('xhtml_urls','1');

INSERT INTO `modx_system_settings` VALUES ('allow_duplicate_alias','0');

INSERT INTO `modx_system_settings` VALUES ('automatic_alias','1');

INSERT INTO `modx_system_settings` VALUES ('datetime_format','dd-mm-YYYY');

INSERT INTO `modx_system_settings` VALUES ('warning_visibility','1');

INSERT INTO `modx_system_settings` VALUES ('remember_last_tab','1');

INSERT INTO `modx_system_settings` VALUES ('enable_bindings','0');

INSERT INTO `modx_system_settings` VALUES ('seostrict','0');

INSERT INTO `modx_system_settings` VALUES ('cache_type','2');

INSERT INTO `modx_system_settings` VALUES ('maxImageWidth','4096');

INSERT INTO `modx_system_settings` VALUES ('maxImageHeight','3072');

INSERT INTO `modx_system_settings` VALUES ('thumbWidth','150');

INSERT INTO `modx_system_settings` VALUES ('thumbHeight','150');

INSERT INTO `modx_system_settings` VALUES ('thumbsDir','.thumbs');

INSERT INTO `modx_system_settings` VALUES ('jpegQuality','90');

INSERT INTO `modx_system_settings` VALUES ('denyZipDownload','0');

INSERT INTO `modx_system_settings` VALUES ('denyExtensionRename','1');

INSERT INTO `modx_system_settings` VALUES ('showHiddenFiles','0');

INSERT INTO `modx_system_settings` VALUES ('docid_incrmnt_method','0');

INSERT INTO `modx_system_settings` VALUES ('make_folders','1');

INSERT INTO `modx_system_settings` VALUES ('tree_page_click','27');

INSERT INTO `modx_system_settings` VALUES ('clean_uploaded_filename','1');

INSERT INTO `modx_system_settings` VALUES ('lang_enable','1');

INSERT INTO `modx_system_settings` VALUES ('lang_default_show','1');

INSERT INTO `modx_system_settings` VALUES ('lang_default','ua');

INSERT INTO `modx_system_settings` VALUES ('lang_list','ua,ru');

INSERT INTO `modx_system_settings` VALUES ('lang_front','ua,ru');

INSERT INTO `modx_system_settings` VALUES ('seo_ga','');

INSERT INTO `modx_system_settings` VALUES ('seo_ya','');

INSERT INTO `modx_system_settings` VALUES ('minify_on','1');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_server','mail.artjoker.org');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_port','587');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_login','test@artjoker.org');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_pass','ckl4gl)lW*X!');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_name','Test Artjoker');

INSERT INTO `modx_system_settings` VALUES ('mailer_smtp_limit','10');

INSERT INTO `modx_system_settings` VALUES ('shop_tpl_category','4');

INSERT INTO `modx_system_settings` VALUES ('shop_root_item','1');

INSERT INTO `modx_system_settings` VALUES ('shop_page_item','4');

INSERT INTO `modx_system_settings` VALUES ('shop_catalog_root','3');

INSERT INTO `modx_system_settings` VALUES ('news_catalog_root','29');

INSERT INTO `modx_system_settings` VALUES ('news_page_item','44');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_name','Гривна|Доллар|Рубль');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_code','uah|usd|rur');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_rate','1|25|0.2');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_sign','грн|$|₽');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_default_sign','грн');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_default_code','uah');

INSERT INTO `modx_system_settings` VALUES ('shop_curr_active','1|0|0');

INSERT INTO `modx_system_settings` VALUES ('shop_thanks_order','20');

INSERT INTO `modx_system_settings` VALUES ('auto_create_account','0');

INSERT INTO `modx_system_settings` VALUES ('as_gmaps_key','AIzaSyBGvi_qV9_1fksCufX4o5A7Rm9NpyRfrQQ');

INSERT INTO `modx_system_settings` VALUES ('webuser_account','0');

INSERT INTO `modx_system_settings` VALUES ('site_id','5d6fb5cae1f6b');

INSERT INTO `modx_system_settings` VALUES ('site_unavailable_page','');

INSERT INTO `modx_system_settings` VALUES ('reload_site_unavailable','');

INSERT INTO `modx_system_settings` VALUES ('siteunavailable_message_default','В настоящее время сайт недоступен.');

INSERT INTO `modx_system_settings` VALUES ('enable_filter','0');

INSERT INTO `modx_system_settings` VALUES ('aliaslistingfolder','0');

INSERT INTO `modx_system_settings` VALUES ('check_files_onlogin','index.php\n.htaccess\ninspiration/index.php\ninspiration/includes/config.inc.php');

INSERT INTO `modx_system_settings` VALUES ('session_cookie_lifetime','31556926');

INSERT INTO `modx_system_settings` VALUES ('error_reporting','1');

INSERT INTO `modx_system_settings` VALUES ('send_errormail','0');

INSERT INTO `modx_system_settings` VALUES ('pwd_hash_algo','UNCRYPT');

INSERT INTO `modx_system_settings` VALUES ('reload_captcha_words','');

INSERT INTO `modx_system_settings` VALUES ('captcha_words_default','MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote');

INSERT INTO `modx_system_settings` VALUES ('smtp_secure','none');

INSERT INTO `modx_system_settings` VALUES ('reload_emailsubject','');

INSERT INTO `modx_system_settings` VALUES ('emailsubject_default','Данные для авторизации');

INSERT INTO `modx_system_settings` VALUES ('reload_signupemail_message','');

INSERT INTO `modx_system_settings` VALUES ('signupemail_message','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('system_email_signup_default','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации в системе управления сайтом [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('reload_websignupemail_message','');

INSERT INTO `modx_system_settings` VALUES ('websignupemail_message','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('system_email_websignup_default','Здравствуйте, [+uid+]!\n\nВаши данные для авторизации на [+sname+]:\n\nИмя пользователя: [+uid+]\nПароль: [+pwd+]\n\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('reload_system_email_webreminder_message','');

INSERT INTO `modx_system_settings` VALUES ('webpwdreminder_message','Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('system_email_webreminder_default','Здравствуйте, [+uid+]!\n\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\n\n[+surl+]\n\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\n\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\n\nС уважением, Администрация');

INSERT INTO `modx_system_settings` VALUES ('use_breadcrumbs','0');

INSERT INTO `modx_system_settings` VALUES ('resource_tree_node_name','pagetitle');

INSERT INTO `modx_system_settings` VALUES ('lock_interval','15');

INSERT INTO `modx_system_settings` VALUES ('lock_release_delay','30');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_skin','lightgray');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_template_docs','');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_template_chunks','');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_entermode','p');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_element_format','xhtml');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_schema','html5');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_custom_plugins','advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_custom_buttons1','undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_custom_buttons2','link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_custom_buttons3','');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_custom_buttons4','');

INSERT INTO `modx_system_settings` VALUES ('tinymce4_blockFormats','Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3');

INSERT INTO `modx_system_settings` VALUES ('rb_webuser','0');

INSERT INTO `modx_system_settings` VALUES ('lang_code','ru');

INSERT INTO `modx_system_settings` VALUES ('sys_files_checksum','a:4:{s:62:\"/hosting/www/dverigarant/dverigarant.app.artjoker.ua/index.php\";s:32:\"3e1c14d8720f3fef0b73ff9876c19f52\";s:62:\"/hosting/www/dverigarant/dverigarant.app.artjoker.ua/.htaccess\";s:32:\"55fee4359ba830b021084cd02e672084\";s:74:\"/hosting/www/dverigarant/dverigarant.app.artjoker.ua/inspiration/index.php\";s:32:\"4897f3bceb9962805eb1b97682b26328\";s:88:\"/hosting/www/dverigarant/dverigarant.app.artjoker.ua/inspiration/includes/config.inc.php\";s:32:\"1aeb9b61095763e2ca2c10897e51849e\";}');

INSERT INTO `modx_system_settings` VALUES ('as_phone_1','(067) 830 88 85');

INSERT INTO `modx_system_settings` VALUES ('as_phone_2','(093) 557 68 80');

INSERT INTO `modx_system_settings` VALUES ('as_country_code','+38');

INSERT INTO `modx_system_settings` VALUES ('advantages','32');

INSERT INTO `modx_system_settings` VALUES ('qqq','');

INSERT INTO `modx_system_settings` VALUES ('questions','33');

INSERT INTO `modx_system_settings` VALUES ('discounts_catalog_root','25');

INSERT INTO `modx_system_settings` VALUES ('discounts_page_item','45');

INSERT INTO `modx_system_settings` VALUES ('success_send','74');

INSERT INTO `modx_system_settings` VALUES ('as_facebook','//facebook.com');

INSERT INTO `modx_system_settings` VALUES ('as_instagram','//instagram.com');

INSERT INTO `modx_system_settings` VALUES ('checkout','19');

INSERT INTO `modx_system_settings` VALUES ('as_delivery','200');

INSERT INTO `modx_system_settings` VALUES ('as_installation','300');

INSERT INTO `modx_system_settings` VALUES ('search','75');

INSERT INTO `modx_system_settings` VALUES ('bread_ignoreIds','53,54,55');


# --------------------------------------------------------

#
# Table structure for table `modx_user_attributes`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_user_attributes`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='Contains information about the backend users.';

#
# Dumping data for table `modx_user_attributes`
#

INSERT INTO `modx_user_attributes` VALUES ('1','1','Default admin account','1','a.gorban@artjoker.team','','','0','0','0','36','1572856729','1572856737','0','i28o1esgfgahd3nlmdn8dl11at','0','0','','','','','','','','');

INSERT INTO `modx_user_attributes` VALUES ('2','2','','1','kolomak9@ukr.net','','','0','0','0','10','1572769772','1572797858','0','s14km5kj3fcvn855r22k41u91r','0','0','','','','','','','','');


# --------------------------------------------------------

#
# Table structure for table `modx_user_messages`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_user_messages`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_user_messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(60) NOT NULL DEFAULT '',
  `message` text,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `postdate` int(20) NOT NULL DEFAULT '0',
  `messageread` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains messages for the Content Manager messaging system.';

#
# Dumping data for table `modx_user_messages`
#


# --------------------------------------------------------

#
# Table structure for table `modx_user_roles`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_user_roles`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_user_roles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `frames` int(1) NOT NULL DEFAULT '0',
  `home` int(1) NOT NULL DEFAULT '0',
  `view_document` int(1) NOT NULL DEFAULT '0',
  `new_document` int(1) NOT NULL DEFAULT '0',
  `save_document` int(1) NOT NULL DEFAULT '0',
  `publish_document` int(1) NOT NULL DEFAULT '0',
  `delete_document` int(1) NOT NULL DEFAULT '0',
  `empty_trash` int(1) NOT NULL DEFAULT '0',
  `action_ok` int(1) NOT NULL DEFAULT '0',
  `logout` int(1) NOT NULL DEFAULT '0',
  `help` int(1) NOT NULL DEFAULT '0',
  `messages` int(1) NOT NULL DEFAULT '0',
  `new_user` int(1) NOT NULL DEFAULT '0',
  `edit_user` int(1) NOT NULL DEFAULT '0',
  `logs` int(1) NOT NULL DEFAULT '0',
  `edit_parser` int(1) NOT NULL DEFAULT '0',
  `save_parser` int(1) NOT NULL DEFAULT '0',
  `edit_template` int(1) NOT NULL DEFAULT '0',
  `settings` int(1) NOT NULL DEFAULT '0',
  `credits` int(1) NOT NULL DEFAULT '0',
  `new_template` int(1) NOT NULL DEFAULT '0',
  `save_template` int(1) NOT NULL DEFAULT '0',
  `delete_template` int(1) NOT NULL DEFAULT '0',
  `edit_snippet` int(1) NOT NULL DEFAULT '0',
  `new_snippet` int(1) NOT NULL DEFAULT '0',
  `save_snippet` int(1) NOT NULL DEFAULT '0',
  `delete_snippet` int(1) NOT NULL DEFAULT '0',
  `edit_chunk` int(1) NOT NULL DEFAULT '0',
  `new_chunk` int(1) NOT NULL DEFAULT '0',
  `save_chunk` int(1) NOT NULL DEFAULT '0',
  `delete_chunk` int(1) NOT NULL DEFAULT '0',
  `empty_cache` int(1) NOT NULL DEFAULT '0',
  `edit_document` int(1) NOT NULL DEFAULT '0',
  `change_password` int(1) NOT NULL DEFAULT '0',
  `error_dialog` int(1) NOT NULL DEFAULT '0',
  `about` int(1) NOT NULL DEFAULT '0',
  `file_manager` int(1) NOT NULL DEFAULT '0',
  `assets_files` int(1) NOT NULL DEFAULT '0',
  `assets_images` int(1) NOT NULL DEFAULT '0',
  `save_user` int(1) NOT NULL DEFAULT '0',
  `delete_user` int(1) NOT NULL DEFAULT '0',
  `save_password` int(11) NOT NULL DEFAULT '0',
  `edit_role` int(1) NOT NULL DEFAULT '0',
  `save_role` int(1) NOT NULL DEFAULT '0',
  `delete_role` int(1) NOT NULL DEFAULT '0',
  `new_role` int(1) NOT NULL DEFAULT '0',
  `access_permissions` int(1) NOT NULL DEFAULT '0',
  `bk_manager` int(1) NOT NULL DEFAULT '0',
  `new_plugin` int(1) NOT NULL DEFAULT '0',
  `edit_plugin` int(1) NOT NULL DEFAULT '0',
  `save_plugin` int(1) NOT NULL DEFAULT '0',
  `delete_plugin` int(1) NOT NULL DEFAULT '0',
  `new_module` int(1) NOT NULL DEFAULT '0',
  `edit_module` int(1) NOT NULL DEFAULT '0',
  `save_module` int(1) NOT NULL DEFAULT '0',
  `delete_module` int(1) NOT NULL DEFAULT '0',
  `exec_module` int(1) NOT NULL DEFAULT '0',
  `view_eventlog` int(1) NOT NULL DEFAULT '0',
  `delete_eventlog` int(1) NOT NULL DEFAULT '0',
  `manage_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'manage site meta tags and keywords',
  `edit_doc_metatags` int(1) NOT NULL DEFAULT '0' COMMENT 'edit document meta tags and keywords',
  `new_web_user` int(1) NOT NULL DEFAULT '0',
  `edit_web_user` int(1) NOT NULL DEFAULT '0',
  `save_web_user` int(1) NOT NULL DEFAULT '0',
  `delete_web_user` int(1) NOT NULL DEFAULT '0',
  `web_access_permissions` int(1) NOT NULL DEFAULT '0',
  `view_unpublished` int(1) NOT NULL DEFAULT '0',
  `import_static` int(1) NOT NULL DEFAULT '0',
  `export_static` int(1) NOT NULL DEFAULT '0',
  `remove_locks` int(1) NOT NULL DEFAULT '0',
  `display_locks` int(1) NOT NULL DEFAULT '0',
  `change_resourcetype` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='Contains information describing the user roles.';

#
# Dumping data for table `modx_user_roles`
#

INSERT INTO `modx_user_roles` VALUES ('2','Editor','Limited to managing content','1','1','1','1','1','1','1','0','1','1','1','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','1','0','1','0','1','1','1','1','1','1','1','1','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','1','0','0','1','1','1');

INSERT INTO `modx_user_roles` VALUES ('3','Publisher','Editor with expanded permissions including manage users, update Elements and site settings','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','0','0','1','1','1','1','1','1','0','0','0','0','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','0','1','0','0','0','0','1','1','1','1','0','1','0','0','1','1','1');

INSERT INTO `modx_user_roles` VALUES ('1','Administrator','Site administrators have full access to all functions','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1');


# --------------------------------------------------------

#
# Table structure for table `modx_user_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_user_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`user`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains backend user settings.';

#
# Dumping data for table `modx_user_settings`
#

INSERT INTO `modx_user_settings` VALUES ('2','tinymce4_custom_buttons2','link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code');

INSERT INTO `modx_user_settings` VALUES ('2','tinymce4_blockFormats','Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3');

INSERT INTO `modx_user_settings` VALUES ('2','tinymce4_custom_buttons1','undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect');

INSERT INTO `modx_user_settings` VALUES ('2','tinymce4_custom_plugins','advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube');

INSERT INTO `modx_user_settings` VALUES ('1','allow_manager_access','1');

INSERT INTO `modx_user_settings` VALUES ('1','which_browser','default');

INSERT INTO `modx_user_settings` VALUES ('1','tinymce4_custom_plugins','advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen spellchecker insertdatetime media nonbreaking save table contextmenu directionality emoticons template paste textcolor codesample colorpicker textpattern imagetools paste modxlink youtube');

INSERT INTO `modx_user_settings` VALUES ('1','tinymce4_custom_buttons1','undo redo | cut copy paste | searchreplace | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent blockquote | styleselect');

INSERT INTO `modx_user_settings` VALUES ('1','tinymce4_custom_buttons2','link unlink anchor image media codesample table | hr removeformat | subscript superscript charmap | nonbreaking | visualchars visualblocks print preview fullscreen code');

INSERT INTO `modx_user_settings` VALUES ('1','tinymce4_blockFormats','Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3');

INSERT INTO `modx_user_settings` VALUES ('2','allow_manager_access','1');

INSERT INTO `modx_user_settings` VALUES ('2','which_browser','default');


# --------------------------------------------------------

#
# Table structure for table `modx_web_groups`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_groups`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `webuser` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_group_user` (`webgroup`,`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_web_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_attributes`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_user_attributes`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_user_attributes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL DEFAULT '0',
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `role` int(10) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` int(1) NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(25) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '' COMMENT 'link to photo',
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains information for web users.';

#
# Dumping data for table `modx_web_user_attributes`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_deliveries`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_user_deliveries`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_user_deliveries` (
  `delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_user` int(11) NOT NULL,
  `delivery_name` varchar(100) DEFAULT NULL,
  `delivery_phone` varchar(100) DEFAULT NULL,
  `delivery_city` varchar(100) DEFAULT NULL,
  `delivery_street` varchar(100) DEFAULT NULL,
  `delivery_house` varchar(100) DEFAULT NULL,
  `delivery_apartament` varchar(100) NOT NULL DEFAULT '',
  `delivery_comment` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`delivery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Dumping data for table `modx_web_user_deliveries`
#

INSERT INTO `modx_web_user_deliveries` VALUES ('1','1','Test Test','(380) 001 23 41','Харьков','ул. Миклухи Маклая','1','1','Тестирование');

INSERT INTO `modx_web_user_deliveries` VALUES ('2','1','Test Test','(380) 001 23 42','Харьков','ул. Маклая Миклухи','5','78','');

INSERT INTO `modx_web_user_deliveries` VALUES ('3','1','Test Test','(380) 001 23 43','Харьков','Властелина колец','5','','Левый угол с правой стороны');

INSERT INTO `modx_web_user_deliveries` VALUES ('4','1','Test Test','(380) 001 23 44','Киев','Президентская','35','','Занести ему самому');


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_settings`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_user_settings`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_user_settings` (
  `webuser` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` text,
  PRIMARY KEY (`webuser`,`setting_name`),
  KEY `setting_name` (`setting_name`),
  KEY `webuserid` (`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains web user settings.';

#
# Dumping data for table `modx_web_user_settings`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_tokens`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_user_tokens`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_user_tokens` (
  `internalKey` int(11) NOT NULL DEFAULT '0',
  `tokenid` varchar(128) NOT NULL DEFAULT '',
  UNIQUE KEY `internalKey_tokenid` (`internalKey`,`tokenid`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains information for web users tokens.';

#
# Dumping data for table `modx_web_user_tokens`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_users`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_web_users`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_web_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '' COMMENT 'Store new unconfirmed password',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

#
# Dumping data for table `modx_web_users`
#


# --------------------------------------------------------

#
# Table structure for table `modx_webgroup_access`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_webgroup_access`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_webgroup_access` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `webgroup` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_webgroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_webgroup_names`
#

SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `modx_webgroup_names`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

CREATE TABLE `modx_webgroup_names` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(245) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_webgroup_names`
#
