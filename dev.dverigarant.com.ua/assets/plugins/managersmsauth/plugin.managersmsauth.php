<?php
if ($modx->config['sms_auth'] == 1) {
    if (isset($_SESSION['username']) && $_SESSION['username'] != '') {
        if (isset($_SESSION['phone']) && $_SESSION['phone'] != '') {
            if (!isset($_SESSION['sms_code']) || $_SESSION['sms_code'] == '') {
                require_once MODX_BASE_PATH."assets/shop/class.shop.php";
                $shop = new Shop($modx);
                    
                $sms_code = $shop->genPass(6, false);
                if ($shop->sendSMS($_SESSION['phone'], $sms_code) > 0) {
                    $_SESSION['sms_code'] = $sms_code;
                    echo 'Вам отправлен смс с кодом подтверждения';
                    die;
                }
            } else {
                if ($_SESSION['sms_code'] == $_REQUEST['sms_code']) {
                    unset($_SESSION['sms_code']);
                } else {
                    unset($_SESSION['sms_code']);
                    echo 'Вы ввели неправильный код подтверждения!';
                    die;
                }
            }
        }
    }
}