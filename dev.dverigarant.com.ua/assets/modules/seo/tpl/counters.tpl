<form action="<?=$url?>&get=counters" method="post">
	<table class="table table-bordered table-condensed table-striped">
		<tr>
			<td width="200px">
				<b><?=$_lang['seo_google']?></b><br>
				<small>[(seo_ga)]</small>
			</td>
			<td><textarea name="seo_ga" class="form-control" cols="30" rows="5"><?=$modx->config['seo_ga']?></textarea></td>
		</tr>
		<tr>
			<td>
				<b><?=$_lang['seo_yandex']?></b><br>
				<small>[(seo_ya)]</small>
			</td>
			<td><textarea name="seo_ya" class="form-control" cols="30" rows="5"><?=$modx->config['seo_ya']?></textarea></td>
		</tr>
	</table>
	<input type="submit" value="<?=$_lang['lang_btn_save']?>" class="btn btn-large btn-primary">
</form>