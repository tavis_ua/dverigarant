<form action="<?=$url?>&get=sitemap" method="post">
	<p><?=$_lang['seo_about_sitemap']?></p>
	<textarea name="post" class="span12"><?=file_get_contents(ROOT."/sitemap.php")?></textarea>
	<input type="submit" value="<?=$_lang['seo_update_sitemap']?>" class="btn btn-primary">
	<a href="<?=$url?>&get=sitemap&do=reconstruct" class="btn btn-info pull-right"><?=$_lang['lang_btn_update']?> <b>sitemap.xml</b></a>
</form>
<?=implode("",$modx->invokeEvent("OnModFormRender"));?>