<form action="<?=$url?>&get=robots" method="post">
	<p><b>robots.txt</b> – <?=$_lang['lang_btn_save']?></p>
	<textarea name="post" class="span12" cols="60" rows="10"><?=file_get_contents($_SERVER['DOCUMENT_ROOT']."/robots.txt")?></textarea>
	<br>
	<input type="submit" value="<?=$_lang['seo_update_robots']?>" class="btn btn-large btn-primary">
</form>
<?=implode("",$modx->invokeEvent("OnModFormRender"));?>