<?php
$langs = $modx->getConfig('lang_enable') == 1 ? explode(",", $modx->getConfig('lang_list')) : [$modx->getConfig('lang_default')];
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($langs as $lang){
    echo '<url><loc>'.$modx->config["site_url"].$lang.'/</loc><lastmod>'.date("Y-m-d").'</lastmod><changefreq>monthly</changefreq><priority>0.8</priority></url>';
}

$pages = $modx->db->makeArray($modx->db->query("SELECT `id`, `template` FROM `modx_site_content` WHERE `searchable` = '1' AND `published` = '1' AND `deleted` = '0' AND `id` != '1'"));
$products = $modx->db->makeArray($modx->db->query("SELECT `product_url` FROM `modx_a_products` WHERE `product_active` = '1'"));

if (is_array($pages)) {
    foreach ($langs as $lang) {
        $modx->config['lang'] = $lang;
        foreach ($pages as $page) {
            if ($page['id'] == $modx->config['shop_page_item']) {
                foreach ($products as $product) {
                    if ($modx->config['shop_root_item']) {
                        echo '<url><loc>'.$modx->makeUrl($modx->config['site_start'], '', '', 'full').$product['product_url'].'/</loc><changefreq>weekly</changefreq></url>'."\n";
                    } else {
                        echo '<url><loc>'.$modx->makeUrl($page['id'], '', '', 'full').$product['product_url'].'/</loc><changefreq>weekly</changefreq></url>'."\n";
                    }
                }
            } else {
                echo '<url><loc>'.$modx->makeUrl($page['id'], '', '', 'full').'</loc><changefreq>weekly</changefreq></url>'."\n";
            }
        }
        if ($modx->config['shop_root_item'] && is_array($products)) {
            foreach ($products as $product) {
                echo '<url><loc>'.$modx->config["site_url"].$lang.'/'.$product['product_url'].'</loc><changefreq>weekly</changefreq></url>'."\n";
            }
        }
    }
}

echo '</urlset>';