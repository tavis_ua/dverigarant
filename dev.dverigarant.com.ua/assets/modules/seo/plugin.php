<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
/**
 * SEO плагин
 */
$modx->config['lang'] = in_array($_GET['lang'], explode(",", $modx->config['lang_list'])) ? $_GET['lang'] : $modx->config['lang_default'];
switch ($modx->event->name) {
    /**
     * Отрабатывает перед обработкой страницы или если страницы не существует
     */
    case "OnWebPageInit":
    case "OnPageNotFound":
    $parts = explode("/", $_GET['q']);

    $request_uri = stristr(preg_replace("/(\/){2,}/",'$1', $modx->db->escape($_SERVER['REQUEST_URI'])), '?', TRUE); // Только цифры и запятая, без дублей запятых
    $sql = "
            SELECT * FROM `modx_a_redirect` WHERE redirect_source
            IN (
                '".$modx->db->escape($_GET['q'])."',
                '{$request_uri}',
                '".trim($request_uri,'/')."',
                '/".trim($request_uri,'/')."/',
                '".rtrim($request_uri,'/')."'
                ) ";
    $redirect = $modx->db->getRow($modx->db->query($sql));
    if (
        $_SERVER['REQUEST_SCHEME'] != $modx->config['server_protocol'] || // Не тот протокол
        $_SERVER['SERVER_NAME'] != $modx->config['valid_hostnames'] || // Не тот адрес сайта (задается в модуле СЕО)
        !empty($redirect['redirect_id']) || // Есть в базе редирект
        preg_match("/(\/){2,}/", $modx->db->escape($_SERVER['REQUEST_URI'])) || // Есть повторяющиеся //
        (strtolower($_SERVER['REQUEST_URI']) != $_SERVER['REQUEST_URI']) && !isset($_GET['search']) && !isset($_GET['gclid']) && !isset($_GET['fbclid']) && !isset($_GET['utm'])  && !isset($_GET['email']) || // Нужно привести к нижнему регистру
        ($_GET['q'] != '' && substr($_GET['q'], -1, 1) != '/') || // Нет завершающей /
        !in_array($_GET['lang'], explode(",", $modx->config['lang_list'])) // Проверка на язык
    )
    {
        if (!empty($redirect['redirect_id'])) {
            if (substr($redirect['redirect_target'], -1, 1) != '/') {
                $redirect['redirect_target'] .= '/';
            }

            switch ($redirect['redirect_code']) {
                case 301:
                    header("HTTP/1.1 301 Moved Permanently");
                case 302:
                    header("Location: ".$modx->config['server_protocol']."://".$modx->config['valid_hostnames']."/".($modx->config['lang_enable'] ? $modx->config['lang']."/" : "").$redirect['redirect_target']);
                    break;
            }
        } else {
            if ($_GET['q'] != '' && substr($_GET['q'], -1, 1) != '/') {
                $_GET['q'] .= '/';
            }
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$modx->config['server_protocol']."://".$modx->config['valid_hostnames']."/".($modx->config['lang_enable'] ? $modx->config['lang'].'/' : "").strtolower($_GET['q']));
            //header("Location: ".$modx->config['server_protocol']."://".$modx->config['seo_url']."/".$_GET['q']);
        }
        die;
    }

    if ($_GET['q'] == "1" || $_GET['q'] == array_search($modx->config["site_start"], $modx->documentListing)) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /".($modx->config['lang_enable'] ? $modx->config['lang']."/" : ""));
        die;
    }

    // 301 редирект при ссылке на первую страницу постраничке
    if (isset($_GET['p']) && (int)$_GET['p'] < 2) {
        header("Cache-Control: no-cache, must-revalidate");
        header("HTTP/1.0 301 Moved Permanently");
        header("HTTP/1.1 301 Moved Permanently");
        header("Status: 301 Moved Permanently");
        header("Location: /".$_GET['lang'].'/'.$_GET['q']);
    }
    // Отследить наличие слеша вконце
    if (substr($_SERVER['REQUEST_URI'], -1) != '/'  && !isset($_GET['gclid']) && !isset($_GET['fbclid']) && !isset($_GET['utm'])  && !isset($_GET['email'])) {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".$_SERVER['REQUEST_URI']."/");
        die;
    }
    break;
    /**
     * Обработчик загрузки страницы (Не работает из кеша)
     */
    case 'OnLoadWebDocument':
        /*
         * $_GET параметры для передачи в строку URL
         *
         * [*get_url_param*] - Пример использования в шаблоне
         */
        $get_url_param = $_GET;
        unset($get_url_param['lang'], $get_url_param['q']);
        $modx->documentObject['get_url_param'] = "";
        if (count($get_url_param) > 0) {
            $modx->documentObject['get_url_param'] = "?".http_build_query($get_url_param);
        }

        /**
         * $textPaginate текст для указания номера страницы
         */
        $textPaginate = isset($_GET['page']) || isset($_GET['p']) ? ' [#29#]'.intval($_GET['page'].$_GET['p']) : ''; // страница-

        // Страница фильтра(категории с одним фильтром)

        if ($modx->documentObject['template'] == $modx->config['shop_tpl_category']) {

            $meta_filter = $modx->db->getRow($modx->db->query("
                SELECT * FROM `modx_a_meta_filter` f
                LEFT JOIN `modx_a_meta_filter_strings` fs ON `f`.`id` = `fs`.`rid` AND `fs`.`lang` = '".$modx->config['lang']."'
                WHERE `url` = '" . $modx->db->escape(trim($_GET['q'], '/')) . "' AND `status` = '1'"));

            if ($meta_filter) {
                $modx->documentObject['seo_title_'.$modx->config['lang']][1] = $meta_filter['seo_title'];
                $modx->documentObject['seo_description_'.$modx->config['lang']][1] = htmlspecialchars_decode($meta_filter['seo_description']);
                $modx->documentObject['seo_keywords_'.$modx->config['lang']][1] = $meta_filter['seo_keywords'];
                $modx->documentObject['seo_canonical_'.$modx->config['lang']][1] = $meta_filter['seo_canonical'];
                $modx->documentObject['seo_robots_'.$modx->config['lang']][1] = $meta_filter['seo_robots'];
                $modx->documentObject['seo_content_'.$modx->config['lang']][1] = htmlspecialchars_decode($meta_filter['content']);

            } elseif (!empty($modx->getPlaceholder("filters"))) {
                $filters = $modx->getPlaceholder("filters");
                $price = strripos('price', $modx->getPlaceholder("filters"));
                $filters = explode(',', str_replace(';', ',', $filters));
                //filters seo title
                $filter_arr = explode(";",substr($_SERVER["REQUEST_URI"],strpos($_SERVER["REQUEST_URI"],$filters[0])));
                $seo_filter_title = "";

//                for($i=0;$i<count($filter_arr);$i++) {
//                    $filter_one = explode("=",trim($filter_arr[$i], '/'));
//                    if (isset($filter_one[0]) && $filter_one[1]) {
//                        $filter_name = $modx->db->getValue($modx->db->query("
//                            SELECT `filter_name`
//                            FROM `modx_a_filters` `f`
//                            LEFT JOIN `modx_a_filter_strings` `fs` ON `fs`.`filter_id` = `f`.`filter_id` AND `fs`.`string_locate` = '".$modx->config['lang']."'
//                            WHERE  `f`.`filter_url` = '" . $modx->db->escape($filter_one[0]) . "'
//                        "));
//
//                        $filter_value = str_replace("/","",$filter_one[1]);
//                        $filter_value = $modx->db->getValue($modx->db->query("
//                            SELECT `filter_value_".$modx->config['lang']."`, `value_id`
//                            FROM `modx_a_filter_values`
//                            WHERE `filter_url` = '" . $modx->db->escape($filter_value) . "'
//                        "));
//
//                        $seo_filter_title .=  $filter_name . ' ' . $filter_value." ";
//                    }
//                }
                //end filters seo title


                if (is_array($filters) && count($filters) > 1) {
                    $modx->documentObject['seo_robots'] = 'noindex,nofollow';
                    $modx->documentObject['seo_canonical'] = $modx->makeUrl($modx->documentObject['id']);
                } elseif (is_array($filters) && count($filters) == 1 && $price === false) {
                    $filter['pagetitle'] = $modx->documentObject['pagetitle_' . $modx->config['lang']];
                    $filters = explode('=', $filters[0]);
                        if (isset($filters[0]) && $filters[1]) {
//                            $filter['name'] = $modx->db->getValue($modx->db->query("SELECT `filter_name`
//                                FROM `modx_a_filters` `f`
//                                LEFT JOIN `modx_a_filter_strings` `fs` ON `fs`.`filter_id` = `f`.`filter_id` AND `fs`.`string_locate` = '".$modx->config['lang']."'
//                                WHERE  `f`.`filter_url` = '" . $modx->db->escape($filters[0]) . "'"));
//
                            $filter['filter_value'] = $modx->db->getValue($modx->db->query("
                                SELECT `filter_value_" . $modx->config['lang'] . "`
                                FROM `modx_a_filter_values` `fv`
                                WHERE  `fv`.`filter_url` = '" . $modx->db->escape($filters[1]) . "'
                            "));
                        }

                        $modx->documentObject['seo_h1_'.$modx->config['lang']][1]          = $modx->parseChunk('tpl_meta_filters_default_h1_'. $modx->config['lang'], $filter);
                        $modx->documentObject['seo_title_'.$modx->config['lang']][1]       = $modx->parseChunk('tpl_meta_filters_default_title_'. $modx->config['lang'], $filter);
                        $modx->documentObject['seo_description_'.$modx->config['lang']][1] = $modx->parseChunk('tpl_meta_filters_default_description_'. $modx->config['lang'], $filter);

                }
            }

            if (isset($_GET['sort'])) {
                $modx->documentObject['seo_robots'] = 'noindex,nofollow';
            }

//                $modx->documentObject['pagetitle'] = $modx->documentObject['pagetitle_'.$modx->config['lang']] = $row['title'];
//                $modx->documentObject['content']   = $modx->documentObject['content_'.$modx->config['lang']]   = $row['content'];
        }

        /*
         * Настройки мета тегов
         *
         * Пример использования в шаблоне
         * [*seo_description*] - SEO description
         * [*seo_keywords*] - SEO keywords
         * [*seo_robots*] - SEO robots
         * [*seo_canonical_link*] - SEO canonical without tag
         * [*seo_canonical*] - SEO canonical
         * [*seo_hreflang*] - SEO hreflang
         * [*seo_head*] - All this SEO
         */
        // SEO description
        $seo_description = isset($modx->documentObject['seo_description']) && $modx->documentObject['seo_description'] != '' && !is_array($modx->documentObject['seo_description']) ? $modx->documentObject['seo_description'] : trim($modx->documentObject['seo_description_'.$modx->config['lang']][1]);
        if ($seo_description == '[*description*]' || $seo_description == "[*description_".$modx->config['lang']."*]") {
            $seo_description = trim($modx->documentObject['description_'.$modx->config['lang']]);
        }
        $seo_description = $seo_description.$textPaginate;
        $modx->documentObject['seo_description'] = '';
        if ($seo_description != "") {
            $last = mb_strlen($seo_description) > 250 ? "..." : "";
            $text = [];
            $word = explode(" ", $seo_description);
            $count = 0;
            for($j = 0; $j < count($word); $j++) {
                $count += mb_strlen($word[$j]);
                if($count < 247) {
                    $text[] = $word[$j];
                }

            }
            $seo_description = implode(" ", $text).$last;
        }
        $modx->documentObject['seo_description'] = trim($seo_description) ? '<meta name="description" content="'.htmlspecialchars($seo_description).'">' : '';
        // SEO keywords
        $seo_keywords = isset($modx->documentObject['seo_keywords']) && $modx->documentObject['seo_keywords'] != '' && !is_array($modx->documentObject['seo_keywords']) ? $modx->documentObject['seo_keywords'] : trim($modx->documentObject['seo_keywords_'.$modx->config['lang']][1]);
        if ($seo_keywords == '[*seo_keywords*]' || $seo_keywords == "[*description_".$modx->config['lang']."*]") {
            $seo_keywords = isset($modx->documentObject['seo_keywords']) ? $modx->documentObject['seo_keywords'] : trim($modx->documentObject['seo_keywords_'.$modx->config['lang']]);
        }
        $modx->documentObject['seo_keywords'] = trim($seo_keywords) ? '<meta name="keywords" content="'.$seo_keywords.'">' : '';
        // SEO robots
        $seo_robots = isset($modx->documentObject['seo_robots']) && $modx->documentObject['seo_robots'] != '' && !is_array($modx->documentObject['seo_robots']) ? $modx->documentObject['seo_robots'] : trim($modx->documentObject['seo_robots_'.$modx->config['lang']][1]);
        if ($seo_robots == '[*seo_robots*]' || $seo_robots == "[*seo_robots_".$modx->config['lang']."*]") {
            $seo_robots = isset($modx->documentObject['seo_robots']) ? $modx->documentObject['seo_robots'] : trim($modx->documentObject['seo_robots_'.$modx->config['lang']]);
        }
        $modx->documentObject['seo_robots'] = trim($seo_robots) ? '<meta name="robots" content="'.$seo_robots.'">' : '';
        // SEO canonical
        $seo_canonical = isset($modx->documentObject['seo_canonical']) && $modx->documentObject['seo_canonical'] != '' && !is_array($modx->documentObject['seo_canonical']) ? $modx->documentObject['seo_canonical'] : trim($modx->documentObject['seo_canonical_'.$modx->config['lang']][1]);
        if ($seo_canonical == '[*seo_canonical*]' || $seo_canonical == "[*seo_canonical_".$modx->config['lang']."*]") {
            $seo_canonical = isset($modx->documentObject['seo_canonical']) ? $modx->documentObject['seo_canonical'] : trim($modx->documentObject['seo_canonical_'.$modx->config['lang']]);
        } else {
            if (!trim($seo_canonical)) {
                $seo_canonical = $modx->config['site_url'].$modx->config['lang'].'/'.$_GET['q'];
                if ( preg_match("/\/f\//i", $seo_canonical ) ) {
                    $seo_canonical = strstr($seo_canonical, '/f/', true) . '/';
                }
            }
        }
        $modx->documentObject['seo_canonical_link'] = trim($seo_canonical);
        $modx->documentObject['seo_canonical'] = trim($seo_canonical) ? '<link rel="canonical" href="'.$seo_canonical.'">' : '';
        // SEO hreflang
        $seo_hreflang = '';
        if ($modx->config['lang_enable'] == 1) {
            $lang_list = explode(",", $modx->config['lang_list']);
            $lang_attr = is_file(MODX_BASE_PATH.'assets/modules/language/lang_list.php') ? require MODX_BASE_PATH.'assets/modules/language/lang_list.php' : [];
            if (is_array($lang_list)  && count($lang_list) > 1) {
                foreach ($lang_list as $item) {
                    $seo_hreflang .= '<link rel="alternate" hreflang="'.$lang_attr[$item]['ISO 639-1'].'" href="'.$modx->config['site_url'].$item.'/'.$_GET['q'].'" />';
                }
            }
        }
        $modx->documentObject['seo_hreflang'] = trim($seo_hreflang) ? $seo_hreflang : '';
        // SEO from head
        $modx->documentObject['seo_head'] = $modx->documentObject['seo_description'].$modx->documentObject['seo_keywords'].$modx->documentObject['seo_robots'].$modx->documentObject['seo_canonical'].$modx->documentObject['seo_hreflang'];

        /*
         * Настройки контента
         *
         * Пример использования в шаблоне
         * [*seo_content*] - SEO content
         */
        // SEO content
        $seo_content = isset($modx->documentObject['seo_content']) && $modx->documentObject['seo_content'] != '' && !is_array($modx->documentObject['seo_content']) ? $modx->documentObject['seo_content'] : trim($modx->documentObject['seo_content_'.$modx->config['lang']][1]);
        if ($seo_content == '[*seo_content*]' || $seo_content == "[*seo_content_".$modx->config['lang']."*]") {
            $seo_content = isset($modx->documentObject['seo_content']) ? $modx->documentObject['seo_content'] : trim($modx->documentObject['seo_content_'.$modx->config['lang']]);
        }
        unset($modx->documentObject['seo_content']);
        unset($modx->documentObject['seo_content_'.$modx->config['lang']]);
        $modx->documentObject['seo_content'] = (trim($seo_content) && !isset($_GET['page']) && !isset($_GET['p'])) ? $seo_content : '';

        if ($_COOKIE['debug'] == 123) {
//            echo '<pre>'; print_r( $modx->documentObject); die;
        }
        break;
    /**
     * Обработчик сохранения ресурса
     */
    case "OnDocFormSave":
        $algoritm = MODX_BASE_PATH . "assets/modules/seo/sitemap.php";
        if (file_exists($algoritm)) {
            ob_start();
            include $algoritm;
            $xml = ob_get_contents();
            ob_end_clean();
            file_put_contents(MODX_BASE_PATH . "/sitemap.xml", $xml);
        }
        break;
}