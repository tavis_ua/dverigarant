<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

/**
 * SEO плагин
 */
global $_lang;
define("ROOT", dirname(__FILE__));
error_reporting(E_ERROR);
$get = isset($_GET['get']) ? $_GET['get'] : "redirect";
$res = Array();
$res['version'] = "v 2.0";
$res['url'] = $table['url'] = $url = "index.php?a=112&id=".$_GET['id']."";
$res['get'] = isset($_GET['get']) ? $_GET['get'] : "";


include MODX_BASE_PATH."assets/shop/class.shop.php";
$shop = new Shop($modx);
$languages = explode(",", $modx->config['lang_list']);

switch ($get) {
    case "filters" :

        if (isset($_GET['r'])) {
            Shop::dd(123);

            switch ($_GET['r']) {
                case "add":
                    $tiny_mce        = $shop->tinyMCE("description, content", "300px");
                    $tpl = "/tpl/filter.tpl";
                    break;
                case 'save':
                    if (isset($_POST['filter'])) {
                        $content = $_POST['filter'];
                        $modx->db->query("INSERT INTO `modx_a_meta_filter` SET 
                              `status` = '".(int)$content['status']."',
                              `url` = '".$modx->db->escape($content['url'])."'
                        ");
                        $rid = $modx->db->getValue($modx->db->query("SELECT LAST_INSERT_ID() AS last_id"));

                        if ($rid > 0) {
                            foreach ($languages as $lang) {
                                $modx->db->query("
                                    INSERT INTO modx_a_meta_filter_strings SET
                                        lang                  = '" . $lang . "',
                                        rid                   = '" . (int)$rid . "',  
                                        seo_title             = '" . $modx->db->escape(mb_substr($content[$lang]['seo_title'], 0, 255, 'UTF-8')) . "',
                                        seo_description       = '" . $modx->db->escape(mb_substr($content[$lang]['seo_description'], 0, 255, 'UTF-8')) . "',
                                        seo_content           = '" . $modx->db->escape($content[$lang]['seo_content']) . "',
                                        seo_keywords          = '" . $modx->db->escape(mb_substr($content[$lang]['seo_keywords'], 0, 255, 'UTF-8')) . "',
                                        seo_canonical         = '" . $modx->db->escape(mb_substr($content[$lang]['seo_canonical'], 0, 255, 'UTF-8')) . "',
                                        seo_robots            = '" . $modx->db->escape(mb_substr($content[$lang]['seo_robots'], 0, 255, 'UTF-8')) . "'
                                    ON DUPLICATE KEY UPDATE
                                        seo_title             = '" . $modx->db->escape(mb_substr($content[$lang]['seo_title'], 0, 255, 'UTF-8')) . "',
                                        seo_description       = '" . $modx->db->escape(mb_substr($content[$lang]['seo_description'], 0, 255, 'UTF-8')) . "',
                                        seo_content           = '" . $modx->db->escape($content[$lang]['seo_content']) . "',
                                        seo_keywords          = '" . $modx->db->escape(mb_substr($content[$lang]['seo_keywords'], 0, 255, 'UTF-8')) . "',
                                        seo_canonical         = '" . $modx->db->escape(mb_substr($content[$lang]['seo_canonical'], 0, 255, 'UTF-8')) . "',
                                        seo_robots            = '" . $modx->db->escape(mb_substr($content[$lang]['seo_robots'], 0, 255, 'UTF-8')) . "'
                                ");
                            }
                        }
                        $modx->clearCache();
                    }
                    die(header("Location: ".$url."&get=filters"));
                    break;
                case "edit":
                    $meta_filter = $modx->db->getRow($modx->db->query("
                        SELECT * 
                        FROM `modx_a_meta_filter` 
                        WHERE `id` = '".(int)$_GET['i']."'"));
                    foreach ($languages as $value) {
                        $meta_filter[$value] = $modx->db->getRow($modx->db->query("
                        SELECT * 
                        FROM `modx_a_meta_filter_strings` 
                        WHERE `lang` = '{$value}' AND `rid` = '".(int)$_GET['i']."'"));
                    }
                    $tiny_mce        = $shop->tinyMCE("description, content", "300px");
                    $tpl = "/tpl/filter.tpl";
                    break;
                case "update":
                    if (isset($_POST['filter'])) {
                        $content = $_POST['filter'];
                        $modx->db->query("UPDATE `modx_a_meta_filter` SET 
                            `status` = '" . (int)$content['status'] . "',
                            `url` = '" . $modx->db->escape($content['url']) . "'
                            WHERE `id` = '" . (int)$content['id'] . "'
                        ");
                        foreach ($languages as $lang) {
                            $modx->db->query("
                                INSERT INTO modx_a_meta_filter_strings SET
                                    lang                  = '" . $lang . "',
                                    rid                   = '" . (int)$content['id'] . "',  
                                    seo_title             = '" . $modx->db->escape(mb_substr($content[$lang]['seo_title'], 0, 255, 'UTF-8')) . "',
                                    seo_description       = '" . $modx->db->escape(mb_substr($content[$lang]['seo_description'], 0, 255, 'UTF-8')) . "',
                                    seo_content           = '" . $modx->db->escape($content[$lang]['seo_content']) . "',
                                    seo_keywords          = '" . $modx->db->escape(mb_substr($content[$lang]['seo_keywords'], 0, 255, 'UTF-8')) . "',
                                    seo_canonical         = '" . $modx->db->escape(mb_substr($content[$lang]['seo_canonical'], 0, 255, 'UTF-8')) . "',
                                    seo_robots            = '" . $modx->db->escape(mb_substr($content[$lang]['seo_robots'], 0, 255, 'UTF-8')) . "'
                                ON DUPLICATE KEY UPDATE
                                    seo_title             = '" . $modx->db->escape(mb_substr($content[$lang]['seo_title'], 0, 255, 'UTF-8')) . "',
                                    seo_description       = '" . $modx->db->escape(mb_substr($content[$lang]['seo_description'], 0, 255, 'UTF-8')) . "',
                                    seo_content           = '" . $modx->db->escape($content[$lang]['seo_content']) . "',
                                    seo_keywords          = '" . $modx->db->escape(mb_substr($content[$lang]['seo_keywords'], 0, 255, 'UTF-8')) . "',
                                    seo_canonical         = '" . $modx->db->escape(mb_substr($content[$lang]['seo_canonical'], 0, 255, 'UTF-8')) . "',
                                    seo_robots            = '" . $modx->db->escape(mb_substr($content[$lang]['seo_robots'], 0, 255, 'UTF-8')) . "'
                            ");
                        }
                        $modx->clearCache();
                    }
                    die(header("Location: " . $url . "&get=filters"));
                    break;
                case "delete":
                    $modx->db->query("DELETE FROM `modx_a_meta_filter` WHERE `id` = '".(int)$_GET['i']."'");
                    $modx->db->query("DELETE FROM `modx_a_meta_filter_strings` WHERE `rid` = '".(int)$_GET['i']."'");
                    $modx->clearCache();
                    die(header("Location: ".$url."&get=filters"));
                    break;
                default:
                    $meta = $modx->db->query("SELECT * FROM `modx_a_meta_filter` ORDER BY `id` DESC ");
                    $tpl = "/tpl/filters.tpl";
                    break;
            }

        } else {
            $tiny_mce        = $shop->tinyMCE("description", "300px");
            $meta = $modx->db->query("SELECT * FROM `modx_a_meta_filter` ORDER BY `id` DESC ");
            $tpl = "/tpl/filters.tpl";
        }
        break;
    case "sitemap":
        if (isset($_POST['post'])) {
            file_put_contents(ROOT."/sitemap.php", $_POST['post']);
            $res['alert'] = $_lang['seo_success_algoritm'];
        }
        if ($_GET['do'] == "reconstruct") {
            $modx->invokeEvent("OnDocFormSave");
            $res['alert'] = $_lang['seo_success_sitemap'];
        }
        $tpl = "/tpl/sitemap.tpl";
        break;
    case "robots":
        if (isset($_POST['post'])) {
            file_put_contents($_SERVER['DOCUMENT_ROOT']."/robots.txt", $_POST['post']);
            $res['alert'] = $_lang['seo_success_robots'];
        }
        $tpl = "/tpl/robots.tpl";
        break;
    case "counters":
        if (isset($_POST) && count($_POST) > 0) {
            foreach ($_POST as $k => $v) {
                $modx->db->query("REPLACE INTO `modx_system_settings` (`setting_value`, `setting_name`) VALUES ('{$modx->db->escape($v)}', '{$k}')");
                $modx->config[$k] = $modx->db->escape($v);
            }
            $res['alert'] = $_lang['seo_success_settings'];
            include_once MODX_MANAGER_PATH."processors/cache_sync.class.processor.php";
            $sync = new synccache();
            $sync->setCachepath(MODX_BASE_PATH."assets/cache/");
            $sync->setReport(false);
            $sync->emptyCache(); // first empty the cache
            $modx->getSettings();
        }
        $tpl = "/tpl/counters.tpl";
        break;
    case "redirect":
        if (isset($_GET['delete']))
            $modx->db->query("delete from `modx_a_redirect` where redirect_id = '".$_GET['delete']."'");
        if (count($_POST['add']) > 0) {
            $modx->db->query("insert into `modx_a_redirect` set 
									redirect_code   = '".$modx->db->escape($_POST['add']['code'])."', 
									redirect_source = '".$modx->db->escape($_POST['add']['source'])."', 
									redirect_target = '".$modx->db->escape($_POST['add']['target'])."'");
        }
        if (count($_POST['edit']) > 0) {
            $modx->db->query("
                UPDATE `modx_a_redirect` set 
				    redirect_code     = '".$modx->db->escape($_POST['edit']['code'])."', 
					redirect_source   = '".$modx->db->escape($_POST['edit']['source'])."', 
					redirect_target   = '".$modx->db->escape($_POST['edit']['target'])."' 
				WHERE redirect_id = '".$modx->db->escape($_POST['edit']['redirect'])."'
			");
        }
        $redirects = $modx->db->query("select * from `modx_a_redirect` order by redirect_id desc limit 20");
        $tpl = "/tpl/redirect.tpl";
        break;
}

if (isset($tpl)) {
    ob_start();
    include ROOT.$tpl;
    $res['content'] = ob_get_contents();
    ob_end_clean();
}
include ROOT."/tpl/index.tpl";
die;
