<?php 
$description = [
	'as_gmaps_key' => 'Ключ для Google Maps',
	'as_address' => 'Адрес',
	'as_phone_1' => 'Телефон номер 1',
	'as_phone_2' => 'Телефон номер 2',
	'as_country_code' => 'Код страны',
	'as_facebook' => 'Facebook',
	'as_instagram' => 'Instagram',
	'as_' => 'Стоимость доставки',
	'as_delivery' => 'Стоимость доставки',
	'as_installation' => 'Стоимость установки',
];