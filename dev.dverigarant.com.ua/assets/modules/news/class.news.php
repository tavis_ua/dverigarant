<?php
if (!class_exists('News')) {
	/**
	 * Class News
	 */
	class News {
			var $modx = null;
			var $db   = null;

		function __construct ($modx, $table_name, $table_name_strings){
			$this->modx     = $modx;
			$this->db       = $modx->db;
			$this->tName    = $table_name;
			$this->tStrings = $table_name_strings;
		}

        function getAll($search = []) {
            $where = [];
            if (isset($search['srch'])) {
                $where[] = "(id = '".(int)$search['srch']."' OR id IN (SELECT rid FROM {$this->tStrings} WHERE `lang` = '{$this->modx->config['lang_default']}' AND title LIKE '%".$this->db->escape($search['srch'])."%'))";
            }
            if (isset($search['filter_publish'])) {
                $where[] = "published = '".(int)$search['filter_publish']."'";
            }

            if (count($where)) {
                $where = 'WHERE '.implode(' AND ', $where);
            } else {
                $where = '';
            }

		    $limit = 30;
            $page = isset($_REQUEST['p']) ? ($this->number($_REQUEST['p'])-1) * $limit : 0;

            $query = "
				SELECT SQL_CALC_FOUND_ROWS n.*,
				    (SELECT title FROM {$this->tStrings} WHERE `rid` = n.`id` AND `lang` = '{$this->modx->config['lang_default']}') AS 'title'
				FROM {$this->tName} AS n
				{$where}
				ORDER BY n.id DESC
				LIMIT {$page}, {$limit};
			";

            $res = $this->db->makeArray($this->db->query($query));
            $pages = $this->db->getRow($this->db->query("SELECT FOUND_ROWS() AS 'cnt'"));
            $this->modx->setPlaceholder("max_items", $pages['cnt']);
            $this->modx->setPlaceholder("limit_items", $limit);
            return $res;
        }

        function getOne($id) {
		    if ($id < 1) return [];
            return $this->db->getRow($this->db->query("SELECT * FROM $this->tName WHERE `id` = ".$id));
        }

        function getNewsProducts() {
            return $this->db->makeArray($this->db->query("
                SELECT `p`.`product_id`,
                    (SELECT `product_name` FROM `modx_a_product_strings` WHERE `string_locate` = 'ru' AND `product_id` = `p`.`product_id`) AS `product_name`
                FROM `modx_a_products` `p`
            "));
        }
        function getNewsCategory($cat_templ_id, $lang, $isfolder = null)
        {
            $res = null;
            $wherefolder = '';
            if (!$isfolder) $wherefolder = "AND isfolder = 0";
            $query = "
                SELECT
                  c.id,
                  c.pagetitle_$lang AS 'pagetitle',
                  (SELECT pagetitle_$lang FROM `modx_site_content` WHERE id = c.parent) AS 'parenttitle',
                  (SELECT pagetitle_$lang FROM `modx_site_content` WHERE id = (SELECT parent FROM `modx_site_content` WHERE id = c.parent)) AS 'parentuptitle'
                FROM `modx_site_content` c
                WHERE c.template = $cat_templ_id AND c.parent <> 0 $wherefolder
                ORDER BY parenttitle ASC, pagetitle ASC
                ";
            $result = $this->db->makeArray($this->db->query($query));
            foreach ($result as $value) {
                $res[$value['id']]['title'] = $value['pagetitle'].' ('.$value['id'].')';
                if ($value['parenttitle']) $res[$value['id']]['title'] = $value['parenttitle'].' -> '.$res[$value['id']]['title'];
                if ($value['parentuptitle']) $res[$value['id']]['title'] = $value['parentuptitle'].' -> '.$res[$value['id']]['title'];
            }
            return $res;
        }

		/**
		* getLangStrings
		* Получить переводы для записи
		*
		* @param integer $id ID записи
		* @param string $lang Язык
		* @return array
		*/
        function getLangStrings($id, $lang = 'ru')
        {
            if ($id < 1) return [];
            return $this->db->getRow($this->db->query("SELECT * FROM $this->tStrings WHERE rid = {$id} AND lang = '{$lang}'"));
        }

        public function saveNews($content)
        {
            date_default_timezone_set('UTC');
            $alias = $content['alias'] != '' ? $this->db->escape($content['alias']) : $this->setUrl($content[$this->modx->config['lang_default']]['title'], $this->tName, "alias");
            $alias = str_replace('/', '-', $alias);
            $published_at = strtotime($content['published_at']);
            $this->db->query("
                INSERT INTO $this->tName SET
                    alias         = '".$alias."',
                    news_products = '".implode(',',$content['news_products'])."',
                    news_category = '".implode(',',$content['news_category'])."',
                    published     = '".(int)$content['published']."',
                    published_at  = ".$published_at.",
                    created_at    = ".time()."
                ");
            $result = $this->db->getRow($this->db->query("SELECT LAST_INSERT_ID() AS last_id"));
            return $result['last_id'];
        }

        public function updateNews($content)
        {
            date_default_timezone_set('UTC');
            $alias = $content['alias'] != '' ? $this->db->escape($content['alias']) : $this->setUrl($content[$this->modx->config['lang_default']]['title'], $this->tName, "alias");
            $alias = str_replace('/', '-', $alias);
            $published_at = strtotime($content['published_at']);

            return $this->db->query("
                UPDATE $this->tName SET
                    alias         = '".$alias."',
                    news_products = '".implode(',',$content['news_products'])."',
                    news_category = '".implode(',',$content['news_category'])."',
                    published     = '".(int)$content['published']."',
                    published_at  = ".$published_at.",
                    updated_at    = ".time()."
                WHERE id = '".(int)$content['id']."'
            ");
        }

        public function saveNewsString($content, $lang = 'ru') // iconv_substr($string, 0 , 80 , “UTF-8” );
        {
            return $this->db->query("
                INSERT INTO $this->tStrings SET
                    lang                  = '".$lang."',
                    rid                   = '".(int)$content['id']."',
                    title                 = '".$this->db->escape(mb_substr($content[$lang]['title'], 0 , 255 , 'UTF-8' ))."',
                    description           = '".$this->db->escape(mb_substr($content[$lang]['description'], 0 , 255 , 'UTF-8' ))."',
                    content               = '".$this->db->escape($content[$lang]['content'])."',
                    seo_title             = '".$this->db->escape(mb_substr($content[$lang]['seo_title'], 0 , 255 , 'UTF-8' ))."',
                    seo_description       = '".$this->db->escape(mb_substr($content[$lang]['seo_description'], 0 , 255 , 'UTF-8' ))."',
                    seo_keywords          = '".$this->db->escape(mb_substr($content[$lang]['seo_keywords'], 0 , 255 , 'UTF-8' ))."',
                    seo_canonical         = '".$this->db->escape(mb_substr($content[$lang]['seo_canonical'], 0 , 255 , 'UTF-8' ))."',
                    seo_robots            = '".$this->db->escape(mb_substr($content[$lang]['seo_robots'], 0 , 255 , 'UTF-8' ))."'
                ON DUPLICATE KEY UPDATE
                    title                 = '".$this->db->escape(mb_substr($content[$lang]['title'], 0 , 255 , 'UTF-8' ))."',
                    description           = '".$this->db->escape(mb_substr($content[$lang]['description'], 0 , 255 , 'UTF-8' ))."',
                    content               = '".$this->db->escape($content[$lang]['content'])."',
                    seo_title             = '".$this->db->escape(mb_substr($content[$lang]['seo_title'], 0 , 255 , 'UTF-8' ))."',
                    seo_description       = '".$this->db->escape(mb_substr($content[$lang]['seo_description'], 0 , 255 , 'UTF-8' ))."',
                    seo_keywords          = '".$this->db->escape(mb_substr($content[$lang]['seo_keywords'], 0 , 255 , 'UTF-8' ))."',
                    seo_canonical         = '".$this->db->escape(mb_substr($content[$lang]['seo_canonical'], 0 , 255 , 'UTF-8' ))."',
                    seo_robots            = '".$this->db->escape(mb_substr($content[$lang]['seo_robots'], 0 , 255 , 'UTF-8' ))."'
                ");
        }

        public function deleteNews($cid)
        {
            $this->db->query("DELETE FROM $this->tName WHERE id = '".(int)$cid."' LIMIT 1");
            $this->db->query("DELETE FROM $this->tStrings WHERE rid = '".(int)$cid."'");
            $this->removeDirRecursive(MODX_BASE_PATH."/assets/images/news/".(int)$cid."/");
            $this->removeDirRecursive(MODX_BASE_PATH."/assets/cache/images/backendNews/".(int)$cid."/");
        }

        /**
         * dd
         * Дебаг переменной с прерыванием выполнения кода
         * помещает в себя любое количество переменных
         *
         * @param mixed $var Переменная или список переменных
         * через запятую для отображения их содержимого
         */
        public function dd(...$var)
        {
            echo "<pre>";
            foreach ($var as $v) {
                var_dump($v);
            }
            die;
        }

		function go($url){
			header("Location: ".$url);
			die;
		}

		function sanitar($var) {
			return $this->db->escape(strip_tags($var));
		}

		function number($var) {
			return preg_replace('/\D+/i', '', $var);
		}

		/**
		* tinyMCE
		* генерирует код необходимый для подключения tinyMCE
		*
		* @param string $ids Список id полей через запятую
		* @param string $height Высота окна
		* @return string
		*/
		public function tinyMCE($ids, $height = '500px')
		{
			$elements = [];
			$langs    = explode(",", $this->modx->config['lang_list']);
			$ids      = explode(",", $ids);
			foreach ($langs as $lang) {
				foreach ($ids as $id) {
					$elements[] = trim($lang)."_".trim($id);
				}
			}
			$editor = "TinyMCE4";
			return implode("", $this->modx->invokeEvent('OnRichTextEditorInit', array(
				'editor' => $editor,
				'elements' => $elements,
				'height' => $height
			)));
		}

        /**
         * setUrl
         * Генератор уникального URL в таблице
         *
         * @param string $name Строка для генерации
         * @param string $table Таблица для поиска
         * @param string $field Поле для поиска
         * @return string
         */
        private function setUrl($name, $table, $field)
        {
            include_once MODX_BASE_PATH . "assets/plugins/transalias/transalias.class.php";
            $transalias = new TransAlias;
            $remove_periods = 'No';
            $table_name = 'russian';
            $transalias->loadTable($table_name, $remove_periods);
            $char_restrict = 'alphanumeric';
            $word_separator = 'dash';
            $alias = $transalias->stripAlias($name,$char_restrict,$word_separator);
            $alias = strip_tags($alias);
            $alias = strtolower($alias);
            $alias = preg_replace('/[^\.A-Za-z0-9 _-]/', '', $alias); // strip non-alphanumeric characters
            $alias = preg_replace('/\s+/', '-', $alias); // convert white-space to dash
            $alias = preg_replace('/-+/', '-', $alias);  // convert multiple dashes to one
            $alias = trim($alias, '-'); // trim excess
            $x = 0;
            do {
                $newAlias = $alias.($x == 0 ? '' : $x);
                $x++;
            } while ($this->db->getRow($this->db->query("SELECT * FROM {$table} WHERE $field = '".$newAlias."'")));
            return  $newAlias;
        }

		public function getDate($date, $from = 'U', $to = 'd.m.Y')
        {
            $resdt = DateTime::createFromFormat($from, $date);
            return $resdt->format($to);
        }

        public function getPaginate($tpl_enable = 'pagination_backend_enable', $tpl_able = 'pagination_backend_able', $classJS = ' js_change_page') {
			if($this->modx->getPlaceholder("max_items") != '' && $this->modx->getPlaceholder("limit_items") != ''){
				$pages['all']   = ceil($this->modx->getPlaceholder("max_items") / $this->modx->getPlaceholder("limit_items"));
			}

			$prev           = $prev != '' ? $prev : '&laquo;';
			$next           = $next != '' ? $next : '&raquo;';
			$classPrev      = ($classPrev != '' ? $classPrev : 'prev').$classJS;
			$classNext      = ($classNext != '' ? $classNext : 'next').$classJS;
			$classPage      = ($classPage != '' ? $classPage : 'page').$classJS;
			$classCurrent   = $classCurrent != '' ? $classCurrent : 'active';
			$classSeparator = $classSeparator != '' ? $classSeparator : 'separator';
			$separator      = $separator != '' ? $separator : '...';
			$url            = $this->modx->getPlaceholder("url") != '' ? $this->modx->getPlaceholder("url") : '';

			if($pages['all'] > 1) {
				$limit_pagination = ($this->modx->documentObject['id'] != 12) ? $this->modx->getPlaceholder("limit_items") : 5;
				//$curent = isset($_REQUEST['p']) ? $_REQUEST['p'] : 1;
				if(isset($_GET['p'])) {
					$curent = isset($_REQUEST['p']) ? $_REQUEST['p'] : 1;
				} else {
					$curent = ($this->modx->getPlaceholder("page")) ? $this->modx->getPlaceholder("page") : 1;
				}

				$start  = $curent - $limit_pagination;
				$end    = $curent + $limit_pagination;
				$start  = $start < 1 ? 1 : $start;
				$end    = $end > $pages['all'] ? $pages['all'] : $end;

				if($curent > 1) {
					$res .= $this->modx->parseChunk($tpl_enable , array('url' => $url."1/", 'class' => $classPrev, 'title' => $prev, 'rel' => "prev"));
				}

				if($curent > $limit_pagination+1) {
					$res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.'1/', 'class' => $classPage, 'title' => '1', 'rel' => "prev")).$this->modx->parseChunk($tpl_disable, array('class' => $classSeparator, 'title' => $separator));
				}

				for($i=$start; $i<$end+1; $i++) {                         
					if($i==($curent)) {
						$res .= $this->modx->parseChunk($tpl_disable, array('class' => $classCurrent, 'title' => $i, 'rel' => ""));
					} else {
						$res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.$i."/", 'class' => $classPage, 'title' => $i,  "rel" => ($i < $curent) ? "prev" : "next"));
					}
				}
				if($curent < ($pages['all'] - $limit_pagination)) {
					$res .= $this->modx->parseChunk($tpl_disable, array('class' => $classSeparator, 'title' => $separator)).$this->modx->parseChunk($tpl_enable , array('url' => $url.$pages['all']."/", 'class' => $classPage, 'title' => $pages['all'], 'rel' => "next"));
				}

				if($curent != $pages['all']) {
					$res .= $this->modx->parseChunk($tpl_enable , array('url' => $url.$pages['all']."/", 'class' => $classNext, 'title' => $next, 'rel' => "next"));        
				}
			}

			return $res;
			}

        /**
         * removeDirRecursive
         * Рекурсивное удаление папки
         *
         * @param $dir
         */
        private function removeDirRecursive($dir)
        {
            if ($objs = glob($dir."/*")){
                foreach($objs as $obj){
                    is_dir($obj) ? $this->removeDirRecursive($obj) : unlink($obj);
                }
            }
            rmdir($dir);
        }
	}
}