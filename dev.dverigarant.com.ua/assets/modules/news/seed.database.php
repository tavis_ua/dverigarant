<?php
/**
 * Создать и заполнить данными таблицы модуля
 * Док аннотация модуля должна содержать строку вида: * @internal    @dependencies requires files located at /assets/modules/domains/
 * @builder - обязательный параметр.
 * `{PREFIX_TABLE_NAME}` - автоматически формирует валидное имя таблицы.
 * `{PREFIX_categories}` - автоматически формирует префикс имени таблицы.
 * {CURRENT_URL} - автоматически формирует ссылку на текущий сайт.
 *
 * "имя_таблицы" => [
 *     "builder" => "строка SQL запроса для создания структуры таблицы",
 *     "seeder" => "строка SQL запроса для наполнения таблицы начальными данными",
 * ]
 */
return [
	"a_news" => [
	    "builder" => "
            CREATE TABLE IF NOT EXISTS `{PREFIX_TABLE_NAME}` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
                `alias` varchar(255) NOT NULL COMMENT 'Alias',
                `image` varchar(255) DEFAULT NULL COMMENT 'Image link',
                `published` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Published',
                `gallery` text COMMENT 'Gallery',
                `home_at` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Show at home page',
                `news_products` varchar(255) DEFAULT NULL
                `published_at` int(10) NOT NULL DEFAULT '0' COMMENT 'Published time',
                `created_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Create time',
                `updated_at` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Update time',
                PRIMARY KEY (`id`),
                UNIQUE KEY `alias` (`alias`),
                KEY `published` (`published`),
                KEY `published_time` (`published_at`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    "
    ],
    "a_new_strings" => [
	    "builder" => "
            CREATE TABLE IF NOT EXISTS `{PREFIX_TABLE_NAME}` (
                `rid` int(11) unsigned NOT NULL COMMENT 'Related news ID',
                `lang` varchar(16) NOT NULL COMMENT 'Language',
                `title` varchar(255) NOT NULL COMMENT 'Title',
                `description` varchar(255) DEFAULT NULL COMMENT 'Description',
                `content` text COMMENT 'Content',
                `seo_title` varchar(255) DEFAULT NULL,
                `seo_description` varchar(255) DEFAULT NULL,
                `seo_keywords` varchar(255) DEFAULT NULL,
                `seo_canonical` varchar(255) DEFAULT NULL,
                `seo_robots` varchar(255) DEFAULT NULL,
                UNIQUE KEY `rid` (`rid`,`lang`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    "
    ],
];