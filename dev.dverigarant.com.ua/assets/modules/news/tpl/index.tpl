<div class="notifier">
    <div class="notifier-txt"></div>
</div>
<h1 class="pagetitle">
    <span class="pagetitle-icon"><i class="fa fa-calendar"></i></span>
    <span class="pagetitle-text"><?=$_lang['news_manage_article'];?></span>
</h1>
<div class="sectionBody">
    <div class="dynamic-tab-pane-control tab-pane">
        <div class="tab-row">
            <h2 class="tab<?=($get=='news'?' selected':'');?>"><a href="<?=$url?>news"><span><i class="fa fa-rss"></i> <?=$_lang['news'];?></span></a></h2>
        </div>
        <div class="tab-page" id="tabContents">
            <?=$res['content']?>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.title = "<?=$res['title']?> <?=$res['version']?>";
    jQuery(document).ready(function() {
        jQuery(".bsw").bootstrapSwitch();
        jQuery(".chosen-select").chosen({width:"100%"});
        jQuery(".select2").select2();
    });
    // Валидация и сохранение формы
    function saveForm(selector) {
        var errors    = 0;
        var messages  = "";
        var validates = jQuery(selector+" [data-validate]");
        validates.each(function(k, v) {
            var rule = jQuery(v).attr("data-validate").split(":");
            switch (rule[0]) {
                case "inputMaxSum":
                    if (parseFloat(jQuery(v).val()) > parseFloat(rule[1])) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
                case "textMaxLenght":
                    if (jQuery(v).val().length > parseInt(rule[1])) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
                case "textNoEmpty": // Не пустое поле
                    if (jQuery(v).val().length < 1) {
                        messages = messages + jQuery(v).parent().find(".error-text").text() + "<br/>";
                        jQuery(v).parent().removeClass("has-success").addClass("has-error");
                        errors = errors + 1;
                    } else {
                        jQuery(v).parent().removeClass("has-error").addClass("has-success");
                    }
                    break;
            }
        });
        if (errors == 0) {
            jQuery(selector).submit();
        } else {
            jQuery('.notifier').addClass("notifier-error");
            jQuery('.notifier').fadeIn(700);
            jQuery('.notifier').find('.notifier-txt').html(messages);
            setTimeout(function() {
                jQuery('.notifier').fadeOut(7000);
            },2000);
            setTimeout(function() {
                jQuery('.notifier').removeClass("notifier-error");
            },7000);
        }
    }
</script>
<?php if (isset($_SESSION['notifier_status']) && isset($_SESSION['notifier_text'])) :?>
<script type="text/javascript">
    jQuery('.notifier').addClass("notifier-<?=$_SESSION['notifier_status']?>");
    jQuery('.notifier').fadeIn(700);
    jQuery('.notifier').find('.notifier-txt').html("<?=$_SESSION['notifier_text']?>");
    setTimeout(function() {
        jQuery('.notifier').fadeOut(7000);
    },2000);
    setTimeout(function() {
        jQuery('.notifier').removeClass("notifier-<?=$_SESSION['notifier_status']?>");
    },7000);
</script>
<?php unset($_SESSION['notifier_status']); ?>
<?php unset($_SESSION['notifier_text']); ?>
<?php endif; ?>