<?php if (!isset($content['id']) || (int)$content['id'] == 0): ?>
<form id="editForm" action="<?=$url?>news&c=save" method="post">
    <h2 class="muted"> <?=$_lang['news_addition'];?> <small> <?=$_lang['news'];?> </small></h2>
<?php else: ?>
    <form id="editForm" action="<?=$url?>news&c=update" method="post">
        <h2 class="muted">&emsp;<?=$_lang['news_editing'];?> <small>(<?=$content[$modx->config['lang_default']]['title']?>)</small></h2>
<?php endif; ?>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="active"><a href="#product" role="tab" data-toggle="tab"><b><i class="fa fa-id-card" aria-hidden="true"></i> <?=$_lang['news_main'];?> </b></a></li>
        <?php foreach ($languages as $k => $lng): ?>
            <li role="presentation"><a href="#<?=$lng?>" data-toggle="tab" class="text-uppercase"><b><i class="fa fa-globe" aria-hidden="true"></i> <?=$lng?> </b></a></li>
        <?php endforeach; ?>
        <?php if ((int)$content['id'] > 0): ?>
            <li role="presentation" id="mediaTab"><a href="#media" role="tab" data-toggle="tab"><b><i class="fa fa-picture-o" aria-hidden="true"></i> <?=$_lang['news_img']?> </b></a></li>
        <?php endif; ?>
    </ul>
    <br />
    <div class="container-fluid">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="product">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" width="160px"> <?=$_lang['news_visible']?>:</th>
                        <td>
                            <div class="bootstrap-switch">
                                <input type="checkbox" name="content[published]" <?=($content['published'] ? "checked" : "")?> value="1" data-on-color="success" data-off-color="danger" data-label-text="&nbsp;" data-on-text="<?=$_lang['news_published']?>" data-off-text="<?=$_lang['news_unpublished']?>" class="bsw">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th> <?=$_lang['news_publish_date']?>:</th>
                        <td>
                            <div class="input-group input-append dt date" style="width:200px;">
                                <input data-format="dd.MM.yyyy" type="text" name="content[published_at]" class="form-control add-on"  id="datetimepicker1" value="<?=date('d.m.Y', ((int)$content['published_at'] > 0 ? $content['published_at'] : time()));?>"/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <!--<p class="text-info text-justify"><b><small>Если оставить дату публикации в будущем и статус "Опубликована", то статья будет опубликована автоматически.</small></b></p>-->
                        </td>
                    </tr>
                    <tr>
                        <th class="vertical-align"> <?=$_lang['news_link']?>:</th>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon"><?=$modx->makeUrl($modx->config['news_catalog_root'], '', '', 'full')?></span>
                                <input type="text" class="form-control" id="basic-url" name="content[alias]" value="<?=$content['alias']?>" onchange="documentDirty=true;" />
                                <span class="input-group-btn actionButtons"><a href="<?=$modx->makeUrl($modx->config['news_catalog_root'], '', '', 'full')?><?=$content['alias']?>/" target="_blank"><i class="fa fa-external-link-square"></i>&emsp;Перейти!</a></span>
                            </div>
                            &emsp;<small class="text-info"> <?=$_lang['news_link_msg']?> </small>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Товары из этой статьи:</b></td>
                        <td>
                            <select name="content[news_products][]"  class="form-control chosen-select nc" data-placeholder="Товары из этой статьи" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($news_products as $news_product) :?>
                                <option value="<?=$news_product['product_id']?>"<?php if(is_array($content['news_products']) && in_array($news_product['product_id'],  $content['news_products'])) echo " selected='selected'";?>><?=$news_product['product_name']?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Категории из этой статьи:</b></td>
                        <td>
                            <select name="content[news_category][]"  class="form-control chosen-select nc" data-placeholder="Выберите категорию" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($news_categories as $id => $title): ?>
                                <option value="<?=$id;?>" <?php if(is_array($content['news_category']) && in_array($id, $content['news_category'])) echo 'selected'; ?>><?=$title['title'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <?php foreach ($languages as $k => $lng): ?>
                <div role="tabpanel" class="tab-pane" id="<?=$lng?>">
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <th class="vertical-align" width="160px"> <?=$_lang['news_title']?>:</th>
                            <td>
                                <input type="text" name="content[<?=$lng?>][title]" value="<?=$content[$lng]['title']?>" class="form-control" data-validate="textMaxLenght:255"/>
                                <div class="error-text" style="display:none;"> <?=$_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?= $_lang['news_error_title_msg']?> </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align"> <?=$_lang['news_description']?>:</th>
                            <td>
                                <textarea id="<?=$lng?>_description" name="content[<?=$lng?>][description]" class="form-control" rows="5" data-validate="textMaxLenght:255"><?=$content[$lng]['description']?></textarea>
                                <div class="error-text" style="display:none;"> <?php $_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?=$_lang['news_error_description_msg']?> </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="vertical-align"> <?=$_lang['news_content']?>:</th>
                            <td>
                                <textarea id="<?=$lng?>_content" name="content[<?=$lng?>][content]" class="form-control" rows="5"><?=$content[$lng]['content']?></textarea>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="vertical-align"> <?=$_lang['news_seo_title']?>:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="content[<?=$lng?>][seo_title]" value="<?=$content[$lng]['seo_title']?>" class="form-control" data-validate="textMaxLenght:70" />
                                    <div class="error-text" style="display:none;"> <?=$_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?=$_lang['news_error_seo_title_msg']?> </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default"> <?=$_lang['news_seo_description']?>:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="content[<?=$lng?>][seo_description]" value="<?=$content[$lng]['seo_description']?>" class="form-control" data-validate="textMaxLenght:255" />
                                    <div class="error-text" style="display:none;"> <?=$_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?=$_lang['news_error_seo_description_msg']?> </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default"> <?=$_lang['news_seo_keywords']?>:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="content[<?=$lng?>][seo_keywords]" value="<?=$content[$lng]['seo_keywords']?>" class="form-control" data-validate="textMaxLenght:70" />
                                    <div class="error-text" style="display:none;"> <?=$_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?=$_lang['news_error_seo_keywords_msg']?> </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default"> <?=$_lang['news_seo_canonical']?>:</th>
                            <td>
                                <div class="">
                                    <input type="text" name="content[<?=$lng?>][seo_canonical]" value="<?=$content[$lng]['seo_canonical']?>" class="form-control" data-validate="textMaxLenght:255" />
                                    <div class="error-text" style="display:none;"> <?=$_lang['news_error_count']?> <b>"<?=strtoupper($lng)?> <?=$_lang['news_error_seo_canonical_msg']?> </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="bg-default">
                            <th class="bg-default"> <?=$_lang['news_seo_robots']?>:</th>
                            <td>
                                <select class="form-control" name="content[<?=$lng;?>][seo_robots]" style="width: 300px;">
                                    <option value="index,follow" <?php if($content[$lng]['seo_robots'] == 'index,follow') echo 'selected'; ?>>index, follow</option>
                                    <option value="noindex,nofollow" <?php if($content[$lng]['seo_robots'] == 'noindex,nofollow') echo 'selected'; ?>>noindex, nofollow</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php endforeach; ?>
            <div role="tabpanel" class="tab-pane" id="media">
                <!--<p>Внимание! Для корректного отображения изображений товара, картинки должны иметь соотношение сторон <b style="color:red;">ширина:высота</b> как <b style="color:red;">1:1.4</b>. Идеальный размер <b style="color:red;">800:1120 пикс</b>.</p>-->
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th class="vertical-align" style="text-align:center;" width="160px">
                            <a href="#" id="cover" class="btn btn-primary"><i class="fa fa-camera-retro"></i>&emsp;<?=$_lang['news_preview']?></a>
                        </th>
                        <td>
                            <ul class="cover"></ul>
                        </td>
                    </tr>
                    <!--<tr>
                        <th class="vertical-align" style="text-align:center;">
                            <a href="#" id="slider" class="btn btn-primary"><i class="fa fa-slideshare"></i>&emsp;<?=$_lang['news_slider']?></a>
                        </th>
                        <td>
                            <ul class="slider"></ul>
                        </td>
                    </tr>-->
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" name="content[id]" value="<?=$content['id']?>" />
    <div id="actions">
        <ul class="actionButtons">
            <li id="Button1" class="transition">
                <a href="#" class="primary" onclick="documentDirty=false;saveForm('#editForm');"><i class="fa fa-save"></i>&emsp;<?=$_lang['news_save']?></a>
            </li>
        </ul>
    </div>
</form>
<div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="translates" action="<?=$url?>&b=save_image_fields" method="post">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?=$_lang['news_img_edit']?></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> <?=$_lang['news_cancel']?></button>
                    <button type="submit" class="btn btn-primary pull-right"> <?=$_lang['news_save']?> <span class="glyphicon glyphicon-ok-sign"></span></button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php if ((int)$content['id'] > 0): ?>
    <script src="/assets/site/ajaxupload.js"></script>
    <script type="text/javascript">
        jQuery(document).on("click", "#mediaTab", function() {
            getCover();
            //getSlider();
            //jQuery(".slider").sortable();
        });

        jQuery(document).on("click", ".js_delete", function() {
            t = jQuery(this).prevAll("img");
            c = jQuery(this).parents('ul').attr('class');
            if (c == 'cover') {
                act = 'delete_cover';
            } else {
                act = 'delete_image';
            }
            jQuery.ajax({
                url:"<?=$url?>images",
                type:"GET",
                data:"c="+act+"&folder=<?=$content['id']?>&delete="+t.attr("alt"),
                success:function(ajax){
                    t.parents("li").fadeOut().remove();
                }
            });
        });

        jQuery(document).on("click", '#Button1', function() {
            jQuery.ajax({url:"<?=$url?>images&c=set_sort&folder=<?=$content['id']?>",type:"POST",data:jQuery('.slider input').serialize()});
            return true;
        });

        function getCover() {
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_cover&folder=<?=$content['id']?>",success:function(ajax){
            jQuery(".cover").html(ajax);
            }});
        }

        function getSlider(){
            jQuery.ajax({url:"<?=$url?>images",type:"GET",data:"c=get_slider&folder=<?=$content['id']?>", success:function(ajax){
            jQuery(".slider").html(ajax);
            }});
        }

        jQuery(document).ready(function(){
            new AjaxUpload(jQuery("#cover"), {
                action: "<?=$url?>images&c=set_cover",
                multiple: false,
                name: "uploader[]",
                data: {
                    "size"  : 2048576,
                    "folder": '<?=$content["id"]?>'
                },
                onSubmit: function(file, ext){
                    if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
                        alert('Только jpg png файлы');
                        return false;
                    }
                },
                onComplete: function(file, response){
                    getCover();
                }
            });

            // new AjaxUpload(jQuery("#slider"), {
            //     action: "<?=$url?>images&c=set_slider",
            //     multiple: true,
            //     name: "uploader[]",
            //     data: {
            //         "size"  : 2048576,
            //         "folder": '<?=$content["id"]?>'
            //     },
            //     onSubmit: function(file, ext){
            //         if (! (ext && /^(jpg|png|jpeg|JPG|PNG|JPEG)$/.test(ext))){
            //             alert('Только jpg png файлы');
            //             return false;
            //         }
            //     },
            //     onComplete: function(file, response){
            //         getSlider();
            //     }
            // });
        });
    </script>
<?php endif; ?>
<?=$tiny_mce?>