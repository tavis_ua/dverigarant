// <?php 
/**
 * News
 * 
 * The module allows you to management news articles
 * 
 * @category	module
 * @version 	1.0
 * @internal	@properties
 * @internal	@guid	
 * @internal	@shareparams 1
 * @internal	@moduleshow 1
 * @internal	@dependencies requires files located at /assets/modules/news/
 * @internal	@modx_category Manager and Admin
 * @internal	@installset base, sample
 * @author	Seiger
 * @lastupdate	11/06/2018
 */

require MODX_BASE_PATH . "assets/modules/news/index.php";