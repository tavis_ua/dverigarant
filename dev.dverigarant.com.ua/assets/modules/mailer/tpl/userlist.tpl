<p>
	<form action="<?=$url?>&get=userlist" method="post" enctype="multipart/form-data" class="form-inline">
	<div class="input-group">
		<input name="file" type="file"/><br>
		<span class="input-group-btn">
        	<button name="submit" value="import_xlsx" class="btn btn-success"><span class="glyphicon glyphicon-import"></span>&emsp;<?=$_lang["mailer_import_xlsx"]?></button>
		</span>
		</div>
		<a href="<?=$url?>&get=userlist&i=import" class="btn btn-success"><span class="glyphicon glyphicon-repeat"></span>&emsp;<?=$_lang["mailer_import_users"]?></a>
		<a href="<?=$url?>&get=userlist&i=export" class="btn btn-success"><span class="glyphicon glyphicon-export"></span>&emsp;<?=$_lang["mailer_export_users"]?></a>
	</form>
</p>
<table class="table table-bordered table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th>ID</th>
			<th><?=$_lang["mailer_name"]?></th>
			<th>Email</th>
			<th><?=$_lang["mailer_actions"]?></th>
		</tr>
	</thead>
	<tbody>
		<?php while($u = $modx->db->getRow($users)): ?>
			<tr>
				<td><?=$u['user_id']?></td>
				<td><?=$u['user_name']?></td>
				<td><?=$u['user_email']?></td>
				<td>
					<a href="<?=$url?>&get=userlist&d=<?=$u['user_id']?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span> <?=$_lang["mailer_del_user"]?></a>
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
</table>