<p>
	<a href="<?=$url?>&get=templates&do=add" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span>&emsp;<?=$_lang["mailer_add_tpl"]?></a>
</p>

<table class="table table-condensed table-striped table-hover table-bordered">
	<thead>
		<tr>
			<th>id</th>
			<th><?=$_lang["mailer_name_tpl"]?> (<?=$_lang["mailer_theme_mail"]?>)</th>
			<th><?=$_lang["mailer_date_tpl"]?></th>
			<th><?=$_lang["mailer_actions"]?></th>
		</tr>
	</thead>
	<tbody>
		<?php while ($r = $modx->db->getRow($templates)): ?>
			<tr>
				<td><?=$r['template_id']?></td>
				<td><?=$r['template_subject']?></td>
				<td><?=$r['template_date']?></td>
				<td>
					<a href="<?=$url?>&get=templates&do=edit&t=<?=$r['template_id']?>" title="<?=$_lang["mailer_edit"]?>" class="btn btn-xs btn-success update"><span class="glyphicon glyphicon-edit"></span>&emsp;<?=$_lang["mailer_edit"]?></a>&emsp;
					<a href="<?=$url?>&get=templates&delete=<?=$r['template_id']?>" title="<?=$_lang["mailer_delete"]?>" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span>&emsp;<?=$_lang["mailer_delete"]?></a>&emsp;
				</td>
			</tr>
		<?php endwhile; ?>
	</tbody>
</table>