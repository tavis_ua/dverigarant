<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <title>Mailer <?=$res['version']?></title>
    <meta charset="UTF-8">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/assets/site/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/site/bootstrap3/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/assets/site/bootstrap3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/assets/site/bootstrap3/css/bootstrap-switch.min.css">
    <link rel="stylesheet" href="/assets/site/interface.css" />
    <link rel="stylesheet" href="/assets/site/chosen/chosen.min.css" />
    <link rel="stylesheet" href="/assets/site/mm-autocomplete.css">
    <script src="/assets/site/jquery-2.1.4.min.js"></script>
    <script src="/assets/site/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/assets/site/bootstrap3/js/bootstrap-switch.min.js"></script>
    <script src="/assets/site/bootstrap3/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/assets/site/chosen/chosen.jquery.min.js"></script>
    <script src="/assets/site/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript" src="/assets/site/mm-autocomplete.js"></script>
    <style type="text/css">
        form {margin-bottom:0px;}
        input[type=text] {margin-bottom:0px;}
        .navbar-inner {padding:0px;}
        #EditAreaArroundInfos_myeditarea {display: none;}
        .progress {margin-bottom: 0px!important;}
        kbd {
            padding: 0 3px; border-width: 2px 3px 4px; border-style: solid; border-color: #cdcdcd #c9c9c9 #9a9a9a #c0c0c0;
            font-size: 11px; color: #000; background: #ececec;
            -moz-border-radius: 2px; -webkit-border-radius: 2px; border-radius: 2px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $("[data-active]").each(function(){
               _option = $(this).find("option[value='"+$(this).attr("data-active")+"']");
               _option.prop("selected", true);
            });
            $("[title]").tooltip();
        });
    </script>
</head>
<body>
    <div class="container">
        <p></p>
        <nav class="navbar navbar-inverse">
          <ul class="nav navbar-nav">
            <li<?=$get == "" ? ' class="active"' : '' ?>><a href="<?=$url?>"><span class="glyphicon glyphicon-stats"></span>&emsp;<?=$_lang["mailer_analitycs"]?></a></li>
            <li<?=$get == "templates" ? ' class="active"' : '' ?>><a href="<?=$url?>&get=templates"><span class="glyphicon glyphicon-envelope"></span>&emsp;<?=$_lang["mailer_tpl"]?></a></li>
            <li<?=$get == "userlist" ? ' class="active"' : '' ?>><a href="<?=$url?>&get=userlist"><span class="glyphicon glyphicon-user"></span>&emsp;<?=$_lang["mailer_list_mail"]?></a></li>
            <li<?=$get == "config" ? ' class="active"' : '' ?>><a href="<?=$url?>&get=config"><span class="glyphicon glyphicon-wrench"></span>&emsp;<?=$_lang["mailer_config"]?></a></li>
          </ul>
        </nav>
        <?php if (isset($res['alert'])): ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong><?=$res['alert']?></strong>
            </div>
        <?php endif; ?>
        <?=$res['content']?>
    </div>
</body>
</html>