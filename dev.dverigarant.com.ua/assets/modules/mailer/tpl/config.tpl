<form action="<?=$url?>&get=config" method="post">
	<table class="table table-bordered table-striped table-hover table-condensed">
		<tr>
			<td><b><?=$_lang["mailer_server"]?></b></td>
			<td>
				<div class="col-md-8">
					<input type="text" name="mailer_smtp_server" value="<?=$modx->config['mailer_smtp_server']?>" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td><b><?=$_lang["mailer_port"]?></b></td>
			<td>
				<div class="col-md-2">
					<input type="text" name="mailer_smtp_port" value="<?=$modx->config['mailer_smtp_port']?>" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td><b><?=$_lang["mailer_login"]?></b></td>
			<td>
				<div class="col-md-8">
					<input type="text" name="mailer_smtp_login" value="<?=$modx->config['mailer_smtp_login']?>" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td><b><?=$_lang["mailer_pass"]?></b></td>
			<td>
				<div class="col-md-8">
					<input type="text" name="mailer_smtp_pass" value="<?=$modx->config['mailer_smtp_pass']?>" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td><b><?=$_lang["mailer_from"]?></b></td>
			<td>
				<div class="col-md-8">
					<input type="text" name="mailer_smtp_name" value="<?=$modx->config['mailer_smtp_name']?>" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td><b><?=$_lang["mailer_mail_cnt"]?></b></td>
			<td>
				<div class="col-md-4">
					<input type="text" name="mailer_smtp_limit" value="<?=$modx->getConfig('mailer_smtp_limit')?>" class="form-control">
				</div>
			</td>
		</tr>
	</table>
	<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span>&emsp;<?=$_lang["mailer_save"]?></button>
</form>