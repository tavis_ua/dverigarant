<form action="<?=$url?>&get=templates" method="post">
	<p><?=$_lang["mailer_tpl_i"]?>:</p>
	<ul>
		<li>&emsp;&emsp;<span class="glyphicon glyphicon-leaf"></span>&emsp;<?=$_lang["mailer_tpl_i2"]?> <b>{username}</b></li>
		<li>&emsp;&emsp;<span class="glyphicon glyphicon-leaf"></span>&emsp;<?=$_lang["mailer_tpl_i3"]?> <b>{unsubscribe_link}</b></li>
	</ul>
	<input type="text" name="theme" placeholder="<?=$_lang["mailer_mail_tpl_name"]?>" title="<?=$_lang["mailer_mail_tpl_name"]?>" autofocus class="form-control" value="<?=$template['template_subject']?>">
	<br/>
	<textarea name="post" class="col-md-8"><?=$template['template_post']?></textarea>
	<div class="pull-left col-md-4">
		<kbd>Ctrl</kbd> + <kbd>H</kbd> - <?=$_lang["mailer_tpl_i4"]?>
	</div>
	<div class="pull-left col-md-4">
		<kbd>Ctrl</kbd> + <kbd>,</kbd> - <?=$_lang["mailer_tpl_i5"]?>
	</div>
	<p>&nbsp;</p>
	<input type="hidden" name="tpl" value="<?=$template['template_id']?>">
	<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span>&emsp;<?=$_lang["mailer_save"]?></button>
</form>
<?=implode("",$modx->invokeEvent("OnTempFormRender"));?>