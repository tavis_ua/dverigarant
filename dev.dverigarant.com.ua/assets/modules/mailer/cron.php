<?php
/*
 *	Модуль рассылки
 */
	// error_reporting(E_ALL);
	define (ROOT, dirname(__FILE__));
	error_reporting(E_ALL);
	require_once(ROOT.'/../../../inspiration/includes/protect.inc.php');
	include_once ROOT.'/../../../inspiration/includes/config.inc.php';
	define('MODX_API_MODE', true);
    include_once(ROOT.'/../../../inspiration/includes/document.parser.class.inc.php');
    include_once(ROOT.'/../../../inspiration/includes/controls/phpmailer/class.phpmailer.php');
	$modx = new DocumentParser();
	$modx->db->connect();
	$modx->getSettings();
	global $modx;
	startCMSSession();

	$lim   = $modx->config['mailer_smtp_limit'] == "" ? 10 : $modx->config['mailer_smtp_limit'];
	$query = "select * 
			  from `modx_a_mailer` m 
			  join `modx_a_mailer_templates` t on t.template_id = m.tpl_id 
			  join `modx_a_mailer_users` 	 u on u.user_id = m.user_id 
			  where m.mailed = 0
			  limit $lim ";
	$list  = $modx->db->query($query);
	$host  = parse_url($modx->config['site_url']);
	$host  = $host['scheme']."://".$host['host']."/".($modx->config['lang_enable'] ? $modx->config['lang_default']."/" : "");

	while ($l = $modx->db->getRow($list)) {
		$letter = str_replace("{username}", $l['user_name'], $l['template_post']);
		$theme  = str_replace("{username}", $l['user_name'], $l['template_subject']);
		$letter = str_replace("{unsubscribe_link}", $host."?unsubscribe=".$l['user_email'], $letter);
		$mail =  new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug  = 0;
        $mail->SMTPAuth   = true;
        if ($modx->config['mailer_smtp_ssl'] == 1) $mail->SMTPSecure = "ssl";
        if ($modx->config['mailer_smtp_tls'] == 1) $mail->SMTPSecure = "tls";
        $mail->Host       = $modx->config['mailer_smtp_server'];
        $mail->Port       = $modx->config['mailer_smtp_port'];
        $mail->Username   = $modx->config['mailer_smtp_login'];
        $mail->Password   = $modx->config['mailer_smtp_pass'];
        $mail->Subject    = $theme;
        $mail->SetFrom($mail->Username, $modx->config['mailer_smtp_name']);
        $mail->AddAddress($l['user_email']);
        $mail->MsgHTML($letter);
        $mail->Send();
		$modx->db->query("update `modx_a_mailer` set mailed = 1 where user_id = '".$l['user_id']."' and tpl_id = '".$l['template_id']."'");
	}
	echo $modx->db->getRecordCount($list);
	die(".");