<?php
	define("ROOT", dirname(__FILE__));
	$get            = isset($_GET['get']) ? $_GET['get'] : "";
	$res            = Array();
	$res['version'] = "v 2.1";
	$res['url']     = $table['url'] = $url = "index.php?a=112&id=".$_GET['id']."";
	$res['get']     = $_GET['get'];
	error_reporting(0);
	// Подключение переводов админки
	include MODX_MANAGER_PATH . "includes/lang/".$modx->config['manager_language'].".inc.php";
    // Подключение библиотек работы с Exel
    require MODX_BASE_PATH."assets/lib/vendor/autoload.php";
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\IOFactory;
	switch ($get) {		
		case "templates":
			switch ($_GET['do']) {
				case "add":
					$tpl = "/tpl/templates_add.tpl";
				break;
				case "edit":
					$template = $modx->db->getRow($modx->db->query("SELECT * FROM `modx_a_mailer_templates` WHERE template_id = ".$_GET['t']));
					$tpl      = "/tpl/template_edit.tpl";
				break;
				default:
					if (isset($_POST['post']) && !isset($_POST['tpl'])) {
					 	$modx->db->query("INSERT INTO `modx_a_mailer_templates` SET 
											template_subject = '".$modx->db->escape($_POST['theme'])."', 
											template_post    = '".$modx->db->escape($_POST['post'])."'");
					 	$id = $modx->db->getInsertId();
					 	$modx->db->query("INSERT INTO `modx_a_mailer` (tpl_id, user_id) SELECT '".$id."' as 'tpl_id', u.user_id from `modx_a_mailer_users` u ON DUPLICATE KEY UPDATE user_id = u.user_id");
					 	$res['alert'] = $_lang["mailer_added_tpl"];
					} elseif (isset($_POST['post']) && isset($_POST['tpl'])) {
					 	$modx->db->query("update `modx_a_mailer_templates` set 
											template_subject  = '".$modx->db->escape($_POST['theme'])."', 
											template_post     = '".$modx->db->escape($_POST['post'])."'
											where template_id = '".$modx->db->escape($_POST['tpl'])."'");
					 	$res['alert'] = $_lang["mailer_update_tpl"];
					}
					if (isset($_GET['delete'])) {
						$modx->db->query("delete from `modx_a_mailer` where tpl_id = '".$_GET['delete']."'");
						$modx->db->query("delete from `modx_a_mailer_templates` where template_id = '".$_GET['delete']."'");
					 	$res['alert'] = $_lang["mailer_del_tpl"];
					}
					$templates = $modx->db->query("select * from `modx_a_mailer_templates` order by template_id desc");
					$tpl       = "/tpl/templates.tpl";
				break;
			}
		break;
		case "config":
			if (count($_POST) > 0) {
				foreach ($_POST as $key => $value) 
					$modx->db->query("replace into `modx_system_settings` (setting_value, setting_name) values('$value','$key')");
				// empty cache
				include_once MODX_BASE_PATH . "inspiration/processors/cache_sync.class.processor.php";
				$sync = new synccache();
				$sync->setCachepath(MODX_BASE_PATH . "assets/cache/");
				$sync->setReport(false);
				$sync->emptyCache(); // first empty the cache
				die(header("Location: ".$url."&get=config&i=yes"));
			}
			if (!empty($_GET['i'])) $res['alert'] = $_lang["mailer_update_conf"];
			$tpl          = "/tpl/config.tpl";
		break;
        case "userlist":
            // Импорт из Веб пользователей
            if (isset($_GET['i']) && $_GET['i'] == "import") { ////
                $modx->db->query("
					INSERT INTO `modx_a_mailer_users` (user_email, user_name) SELECT email AS 'user_email', fullname AS 'user_name' 
					FROM `modx_web_user_attributes` 
					ON DUPLICATE KEY 
					UPDATE user_name = user_name
				");
            }
            // Экспорт пользователей в файл
            if (isset($_GET['i']) && $_GET['i'] == "export") {
                $spreadsheet = new Spreadsheet();
                // Установить настройки документа
                $spreadsheet->getProperties()->setCreator('Seiger')
                    ->setLastModifiedBy('Seiger')
                    ->setTitle('Office 2007 XLSX Document')
                    ->setSubject('Office 2007 XLSX Document')
                    ->setDescription('Exporting the list of users for the mailing list')
                    ->setKeywords('office 2007 openxml php')
                    ->setCategory('Export file');
                // Настройки форматирования
                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                // Добавляем заголовки
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Email')
                    ->setCellValue('B1', $_lang["mailer_name"]);
                // Выбрать пользователей из БД
                $emailArr = $modx->db->makeArray($modx->db->query("
					SELECT * FROM `modx_a_mailer_users` ORDER BY user_email
				"));
                // Обработка списка
                if (is_array($emailArr)) {
                    $i = 1;
                    foreach ($emailArr as $user) {
                        ++$i;
                        $spreadsheet->getActiveSheet()->setCellValue('A' . $i, $user['user_email']);
                        $spreadsheet->getActiveSheet()->setCellValue('B' . $i, $user['user_name']);
                    }
                }
                // Имя вкладки
                $spreadsheet->getActiveSheet()->setTitle($_lang["mailer_users_list"]);
                // Установите активный индекс листа на первый лист, так что Excel открывает это как первый лист
                $spreadsheet->setActiveSheetIndex(0);
                // Перенаправить вывод в веб-браузер клиента
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="Mailer user list.xlsx"');
                header('Cache-Control: max-age=0');
                // Для IE 9, может потребоваться следующее
                header('Cache-Control: max-age=1');
                // Для IE через SSL, может понадобиться следующее:
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s').' GMT');
                header('Cache-Control: cache, must-revalidate');
                header('Pragma: public');
                $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
                $writer->save('php://output');
                die;
            }
            // Импорт пользователей из файла
            if (isset($_FILES['file']['tmp_name']) && !empty($_FILES['file']['tmp_name'])) {
                $extension = pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
                if ($extension == "xls" || $extension == "xlsx") {
                    $spreadsheet = IOFactory::load($_FILES['file']['tmp_name']);
                    $sheetData   = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                    if (is_array($sheetData)) {
                        $heads = $sheetData[1];
                        if (is_array($heads)) {
                            foreach ($heads as $k => $v) {
                                if (!empty($v)) {
                                    switch (mb_strtolower(trim($v))) {
                                        case 'mail':
                                        case 'email':
                                        case 'e-mail':
                                        case 'почта':
                                        case 'ящик':
                                            $head['email'] = $k;
                                            break;
                                        case 'name':
                                        case 'имя':
                                            $head['name'] = $k;
                                            break;
                                        default:
                                            $head[$k] = $k;
                                            break;
                                    }
                                }
                            }
                            unset($sheetData[1]);
                            foreach ($sheetData as $value) {
                                $modx->db->query("
									INSERT INTO `modx_a_mailer_users` SET
										user_name  = '".$modx->db->escape($value[$head['name']])."',
										user_email = '".$modx->db->escape($value[$head['email']])."'
									ON DUPLICATE KEY 
									UPDATE
										user_name  = '".$modx->db->escape($value[$head['name']])."'
								");
                            }
                        }
                    }
                }
            }
            if (isset($_GET['d'])) {
                $modx->db->query("delete from `modx_a_mailer_users` where user_id =".$_GET['d']);
                $res['alert'] = $_lang["mailer_del_user_i"];
            }
            $users = $modx->db->query("select * from `modx_a_mailer_users` order by user_id desc limit 0, 100");
            $tpl   = "/tpl/userlist.tpl";
            break;
        default:
			$stats = $modx->db->query("
						SELECT 
							t.*,
							(select count(*) FROM `modx_a_mailer` WHERE tpl_id = t.template_id) AS 'all',
							(select count(*) FROM `modx_a_mailer` WHERE tpl_id = t.template_id AND mailed = 1) AS 'mailed'
						FROM `modx_a_mailer_templates` t
						ORDER BY t.template_date DESC
						");
			$tpl   = "/tpl/stat.tpl";
		break;
	}
	if (isset($tpl)) {
		ob_start();
		include ROOT . $tpl;
		$res['content'] = ob_get_contents();
		ob_end_clean();
	}
	include ROOT . "/tpl/index.tpl";
	die;

