// <?php 
/**
 * Discounts
 * 
 * The module allows you to management discounts articles
 * 
 * @category	module
 * @version 	1.0
 * @internal	@properties
 * @internal	@guid	
 * @internal	@shareparams 1
 * @internal	@moduleshow 1
 * @internal	@dependencies requires files located at /assets/modules/discounts/
 * @internal	@modx_category Manager and Admin
 * @internal	@installset base, sample
 * @author	Seiger
 * @lastupdate	11/06/2018
 */

require MODX_BASE_PATH . "assets/modules/discounts/index.php";