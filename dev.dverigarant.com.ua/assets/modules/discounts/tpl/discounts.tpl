<h2 class="muted"><?=$_lang['discounts_list'];?></h2>
<form action="index.php" method="GET" role="form">
	<input type="hidden" name="a" value="112">
	<input type="hidden" name="id" value="<?=$_REQUEST['id']?>">
	<input type="hidden" name="get" value="discounts">
	<input type="text" style="width:20%; float:left; margin:0 5px 0 0;" value="<?=$_GET['srch'];?>" name="srch" placeholder="<?=$_lang['discounts_id_or_title'];?>" class="form-control">
	<select name="filter_publish" class="select2">
		<option value="" selected><?=$_lang['discounts_visible'];?></option>
		<option value="1"<?php if('1' == $_GET['filter_publish']) echo ' selected';?>><?=$_lang['discounts_published'];?></option>
		<option value="0"<?php if('0' == $_GET['filter_publish']) echo ' selected';?>><?=$_lang['discounts_no_published'];?></option>
	</select>
	<input type="submit" style="margin-right:20px;padding-top:5px;min-height:34px;" class="actionButtons" value="<?=$_lang['discounts_search'];?>">
	<a href="<?=$url?>discounts" style="float: right;margin: 7px 0 0 0;display: inline-block;text-decoration: underline;"><b><?=$_lang['discounts_reset_filters'];?></b></a>
</form>
<br />
<?php if (!is_array($contents) || count($contents) == 0):?>
<div class="row"><h1 class="screen"><strong><?=$_lang['discounts_not_found'];?></strong></h1></div>
<?php else: ?>
<table class="table table-condensed table-striped table-bordered table-hover">
	<thead>
	<tr>
		<th width="70px" style="text-align:center;"><?=$_lang['discounts_id'];?></th>
		<th style="text-align:center;"><?=$_lang['discounts_title'];?></th>
		<th width="110px" style="text-align:center;"><?=$_lang['discounts_publish_date'];?></th>
		<th width="110px" style="text-align:center;"><?=$_lang['discounts_visible'];?></th>
		<th width="110px" style="text-align:center;"><?=$_lang['discounts_success_at'];?></th>
		<th width="250px" style="text-align:center;"><?=$_lang['discounts_action'];?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($contents as &$content): ?>
	<tr id="content-<?=$content['id']?>">
		<td class="vertical-align"><?=$content['id']?></td>
		<td>&nbsp;
			<img src="<?=$modx->runSnippet('R', ['img' => '/assets/images/discounts/'.$content['id'].'/'.$content['image'], "folder" => "backendDiscounts/".$content['id'], 'opt' => 'w=50&h=30&far=1'])?>" alt="<?php if ($content['image']) echo '/assets/images/discounts/'.$content['id'].'/'.$content['image'];?>" class="img-thumbnail"/>&emsp;
			<a href="<?=$modx->makeUrl($modx->config['site_start'], '', '', 'full')?><?=$content['alias']?>/" target="_blank"><b><?=$content['title']?></b></a>
		</td>
		<td class="vertical-align" style="text-align:center;"><b><?=date("d.m.Y", $content['published_at']);?></b></td>
		<td class="vertical-align" style="text-align:center;">
			<b>
				<?php if($content['published'] == 0):?><span class="label label-danger"><?=$_lang['discounts_no_published'];?></span><?php endif;?>
				<?php if($content['published'] == 1):?><span class="label label-success"><?=$_lang['discounts_published'];?></span><?php endif;?>
			</b>
		</td>
		<td class="vertical-align" style="text-align:center;">
			<b>
				<?php if($content['success_at'] == 0):?><span class="label label-danger"><?=$_lang['discounts_no_published'];?></span><?php endif;?>
				<?php if($content['success_at'] == 1):?><span class="label label-success"><?=$_lang['discounts_published'];?></span><?php endif;?>
			</b>
		</td>
		<td class="vertical-align" style="text-align:center;">
			<a href="<?=$url?>discounts&c=edit&i=<?=$content['id']?>" class="btn btn-success"><i class="fa fa-pencil"></i>&emsp;<?=$_lang['discounts_edit'];?></a>
			<a href="#" data-href="<?=$url?>discounts&c=delete&i=<?=$content['id']?>" data-toggle="modal" data-target="#confirm-delete" data-id="<?=$content['id']?>" data-name="<?=$content['title']?>" class="btn btn-danger"><i class="fa fa-trash"></i>&emsp;<?=$_lang['discounts_delete'];?></a>
		</td>
	</tr>
	<?php endforeach;?>
	</tbody>
</table>
<?php endif; ?>
<ul class="pagination"><?=$pagin;?></ul>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><?=$_lang['discounts_confirm_delete'];?></div>
			<div class="modal-body">
				<?=$_lang['discounts_you_sure'];?> <b id="confirm-name"></b> <?=$_lang['discounts_with_id'];?> <b id="confirm-id"></b>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?=$_lang['discounts_cancel'];?></button>
				<a class="btn btn-danger btn-ok"><?=$_lang['discounts_delete'];?></a>
			</div>
		</div>
	</div>
</div>
<div id="actions">
	<ul class="actionButtons">
		<li id="Button2" class="transition">
			<a href="<?=$url?>discounts&c=add"><i class="fa fa-plus-square"></i>&emsp;<?=$_lang['discounts_create'];?></a>
		</li>
	</ul>
</div>
<img src="/inspiration/media/style/MODxRE2/images/misc/ajax-loader.gif" id="img-preview" style="display: none;" class="img-thumbnail">
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("table img").on("mouseenter", function() {
            var alt = jQuery(this).attr("alt");
            if (alt.length > 0) {
                jQuery("#img-preview").attr("src", alt).show();
            }
        });
        jQuery("table img").on("mouseleave", function() {
            jQuery("#img-preview").hide();
        });
        jQuery('#confirm-delete').on('show.bs.modal', function(e) {
            jQuery(this).find('#confirm-id').text(jQuery(e.relatedTarget).data('id'));
            jQuery(this).find('#confirm-name').text(jQuery(e.relatedTarget).data('name'));
            jQuery(this).find('.btn-ok').attr('href', jQuery(e.relatedTarget).data('href'));
        });
    });
</script>
