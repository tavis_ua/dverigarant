<?php
/**
 *	Модуль управления новостями
 */
if(!defined('IN_MANAGER_MODE') || IN_MANAGER_MODE != 'true') die("No access");
global $_lang;
global $modx_lang_attribute;
define("ROOT", dirname(__FILE__));
$get    = $_REQUEST['get'] ?: "discounts";
$res    = [];
$m_lang = is_file(ROOT.'/langs.php') ? require_once ROOT.'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));
$res['url']     = $url = "/".MGR_DIR."/index.php?a=".$_REQUEST['a']."&id=".$_REQUEST['id']."&get=";
$res['title']   = "Управление новостями";
$res['version'] = "v 1.0";
$modx->config['lang_list']    = $modx->config['lang_list'] ?: $modx_lang_attribute;
$modx->config['lang_default'] = $modx->config['lang_default'] ?: $modx_lang_attribute;
$modx->config['lang']         = $modx->config['lang_default'];
$languages                    = explode(",", $modx->config['lang_list']);
$table_name                   = $modx->getFullTableName('a_discounts');
$table_name_strings           = $modx->getFullTableName('a_discounts_strings');
require_once ROOT . "/class.discounts.php";
$discounts = new Discounts($modx, $table_name, $table_name_strings);
$cat_templ_id = $modx->config['shop_tpl_category']; // id шаблона категорий
switch ($get) {
	case 'delete':
		$crud->delete($id);
		$modx->clearCache('full');
		die(header("Location: ".$url."b=".$page."&w=deleted"));
		break;

    case "images":
        if (!file_exists(MODX_BASE_PATH."assets/images/discounts")) {
            mkdir(MODX_BASE_PATH."assets/images/discounts");
            chmod(MODX_BASE_PATH."assets/images/discounts", 0777);
        }
        $imagePath = "assets/images/discounts/".$_REQUEST['folder'];
        $folder = MODX_BASE_PATH.$imagePath;
        if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0777);
        }
        switch ($_GET['c']) {
            case "set_cover":
                foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
                    $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
                    $filename = $folder."/". $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                    $modx->db->query("UPDATE {$table_name} SET `image` = '".$name."' WHERE `id` = ".(int)$_REQUEST['folder']);
                }
                echo $name;
                die;
            case "get_cover" :
                $image = $modx->db->getValue($modx->db->query("SELECT `image` FROM {$table_name} WHERE `id` = ".(int)$_REQUEST['folder']));
                if ($image) {
                    $img['image_file'] = $image;
                    $rimg = $modx->runSnippet('R', ['img' => "/".$imagePath."/".$image, "folder" => "backendDiscounts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100']);
                    require ROOT.'/tpl/image_cover.tpl';
                }
                die;
            case "set_slider":
                $names = [];
                foreach ($_FILES['uploader']['tmp_name'] as $key => $value){
                    $name = preg_replace('/\D+/', '', microtime()).".".end(explode(".", $_FILES['uploader']['name'][$key]));
                    $filename = $folder."/". $name;
                    move_uploaded_file($value, $filename);
                    chmod($filename, 0644);
                    $names[] = $name;
                }
                if (count($names)) {
                    $galleries = $modx->db->getValue($modx->db->query("SELECT `gallery` FROM {$table_name} WHERE `id` = ".(int)$_REQUEST['folder']));
                    $galleries = explode(',', $galleries);
                    $names = array_merge($names, $galleries);
                    $modx->db->query("UPDATE {$table_name} SET `gallery` = '".implode(',', $names)."' WHERE `id` = ".(int)$_REQUEST['folder']);
                }
                echo $name;
                die;
            case "get_slider" :
                $galleries = $modx->db->getValue($modx->db->query("SELECT `gallery` FROM {$table_name} WHERE id = ".(int)$_REQUEST['folder']));
                $images = explode(',', $galleries);
                if (is_array($images)) {
                    foreach ($images as &$image) {
                        if ($image) {
                            $img['image_file'] = $image;
                            $rimg = $modx->runSnippet('R', ['img' => "/".$imagePath."/".$image, "folder" => "backendDiscounts/".$_REQUEST['folder'], 'opt' => 'w=200&h=100&far=1&q=100']);
                            require ROOT.'/tpl/imagesC.tpl';
                        }
                    }
                }
                die;
            case "delete_cover" :
                $folder = (int)$_REQUEST['folder'];
                unlink($folder."/".$_REQUEST['delete']);
                $modx->db->query("UPDATE {$table_name} SET `image` = NULL WHERE `id` = ".(int)$folder);
                die;
            case "delete_image" :
                $folder = (int)$_REQUEST['folder'];
                unlink($folder."/".$_REQUEST['delete']);
                $galleries = $modx->db->getValue($modx->db->query("SELECT `gallery` FROM {$table_name} WHERE `id` = ".(int)$_REQUEST['folder']));
                $galleries = explode(',', $galleries);
                foreach ($galleries as $key => $gallery) {
                    if ($_REQUEST['delete'] == $gallery || '' == $gallery) {
                        unset($galleries[$key]);
                    }
                }
                $modx->db->query("UPDATE {$table_name} SET `gallery` = '".implode(',', $galleries)."' WHERE `id` = ".(int)$_REQUEST['folder']);
                die;
            case "set_sort" :
                if (count($_POST) > 0) {
                    $galleries = array_keys($_POST['image']);
                    $modx->db->query("UPDATE {$table_name} SET `gallery` = '".implode(',', $galleries)."' WHERE `id` = ".(int)$_REQUEST['folder']);
                }
                die;
        }
        break;
	default:
	    switch ($_GET['c']) {
            case "add":
                $tiny_mce = $discounts->tinyMCE("description, content", "300px");
                $discounts_products = $discounts->getDiscountsProducts();
                $discounts_categories = $discounts->getDiscountsCategory($cat_templ_id, $modx->config['lang']);
                $tpl = "discounts_one.tpl";
                break;
            case "save":
                $_POST['content']['id'] = $discounts->saveDiscounts($_POST['content']);
                foreach ($languages as $value) {
                    $discounts->saveDiscountsString($_POST['content'], $value);
                }
                $modx->clearCache();
                die(header("Location: ".$url."discounts&c=edit&i=".$_POST['content']['id']));
            case 'edit':
                $tiny_mce = $discounts->tinyMCE("description, content", "300px");
                $content  = $discounts->getOne((int)$_GET['i']);
                $content['discounts_products'] = explode(',', $content['discounts_products']);
                $content['discounts_category'] = explode(',', $content['discounts_category']);
                $discounts_categories = $discounts->getDiscountsCategory($cat_templ_id, $modx->config['lang']);
                $discounts_products = $discounts->getDiscountsProducts();
                foreach ($languages as $value) {
                    $content[$value] = $discounts->getLangStrings((int)$_GET['i'], $value);
                }

                $tpl = "discounts_one.tpl";
                break;
            case "update":
                $discounts->updateDiscounts($_POST['content']);
                foreach ($languages as $value) {
                    $discounts->saveDiscountsString($_POST['content'], $value);
                }
                $modx->clearCache();
                die(header("Location: ".$url."Discounts"));
            case "delete" :
                $discounts->deleteDiscounts($_GET['i']);
                $modx->clearCache();
                die(header("Location: ".$url."discounts"));
            default:
                $search = [];
                if (isset($_GET['srch']) && trim($_GET['srch']) != '') {
                    $search['srch'] = trim($_GET['srch']);
                    $url .= "&srch={$search['srch']}";
                }
                if (isset($_GET['filter_publish']) && trim($_GET['filter_publish']) != '') {
                    $search['filter_publish'] = (int)$_GET['filter_publish'];
                    $url .= "&filter_publish={$search['filter_publish']}";
                }
                if (isset($_GET['filter_main']) && trim($_GET['filter_main']) != '') {
                    $search['filter_main'] = (int)$_GET['filter_main'];
                    $url .= "&filter_main={$search['filter_main']}";
                }

                $contents  = $discounts->getAll($search);
                $modx->setPlaceholder('url', $url."&p=");
                $pagin     = $discounts->getPaginate();
                $tpl       = "discounts.tpl";
                break;
        }
		break;
}
if (isset($tpl)) {
    ob_start();
    include ROOT.'/tpl/'.$tpl;
    $res['content'] = ob_get_contents();
    ob_end_clean();
}
include_once(includeFileProcessor("includes/header.inc.php",$manager_theme));
include ROOT.'/tpl/index.tpl';
die;