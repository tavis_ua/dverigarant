// <?php 
/**
 * Language
 * 
 * The module allows you to configure transfer elements templates
 * 
 * @category	module
 * @version 	2.0
 * @internal	@properties
 * @internal	@guid	
 * @internal	@shareparams 1
 * @internal	@moduleshow 1
 * @internal	@dependencies requires files located at /assets/modules/language/
 * @internal	@modx_category Manager and Admin
 * @internal	@installset base, sample
 * @author	Seiger
 * @lastupdate	08/10/2018
 */

require MODX_BASE_PATH . "assets/modules/language/index.php";