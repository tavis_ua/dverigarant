<?php
/**
 * Модуль "Коллекции"
 */
return [
    "ru" => [
        'image_title'          => "Управление изображениями",
        'save_first'           => "Прежде чем добавлять изображения сохраните ресурс.",
        'image_upload'         => "Загрузить ",
        'add_youtube'          => "Youtube",
        'youtube_link'         => "Ссылка на видео",
        'image_image'          => "Изображение",
        'image_name'           => "Название",
        'image_alt'            => "Атрибут ALT",
        'image_description'    => "Описание",
        'image_update'         => "Сохранить порядок",
        'image_fields_edit'    => "Редактирование полей изображения",
        'image_delete'         => "Удалить изображение",
        'image_confirm_delete' => "Удалить изображение с сервера?",
        'image_error_format'   => "Неверный формат файла! Поддерживаются: JPG, PNG, WEBP, GIF, FLV, MP4, WEBM",
        'image_error_extension'=> "Ошибка. Неподдерживаемое расширение: ",
        'image_error_filetype' => "Ошибка. Неподдерживаемый тип файла: ",
        'image_error_height'   => "Высота изображения не должна превышать ",
        'image_error_width'    => "Ширина изображения не должна превышать ",
        'image_saved'          => "Изменения сохранены",
        'lang_btn_add'         => "Добавить",
        'lang_btn_save'        => "Сохранить",
        'lang_btn_cancel'      => "Отменить",
        'lang_btn_update'      => "Обновить",
        'edit_text'            => "Редактировать текст"
    ],

    "uk" => [
        'region_title'                  => 'Настройка регионов',
    ],

    "en" => [
        'region_title'                  => 'Настройка регионов',
    ],
];