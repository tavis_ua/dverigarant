<?php
global $_lang;
define("ROOT", dirname(__FILE__));
define("UPLOAD", MODX_BASE_PATH . "assets/images/pictures/");
$get        = isset($_GET['get']) ? $_GET['get'] : "";
$langs      = explode(',', $modx->config['lang_list']);
$res        = Array();
$res['url'] = $table['url'] = $url = "index.php?a=112&id=".$_GET['id']."&category=".$_GET['category'];
$res['get'] = $_GET['get'];

$path = str_replace("\\", "/", dirname(__FILE__));
$path = str_replace(MODX_BASE_PATH, '', $path);

$m_lang = is_file(dirname(__FILE__).'/langs.php') ? require_once dirname(__FILE__).'/langs.php' : [$modx_lang_attribute => []];
$_lang  = is_array($m_lang[$modx_lang_attribute]) ? array_merge($_lang, $m_lang[$modx_lang_attribute]) : array_merge($_lang, reset($m_lang));

error_reporting(0);
switch ($get) {
    case "upload":
        $error = [];

        if (!file_exists(UPLOAD)) {
            mkdir(UPLOAD);
            chmod(UPLOAD, 0777);
        }

        if (!file_exists(UPLOAD . $_GET['category'])) {
            mkdir(UPLOAD . $_GET['category']);
            chmod(UPLOAD . $_GET['category'], 0777);
        }
        if (count($_FILES['upload']['tmp_name']) > 0) {
            foreach ($_FILES['upload']['tmp_name'] as $key => $file) {
                list($type, $extension) = explode("/", $_FILES['upload']['type'][$key]);
                switch ($type) {
                    case 'image':
                        if ($extension == 'svg+xml') {
                            $extension = 'svg';
                        }

                        if (!in_array($extension, ['jpg', 'png', 'jpeg', 'webp', 'svg', 'gif'])) {
                            $error[$_FILES['upload']['name'][$key]] = $_lang['image_error_extension'].$extension;
                        }

                        list($width, $height) = getimagesize($file);
                        if ($width > $modx->config['maxImageWidth']) {
                            $error[$_FILES['upload']['name'][$key]] = $_lang['image_error_width'] . $modx->config['maxImageWidth'] . "px.";
                        }
                        if ($height > $modx->config['maxImageHeight']) {
                            $error[$_FILES['upload']['name'][$key]] = $_lang['image_error_height'] . $modx->config['maxImageHeight'] . "px.";
                        }
                        break;

                    case 'video':
                        if (!in_array($extension, ['mp4', 'flv', 'webm'])) {
                            $error[$_FILES['upload']['name'][$key]] = $_lang['image_error_extension'].$extension;
                        }
                        break;

                    default:
                        $error[$_FILES['upload']['name'][$key]] = $_lang['image_error_filetype'].$type;
                }
                if (count($error) == 0) {
                    move_uploaded_file($file, UPLOAD . $_GET['category'] . "/" . md5($file) . "." . $extension);
                    chmod(UPLOAD . $_GET['category'] . "/" . md5($file) . "." . $extension, 0777);
                }
            }
        }
        die(json_encode($error));
    case "add_youtube":
        if(preg_match('/[A-z0-9_-]{11}/', $_POST['youtube_link'], $video)) {
            $modx->db->query("
				INSERT INTO `modx_a_images` SET 
					`parent` = '".(int)$_GET['category']."',
					`file` = '".$modx->db->escape(reset($video))."',
					`type` = 'youtube',
					`position` = '999'
			");
        }
        die('Ok');
        break;
    case "delete":
        unlink(UPLOAD.$_GET['category'].'/'.$_GET['file']);
        $modx->db->query("
			DELETE FROM `modx_a_images_fields` 
			WHERE `rid` = (SELECT `id` FROM `modx_a_images` WHERE `file` = '".$modx->db->escape($_GET['file'])."')");
        $modx->db->query("DELETE FROM `modx_a_images` WHERE `file` = '".$modx->db->escape($_GET['file'])."'");
        die;
    case "edit":
        $img_id = $modx->db->makeArray($modx->db->query("
			SELECT `id` 
			FROM `modx_a_images` 
			WHERE `parent` = ".(int)$_GET['category']." AND `file` = '".$modx->db->escape($_GET['file'])."' 
			LIMIT 1
		"));
        if (0 < count($img_id)) {
            $id = $img_id[0]['id'];
        } else {
            $modx->db->query("
				INSERT INTO `modx_a_images` SET 
					`parent` = '".(int)$_GET['category']."',
					`file` = '".$modx->db->escape($_GET['file'])."',
					`position` = '".(int)$_GET['position']."'
			");
            $result   = $modx->db->getRow($modx->db->query("SELECT LAST_INSERT_ID() AS last_id"));
            $id = $result['last_id'];
        }
        $fields = $modx->db->query("
			SELECT * 
			FROM  `modx_a_images_fields` 
			WHERE `rid` = $id
		");

        while ($field = $modx->db->getRow($fields)) {
            $image[$field['lang']] = $field;
        }

        include ROOT . "/tpl/tabs.tpl";
        die;
        break;
    case "save":
        if (is_array($_POST['image'])) {
            foreach ($_POST['image'] as $id => $lagArray) {
                if (0 < (int)$id) {
                    foreach ($lagArray as $langId => $value) {
                        $modx->db->query("
							INSERT INTO `modx_a_images_fields` SET 
	       						`rid` = '".(int)$id."',
	       						`lang` = '".$modx->db->escape($langId)."',
	       						`alt` = '".$modx->db->escape($value['alt'])."',
	       						`title` = '".$modx->db->escape($value['title'])."',
	       						`description` = '".$modx->db->escape($value['description'])."',
	       						`link` = '".$modx->db->escape($value['link'])."'
	       					ON DUPLICATE KEY UPDATE 
	       						`alt` = '".$modx->db->escape($value['alt'])."',
	       						`title` = '".$modx->db->escape($value['title'])."',
	       						`description` = '".$modx->db->escape($value['description'])."',
	       						`link` = '".$modx->db->escape($value['link'])."'
	       				");
                    }
                }
            }
        }
        die('Ok');
        break;
    case "imageslist":
        //Получаем массив картинок в папке без сортировки
        $images = glob(UPLOAD.$_GET['category']."/*",GLOB_NOSORT);
        //Получаем путь к папке
        if(isset($images[0])) {
            $url =  dirname($images[0]).'/';
        }
        $images_db = array();
        //Получаем массив картинок в базе
        $images1 = $modx->db->query("SELECT `file` FROM `modx_a_images` WHERE `parent` = ".(int)$_GET['category']." ORDER BY `position` ASC");
        while ($img = $modx->db->getRow($images1)) {
            $images_db[] = $url.$img['file'];
        }
        //Проверяем массивы ли два эти элемента для того что бы убрать из массива картинок папки лишние. Те, которые есть в базе
        if (is_array($images) and count($images_db) > 0) {
            $images = array_diff($images, $images_db);
        }
        // Проверяем осталось хоть что то в массиве картинок папки. Если не проверять - ошибка обьединения
        if(is_array($images)){
            //Если что то осталось - сортируем по дате добавление
            usort($images, function($a,$b) {return filemtime($a) - filemtime($b);});
            //Объeденяем два массива в один для дальнейшего вывода. Первыми идет массив с базы
            $images = array_merge ($images_db, $images );
        }
        else {
            $images = $images_db;
        }
        if (is_array($images)) {
            foreach ($images as $pos => $image) {
                $extension = strtolower(end(explode(".", $image)));
                switch ($extension) {
                    case 'jpg':
                    case 'png':
                    case 'jpeg':
                    case 'webp':
                    case 'svg':
                    case 'gif':
                        $image = str_replace(MODX_BASE_PATH, '', $image);
                        include ROOT."/tpl/image.tpl";
                        break;

                    case 'mp4':
                    case 'flv':
                    case 'webm':
                        $image = str_replace(MODX_BASE_PATH, '', $image);
                        include ROOT."/tpl/video.tpl";
                        break;

                    default:
                        include ROOT."/tpl/youtube.tpl";
                        break;
                }
            }
        }
        die;
        break;
    default:
        // Если отправили форму, то сохраняем всё
        if (count($_POST) > 0) {
            $sort = 0;
            foreach ($_POST['image'] as $key => $value) {
                $modx->db->query("
                    INSERT INTO `modx_a_images` SET 
						`parent` = '".(int)$_GET['category']."',
						`file` = '".$modx->db->escape($key)."',
						`type` = '".$modx->db->escape($value)."',
						`position` = '".$sort."'
					ON DUPLICATE KEY UPDATE 
						`position` = '".$sort."',
						`type` = '".$modx->db->escape($value)."'
					");
                $sort++;
            }
        }

        //Получаем массив картинок в папке без сортировки
        $images = glob(UPLOAD.$_GET['category']."/*",GLOB_NOSORT);
        //Получаем путь к папке
        if(isset($images[0])) {
            $dir =  dirname($images[0]).'/';
        }
        $images_db = array();
        //Получаем массив картинок в базе
        $images1 = $modx->db->query("SELECT `file` FROM `modx_a_images` WHERE `parent` = ".(int)$_GET['category']." ORDER BY `position` ASC");
        while ($img = $modx->db->getRow($images1)) {
            $images_db[] = $dir.$img['file'];
        }
        //Проверяем массивы ли два эти элемента для того что бы убрать из массива картинок папки лишние. Те, которые есть в базе
        if (is_array($images) and count($images_db) > 0) {
            $images = array_diff($images, $images_db);
        }
        // Проверяем осталось хоть что то в массиве картинок папки. Если не проверять - ошибка обьединения
        if (is_array($images)) {
            //Если что то осталось - сортируем по дате добавление
            usort($images, function($a,$b) {return filemtime($a) - filemtime($b);});
            //Обеденяем два массива в один для дальнейшего вывода. Первыми идет массив с базы
            $images = array_merge ($images_db, $images);
        } else {
            $images = $images_db;
        }
        if (is_array($images)) {
            ob_start();
            foreach ($images as $pos => $image) {
                $extension = strtolower(end(explode(".", $image)));
                switch ($extension) {
                    case 'jpg':
                    case 'png':
                    case 'jpeg':
                    case 'webp':
                    case 'svg':
                    case 'gif':
                        $image = str_replace(MODX_BASE_PATH, '', $image);
                        include ROOT."/tpl/image.tpl";
                        break;

                    case 'mp4':
                    case 'flv':
                    case 'webm':
                        $image = str_replace(MODX_BASE_PATH, '', $image);
                        include ROOT."/tpl/video.tpl";
                        break;

                    default:
                        include ROOT."/tpl/youtube.tpl";
                        break;
                }
            }
            $res['images'] = ob_get_contents();
            ob_end_clean();
            $doc = $modx->getDocument($_GET['category']);
        }
        break;
}
if (isset($tpl)) {
    ob_start();
    include ROOT . $tpl;
    $res['content'] = ob_get_contents();
    ob_end_clean();
}
if ((int)$_GET['category'] > 0) {
    include ROOT . "/tpl/index.tpl";
} else {
    echo '<p class="warning">' . $_lang['save_first'] . '</p>';
}

die;