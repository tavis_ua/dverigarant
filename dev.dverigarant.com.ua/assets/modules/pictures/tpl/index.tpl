<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <title><?=$_lang['image_title']?></title>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    <link href="/assets/site/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/site/bootstrap3/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="media/style/common/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <script src="/assets/site/jquery-2.1.4.min.js"></script>
    <script src="/assets/site/bootstrap3/js/bootstrap.min.js"></script>
    <script src="/assets/site/ajaxupload.js"></script>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("[data-active]").each(function() {
                _option = $(this).find("option[value='"+$(this).attr("data-active")+"']");
                _option.prop("selected", true);
            });
        });
    </script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header pull-right">
        <a class="navbar-brand" href="<?=$url?>"><?=$doc['pagetitle_'.$modx->config['lang_default']]?></a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <?php if ($_GET['category'] != '') :?>
                <li><a href="<?=$url?>&get=uploader" id="uploader" title=""><span class="glyphicon glyphicon-upload"></span>&nbsp;&nbsp;<?=$_lang['image_upload']?></a></li>
                <li><a id="add_youtube"><span class="fa fa-youtube-square"></span>&nbsp;&nbsp;<?=$_lang['add_youtube']?></a></li>
            <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<br /><br /><br />
<div class="container">
    <div class="row">
        <form id="img_form" action="<?=$url?>" enctype="multipart/form-data" method="post">
            <ul id="upload_base">
                <?=$res['images']?>
            </ul>
        </form>
        <div class="modal fade" id="translate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form id="translates" action="<?=$url?>&get=save" method="post">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel"><?=$_lang['image_fields_edit']?></h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> <?=$_lang['lang_btn_cancel']?></button>
                            <button type="submit" class="btn btn-primary pull-right"><?=$_lang['lang_btn_save']?> <span class="glyphicon glyphicon-ok-sign"></span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script src="/assets/site/jquery-ui.min.js"></script>
        <script>
            function updateImages() {
                $.ajax({
                    url:'<?=$url?>&get=imageslist',
                    cache:false,
                    success:function(ajax) {
                        $("#upload_base").html(ajax);
                        $("#upload_base").sortable();
                        savePosition();
                    }
                });
            }

            function savePosition() {
                $.ajax({
                    url:'<?=$url?>',
                    cache:false,
                    type: "POST",
                    data: $("#img_form").serialize(),
                    success:function(ajax) {
                        console.log('ok!');
                    }
                });
            }

            $(document).ready(function() {
                $("#upload_base").sortable({
                    cursor: "move",
                    update: function () {
                        savePosition();
                    },
                    stop: function (e,i) {
                        var item = i.item.find('.youtube_button')
                        $(item).removeClass('fa fa-pause-circle-o fa-5x');
                        $(item).addClass('play');
                    }
                });

                new AjaxUpload($("#uploader"), {
                    action:"<?=$url?>&get=upload",
                    multiple:true,
                    name:"upload[]",
                    onSubmit:function(file, ext) {
                        if (!(ext && /^(jpg|png|jpeg|webp|svg|gif|JPG|PNG|JPEG|WEBP|SVG|GIF|mp4|flv|webm|MP4|FLV|WEBM)$/.test(ext))) {
                            alert("<?=$_lang['image_error_format']?>");
                            return false;
                        }
                    },
                    onComplete:function(file, response) {
                        if (response.length > 3) {
                            JSON.parse(response, function(k, v) {
                                if (k != "") {
                                    alert("<?=$_lang['image_image']?> "+k+" "+v);
                                }
                            });
                        }
                        updateImages();
                    }
                });

                $(document).on("click", "[data-image-remove]", function() {
                    if (confirm("<?=$_lang['image_confirm_delete']?>")) {
                        var _this = $(this);
                        $.ajax({
                            url:'<?=$url?>&get=delete&file='+_this.attr("data-image-remove"),
                            cache:false,
                            success:function(ajax) {
                                _this.parents(".image").fadeOut(1000, function() { $(this).remove() });
                            }
                        });
                    }
                    return false;
                });

                $(document).on("click", "[data-image-edit]", function() {
                    var _this = $(this);
                    $.ajax({
                        url:'<?=$url?>&get=edit&file='+_this.attr("data-image-edit")+'&position='+_this.attr("data-image-position"),
                        cache:false,
                        success:function(ajax) {
                            $("#translate .modal-body").html(ajax);
                            $('#translate').modal('show');
                        }
                    });
                    return false;
                });

                $(document).on("submit", "#translates", function() {
                    $.ajax({
                        url:'<?=$url?>&get=save',
                        type:"POST",
                        data:$('#translates').serialize(),
                        cache:false,
                        success:function(ajax) {
                            if (ajax == 'Ok') {
                                $('#translate').modal('hide');
                                alert('Изменения успешно сохранены');
                            }
                        }
                    });
                    return false;
                });

                $(document).on("click", "#add_youtube", function() {
                    var result = prompt("<?php echo $_lang['youtube_link']; ?>");
                    $.ajax({
                        url:'<?=$url?>&get=add_youtube',
                        type:"POST",
                        data:'youtube_link='+result,
                        cache:false,
                        success:function(ajax) {
                            if (ajax == 'Ok') {
                                updateImages();
                            }
                        }
                    });
                    return false;
                });

                $(document).on("click", "i.play_button", function() {
                    var video = $(this).parent().find('video').get(0);

                    if (video.paused) {
                        $(this).removeClass('fa-play-circle-o');
                        $(this).addClass('fa-pause-circle-o');
                        video.play();
                    } else {
                        $(this).removeClass('fa-pause-circle-o');
                        $(this).addClass('fa-play-circle-o');
                        video.pause();
                    }
                    return false;
                });
            });

            var player = [];

            $(document).on("click", "i.youtube_button", function() {
                var video = $(this).parent().find('iframe').get(0).id;

                if (player[video].getPlayerState() !== 1 ) {
                    $(this).removeClass('play');
                    $(this).addClass('fa fa-pause-circle-o fa-5x');
                    player[video].playVideo();
                } else {
                    $(this).removeClass('fa fa-pause-circle-o fa-5x');
                    $(this).addClass('play');
                    player[video].pauseVideo();
                }
                return false;
            });

            window.onYouTubeIframeAPIReady = function() {
                $.each($('iframe.thumbnail'), function(index, element) {
                    player[element.id] = new YT.Player(element.id);
                });
            }
        </script>
        <style>
            #uploader,
            #add_youtube {
                cursor: pointer;
            }
            #uploader.hover {
                color: #fff;
                background-color: transparent;
            }
            .image {
                position: relative;
                width: 275px;
                height: 180px;
                margin: 0 5px 15px 0;
                list-style: none;
                float: left;
            }
            .image > .thumbnail {
                margin-bottom: 5px;
            }
            .image > span {
                position: absolute;
                top: 5px;
                right: 5px;
                display: none;
            }
            .image:hover > span {
                display: inline;
                z-index: 100;
            }
            .image > .btn-primary {
                position: absolute;
                top: 5px;
                left: 5px;
                display: none;
            }
            .image:hover > .btn-primary {
                display: inline;
                z-index: 100;
            }
            .image > .form-control,
            .image > div > .form-control {
                margin: 0 0px -17px 0;
            }
            .image > i.type {
                position: absolute;
                top: auto;
                bottom: 5px;
                right: 15px;
                display: block;
                margin: 0;
                color: #ffffff;
                text-shadow: 0 0 3px rgba(0,0,0,1);
            }
            .image > i.play_button {
                position: absolute;
                top: calc(50% - 35px);
                right: calc(50% - 35px);
                display: block;
                margin: 0;
                color: #ffffff;
                opacity: 0.5;
                text-shadow: 0 0 5px rgba(0,0,0,1);
                cursor: pointer;
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
                -webkit-transform-origin: center;
                -ms-transform-origin: center;
                transform-origin: center;
                -webkit-transition: all 0.5s;
                -o-transition: all 0.5s;
                transition: all 0.5s;
            }
            .image > i.play_button.fa-pause-circle-o {
                opacity: 0.1;
            }
            .image:hover > i.play_button.fa-play-circle-o {
                -webkit-transform: rotate(120deg);
                -ms-transform: rotate(120deg);
                transform: rotate(120deg);
                opacity: 1;
            }
            .image:hover > i.play_button.fa-pause-circle-o {
                opacity: 1;
            }
            .image > i.youtube_button.play {
                background: url('/<?php echo $path; ?>/images/youtube-logo.png') no-repeat;
                background-size: contain;
                position: absolute;
                top: calc(50% - 30px);
                right: calc(50% - 35px);
                width: 70px;
                height: 70px;
                cursor: pointer;
                transition: scale 0.5s;
            }
            .image:hover > i.youtube_button.play {
                transform: scale(1.1);
            }
            .image > i.youtube_button.fa-pause-circle-o {
                position: absolute;
                top: calc(50% - 30px);
                right: calc(50% - 30px);
                transition: opacity 0.5s;
                opacity: 0.2;
            }
            .image:hover > i.youtube_button.fa-pause-circle-o {
                opacity: 1;
                color: white;
                cursor: pointer;
            }
            iframe.thumbnail {
                pointer-events: none;
            }
        </style>
    </div>
</div>
</body>
</html>