<ul class="nav nav-tabs">
	<?php foreach ($langs as $k => $lang): ?>
		<li <?php if($k == 0) {echo 'class="active"';} ?>><a href="#<?=$lang?>" data-toggle="tab" class="text-uppercase"><b><?=$lang?></b></a></li>
	<?php endforeach; ?>
</ul>
<div class="tab-content">
	<?php foreach ($langs as $k => $lang): ?>
		<div class="tab-pane <?php if($k == 0) {echo 'fade in active';} ?>" id="<?=$lang?>">
			<table class="table table-condensed table-bordered table-striped">
				<tr>
					<td class="text-center">
						<div class="has-success">
							<input type="text" name="image[<?=$id?>][<?=$lang?>][alt]" value="<?=$image[$lang]['alt']?>" class="form-control" placeholder="ALT" />
						</div>
					</td>
				</tr>
				<tr>
					<td class="text-center">
						<div class="has-success">
							<input type="text" name="image[<?=$id?>][<?=$lang?>][title]" value="<?=$image[$lang]['title']?>" class="form-control" placeholder="Title" />
						</div>
					</td>
				</tr>
				<tr>
					<td class="text-center">
						<div class="has-success">
							<textarea name="image[<?=$id?>][<?=$lang?>][description]" rows="3" class="form-control" placeholder="Description"><?=$image[$lang]['description']?></textarea>
						</div>
					</td>
				</tr>
				<tr>
					<td class="text-center">
						<div class="has-success">
							<input type="text" name="image[<?=$id?>][<?=$lang?>][link]" value="<?=$image[$lang]['link']?>" class="form-control" placeholder="Link" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	<?php endforeach; ?>
</div>