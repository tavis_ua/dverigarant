<li class="image">
	<button type="button" title="<?=$_lang['edit_text']?>" class="btn btn-xs btn-primary" data-image-edit="<?=basename($image)?>" data-image-position="<?=$pos?>"><span class="glyphicon glyphicon-th-list"></span></button>
	<span class="btn btn-danger btn-xs" data-image-remove="<?=basename($image)?>" title="<?=$_lang['image_delete']?>"><span class="glyphicon glyphicon-remove-sign"></span></span>
	<iframe
			id="<?=basename($image)?>"
			width="275"
			height="190"
			class="thumbnail"
			src="https://www.youtube.com/embed/<?=basename($image)?>?controls=0&showinfo=0&rel=0&loop=1&modestbranding=1&enablejsapi=1&fs=0&playlist=<?=basename($image)?>"
			allowfullscreen>
	</iframe>
	<i class="type fa fa-youtube fa-2x"></i>
	<i class="youtube_button play"></i>
	<input type="hidden" name="image[<?=basename($image)?>]" value="youtube" class="form-control" placeholder="<?=$_lang['image_name']?>" />
</li>