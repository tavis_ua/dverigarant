<li class="image">
	<button type="button" title="<?=$_lang['edit_text']?>" class="btn btn-xs btn-primary" data-image-edit="<?=basename($image)?>" data-image-position="<?=$pos?>"><span class="glyphicon glyphicon-th-list"></span></button>
	<span class="btn btn-danger btn-xs" data-image-remove="<?=basename($image)?>" title="<?=$_lang['image_delete']?>"><span class="glyphicon glyphicon-remove-sign"></span></span>
	<img src="<?=$modx->runSnippet("R", Array("img" => $image, "folder" => "backend/".$_GET['category'], "opt" => "w=265&h=180&zc=1"))?>" alt="<?php echo $image;?>" class="thumbnail" />
	<i class="type fa fa-file-image-o fa-2x"></i>
	<input type="hidden" name="image[<?=basename($image)?>]" value="image" class="form-control" placeholder="<?=$_lang['image_name']?>" />
</li>