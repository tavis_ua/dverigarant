<li class="image">
	<button type="button" title="<?=$_lang['edit_text']?>" class="btn btn-xs btn-primary" data-image-edit="<?=basename($image)?>" data-image-position="<?=$pos?>"><span class="glyphicon glyphicon-th-list"></span></button>
	<span class="btn btn-danger btn-xs" data-image-remove="<?=basename($image)?>" title="<?=$_lang['image_delete']?>"><span class="glyphicon glyphicon-remove-sign"></span></span>
	<video src="/<?=$image;?>" class="thumbnail" width='275' height='190' loop></video>
	<i class="type fa fa-file-movie-o fa-2x"></i>
    <i class="play_button fa fa-play-circle-o fa-5x"></i>
	<input type="hidden" name="image[<?=basename($image)?>]" value="video" class="form-control" placeholder="<?=$_lang['image_name']?>" />
</li>