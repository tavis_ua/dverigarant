# Seigershop

**Сборка магазина основанная на MODX Evolution 1.2**

### Важно

  * На хостинге должна быть возможно запуска функций **exec** и **shell_exec**

### Установка

Перед установкой необходимо установить права на запись для следующих папок:

* /assets/cache
* /assets/cache/rss
* /assets/images
* /assets/files
* /assets/flash
* /assets/media
* /assets/backup
* /assets/.thumbs
* /assets/export

И файлов:

* /assets/cache/siteCache.idx.php
* /assets/cache/sitePublishing.idx.php
* /inspiration/includes/config.inc.php

Если у Вас база данных MYSQL ниже версии 5.7, необходимо в файле ./install/setup.sql заменить все вхождения CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP на '0000-00-00 00:00:00'.

Для развёртывания CMS перейдите по адресу http(s)://ваш-сайт/install и следуйте рекомендациям установщика.

После установки удалить папку /install/

### Возможности

**Модули**

  * Shop - Функционал бекенда магазина
  * Language - Мультиязычность
  * SEO - Редиректы, счетчики, robots.txt, sitemap.xml
  * Pictures - Галлерея изображений и видео к ресурсам
  * Autologin - Функционал "Запомнить меня"
  * SMS Auth - Функционал двухфакторной авторизации в админ панель

**Снипеты**

  * Отправка почты
  * Хлебные крошки
  * Минификатор
  
**Ништячки**
  
  * PHP > 7.0
  * Мультиязычность
  * Минификация контента (Управление из админки)
  * Ограничение размеров изображений в модуле Pictures (Управляется из админки)
  * Ускорение загрузки каталога за счёт кеширования фильтров
  * Возможность полного копирования товара

### Ресурсы

Evo:
https://modx.com/community/modx-evolution

Download:
https://modx.com/download/evolution/

Documentation:
http://docs.evolution-cms.com/

@pm Avdieieva
@dev Gorban

