<?php
$modx_version      = '1.2 Artjoker'; // Current version number
$modx_release_date = 'Okt 05, 2017'; // Date of release
$modx_branch = 'Seigershop';         // Codebase name
$modx_full_appname = 'MODX '.$modx_branch.' '.$modx_version.' ('.$modx_release_date.')';