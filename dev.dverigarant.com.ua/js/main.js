// start default js
(function () {
    "use strict";
    var body = document.querySelector('body'),
    isMobile = false,
    scrollTopPosition,
    browserYou,
    _winWidth = $(window).outerWidth();

    var genFunc = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;

            this.initialized = true;

            this.build();
        },
        build: function () {
            // browser
            browserYou = this.getBrowser();
            if (browserYou.platform == 'mobile') {
                isMobile = true;
                document.documentElement.classList.add('mobile');
            } else {
                document.documentElement.classList.add('desktop');
            }
            if ((browserYou.browser == 'ie')) {
                document.documentElement.classList.add('ie');
            }
            if (navigator.userAgent.indexOf("Edge") > -1) {
                document.documentElement.classList.add('edge');
            }
            if (navigator.userAgent.search(/Macintosh/) > -1) {
                document.documentElement.classList.add('macintosh');
            }
            if ((browserYou.browser == 'ie' && browserYou.versionShort < 9) || ((browserYou.browser == 'opera' || browserYou.browser == 'operaWebkit') && browserYou.versionShort < 18) || (browserYou.browser == 'firefox' && browserYou.versionShort < 30)) {
                alert('Обновите браузер');
            }
            if (document.querySelector('.yearN') !== null) {
                this.copyright();
            }
        },
        copyright: function () {
            var yearBlock = document.querySelector('.yearN'),
            yearNow = new Date().getFullYear().toString();
            if (yearNow.length) {
                yearBlock.innerText = yearNow;
            }
        },
        getBrowser: function () {
            var ua = navigator.userAgent;
            var bName = function () {
                if (ua.search(/Edge/) > -1) return "edge";
                if (ua.search(/MSIE/) > -1) return "ie";
                if (ua.search(/Trident/) > -1) return "ie11";
                if (ua.search(/Firefox/) > -1) return "firefox";
                if (ua.search(/Opera/) > -1) return "opera";
                if (ua.search(/OPR/) > -1) return "operaWebkit";
                if (ua.search(/YaBrowser/) > -1) return "yabrowser";
                if (ua.search(/Chrome/) > -1) return "chrome";
                if (ua.search(/Safari/) > -1) return "safari";
                if (ua.search(/maxHhon/) > -1) return "maxHhon";
            }();

            var version;
            switch (bName) {
                case "edge":
                version = (ua.split("Edge")[1]).split("/")[1];
                break;
                case "ie":
                version = (ua.split("MSIE ")[1]).split(";")[0];
                break;
                case "ie11":
                bName = "ie";
                version = (ua.split("; rv:")[1]).split(")")[0];
                break;
                case "firefox":
                version = ua.split("Firefox/")[1];
                break;
                case "opera":
                version = ua.split("Version/")[1];
                break;
                case "operaWebkit":
                bName = "opera";
                version = ua.split("OPR/")[1];
                break;
                case "yabrowser":
                version = (ua.split("YaBrowser/")[1]).split(" ")[0];
                break;
                case "chrome":
                version = (ua.split("Chrome/")[1]).split(" ")[0];
                break;
                case "safari":
                version = ua.split("Safari/")[1].split("")[0];
                break;
                case "maxHhon":
                version = ua.split("maxHhon/")[1];
                break;
            }
            var platform = 'desktop';
            if (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())) platform = 'mobile';
            var browsrObj;
            try {
                browsrObj = {
                    platform: platform,
                    browser: bName,
                    versionFull: version,
                    versionShort: version.split(".")[0]
                };
            } catch (err) {
                browsrObj = {
                    platform: platform,
                    browser: 'unknown',
                    versionFull: 'unknown',
                    versionShort: 'unknown'
                };
            }
            return browsrObj;
        }
    };
    genFunc.initialize();
    //console.log(browserYou);
    
    $(document).on("click", ".js_validate button[type=submit], .js_validate input[type=submit]", function () {
        var valid = validate($(this).parents(".js_validate"));
        if (valid == false) {
            return false;
        }
    });

     /*Function validate*/
     function validate(form) {
        var error_class = "error";
        var norma_class = "pass";
        var item = form.find("[required]");
        var e = 0;
        var reg = undefined;
        var pass = form.find('.password').val();
        var pass_1 = form.find('.password_1').val();
        var email = false;
        var password = false;
        var phone = false;

        function mark(object, expression) {

            if (expression) {
				object.parents('.form__item').addClass(error_class).removeClass(norma_class).find('.text-error').show();
				e++;
				if (email && (object.val().length > 0)) {
					object.parents('.form__item').addClass('error-email').removeClass(error_class);
				}
				if (password) {
					if (object.val().length > 0) {
							if (object.val().length < 6) {
									object.parents('.form__item').addClass('error-date').removeClass(error_class);
							}
							else if  (object.val().length > 20) {
									object.parent('div').find('.text-error').text('Пароль должен содержать не более 20 символов');
							}
							else {
									object.parent('div').find('.text-error').text(object.attr("data-error-wrong"));
							}
					} else {
							object.parent('div').find('.text-error').text(object.attr("data-error-empty"));
					}
                }
                if (item && (object.val().length == 0)) {
                    //console.log(object.val().length);
                    object.parents('.form__item').addClass(error_class).removeClass(norma_class);
                }
				if (pass_1 !== pass) {
					object.parents('.form__item').attr('data-error');
				}
				if (phone && (object.val().length > 0)) {
					object.parents('.form__item').attr('data-error', 'Некорректный формат телефона');
				}
				e++;
			} else
				object.parents('.form__item').addClass(norma_class).removeClass(error_class).removeClass('error-email').find('.text-error').hide();
        }

        form.find("[required]").each(function () {
            switch ($(this).attr("data-validate")) {
                case undefined:
                    mark($(this), $.trim($(this).val()).length == 0);
                    break;
                case "email":
                    reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
                case "phone":
                    reg = /[0-9 -()+]{10}$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
                case "select2":
                    //console.log($(this), $(this).val());
                    if ($(this).val()!="default") {
                        //console.log("select2pass");
                        mark($(this), false);
                        break;
                    } else {
                        mark($(this), true);
                        break;
                    };
                case "search": 
                	//console.log($(this), $(this).val());
                	reg = /^([A-Za-zА-Яа-ёя0-9_\-\.]{3,})$/;
                	mark($(this), !reg.test($.trim($(this).val())));
                    break;
                case "pass":
                    password = true;
                    reg = /^[a-zA-Z0-9_-]{6,}$/;
                    mark($(this), !reg.test($.trim($(this).val())));
                    password = false;
                    break;
                case "pass1":
                    mark($(this), pass_1 !== pass || $.trim($(this).val()).length === 0);
                    break;
                default:
                    reg = new RegExp($(this).attr("data-validate"), "g");
                    mark($(this), !reg.test($.trim($(this).val())));
                    break;
            }
        });
        e += form.find("." + error_class).length;
        if (e == 0)
            return true;
        else {
            $('.js_alert_error').show();
            setTimeout(function () {
                $('.js_alert_error').hide();
            }, 4000);
            form.find('.error input:first').focus();
            return false;
        }
    }
    /*Function validate end*/

})();
/*TABS HASH */
(function( $ ){
    /* default option */
  
    var defaultOptions = {
      hash: false,
      active: 1,
      afterShow: null
    };
  
    var methods = {
      init : function( options ) { 
        // option
        options = $.extend({}, defaultOptions, options);
        var elementA = this.find('a'),
            containerTab = this.next(),
            boxTab = this.next().find('.tabs-block'),
            hashWindow = window.location.hash && window.location.hash.replace('-tab', '');
  
            /* check hash after load */
            if (hashWindow && hashWindow.length && options.hash) {
              setActiveTabLoad('', elementA, boxTab, hashWindow);
            } else {
              /* set active tab */
              setActiveElement(options.active, elementA);
              setActiveBoxTab(options.active, boxTab);
            }
  
            /* event click on a*/ 
            eventHandler(elementA, boxTab, options.hash, options);
      }
    };
  
    function setActiveElement (active, elementA, id) {
      if (id) {
        $(elementA).each(function(i,e){
            $(e).removeClass('active');
        });
        $('[href="'+ id +'"]').addClass('active');
      } else {
        $(elementA).each(function(i, item) {
          $(item).removeClass('active')
          if ( (i + 1) == active) {
            $(item).addClass('active')
          }
        })
      }
    }
  
    function setActiveBoxTab (active, box, id) {
      if (id) {
        $(box).removeClass('active').hide();
        $(id).addClass('active').show();
      } else {
        $(box).each(function(i, item) {
          $(item).removeClass('active').hide();
          if ( (i + 1) == active) {
            $(item).addClass('active').show();
          }
        })
      }
    }
  
    function setActiveTabLoad (active, elementA, box, id) {
      setActiveElement('', elementA, id)
      setActiveBoxTab('', box, id)
    }
  
    function eventHandler (elementA, box, hash, settings) {
      elementA.on('click', function() {
        var activeTab = $(this).attr('href'),
            _this = this;
        if (hash) window.location.hash = activeTab + '-tab';
        elementA.removeClass('active');
        $(this).addClass('active');
        setActiveBoxTab('', box, activeTab);
        if (typeof settings.afterShow === "function") {
            settings.afterShow(_this, activeTab);
        }
        return false
      })
    }
  
  
  
    $.fn.tabArt = function( method ) {
  
      if ( methods[method] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
      } else if ( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
      } else {
        $.error( 'The ' +  method + ' is not undefined' );
      } 
    };
  
  })( jQuery );

    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
        parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
        parent.find('input[name=' + fieldName + ']').val(1);
        }
        parent.find('input[name=' + fieldName + ']').trigger('change');
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 1) {
        parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
        parent.find('input[name=' + fieldName + ']').val(1);
        }
        parent.find('input[name=' + fieldName + ']').trigger('change');
    }

    var orderMsg = '',
        editMsg = $('.edit-msg');

    function saveOrderMessage() {
        orderMsg = $('.order-msg').val();
        editMsg.text(orderMsg);
        //console.log(editMsg);
        return orderMsg;
    }

        /*Function for same height*/
    function heightBlock() {
        $('.js_height-block').each(function (i, e) {
            var elH = e.getElementsByClassName("height");
            var maxHeight = 0;
            for (var i = 0; i < elH.length; ++i) {
                elH[i].style.height = "";
                if (maxHeight < elH[i].clientHeight) {
                    maxHeight = elH[i].clientHeight;
                }
            }
            for (var i = 0; i < elH.length; ++i) {
                elH[i].style.height = maxHeight + "px";
            }
        }
        )
    }
    /*Function for same height end*/
    /*Function for go to top*/
    $('.js__go-to-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    /*Function for go to top end*/

$(document).ready(function () {

    $('.js_save-msg').on('click', function(e) {
        e.preventDefault();
        saveOrderMessage();
        $(this).parents('.comments-block').slideToggle();
        $(this).parents('.comments-block').next().addClass('active');
        let _url      = window.location.href.split("/").slice(0, 4).join("/")+"/";
        let _textarea = $(this).parents('form').find('textarea').serialize();
        let product   = $('[data-buy]').data('buy');
        $.ajax({
            type: "GET",
            url: _url,
            dataType: 'json',
            cache: false,
            data: 'ajax=change_comment&&product='+product+'&' + _textarea,
            success: function (response) {
                //console.log(response);
            },
        });
    });

    $('.js_edit-msg').on('click', function(e) {
        e.preventDefault();
        $(this).parents('.comments-block--edit').removeClass('active');
        $(this).parents('.comments-block--edit').prev().slideToggle();
    });

    var $mainSlider = $('.main-slider'),
        $productSlider = $('.product-slider'),
        $advantagesListSlider = $('.advantages-list'),
        $articlesListSlider = $('.articles-list'),
        $topSaleSlider = $('.top-sale-slider'),
        $appointmentSlider = $('.appointment-list'),
        $materialsSlider = $('.materials-list'),
        $doorsTypeSlider = $('.door-types-list'),
        $productGallery = $('.product-gallery'),
        $productGalleryNav = $('.product-gallery-nav'),
        $productGalleryFS = $('.product-gallery-fs'),
        $productGalleryNavFS = $('.product-gallery-fs-nav'),
        $productPageSlider1 = $('.slider-first .product-page-slider'),
        $productPageSlider2 = $('.slider-second .product-page-slider'),
        $equipmentSlider = $('.equipment-list'),
        $articleProductSlider = $('.article-products-slider'),
        $factsSlider = $('.facts');

    $mainSlider.slick({
        dots: true,
        slidesToShow: 1,
        fade: true,
        arrows: true, 
        prevArrow: '<button type="button" class="slick-prev"></button>',
        nextArrow: '<button type="button" class="slick-next"></button>',
        responsive: [
            {
            breakpoint: 1025,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    function productSliderInit() {
        $productSlider.slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-arrow"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="icon icon-arrow"></i></button>',
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
    productSliderInit();
    

    $topSaleSlider.slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:  $('.slider-nav__item--prev'),
        nextArrow:  $('.slider-nav__item--next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $productGallery.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:  $('.slider-nav__item--prev'),
        nextArrow:  $('.slider-nav__item--next'),
    });

    $productGalleryNav.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.product-gallery',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

    function productGalleryInit() {
        $productGalleryFS.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow:  $('.slider-wrapper .slider-nav__item--prev'),
            nextArrow:  $('.slider-wrapper .slider-nav__item--next')
        });

        $productGalleryNavFS.slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.product-gallery-fs',
            focusOnSelect: true
        });
    }

    productGalleryInit();

    $productPageSlider1.slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:  $('.slider-first .slider-nav__item--prev'),
        nextArrow:  $('.slider-first .slider-nav__item--next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $productPageSlider2.slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:  $('.slider-second .slider-nav__item--prev'),
        nextArrow:  $('.slider-second .slider-nav__item--next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    // $equipmentSlider.slick({
    //     infinite: false,
    //     slidesToShow: 4,
    //     slidesToScroll: 1,
    //     // focusOnSelect: true,
    //     arrows: true,
    //     prevArrow:  false,
    //     nextArrow:  $('.equipment-wrapper .slider-nav__item--next'),
    //     responsive: [
    //         {
    //             breakpoint: 1600,
    //             settings: {
    //                 slidesToShow: 3
    //             }
    //         },
    //         {
    //             breakpoint: 1200,
    //             settings: {
    //                 slidesToShow: 2
    //             }
    //         },
    //         {
    //             breakpoint: 1024,
    //             settings: {
    //                 slidesToShow: 3
    //             }
    //         },
    //         {
    //             breakpoint: 768,
    //             settings: {
    //                 slidesToShow: 2
    //             }
    //         },
    //         {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 2
    //             }
    //         },
    //         {
    //             breakpoint: 476,
    //             settings: {
    //                 slidesToShow: 1
    //             }
    //         }
    //     ]
    // });

    $articleProductSlider.slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        prevArrow:  $('.article-products .slider-nav__item--prev'),
        nextArrow:  $('.article-products .slider-nav__item--next'),
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1240,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $factsSlider.slick({ 
        slidesToShow: 4,
        arrows: false,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 2500,
        responsive: [
            {  
                breakpoint: 1240,
                settings: {
                    unslick: false,
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $(window).on('load resize orientationchange', function() {
        if(window.matchMedia("(max-width: 1024px)").matches) { 
            $advantagesListSlider.slick({
                slidesToShow: 2,
                dots: true,
                arrows: false,
                responsive: [
                    {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 1,
                        centerMode: true
                    }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1,
                        centerMode: false
                        }
                    }
                ]
            });
            $appointmentSlider.slick({
                slidesToShow: 2,
                dots: true,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            centerMode: false
                        }
                    }
                ]
            });

            $materialsSlider.slick({
                slidesToShow: 2,
                dots: true,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            centerMode: false
                        }
                    }
                ]
            });
        } else {
            $advantagesListSlider.slick('unslick');
            $appointmentSlider.slick('unslick');
            $materialsSlider.slick('unslick');
        }
        if(window.matchMedia("(max-width: 768px)").matches) {
            $articlesListSlider.slick({
                slidesToShow: 1,
                dots: true,
                arrows: false,
                adaptiveHeight: true
            });

            $doorsTypeSlider.slick({
                slidesToShow: 1,
                dots: true,
                arrows: false
            });
        } else {
            $articlesListSlider.slick('unslick');
            $doorsTypeSlider.slick('unslick');
        }
    });


    function changeHeader() {
        if(window.matchMedia("(max-width: 1280px)").matches) { 
            $('.header-desktop').hide();
            $('.header-mobile').show();
        } 
        if (window.matchMedia("(min-width: 1281px)").matches) {
            $('.header-desktop').show();
            $('.header-mobile').hide();
        }
    }
    changeHeader();

    $(window).resize(function() {
        changeHeader();
    });

    $('.tabs-wrapper .tabs-list').tabArt({
        hash: true, /* Если true, hash будет проставляться в урл\. и при за грузке будет прочитываться и ставиться активный */
        active: 1, /* активный tab при загрузке */
        afterShow: function (el, hash) {
            //console.log(el, hash)
            /* callback на активном табе после переключения*/
        } 
    });

    $('.menu-btn').on('click', function() {
        $(this).toggleClass('active');
        $('.header-menu-wrapper').toggle();
    });

    $('.languages').on('click', function() {
        $(this).children('.dropdown').toggle();
    });

    $('.js_sub-open').on('click', function() {
    		if($(this).parent().hasClass('active')) {
        	$(this).parent().removeClass('active');
	        $(this).parent().children('.submenu-wrapper').removeClass('open');
        } else {
	        $(this).parents('.main-menu').find('.active').removeClass('active');
	        $(this).parents('.main-menu').find('.open').removeClass('open');
	        $(this).parent().toggleClass('active');
	        $(this).parent().children('.submenu-wrapper').toggleClass('open');
        }
    });

    $('.order-items-switch').on('click', function() {
        $(this).toggleClass('closed');
        $(this).closest('.order-wrapper').children('.order-body').slideToggle();
        $(this).closest('.order-wrapper').children('.order-footer').toggleClass('no-margin');
    });

    $('.js_search').on('click', function(e) {
        e.preventDefault();
        $('.search-wrapper').show();
    });

    /*phone number mask */

    $('#phone-number, #phone, #phone-number-call').mask('+38(999)9999999', {
        placeholder: "+38(xxx)xxxxxxx"
    });

    /* QUANTITY */

    $(document).on('click', '.btn-qvt--plus', '.icon-plus', function(e) {
        incrementValue(e);
    });
    $(document).on('click', '.btn-qvt--minus', '.icon-minus' , function(e) {
        decrementValue(e);
    });

    /* popup */

    $('.header .btn-order, .btn-call, .js_call, .js_call-me, .js_show-filters').fancybox({
        smallBtn : true,
        toolbar  : false,
        btnTpl   : {
            smallBtn : '<button data-fancybox-close="" class="popup-close" title="Close"><i class="icon icon-close"></i></button>'
        }
    });

    $('#added_to_cart .btn-order').click(function(e){
        e.preventDefault();
            $.fancybox.close($('#added_to_cart'));
            $.fancybox.open($('#order'));
		});

		$(document).on('click touch', '.js_phones-drop', function() {
			$(this).next().toggle()
		});

    $('.product-gallery-link').fancybox({
        btnTpl   : {
            smallBtn : '<button data-fancybox-close="" class="popup-close" title="Close"><i class="icon icon-close"></i></button>'
        },
        baseTpl:
        '<div class="fancybox-container np" role="dialog" tabindex="-1">' +
        '<div class="fancybox-bg"></div>' +
        '<div class="fancybox-inner">' +
        '<div class="fancybox-stage"></div>' +
        '<div class="fancybox-caption"></div>' +
        "</div>" +
        "</div>",
    });

    $('.product-gallery-link').on('click', function() {
        $productGalleryFS.slick('unslick');
        $productGalleryNavFS.slick('unslick');
        productGalleryInit();
    });

    /*custom select */
    $('.js_select-checkout').select2({
        width: '100%'
    });

    $('.js_select-cost').select2({
        width: '190px',
        minimumResultsForSearch: -1
    });

    $('.js_select-popular').select2({
        width: '200px',
        minimumResultsForSearch: -1
    });

    $('.js_select-new').select2({
        width: '150px',
        minimumResultsForSearch: -1
    });

    $('.js_select').select2({
        width: '100%',
        minimumResultsForSearch: -1
    });
   
    $('.js_select-call').select2({
        width: '100%',
        placeholder: "Select a state",
        dropdownParent: $('.call-popup').parents('.wrapper')
    });

    $('.js_text-toggle').on('click', function() {
        $(this).prev().toggleClass('content--hide');
    });

    /* filters state */
    $('.filters-list-link').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
    });

    $('.filters-title').on('click', function() {
        $(this).toggleClass('active');
        $(this).next().slideToggle().toggleClass('active');
    });

    $('.js_add-comment').on('click', function() {
        $(this).toggleClass('active');
        $(this).next().slideToggle();
    })

    $('.range-slider').slider();

    $(".range-slider").on("slide", function(slideEvt) {
        $("#range-amount").val(slideEvt.value);
    });

		$(".range-slider").on("slideStop", function(slideEvt) {
        generateFilterUrl('price=' + '0' + '-' + slideEvt.value);
    });

    $('.equipment-list__item').click(function(e) {
        e.preventDefault();
        $(this).parents('.equipment-list').find('.equipment-list__item').removeClass('active');
        $(this).addClass('active');
    });

    $(".equipment-wrapper").mCustomScrollbar({
        axis:"x", // horizontal scrollbar
        theme:"orange",
        moveDragger:true,
        advanced:{autoExpandHorizontalScroll:true},
        mouseWheel:false,
        advanced:{ releaseDraggableSelectors: ".equipment-list__item-wrapper, equipment-list__item",
        extraDraggableSelectors: ".equipment-list, .equipment-list__item-wrapper, equipment-list__item", }
    });

    $('.tabs__link').on('click', function() {
        $('.tabs-block > .product-slider').slick('unslick');
        productSliderInit();
    });

    $(document).click(function(event) {

        if (!$(event.target).closest(".btn-search, .form-input--search").length) {
            $("body").find(".search-wrapper").hide();
        }

        if (!$(event.target).closest(".header-menu-wrapper, .menu-btn").length) {
            $("body").find(".menu-btn").removeClass('active');
            $("body").find(".header-mobile .header-menu-wrapper").hide();
        }

        if (!$(event.target).closest('.languages').length) {
            $("body").find(".languages .dropdown").hide();
        }

    });

});
