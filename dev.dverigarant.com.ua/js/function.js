$(document).ready(function() {
    console.log('test');
    /*------------------ Показать/скрыть похожие характеристики в груповом товаре -----------------*/
    function unique(array) {
        return array.filter(function (el, index, arr) {
            return index == arr.indexOf(el);
        });
    }
    var unique_button = true;
    var filters = [];
    $('[data-f_url]').each(function (i, e) {
        filters.push($(e).data('f_url'));
    })
    $.each(unique(filters), function (i, value) {
        let val = [];
        $('[data-f_url=' + value + ']').each(function (i, el) {
            val.push($(el).find('.value').text());
        });
        if (unique(val).length == 1) {
            unique_button = false;
        }
    });
    if (unique_button) {
        $('.toggle-equipment').hide();
    }
    $(document).on('click', '[data-unique]', function (e) {
        if ($(this).data('unique') == 'show') {
            $('[data-f_url]').show();
            $(this).hide();
            $('[data-unique="hide"]').addClass('active');
            $('[data-unique="hide"]').show();
        } else if ($(this).data('unique') == 'hide') {
            // function unique(array) {
            //     return array.filter(function (el, index, arr) {
            //         return index == arr.indexOf(el);
            //     });
            // }
            var filters = [];
            $('[data-f_url]').each(function (i, e) {
                filters.push($(e).data('f_url'));
            })
            $.each(unique(filters), function (i, value) {
                let val = [];
                $('[data-f_url=' + value + ']').each(function (i, el) {
                    val.push($(el).find('.value').text());
                });
                if (unique(val).length == 1) {
                    $('[data-f_url=' + value + ']').hide()
                }
            });
            $(this).hide();
            $('[data-unique="show"]').addClass('active');
            $('[data-unique="show"]').show();
        }
    })
    /*------------------ Показать/скрыть похожие характеристики в груповом товаре -----------------*/
    /*------------------ Сбросить фильтр каткекгорий на странице поиска -----------------*/
    $(document).on('click', '.js_get_all', function (e) {
        $('.js_fcat').removeClass('active');
        return location.href = $(this).attr('href');
    })
    /*------------------ Сбросить фильтр каткекгорий на странице поиска -----------------*/
    /*------------------ Доп услуги в корзине -----------------*/
    $(document).on('click', '.js_services', function (e) {
        let _this = $(this);
        let serv  = _this.attr('id');
        let cost = parseInt(_this.data('cost'));
        let total = parseInt($('.js_total_checkout').text().replace(/\s/, ""));
        if (_this.prop('checked') === true) {
            $('.js_' + serv).text(_this.data('yes'));
            $('.js_' + serv).addClass('active');
            $('.js_total').html(priceFormat(total + cost));
            $('[name="checkout[price][cart]"]').val(total + cost);
        } else if (_this.prop('checked') === false) {
            $('.js_' + serv).text(_this.data('no'));
            $('.js_' + serv).removeClass('active');
            $('.js_total').html(priceFormat(total - cost));
            $('[name="checkout[price][cart]"]').val(total - cost);

        }
    })
    function calcSumPrice(element) {
        var count = 1;
        if ($("input.js-changeAmount").length > 0) {
            count = $("input.js-changeAmount").val();
        }
        if ($('.equipment-list__item.active').length) {
            var cost = $('.equipment-list__item.active').data('po_price') * count;
            var cost_old = $('.equipment-list__item.active').data('po_price_old') * count;
        } else {
            var cost =  $('.js_price').data('po_price_else') * count;
            var cost_old = $('.js_price_old').data('po_price_old_else') * count;
        }
        let addEqp = 0;
        $.each($('.styled'), function (key, item) {
            if(item.checked) {
                addEqp += parseFloat($(item).attr('addEqp-price'));
            }
        })
        $.each($('.parameters_item'), function (key, element) {
            addEqp += parseFloat($('option:selected', element).attr('price'));
        });
        cost += addEqp*count;

        if (cost > 0) {
            $('.js_price').html(cost);
        }
        if (cost_old > 0) {
            $('.js_price_old').html(cost_old);
            $('.js_price_old').parent().show();
        } else {
            $('.js_price_old').parent().hide();
        }
        if ($(this).data('product_code') != '') {
            $('.js_product_code').text($(this).data('product_code'));
        }
        $('[data-buy]').data('buy', $(this).data('poid'))
        $('[data-buy]').attr('data-buy', $(this).data('poid'))
        $('[data-buy]').text($('[data-buy]').data('buy_string'));
        $('[data-buy]').removeClass('btn-bordered');
        $('[data-buy]').addClass('btn-orange');
        $('[data-buy]').removeAttr('disabled');
    }
    /*------------------ Доп услуги в корзине -----------------*/
    /*------------------ Груповой товар -----------------*/
    if (!$('form.js_select_options').length) {
        $(document).on('change', 'select.parameters_item', function () {
            calcSumPrice(this);
        });
        $(document).on('click change', '[data-poid], .js-changeAmount, input.styled[type="checkbox"]', function () {
            calcSumPrice(this);
        })
    }
    /*------------------ Груповой товар -----------------*/
    /*------------------ Вариации товара -----------------*/
    $(document).on('change', 'form.js_select_options input, form.js_select_options select', function (e) {
        let _url      = window.location.href.split("/").slice(0, 4).join("/")+"/";
        let _form = $(this).parents('form');
        var count   = 1;
        if ($("input.js-changeAmount").length > 0) {
            count = $("input.js-changeAmount").val();
        }
        $.ajax({
            type: "POST",
            url: _url,
            dataType: 'json',
            cache: false,
            data: 'ajax=change_option&' + _form.serialize(),
            success: function (response) {
                if (response.status == 1) {
                    if (response.price > 0) {
                        $('.js_price').text(response.price);
                    }
                    if (response.price_old > 0) {
                        $('.js_price_old').html(response.price_old * count);
                        $('.js_price_old').parent().show();
                    } else {
                        $('.js_price_old').parent().hide();
                    }
                    if (response.product_code != '') {
                        $('.js_product_code').text(response.product_code);
                    }
                    $('[data-buy]').data('buy', response.id)
                    $('[data-buy]').attr('data-buy', response.id)
                    $('[data-buy]').text($('[data-buy]').data('buy_string'));
                    $('[data-buy]').removeClass('btn-bordered');
                    $('[data-buy]').addClass('btn-orange');
                    $('[data-buy]').removeAttr('disabled');
                } else {
                    $('[data-buy]').removeClass('btn-orange');
                    $('[data-buy]').addClass('btn-bordered');
                    $('[data-buy]').attr('disabled','disabled');
                }
            },
        });
    })
    /*------------------ Вариации товара -----------------*/
    /*------------ Количество товара в корзине -----------*/
    $(document).on('change keyup', 'input.js_cart_quantity', function (e) {
        let _this   = $(this);
        let product = _this.attr("data-pid");
        var count   = 1;
        if (_this.length > 0) {
            count = _this.val();
        }
        let _url    = window.location.href.split("/").slice(0, 4).join("/")+"/";
        let height  = $('.js_cart_list').height();
        $('.js_cart_list').height(height);
        $.ajax({
            url: _url,
            dataType: "json",
            type: "GET",
            data: "ajax=buy&product="+product+"&count="+count+"&strong=1/",
            success: function(res){
                if (res.status == 1) {
                    $('.js_count').html(res.count);
                    $('.js_declination').html(res.declination);
                    $('.js_total').html(res.total);
                    $('[name="checkout[price][cart]"]').val(parseInt($('.js_total_checkout').text().replace(/\s/, "")));
                    $('.js_services').each(function (i,e) {
                        if($(e).prop('checked') === true) {
                            let cost = parseInt($(e).data('cost'));
                            let total = parseInt($('.js_total_checkout').text().replace(/\s/, ""));
                            $('.js_total').html(total + cost);
                            $('[name="checkout[price][cart]"]').val(total + cost);

                        }
                    })
                    $('.js_cart_list').html(res.cart);
                }
            }
        });
        return false;
    });
    /*------------ Количество товара в корзине ---------------*/
    /*------------------ Покупка в один клик -----------------*/
    $(document).on("click", ".js_buy_click", function () {
        var button  = $(this);
        var product = button.attr("data-buy-click");
        var count   = 1;
        if ($("input.js-changeAmount").length > 0) {
            count = $("input.js-changeAmount").val();
        }
        if(product > 0) {
            $('.js_product').val(product);
            $('.js_product_count').val(count);
            $('#buy-click').modal('show');
        }
        return false;
    });
    /*------------------ Покупка в один клик -----------------*/
    /*---------------------- Хеши и табы ---------------------*/
    if (window.location.hash != "") {
        var tab = window.location.hash;
        tab = tab.substring(0, tab.length - 4);
        $("[data-toggle='tab'][href='" + tab + "']").trigger("click");
    }
    $(document).on("click", 'a[data-toggle="tab"]', function () {
        window.location.hash = $(this).attr("href") + "-tab";
    });
    /*---------------------- Хеши и табы ---------------------*/
    /*------------------- Загрузка аватарки ------------------*/
    $(".js_upload_avatar").on("click", function() { // Событие по которому мы открываем форму загрузки файлов
        $(this).siblings('input:file[name="upload_avatar[]"]').trigger('click');
    });

    $('input:file[name="upload_avatar[]"]').on("change", function() { // Если что то пользователь выбрал в качестве аватарки
        var _url      = window.location.href.split("/").slice(0, 4).join("/")+"/";
        var file_data = $(this).prop("files")[0];
        var ext       = file_data.name.split(".").pop();
        var size      = file_data.size; // filesize

        if ($.inArray( ext, ['jpg','png','jpeg','JPG','PNG','JPEG']) >= 0) { // Не даем загрузить что либо помимо изображений
            var formData = new FormData();
            formData.append('upload_avatar[]', file_data);
            formData.append('ajax', 'upload_avatar');

            $.ajax({
                url: _url,
                dataType: "json",
                type: "POST",
                contentType: false,
                processData: false,
                data: formData,
                success: function (res) {
                    if (res.status == 1) {
                        $(".js_upload_avatar").attr('src', res.image);
                    }
                }
            });
        } else {
            var validate = $(this).data('validate'); // Допустима загрузка только изображений
            showModal(validate);
        }
    });
    /*------------------- Загрузка аватарки ------------------*/
    /*-------------------- Отправка формы --------------------*/
    $(".js_ajax").on("submit", function() {
        var _form = $(this);
        var _url  = window.location.href.split("/").slice(0, 4).join("/")+"/";

        if (_form.find('input:file').length) {
            var data        = new FormData( _form.get( 0 ) );
            var contentType = false;
        } else {
            var data        = _form.serialize();
            var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
        }

        $.ajax({
            url: _url,
            dataType: "json",
            type: "POST",
            contentType: contentType,
            processData: contentType,
            data: data,
            beforeSend: function() { // Не даем несколько раз нажать кнопку
                _form.find("input[type=submit]").attr('disabled','disabled');
                _form.find("button[type=submit]").attr('disabled','disabled');
            },
            success: function(res){
                if (res.status == 1) {
                    _form.trigger( 'reset' ); // Очищаем форму
                    $.fancybox.close();
                    $.fancybox.open({src: '#'+res.modal});                }
                _form.find("input[type=submit]").removeAttr('disabled');
                _form.find("button[type=submit]").removeAttr('disabled');
            }
        });
        return false;
    });
    /*-------------------- Отправка формы --------------------*/
    /*------------------ Добавить в избранное ------------------*/
    $(document).on("click", "[data-wish]", function () {
        var button  = $(this);
        var product = button.attr("data-wish");
        var _url    = window.location.href.split("/");

        $.ajax({
            url: _url.slice(0, 4).join("/")+"/",
            dataType: "json",
            type: "GET",
            data: "ajax=wishlist&product="+product,
            success: function(res){
                alert(res.message);
                button.find('span').text(res.text); // Меняем текст на кнопке
                
                if (res.count > 0) { // Прячем, добавляем ссылку на Избранное
                    $(".js_wishlist_count").parent().attr('href', $(".js_wishlist_count").parent().data('href'));
                } else {
                    $(".js_wishlist_count").parent().removeAttr('href');
                }

                if (res.status == 1) {
                    button.addClass("active");
                    $(".js_wishlist_count").text(res.count);
                } else {
                    button.removeClass("active");
                    $('.wishlist_' + product).remove(); // Удаляем визуально товар со страницы Избранное (css только внутри страницы избранного есть)
                    $(".js_wishlist_count").text(res.count);
                    if (res.count == 0 && $('#wishlist').size()) { // Проверяем есть ли id wishlist, что бы сделать редирект на главную
                        $("#alert").on('hide.bs.modal', function() {
                            var _url = window.location.href.split("/").slice(0, 4).join("/")+"/";
                            window.location.href = _url
                        });
                    }
                }
            }
        });
        return false;
    });
    /*------------------ Добавить в избранное ------------------*/
    /*---------------- Автокомплит для поиска ----------------*/
    $(".js_ajaxSearch").on("keyup input change", function() {
        var _form = $(this);
        var _url  = window.location.href.split("/");
        var data  = _form.find('[name="search"]').val();
        if (data.length > 2) {
            $.ajax({
                url: _url.slice(0, 4).join("/")+"/",
                dataType: "json",
                type: "POST",
                data: "ajax=search&search="+data,
                success: function(res){
                    if (res.status == 1) {
                        $("#searchResult").html(res.content);
                        var contentLength = res.content.length;
                        if (contentLength > 0) {
                            $("#searchResult").removeAttr("style");
                            $("#searchResult").attr("style", "display:block;");
                        } else {
                            $("#searchResult").removeAttr("style");
                            $("#searchResult").attr("style", "display:none;");
                        }
                    } else {
                        $("#searchResult").removeAttr("style");
                        $("#searchResult").attr("style", "display:none;");
                        $("#searchResult").html('');
                    }
                }
            });
        }
        return false;
    });
    /*---------------- Автокомплит для поиска ----------------*/
    /*---------------- Открытие мини корзины -----------------*/
    if (parseFloat($('.js_cart_count').text()) > 0) {
        $('.js_cart_menu').removeAttr('style');
        $('.js_cart-empty').removeClass('cart-empty');
    }
    /*---------------- Открытие мини корзины -----------------*/
    /*-------------------- Сменить пароль --------------------*/
    $(document).on("click", ".js_changePassword", function () {
        $('[name="account[password]"]').removeAttr("readonly");
        return false;
    });
    /*-------------------- Сменить пароль --------------------*/
    /*------------- Добавить/удалить новый адрес -------------*/
    $(document).on("click", ".js_add_address", function() {
        var button = $(this);
        var _url = window.location.href.split("/");
        var buttonRemoveAddr = $('.js-removeAddr');
        $.ajax({
            url: _url.slice(0, 4).join("/") + "/",
            dataType: "json",
            type: "GET",
            data: "ajax=addaccountaddress&index=" + $("input").length,
            success: function(res) {
                if (res.status == 1) {
                    button.parent().before(res.content);
                    $('input[data-validate="phone"]').unmask().mask("+38(999) 999 99 99");
                    buttonRemoveAddr.show();
                }
            }
        });
        return false;
    });
    if($('.account-form').find(".input-group-wrapper").length == 1) {
        $('.js-removeAddr').hide();
    }
    $(document).on("click", ".js-removeAddr", function(event) {
        console.log($('.account-form').find(".input-group-wrapper").length);
        if($('.account-form').find(".input-group-wrapper").length < 3) {
            $(this).parents('.input-group-wrapper').remove();
            $('.js-removeAddr').hide();
        } else{
            $(this).parents('.input-group-wrapper').remove();
        }
    });
    /*------------- Добавить/удалить новый адрес -------------*/
    /*--------------------- Смена валюты ---------------------*/
    $(document).on("click", "[data-currency]", function () {
        var button = $(this);
        var _url   = window.location.href.split("/");
        $.ajax({
            url: _url.slice(0, 4).join("/")+"/",
            dataType: "json",
            type: "GET",
            data: "ajax=changecurrency&code="+button.attr('data-currency'),
            success: function(res){
                if (res.status == 1) {
                    window.location.reload();
                }
            }
        });
        return false;
    });
    /*--------------------- Смена валюты ---------------------*/
    /*---------------- Инициализация фильтра -----------------*/
    $(document).on("click", "[data-filter]", function () {
        generateFilterUrl($(this).attr("data-filter"), $(this).attr("type"));
        return false;
    });
    $(document).on("click", "[data-price]", function () {
        var priceMin = parseFloat($("[name*='min']").val());
        var priceMax = parseFloat($("[name*='max']").val());
        generateFilterUrl('price='+priceMin+'-'+priceMax);
        return false;
    });
    /*---------------- Инициализация фильтра -----------------*/
    /*---------------------- Сортировка ----------------------*///
    $(document).on("click", "[data-sort]", function () {
        var sort = $(this).attr("data-sort");
        var _url = window.location.href;
        var parts= _url.split('?');
        if (parts.length>=2) {
            var prefix = encodeURIComponent("sort")+"=";
            var pars= parts[1].split(/[&;]/g);
            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }
            _url= parts[0] + (pars.length > 0 ? "?" + pars.join('&') : "");
        }
        if (sort.length > 0) {
            if (_url.indexOf("?") < 0) {
                _url += "?sort=" + sort;
            } else {
                _url += "&sort=" + sort;
            }
        }
        window.location.href = _url;
        return false;
    });
    /*---------------------- Сортировка ----------------------*///
    /*------------------ Добавить в корзину ------------------*/
    $(document).on("click", "[data-buy]", function () {
        var button  = $(this);
        var product = button.attr("data-buy");
        var count   = 1;
        var _url    = window.location.href.split("/");
        if ($("input.js-changeAmount").length > 0) {
            count = $("input.js-changeAmount").val();
        }
        let addEqp = [];
        $.each($('input.styled'), function (key, item) {
            if(item.checked) {
                addEqp.push(item.value);
            }
        })
        let Eqp = [];
        $.each($('select.parameters_item'), function (key, item) {
            Eqp.push(item.value);
        })
        let comment = $('[name="comment"]').serialize();
        let eqp = addEqp.join(',');
        console.log(eqp);
        $.ajax({
            url: _url.slice(0, 4).join("/")+"/",
            dataType: "json",
            type: "GET",
            data: "ajax=buy&product="+product+"&count="+count+"&"+comment+"&eqp="+Eqp.join(',')+"&add_eqp="+addEqp.join(','),
            success: function(res){
                if (res.status == 1) {
                    $('.js_count').html(res.count);
                    $('.js_declination').html(res.declination);
                    $('.js_total').html(res.total);
                    $('.js_cart_list').html(res.cart);
                    button.text(button.data('bought'));
                    button.addClass('btn-bordered');
                    button.removeClass('btn-orange');
                    button.attr('disabled', 'disabled');
                    $.fancybox.open({src: '#added_to_cart'});
                    if (res.count > 0) {
                        $('.js_ico_cart').addClass('btn-order active');
                        $('.js_ico_cart').attr('href', '#order')
                    }
                }
            }
        });
        return false;
    });
    /*------------------ Добавить в корзину ------------------*/
    /*------------------ Удалить из корзины ------------------*/
    $(document).on("click", "[data-delete]", function () {
        var button  = $(this);
        var product = button.attr("data-delete");
        var _url    = window.location.href.split("/");
        var reload  = 0;
        if (button.attr('data-reload') && button.attr('data-reload').length > 0) {
            reload = 1;
        }
        $.ajax({
            url: _url.slice(0, 4).join("/")+"/",
            dataType: "json",
            type: "GET",
            data: "ajax=deleteProductCart&product="+product+"&reload="+reload,
            success: function(res){
                if (res.status == 1) {
                    $('[data-buy]').text($('[data-buy]').data('buy_string'));
                    $('[data-buy]').removeClass('btn-bordered');
                    $('[data-buy]').addClass('btn-orange');
                    $('[data-buy]').removeAttr('disabled');
                    $('.js_count').html(res.count);
                    $('.js_declination').html(res.declination);
                    $('.js_total').html(res.total);
                    $('.js_cart_list').html(res.cart);
                    if (res.count > 0) {
                        $('.js_ico_cart').addClass('btn-order active');
                    } else {
                        $('.js_ico_cart').removeClass('btn-order active');
                        $('.js_count').text('');
                        $.fancybox.close({src: '#order'});
                        $('.js_ico_cart').attr('href', '#empty_cart');
                        if ($('.js_checkout_submit').length) {
                            $('.js_checkout_submit').prop('disabled', true);
                            window.location.href = _url.slice(0, 4).join("/") + "/";
                        }
                    }
                }
            }
        });
        return false;
    });
    /*------------------ Удалить из корзины ------------------*/
    /*-------- Изменить колличество товара в корзине ---------*/
    $(document).on("change", ".js_change_quantity", function () {
        var item    = $(this);
        var product = item.parents("tr").find('[data-delete]').attr('data-delete');
        var price   = parseFloat(item.parents("tr").find('.js_item_price').html().replace('&nbsp;', ''));
        var count   = parseInt(item.val());
        var total   = priceFormat(price * count);
        item.parents("tr").find(".js_item_total").html(total);

        if (item.parents("tr").find('[name="quant[1]"]').length > 0) {
            count = item.parents("tr").find('[name="quant[1]"]').val();
        }
        $.ajax({
            url: window.location.href,
            dataType: "json",
            type: "GET",
            data: "ajax=buy&product="+product+"&count="+count+"&strong=1",
            success: function(res){
                if (res.status == 1) {
                    $(".js_cart_count").text(res.count);
                    $(".js_cart_declination").text(res.text);
                    $(".js_cart_total").html(res.total);
                    $(".js_miniCartList").html(res.miniCart);
                }
            }
        });
    });
    /*-------- Изменить колличество товара в корзине ---------*/
    /*------ Подсчитать сумму всего заказа с доставкой -------*/
    if ($("[name=\"checkout[price][delivery]\"]").length > 0) {
        var price = parseFloat($("[name=\"checkout[price][delivery]\"]").val());
        var cartt = parseFloat($("[name=\"checkout[price][cart]\"]").val());
        var total = parseFloat(price + cartt);

        $(".js_price_total").html(priceFormat(total));
        $("[name=\"checkout[price][total]\"]").val(total.toFixed(2));
    }
    /*------ Подсчитать сумму всего заказа с доставкой -------*/
    /*------------------ Применить промокод ------------------*/
    $(document).on("click", ".js_promocode", function () {
        var button  = $(this);
        var code    = button.parent().find("[name=\"checkout[payment][code]\"]").val();
        $.ajax({
            url: window.location.href,
            dataType: "json",
            type: "GET",
            data: "ajax=promocode&code="+code,
            success: function(res){
                if (res.status == 1) {
                    button.parent().find(".table-row").removeClass("has-error");
                    button.parent().find(".table-row").addClass("has-success");
                    $(".js_price_discount").html(res.summa);
                    $("[name=\"checkout[price][discount]\"]").val(res.summa);
                    showModal(res.text);
                } else {
                    button.parent().find(".table-row").removeClass("has-success");
                    button.parent().find(".table-row").addClass("has-error");
                    $(".js_price_discount").html("0");
                    $("[name=\"checkout[price][discount]\"]").val(0);
                    showModal(res.text);
                }

                var price = parseFloat($("[name=\"checkout[price][delivery]\"]").val());
                var disco = parseInt($("[name=\"checkout[price][discount]\"]").val());
                var cartt = parseFloat($("[name=\"checkout[price][cart]\"]").val());
                var summa = parseFloat(cartt - (cartt * (disco / 100)));
                var total = parseFloat(price + summa);

                $(".js_price_total").html(priceFormat(total));
                $("[name=\"checkout[price][total]\"]").val(total.toFixed(2));
            }
        });
    });
    /*------------------ Применить промокод ------------------*/
    /*----------------- Выбор метода доставки ----------------*/
    $(document).on("change", "#delivery-method", function () {
        var value = $("#delivery-method option:selected").val();
        $.ajax({
            url: window.location.href,
            dataType: "json",
            type: "POST",
            data: "ajax=deliveryMethod&delivery=" + value,
            success: function(res) {
                if (res.status == 1) {
                    $(".delivery-method").html(res.content);
                    var price = parseFloat(res.delivery_price);
                    var cartt = parseFloat($("[name=\"checkout[price][cart]\"]").val());
                    var disco = parseInt($("[name=\"checkout[price][discount]\"]").val());
                    var summa = parseFloat(cartt - (cartt * (disco / 100)));
                    var total = parseFloat(price + summa);
                    $(".js_price_delivery").html(priceFormat(price));
                    $("[name=\"checkout[price][delivery]\"]").val(price.toFixed(2));
                    $(".js_price_total").html(priceFormat(total));
                    $("[name=\"checkout[price][total]\"]").val(total.toFixed(2));
                }
                $(".chosen-select").chosen({width:"100%"});
            }
        });
    });
    /*----------------- Выбор метода доставки ----------------*/
    /*---------------- Выбор адреса самовывоза ---------------*/
    $(document).on("change", "#self-city", function () {
        $(".self-city").html($("#self-city option:selected").data("value"));
        $(".chosen-select").chosen({width:"100%"});
    });
    /*---------------- Выбор адреса самовывоза ---------------*/
    /*----------- Выбор метода доставки авторизован ----------*/
    $(document).on("change", "#courier-city-authorize", function (value, inst) {
        var name = $("#courier-city-authorize option:selected").attr("data-name");
        var phone = $("#courier-city-authorize option:selected").attr("data-phone");
        var street = $("#courier-city-authorize option:selected").attr("data-street");
        var house = $("#courier-city-authorize option:selected").attr("data-house");
        var apartament = $("#courier-city-authorize option:selected").attr("data-apartament");
        var comment = $("#courier-city-authorize option:selected").attr("data-comment");
        var price = parseFloat($("#courier-city-authorize option:selected").attr("data-value"));
        var cartt = parseFloat($("[name=\"checkout[price][cart]\"]").val());
        var disco = parseInt($("[name=\"checkout[price][discount]\"]").val());
        var summa = parseFloat(cartt - (cartt * (disco / 100)));
        var total = parseFloat(price + summa);
        // Заполнение полей доставки
        $('[name="checkout[delivery][name]"]').val(name);
        $('[name="checkout[delivery][phone]"]').val(phone);
        $('[name="checkout[delivery][street]"]').val(street);
        $('[name="checkout[delivery][house]"]').val(house);
        $('[name="checkout[delivery][apartament]"]').val(apartament);
        $('[name="checkout[delivery][comment]"]').val(comment);
        // Подсчёт суммы доставки общей стоимости заказа
        $(".js_price_delivery").html(priceFormat(price));
        $("[name=\"checkout[price][delivery]\"]").val(price.toFixed(2));
        $(".js_price_total").html(priceFormat(total));
        $("[name=\"checkout[price][total]\"]").val(total.toFixed(2));
    });
    /*----------- Выбор метода доставки авторизован ----------*/
    /*------------ Доставка курьером выбор города ------------*/
    $(document).on("change", "#courier-city", function () {
        var price = parseFloat($("#courier-city option:selected").attr("data-value"));
        var cartt = parseFloat($("[name=\"checkout[price][cart]\"]").val());
        var disco = parseInt($("[name=\"checkout[price][discount]\"]").val());
        var summa = parseFloat(cartt - (cartt * (disco / 100)));
        var total = parseFloat(price + summa);
        $(".js_price_delivery").html(priceFormat(price));
        $("[name=\"checkout[price][delivery]\"]").val(price.toFixed(2));
        $(".js_price_total").html(priceFormat(total));
        $("[name=\"checkout[price][total]\"]").val(total.toFixed(2));
    });
    /*------------ Доставка курьером выбор города ------------*/
    /*--------------- Новая почта выбор города ---------------*/
    $(document).on("change", "#novaPoshta-city", function () {
        var city = $("#novaPoshta-city option:selected").data("value");
        $.ajax({
            url:window.location.href,
            dataType:"json",
            type:"POST",
            data:"ajax=novaPoshtaDepartaments&cityRef="+city,
            success:function(res) {
                if (res.status == 1) {
                    $("#novaPoshta-departament").chosen('destroy');
                    $("#novaPoshta-departament").html(res.content);
                    $("#novaPoshta-departament").chosen({width:"100%"});
                }
            }
        });
    });
    /*--------------- Новая почта выбор города ---------------*/
    /*---------- Добавить адресс оформление заказа -----------*/
    $(document).on("click", "#addDelivery", function () {
        $("[name=\"checkout[delivery][name]\"]").val($("[name=\"checkout[user][fullname]\"]").val());
        $("[name=\"checkout[delivery][name]\"]").attr("required", true);
        $("[name=\"checkout[delivery][phone]\"]").val($("[name=\"checkout[user][phone]\"]").val());
        $("[name=\"checkout[delivery][city]\"]").attr("required", true);
        $("[name=\"checkout[delivery][street]\"]").attr("required", true);
        $("[name=\"checkout[delivery][house]\"]").attr("required", true);
        $(".js_addAdressShow").remove();
        $(".js_addAdressHide").removeAttr("style");
        return false;
    });
    /*---------- Добавить адресс оформление заказа -----------*/
});
/*---------------- Генератор URL фильтра -----------------*/
function generateFilterUrl(filter, type) {
    if (type === undefined) {
        type = 'checkbox';
    }
    var newFilters = [];
    var searchFilter = true;
    // Текущий урл
    var _url   = window.location.pathname;
    var _get   = window.location.search;
    var regex  = /\/f\/(.*)/i;
    // Текущие фильтры
    _filterUrl = _url.match(regex);
    if (_filterUrl) {
        // Переданный фильтр
        var filter = filter.split("=");
        _filterArr = _filterUrl[0].split('/');
        // Список текущих фильтров
        _filters   = _filterArr[2].split(';');
        for (var i = 0; i < _filters.length; i++) {
            // Если текущий фильтр совпал с переданным, добавляем или заменяем значение
            var _filter = _filters[i].split("=");
            if (filter[0] == _filter[0]) {
                searchFilter = false;
                _values = _filter[1].split(',');
                // Проверить существует ли значение в массиве
                _index = _values.indexOf(filter[1]);
                if (_index != -1) {
                    _values.splice(_index, 1);
                } else {
                    _values.push(filter[1]);
                }
                // Если тип радио, всё равно удаляем значение
                if (type == 'radio') {
                    _values = [];
                    _values.push(filter[1]);
                }
                // Если цена, всё равно удаляем значение
                if (filter[0] == 'price') {
                    _values = [];
                    _values.push(filter[1]);
                }
                // Если в массиве что то осталось
                if (_values.length > 0) {
                    _values.sort();
                    _filter[1]  = $.unique(_values).join(',');
                    _filters[i] = _filter.join('=');
                    newFilters.push(_filters[i]);
                }
            } else {
                newFilters.push(_filters[i]);
            }
        }
        // Если текущий фильтр не совпал ни с одним из существующих
        if (searchFilter) {
            newFilters.push(filter.join('='));
        }
        // Если есть хоть какие то фильтры
        if (newFilters.length > 0) {
            newFilters.sort();
            var _newUrl = _url.slice(0, -_filterUrl[0].length)+'/f/'+newFilters.join(';')+'/';
        } else {
            var _newUrl = _url.slice(0, -_filterUrl[0].length)+'/';
        }
    } else {
        var _newUrl = _url+'f/'+filter+'/';
    }

    if (_get.length > 0) {
        window.location.href = _newUrl+_get;
    } else {
        window.location.href = _newUrl;
    }
}
/*---------------- Генератор URL фильтра -----------------*/
/*----------------- Форматирование цены ------------------*///
function priceFormat(n) {
    /* Фикс округления в JS */
    n = n * 100;
    n = n.toFixed(0);
    n = Math.round(n);
    n = n / 100;
    /* Фикс округления в JS */
    return n.toFixed(0).replace(/./g, function (c, i, a) {
        return i && c !== "." && ((a.length - i) % 3 === 0) ? "&nbsp;" + c : c;
    });
}
/*----------------- Форматирование цены ------------------*///
/*---------------- Авторизация через соц.сети ulogin -----------------*/
function socialAuth(token) {
    $.ajax({
        url: window.location.href.split("/").slice(0, 4).join("/")+"/",
        dataType: "json",
        type: "POST",
        data: "ajax=socialAuth&token="+token,
        success: function (res) {
            if (res.status == 1) {
                window.location.href = res.link;
            }
        }
    });
}
/*---------------- Авторизация через соц.сети ulogin -----------------*/